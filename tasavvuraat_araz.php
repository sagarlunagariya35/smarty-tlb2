<?php
require_once 'session.php';
require_once 'requires_login.php';

$body_class = 'page-sub-page';

$miqat_id = 5;
if(isset($_POST['submit'])){
  $audio_file = $_FILES['audio_file']['name'];
  $text = $_POST['text'];
  $its = $_SESSION[USER_ITS];
  
  $filearray = explode('.',$audio_file);
  $ext = $filearray[count($filearray)-1];
  $file_name = $its.'.'.$ext;
  $filetemp = $_FILES['audio_file']['tmp_name'];
  
  $pathANDname = "upload/tasavvuraat_araz/" . $file_name;
  $moveResult = move_uploaded_file($filetemp, $pathANDname);
  
  $insert = insert_tasavvuraat_araz($its,$file_name,$text);
  if($insert){
    $_SESSION[SUCCESS_MESSAGE] = 'Tasavvuraat Araz add Successfully..';
  } else {
    $_SESSION[ERROR_MESSAGE] = 'Error Encounter While Inserting Data';
  }     
}

$miqaat_istibsaar = get_single_miqaat_istibsaar($miqat_id);
$mawaqeet = get_all_mawaqeet_by_year($miqaat_istibsaar['year']);

$tasavvuraat_araz = get_araz_done_tasavvuraat_araz();
require_once 'inc/inc.header2.php';
?>

<!-- Contents -->
<!-- ====================================================================================================== -->
<div class="container white-bg">
  <div class="col-md-12 col-sm-12">
    <div class="page-title">
      <p style="margin-top:5px;">
        <a href="#" class="active">Asharah Mubarakah 1437 H</a></p>
      <h1>Asharah Mubarakah 1437 H<span class="alfatemi-text">معلومات ذاتية</span></h1>
    </div>
  </div>
  <div class="clearfix"></div> <!-- do not delete -->
  <?php if(isset($_SESSION[SUCCESS_MESSAGE]) OR isset($_SESSION[ERROR_MESSAGE])) { ?>
    <div class="row">
        <?php include_once 'inc/message.php'; ?>
    </div>
  <?php } ?>
  <form class="forms1 white" action="" name="raza_form" method="post" enctype="multipart/form-data">
    <div class="col-md-3 col-sm-12 hidden-xs">
      <div class="profile-box-static">
        <h3 class="uppercase text-center">Mawaqeet <?php echo $miqaat_istibsaar['year']; ?> H</h3>
      </div>
      <div class="profile-box-static-bottom">
        <?php
        foreach ($mawaqeet as $mqt) {
          echo "<a href=\"" . SERVER_PATH . "istibsaar/$mqt[id]/$mqt[slug]/$mqt[year]/" . "\">» $mqt[miqaat_title]<hr></a>";
        }

        $make_pre_active = '';
        $make_in_active = '';
        $make_post_active = '';

        if ($miqaat_istibsaar['show_post_event'] == '1') {
          $make_post_active = ' active';
        } elseif ($miqaat_istibsaar['show_in_event'] == '1') {
          $make_in_active = ' active';
        } elseif ($miqaat_istibsaar['show_pre_event'] == '1') {
          $make_pre_active = ' active';
        }
        ?>
      </div>
    </div>

    <div class="col-md-8 col-sm-12">
<!--      <div class="static-show-button lsd large-fonts col-xs-12 col-sm-5" dir="rtl">معلومات ذاتية</div>-->
      <span class="multitextbuttton uppercase col-xs-12 col-sm-5">Tasawwuraat Araz</span>
      <div class="clearfix"></div>
      <div>
        <h2 class="text-center">
          <input type="file" name="audio_file" accept="audio/*;capture=microphone" class="file">
          <hr>
        </h2>
        <div class="blue-box1">
          <h3 class="bordered">
            <div class="col-sm-12 shift">
              <div class="shift10">
                <textarea name="text" class="form-control" rows="5" cols="15" placeholder="Write your Tasawwur"></textarea>
              </div>
              <div class="shift10">
                <input type="submit" value="Submit" name="submit" class="submit">
              </div>
            </div>
            <div class="clearfix"></div>
          </h3>
          <div class="clearfix"></div>
        </div>
      </div>
      <div>
        <h2 class="text-center">Selected Tasawwuraat<hr></h2>
        <?php
          if($tasavvuraat_araz){
            foreach ($tasavvuraat_araz as $data) {
          ?>
            <div class="col-md-6">
              <br>
                <audio controls>
                  <source src="<?php echo SERVER_PATH . 'upload/tasavvuraat_araz/' . $data['audio_file']; ?>">
                </audio>
            </div>
            <div class="clearfix"></div>
          <?php
            }
          }
        ?>
      </div>
    </div>
    <div class="clearfix"></div>
  </form>
</div>

<?php
require_once 'inc/inc.footer.php';
?>
