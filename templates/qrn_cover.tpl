<div class="container white-bg">
  <div class="col-md-12 col-sm-12">
    <div class="page-title">
      <p style="margin-top:5px;">
        <a href="#" class="active">Al-Akh al-Qurani</a></p>
      <h1>Al-Akh al-Qurani</h1>
    </div>
  </div>
  <div class="clearfix"></div> <!-- do not delete -->
  <div class="col-md-12 col-sm-12">
    <form action="" method="post" name="enroll_form">
      <div class="body-container white-bg">
        <div class="enroll-wrapper">
          <div class="en-left qrn-intro">
            <p dir="ltr">
              <b>I want to perform Tasmee’ of My Hifz to My sadeeq:</b>
              <br/>
              (Note We will use the word Sadeeq for the person you connect with for tasmee and tehfeez. Sadeeq means friend) al Akh al Qurani)
            </p>
            <p dir="ltr">
              This is a tehfeez program based on the concept that everyone of us should help each other achieve the goal of Hifz al Quran given to us by Moula TUS, which
              would finally lead to the ultimate goal; “Har ghar ma ek Hafiz”.
            </p>
            <p dir="ltr">
              In this program, you will register yourself as a Talib e Ilm doing Hifz. You can then choose someone from a provided list to perform tasmee’ of your Quran
              in front of the person you chose. In simple words, you will be doing Hifz on your own, but for the assurance that you have memorized correctly, you will
              perform tasmee to your brother. He will take note of how much tasmee’ you have done on the <b>talabulilm</b> portal.
            </p>
            <p dir="ltr">
              This exercise will eventually lead to a daily routine of tasmee’ and a habitual way of memorizing the Quran. You will be able to see your daily progress,
              your overall graph, how many pages have you memorized and other beneficial details.
            </p>
            <p dir="ltr">
              When you reach the stage of progressing from one Sanaa’ to the next you will be prompted to attempt an Ikhtebar in the Mahad al Zahra elearning programme
              and will be able to obtain your Sanad on passing the Ikhtebaar. The breakdown of the four Sanaa’ are as follows:
            </p>
            <p dir="ltr">
              La Uqsemo
            </p>
            <p dir="ltr">
              Juz Amma
            </p>
            <p dir="ltr">
              Sanaa' 1: 1-5 Siparas
            </p>
            <p dir="ltr">
              Sanaa' 2:  6-12 Siparas
            </p>
            <p dir="ltr">
              Sanaa' 3: 13-21 Siparas
            </p>
            <p dir="ltr">
              Sanaa' 4:  22- Completion of Quran
            </p>
            <br/>
            <p dir="ltr">
              <b>I am also willing to listen to the Hifz of My Brothers.</b>
            </p>
            <p dir="ltr">
              This is a tehfeez program based on the concept that everyone of us should help each other achieve the goal of Hifz al Quran given to us by Moula TUS, which
              would finally lead to the ultimate goal; “Har ghar ma ek Hafiz”.
            </p>
            <p dir="ltr">
              While doing Hifz you should not forget to help your brothers do Hifz al Quran as well.
            </p>
            <p dir="ltr">
              In the very same manner you are performing tasmee’, you can help your brothers perform tasmee’ as well.
            </p>
            <p dir="ltr">
              To help a brother do Hifz al Quran, follow the given instructions:
            </p>
            <ul>
              <li dir="ltr">
                <p dir="ltr">
                  Log on to Talabulilm
                </p>
              </li>
              <li dir="ltr">
                <p dir="ltr">
                  Go to Hifz al Quran section in the Istifdaah menu
                </p>
              </li>
              <li dir="ltr">
                <p dir="ltr">
                  Select the check box
                </p>
              </li>
              <li dir="ltr">
                <p dir="ltr">
                  Enroll yourself by pressing the enroll button
                </p>
              </li>
              <li dir="ltr">
                <p dir="ltr">
                  You can then type in the ITS ID of the persons’ Quran you want to listen to
                </p>
              </li>
              <li dir="ltr">
                <p dir="ltr">
                  Select the Surat and Ayat till where your brother has reached
                </p>
              </li>
              <li dir="ltr">
                <p dir="ltr">
                  Press the Submit button.
                </p>
              </li>
            </ul>
            <p dir="ltr">
              As soon as you press the button you will have his details logged beneath the form. You will be able to see the details of all the persons you have done
              tasmee’ today.
            </p>
            <p dir="ltr">
              Detailed log will be available in the left hand panel in graphical and statistical form.
            </p>
            
            {if ($muhafiz_data_header == FALSE AND $mumin_data_header == FALSE)}
              <div class="__action">
                <a href="qrn_enroll.php" class="btn _wahid" style="text-align:center;">Proceed to Enroll</a>
              </div>
            {/if}
            
          </div>
          <div class="en-right">
            <img src="img/qrn-intro-banner.jpg" alt="Banner" width="100%" />
          </div>
            {if ($muhafiz_data_header OR $mumin_data_header)}
              {include file="include/qrn_links.tpl"}
            {/if}
        </div>
      </div>
    </form>
  </div>
</div>
{include file="include/js_block.tpl"}
{include file="include/footer.tpl"}