<div class="container white-bg">
  <div class="col-md-12 col-sm-12 bayan">
    <div class="page-title">
      <p style="margin-top:5px;"><a href="#">Home</a> / <a href="#">{$year} {$event.event_title}</a></p>
      <h1>Event Gallery - {$year} {$event.event_title}</h1>
    </div>
  </div>
  <div class="clearfix"></div> <!-- do not delete -->
  <form class="forms" name="raza_form" method="post">
    <div class="col-md-3 col-sm-12 hidden-xs">
      <div class="profile-box-static">
        <h3 class="uppercase text-center">Events - {$year}</h3>
      </div>
      <div class="profile-box-static-bottom">
        {if $all_events}
          {foreach $all_events as $evnt}
            <a href="{$server_path}events/{$year}/{$evnt['slug']}/{$evnt['id']}/">» {$evnt['event_title']}<hr></a>
          {/foreach}
        {/if}
      </div>
    </div>

  {if $event}

    <div class="col-md-6 col-sm-12">
      <h2><span class="label label-primary">{$year} {$event.event_title}</span></h2><br>
      <span>
        {$event['text']}
      </span><br>

      {if $videos}
      <br><h2><span class="label label-success">Videos - {$year} {$event.event_title}</span></h2><br>

        <div style="display:none;" class="html5gallery" data-skin="horizontal" data-width="480" data-height="272" data-resizemode="fill">
          {foreach $videos as $video}

            <a href="{$server_path}upload/events/video/{$video['event_video_url']}"><img src="
              {if ($video['event_video_thumb'] != '')}
                {$server_path}upload/events/video/images/{$video['event_video_thumb']}
              {else}
                {$server_path}images/flash-logo.png
              {/if}
              "></a>      

          {/foreach}
        </div>

        {/if}

        {if $audios}
        <br><h2><span class="label label-danger">Audios - {$year} {$event.event_title}</span></h2><br>

        <div id="jquery_jplayer_1" class="jp-jplayer"></div>
        <div id="jp_container_1" class="jp-audio" role="application" aria-label="media player">
          <div class="jp-type-playlist">
            <div class="jp-gui jp-interface">
              <div class="jp-controls">
                <button class="jp-previous" role="button" tabindex="0">previous</button>
                <button class="jp-play" role="button" tabindex="0">play</button>
                <button class="jp-next" role="button" tabindex="0">next</button>
                <button class="jp-stop" role="button" tabindex="0">stop</button>
              </div>
              <div class="jp-progress">
                <div class="jp-seek-bar">
                  <div class="jp-play-bar"></div>
                </div>
              </div>
              <div class="jp-volume-controls">
                <button class="jp-mute" role="button" tabindex="0">mute</button>
                <button class="jp-volume-max" role="button" tabindex="0">max volume</button>
                <div class="jp-volume-bar">
                  <div class="jp-volume-bar-value"></div>
                </div>
              </div>
              <div class="jp-time-holder">
                <div class="jp-current-time" role="timer" aria-label="time">&nbsp;</div>
                <div class="jp-duration" role="timer" aria-label="duration">&nbsp;</div>
              </div>
              <div class="jp-toggles">
                <button class="jp-repeat" role="button" tabindex="0">repeat</button>
                <button class="jp-shuffle" role="button" tabindex="0">shuffle</button>
              </div>
            </div>
            <div class="jp-playlist">
              <ul>
                <li>&nbsp;</li>
              </ul>
            </div>
            <div class="jp-no-solution">
              <span>Update Required</span>
              To play the media you will need to either update your browser to a recent version or update your <a href="http://get.adobe.com/flashplayer/" target="_blank">Flash plugin</a>.
            </div>
          </div>
        </div>

      {/if}

      {if $images}
        <br><h2><span class="label label-info">Images - {$year} {$event.event_title}</span></h2><br>
        <ul class="gallery">
          {foreach $images as $image}
            <li><a href="{$server_path}upload/events/image/{$image['event_image_url']}" rel="prettyPhoto[gallery1]"><img src="{$server_path }upload/events/image/{$image['event_image_url']}" alt=""></a></li>
          {/foreach}
        </ul>
      {/if}

    </div>
  
      <div class="col-md-3 col-sm-12 hidden-xs">
        <div class="profile-box-static">
          <h3 class="uppercase text-center">Registration</h3>
        </div>
        <div class="profile-box-static-bottom">
          <span><strong>Start Date: </strong></span><br><br><span> {date('d, F Y',strtotime($event['start_date']))}</span>
          <hr>
          <span><strong>End Date: </strong></span><br><br><span> {date('d, F Y',strtotime($event['end_date']))}</span>
          <hr>
          <span><strong>Capacity: </strong>{$event['capacity']}</span>
          <hr>
          <span><strong>Venue: </strong>{$event['venue']}</span>
          <hr>
          <span><strong>Contact Person: </strong></span><br><br><span> {$event['contact_person']}</span>
          <hr>
          <span><strong>Email: </strong></span><br><br><span> {$event['email']}</span>
          <hr>
          <span><strong>Mobile: </strong></span><br><br><span> {$event['mobile']}</span>
          <hr><br>
            {if $event_register}
            <div class="text-center">
              <span>You have already Registered for the Event.</span><br><br>
                <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#myModal">Cancel Registration</button>
                
                <a target="_blank" href="{$server_path}print_event_details.php?event_id={$event_id}" class="btn btn-primary pull-right" style="color:#FFF !important;">Print</a>
                <div class="clearfix"></div>
                <input type="hidden" name="event_id" value="{$event['id']}">

                <div id="myModal" class="modal fade" role="dialog">
                  <div class="modal-dialog">

                    <!-- Modal content-->
                    <div class="modal-content">
                      <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Reason For Cancel</h4>
                      </div>
                      <div class="modal-body">
                        <textarea name="reason" class="form-control"></textarea>
                      </div>
                      <div class="modal-footer">
                        <input type="submit" name="cancel_event" class="btn btn-success" value="Submit">
                      </div>
                    </div>

                  </div>
                </div>
            </div>
          
            {else}
              {if ($event['capacity'] > $total_event_register)}
        
            <div class="text-center">
                <input type="submit" class="btn btn-success" name="register" value="Register For Event">
                <input type="hidden" name="event_id" value="{$event['id']}">
            </div>
              {/if} 
            {/if}
        </div>
      </div>
          
    {else}
      
    <div class="col-md-6 col-sm-12">
      {if $all_events}
        {foreach $all_events as $evnt}
          {$evnt_id = $evnt['id']}
          {$img_art = get_first_image_from_event($evnt_id)}

          <a href="{$server_path}events/{$year}/{$evnt['slug']}/{$evnt['id']}/">
            <div class="col-sm-3 col-xs-12">
              <div class="istibsaar-boxes text-center">
                <img src="{$server_path}upload/events/image/{$img_art['event_image_url']}" class="img-responsive img-mts" style="border: 1px solid #09386c">
                <div>
                  <p class="text-center">{$evnt['event_title']}</p>
                  <p>&nbsp;</p>
                  <p class="text-center" style="color:#000;">{$evnt['desc']}</p>
                </div>
              </div>
            </div>
          </a>
        {/foreach}
      {/if}
    </div>
    {/if}
  </form>
</div>

{include file="include/js_block.tpl"}

<script type="text/javascript" src="{$server_path}js/jquery.leanModal.min.js"></script>
<link href="{$server_path}css/popup_style.css" rel="stylesheet" type="text/css" />

<script type="text/javascript">
  $("#modal_trigger").leanModal({
    top: 200, overlay: 0.6, closeButton: ".modal_close"
  });
</script>

<script type="text/javascript" src="{$server_path}html5gallery/html5gallery.js"></script>


<link href="{$server_path}dist/skin/blue.monday/css/jplayer.blue.monday.min.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="{$server_path}dist/jplayer/jquery.jplayer.min.js"></script>
<script type="text/javascript" src="{$server_path}dist/add-on/jplayer.playlist.min.js"></script>



<script type="text/javascript">
  $(document).ready(function () {

    new jPlayerPlaylist({
    jPlayer: "#jquery_jplayer_1",
            cssSelectorAncestor: "#jp_container_1"
    }, [
      {if $audios}
        {foreach $audios as $audio}
            {
            title:"{$audio['event_audio_url']}",
                    mp3: "{$server_path}upload/events/audio/{$audio['event_audio_url']}",
            }
          {if (count($audios) - 1)}
            {','}
          {/if}
        {/foreach}
      {/if}
    ], {
    swfPath: "../../dist/jplayer",
            supplied: "oga, mp3",
            wmode: "window",
            useStateClassSkin: true,
            autoBlur: false,
            smoothPlayBar: true,
            keyEnabled: true
    });
  });
</script>

<script type="text/javascript" charset="utf-8">
  $(document).ready(function () {
    $("area[rel^='prettyPhoto']").prettyPhoto();

    $(".gallery:first a[rel^='prettyPhoto']").prettyPhoto({
      animation_speed: 'normal', theme: 'light_square', slideshow: 5000, autoplay_slideshow: true
    });
    $(".gallery:gt(0) a[rel^='prettyPhoto']").prettyPhoto({
      animation_speed: 'normal', slideshow: 10000, hideflash: true
    });

    $("#custom_content a[rel^='prettyPhoto']:first").prettyPhoto({
      custom_markup: '<div id="map_canvas" style="width:260px; height:265px"></div>',
      changepicturecallback: function () {
        initialize();
      }
    });

    $("#custom_content a[rel^='prettyPhoto']:last").prettyPhoto({
      custom_markup: '<div id="bsap_1259344" class="bsarocks bsap_d49a0984d0f377271ccbf01a33f2b6d6"></div><div id="bsap_1237859" class="bsarocks bsap_d49a0984d0f377271ccbf01a33f2b6d6" style="height:260px"></div><div id="bsap_1251710" class="bsarocks bsap_d49a0984d0f377271ccbf01a33f2b6d6"></div>',
      changepicturecallback: function () {
        _bsap.exec();
      }
    });
  });
</script>

<style>
  .profile-box-static-bottom span{
    text-align: center !important;
    padding: 10px;
  }
  .profile-box-static h3{
    display: block;
    font-family: "Lato",sans-serif;
    font-size: 18px;
    line-height: 150%;
    padding: 0px 0px 20px;
    text-align: left;
    color: #FFF !important;
    text-transform: uppercase;
    font-weight: 600;
  }
  .actv{
    background-color:#cf4914;
    display:block;
    font-family: 'Lato', sans-serif;
    font-size:14px;
    padding:10px;
    text-align:center;
    color:white;
    text-transform:uppercase;
    font-weight:600;

  }

  .ntactv{
    display:block;
    font-family: 'Lato', sans-serif;
    font-size:14px;
    text-align:center;
    color:white;
    text-transform:uppercase;
    font-weight:600;

  }

  .profile-box1 a{
    color: white;
    padding: 5px;
  }

  .gallery {
    clear: both;
    display: table;
    list-style: none;
    padding: 0;
    margin: 0;
    margin-bottom: 20px;
  }
  .gallery li {
    float: left;
    margin-bottom: 7px;
    margin-right: 7px;
    height: 100px;
    width: 100px;
    overflow: hidden;
    text-align: center;
    position: relative;
    padding: inherit;
  }
  .gallery li:last-child {
    margin-right: 0;
  }
  .gallery li a {
    position: relative;
    left: 100%;
    margin-left: -200%;
    position: relative;
  }
  .gallery li a:after {
    text-shadow: none;
    -webkit-font-smoothing: antialiased;
    font-family: 'fontawesome';
    speak: none;
    font-weight: normal;
    font-variant: normal;
    line-height: 1;
    text-transform: none;
    -moz-transition: 0.6s;
    -o-transition: 0.6s;
    -webkit-transition: 0.6s;
    transition: 0.6s;
    filter: progid:DXImageTransform.Microsoft.Alpha(Opacity=0);
    opacity: 0;
    -moz-border-radius: 50%;
    -webkit-border-radius: 50%;
    border-radius: 50%;
    color: #fff;
    content: "\f06e";
    display: inline-block;
    font-size: 16px;
    position: absolute;
    width: 30px;
    height: 30px;
    top: 0;
    left: 0;
    right: 0;
    bottom: 0;
    margin: auto;
    background-color: #252525;
    padding-top: 7px;
    margin-top: -7px;
  }
  .gallery li a:hover img {
    -moz-transform: scale(1.08, 1.08);
    -ms-transform: scale(1.08, 1.08);
    -webkit-transform: scale(1.08, 1.08);
    transform: scale(1.08, 1.08);
  }
  .gallery li a img {
    -moz-transition: 0.3s;
    -o-transition: 0.3s;
    -webkit-transition: 0.3s;
    transition: 0.3s;
    height: 100%;
    width: auto;
  }
</style>
{include file="include/footer.tpl"}