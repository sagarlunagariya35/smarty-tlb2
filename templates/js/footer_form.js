/********************************************************************************
* This script is brought to you by Vasplus Programming Blog to whom all copyrights are reserved.
* Website: www.vasplus.info
* Email: info@vasplus.info
* Do not remove this information from the top of this code.
*********************************************************************************/

$(document).ready(function()
{
	$('#vpv_tooltip_image').mouseenter(function(){
		/*$(this).fadeOut('fast');*/
	});
	$("#vpb_message_body").Watermark("Enter Text");
});

function vpb_leave_a_message_show() {
	$('#vasplus_programming_blog_bottom').slideUp('fast');
	$('#vpv_tooltip_image').fadeOut('fast');
	$('.vasplus_programming_blog_bottom').slideDown('slow');
}

function vpb_leave_a_message_hide() {
	$("#vpb_message_body").val('').animate({
			"height": "80px"
	}, "fast" );
	$("#vpb_message_body").Watermark("Enter Text");
	$('#vasplus_programming_blog_mailer_status').html('').hide();
	$('.vasplus_programming_blog_bottom').slideUp('fast');
	$('#vasplus_programming_blog_bottom').slideDown('slow');
	$('#vpv_tooltip_image').fadeIn('fast');
}

//Send Mails
function Contact_Form_Submission_By_Vasplus_Programming_Blog(str) 
{
	var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
	var vpb_ques = $("#vpb_ques").val();
	var vpb_message_body = $("#vpb_message_body").val();
	
	if(vpb_ques == "")
	{
		$('#vasplus_programming_blog_mailer_status').show().css("background-color","#FFFFB7").html('Please select your Question in the specified field to proceed. Thanks.');
		 $("#vpb_ques").focus();
	}
	else if(vpb_message_body == "" || vpb_message_body == "Enter Text")
	{
		$('#vasplus_programming_blog_mailer_status').show().css("background-color","#FFFFB7").html('Please enter your message content in the required field to go. Thanks.');
		$("#vpb_message_body").focus();
	}
	else 
	{
		var dataString = 'vpb_ques=' + vpb_ques + '&vpb_message_body=' + vpb_message_body;
        
		$.ajax({
			type: "POST",
			url: str+"js/footer_form.php",
			data: dataString,
			cache: false,
			beforeSend: function()
			{
				$('#vasplus_programming_blog_mailer_status').show().css("background-color","white").html('<font style="font-family:Verdana, Geneva, sans-serif; font-size:12px;color:gray;">Please wait</font> <img style="" src="../img/loading.gif" align="absmiddle" alt="Loading" />');
			},
			success: function(response) 
			{
				$("#vpb_message_body").val('').animate({
						"height": "80px"
				}, "fast" );
				$("#vpb_ques").val('');
				$('#vasplus_programming_blog_mailer_status').show().css("background-color","#FFFFB7").html(response);
			}
		});
		return false;
	}
}