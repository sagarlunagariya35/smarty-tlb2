<?php
require_once '../session.php';

$f_ques = $_POST['vpb_ques'];
$f_ans = $_POST['vpb_message_body'];
$its = $_SESSION[USER_ITS];

$queries = insert_queries($its, $f_ques, $f_ans);


if ($queries) {
  echo '<div class="vasplus_blog_success_status_message">Your message has been sent successfully. We will get back to you as soon as possible.<br><br>Thanks.</div>';
} else {
  echo '<div class="vasplus_blog_success_status_message">Your message could not be sent at the moment due to connection problem. <br>Please try again or contact the site admin to report this error if the problem persist. Thanks.</div>';
}
?>