/*****page loader script start*****/

$(window).load(function () {
    "use strict";
	//$("#status").fadeOut();
	$("#preloader").delay(550).toggleClass("animated zoomOut").fadeOut(1500);

	
});
/********page loader script end********/


$(document).ready(function() {





// Responsive Menu

$('.top-menu').slicknav({
            label: 'Menu',
            prependTo: 'nav.nav'
});

$('.app-menu').slicknav({
            label: 'Dashboard',
            prependTo: 'nav.appmenu'
});


// Form Submit

//  $(function(){$('.date-pick').datePicker({autoFocusNextInput: true,clickInput:true});});
		
//$('#dp-popup').dpSetPosition($.dpConst.POS_TOP, $.dpConst.POS_RIGHT);


// Banners 

$('#banners').nivoSlider({
    effect: 'fade',               // Specify sets like: 'fold,fade,sliceDown'
    slices: 15,                     // For slice animations
    boxCols: 18,                     // For box animations
    boxRows: 9,                     // For box animations
    animSpeed: 750,                 // Slide transition speed
    pauseTime: 7000,                // How long each slide will show
    startSlide: 0,                  // Set starting Slide (0 index)
    directionNav: false,             // Next & Prev navigation
    controlNav: false,               // 1,2,3... navigation
    controlNavThumbs: false,        // Use thumbnails for Control Nav
    pauseOnHover: false,             // Stop animation while hovering
    prevText: '<span class="pre-btn"></span>',               // Prev directionNav text
    nextText: '<span class="nxt-btn"></span>',               // Next directionNav text
});

});