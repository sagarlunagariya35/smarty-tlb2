<div class="container white-bg">
  <div class="col-md-12 col-sm-12">

    <div class="page-title">
      <p style="margin-top:5px;">
        <a href="#" class="active">My Araz for education</a></p>
      <h1>My Araz for education<span class="alfatemi-text">ماري تعليم واسطسس عرض</span></h1>
    </div>
  </div>
  <p>&nbsp;</p>
  <div class="clearfix"></div> <!-- do not delete -->
  <div class="col-md-12 col-sm-12">
    <form class="forms" action="{$server_path}marahil/" method="post">

        <div class="col-md-3 col-xs-12 mb20">
          <input type="radio" name="already_araz_done" id="already_araz_not_done" class="css-radiobutton1" />
          <label for="already_araz_not_done" class="css-label1 css-label3 radGroup1">Araz for New Raza</label><i class="fa fa-question-circle help-icon" data-toggle="popover" data-content="I want to do araz for education in Hadrat aaliyah."></i>
        </div>
        <div class="col-md-3 col-xs-12">
          <input type="radio" name="already_araz_done" id="already_araz_done" class="css-radiobutton1" value="1" />
          <label for="already_araz_done" class="css-label1 css-label3 radGroup1">Register existing Raza</label><i class="fa fa-question-circle help-icon" data-toggle="popover" data-content="I have already obtained raza from hadrat aaliyah through sources other than this website and would like to register my raza on this portal."></i>
        </div>
      <p>&nbsp;</p>
      <div class="clearfix"></div> <!-- do not delete -->
      {if ($age > 17)}
      <!--<div class="arz-list">If you want to do araz for your child, Please log out and log in again with your child's ITS ID.</div>-->
      {/if}
      
        {if ($age < 17)}
          {$marhala_text = 'below_5'}
        {else}
          {$marhala_text = 'above_5'}
        {/if}

      <ul class="arz-list">
        <a href="#" class="marhala-show-hide pull-right">Show</a>
        <div class="clearfix"><br><br></div>
        <li class="col-md-6 col-sm-12 below_5">
          <button type="submit" {*if (in_array('1', $ary_done_araz))} disabled {/if*} class="col-xs-12" name="submit" value="1">Pre Primary <span>Marhala 1</span></button> 
        </li>
        <li class="col-md-6 col-sm-12 below_5">
          <button type="submit" {*if (in_array('2', $ary_done_araz))} disabled {/if*} class="col-xs-12" name="submit" value="2">Primary <span>Marhala 2</span></button>
        </li>
        <li class="col-md-6 col-sm-12 below_5">
          <button type="submit" {*if (in_array('3', $ary_done_araz))} disabled {/if*} class="col-xs-12" name="submit" value="3">Std 5<sup>th</sup>  - 7<sup>th</sup><span>Marhala 3</span></button>
        </li>
        <li class="col-md-6 col-sm-12 below_5">
          <button type="submit" {*if (in_array('4', $ary_done_araz))} disabled {/if*} class="col-xs-12" name="submit" value="4">Std 8<sup>th</sup>  - 10<sup>th</sup> <span>Marhala 4</span></button>
        </li>
        <li class="col-md-6 col-sm-12 above_5">
          <button type="submit" {*if (in_array('5', $ary_done_araz))} disabled {/if*} class="col-xs-12" name="submit" value="5">Std 11<sup>th</sup>  - 12<sup>th</sup> <span>Marhala 5</span></button>
        </li>
        <li class="col-md-6 col-sm-12 above_5">
          <button type="submit" class="col-xs-12" name="submit" value="6">Graduation <span>Marhala 6</span></button>
        </li>
        <li class="col-md-12 col-sm-12 above_5">
          <button type="submit" class="col-md-6 col-md-offset-3 col-xs-12" name="submit" value="7">Post Grad. <span>Marhala 7</span></button>
        </li>
        <li class="col-md-12 col-sm-12 above_5">
          <button type="submit" class="col-md-6 col-md-offset-3 col-xs-12" name="submit" value="8">Diploma / other courses<span>Others</span></button>
        </li>
        <span class="page-steps">Step : 1 out of 5</span>
      </ul>
      <div class="clearfix"></div>
    </form>
  </div>
  <div class="col-md-12 spacer"></div>
</div>

{include file="include/js_block.tpl"}

<script type="text/javascript">
  $(document).ready(function () {

    $('input[type="radio"]').click(function () {
      $('.arz-list').show(600);
      $('.spacer').hide();
    });

    $('.arz-list').hide();
    
    $('.marhala-show-hide').click(function () {
      {if ($age < 17)}
         $(".above_5").show(600);
      {else}
         $(".below_5").show(600);
      {/if}
    });
       
    {if ($marhala_text == 'below_5')}
        $(".above_5").hide();
    {else}
        $(".below_5").hide();
    {/if}
  });
</script>
{include file="include/footer.tpl"}
