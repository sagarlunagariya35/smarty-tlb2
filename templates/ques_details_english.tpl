<style>
  .skip{
    display:block;
    max-width:300px;
    -webkit-transition: 0.4s;
    -moz-transition: 0.4s;
    -o-transition: 0.4s;
    transition: 0.4s;
    border: none;
    background-color: #155485;
    height: auto;
    line-height:20px;
    margin:0px auto 20px;
    padding:10px;
    outline: none;
    color:#FFF;
    text-transform:uppercase;
    text-align:center;
    font-weight:bold;
    font-size:18px;
    float:right;
    text-decoration: underline none;
  }
  
  .set{
    background-color: #549BC7 !important;
  }
  
  .skip:hover{
    background-color: #549BC7;
  }
  
  #load {
    color: #000 !important;
    margin: 20px auto 0;
    float: right;
    text-align: center;
  }
</style>

<div class="container white-bg">
  <div class="col-md-12 col-sm-12">

    <div class="page-title">
      <p style="margin-top:5px;">
        <a href="{$server_path}marahil/">My Araz for education</a> 
        {if (in_array($marhala_id, $marhala_school_array))}
          / <a href="{$server_path}marhala-{$marhala_id}/{get_marhala_slug($marhala_id)}/school">{$panel_heading} ( School )</a> 
        {else}
          / <a href="{$server_path}marhala-{$marhala_id}/{get_marhala_slug($marhala_id)}/select-institute/">{$panel_heading} (Select Institute)</a>
        {/if}
        / <a href="#" class="active">{$panel_heading}</a></p>

      <h1>{$panel_heading}<span class="alfatemi-text">{$page_heading_ar}</span></h1>
    </div>
  </div>
  {if (isset($success_message) OR isset($error_message))}
    <div class="row">
      {include file="include/message.tpl"}
    </div>
  {/if}
  <div class="clearfix"></div> <!-- do not delete -->  
  
  <div class="col-md-12 col-sm-12">
    <div class="page">
      <span class="block vmgn20"></span>
      <div class="clearfix"></div> <!-- do not delete -->
      <h4>If you would like to submit your azaim (resolutions) before proceeding for raza araz, complete this questionnaire.</h4>
      
      <div class="col-md-3 pull-right hidden-xs"></div>
      <div class="col-md-3 col-xs-12 pull-right text-center"><br>
        <button type="button" name="for_english" id="for_arabic" class="btn btn-block skip lsd" value="1" dir="rtl" onClick="MM_goToURL('parent','{$server_path}marhala-{$marhala_id}/{get_marhala_slug($marhala_id)}/ques/');return document.MM_returnValue">لسان الدعوة واسطسس يظظاطط click كروو</button>
      </div>
      
      <div class="col-md-3 col-xs-12 pull-right text-center"><br>
        <button type="button" name="for_english" id="for_english" class="btn btn-block skip set lsd" {if ($page == 'ques_eng')} checked {/if}>For English Click Here</button>
      </div><br><br>
      <div class="clearfix"></div>
      
      <form class="forms1 white" action="{$server_path}marhala-{$marhala_id}/{get_marhala_slug($marhala_id)}/ques_english/" method="post" onsubmit="return(validate());">
                
        <div class="clearfix"></div> <!-- do not delete -->
        
        {assign var="i" value=0}
          {if $ques_preffered}
            {foreach $ques_preffered as $q}
        
              {if (isset($araz_data[$i]['answer']))}
                {if ($araz_data[$i]['answer'] == '1')}
                  {$check1 = 'checked'}
                {elseif ($araz_data[$i]['answer'] == '2')}
                  $check2 = 'checked';
                {elseif ($araz_data[$i]['answer'] == '3')}
                  $check3 = 'checked';
                {elseif ($araz_data[$i]['answer'] == '4')}
                  $check4 = 'checked';
                {/if}
              {/if}
              {assign var="i" value=$i+1}
        
        <div class="blue-box1"> <!-- Add class rtl -->
          <h3><span class="inline-block lh3" style="text-transform:none;">{$i}. {$q['question']}</span>
          </h3>
        </div>
            <div class="clearfix"></div> <!-- do not delete -->
            <div class="col-md-3 col-sm-12">
              <div class="radiobuttons-holder1"> <!-- Add Class .rtl -->
                <input type="radio" name="q_{$q['id']}" value="1" id="radio1{$q['id']}" class="css-radiobutton1" {$check1} />
                <label for="radio1{$q['id']}" style="color: #000;" class="css-label1 radGroup3 lsd">I will, by all means.</label>
              </div>
            </div>
            <div class="clearfix"></div>
         {/foreach}
      {/if}
            
        <p style="color:#444444;">We recommend you to complete the remaining questionnaire pertaining your educational journey. <a data-toggle="collapse" href="#collapseExample" aria-expanded="false" aria-controls="collapseExample" style="color:#155485;">Show / Hide</a></p><br>

        <div class="collapse" id="collapseExample">
          {if $ques}
      		{foreach $ques as $q}

            {if (isset($araz_data[$i]['answer']))}
               {if ($araz_data[$i]['answer'] == '1')}
                 {$check1 = 'checked'}
               {elseif ($araz_data[$i]['answer'] == '2')}
                 $check2 = 'checked';
               {elseif ($araz_data[$i]['answer'] == '3')}
                 $check3 = 'checked';
               {elseif ($araz_data[$i]['answer'] == '4')}
                 $check4 = 'checked';
                {/if}
              {/if}
              {assign var="i" value=$i+1}

              <div class="blue-box1"> <!-- Add class rtl -->
                <h3><span class="inline-block lh3" style="text-transform:none;">{$i}. {$q['question']}</span>
                </h3>
              </div>
              <div class="clearfix"></div> <!-- do not delete -->
              <div class="col-md-3 col-sm-12">
                <div class="radiobuttons-holder1"> <!-- Add Class .rtl -->
                  <input type="radio" name="q_{$q['id']}" value="1" id="radio1{$q['id']}" class="css-radiobutton1" {$check1} />
                  <label for="radio1{$q['id']}" style="color: #000;" class="css-label1 radGroup3 lsd">I will, by all means.</label>
                </div>
              </div>
              <div class="clearfix"></div>
          {/foreach}
  		{/if}
        </div>
              
        {if (!$ques_preffered && !$ques)}
            <div class="blue-box1"> <!-- Add class rtl -->
          <h3><span class="inline-block bigger1 lh1 w500 left-mgn20">1</span>
            <span class="inline-block lh3">Will your Child be able to attend full waaz of all nine days of Asharah Mubarakah?</span>
          </h3>
        </div>
            <div class="clearfix"></div> <!-- do not delete -->
            <div class="col-md-3 col-sm-12">
              <div class="radiobuttons-holder1"> <!-- Add Class .rtl -->
                <input type="radio" name="q_1" value="1" id="radio11" class="css-radiobutton1" />
                <label for="radio11" style="color: #000;" class="css-label1 radGroup3 lsd">I will, by all means.</label>
              </div>
            </div>
            <div class="clearfix"></div>
        {/if}
        
         <!--div class="blue-box1">
          <h3 class="bordered">
            <span class="lsd">
                <div class="show lsd font18 text-center"><?php echo $araz_notes[0]['note']; ?></div>
            </span>
          </h3>
          <div class="clearfix"></div> <!-- do not delete -->
        <!--/div-->
        
        <div class="clearfix"></div> <!-- do not delete -->
        <input type="submit" class="submit1 proceed col-md-3 col-xs-12" name="submit" value="Proceed with Araz" onclick="return ray.ajax()"/>
        <div id="load" style="display: none;"><i class="fa fa-spinner fa-spin fa-3x"></i></div>
        <input type="submit" class="submit1 col-md-3 col-xs-12" name="save_araz"  value="Save Araz" style="margin-right: 5px;" />
        <a class="pull-left col-md-3 col-xs-12 start-again" onclick="start_again();">Start Over Again</a>
        <span class="page-steps">Step : 4 out of 5</span>
        <div class="clearfix"></div> <!-- do not delete -->
      </form>
      <div class="clearfix"></div> <!-- do not delete -->
    </div>
  </div>
</div>

{include file="include/js_block.tpl"}

<style>

  .morectnt span {
    display: none;
  }
  .showmoretxt {
    text-decoration: none;
  }
</style>

<script type="text/javascript">
// Form validation code will come here.
  //function validate() {
    //{foreach $ques as $q}
		//      if ($('input[name=q_<?php echo $q['id']; ?>]:checked').length <= 0)
		//      {
		//        alert("Please Select any Answer!");
		//        return false;
		//      }
	//{/foreach}
  //}
//-->
  var ray = {
    ajax: function(st)
    {
      this.show('load');
      $('.proceed').hide();
    },
    show: function(el)
    {
      this.getID(el).style.display = '';
    },
    getID: function(el)
    {
      return document.getElementById(el);
    }
  }
</script>

<script src="http://code.jquery.com/jquery-1.8.2.js"></script>
<script>
  $(function() {
    var showTotalChar = 200, showChar = "Read More", hideChar = "Hide";
    $('.show').each(function() {
      var content = $(this).text();
      if (content.length > showTotalChar) {
        var con = content.substr(0, showTotalChar);
        var hcon = content.substr(showTotalChar, content.length - showTotalChar);
        var txt = con + '<span class="dots">...</span><span class="morectnt"><span>' + hcon + '</span>&nbsp;&nbsp;<a href="" class="showmoretxt">' + showChar + '</a></span>';
        $(this).html(txt);
      }
    });
    $(".showmoretxt").click(function() {
      if ($(this).hasClass("sample")) {
        $(this).removeClass("sample");
        $(this).text(showChar);
      } else {
        $(this).addClass("sample");
        $(this).text(hideChar);
      }
      $(this).parent().prev().toggle();
      $(this).prev().toggle();
      return false;
    });
  });
  
  function MM_goToURL() { //v3.0
    var i, args=MM_goToURL.arguments; document.MM_returnValue = false;
    for (i=0; i<(args.length-1); i+=2) eval(args[i]+".location='"+args[i+1]+"'");
  }
  
  function start_again() {
    var ask = window.confirm("Are you sure you want to start again?");
    if (ask) {
        document.location.href = "{$server_path}start_over_again.php";
    }
  }
</script>
{include file="include/footer.tpl"}