{include file="include/js_block.tpl"}
<link href="{$server_path}assets/datatables/dataTables.bootstrap.css" rel="stylesheet" type="text/css" />
<script src="{$server_path}assets/datatables/jquery.dataTables.min.js" type="text/javascript"></script>
<script src="{$server_path}assets/datatables/dataTables.bootstrap.min.js" type="text/javascript"></script>

<div class="container white-bg">
  <div class="col-md-12 col-sm-12">
    <div class="page-title">
      <p style="margin-top:5px;">
        <a href="#" class="active">My Tasmee Report</a></p>
        <h1>My Tasmee Report</h1>
    </div>
  </div>
  {if (isset($success_message) OR isset($error_message))}
    <div class="row">
      {include file="include/message.tpl"}
    </div>
  {/if}
  <div class="clearfix"></div> <!-- do not delete -->
  <div class="col-md-12 col-sm-12">
    <form action="" method="post" name="tasmee_form">
      {include file="include/qrn_message.tpl"}
      <div class="body-container white-bg">
        <div class="hifz-tasmee-details-wrapper">
          <div class="htd-section">
            <h2>How many aayaat I do tasmee daily ({date('jS M Y')})</h2>
            <div class="row">
              <div class="col-xs-12 col-md-12">
                <div class="col-xs-12 col-md-9">
                  {if $tasmee_records}
                    <div id="columnchart_values"></div>
                  {else}
                    No Data Available.
                  {/if}
                  <div class="clearfix"></div>
                  <div id="donutchart" style="height: 400px;"></div>
                </div>
                <div class="col-xs-12 col-md-3">
                  <div class="text-center">
                    <p>Current Sanad</p>
                    <p><strong>{$mumin_data[0]['mumin_quran_sanad']}</strong></p>

                    <div class="clearfix"><br></div>

                    <p>Currently at</p>
                    <p>Surah: <strong>{$qrn_tasmee->get_surat_name($mumin_data[0]['last_tasmee_surat'])}</strong></p>
                    <p>Aayaah: <strong>{$mumin_data[0]['last_tasmee_ayat']}</strong></p>

                    <p>Your Sadeeq Name</p>
                    <p><strong>{$mumin_data[0]['muhafiz_full_name']}</strong><p>
                    <p>Email: {$user_logedin->get_email()}<p>
                    <p>Mobile: {$user_logedin->get_mobile()}<p>
                    <p>Whatsapp: {$user_logedin->get_whatsapp()}<p>
                    <p>Skype: {$user_logedin->get_viber()}<p>
                      
                    <p>Your Schedule</p>
                    <p>Days: {$schedule_record['days']}</p>
                    <p>Hour: {$schedule_record['hour']}</p>
                    <p>Time: {$schedule_record['time']}</p>
                  </div>
                </div>
              </div>
            </div>
            <div class="clearfix"><br></div>
            <h2><a href="#" id="tlb_mumin">Show Tasmee Details</a><a href="{$server_path}qrn_edit_last_tasmee_entry.php" class="pull-right">Edit my last Tasmee Entry</a></h2>
            <div class="row" id="show_mumin_table"></div>
          </div>
          {include file="include/qrn_links.tpl"}
        </div>
      </div>
    </form>
  </div>
</div>

<script type="text/javascript" src="{$server_path}js/loader.js"></script>
<script type="text/javascript">
  google.charts.load("current", {
    packages:['corechart']
  });
  google.charts.setOnLoadCallback(drawChart);
  function drawChart() {
    var data = google.visualization.arrayToDataTable([
      ["Date", "Total Aayaat"]
        {if $tasmee_records}
          {','}
          {foreach $tasmee_records as $data}
            {[{date('d, M Y',  strtotime($data['timestamp']))}, {$data['sum_total_ayat']}]}
            {if (count($tasmee_records) - 1)}
              {','}
            {/if}
          {/foreach}
        {/if}
      ]);

    var options = {
      colors: ['#337AB7'],
      pointSize: 10,
      pointShape: {
        type: 'point', rotation: 180
      },
      hAxis: {
        title: 'Date'
      },
      vAxis: {
        title: 'No. of Aayaat recited'
      }
    };

    var chart = new google.visualization.AreaChart(document.getElementById("columnchart_values"));
    chart.draw(data, options);
  }

  $(window).resize(function () {
    drawChart();
  });

  var tasmee_ayat = {$total_ayat_count};
  var remain_ayat = 6236 - tasmee_ayat;
  google.charts.setOnLoadCallback(drawPieChart);
  function drawPieChart() {
    var data = google.visualization.arrayToDataTable([
      ['Remaining Aayaat', 'Tasmee Aayaat'],
      ['Remaining Aayaat {(6236 - $total_ayat_count)}', remain_ayat],
      ['Tasmee Aayaat {$total_ayat_count}', tasmee_ayat]
    ]);

    var options = {
      title: 'Aayaat Completed',
      pieSliceText: 'value',
      pieHole: 0.4,
    };

    var chart = new google.visualization.PieChart(document.getElementById('donutchart'));
    chart.draw(data, options);
  }
  
  $('#tlb_mumin').on("click", function(e){
    e.preventDefault();
    
    $.ajax({
      type: "POST",
      url: "ajax.php",
      data: 'query=get_mumin_table&mumin_table=mumin_table',
      cache: false,
      success: function (response) {
        $('.show_mumin_table').show(600);
        
        if (response != '') {
          $('#show_mumin_table').html(response);
        } 
      }
    });
  });

</script>

<style>
  p {
    padding: 5px;
  }
</style>
{include file="include/footer.tpl"}