<div class="container white-bg">
  <div class="col-md-12 col-sm-12">

    <div class="page-title">
      <p style="margin-top:5px;">
        <a href="#" class="active">Istibsaar {$year} H</a></p>
      <h1>Istibsaar {$year} H <span class="alfatemi-text">الاستبصار</span></h1>
    </div>
  </div>
  <div class="clearfix"></div> <!-- do not delete -->
  <form class="forms1 white" name="raza_form" method="post">
    <div class="col-md-3 col-sm-12 hidden-xs">
      <div class="profile-box-static">
        <h3 class="uppercase text-center">Years</h3>
      </div>
      <div class="profile-box-static-bottom">
        <a href="{$server_path}istibsaar/1436/">» 1436 H<hr></a>
        <a href="{$server_path}istibsaar/1437/">» 1437 H<hr></a>
      </div>
    </div>

    <div class="col-md-8 col-sm-12">      
        {if $miqaat_istibsaars}
          {assign var="i" value=1}
          {foreach $miqaat_istibsaars as $mi}
            <div class="col-md-4">
              <a href="{$server_path}miqaat_content.php?miqaat_id={$mi.id}">
              <div class="istibsaar-boxes text-center">
                <img src="{$server_path}upload/miqaat_upload/logo/{$mi.logo}" class="img-responsive img-mts">
                <div class="istibsaar-boxes-bottom">
                  <p class="text-center">{$mi.miqaat_title}</p>
                  <div class="clearfix"></div>
                  <p></p>
                  <p class="text-center black_font" style="color:#000;">{$mi.desc}</p>
                </div>
              </div>
              </a>
              <div class="clearfix"></div>
            </div>
          {if ($i++ % 3 == 0)} 
            <div class="clearfix"><br></div><div class="clearfix"><br></div>
          {/if}
          {/foreach}
        {/if}
    </div>
  </form>
</div>

{include file="include/js_block.tpl"}

<style>
  .istibsaar-boxes-bottom:hover{
    background-color: #549BC7;
  }
</style>
{include file="include/footer.tpl"}