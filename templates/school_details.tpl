<div class="container white-bg">
  <div class="col-md-12 col-sm-12">

    <div class="page-title">
      <p style="margin-top:5px;">
        <a href="{$server_path}marahil/">My Araz for education</a> / 
        <a href="#" class="active">{$panel_heading}</a>
      </p>
      <h1>{$panel_heading}<span class="alfatemi-text">{$panel_heading_ar}</span></h1>
    </div>
  </div>
  {if (isset($success_message) OR isset($error_message))}
    <div class="row">
      {include file="include/message.tpl"}
    </div>
  {/if}
  <div class="clearfix"></div> <!-- do not delete -->
  <div class="col-md-12 col-sm-12">
    <div class="page">

      <h6><span class="alfatemi-text" dir="rtl">{$kalemaat_nooraniyah} - من الكلمات النورانية</span></h6>

      <h4>Complete this form to submit your araz for {$std_heading} Education. <span class="alfatemi-text">تماري تعليم ني عرض واسطے اْ فارم بهري عرض كرو</span></h4>

      <form name="school_form" class="forms1" action="{$server_path}marhala-{$group_id}/{get_marhala_slug($group_id)}/school/" method="post" onsubmit="return(validate());">

        <h3>School / Grade<span class="alfatemi-text">اسكول</span></h3>

        <div class="clearfix"></div> <!-- do not delete -->
        <div class="col-md-6 col-sm-12 shift">
          <lable class="white required-star"><b>Is your school:</b></lable>
          <div  class="shift10 lable-mrgn">
            <select name="filter_school" class="required select-input">
              <option value="">Is your school</option>
              {foreach $filter_school_array as $scl}
                <option value="{$scl}" {if ($school_filter == $scl)} selected {/if}>{$scl}</option>
              {/foreach}
            </select>
          </div>
        </div>
        <div class="col-md-6 col-sm-12 shift">
          <lable class="white required-star"><b>Standard:</b></lable>
          <div  class="shift10 lable-mrgn">
            <select name="school_standard" class="required select-input">
              <option value="">Standard</option>
              {foreach $ary_standard as $std}
              	{assign var="school_standard" value=""}
                {if ($msb_student_info == '1')}
                  {assign var="school_standard" value=$madrasah_darajah}
                {else}
                  {assign var="school_standard" value=$school_standard}
                {/if}
                <option value="{$std}" {if ($school_standard == $std)} selected {/if}>{$std}</option>
              {/foreach}
            </select>
          </div>
        </div>

        <div class = "col-md-6 col-sm-12 shift">
          <lable class="white required-star"><b>Country:</b></lable>
          <div class="shift10 lable-mrgn">
            <select name="madrasah_country" onChange="showcity(this.value)" class="required select-input">
              <option value="">Country</option>
              {if ($countries != '')}
                {foreach $countries as $cntry}
                  <option value="{$cntry['name']}" {if ($cntry['name'] == $scl_country)} selected {/if} >{$cntry['name']}</option>
                {/foreach}
              {/if}
            </select>
          </div>
        </div>


        <div class="col-md-6 col-sm-12 shift">
          <lable class="white required-star"><b>City:</b></lable>
          <div  class="shift10 lable-mrgn" id="your_city">
            <select name="madrasah_city" id="select-city" class="required select-input">
              <option value="">City - Where your school is located</option>
              {if ($ary_cities)}
                {assign var="scl_city" value=""}
                {foreach $ary_cities as $city}
                  {if ($school_city != '')}
                    {assign var="scl_city" value=$school_city}
                  {elseif ($my_araz['school_city'] != '')}
                    {assign var="scl_city" value=$my_araz['school_city']}
                  {else}
                    {assign var="scl_city" value=$user->get_city()}
                  {/if}
                  <option value="{$city['city']}" {if ($city['city'] == $scl_city)} selected {/if} >{$city['city']}</option>
                {/foreach}
              {/if}
            </select>
          </div>
        </div>

        <div class="col-md-6 col-sm-12 shift">
          <lable class="white required-star"><b>School Name:</b></lable>
          <div  class="shift10 lable-mrgn">
            <select name="school_name" id="school_name" class="required school_name">
              <option value="">Please write the School name with correct spelling</option>
              {*if $my_araz}
                <option value="{$my_araz['school_name']}">{$my_araz['school_name']}</option>
              {/if*} 
            </select>
          </div>
        </div>
        <div class="col-md-6 col-sm-12 shift">
          <lable class="white required-star"><b>Pincode:</b></lable>
          <div  class="shift10 lable-mrgn">
            <input type="text" name="school_pincode" placeholder="Pincode/Zipcode" value="{$school_pincode}">
          </div>
        </div>
        <div class="clearfix"></div> <!-- do not delete -->
        <input type="submit" class="submit1" name="submit" value="next" onclick="return ray.ajax()"/>
        <div id="load" style="display: none;"><i class="fa fa-spinner fa-spin fa-3x"></i></div>
        <input type="submit" name="save_araz" class="submit1" value="Save Araz" style="margin-right: 5px;" />
        <a class="pull-left col-md-3 col-xs-12 start-again" onclick="start_again();">Start Over Again</a>
        <span style="color:#FFF;" class="page-steps">Step : 3 out of 5</span>
        <div class="clearfix"></div> <!-- do not delete -->
      </form>
      <div class="clearfix"></div> <!-- do not delete -->
    </div>
  </div>
</div>
            
{include file="include/js_block.tpl"}

<script type="text/javascript">
// Form validation code will come here.
  function validate()
  {

    if (document.school_form.filter_school.value == "")
    {
      alert("Please Select Your School!");
      document.school_form.filter_school.focus();
      return false;
    }
    if (document.school_form.school_standard.value == "")
    {
      alert("Please Select Your Standard!");
      document.school_form.school_standard.focus();
      return false;
    }
    if (document.school_form.madrasah_country.value == "")
    {
      alert("Please Select Country!");
      document.school_form.madrasah_country.focus();
      return false;
    }
    if (document.school_form.madrasah_city.value == "")
    {
      alert("Please Enter City where your School is Located!");
      document.school_form.madrasah_city.focus();
      return false;
    }
    if (document.school_form.school_name.value == "")
    {
      alert("Please Enter School Name!");
      document.school_form.school_name.focus();
      return false;
    }
    return(true);
  }
//-->
</script>
<script type="text/javascript">
  function formatRepo(repo) {
    if (repo.loading)
      return repo.text;

    var markup = '<div class="clearfix">' +
            '<div class="col-sm-12">' + repo.school_name + '</div>' +
            '</div>';

    return markup;
  }

  function formatRepoSelection(repo) {
    return repo.school_name;
  }

  $(".school_name").select2({
    ajax: {
      url: "{$server_path}ajax_search.php?cmd=get_school_list",
      dataType: 'json',
      delay: 250,
      data: function(params) {
        return {
          q: params.term, // search term
          page: params.page
        };
      },
      processResults: function (data, params) {
        // parse the results into the format expected by Select2
        // since we are using custom formatting functions we do not need to
        // alter the remote JSON data, except to indicate that infinite
        // scrolling can be used
        params.page = params.page || 1;

        return {
          results: data.items,
          pagination: {
            more: (params.page * 30) < data.total_count
          }
        };
      },
      cache: true
    },
    escapeMarkup: function(markup) {
      return markup;
    }, // let our custom formatter work
    minimumInputLength: 1,
    templateResult: formatRepo, // omitted for brevity, see the source of this page
    templateSelection: formatRepoSelection // omitted for brevity, see the source of this page
  });
  
  function start_again() {
    var ask = window.confirm("Are you sure you want to start again?");
    if (ask) {
      document.location.href = "{$server_path}start_over_again.php";
    }
  }
  
  var ray = {
    ajax: function(st)
    {
      this.show('load');
      $('.submit1').hide();
    },
    show: function(el)
    {
      this.getID(el).style.display = '';
    },
    getID: function(el)
    {
      return document.getElementById(el);
    }
  }
</script>

<script>
  function showcity(str)
  {
    if (str == "")
    {
      document.getElementById("your_city").innerHTML = "";
      document.getElementById("mname").innerHTML = "";
      return;
    }

    if (window.XMLHttpRequest)
    {
      xmlhttp = new XMLHttpRequest();
    }
    else
    {
      xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
    }

    xmlhttp.onreadystatechange = function ()
    {
      if (xmlhttp.readyState == 4 && xmlhttp.status == 200)
      {
        document.getElementById("your_city").innerHTML = xmlhttp.responseText;
      }
    }
    xmlhttp.open("GET", "get_city_by_country/" + str, true);
    xmlhttp.send();
  }
</script>

<style>
  #result
  {
    position:relative;
    width:auto;
    padding:10px;
    display:none;
    margin-top:-1px;
    border-top:0px;
    overflow:hidden;
    border:1px #CCC solid;
    background-color: white;
    height: auto;
    max-height: 350px;
    overflow: scroll;
  }
  .show
  {
    padding:5px; 
    border-bottom:1px #999 dashed;
    font-size:15px; 
    height:30px;
  }
  .show:hover
  {
    background:#eee;
    color:#000;
    cursor:pointer;
  }
  .lable-mrgn{
    margin-top:5px !important;
  }
  
  #load {
    color: #FFF !important;
    margin: 20px auto 0;
    float: right;
    text-align: center;
  }
</style>
{include file="include/footer.tpl"}
{include file="sent_queries.tpl"}