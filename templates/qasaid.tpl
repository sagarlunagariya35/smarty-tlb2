<div class="container white-bg">
  <div class="col-md-12 col-sm-12">
    <div class="page-title">
      <p style="margin-top:5px;"><a href="{$server_path}index.php">Home</a> / <a href="{$server_path}istifaadah/qasaid/">Qasaid</a> / <a href="#" class="active">{$this_qasida['title']}</a></p>
      <h1>Qasaid</h1>
    </div>
  </div>
  <div class="clearfix"></div> <!-- do not delete -->
  <div class="col-md-12 col-sm-12">
    <form class="forms1 white" name="raza_form" method="post">
      <div class="col-md-3 hidden-xs">
        <div class="profile-box-static">
          <h3 class="uppercase text-center">Qasaid</h3>
        </div>
        <div class="profile-box-static-bottom">
          <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
              {if $qasaid_alams}
                {assign var="i" value=1}
                 {foreach $qasaid_alams as $qa}
                   {assign var="i" value=$i+1}
                    {$list_qasaides = get_all_qasaid($qa['alam'])}
                      <div class="panel panel-default">
                        <div class="panel-heading" role="tab" id="headingOne">
                          <h4 class="panel-title">
                            <a role="button" data-toggle="collapse" data-parent="#accordion" href="#{$i}" aria-expanded="true" aria-controls="collapseOne">
                              {$qa['alam']}
                            </a>
                          </h4>
                        </div>
                        <div id="{$i}" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
                          <div class="panel-body">
                            
                              {if $list_qasaides}
                                {foreach $list_qasaides as $q}
                                  {$qasaid_done = get_qasaid_is_done($q['id'])}
                                    <a href="{$server_path}istifaadah/qasaid/{$q['id']}/">{$q['title']}</a>
                                    <hr class="sleek">
                                {/foreach}
                              {/if}
                          </div>
                        </div>
                      </div>
                {/foreach}
              {/if}              
            </div>
        </div>
      </div>
      <div class="col-md-6">
        <div class="col-md-3 col-xs-12 text-center">
          <input class="form-control" type="checkbox" name="alfazmana" checked>
          <div class="text-center" style="padding-bottom: 20px;color: #000">Alfaaz Maana</div>
        </div>
        <div class="col-md-3 col-xs-12 text-center">
          <input class="form-control" type="checkbox" name="lafzi" checked>
          <div class="text-center" style="padding-bottom: 20px;color: #000">Phrase Maana</div>
        </div>
        <div class="col-md-3 col-xs-12 text-center">
          <input class="form-control" type="checkbox" name="fehwa" checked>
          <div class="text-center" style="padding-bottom: 20px;color: #000">Fehwa</div>
        </div>
        <div class="col-md-3 col-xs-12 text-center">
          <input class="form-control" type="checkbox" name="taleeq" checked>
          <div class="text-center" style="padding-bottom: 20px;color: #000">Taleeq</div>
        </div>
        <div class="clearfix"></div>
        {$this_qasida['text']}
        <div class="clearfix"><br></div>
        <div style='width:100%;text-align:center'><br>
          <audio id='sound1' preload='auto' src='' controls style='width:100%'>
        </div>
        <div class="clearfix"><br></div>
      </div>
      <div class="col-md-3">
        <div class="profile-box-static">
          <h3 class="uppercase text-center">Progress</h3>
        </div>
        <div class="profile-box-static-bottom">
          <div class="text-center">
            <div style="margin:0px 30px;" class="text-center">
              <label style="padding-bottom: 5px;" class="sparkline">&nbsp;</label>
            </div>
            <div class="col-xs-12 text center prgupdate"></div>
          </div>
          <div class="clearfix"></div>
          <hr>
          <div class="text-center">
            <div class="noui-header">Qasida Recited %</div>
            <div class="clearfix"></div>
            <div id="slider1" class="sliders"></div>
            <span class="nouivalspan" id="slider-range-value1">
              {if $get_slider1_val}
                {number_format($get_slider1_val, 2)}%
              {else}
                0.00%
              {/if}
            </span>
          </div>
          <div class="clearfix"></div>
          <hr>
          <div class="text-center">
            <div class="noui-header">Mana Understood %</div>
            <div class="clearfix"></div>
            <div id="slider2" class="sliders"></div>
            <span class="nouivalspan" id="slider-range-value2">
              {if $get_slider2_val}
                {number_format($get_slider2_val, 2)}%
              {else}
                0.00%
              {/if}
            </span>
          </div>
          <div class="clearfix"></div>
          <hr>
          <div class="text-center">
            <div class="noui-header">Qasida Hifz %</div>
            <div class="clearfix"></div>
            <div id="slider3" class="sliders"></div>
            <span class="nouivalspan" id="slider-range-value3">
              {if $get_slider3_val}
                {number_format($get_slider3_val, 2)}%
              {else}
                0.00%
              {/if}
            </span>
          </div>
        </div>
      </div>
      <input type="hidden" value="{$server_path}" name="path" id="path">
    </form>
  </div>
</div>
      
{include file="include/js_block.tpl"}

<link href="{$server_path}css/bootstrap-switch.min.css" rel="stylesheet" type="text/css"/>
<link href="{$server_path}noUiSlider.8.0.2/nouislider.min.css" rel="stylesheet">
<script src="{$server_path}noUiSlider.8.0.2/nouislider.min.js"></script>
<script src="{$server_path}js/bootstrap-switch.min.js"></script>
<script src="{$server_path}js/jquery.sparkline.min.js" type="text/javascript"></script>

<script>
  var slider1Val = {$get_slider1_val}, slider2Val = {$get_slider2_val}, slider3Val = {$get_slider3_val};
  update_pie();

  function update_pie() {

    var AvgVal = (parseInt(slider1Val) + parseInt(slider2Val) + parseInt(slider3Val)) / 3;
    var RemVal = 100 - AvgVal;

    $('.sparkline').sparkline([AvgVal, RemVal], {
      type: "pie",
      width: "70px",
      sliceColors: ['#549BC7', '#09386c'],
      height: "70px",
      tooltipFormat: ''
    });
    $('.prgupdate').html("<strong>Completed:</strong> " + AvgVal.toFixed(2) + "%");
  }

  $(function (argument) {
    $('[type="checkbox"]').bootstrapSwitch();
  })
  $('input[name="alfazmana"]').on('switchChange.bootstrapSwitch', function (event, state) {
    if (state) {
      $('.mana').show();
    } else {
      $('.mana').hide();
    }
  });
  $('input[name="lafzi"]').on('switchChange.bootstrapSwitch', function (event, state) {
    if (state) {
      $('.lafzi').show();
    } else {
      $('.lafzi').hide();
    }
  });
  $('input[name="fehwa"]').on('switchChange.bootstrapSwitch', function (event, state) {
    if (state) {
      $('.fehwa').show();
    } else {
      $('.fehwa').hide();
    }
  });
  $('input[name="taleeq"]').on('switchChange.bootstrapSwitch', function (event, state) {
    if (state) {
      $('.taleeq').show();
    } else {
      $('.taleeq').hide();
    }
  });

  var slider1 = document.getElementById('slider1');
  var slider2 = document.getElementById('slider2');
  var slider3 = document.getElementById('slider3');

  var rangeSliderValueElement1 = document.getElementById('slider-range-value1');
  var rangeSliderValueElement2 = document.getElementById('slider-range-value2');
  var rangeSliderValueElement3 = document.getElementById('slider-range-value3');

  noUiSlider.create(slider1, {
    start: [{if $get_slider1_val}
              {$get_slider1_val}
            {else}
              {0}
            {/if}
           ],
    range: {
      'min': 0,
      '25%': 25,
      '50%': 50,
      '75%': 75,
      'max': 100
    },
    connect: 'lower',
    snap: true
  });

  slider1.noUiSlider.on('change', function (values, handle) {
    rangeSliderValueElement1.innerHTML = values[handle] + '%';
    slider1Val = values[handle];
    update_pie();
    var str = $('#path').val();
    var dataString = 'qasaid_id={$qasaid_id}&query=make_entry_of_qasaid&slider_id=1&data=' + values[handle];

    $.ajax({
      type: "POST",
      url: str + "ajax.php",
      data: dataString,
      cache: false,
      success: function () {
      }
    });
  });

  noUiSlider.create(slider2, {
    start: [{if $get_slider2_val}
              {$get_slider2_val}
            {else}
              {0}
            {/if}],
    range: {
      'min': 0,
      '25%': 25,
      '50%': 50,
      '75%': 75,
      'max': 100
    },
    connect: 'lower',
    snap: true
  });
  slider2.noUiSlider.on('change', function (values, handle) {
    rangeSliderValueElement2.innerHTML = values[handle] + '%';
    slider2Val = values[handle];
    update_pie();
    var str = $('#path').val();
    var dataString = 'qasaid_id={$qasaid_id}&query=make_entry_of_qasaid&slider_id=2&data=' + values[handle];

    $.ajax({
      type: "POST",
      url: str + "ajax.php",
      data: dataString,
      cache: false,
      success: function () {
      }
    });
  });

  noUiSlider.create(slider3, {
    start: [{if $get_slider3_val}
              {$get_slider3_val}
            {else}
              {0}
            {/if}],
    range: {
      'min': 0,
      '25%': 25,
      '50%': 50,
      '75%': 75,
      'max': 100
    },
    connect: 'lower',
    snap: true
  });
  slider3.noUiSlider.on('change', function (values, handle) {
    rangeSliderValueElement3.innerHTML = values[handle] + '%';
    slider3Val = values[handle];
    update_pie();
    var str = $('#path').val();
    var dataString = 'qasaid_id={$qasaid_id}&query=make_entry_of_qasaid&slider_id=3&data=' + values[handle];

    $.ajax({
      type: "POST",
      url: str + "ajax.php",
      data: dataString,
      cache: false,
      success: function () {
      }
    });
  });
  
  function pa(file) {
    audio = document.getElementById('sound1');
    audio.src = '{$server_path}/istifaadah/qasaid/{$this_qasida['folder']}/' + file + '.mp3';
    audio.load();
    audio.play();
  }
</script>

<style type='text/css'>
  Table{
    border-collapse:collapse;
    width: 100%;
    margin-top:5px;
  }
  Table , td, th{
    border:1px solid black;
    text-align: center;
    line-height: 1.6;
  }
  .bayt td {
    line-height: 70px;
  }
  .bayt{    
    font-family:  'lsdqasaid'; 
    font-size: 24px; 
    font-weight: bold;
    color: #009;
    text-shadow: 2px 2px 10px grey;
  }
  .mana{
    font-family: 'lsarabic';
    font-size: 14pt;
    color: #666;
    background-color: lightGoldenrodYellow;
  }
  .lafzi{
    font-family: 'lsarabic';
    font-size: 16pt;
    color: #111;
    background-color: paleGoldenrod;
  }
  .fehwa{
    font-family: 'lsarabic';
    font-size: 18pt;
    color: #000;
    background-color: lightYellow;
  }
  #container{
    width: 530px;
  }
  #mawad{
    float:right;
    width: 410px;
  }
  #comment-container{
    float: left;
    width:110px;
    clear: left;
  }
  .taleeq{
    width: 110px;
    font-family: 'lsarabic';
    font-size: 18pt;
    color: black;
    text-align: right;
    background-color: gold;
    border-radius: 5px;
    margin-top: 5px;
    margin-left: 2px;
    padding: 5px;
    line-height: 1.6;
  }
  .btn {
    display: block;
    width: auto;
    -webkit-transition: 0.4s;
    -moz-transition: 0.4s;
    -o-transition: 0.4s;
    transition: 0.4s;
    border: none;
    background-color: #033054;
    height: auto;
    padding: 10px;
    outline: none;
    color: #fff;
    text-transform: uppercase;
    text-align: center;
    float: right;
    line-height: 20px;
    border: 2px solid #fff;
  }
  .btn:active{
    color: #fff;
  }
  .btn:hover{
    background-color: #fff;
    color: #033054;
    border: 2px solid #033054;
  }
  @media screen and (max-width: 1200px) {
    #container {
      width: 100%;
    }
    #mawad {
      width: 100%;
    } 
    #comment-container {
      width: 100%;
    }
    .taleeq {
      width: 99%;
    }
  }
  .aud{
    cursor: pointer;
    width:15px;
  }
  .erab {
    font-family: 'lsdqasaid';
    font-weight: bold;
  }
  td{
    cursor: pointer;
  }
  .nouitooltip {
    display: block;
    position: absolute;
    border: 1px solid #D9D9D9;
    font: 400 12px/12px Arial;
    border-radius: 3px;
    background: #fff;
    top: -43px;
    padding: 5px;
    left: -9px;
    text-align: center;
    width: 50px;
    color: #000;
  }
  .nouitooltip strong {
    display: block;
    padding: 2px;
  }
</style>
{include file="include/footer.tpl"}