<div class="container white-bg">
  <div class="col-md-12 col-sm-12">

    <div class="page-title">
      <p style="margin-top:5px;">
        <a href="#" class="active">Academic Institutes - List Of Imaani Schools</a>
      </p>

      <h1>Academic Institutes - List Of Imaani Schools</h1>
    </div>
  </div>
  <div class="clearfix"></div> <!-- do not delete -->
  <div class="col-md-12 col-sm-12">
    <div class="">
      <form class="white">
        <br>
        <div class="clearfix"></div> <!-- do not delete -->
        <table id="institutes" class="table2 table table-responsive" data-page-length='20'>
          <thead class="first-child">
          <th class="first-child blue">Sr. No.</th>
          <th class="blue1">Institute Name</th>
          <th class="blue2">City</th>
          <th class="blue3">Address</th>
          <th class="blue3">Contact No.</th>
          <th class="blue2">Email</th>
          <th class="blue1">Website</th>
          </thead>
          {if $institutes}
            {counter start=0 print=false}
            {foreach $institutes as $inst}
            <tr>
              <td>{counter}</td>
              <td>{$inst.inst_name}</td>
              <td>{$inst.center_name}</td>
              <td>{$inst.address}</td>
              <td>{$inst.principal_contact}</td>
              <td class="emailadd"><a href="mailto:{$inst.email}" target="_blank">{$inst.email}</a></td>
              <td class="webadd"><a href="http://{$inst.website}" target="_blank">{$inst.website}</a></td>
            </tr>
           {/foreach}
          {/if}
        </table>        
        <div class="clearfix"></div> <!-- do not delete -->
      </form>
    </div>
  </div>
</div>

{include file="include/js_block.tpl"}

<link href="assets/datatables/dataTables.bootstrap.css" rel="stylesheet" type="text/css" />
<script src="assets/datatables/jquery.dataTables.min.js" type="text/javascript"></script>
<script src="assets/datatables/dataTables.bootstrap.min.js" type="text/javascript"></script>

<script type="text/javascript">
  $(function () {
    $('#institutes').DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": true,
      "ordering": false,
      "info": true,
      "autoWidth": false,
      language: {
        searchPlaceholder: "Search"
      }
    });
  });
</script>
{include file="include/footer.tpl"}
