<div class="container white-bg">
  <div class="col-md-12 col-sm-12">
    <div class="page-title">
      <p style="margin-top:5px;">
        <a href="#" class="active">My Last Tasmee Entry</a></p>
        <h1>My Last Tasmee Entry</h1>
    </div>
  </div>
  {if (isset($success_message) OR isset($error_message))}
    <div class="row">
      {include file="include/message.tpl"}
    </div>
  {/if}
  <div class="clearfix"></div> <!-- do not delete -->
  <div class="col-md-12 col-sm-12">
    <form action="" method="post" name="mumin_form">
      {include file="include/qrn_message.tpl"}
      <div class="body-container white-bg">
        <div class="hifz-tasmee-details-wrapper">
          <div class="htd-section">
            <div class="row">
              <table class="last-entry-table dataTable table table-bordered" style="border-collapse:collapse;">
                <thead>
                  <tr>
                    <th>Muhafiz ITS</th>
                    <th>Date</th>
                    <th>Last Entry From Surah</th>
                    <th>Last Entry To Surah</th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <td>{$my_last_entry['muhafiz_its']}</td>
                    <td>{date('d, F Y', strtotime($my_last_entry['timestamp']))}</td>
                    <td><b><span class="al-kanz">{$qrn_tasmee->get_surat_name($my_last_entry['from_surat'])}</span> - {$my_last_entry['from_ayat']}</b></td>
                    <td><b><span class="al-kanz">{$qrn_tasmee->get_surat_name($my_last_entry['to_surat'])}</span> - {$my_last_entry['to_ayat']}</b></td>
                  </tr>
                </tbody>
              </table>
              <hr>
              <h2>The selected mumin / mumena did tasmee from <span class="al-kanz">{$qrn_tasmee->get_surat_name($my_last_entry['to_surat'])}</span> - <span><{$my_last_entry['to_ayat']}</span> till . . .</h2>

              <div class="row">
                <div class="col-xs-12 col-md-5 qrn-tasmee-col-margin al-kanz">
                  <select name="surah" id="surah" class="form-control select-input">
                    {include file="include/qrn_surat_list.tpl"}
                  </select>
                </div>
                <div class="col-xs-12 col-md-5 qrn-tasmee-col-margin">
                  <input type="text" name="ayat_to" class="qrn-text-box col-md-3" id="ayat_to" placeholder="Enter Ayat No" />
                </div>
                <div class="col-xs-12 col-md-2 qrn-tasmee-col-margin">
                <input type="hidden" name="log_id" value="{$my_last_entry['id']}">
                <input type="hidden" name="from_surah" value="{$my_last_entry['from_surat']}">
                <input type="hidden" name="from_ayat" value="{$my_last_entry['from_ayat']}">
                  <button type="submit" name="submit" id="tasmee_log" class="btn btn-primary">Submit</button>
                </div>
              </div>
            </div>
          </div>
          <div class="clearfix"><br></div>
          {include file="include/qrn_links.tpl"}
        </div>
      </div>
    </form>
  </div>
</div>
{include file="include/js_block.tpl"}
{include file="include/footer.tpl"}