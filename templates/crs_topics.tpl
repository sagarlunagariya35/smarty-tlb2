<div class="container white-bg">
  <div class="col-md-12 col-sm-12">
    <div class="page-title">
      <p style="margin-top:5px;"><a href="#">Home</a> / <a href="#">Qasaid</a></p>
      <h1>Qasaid</h1>
    </div>
  </div>
  <div class="clearfix"></div>  
  <div class="col-md-12 col-sm-12">
	<div class="row">
		<div class="col-sm-3 side-bar">
			<div class="sidebar-block">
				<h3 class="sidebar-block-title">All courses <i class="fa fa-search"></i></h3>
				<ul class="courses-list">
                  {if $topics}
                    {assign var="i" value=1}
                    {foreach $topics as $topic}
                      {$sub_topics = $crs_topic->get_all_topics($topic.id)}
                      <li>
                        <a {if $sub_topics} data-toggle="collapse" class="list-toggle {if $i != '1'}collapsed{/if}" href="#sub-{$i}" {else} href="javascript:void(0);" {/if} >{$topic.title} {if $sub_topics} <i class="fa fa-angle-down"></i> {/if}</a>

                        {if $sub_topics}
                          <ul class="sub-courses collapse {if $i == '1'}in{/if}" id="sub-{$i}">
                            {foreach $sub_topics as $st}
                              <li><a href="{$server_path}crs_topics.php?topic_id={$st.id}">{$st.title}</a></li>
                            {/foreach}
                          </ul>
                        {/if}
                      </li>
                      {$i=$i+1}
                    {/foreach}
                  {/if}
				</ul>
			</div>
			<div class="sidebar-block">
				<h3 class="sidebar-block-title">SORT BY <i class="fa fa-search"></i></h3>
				<ul class="courses-list">
					<li><a href="javascript:void(0);">Free Courses</a></li>
					<li><a href="javascript:void(0);" class="active">Courses Not Taken</a></li>
					<li><a href="javascript:void(0);">Courses in progress</a></li>
				</ul>
			</div>
			<div class="sidebar-block padding resume-card">
				<div class="r-name">
					<h2>FiQH</h2>
					<h5>Beginner</h5>
				</div>
				<span class="r-btn">50/250</span>
				<div class="progress">
					<div class="progress-bar progress-bar-danger" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width: 40%"></div>
				</div>
				<a href="javascript:void(0);" class="orange-btn btn">Resume</a>
			</div>
			<div class="sidebar-block profile-sum">
				<h3 class="sidebar-block-title">My Profile</h3>
				<img src="images/user-profile.jpg" />
				<div class="profile-sum-foot">
					<h2 class="user-name">MUSTAFA ABBAS</h2>
					<p>Courses completed: <i>3 of 26</i></p>
					<p>Courses in progress: <i>8 of 26</i></p>
				</div>
			</div>
		</div>
		<div class="col-sm-9 right-side-wrap">
			<div class="title-wrap clearfix">
				<h2>{$topic_details.title}</h2>
			</div>
			<div class="row category-wrap">
                {if $sub_topics_of_topic}
                  {foreach $sub_topics_of_topic as $sub_topic}
                    {$sub_sub_topics = $crs_topic->get_all_topics($sub_topic.id)}
                    <div class="col-md-4 col-sm-6">
                      <div class="category-card">
                        <h2 class="category-name">{$sub_topic.title}</h2>
                        <h4 class="category-subtitle">Tajweed</h4>
                        <p>{$sub_topic.description}</p>
                        {if $sub_sub_topics}
                          <a href="{$server_path}crs_topics.php?topic_id={$sub_topic.id}" class="orange-btn">View Sub Topics </a>
                        {else}
                          <a href="{$server_path}crs_courses.php?topic_id={$sub_topic.id}" class="orange-btn">View Courses </a>
                        {/if}
                      </div>
                    </div>
                  {/foreach}
                {/if}
			</div>
		</div>
	</div>
  </div>
</div>

{include file="include/js_block.tpl"}

<style>
  .blue-box1-level a{
    color:#FFF!important;
    text-decoration:none;
  }
  .progress{
    color:#000;
    font-size:12px;
    line-height:20px;
    text-align:center;
  }
</style>

<script>
  $(document).ready(function () {
    $('[data-toggle="popover"]').popover({
      placement: 'bottom'
    });
    $('body').on('click', function (e) {
      $('[data-toggle="popover"]').each(function () {
        //the 'is' for buttons that trigger popups
        //the 'has' for icons within a button that triggers a popup
        if (!$(this).is(e.target) && $(this).has(e.target).length === 0 && $('.popover').has(e.target).length === 0) {
          $(this).popover('hide');
        }
      });
    });

  });
  
</script>