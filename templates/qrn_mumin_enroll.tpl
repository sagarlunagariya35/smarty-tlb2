<div class="container white-bg">
  <div class="col-md-12 col-sm-12">
    <div class="page-title">
      <p style="margin-top:5px;">
        <a href="#" class="active">Mumin Enroll</a></p>
        <h1>Mumin Enroll</h1>
    </div>
  </div>
  {if (isset($success_message) OR isset($error_message))}
    <div class="row">
      {include file="include/message.tpl"}
    </div>
  {/if}
  <div class="clearfix"></div> <!-- do not delete -->
  <div class="col-md-12 col-sm-12">
    <form action="" method="post" name="mumin_form">
      {include file="include/qrn_message.tpl"}
      <div class="body-container white-bg">
        <div class="hifz-tasmee-details-wrapper">
          <div class="htd-section">
            <div class="row">
              <div class="col-xs-12 col-md-3">
                <input type="text" class="form-control" value="{$mumin_its}" name="mumin_its" placeholder="Enter Mumin ITS">
              </div>
              <div class="col-xs-12 col-md-2">
                <button type="submit" name="search" class="btn btn-success">Search</button>
              </div>
            </div>
            <div class="clearfix"><br></div>
            
            {if $mumin_user_data}
              <div class="row">
                <div class="col-xs-12 col-md-5">
                  <div class="select-muhaffix-wrapper">
                    <div class="select-muhaffix-list">
                      <div><span class="__enhead">{$user_logedin->get_full_name()}</span>  {$user_logedin->get_jamaat()}, {$user_logedin->get_jamiat()}<br> {$user_logedin->get_quran_sanad()}</div>
                    </div>
                  </div>
                </div>
                <div class="col-xs-12 col-md-3">
                  <select name="surah" id="surah" class="form-control select-input">
                    {include file="include/qrn_surat_list.tpl"}
                  </select>
                </div>
                <div class="col-xs-12 col-md-2">
                  <input type="text" name="ayat_to" class="qrn-text-box" id="ayat_to" placeholder="Enter Ayat No">
                </div>
                <div class="col-xs-12 col-md-2">
                  <button type="submit" name="submit" class="btn btn-primary pull-right">Enroll</button>
                </div>
              </div>
              <div class="clearfix"><br></div>
            {/if}
          </div>
          <div class="clearfix"><br></div>
          {include file="include/qrn_links.tpl"}
        </div>
      </div>
    </form>
  </div>
</div>
{include file="include/js_block.tpl"}
{include file="include/footer.tpl"}