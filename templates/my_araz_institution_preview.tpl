{if ($marhala_id != '1' && in_array($marhala_id, $marhala_school_array))}
  <table class="table1 table table-responsive">
    <tr class="first-child">
      <td class="first-child">Hifz al Quran</td>
    </tr>
    <tr>
      <td>{if ($araz_data['hifz_sanad'] != '')} {$araz_data['hifz_sanad']} {else} NA {/if}</td>
    </tr>
  </table>

  <table class="table1 table table-responsive">
    <tr class="first-child">
      <td class="first-child">Madrasah name</td>
      <td>Darajah</td>
    </tr>
    <tr>
      <td>{if ($araz_data['madrasah_name'] != '')} {$araz_data['madrasah_name']} {else} NA {/if}</td>
      <td>{if ($araz_data['madrasah_darajah'] != '')} {$araz_data['madrasah_darajah']} {else} NA {/if}</td>
    </tr>
  </table>

{else}
  <table class="table1 table table-responsive">
    <tr class="first-child">
      <td class="first-child">Sabaq</td>
    </tr>
    <tr>
      <td>{$araz_data['madrasah_darajah']}</td>
    </tr>
  </table>
{/if}

{if (in_array($marhala_id, $marhala_school_array))}

  <table class="table1 table table-responsive">
    <tr class="first-child">
      <td class="first-child">Institution name</td>
      <td>Place</td>
      <td>Standard</td>
    </tr>
    <tr>
      <td>{$araz_data['school_name']}</td>
      <td>{$araz_data['school_city']}</td>
      <td>{$araz_data['school_standard']}</td>
    </tr>
  </table>

{else}
  {if ($institute_data)}
    <table class="table1 table table-responsive">
      <tr class="first-child">
        <td class="first-child">Institute name</td>
        <td>Place</td>
        <td>Degree / Course</td>
        <td>Accomodation</td>
        <td>Duration</td>
        <td>Course Start Date</td>
      </tr>
      {foreach $institute_data as $inst}
        <tr>
          <td>{$inst['institute_name']}</td>
          <td>{$inst['institute_city']}</td>
          <td>{$inst['course_name']}</td>
          <td>{$inst['accomodation']}</td>
          <td>{$inst['course_duration']}</td>
          <td>{$inst['course_started']}</td>
        </tr>
      {/foreach}
    </table>
  {/if}
{/if}
