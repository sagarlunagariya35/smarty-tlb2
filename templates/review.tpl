<div class="container white-bg">
  <div class="col-md-12 col-sm-12">

    <div class="page-title">
      {if ($already_araz_done == '1')}
      
        <p style="margin-top:5px;">
          <a href="{$server_path}marahil/">Raza Araz for</a> {if ($marhala_id != '1')} / <a href="{$server_path}marhala-{$marhala_id}/{get_marhala_slug($marhala_id)}/madrasah">{$panel_heading} ( Madrasah )</a> {/if} / <a href="{$server_path}marhala-{$marhala_id}/{get_marhala_slug($marhala_id)}/school">{$panel_heading} ( School )</a> / <a href="{$server_path}marhala-{$marhala_id}/{get_marhala_slug($marhala_id)}/ques">Istibsaar</a> / <a href="#" class="active">{$heading_msg}</a></p>
        <h1>{$heading_msg}<span class="alfatemi-text">&#1575;&#1604;&#1571;&#1606;&#1588;&#1591;&#1577;</span></h1>
        
      {else}
        
        <p style="margin-top:5px;">
          <a href="#" class="active">{$heading_msg}</a></p>
        <h1>{$heading_msg}</h1>
        
      {/if}
      
    </div>
  </div>
  <div class="clearfix"></div> <!-- do not delete -->
  <div class="col-md-12 col-sm-12">
    <div class="page">
      <form class="forms1 white" action="{$server_path}marhala-{$marhala_id}/{get_marhala_slug($marhala_id)}/message/" method="post">
        <div class="block top-mgn10"></div>
        <div class="blue-box1">
          <div class="block bot-mgn20 hmgn20">
            
            {if ($already_araz_done == '1')}
              <p>Your Araz has been Registered with us.</p>
            {else}
              <p>Your Araz is in under process. We will contact you within 72 hours.</p>
            {/if}
            
          </div>
          <div class="clearfix"></div> <!-- do not delete -->
        </div>
        </div>
        <div class="clearfix"></div> <!-- do not delete -->
      </form>
      <div class="clearfix"></div> <!-- do not delete -->
    </div>
  </div>
</div>
{include file="include/js_block.tpl"}
{include file="include/footer.tpl"}