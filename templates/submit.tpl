<div class="container white-bg">
  <div class="col-md-12 col-sm-12">

    <div class="page-title">
      <p style="margin-top:5px;">
        <a href="#" class="active">Araz Submitted</a></p>
      <h1>Araz Submitted - Track ID: <strong>{$araz_id}</strong><span class="alfatemi-text">عرض</span></h1>
    </div>
  </div>
  <div class="clearfix"></div> <!-- do not delete -->
  <div class="col-md-12 col-sm-12">
    <div class="page">
      <form class="forms1 white" action="{$server_path}istibsaar/" method="post">
        <div class="block top-mgn10"></div>
        <div class="blue-box1">
          <h3 class="bordered">
            <span class="lsd">
              {if (isset($already_araz_done) && $already_araz_done == 1)}
              <p class="large-fonts text-center">The Raza that you had been granted has been registered with talabulilm.com and further communication will be done through <a href="{$server_path}">www.talabulilm.com</p>
              {else}
              <p class="lsd large-fonts text-center" dir="rtl">ان شاء الله تعالى تماري عرض حضرة امامية نورانية ما عرض كروا ما اْؤسسس،
انسس جواب فضل تهاسسس ته وقت تمنسس خبر كروا ما اْؤسسس، تماري عرض مطابق مزيد كوئي سؤال هوئي تو <a href="mailto:education@alvazarat.org">education@alvazarat.org</a> ثثر email كروو.</p>
              
                {if (in_array($marhala_id,$marhala_array))}
                  {if ($success_message)}
                	<p>&nbsp;</p>
                	<p class="lsd large-fonts text-center" dir="rtl">{$success_message}</p>
                  {/if}
                {/if}
              
              <p class="large-fonts text-center">Inshallah o taala your araz will be presented in Hadrat Imamiyah Nooraniyah. For any further queries related to your araz please contact <a href="mailto:education@alvazarat.org">education@alvazarat.org</a></p>
              
              <p class="large-fonts text-center">You may be contacted regarding your araz.</p>
			  {/if}
              
              <p class="large-fonts text-center">Your araz track id is <strong style="color:#FFB99E;">{$araz_id}</strong>.</p>
              
              {if ($total_ques > $total_ques_answers)}
              	<p class="large-fonts text-center"><a href="{$server_path}complete_ques_details.php?araz_id={$araz_id}">Would you like to complete your azaim questionnaire?</a></p>
              {/if}
            </span>
          </h3>
          <div class="clearfix"></div> <!-- do not delete -->
        </div>

        <a href="{$redirect_link}"  class="skip lsd"> تفكر ساعة خير من عبادة سنة</a>
        <div class="clearfix"></div> <!-- do not delete -->
      </form>
      <div class="clearfix"></div> <!-- do not delete -->
    </div>
  </div>
</div>

<style>
  .skip{
    display: block;
    -webkit-transition: 0.4s;
    -moz-transition: 0.4s;
    -o-transition: 0.4s;
    transition: 0.4s;
    border: none;
    background-color: #033054;
    height: auto;
    margin: 20px auto 0;
    padding: 10px;
    outline: none;
    color: #fff !important;
    min-width: 150px;
    text-transform: uppercase;
    text-align: center;
    float: right;
    line-height: 20px;
    text-decoration: none !important;
  }
  
</style>
{include file="include/js_block.tpl"}
{include file="include/footer.tpl"}