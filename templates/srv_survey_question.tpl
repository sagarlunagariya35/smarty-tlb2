{if $survey_questions}
  {foreach $survey_questions as $srv_ques}
    <div class="col-xs-12 col-sm-12">
      <div class="quiz-point-ques">
        <div id="question_panel">
          {if ($srv_ques['question_type'] != QUIZ_GENERAL_HTML)}
          <div class="blue-box1 rtl border"> <!-- Add class rtl -->
            <h3><span class="inline-block lh3 arb_ques">{$srv_ques['question']}</span></h3>
          </div>

          {$options = unserialize($srv_ques['options'])}
          {if ($srv_ques['question_type'] == QUIZ_MULTIPLE_CHOICE || $srv_ques['question_type'] == QUIZ_TRUE_AND_FALSE)}
            {assign var="m" value=0}
            {assign var="selected" value=""}
            {foreach $options as $opt}
              {assign var="m" value=$m+1}
              <div class="col-md-12 col-sm-12">
                <div class="quiz-point-radio rtl"> <!-- Add Class .rtl -->
                  <input type="radio" name="your_answer{$srv_ques.id}" value="{$opt}" id="radio1{$m}" class="" required/>
                  <label for="radio1{$m}" style="color: #000;" class="radGroup3 lsd">{$opt}</label>
                </div>
              </div>
            {/foreach}
            <div class='clearfix'>&nbsp;</div>
          {/if}

          {if ($srv_ques['question_type'] == QUIZ_FILL_IN_THE_BLANK)}
            {if (count($options) == 1 && $options[0] == '')}
              <div class="col-md-12 col-sm-12">&nbsp;</div>
              <div class="col-md-12 col-sm-12">
                <input type="text" name="your_answer{$srv_ques.id}" id="your_answer" class="form-control" required>
              </div>
              <div class='clearfix'>&nbsp;</div>
            {else}
              {assign var="selected" value=""}
              <div class="col-md-12 col-sm-12">&nbsp;</div>
              <div class="col-md-12 col-sm-12">
                <select name="your_answer{$srv_ques.id}" id="your_answer" class="form-control select-input" required>
                  <option value="">Select Answer</option>
                    {foreach $options as $opt}
                      <option value="{$opt}">{$opt}</option>
                    {/foreach}
                </select>
              </div>
              <div class='clearfix'>&nbsp;</div>
            {/if}
          {/if}

          {if ($srv_ques['question_type'] == QUIZ_MATCH_THE_FOLLOWING)}
            <div class="col-md-12 col-sm-12">
              <div id="dragScriptContainer">
                <div id="questionDiv">
                {assign var="m" value=0}
                {assign var="selected" value=""}
                  {foreach $options as $opt}
                    {assign var="m" value=$m+1}
                    <div class="dragDropSmallBox" id="q{$m}">{$opt}</div>
                    <div class="destinationBox"></div>
                  {/foreach}
                </div>
                {$ans_options = unserialize($srv_ques['correct_answer'])}
                {*shuffle($ans_options)*}
                <div id="answerDiv">
                  {assign var="a" value=0}
                  {foreach $ans_options as $opt}
                    {assign var="a" value=$a+1}
                    <div class="dragDropSmallBox" id="a{$a}">{$opt}</div>
                  {/foreach}
                </div>
                <div id="dragContent"></div>
              </div>
              <input type="hidden" name="your_answer{$srv_ques.id}" id="your_answer" class="form-control" required>
            </div>
            <div class='clearfix'>&nbsp;</div>
          {/if}

          {if ($srv_ques['question_type'] == QUIZ_OPEN_ANSWERS)}
            <div class="col-md-12 col-sm-12">&nbsp;</div>
            <div class="col-md-12 col-sm-12">
              <input type="text" name="your_answer{$srv_ques.id}" id="your_answer" class="form-control" required>
            </div>
            <div class='clearfix'>&nbsp;</div>
          {/if}
        {else}
          {$srv_ques['question']}
        {/if}
        </div>
      </div>
    </div>
    <div class='clearfix'>&nbsp;</div>
  {/foreach}
{/if}

<div class="col-md-12 col-sm-12"><br></div>
<div class="col-md-12 col-sm-12">
  <button type="submit" id="submit_srv_ques" name="submit_srv_ques" class="btn btn-success pull-right col-md-2">Submit</button>
</div>
<div class="col-md-12 col-sm-12"><br></div>
<div class="clearfix"></div>
