{include file="include/js_block.tpl"}
<link href="{$server_path}assets/datatables/dataTables.bootstrap.css" rel="stylesheet" type="text/css" />
<script src="{$server_path}assets/datatables/jquery.dataTables.min.js" type="text/javascript"></script>
<script src="{$server_path}assets/datatables/dataTables.bootstrap.min.js" type="text/javascript"></script>
<script type="text/javascript" src="{$server_path}js/loader.js"></script>
<script type="text/javascript">
  google.charts.load("current", {
    packages: ['corechart']
  });
</script>

<div class="container white-bg">
  <div class="col-md-12 col-sm-12">
    <div class="page-title">
      <p style="margin-top:5px;">
        <a href="#" class="active">My Ikhwaan Report</a></p>
      <h1>My Ikhwaan Report</h1>
    </div>
  </div>
  {if (isset($success_message) OR isset($error_message))}
    <div class="row">
      {include file="include/message.tpl"}
    </div>
  {/if}
  <div class="clearfix"></div> <!-- do not delete -->
  <div class="col-md-12 col-sm-12">
    <form action="" method="post" name="tasmee_form">
      {include file="include/qrn_message.tpl"}
      <div class="body-container white-bg">
        <div class="hifz-tasmee-details-wrapper">
          <div class="htd-section">
            {if $select_mumin}
              <h2>You are currently linked with <span class="badge">{count($select_mumin)}</span> Ikhwaan</h2>
            {/if}
            <h2>My Ikhwaan progress ({date('jS M Y')})</h2>
            <div class="row">
              <div class="col-xs-12 col-md-12">
                <div class="col-xs-12 col-md-9">
                  {if $select_mumin}
                    <div id="barchart"></div>
                  {else}
                    No Data Available.
                  {/if}
                </div>
              </div>
            </div>
            <div class="clearfix"><br></div>
            <h2>Set Time Schedule</h2>
            <div class="row">
              <div class="col-xs-12 col-md-3">
                <label for="sch_mumin_its">Select Mumin :</label>
                <select name="sch_mumin_its" class="form-control select-input">
                  {if $select_mumin}
                    {foreach $select_mumin as $mumin_data}
                      <option value="{$mumin_data['its_id']}">{$mumin_data['mumin_full_name']}</option>
                    {/foreach}
                  {/if}
                </select>
              </div>
              <div class="col-xs-12 col-md-3">
                <label for="sch_days">Days Entry :</label>
                <select name="sch_days[]" multiple class="form-control select-input">
                  <option value="Everyday">Everyday</option>
                  <option value="Sunday">Sunday</option>
                  <option value="Monday">Monday</option>
                  <option value="Tuesday">Tuesday</option>
                  <option value="Wednesday">Wednesday</option>
                  <option value="Thursday">Thursday</option>
                  <option value="Friday">Friday</option>
                  <option value="Saturday">Saturday</option>
                  <option value="Excluding Sunday">Excluding Sunday</option>
                  <option value="Excluding Weekend">Excluding Weekend</option>
                </select>
              </div>
              <div class="col-xs-12 col-md-2">
                <label for="sch_hour">Hour Entry :</label>
                <select name="sch_hour" class="form-control select-input">
                  {for $i=1 to 24}
                  <option value="{$i}">{$i}</option>
                  {/for}
                </select>
              </div>
              <div class="col-xs-12 col-md-2">
                <label for="sch_time">Time Entry :</label>
                <select name="sch_time" class="form-control select-input">
                  <option value="00">00</option>
                  <option value="15">15</option>
                  <option value="30">30</option>
                  <option value="45">45</option>
                </select>
              </div>
              <div class="col-xs-12 col-md-1">
                <input type="submit" name="set_schedule" value="Set Schedule" class="btn btn-success">
              </div>
            </div>
            <div class="clearfix"><br></div>
            <div class="row">
              <div class="col-xs-12 col-md-12 table-responsive">
                <table class="table table-bordered table-hover tlb_schedule">
                  <thead>
                    <tr>
                      <th>Sr No</th>
                      <th>Mumin ITS</th>
                      <th>Days</th>
                      <th>Time</th>
                      <th class="text-center">Delete</th>
                    </tr>
                  </thead>
                  <tbody>
                    {if $schedule_records}
                      {assign var="s" value=1}
                      {foreach $schedule_records as $sr}
                        <tr>
                          <td>{$s++}</td>
                          <td>{$sr['mumin_its']}</td>
                          <td>{$sr['days']}</td>
                          <td>{$sr['hour']}:{$sr['time']}</td>
                          <td class="text-center"><a href="#" id="{$sr['id']}" class="delete_schedule"><i class="fa fa-remove"></i></a></td>
                        </tr>
                      {/foreach}
                    {/if}
                  </tbody>
                </table>
              </div>
            </div>
            <div class="clearfix"><br></div>
            <h2>Select Sadeeq for his/her Details &amp; Reports</h2>
            <div class="row">
              <div class="col-xs-12 col-md-3">
                <select name="s_mumin_its" id="s_mumin_its" class="form-control select-input">
                  <option value="">Select Mumin</option>
                  {if $select_mumin}
                    {foreach $select_mumin as $mumin_data}
                      <option value="{$mumin_data['its_id']}">{$mumin_data['mumin_full_name']}</option>
                    {/foreach}
                  {/if}
                </select>
              </div>
              <div class="col-xs-12 col-md-2">
                <a name="search" id="search" class="btn btn-success">Get Details</a>
              </div>
            </div>
            <div class="clearfix"><br></div>
            <div class="row" id="show_mumin_graph"></div>
          </div>
          {include file="include/qrn_links.tpl"}
        </div>
      </div>
    </form>
    <form method="post" id="frm-del-schedule">
      <input type="hidden" name="delete_schedule" id="del-schedule" value="">
    </form>    
  </div>
</div>

<script type="text/javascript">
  google.charts.setOnLoadCallback(drawChart);
  function drawChart() {
    var data = google.visualization.arrayToDataTable([
      ['ITS', 'Days past since bonding', 'Days attended', 'Total aayaat recited']
      {if $select_mumin}
        {','}
        {foreach $select_mumin as $data}
          {$result = $qrn_tasmee->get_tasmee_log_of_mumin($data['its_id'], $_SESSION[USER_ITS])}
          {if $result}
            {["{$data['mumin_full_name']}", {$result['Days_Past']}, {$result['Total_Record']}, {$result['Total_Ayat']}]}
            {if (count($select_mumin) - 1)}
              {','}
            {/if}
          {/if}
        {/foreach}
      {/if}
    ]);

    var options = {
      bars: 'horizontal' // Required for Material Bar Charts.
    };

    var chart = new google.visualization.BarChart(document.getElementById('barchart'));

    chart.draw(data, options);
  }
  
  $(window).resize(function () {
    drawChart();
  });

  $('#search').on("click", function (e) {
    e.preventDefault();
    var mumin_its = $('#s_mumin_its').val();

    $.ajax({
      type: "POST",
      url: "ajax.php",
      data: 'query=get_mumin_graph&mumin_its=' + mumin_its,
      cache: false,
      success: function (response) {
        $('.show_mumin_graph').show(600);

        if (response != '') {
          $('#show_mumin_graph').html(response);
        }
      }
    });
  });
  
  $('.tlb_schedule').DataTable({
    "paging": true,
    "lengthChange": false,
    "searching": true,
    "ordering": false,
    "info": true,
    "autoWidth": true,
    language: {
      searchPlaceholder: "Search"
    }
  });
  
  $(".delete_schedule").confirm({
    text: "Are you sure you want to delete time schedule?",
    title: "Confirmation required",
    confirm: function (button) {
      $('#del-schedule').val($(button).attr('id'));
      $('#frm-del-schedule').submit();
      alert('Are you sure you want to delete time schedule: ' + id);
    },
    cancel: function (button) {
      // nothing to do
    },
    confirmButton: "Yes",
    cancelButton: "No",
    post: true,
    confirmButtonClass: "btn-danger"
  });

</script>

<style>
  p {
    padding: 5px;
  }
</style>
{include file="include/footer.tpl"}