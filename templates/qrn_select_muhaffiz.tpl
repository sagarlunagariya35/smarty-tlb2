<div class="container white-bg">
  <div class="col-md-12 col-sm-12">
    <div class="page-title">
      <p style="margin-top:5px;">
        <a href="#" class="active">Send Request for Tasmee</a></p>
      <h1>Send Request for Tasmee</h1>
    </div>
  </div>
  {if (isset($success_message) OR isset($error_message))}
    <div class="row">
      {include file="include/message.tpl"}
    </div>
  {/if}
  <div class="clearfix"></div> <!-- do not delete -->
  {include file="include/qrn_message.tpl"}
  <div class="col-md-12 col-sm-12">
    <form action="" method="post" name="enroll_form" onsubmit="return(validate());">
      <div class="body-container">
        <div class="hifz-tasmee-details-wrapper">
          <div class="htd-section">
          <div class="qrn-reg-wrapper">
            <div class="qrn-reg-header">
              <span><strong>You can send 3 request only.</strong></span>
            </div>
          </div>
          <div class="clearfix"><br></div>
            
          {if $count_pending_request}
            <h2>Pending Requests <span class="badge">{$count_pending_request}</span></h2>
            {if $pending_request}
              <div class="row pending-request-wrapper">
                {foreach $pending_request as $data}
                  {$muhaffiz_data = $user_logedin->loaduser($data['muhafiz_its'])}
                  <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                    <div class="pending-request">
                      <div class="__text">
                        <span class="__prhead">{$muhaffiz_data->get_full_name()}</span>
                        {$muhaffiz_data->get_jamaat()}, {$muhaffiz_data->get_jamiat()}<br> {$muhaffiz_data->get_quran_sanad()}
                      </div>
                    </div>
                  </div>
                {/foreach}
              </div>
            {/if}
          {/if}
          </div>
        </div>
        <div class="clearfix"></div>
        <div class="select-muhaffix-wrapper">
          <h2>Select Al-Akh Al-Quraani</h2>
          <div class="qrn-tabs">
            <ul>
              <li id="select_sadeeq" class="select_sadeeq _active">Select from List</li>
              <li id="select_its_id" class="select_its_id">Enter ITS ID</li>
            </ul>
          </div>
          
            {if $already_set_muhafiz}
            <span>You are now connected with {$muhaffiz_name} - <a href="#" class="remove_muhafiz" id="{$already_set_muhafiz}">Change Muhaffiz click here</a></span>
            {else}
            <div class="select-muhaffix-list view_sadeeq">
              <ul class="no-style clearfix row">
                {if $select_muhafiz}
                  {assign var="i" value=0}
                    {foreach $select_muhafiz as $data}
                      {assign var="i" value=$i+1}
                        <li class="col-lg-3 col-md-4 col-sm-6 col-xs-12">
                          <div><input type="radio" id="rdMember{$i}" name="rdmuhafiz" value="{$data['muhafiz_its']}"> <label for="rdMember{$i}"> <span class="__enhead">{$data['muhafiz_full_name']}</span>  {$data['muhafiz_jamaat']}, {$data['muhafiz_jamiat']}<br> {$data['muhafiz_quran_sanad']}<br></label></div>
                        </li>
                    {/foreach}
                {else}
                  <li>Currently Muhafiz not here Check again later..</li>
                {/if}
              </ul>
            </div>
            <div class="row view_its_id">
              <div class="col-lg-3 col-md-4 col-sm-6 col-xs-12">
                <input type="text" class="form-control" name="muhafiz_its" placeholder="Enter Muhafiz ITS">
                <div class="clearfix"><br></div>
              </div>
            </div>
            <div class="__action">
              <button type="submit" name="submit" class="btn _wahid pull-left">Send Request for Tasmee</button>
            </div>
          {/if}
        </div>
        <div class="clearfix"><br></div>
          {include file="include/qrn_links.tpl"}
      </div>
    </form>
    <form method="post" id="frm-del-muhafiz-grp">
      <input type="hidden" name="remove_muhafiz" id="del-muhafiz-val" value="">
    </form>
  </div>
</div>
          
{include file="include/js_block.tpl"}

<script type="text/javascript">
  $('#select_sadeeq').on('click', function (e) {
    e.preventDefault();
    $("li.select_its_id").removeClass("_active");
    $("li.select_sadeeq").addClass("_active");
    $('.view_sadeeq').show(600);
    $('.view_its_id').hide(600);
    $('.show_ajx_result').hide();
  });

  $('#select_its_id').on('click', function (e) {
    e.preventDefault();
    $("li.select_its_id").addClass("_active");
    $("li.select_sadeeq").removeClass("_active");
    $('.view_sadeeq').hide(600);
    $('.view_its_id').show(600);
    $('.show_ajx_result').hide();
  });
  $('.view_its_id').hide();
  
  $(".remove_muhafiz").confirm({
    text: "Are you sure you want to change muhaffiz?",
    title: "Confirmation required",
    confirm: function (button) {
      $('#del-muhafiz-val').val($(button).attr('id'));
      $('#frm-del-muhafiz-grp').submit();
      alert('Are you sure you want to change muhaffiz: ' + id);
    },
    cancel: function (button) {
      // nothing to do
    },
    confirmButton: "Yes",
    cancelButton: "No",
    post: true,
    confirmButtonClass: "btn-danger"
  });
</script>
{include file="include/footer.tpl"}