<style>
  .skip{
    display:block;
    max-width:300px;
    -webkit-transition: 0.4s;
    -moz-transition: 0.4s;
    -o-transition: 0.4s;
    transition: 0.4s;
    border: none;
    background-color: #155485;
    height: auto;
    line-height:20px;
    margin:0px auto 20px;
    padding:10px;
    outline: none;
    color:#FFF;
    text-transform:uppercase;
    text-align:center;
    font-weight:bold;
    font-size:18px;
    float:right;
    text-decoration: underline none;
  }
  
  .set{
    background-color: #549BC7 !important;
  }
  
  .skip:hover{
    background-color: #549BC7;
  }
  
  #load {
    color: #000 !important;
    margin: 20px auto 0;
    float: right;
    text-align: center;
  }
</style>

<!-- Contents -->
<!-- ====================================================================================================== -->
<div class="container white-bg">
  <div class="col-md-12 col-sm-12">

    <div class="page-title">
      <p style="margin-top:5px;">
        <a href="{$server_path}marahil/">My Araz for education</a> 
        {if (in_array($marhala_id, $marhala_school_array))}
          / <a href="{$server_path}marhala-{$marhala_id}/{get_marhala_slug($marhala_id)}/school">{$panel_heading} ( School )</a> 
        {else}
          / <a href="{$server_path}marhala-{$marhala_id}/{get_marhala_slug($marhala_id)}/select-institute/">{$panel_heading} (Select Institute)</a>
        {/if}
        / <a href="{$server_path}marhala-{$marhala_id}/{get_marhala_slug($marhala_id)}/ques/">Azaaim</a> / <a href="#" class="active">Araz Preview</a>
      </p>
      
      <h1>Araz Preview<span class="alfatemi-text">عرض</span></h1>
    </div>
  </div>
  <div class="clearfix"></div> <!-- do not delete -->
  <div class="col-md-12 col-sm-12">
    <div class="page">
      
      <div class="clearfix"></div> <!-- do not delete -->
      <div class="col-md-3 pull-right hidden-xs"></div>
      
      <div class="col-md-3 col-xs-12 pull-right text-center"><br>
        <button type="button" name="for_arabic" id="for_arabic" class="skip lsd set btn btn-block" dir="rtl" value="1">لسان الدعوة واسطسس يظظاطط click كروو</button>
      </div>
      
      <div class="col-md-3 col-xs-12 pull-right text-center"><br>
        <button type="button" name="for_arabic" id="for_english" class="skip lsd btn btn-block" onClick="MM_goToURL('parent','{$server_path}marhala-{$marhala_id}/{get_marhala_slug($marhala_id)}/preview-english/');return document.MM_returnValue">For English Click Here</button>
      </div><br><br>
      <div class="clearfix"></div>
      
      <form class="forms1 white" action="{$server_path}marhala-{$marhala_id}/{get_marhala_slug($marhala_id)}/preview/" method="post">
        
        <div class="clearfix"></div> <!-- do not delete -->
        
        <div class="block top-mgn10"></div>
        <div class="blue-box1">
          <h3 class="bordered">
            <span class="lsd">
                <p class="lsd large-fonts text-center" dir="rtl">غب السجدات العبودية</p>
                <p class="lsd large-fonts text-center" dir="rtl">في الحضرة العالية الامامية اشرق الله انوارها</p>
                <p class="lsd large-fonts text-center" dir="rtl">عرض كرنار: {$user->get_full_name_ar()}</p>
                <p class="lsd large-fonts text-center" dir="rtl">موضع: {$user->get_city()}</p>
                <p class="lsd large-fonts text-center" dir="rtl">ادبًـا عرض كه</p>
                  {if ($marhala_id == '1')}
                  <p class="lsd large-fonts text-center" dir="rtl">تعليم ني ابتداء واسطسس مع الدعاء المبارك رزا مبارك فضل فرماوا ادبًـا عرض</p>
                  {elseif (count($institute_data) > 1)}
                  <p class="lsd large-fonts text-center" dir="rtl">حسب الذيلoptions ما سي تعليم حاصل كرواني استرشادًا  عرض</p>
                  {else}
                   <p class="lsd large-fonts text-center" dir="rtl">حسب الذيل تعليم حاصل كرواني مع الدعاء المبارك رزا مبارك فضل فرماوا ادبًأ عرض</p>
                  {/if}
                  
                <p class="lsd large-fonts text-center" dir="rtl">والسجدات</p>
            </span>
            
          </h3>
          <div class="clearfix"></div> <!-- do not delete -->
        </div>
        
        {include file="araz_preview.tpl"}
        
        <input type="submit" class="submit1 btn col-md-3 col-xs-12 pull-right" name="submit" value="Submit Araz" onclick="return ray.ajax()"/>
        <div id="load" style="display: none;"><i class="fa fa-spinner fa-spin fa-3x"></i></div>
        <a class="pull-left col-md-3 col-xs-12 start-again" onclick="start_again();">Start Over Again</a>
        <span class="page-steps">Step : 5 out of 5</span>
        <div class="clearfix"></div> <!-- do not delete -->
      </form>
      <div class="clearfix"></div> <!-- do not delete -->
    </div>
  </div>
</div>

{include file="include/js_block.tpl"}

<script language="JavaScript">

  function MM_goToURL() { //v3.0
    var i, args=MM_goToURL.arguments; document.MM_returnValue = false;
    for (i=0; i<(args.length-1); i+=2) eval(args[i]+".location='"+args[i+1]+"'");
  }

  function start_again() {
      var ask = window.confirm("Are you sure you want to start again?");
      if (ask) {
          document.location.href = "{$server_path}start_over_again.php";
      }
  }
  
  var ray = {
    ajax: function(st)
    {
      this.show('load');
      $('.submit1').hide();
    },
    show: function(el)
    {
      this.getID(el).style.display = '';
    },
    getID: function(el)
    {
      return document.getElementById(el);
    }
  }

</script>
<style>
  .orange-btn {
    margin-bottom: 20px;
  }
</style>
{include file="include/footer.tpl"}
{include file="sent_queries.tpl"}