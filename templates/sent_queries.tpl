<!--Beginning of the the footer contact form -->
<div class="vasplus_programming_blog_bottom" style=" display:none;">
  <div style="width:300px;margin:0px;padding:8px; border-bottom:1px solid #E1E1E1; background:#F6F6F6; float:left;-webkit-border-radius: 10px 0 0 0;-moz-border-radius: 10px 0 0 0;border-radius: 10px 0 0 0;">
    <div style="width:262px; float:left;font-size:12px;">Leave a message</div><div style="width:20px; float:left; text-align:right;"><a href="javascript:void(0);" id="vpb_exit_contact_box_buttons" style="padding:3px; padding-left:6px; padding-right:6px; text-decoration:none;" onclick="vpb_leave_a_message_hide();">x</a></div>
  </div><br clear="all" />

  <div id="vasplus_programming_blog_mailer_status" align="left"></div><br clear="all" />
  <div style="padding:10px;float:left;" align="left">
    <select name="" id="vpb_ques" class="vpb_contact_form_fields select-input" required> 
      <option value="">Select Any</option>
      <option value="1">Course not in the List</option>
      <!--option value="2">Institute not in the List</option>
      <option value="3">Country not in the List</option>
      <option value="4">City not in the List</option-->
    </select>
  </div><br clear="all" />

  <div style="padding:10px;float:left;" align="left">
    <textarea name="" id="vpb_message_body" class="vpb_contact_form_fields form-control" style="height:120px; padding-top:10px;" maxlength="100" required></textarea>
  </div><br clear="all" />

  <div style="padding:10px;float:left;" align="left">
    <input type="submit" id="vasplusPB_send_button" value="submit" onClick="Contact_Form_Submission_By_Vasplus_Programming_Blog('{$server_path}');">
  </div><br clear="all" />
</div>

<div id="vasplus_programming_blog_bottom" style="cursor:pointer;" onclick="vpb_leave_a_message_show();">
  <span id="vpv_tooltip_image"></span>
</div>
<script type="text/javascript" src="{$server_path}js/footer_form.js"></script>
<script type="text/javascript" src="{$server_path}js/post_watermarkinput.js"></script>
<link type="text/css" href="{$server_path}css/footer_form.css?v=1" rel="stylesheet" />
<!--End of the the footer contact form -->