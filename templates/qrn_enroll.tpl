<style>
  .lable-mrgn{
    margin-top:5px !important;
  }
</style>

<div class="container white-bg">
  <div class="col-md-12 col-sm-12">
    <div class="page-title">
      <p style="margin-top:5px;">
        <a href="#" class="active">Al-Akh al-Qurani</a></p>
      <h1>Al-Akh al-Qurani</h1>
    </div>
  </div>
  {if (isset($success_message) OR isset($error_message))}
    <div class="row">
      {include file="include/message.tpl"}
    </div>
  {/if}
  <div class="clearfix"></div> <!-- do not delete -->
  <div class="col-md-12 col-sm-12">
    <form action="" method="post" name="enroll_form" onsubmit="return(validate());">
      {include file="include/qrn_message.tpl"}
      <div class="body-container white-bg">
        <div class="enroll-wrapper">
          <div class="en-left">
            <ul class="enroll-list no-style">
              <li>
                <input type="checkbox" name="mumin" value="1" {$mumin_selected} id="chRecite"> <label for="chRecite"> <span class="__enhead">Recite / Tasmee</span> {$mumin_text}</label>
                {if $show_surat_ayat_options}
                  <div class="col-xs-12 col-sm-12 show_mumin">
                    <div class="clearfix"></div>
                    <div class="col-xs-12 col-sm-6 al-kanz">
                      <select name="surah" id="" class="form-control select-input">
                        {include file="include/qrn_surat_list.tpl"}
                      </select>
                    </div>
                    <div class="col-xs-12 col-sm-6">
                      <input type="text" name="ayat_to" class="qrn-text-box" id="" placeholder="Enter Ayat No">
                    </div>
                  </div>
                {/if}
                <div class="clearfix"></div>
              </li>
              <div class="clearfix"></div>
              <li>
                <input type="checkbox" name="muhafiz" value="2" {$muhafiz_selected} id="chListen"> <label for="chListen"> <span class="__enhead">Listen / Tehfeez</span> {$muhafiz_text}</label>
              </li>
            </ul>
            <hr>
            <h2>Before You Enroll Please Update Your Details If Required</h2>
            <div class="row">
              <div class="col-md-12">&nbsp;</div>
              <div class="form-group col-md-6">
                <lable class="color1"><b>E-mail:</b></lable>
                <div  class="shift20 lable-mrgn">
                  <input type="text" name="email" class="form-control" value="{$user_email}" placeholder="eg. abc@gmail.com">
                </div>
              </div>
              <div class="form-group col-md-6">
                <lable class="color1"><b>Mobile:</b></lable>
                <div  class="shift20 lable-mrgn">
                  <input type="text" name="mobile" class="form-control" value="{$user_contact}" placeholder="Current Mobile Number">
                </div>
              </div>
              <div class="form-group col-md-6">
                <lable class="color1"><b>Whatsapp:</b></lable>
                <div  class="shift20 lable-mrgn">
                  <input type="text" name="whatsapp" class="form-control" value="{$user_whatsapp}" placeholder="Whatsapp No.">
                </div>
              </div>
              <div class="form-group col-md-6">
                <lable class="color1"><b>Skype:</b></lable>
                <div  class="shift20 lable-mrgn">
                  <input type="text" name="viber" class="form-control" value="{$user_skype}" placeholder="Skype Id">
                </div>
              </div>
            </div>
            {if ($select_mumin == FALSE OR $select_muhafiz == FALSE)}
              <div class="__action">
                <button type="submit" name="submit" class="btn _wahid">Enroll for Al-Akh al-Quraani program</button>
              </div>
            {/if}
          </div>
          <div class="en-right">
            <img src="./img/qrn-intro-banner.jpg" alt="Banner" width="100%" />
          </div>
        </div>
      </div>
      {include file="include/qrn_links.tpl"}
      <div class="clearfix"><br></div>
    </form>
    <form method="post" id="frm-del-mumin-grp">
      <input type="hidden" name="delete_mumin" id="del-mumin-val" value="">
    </form>
    <form method="post" id="frm-del-muhafiz-grp">
      <input type="hidden" name="delete_muhafiz" id="del-muhafiz-val" value="">
    </form>
  </div>
</div>
      
{include file="include/js_block.tpl"}

<script type="text/javascript">
  $(document).ready(function () {
    $('#chRecite').change(function () {
      if (this.checked) {
        $('.show_mumin').show(600);
      } else {
        $('.show_mumin').hide(600);
      }
    });
  });

  $(".delete_mumin").confirm({
    text: "Are you sure you want to unregister from Al Akh al-Qurani program?",
    title: "Confirmation required",
    confirm: function (button) {
      $('#del-mumin-val').val($(button).attr('id'));
      $('#frm-del-mumin-grp').submit();
      alert('Are you sure you want to unregister from Al Akh al-Qurani program: ' + id);
    },
    cancel: function (button) {
      // nothing to do
    },
    confirmButton: "Yes",
    cancelButton: "No",
    post: true,
    confirmButtonClass: "btn-danger"
  });

  $(".delete_muhafiz").confirm({
    text: "Are you sure you want to unregister from Al Akh al-Qurani program?",
    title: "Confirmation required",
    confirm: function (button) {
      $('#del-muhafiz-val').val($(button).attr('id'));
      $('#frm-del-muhafiz-grp').submit();
      alert('Are you sure you want to unregister from Al Akh al-Qurani program: ' + id);
    },
    cancel: function (button) {
      // nothing to do
    },
    confirmButton: "Yes",
    cancelButton: "No",
    post: true,
    confirmButtonClass: "btn-danger"
  });

  function validate()
  {
    var mumin = document.enroll_form.mumin.checked;
    var muhafiz = document.enroll_form.muhafiz.checked;

    var error = 'Please Select any of Below :\n\n';
    var validate = true;

    if (mumin == '')
    {
      error += 'Please Select Recite\n';
      if (muhafiz == '')
      {
        error += 'Please Select Listen\n';
        validate = false;
      }
    }

    if (mumin != '') {
      if (document.enroll_form.surah.value == '') {
        error += 'Please select Surah.\n';
        validate = false;
      }

      if (document.enroll_form.ayat_to.value == '') {
        error += 'Please Enter Ayat No.\n';
        validate = false;
      }
    }

    if (validate == false)
    {
      alert(error);
      return validate;
    }
  }
</script>
{include file="include/footer.tpl"}