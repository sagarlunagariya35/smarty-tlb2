<div class="container white-bg">
  <div class="col-md-12 col-sm-12">
    <div class="page-title">
      <p style="margin-top:5px;"><a href="#">Home</a> / <a href="#">Survey</a></p>
      <h1>Survey</h1>
    </div>
  </div>
  <div class="clearfix"></div> <!-- do not delete -->
  <div class="col-md-12 col-sm-12">
    <div class="page">
      <form class="form1 white" method="post" action="">

        <div class="clearfix"></div> <!-- do not delete -->  
        <div class="row">
          <div class="col-md-3 col-xs-12 hidden-xs">
            <div class="profile-box-static">
              <h3 class="uppercase text-center">List of Surveys</h3>
            </div>
            <div class="profile-box-static-bottom">
              {if $surveys}
                {foreach $surveys as $srv}
                  <a href="{$server_path}srv_take_survey.php?survey_id={$srv.survey_id}">» {$srv.title}<hr></a>
                {/foreach}
              {/if}
            </div>
          </div>
          <div class="col-md-9 col-xs-12" >
            <div class="col-xs-12">&nbsp;</div>
            <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
              {if $surveys}
                {assign var="i" value=1}
                  {foreach $surveys as $srv}
                    {assign var="i" value=$i+1}
                    <div class="panel panel-default">
                      <div class="panel-heading" role="tab" id="headingOne">
                        <h4 class="panel-title">
                          <a href="{$server_path}srv_take_survey.php?survey_id={$srv.survey_id}">
                            {$srv.title}
                          </a>
                        </h4>
                      </div>
                  </div>
                {/foreach}
              {/if}
            </div>
          </div>
        </div>
      </form>
    </div>
  </div>
</div>
<style>
  .blue-box1-level a {
    color: #FFF !important;
    text-decoration: none;
  }
  .progress {
    color: #000;
    font-size: 12px;
    line-height: 20px;
    text-align: center;
  }
</style>
{include file="include/js_block.tpl"}
{include file="include/footer.tpl"}