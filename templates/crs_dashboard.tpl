<div class="container white-bg">
  <div class="col-md-12 col-sm-12">
    <div class="page-title">
      <p style="margin-top:5px;"><a href="#">Home</a> / <a href="#">Qasaid</a></p>
      <h1>Qasaid</h1>
    </div>
  </div>
  <div class="clearfix"></div>  
  <div class="col-md-12 col-sm-12">
	<div class="row">
		<div class="col-sm-3 side-bar">
			<div class="sidebar-block">
				<h3 class="sidebar-block-title">All courses <i class="fa fa-search"></i></h3>
				<ul class="courses-list">
                  {if $topics}
                    {assign var="i" value=1}
                    {foreach $topics as $topic}
                      {$sub_topics = $crs_topic->get_all_topics($topic.id)}
                      <li>
                        <a {if $sub_topics} data-toggle="collapse" class="list-toggle {if $i != '1'}collapsed{/if}" href="#sub-{$i}" {else} href="javascript:void(0);" {/if} >{$topic.title} {if $sub_topics} <i class="fa fa-angle-down"></i> {/if}</a>
                        
                        {if $sub_topics}
                          <ul class="sub-courses collapse {if $i == '1'}in{/if}" id="sub-{$i}">
                            {foreach $sub_topics as $st}
                              <li><a href="{$server_path}crs_topics.php?topic_id={$st.id}">{$st.title}</a></li>
                            {/foreach}
                          </ul>
                        {/if}
                      </li>
                      {$i=$i+1}
                    {/foreach}
                  {/if}
				</ul>
			</div>
			<div class="sidebar-block">
				<h3 class="sidebar-block-title">SORT BY <i class="fa fa-search"></i></h3>
				<ul class="courses-list">
					<li><a href="javascript:void(0);">Free Courses</a></li>
					<li><a href="javascript:void(0);" class="active">Courses Not Taken</a></li>
					<li><a href="javascript:void(0);">Courses in progress</a></li>
				</ul>
			</div>
			<div class="sidebar-block padding profile-block">
				<h3 class="sidebar-block-title">My Profile</h3>
				<h2 class="user-name">MUSTAFA ABBAS</h2>
				<p class="info-text">Progress tracker</p>
				<div class="chart-holder clearfix">
					<canvas id="chart-area" width="85" height="85"></canvas>
					<div class="chart-data">
						<p><span class="completed-text">Completed: </span>30/90</p>
						<p><span class="progress-text">In Progress: </span>10/90</p>
						<p><span class="nottaken-text">Not taken: </span>30/90</p>
					</div>
				</div>
				<p class="info-text">My Achievements</p>
				<div class="achievement-list">
					<img src="images/badge1.png" alt="" />
					<img src="images/icn-trophy.png" alt="" />
				</div>
				<a href="javascript:void(0);" class="see-more">See more on my profile <i class="fa fa-arrow-right"></i></a>
			</div>
			<div class="sidebar-block padding resume-card">
				<div class="r-name">
					<h2>FiQH</h2>
					<h5>Beginner</h5>
				</div>
				<span class="r-btn">50/250</span>
				<div class="progress">
					<div class="progress-bar progress-bar-danger" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width: 40%"></div>
				</div>
				<a href="javascript:void(0);" class="orange-btn btn">Resume</a>
			</div>
		</div>
		<div class="col-sm-9 right-side-wrap">
			<div class="intro">
				<h3>Istifadah</h3>
				<p>A section where you can meet the inner intellect and learn new things EVERYDAY. Lorem ipsum dolor sit amet,
 lectus, a tempor turpis ornare nec. Maecenas malesuada scelerisque felis vestibulum pellentesque. </p>
			</div>
			<div class="title-wrap clearfix">
				<h2>Categories</h2>
			</div>
			<ul class="categories-list">
              {if $topics}
                {foreach $topics as $topic}
                  <li><a href="javascript:void(0);"><img src="{$server_path}upload/courses/topics/{$topic.img_icon}" alt="{$topic.title}" height="100" width="100" /><span>{$topic.title}</span></a></li>
                {/foreach}
              {/if}
			</ul>
			<div class="title-wrap clearfix">
				<h2>My Courses</h2>
			</div>
			{include file="include/crs.course.tpl"}
		</div>
	</div>
  </div>
</div>

{include file="include/js_block.tpl"}

<style>
  .blue-box1-level a{
    color:#FFF!important;
    text-decoration:none;
  }
  .progress{
    color:#000;
    font-size:12px;
    line-height:20px;
    text-align:center;
  }
</style>

<script>
  $(document).ready(function () {
    $('[data-toggle="popover"]').popover({
      placement: 'bottom'
    });
    $('body').on('click', function (e) {
      $('[data-toggle="popover"]').each(function () {
        //the 'is' for buttons that trigger popups
        //the 'has' for icons within a button that triggers a popup
        if (!$(this).is(e.target) && $(this).has(e.target).length === 0 && $('.popover').has(e.target).length === 0) {
          $(this).popover('hide');
        }
      });
    });

	// chart
	var doughnutData = [
			{
				value: 33,
				color:"#cccccc",
				highlight: "#cccccc"
			},
			{
				value: 20,
				color: "#cf4914",
				highlight: "#cf4914"
			},
			{
				value: 33,
				color: "#337ab7",
				highlight: "#337ab7"
			}
		];

		window.onload = function(){
			var ctx = document.getElementById("chart-area").getContext("2d");
			window.myDoughnut = new Chart(ctx).Doughnut(doughnutData, {
				responsive : false,
				segmentStrokeWidth : 5,
				percentageInnerCutout : 80,
				animateRotate : false,
				scaleStartValue: 0,
				showScale: false,
				showTooltips: false,
				onAnimationComplete: addText
			});
		};
		
		function addText() {
		  var canvas = document.getElementById("chart-area");
		  var ctx = document.getElementById("chart-area").getContext("2d");

		  var cx = canvas.width / 2;
		  var cy = canvas.height / 2;

		  ctx.textAlign = 'center';
		  ctx.textBaseline = 'middle';
		  ctx.font = '18px Ubuntu';
		  ctx.fillStyle = 'black';
		  ctx.fillText("33%", cx, cy);

		}
	// chart course
	var doughnutData2 = [
			{
				value: 70,
				color:"#cccccc",
				highlight: "#cccccc"
			},
			{
				value: 30,
				color: "#cf4914",
				highlight: "#cf4914"
			}
		];

		window.onload = function(){
			var ctx = document.getElementById("chart-course").getContext("2d");
			window.myDoughnut = new Chart(ctx).Doughnut(doughnutData2, {
				responsive : false,
				segmentStrokeWidth : 5,
				percentageInnerCutout : 80,
				animateRotate : false,
				scaleStartValue: 0,
				showScale: false,
				showTooltips: false,
				onAnimationComplete: addText2
			});
		};
		
		function addText2() {
		  var canvas = document.getElementById("chart-course");
		  var ctx = document.getElementById("chart-course").getContext("2d");

		  var cx = canvas.width / 2;
		  var cy = canvas.height / 2;

		  ctx.textAlign = 'center';
		  ctx.textBaseline = 'middle';
		  ctx.font = '18px Ubuntu';
		  ctx.fillStyle = 'black';
		  ctx.fillText("24%", cx, cy);

		}
  });
  
</script>