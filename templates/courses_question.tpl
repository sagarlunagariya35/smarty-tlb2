<div id="question_panel">
  {if ($questions['question_type'] != QUIZ_GENERAL_HTML)}
  <div class="blue-box1 rtl border"> <!-- Add class rtl -->
    <h3><span class="inline-block lh3 arb_ques">{$questions['question']}</span></h3>
  </div>

  <input type="hidden" id="current_que_id" value="{$questions['id']}">
  <input type="hidden" id="current_course_id" value="{$questions['course_id']}">

  {$options = unserialize($questions['options'])}
  {if ($questions['question_type'] == QUIZ_MULTIPLE_CHOICE || $questions['question_type'] == QUIZ_TRUE_AND_FALSE)}
    {assign var="m" value=0}
    {assign var="selected" value=""}
    {foreach $options as $opt}
      {assign var="m" value=$m+1}
      {if ($pre_fill_user_answers['submitted_answer'] == $opt)}
        {assign var="selected" value="checked"}
      {/if}
      <div class="col-md-12 col-sm-12">
        <div class="quiz-point-radio rtl"> <!-- Add Class .rtl -->
          <input type="radio" name="your_answer" value="{$opt}" id="radio1{$m}" class="" {$selected}/>
          <label for="radio1{$m}" style="color: #000;" class="radGroup3 lsd">{$opt}</label>
        </div>
      </div>
    {/foreach}
  {/if}

  {if ($questions['question_type'] == QUIZ_FILL_IN_THE_BLANK)}
    {if (count($options) == 1 && $options[0] == '')}
      <div class="col-md-12 col-sm-12">&nbsp;</div>
      <div class="col-md-12 col-sm-12">
        <input type="text" name="your_answer" id="your_answer" class="form-control" value="{$pre_fill_user_answers['submitted_answer']}">
      </div>
    {else}
      {assign var="selected" value=""}
      <div class="col-md-12 col-sm-12">&nbsp;</div>
      <div class="col-md-12 col-sm-12">
        <select name="your_answer" id="your_answer" class="form-control select-input">
          <option value="">Select Answer</option>
            {foreach $options as $opt}
              {if ($pre_fill_user_answers['submitted_answer'] == $opt)}
                {assign var="selected" value="checked"}
              {/if}
            <option value="{$opt}" {$selected}>{$opt}</option>
            {/foreach}
        </select>
      </div>
    {/if}
  {/if}

  {if ($questions['question_type'] == QUIZ_MATCH_THE_FOLLOWING)}
    <div class="col-md-12 col-sm-12">
      <div id="dragScriptContainer">
        <div id="questionDiv">
        {assign var="m" value=0}
        {assign var="selected" value=""}
          {foreach $options as $opt}
            {assign var="m" value=$m+1}
            <div class="dragDropSmallBox" id="q{$m}">{$opt}</div>
            <div class="destinationBox"></div>
          {/foreach}
        </div>
        {$ans_options = unserialize($questions['correct_answer'])}
        {*shuffle($ans_options)*}
        <div id="answerDiv">
          {assign var="a" value=0}
          {foreach $ans_options as $opt}
            {assign var="a" value=$a+1}
            <div class="dragDropSmallBox" id="a{$a}">{$opt}</div>
          {/foreach}
        </div>
        <div id="dragContent"></div>
      </div>
      <input type="hidden" value="{$pre_fill_user_answers['submitted_answer']}" name="your_answer" id="your_answer" class="form-control">
    </div>

  {/if}
  
  {if ($questions['question_type'] == QUIZ_OPEN_ANSWERS)}
    <div class="col-md-12 col-sm-12">&nbsp;</div>
    <div class="col-md-12 col-sm-12">
      <input type="text" name="your_answer" id="your_answer" class="form-control" value="{$pre_fill_user_answers['submitted_answer']}">
    </div>
  {/if}
{else}
  {$questions['question']}
{/if}
</div>

<div class="col-md-12 col-sm-12"><br></div>
<div class="col-md-12 col-sm-12">
  <button type="button" id="next" class="btn btn-success pull-right col-md-2">Next</button>
</div>
<div class="col-md-12 col-sm-12"><br></div>
<div class="clearfix"></div>

{if ($questions['timer'] != 0)}
  <script type="text/javascript">
    secs = {$questions['timer']};
    timer = setInterval(function () {
        if(secs < 1){
            clearInterval(timer);
            document.getElementById('next').click();
        }
        secs--;
    }, 1000);
  </script>
{/if}
