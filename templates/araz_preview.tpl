{*$hifz_table}
{$madrasah_table*}
{$institute_table}

{if (in_array($marhala_id, array(6, 7, 8)))}
  
  {assign var=min_date value=FALSE}
  {if $institute_data}
    {foreach $institute_data as $inst}
      {$all_dates[] = $inst['course_started']}
      {$tmp_dt = strtotime($inst['course_started'])}
      {if ($min_date > $tmp_dt || $min_date == FALSE)}
      	{$min_date = $tmp_dt}
      {else}
        {$min_date = $min_date}
      {/if}
    {/foreach}
  {/if}
  
  {*usort($all_dates, function($a, $b) {
    return strtotime($a) < strtotime($b) ? -1: 1;
  });

  $lowest_date = $all_dates[0];
  foreach($institute_data as $elementKey => $element) {
    foreach($element as $key => $value) {
      if($key == 'course_started' && $value == $lowest_date){
        $current_study_array = $institute_data[$elementKey];
      } 
    }
  }*}
    
  {$current_date = time()}
    {if ($min_date > $current_date)}
    
      <div class="blue-box1" style="background-color: #cf7f15">
      <h3>Current study details:</h3>
      <p>If you have completed a course, and are waiting for the pursuing of your next course, fill in the last course details.</p>
      <p>&nbsp;</p>
      <div class="col-md-6 col-sm-12 shift">
        <div class="shift10">
          <input type="text" name="course_name" placeholder="Course Name" id="search_course" class="search_course required" required>
          <div id="result_course"></div>
        </div>
      </div>
      <div class="col-md-6 col-sm-12 shift">
        <div class="shift10">
          <input type="text" name="institute_name" placeholder="Institute Name" id="search_inst" class="search_institute required" required>
          <div id="result_institute"></div>
        </div>
      </div>
      <div class="col-md-6 col-sm-12 shift">
        <div class="shift10">
          <select name="institute_country" onChange="showcity(this.value)" class="required select-input" required>
            <option value="">Institute Country</option>
            {if ($countries != '')}
              {foreach $countries as $cntry}
                <option value="{$cntry['name']}">{$cntry['name']}</option>
              {/foreach}
            {/if}
          </select>
        </div>
      </div>
      <div class="col-md-6 col-sm-12 shift">
        <div  class="shift10" id="your_city">
          <select name="madrasah_city" class="required select-input" required>
            <option value="">Institute City</option>
          </select>
        </div>
      </div>
      <div class="row"></div>
      <div class="row"></div>
      {$cur_year = date('Y')}
      {$rec_year = $cur_year - 4}
      {$nxt_year = $cur_year + 4}
    <!--      <div class="col-md-6 col-sm-12 shift">
        <div  class="shift10">
          <select name="course_start_month" class="required select-input" required>
            <option value="">Course Start Month</option>
            <?php
            foreach ($course_month_array as $str_mnth) {
              ?>
              <option value="<?php //echo $str_mnth; ?>"><?php //echo $str_mnth; ?></option>
              <?php
            }
            ?>
          </select>
        </div>
      </div>
      <div class="col-md-6 col-sm-12 shift">
        <div  class="shift10">
          <select name="course_start_year" class="required select-input" required>
            <option value="">Course Start Year</option>
            <?php
            for ($i = $rec_year; $i <= $nxt_year; $i++) {
              ?>
              <option value="<?php //echo $i; ?>"><?php //echo $i; ?></option>
              <?php
            }
            ?>
          </select>
        </div>
      </div>
      <div class="col-md-6 col-sm-12 shift">
        <div  class="shift10">
          <select name="course_end_month" class="select-input">
            <option value="">Course End Month</option>
            <?php
            foreach ($course_month_array as $end_mnth) {
              ?>
              <option value="em_<?php //echo $end_mnth; ?>"><?php //echo $end_mnth; ?></option>
              <?php
            }
            ?>
          </select>
        </div>
      </div>
      <div class="col-md-6 col-sm-12 shift">
        <div  class="shift10">
          <select name="course_end_year" class="select-input">
            <option value="">Course End Year</option>
            <?php
            for ($j = $rec_year; $j <= $nxt_year; $j++) {
              ?>
              <option value="ey_<?php //echo $j; ?>"><?php //echo $j; ?></option>
              <?php
            }
            ?>
          </select>
        </div>
      </div>-->
      <div class="clearfix"></div> <!-- do not delete -->
    <div class="clearfix"></div> <!-- do not delete -->
    </div><br>
    
      <script>
        $(document).ready(function () {
          //Institute - Search
          $(".search_institute").keyup(function ()
          {
            var search_inst = $(this).val();
            if (search_inst == '') {
              $("#result_institute").fadeOut();
            } else {
              var dataString = search_inst;
              if (search_inst != '')
              {
                $.ajax({
                  type: "POST",
                  url: "ajax_search_institute/" + dataString,
                  cache: false,
                  success: function (html)
                  {
                    if (html != '') $("#result_institute").html(html).show();
                  }
                });
              }
              return false;
            }
          });
          
          $('#search_inst').click(function () {
            if ($(this).val() == '') {
              $("#result_institute").fadeOut();
            } else {
              $("#result_institute").fadeIn();
            }
          });
          
          $('#search_inst').focusout(function(){
            $("#result_institute").fadeOut(); 
          });
          
          // Course - Search
          $(".search_course").keyup(function ()
          {
            var search_course = $(this).val();
            if (search_course == '') {
              $("#result_course").fadeOut();
            } else {
              var datacourse = search_course;
              if (search_course != '')
              {
                $.ajax({
                  type: "POST",
                  url: "ajax_search_course/" + datacourse,
                  cache: false,
                  success: function (html)
                  {
                    if (html != '') $("#result_course").html(html).show();
                  }
                });
              }
              return false;
            }
          });
    
          $('#search_course').click(function () {
            if ($(this).val() == '') {
              $("#result_course").fadeOut();
            } else {
              $("#result_course").fadeIn();
            }
          });
          
          $('#search_course').focusout(function(){
            $("#result_course").fadeOut(); 
          });
    
        });
    
        function get_value_institute(str) {
          $('#search_inst').val(str);
          $("#result_institute").fadeOut();
        }
        
        function get_value_course(str) {
          $('#search_course').val(str);
          $("#result_course").fadeOut();
        }
        
        function showcity(str)
        {
          if (str == "")
          {
            document.getElementById("your_city").innerHTML = "";
            document.getElementById("mname").innerHTML = "";
            return;
          }
    
          if (window.XMLHttpRequest)
          {
            xmlhttp = new XMLHttpRequest();
          }
          else
          {
            xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
          }
    
    
    
          xmlhttp.onreadystatechange = function ()
          {
            if (xmlhttp.readyState == 4 && xmlhttp.status == 200)
            {
              document.getElementById("your_city").innerHTML = xmlhttp.responseText;
            }
          }
          xmlhttp.open("GET", "get_city_by_country/" + str, true);
          xmlhttp.send();
        }
      </script>
    
      <style>
        #result_institute
        {
          position:relative;
          width:auto;
          padding:10px;
          display:none;
          margin-top:-1px;
          border-top:0px;
          overflow:hidden;
          border:1px #CCC solid;
          background-color: white;
          height: 350;
          max-height: 350px;
          overflow: scroll;
        }
        #result_course
        {
          position:relative;
          width:auto;
          padding:10px;
          display:none;
          margin-top:-1px;
          border-top:0px;
          overflow:hidden;
          border:1px #CCC solid;
          background-color: white;
          height: 350;
          max-height: 350px;
          overflow: scroll;
        }
        .show
        {
          border-bottom:1px #999 dashed;
          font-size:13px; 
          color: #000;
          font-weight: 500;
        }
        .show:hover
        {
          background:#eee;
          color:#000;
          cursor:pointer;
        }
        .search_institute{
          color: #000;
        }
        .search_course{
          color: #000;
        }
      </style>
    {/if}
 {/if}