{include file="include/js_block.tpl"}
<link href="{$server_path}assets/datatables/dataTables.bootstrap.css" rel="stylesheet" type="text/css" />
<script src="{$server_path}assets/datatables/jquery.dataTables.min.js" type="text/javascript"></script>
<script src="{$server_path}assets/datatables/dataTables.bootstrap.min.js" type="text/javascript"></script>

<div class="container white-bg">
  <div class="col-md-12 col-sm-12">
    <div class="page-title">
      <p style="margin-top:5px;">
        <a href="#" class="active">My Tehfeez report</a></p>
        <h1>My Tehfeez report</h1>
    </div>
  </div>
  {if (isset($success_message) OR isset($error_message))}
    <div class="row">
      {include file="include/message.tpl"}
    </div>
  {/if}
  <div class="clearfix"></div> <!-- do not delete -->
  <div class="col-md-12 col-sm-12">
    <form action="" method="post" name="tasmee_form">
      {include file="include/qrn_message.tpl"}
      <div class="body-container white-bg">
        <div class="hifz-tasmee-details-wrapper">
          <div class="htd-section">
            <h2>How many aayaat I listen to daily ({date('jS M Y')})</h2>
            <div class="row">
              <div class="col-xs-12 col-md-9">
                {if $tasmee_records}
                  <div id="columnchart_values"></div>
                {else}
                  No Data Available.
                {/if}
              </div>
              <div class="col-xs-12 col-md-3">
                <div class="text-center">
                  <p>Yesterday I listened to</p>
                  <p><span style="font-size: 16px; font-weight: 900" class="text-success">{$yesterday_ayat_count}</span> Sadeeq</p>
                </div>
              </div>
            </div>
            <div class="clearfix"><br></div>
            <h2><a href="#" id="tlb_muhafiz">Show Tehfeez Details</a></h2>
            <div class="row" id="show_muhafiz_table"></div>
          </div>
          {include file="include/qrn_links.tpl"}
        </div>
      </div>
    </form>
  </div>
</div>

<script type="text/javascript" src="{$server_path}js/loader.js"></script>
<script type="text/javascript">
  google.charts.load("current", {
    packages:['corechart']
  });
  google.charts.setOnLoadCallback(drawChart);
  function drawChart() {
    var data = google.visualization.arrayToDataTable([
      ["Date", "Total Ayat"]
        {if $tasmee_records}
          {','}
          {foreach $tasmee_records as $data}
            {[{date('d, M Y',  strtotime($data['timestamp']))}, {$data['sum_total_ayat']}]}
            {if (count($tasmee_records) - 1)}
              {','}
            {/if}
          {/foreach}
        {/if}
    ]);

    var options = {
          colors: ['#337AB7'],
          pointSize: 10,
          pointShape: { type: 'point', rotation: 180 },
          hAxis: {
            title: 'No. of Aayaat listened to'
          },
          vAxis: {
            title: 'Date'
          }
        };
        
    var chart = new google.visualization.AreaChart(document.getElementById("columnchart_values"));
    chart.draw(data, options);
  }
  
  $(window).resize(function(){
    drawChart();
  });
    
  $('#tlb_muhafiz').on("click", function(e){
    e.preventDefault();
    
    $.ajax({
      type: "POST",
      url: "ajax.php",
      data: 'query=get_muhafiz_table&muhafiz_table=muhafiz_table',
      cache: false,
      success: function (response) {
        $('.show_muhafiz_table').show(600);
        
        if (response != '') {
          $('#show_muhafiz_table').html(response);
        } 
      }
    });
  });
</script>

<style>
  p {
    padding: 5px;
  }
</style>
{include file="include/footer.tpl"}