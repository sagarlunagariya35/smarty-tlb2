<style>
  .orange{
    color: #cf4914;
  }
</style>
<div class="container white-bg">
  <div class="col-md-12 col-sm-12">

    <div class="page-title">
      <p style="margin-top:5px;">
        <a href="#" class="active">Istibsaar {$miqaat_istibsaar['year']} H</a></p>
      <h1>Istibsaar {$miqaat_istibsaar['year']} H<span class="alfatemi-text">الاستبصار</span></h1>
    </div>
  </div>
  <div class="clearfix"></div> <!-- do not delete -->
  <form class="forms1 white" name="raza_form" method="post">
    <div class="col-md-3 col-sm-12 hidden-xs">
      <div class="profile-box-static">
        <h3 class="uppercase text-center">Mawaqeet {$miqaat_istibsaar['year']} H</h3>
      </div>
      <div class="profile-box-static-bottom">
        {foreach $mawaqeet as $mqt}
          <a href="{$server_path}istibsaar/{$mqt['id']}/{$mqt['slug']}/{$mqt['year']}/">» {$mqt['miqaat_title']}<hr></a>
        
        {/foreach}

        {assign var="make_pre_active" value=''}
        {assign var="make_in_active" value=''}
        {assign var="make_post_active" value=''}

        {if ($miqaat_istibsaar['show_post_event'] == '1')}
          {$make_post_active = ' active'}
        {elseif ($miqaat_istibsaar['show_in_event'] == '1')}
          {$make_in_active = ' active'}
        {elseif ($miqaat_istibsaar['show_pre_event'] == '1')}
          {$make_pre_active = ' active'}
        {/if}
      </div>
    </div>

    <div class="col-md-8 col-sm-12">
      <div class="static-show-button large-fonts col-xs-12 col-sm-12 pull-left">{$miqaat_istibsaar['miqaat_title']}</div>
      <div class="clearfix"></div>

      <div>
        <ul class="nav nav-tabs" role="tablist">

          {if ($miqaat_istibsaar['show_pre_event'] == '1')}<li role="presentation" class="{$make_pre_active}"><a href="#pre" aria-controls="pre" role="tab" data-toggle="tab">Ohbat</a></li>{/if}

          {if ($miqaat_istibsaar['show_in_event'] == '1')}<li role="presentation" class="{$make_in_active}"><a href="#in" aria-controls="in" role="tab" data-toggle="tab">Talaqqi</a></li>{/if}

          {if ($miqaat_istibsaar['show_post_event'] == '1')}<li role="presentation" class="{$make_post_active}"><a href="#post" aria-controls="post" role="tab" data-toggle="tab">Tizkaar</a></li>{/if}

        </ul>

        {assign var="video_pre" value=''}
        {assign var="video_in" value=''}
        {assign var="video_post" value=''}
        {assign var="audio_pre" value=''}
        {assign var="audio_in" value=''}
        {assign var="audio_post" value=''}
        {assign var="audio_pre2" value=''}
        {assign var="audio_in2" value=''}
        {assign var="audio_post2" value=''}
        {assign var="stat_pre" value=''}
        {assign var="stat_in" value=''}
        {assign var="stat_post" value=''}
        {assign var="ohbat_pre" value=''}
        {assign var="ohbat_in" value=''}
        {assign var="ohbat_post" value=''}
        {assign var="sentences_pre" value=''}
        {assign var="sentences_in" value=''}
        {assign var="sentences_post" value=''}
        {assign var="quiz_pre" value=''}
        {assign var="quiz_in" value=''}
        {assign var="quiz_post" value=''}
        {assign var="course_pre" value=''}
        {assign var="course_in" value=''}
        {assign var="course_post" value=''}
        {assign var="youtube_pre" value=''}
        {assign var="youtube_in" value=''}
        {assign var="youtube_post" value=''}
        
        {if $miqaat_videos}
          {foreach $miqaat_videos as $video}

            {$video_table = '<a href="'. $server_path .'upload/miqaat_upload/video/'. $video.video_url .'"><img src="'. $server_path .'upload/miqaat_upload/video/images/'. $video.video_thumb .'"  title=""></a>'}
            
            {if ($video['event_time'] == 'pre')}
              {$video_pre .= $video_table}
            {elseif ($video['event_time'] == 'in')}
              {$video_in .= $video_table}
            {elseif ($video['event_time'] == 'post')}
              {$video_post .= $video_table}
            {/if}
            
          {/foreach}
        {/if}
        
        {if $miqaat_youtube}
          {foreach $miqaat_youtube as $you_vid}
            
            {if ($you_vid['event_time'] == 'pre')}
              {$youtube_pre .= $you_vid.video_url}
            {elseif ($you_vid['event_time'] == 'in')}
              {$youtube_in .= $you_vid.video_url}
            {elseif ($you_vid['event_time'] == 'post')}
              {$youtube_post .= $you_vid.video_url}
            {/if}
            
          {/foreach}
        {/if}
        
        {if $miqaat_audios}
          {foreach $miqaat_audios as $audio}
            
            {if ($audio['event_time'] == 'pre')}
              {$player_id = 'jquery_jplayer_1' $css_id = 'jp_container_1'}
            {elseif ($audio['event_time'] == 'in')}
              {$player_id = 'jquery_jplayer_2' $css_id = 'jp_container_2'}
            {elseif ($audio['event_time'] == 'post')}
              {$player_id = 'jquery_jplayer_3' $css_id = 'jp_container_3'}
            {/if}
            
            {$audio_table = '<div id="' . $player_id . '" class="jp-jplayer"></div>
            <div id="' . $css_id . '" class="jp-audio" role="application" aria-label="media player">
              <div class="jp-type-playlist">
                <div class="jp-gui jp-interface">
                  <div class="jp-controls">
                    <button class="jp-previous" role="button" tabindex="0">previous</button>
                    <button class="jp-play" role="button" tabindex="0">play</button>
                    <button class="jp-next" role="button" tabindex="0">next</button>
                    <button class="jp-stop" role="button" tabindex="0">stop</button>
                  </div>
                  <div class="jp-progress">
                    <div class="jp-seek-bar">
                      <div class="jp-play-bar"></div>
                    </div>
                  </div>
                  <div class="jp-volume-controls">
                    <button class="jp-mute" role="button" tabindex="0">mute</button>
                    <button class="jp-volume-max" role="button" tabindex="0">max volume</button>
                    <div class="jp-volume-bar">
                      <div class="jp-volume-bar-value"></div>
                    </div>
                  </div>
                  <div class="jp-time-holder">
                    <div class="jp-current-time" role="timer" aria-label="time">&nbsp;</div>
                    <div class="jp-duration" role="timer" aria-label="duration">&nbsp;</div>
                  </div>
                  <div class="jp-toggles">
                    <button class="jp-repeat" role="button" tabindex="0">repeat</button>
                    <button class="jp-shuffle" role="button" tabindex="0">shuffle</button>
                  </div>
                </div>
                <div class="jp-playlist">
                  <ul>
                    <li>&nbsp;</li>
                  </ul>
                </div>
                <div class="jp-no-solution">
                  <span>Update Required</span>
                  To play the media you will need to either update your browser to a recent version or update your <a href="http://get.adobe.com/flashplayer/" target="_blank">Flash plugin</a>.
                </div>
              </div>
            </div>'}
          
            {$audio_table2 = '{
                          title:"' . $audio['audio_url'] . '",
                          mp3: "' . $server_path . 'upload/miqaat_upload/audio/' . $audio['audio_url'] . '",
                        }'}
            
            {if (count($miqaat_audios) - 1)}
              {$audio_table2 .= ','}
            {/if}
                      
            {if ($audio['event_time'] == 'pre')}
              {$audio_pre = $audio_table}
              {$audio_pre2 .= $audio_table2}
            {elseif ($audio['event_time'] == 'in')}
              {$audio_in = $audio_table}
              {$audio_in2 .= $audio_table2}
            {elseif ($audio['event_time'] == 'post')}
              {$audio_post = $audio_table}
              {$audio_post2 .= $audio_table2}
            {/if}
            
          {/foreach}
        {/if}
        
        {if $miqaat_stationaries}
          {foreach $miqaat_stationaries as $stat_files}
            {$file_name = $stat_files['name']}
            
            {$stat_category = explode(',', $stat_files['category'])}
            {foreach ($stat_category as $st_cat)}
              {$stat_categories[] = $st_cat}
            {/foreach}
            
            {$stat_cls = implode(' ', $stat_category)}
            {$logo_image = ($stat_files['logo_image']) ? $stat_files['logo_image'] : 'images/waaz_pdf.png'}
            {$stat_table = '<div class="col-md-4 all '. $stat_cls .'">
              <a href="http://docs.google.com/gview?embedded=true&url='.$server_path.'upload/miqaat_upload/stationary/' . $stat_files['stationary_url'] . '" target="_blank">
                  <div class="istibsaar-boxes text-center">
                    <img src="' . $server_path . $logo_image . '" class="img-responsive" style="max-height: 100px; margin: 0 auto" >
                    <div class="istibsaar-boxes-bottom">
                      <p class="text-center">' . $file_name . '</p>
                    </div>
                  </div>
              </a>
                </div>'}
                
            {if ($stat_files['event_time'] == 'pre')}
              {$stat_pre .= $stat_table}
            {elseif ($stat_files['event_time'] == 'in')}
              {$stat_in .= $stat_table}
            {elseif ($stat_files['event_time'] == 'post')}
              {$stat_post .= $stat_table}
            {/if}
          {/foreach}
          {$stationary_categories = array_unique($stat_categories)}         
        {/if}
        
        {if $miqaat_ohbats}
          {foreach $miqaat_ohbats as $mo}
            
            {$ohbat_table = '<div class="col-md-4">
                <a href="' . $server_path . 'daily_sentences_ar.php?mid=' . $miqat_id . '">
              <div class="istibsaar-boxes text-center">
                <img src="' . $server_path . 'images/beer.png" class="img-responsive" style="max-height: 100px; margin: 0 auto" >
                <div class="istibsaar-boxes-bottom">
                  <p class="text-center">Ohbat Kalemaat Nooraniyah</p>
                </div>
              </div>
                </a>
            </div>'}
                
            {if ($mo['event_time'] == 'pre')}
              {$ohbat_pre = $ohbat_table}
            {elseif ($mo['event_time'] == 'in')}
              {$ohbat_in = $ohbat_table}
            {elseif ($mo['event_time'] == 'post')}
              {$ohbat_post = $ohbat_table}
            {/if}
          {/foreach}
        {/if}
        
        {if $miqaat_id == 5}
            {$tasavvuraat_in = '<div class="col-md-4">
                <a href="' . $server_path . 'tasavvuraat_araz.php">
              <div class="istibsaar-boxes text-center">
                <i class="fa fa-microphone  fa-5x dark-blue"></i>
                <div class="istibsaar-boxes-bottom">
                  <p class="text-center">Tasawwuraat Araz</p>
                </div>
              </div>
                </a>
            </div>'}
        {/if}
        
        {if $miqaat_sentences}
          {foreach $miqaat_sentences as $ms}
            
            {$sentences_table = '<div class="col-md-4">
                <a href="' . $server_path . 'istibsaar/ashara-mubarakah/">
              <div class="istibsaar-boxes text-center">
                <img src="' . $server_path . 'images/beer.png" class="img-responsive" style="max-height: 100px; margin: 0 auto" >
                <div class="istibsaar-boxes-bottom">
                  <p class="text-center">Sentences</p>
                </div>
              </div>
                </a>
            </div>'}
                
            {if ($ms['event_time'] == 'pre')}
              {$sentences_pre = $sentences_table}
            {elseif ($ms['event_time'] == 'in')}
              {$sentences_in = $sentences_table}
            {elseif ($ms['event_time'] == 'post')}
              {$sentences_post = $sentences_table}
            {/if}
          {/foreach}
        {/if}
        
        {if $miqaat_quizes}
          {foreach $miqaat_quizes as $mq}
            
            {$quiz_table = '<div class="col-md-4">
                <a href="quiz_group_list.php">
              <div class="istibsaar-boxes text-center">
                <i class="fa fa-list-alt   fa-5x dark-blue"></i>
                <div class="istibsaar-boxes-bottom">
                  <p class="text-center">Take Quiz</p>
                </div>
              </div>
                </a>
            </div>'}
                
            {if ($mq['event_time'] == 'pre')}
              {$quiz_pre = $quiz_table}
            {elseif ($mq['event_time'] == 'in')}
              {$quiz_in = $quiz_table}
            {elseif ($mq['event_time'] == 'post')}
              {$quiz_post = $quiz_table}
            {/if}
          {/foreach}
        {/if}
        
        {if $miqaat_courses}
          {assign var="course_table" value=''}
          {assign var="first_child" value=TRUE}
          {$tmp_quiz = new Quiz()}
          {foreach $miqaat_courses as $mc}
            {$course_id = $mc['course_id']}
            {$user_its = $_SESSION['user_its']}
            
            {$course_questions = $tmp_quiz->get_courses_question_by_course_id($course_id)}
            {$user_answers = $tmp_quiz->get_user_answers_for_course($course_id, $user_its)}
            
            {if $user_answers}
              {assign var="correct_ans" value=0}
              {foreach $user_answers as $ua}
                {($ua['correct'] == 1) ? $correct_ans++ : ''}
              {/foreach}
              
              {$user_quiz_interaction = '<p class="text-center" style="padding-left: 25px;"><span class="pull-left">' . $mc['course_title'] . '</span><span class="pull-right">' . $correct_ans . ' / ' . count($course_questions) . '</span><div class="clearfix"></div></p>'}
            {else}
              
              {$user_quiz_interaction = '<p class="text-center">' . $mc['course_title'] . '</p>'}
            {/if}
            
            {if $mc['active'] == 1}
              {$color = 'dark-blue'}
              {$img = $server_path . 'assets/img/icon_tizkaar_act.png'}
            
              {if $first_child}
                {$color = 'orange'}
                {$first_child = FALSE}
              {/if}
            
              {$course_table = '<div class="col-md-4">
                  <a href="' . $server_path . 'take_courses.php?course_id=' . $mc['course_id'] . '">
                <div class="istibsaar-boxes text-center">
                  <img class="img-responsive" style="max-height: 100px; margin: 0 auto" src="' . $img . '">
                  <div class="istibsaar-boxes-bottom">' . $user_quiz_interaction . '<p class="text-center black_font">' . $mc['description'] . '</p>
                  </div>
                </div>
                  </a>
              </div>'}

              {if ($mc['event_time'] == 'pre')}
                {$course_pre = $course_table}
              {elseif ($mc['event_time'] == 'in')}
                {$course_in = $course_table}
              {elseif ($mc['event_time'] == 'post')}
                {$course_post = $course_table}
              {/if}
            {/if}
          {/foreach}
        {/if}

        {$no_videos = '<p style="margin-left:10px;">No Videos</p>'}
        {$no_audios = '<p style="margin-left:10px;">No Audios</p>'}
        {$no_stationaries = '<p style="margin-left:10px;">No Stationaries</p>'}
        
        <div class="tab-content">
          {if $miqaat_istibsaar['show_pre_event'] == '1'}
            <div role="tabpanel" class="tab-pane {$make_pre_active}" id="pre">
              
              {if $stat_pre}
                <div class="static-show-button2 large-fonts col-xs-12 col-sm-12 pull-left">Downloads</div><div class="clearfix"></div>
                {$sp = 1}
                <div class="black_font pull-right">
                <a href="#" class="black_font" id="all"> All </a> |
                {foreach $stationary_categories as $stat_cat}
                  <a href="#" class="black_font" id={$stat_cat}> {$stat_cat} </a>
                  {if (count($stationary_categories) > $sp++)}
                    {' | '}
                  {/if}
                {/foreach}
                </div>
                <div class="clearfix"><br></div>
                  {$stat_pre}
                <div class="clearfix"></div>
              {/if}

              {if $video_pre}
                 <div class="static-show-button2 large-fonts col-xs-12 col-sm-12 pull-left">Videos</div><div class="clearfix"></div>
                 <div style="display:none;" class="html5gallery" data-skin="horizontal" data-width="290" data-height="200" data-resizemode="fill">{$video_pre}</div>
                 <div class="clearfix"></div>
              {/if}
              
              {if $youtube_pre}
                 <div class="static-show-button2 large-fonts col-xs-12 col-sm-12 pull-left">Videos</iv><div class="clearfix"></div>
                  {$youtube_pre}
                 <div class="clearfix"></div>
              {/if}
              
              

              {if ($ohbat_pre || $sentences_pre || $quiz_pre || $course_pre)}
                <div class="static-show-button2 large-fonts col-xs-12 col-sm-12 pull-left">Interaction</div><div class="clearfix"></div>

                {if $ohbat_pre}
                  {$ohbat_pre}
                {/if}
                {if $sentences_pre}
                  {$sentences_pre}
                {/if}
                {if $quiz_pre}
                  {$quiz_pre}
                {/if}
                {if $course_pre}
                  {$course_pre}
                {/if}
                <div class="clearfix"></div>
              {/if}

              {if $audio_pre}
                <div class="static-show-button2 large-fonts col-xs-12 col-sm-12 pull-left">Audios</div><div class="clearfix"></div>
                  {$audio_pre}
                <div class="clearfix"></div>
              {/if}
            </div>
          {/if}

          {if $miqaat_istibsaar['show_in_event'] == '1'}
            <div role="tabpanel" class="tab-pane {$make_in_active}" id="in">
              {if $stat_in}
                <div class="static-show-button2 large-fonts col-xs-12 col-sm-12 pull-left">Downloads</div><div class="clearfix"></div>
                {$sn = 1}
                <div class="black_font pull-right">
                <a href="#" class="black_font" id="all"> All </a> | 
                {foreach $stationary_categories as $stat_cat}
                  <a href="#" class="black_font" id={$stat_cat}> {$stat_cat} </a>
                  {if (count($stationary_categories) > $sn++)}
                    {' | '}
                  {/if}
                {/foreach}
                </div>
                <div class="clearfix"><br></div>
                  {$stat_in}
                <div class="clearfix"></div>
              {/if}

              {if $video_in}
                 <div class="static-show-button2 large-fonts col-xs-12 col-sm-12 pull-left">Videos</div><div class="clearfix"></div>
                 <div style="display:none;" class="html5gallery" data-skin="horizontal" data-width="290" data-height="200" data-resizemode="fill">{$video_in}</div>
                 <div class="clearfix"></div>
              {/if}
              
              {if $youtube_in}
                 <div class="static-show-button2 large-fonts col-xs-12 col-sm-12 pull-left">Videos</iv><div class="clearfix"></div>
                  {$youtube_in}
                 <div class="clearfix"></div>
              {/if}

              {if ($ohbat_in || $sentences_in || $quiz_in || $course_in)}
                <div class="static-show-button2 large-fonts col-xs-12 col-sm-12 pull-left">Interaction</div><div class="clearfix"></div>

                {if $ohbat_in}
                  {$ohbat_in}
                {/if}
                {if $sentences_in}
                  {$sentences_in}
                {/if}
                {if $quiz_in}
                  {$quiz_in}
                {/if}
                {if $course_in}
                  {$course_in}
                {/if}
                <div class="clearfix"></div>
              {/if}

              {if $audio_in}
                <div class="static-show-button2 large-fonts col-xs-12 col-sm-12 pull-left">Audios</div><div class="clearfix"></div>
                  {$audio_in}
                <div class="clearfix"></div>
              {/if}              
            </div>
          {/if}
          
          {if $miqaat_istibsaar['show_post_event'] == '1'}
            <div role="tabpanel" class="tab-pane {$make_post_active}" id="post">
              {if $stat_post}
                <div class="static-show-button2 large-fonts col-xs-12 col-sm-12 pull-left">Downloads</div><div class="clearfix"></div>
                {$sn = 1}
                <div class="black_font pull-right">
                <a href="#" class="black_font" id="all"> All </a> | 
                {foreach $stationary_categories as $stat_cat}
                  <a href="#" class="black_font" id={$stat_cat}> {$stat_cat} </a>
                  {if (count($stationary_categories) > $sn++)}
                    {' | '}
                  {/if}
                {/foreach}
                </div>
                <div class="clearfix"><br></div>
                  {$stat_in}
                <div class="clearfix"></div>
              {/if}

              {if $video_post}
                 <div class="static-show-button2 large-fonts col-xs-12 col-sm-12 pull-left">Videos</div><div class="clearfix"></div>
                 <div style="display:none;" class="html5gallery" data-skin="horizontal" data-width="290" data-height="200" data-resizemode="fill">{$video_post}</div>
                 <div class="clearfix"></div>
              {/if}
              
              {if $youtube_post}
                 <div class="static-show-button2 large-fonts col-xs-12 col-sm-12 pull-left">Videos</iv><div class="clearfix"></div>
                  {$youtube_post}
                 <div class="clearfix"></div>
              {/if}

              {if ($ohbat_post || $sentences_post || $quiz_post || $course_post)}
                <div class="static-show-button2 large-fonts col-xs-12 col-sm-12 pull-left">Interaction</div><div class="clearfix"></div>

                {if $ohbat_post}
                  {$ohbat_post}
                {/if}
                {if $sentences_post}
                  {$sentences_post}
                {/if}
                {if $quiz_post}
                  {$quiz_post}
                {/if}
                {if $course_post}
                  {$course_post}
                {/if}
                <div class="clearfix"></div>
              {/if}

              {if $audio_post}
                <div class="static-show-button2 large-fonts col-xs-12 col-sm-12 pull-left">Audios</div><div class="clearfix"></div>
                  {$audio_post}
                <div class="clearfix"></div>
              {/if}              
            </div>
          {/if}
        </div>
      </div>
    </div>
    <div class="clearfix"></div>
  </form>
</div>
 
{include file="include/js_block.tpl"}

<script>
  $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
    e.target // newly activated tab
    e.relatedTarget // previous active tab
  })
</script>

<script type="text/javascript" src="{$server_path}html5gallery/html5gallery.js"></script>

<link href="{$server_path}dist/skin/blue.monday/css/jplayer.blue.monday.min.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="{$server_path}dist/jplayer/jquery.jplayer.min.js"></script>
<script type="text/javascript" src="{$server_path}dist/add-on/jplayer.playlist.min.js"></script>

<script type="text/javascript">
  $(document).ready(function () {
  {if $audio_pre2}
      new jPlayerPlaylist({
        jPlayer: "#jquery_jplayer_1",
        cssSelectorAncestor: "#jp_container_1"
      }, [
      {if $audio_pre2}
        {$audio_pre2}
      {/if}
      ], {
        swfPath: "../../dist/jplayer",
        supplied: "oga, mp3",
        wmode: "window",
        useStateClassSkin: true,
        autoBlur: false,
        smoothPlayBar: true,
        keyEnabled: true
      });
  {/if}
  
  {if $audio_in2}
      new jPlayerPlaylist({
        jPlayer: "#jquery_jplayer_2",
        cssSelectorAncestor: "#jp_container_2"
      }, [
      {if $audio_in2}
        {$audio_in2}
      {/if}
      ], {
        swfPath: "../../dist/jplayer",
        supplied: "oga, mp3",
        wmode: "window",
        useStateClassSkin: true,
        autoBlur: false,
        smoothPlayBar: true,
        keyEnabled: true
      });
  {/if}
  
  {if $audio_post2}
      new jPlayerPlaylist({
        jPlayer: "#jquery_jplayer_3",
        cssSelectorAncestor: "#jp_container_3"
      }, [
      {if $audio_post2}
        {$audio_post2}
      {/if}
      ], {
        swfPath: "../../dist/jplayer",
        supplied: "oga, mp3",
        wmode: "window",
        useStateClassSkin: true,
        autoBlur: false,
        smoothPlayBar: true,
        keyEnabled: true
      });

  {/if}
  
    {if $stationary_categories}
      $('#all').click(function(e){
        e.preventDefault();
        $('.all').show(300);
      });
      {foreach $stationary_categories as $cnt}
        $('#{$cnt}').click(function(e){
          e.preventDefault();
          $('.all').hide(300);
          $('.{$cnt}').show(300);
        });
      {/foreach}
    {/if}
  });
</script>

<style>
  .miqaat_stat:hover{
    background-color: #549BC7;
  }
  .black_font{
    color:#000 !important;
  }
</style>
{include file="include/footer.tpl"}