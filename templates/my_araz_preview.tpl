<style>
  .skip{
  display:block;
  max-width:300px;
  -webkit-transition: 0.4s;
  -moz-transition: 0.4s;
  -o-transition: 0.4s;
  transition: 0.4s;
  border: none;
  background-color: #155485;
  height: auto;
  line-height:20px;
  margin:0px 5px 20px;
  padding:10px;
  outline: none;
  color:#FFF;
  text-transform:uppercase;
  text-align:center;
  font-weight:bold;
  font-size:12px;
  float:right;
  text-decoration: underline none;
}
</style>

<div class="container white-bg">
  <div class="col-md-12 col-sm-12">

    <div class="page-title">
      <p style="margin-top:5px;">
        <a href="#" class="active">Araz Preview</a>
      </p>
      
      <h1>Araz Preview - Track ID: {$araz}<span class="alfatemi-text">عرض</span></h1>
    </div>
  </div>
  <div class="clearfix"></div> <!-- do not delete -->
  <div class="col-md-12 col-sm-12">
    <div class="page">
      <form class="forms1 white" action="" method="post">
        
        <!--a class="skip" href="my_araz_preview_english.php?araz=<?php echo $araz; ?>" class="btn btn-color-grey-light">For English Click Here</a-->
        
        <div class="clearfix"></div> <!-- do not delete -->
        
        <div class="block top-mgn10"></div>
        <div class="blue-box1">
          <h3 class="bordered">
            <span class="lsd">
                <p class="lsd large-fonts text-center" dir="rtl">غب السجدات العبودية</p>
                <p class="lsd large-fonts text-center" dir="rtl">في الحضرة العالية الامامية اشرق الله انوارها</p>
                <p class="lsd large-fonts text-center" dir="rtl">عرض كرنار: {$user->get_full_name_ar()}</p>
                <p class="lsd large-fonts text-center" dir="rtl">موضع: {$user->get_city()}</p>
                <p class="lsd large-fonts text-center" dir="rtl">ادبًـا عرض كه</p>
            </span>
          </h3>
          <div class="clearfix"></div> <!-- do not delete -->
        </div>
        
        {include file="my_araz_institution_preview.tpl"}
        
        <div class="blue-box1">
          <h3 class="bordered">
            <span class="lsd">
              
                {if ($marhala_id == '1')}
                  <p class="lsd large-fonts text-center" dir="rtl">تعليم ني ابتداء واسطسس مع الدعاء المبارك رزا مبارك فضل فرماوا ادبًا عرض؛</p>
                {else}
                  {if (count($institute_data) > 1)}
                  <p class="lsd large-fonts text-center" dir="rtl">ما تعليم حاصل كرواني استرشادًا عرض ححهسس</p>
                  {else}
                   <p class="lsd large-fonts text-center" dir="rtl">مع الدعاء المبارك رزا مبارك فضل فرماوا ادبًـا عرض ححهسس</p>
                  {/if}
                {/if}
                <p class="lsd large-fonts text-center" dir="rtl">والسجدات</p>
            </span>
          </h3>
          <div class="clearfix"></div> <!-- do not delete -->
        </div>
      </form>
      <div class="clearfix"></div> <!-- do not delete -->
    </div>
  </div>
</div>
{include file="include/js_block.tpl"}
{include file="include/footer.tpl"}