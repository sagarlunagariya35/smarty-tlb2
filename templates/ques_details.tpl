<style>
  .skip{
    display:block;
    max-width:300px;
    -webkit-transition: 0.4s;
    -moz-transition: 0.4s;
    -o-transition: 0.4s;
    transition: 0.4s;
    border: none;
    background-color: #155485;
    height: auto;
    line-height:20px;
    margin:0px auto 20px;
    padding:10px;
    outline: none;
    color:#FFF;
    text-transform:uppercase;
    text-align:center;
    font-weight:bold;
    font-size:18px;
    float:right;
    text-decoration: underline none;
  }
  
  .set{
    background-color: #549BC7 !important;
  }
  
  .skip:hover{
    background-color: #549BC7;
  }
  
  #load {
    color: #000 !important;
    margin: 20px auto 0;
    float: right;
    text-align: center;
  }
</style>

<div class="container white-bg">
  <div class="col-md-12 col-sm-12">

    <div class="page-title">
      <p style="margin-top:5px;">
        <a href="{$server_path}marahil/">My Araz for education</a> 
        {if (in_array($marhala_id, $marhala_school_array))}
          / <a href="{$server_path}marhala-{$marhala_id}/{get_marhala_slug($marhala_id)}/school">{$panel_heading} ( School )</a> 
        {else}
          / <a href="{$server_path}marhala-{$marhala_id}/{get_marhala_slug($marhala_id)}/select-institute/">{$panel_heading} (Select Institute)</a>
        {/if}
        / <a href="#" class="active">{$panel_heading}</a></p>

      <h1>{$panel_heading}<span class="alfatemi-text">{$page_heading_ar}</span></h1>
    </div>
  </div>
  {if (isset($success_message) OR isset($error_message))}
    <div class="row">
      {include file="include/message.tpl"}
    </div>
  {/if}
  <div class="clearfix"></div> <!-- do not delete -->

  <div class="col-md-12 col-sm-12">
    <div class="page">
      <span class="block vmgn20"></span>
      <div class="clearfix"></div> <!-- do not delete -->

      <h4><span class="alfatemi-text">تماري تعليم ني عرض كروا ثثظظلسس عزائم ثثيش كروا هوئي توئي اْفارم ثثورو كرو</span></h4><br>
      
      <div class="clearfix"></div> <!-- do not delete -->
      <div class="col-md-3 pull-right hidden-xs"></div>
      
      <div class="col-md-3 col-xs-12 pull-right text-center"><br>
        <button type="button" name="for_arabic" id="for_arabic" class="btn btn-block skip lsd set" dir="rtl" value="1" {if ($page == 'ques_ar')} checked {/if}>سان الدعوة واسطسس يظظاطط click كروو</button>
      </div>
      
      <div class="col-md-3 col-xs-12 pull-right text-center"><br>
        <button type="button" name="for_arabic" id="for_english" class="btn btn-block skip lsd" onClick="MM_goToURL('parent','{$server_path}marhala-{$marhala_id}/{get_marhala_slug($marhala_id)}/ques_english/');return document.MM_returnValue">For English Click Here</button>
      </div><br><br>
      <div class="clearfix"></div>
      
      <form name="ques_form" class="forms1 white" action="{$server_path}marhala-{$marhala_id}/{get_marhala_slug($marhala_id)}/ques/" method="post" onsubmit="return(validate());">

  {assign var="i" value=0}
  {if $ques_preffered}
    {foreach $ques_preffered as $q}

      {if (isset($araz_data[$i]['answer']))}
        {if ($araz_data[$i]['answer'] == '1')}
          {$check1 = 'checked'}
        {elseif ($araz_data[$i]['answer'] == '2')}
          {$check2 = 'checked'}
        {elseif ($araz_data[$i]['answer'] == '3')}
          {$check3 = 'checked'}
        {elseif ($araz_data[$i]['answer'] == '4')}
          {$check4 = 'checked'}
        {/if}
      {/if}
      {assign var="i" value=$i+1}
        <div class="blue-box1 rtl"> <!-- Add class rtl -->
          <h3><span class="inline-block lh3 arb_ques">{$i}. {$q['question_preview']}</span></h3>
        </div>
        <div class="clearfix"></div> <!-- do not delete -->
        <div class="col-md-3 col-sm-12 pull-right">
          <div class="radiobuttons-holder1 rtl"> <!-- Add Class .rtl -->
            <input type="radio" name="q_{$q['id']}" value="1" id="radio1{$q['id']}" class="css-radiobutton2" {$check1} />
            <label for="radio1{$q['id']}" style="color: #000;" class="css-label2 radGroup3 lsd">ميں عزم كروطط ححهوطط</label>
          </div>
        </div>
        <div class="clearfix"></div>
    {/foreach}
  {/if}
        
  <p style="color:#444444;">We recommend you to complete the remaining questionnaire pertaining your educational journey. <a data-toggle="collapse" href="#collapseExample" aria-expanded="false" aria-controls="collapseExample" style="color:#155485;">Show / Hide</a></p><br>
  
  <div class="collapse" id="collapseExample">      
    {if $ques}
      {foreach $ques as $q}

        {if (isset($araz_data[$i]['answer']))}
           {if ($araz_data[$i]['answer'] == '1')}
             {$check1 = 'checked'}
           {elseif ($araz_data[$i]['answer'] == '2')}
             {$check2 = 'checked'}
           {elseif ($araz_data[$i]['answer'] == '3')}
             {$check3 = 'checked'}
           {elseif ($araz_data[$i]['answer'] == '4')}
             {$check4 = 'checked'}
            {/if}
          {/if}
          {assign var="i" value=$i+1}
          <div class="blue-box1 rtl"> <!-- Add class rtl -->
            <h3><span class="inline-block lh3 arb_ques">{$i}. {$q['question_preview']}</span></h3>
          </div>
          <div class="clearfix"></div> <!-- do not delete -->
          <div class="col-md-3 col-sm-12 pull-right">
            <div class="radiobuttons-holder1 rtl"> <!-- Add Class .rtl -->
              <input type="radio" name="q_{$q['id']}" value="1" id="radio1{$q['id']}" class="css-radiobutton2" {$check1} />
              <label for="radio1{$q['id']}" style="color: #000;" class="css-label2 radGroup3 lsd">ميں عزم كروطط ححهوطط</label>
            </div>
          </div>
          <div class="clearfix"></div>
      {/foreach}
  	{/if}
  </div>
  {if (!$ques_preffered && !$ques)}
      <div class="blue-box1 rtl"> <!-- Add class rtl -->
        <h3><span class="inline-block bigger1 lh1 w500 left-mgn20">1</span>
          <span class="inline-block lh3">&#1587;&#1608;&#1601; &#1578;&#1603;&#1608;&#1606; &#1602;&#1575;&#1583;&#1585;&#1577; &#1593;&#1604;&#1609; &#1581;&#1590;&#1608;&#1585; waaz &#1575;&#1604;&#1603;&#1575;&#1605;&#1604; &#1605;&#1606; &#1603;&#1604; &#1578;&#1587;&#1593;&#1577; &#1571;&#1610;&#1575;&#1605; &#1605;&#1606; &#1575;&#1588;&#1575;&#1585;&#1575; &#1605;&#1576;&#1575;&#1585;&#1603;&#1577; &#1591;&#1601;&#1604;&#1603;&#1567;</span>
        </h3>
      </div>
      <div class="clearfix"></div> <!-- do not delete -->
      <div class="col-md-3 col-sm-12">
        <div class="radiobuttons-holder1 rtl"> <!-- Add Class .rtl -->
          <input type="radio" name="q_1" value="1" id="radio11" class="css-radiobutton1" />
          <label for="radio11" style="color: #000;" class="css-label1 radGroup3 lsd">ميں عزم كروطط ححهوطط</label>
        </div>
      </div>
      <div class="clearfix"></div>
  {/if}

        <!--div class="blue-box1">
          <h3 class="bordered">
            <span class="lsd">
              <div class="show lsd large-fonts text-center" dir="rtl"><?php echo $araz_notes[0]['note_ar']; ?></div>
            </span>
          </h3>
          <div class="clearfix"></div> <!-- do not delete -->
        <!--/div-->

        <div class="clearfix"></div> <!-- do not delete -->
        <input type="submit" class="submit1 proceed col-md-3 col-xs-12" name="submit" value="Proceed with Araz" onclick="return ray.ajax()"/>
        <div id="load" style="display: none;"><i class="fa fa-spinner fa-spin fa-3x"></i></div>
        <input type="submit" class="submit1 col-md-3 col-xs-12" name="save_araz"  value="Save Araz" style="margin-right: 5px;" />
        <a class="pull-left col-md-3 col-xs-12 start-again" onclick="start_again();">Start Over Again</a>
        <span class="page-steps">Step : 4 out of 5</span>
        <div class="clearfix"></div> <!-- do not delete -->
      </form>
      <div class="clearfix"></div> <!-- do not delete -->
    </div>
  </div>
</div>
  
{include file="include/js_block.tpl"}

<style>

  .morectnt span {
    display: none;
  }
  .showmoretxt {
    text-decoration: none;
  }
</style>

<script type="text/javascript">
// Form validation code will come here.
  function validate()
  {
    {foreach $ques as $q}
      /*if ($('input[name=q_<?php echo $q['id']; ?>]:checked').length <= 0)
      {
        alert("Please Select any Answer!");
        return false;
      }*/
    {/foreach}
  }

  var ray = {
    ajax: function(st)
    {
      this.show('load');
      $('.proceed').hide();
    },
    show: function(el)
    {
      this.getID(el).style.display = '';
    },
    getID: function(el)
    {
      return document.getElementById(el);
    }
  }
</script>

<script>
  $(function() {
    var showTotalChar = 200, showChar = "Read More", hideChar = "Hide";
    $('.show').each(function() {
      var content = $(this).text();
      if (content.length > showTotalChar) {
        var con = content.substr(0, showTotalChar);
        var hcon = content.substr(showTotalChar, content.length - showTotalChar);
        var txt = con + '<span class="dots">...</span><span class="morectnt"><span>' + hcon + '</span>&nbsp;&nbsp;<a href="" class="showmoretxt">' + showChar + '</a></span>';
        $(this).html(txt);
      }
    });
    $(".showmoretxt").click(function() {
      if ($(this).hasClass("sample")) {
        $(this).removeClass("sample");
        $(this).text(showChar);
      } else {
        $(this).addClass("sample");
        $(this).text(hideChar);
      }
      $(this).parent().prev().toggle();
      $(this).prev().toggle();
      return false;
    });
  });

  function MM_goToURL() { //v3.0
    var i, args=MM_goToURL.arguments; document.MM_returnValue = false;
    for (i=0; i<(args.length-1); i+=2) eval(args[i]+".location='"+args[i+1]+"'");
  }
  
  function start_again() {
      var ask = window.confirm("Are you sure you want to start again?");
      if (ask) {
          document.location.href = "{$server_path}start_over_again.php";
      }
  }

</script>
{include file="include/footer.tpl"}