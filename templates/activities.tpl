<div class="container white-bg">
  <div class="col-md-12 col-sm-12 bot-mgn20">
    <div class="page-title">
      <p style="margin-top:5px;"><a href="#">Home</a> / <a href="#">{$year} {$article.article_title}</a></p>
      <h1>Gallery - {$year} {$article.article_title}</h1>
    </div>
  </div>
  <div class="clearfix"></div> <!-- do not delete -->

  <div class="col-md-3 col-sm-12 hidden-xs">
    <div class="profile-box-static">
      <h3 class="uppercase text-center">Activities - {$year}</h3>
    </div>
    <div class="profile-box-static-bottom">
      <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
        
        {if $all_categories}
          {counter start=0 print=false}
          {foreach $all_categories as $ac}
            {$cat_articles = get_article_by_submenu('article', $menu_id[0]['id'], $ac.category)}
              <div class="panel panel-default" onclick="get_articles('{$ac.category}')">
                <div class="panel-heading" role="tab" id="headingOne">
                  <h4 class="panel-title">
                    <a role="button" data-toggle="collapse" data-parent="#accordion" href="#{counter}" aria-expanded="true" aria-controls="collapseOne">
                      {$ac.category}
                    </a>
                  </h4>
                </div>
                <div id="{counter}" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
                  <div class="panel-body">
                    
                      {if $cat_articles}
                        {foreach $cat_articles as $cat_a}
                          <a href="{$server_path}activities/{$year}/{$cat_a.slug}/{$cat_a.id}/">» {$cat_a.article_title}<hr></a>
                        {/foreach}
                      {/if}
                  </div>
                </div>
              </div>
          {/foreach}
        {/if}              
      </div>
    </div>
  </div>


  
  {if $article}
    <div class="col-md-8 col-sm-12">
      <h2><span class="label label-primary lsd">{$year} {$article.article_title}</span></h2>
      <br>
      <span>
        {$article.text}
      </span><br>

      {if $videos}
        <br><h2><span class="label label-success">Videos - {$year} {$article.article_title}</span></h2><br>

        <div style="display:none;" class="html5gallery" data-skin="horizontal" data-width="480" data-height="272" data-resizemode="fill">
          
          {foreach $videos as $video}
            <a href="{$server_path}upload/video/{$video.video_url}"><img src="
              {if $video.video_thumb != ''}
                {$server_path}upload/articles/video/images/{$video.video_thumb}
              {else}
                 {$server_path}images/flash-logo.png
              {/if}
              "  title=""></a>
          {/foreach}

        </div>
      {/if}
        
      {if $audios}
        <br><h2><span class="label label-danger">Audios - {$year} {$article.article_title}</span></h2><br>

        <div id="jquery_jplayer_1" class="jp-jplayer"></div>
        <div id="jp_container_1" class="jp-audio" role="application" aria-label="media player">
          <div class="jp-type-playlist">
            <div class="jp-gui jp-interface">
              <div class="jp-controls">
                <button class="jp-previous" role="button" tabindex="0">previous</button>
                <button class="jp-play" role="button" tabindex="0">play</button>
                <button class="jp-next" role="button" tabindex="0">next</button>
                <button class="jp-stop" role="button" tabindex="0">stop</button>
              </div>
              <div class="jp-progress">
                <div class="jp-seek-bar">
                  <div class="jp-play-bar"></div>
                </div>
              </div>
              <div class="jp-volume-controls">
                <button class="jp-mute" role="button" tabindex="0">mute</button>
                <button class="jp-volume-max" role="button" tabindex="0">max volume</button>
                <div class="jp-volume-bar">
                  <div class="jp-volume-bar-value"></div>
                </div>
              </div>
              <div class="jp-time-holder">
                <div class="jp-current-time" role="timer" aria-label="time">&nbsp;</div>
                <div class="jp-duration" role="timer" aria-label="duration">&nbsp;</div>
              </div>
              <div class="jp-toggles">
                <button class="jp-repeat" role="button" tabindex="0">repeat</button>
                <button class="jp-shuffle" role="button" tabindex="0">shuffle</button>
              </div>
            </div>
            <div class="jp-playlist">
              <ul>
                <li>&nbsp;</li>
              </ul>
            </div>
            <div class="jp-no-solution">
              <span>Update Required</span>
              To play the media you will need to either update your browser to a recent version or update your <a href="http://get.adobe.com/flashplayer/" target="_blank">Flash plugin</a>.
            </div>
          </div>
        </div>
      {/if}

      {if $images}
        <br><h2><span class="label label-info">Images - {$year} {$article.article_title}</span></h2><br>
        <ul class="gallery">
          {foreach $images as $image}
            <li><a href="{$server_path}upload/articles/image/{$image.image_url}" rel="prettyPhoto[gallery1]"><img src="{$server_path}upload/articles/image/{$image.image_url}" alt=""></a></li>
          {/foreach}
        </ul>
      {/if}
      <br>
    </div>
  {else}
    <div class="col-md-8 col-sm-12">
      {foreach $all_articles as $art}
        {$img_art = get_first_image_from_article($art.id)}
        
        <a href="{$server_path}activities/{$year}/{$art.slug}/{$art.id}/">
          <div class="col-sm-3 col-xs-12 boxes-{$art.category}">
            <div class="istibsaar-boxes text-center">
              <img src="{$server_path}upload/articles/image/{$img_art.image_url}" class="img-responsive img-mts" style="border: 1px solid #09386c">
              <div>
                <p class="text-center">{$art.article_title}</p>
                <p>&nbsp;</p>
                <p class="text-center" style="color:#000;">{$art.desc}</p>
              </div>
            </div>
          </div>
        </a>
      {/foreach}
    </div>
  {/if}
</div>

{include file="include/js_block.tpl"}

<script type="text/javascript" src="{$server_path}html5gallery/html5gallery.js"></script>

<link href="{$server_path}dist/skin/blue.monday/css/jplayer.blue.monday.min.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="{$server_path}dist/jplayer/jquery.jplayer.min.js"></script>
<script type="text/javascript" src="{$server_path}dist/add-on/jplayer.playlist.min.js"></script>

<script type="text/javascript">
//<![CDATA[
  $(document).ready(function () {

    new jPlayerPlaylist({
    jPlayer: "#jquery_jplayer_1",
            cssSelectorAncestor: "#jp_container_1"
    }, [

    {if $audios}
      {foreach $audios as $audio}
            {
            title:"{$audio.audio_url}",
                    mp3: "{$server_path}upload/articles/audio/{$audio.audio_url}",
            }
        {if (count($audios) - 1)}
          {','}
        {/if}
      {/foreach}
    {/if}

    ], {
    swfPath: "../../dist/jplayer",
            supplied: "oga, mp3",
            wmode: "window",
            useStateClassSkin: true,
            autoBlur: false,
            smoothPlayBar: true,
            keyEnabled: true
    });
  });
//]]>
</script>

<script type="text/javascript" charset="utf-8">
  $(document).ready(function () {
    $("area[rel^='prettyPhoto']").prettyPhoto();

    $(".gallery:first a[rel^='prettyPhoto']").prettyPhoto({
      animation_speed: 'normal', theme: 'light_square', slideshow: 5000, autoplay_slideshow: true
    });
    
    $(".gallery:gt(0) a[rel^='prettyPhoto']").prettyPhoto({
      animation_speed: 'normal', slideshow: 10000, hideflash: true
    });

    $("#custom_content a[rel^='prettyPhoto']:first").prettyPhoto({
      custom_markup: '<div id="map_canvas" style="width:260px; height:265px"></div>',
      changepicturecallback: function () {
        initialize();
      }
    });

    $("#custom_content a[rel^='prettyPhoto']:last").prettyPhoto({
      custom_markup: '<div id="bsap_1259344" class="bsarocks bsap_d49a0984d0f377271ccbf01a33f2b6d6"></div><div id="bsap_1237859" class="bsarocks bsap_d49a0984d0f377271ccbf01a33f2b6d6" style="height:260px"></div><div id="bsap_1251710" class="bsarocks bsap_d49a0984d0f377271ccbf01a33f2b6d6"></div>',
      changepicturecallback: function () {
        _bsap.exec();
      }
    });
  });
  
  function get_articles(str){
    $('.boxes-' + str).hide();
  }
</script>

<style>

  .actv{
    background-color:#cf4914;
    display:block;
    font-family: 'Lato', sans-serif;
    font-size:14px;
    padding:10px;
    text-align:center;
    color:white;
    text-transform:uppercase;
    font-weight:600;

  }

  .ntactv{
    display:block;
    font-family: 'Lato', sans-serif;
    font-size:14px;
    text-align:center;
    color:white;
    text-transform:uppercase;
    font-weight:600;

  }

  .profile-box1 a{
    color: white;
    padding: 5px;
  }

  .gallery {
    clear: both;
    display: table;
    list-style: none;
    padding: 0;
    margin: 0;
    margin-bottom: 20px;
  }
  .gallery li {
    float: left;
    margin-bottom: 7px;
    margin-right: 7px;
    height: 100px;
    width: 100px;
    overflow: hidden;
    text-align: center;
    position: relative;
    padding: inherit;
  }
  .gallery li:last-child {
    margin-right: 0;
  }
  .gallery li a {
    position: relative;
    left: 100%;
    margin-left: -200%;
    position: relative;
  }
  .gallery li a:after {
    text-shadow: none;
    -webkit-font-smoothing: antialiased;
    font-family: 'fontawesome';
    speak: none;
    font-weight: normal;
    font-variant: normal;
    line-height: 1;
    text-transform: none;
    -moz-transition: 0.6s;
    -o-transition: 0.6s;
    -webkit-transition: 0.6s;
    transition: 0.6s;
    filter: progid:DXImageTransform.Microsoft.Alpha(Opacity=0);
    opacity: 0;
    -moz-border-radius: 50%;
    -webkit-border-radius: 50%;
    border-radius: 50%;
    color: #fff;
    content: "\f06e";
    display: inline-block;
    font-size: 16px;
    position: absolute;
    width: 30px;
    height: 30px;
    top: 0;
    left: 0;
    right: 0;
    bottom: 0;
    margin: auto;
    background-color: #252525;
    padding-top: 7px;
    margin-top: -7px;
  }
  .gallery li a:hover img {
    -moz-transform: scale(1.08, 1.08);
    -ms-transform: scale(1.08, 1.08);
    -webkit-transform: scale(1.08, 1.08);
    transform: scale(1.08, 1.08);
  }
  .gallery li a img {
    -moz-transition: 0.3s;
    -o-transition: 0.3s;
    -webkit-transition: 0.3s;
    transition: 0.3s;
    height: 100%;
    width: auto;
  }
</style>
{include file="include/footer.tpl"}
