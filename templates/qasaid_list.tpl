<div class="container white-bg">
  <div class="col-md-12 col-sm-12">
    <div class="page-title">
      <p style="margin-top:5px;"><a href="#">Home</a> / <a href="#">Qasaid</a></p>
      <h1>Qasaid</h1>
    </div>
  </div>
  <div class="clearfix"></div> <!-- do not delete -->
  <div class="col-md-12 col-sm-12">
    <div class="page">
      <form class="form1 white" method="post" action="">

        <div class="clearfix"></div> <!-- do not delete -->  
        <div class="row">
          <div class="col-md-3 col-xs-12 hidden-xs">
            <div class="profile-box-static">
              <h3 class="uppercase text-center">Istifadah</h3>
            </div>
            <div class="profile-box-static-bottom">

              <a href="{$server_path}istifaadah/qasaid/">» Qasaid<hr></a>
            </div>
          </div>
          <div class="col-md-9 col-xs-12" >
            <div class="col-xs-12">&nbsp;</div>
            <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
              {if $qasaid_alams}
                {assign var="i" value=1}
                  {foreach $qasaid_alams as $qa}
                    {assign var="i" value=$i+1}
                    {$qasaides = get_all_qasaid($qa['alam'])}
                    <div class="panel panel-default">
                      <div class="panel-heading" role="tab" id="headingOne">
                        <h4 class="panel-title">
                          <a role="button" data-toggle="collapse" data-parent="#accordion" href="#{$i}" aria-expanded="true" aria-controls="collapseOne">
                            {$qa['alam']}
                          </a>
                        </h4>
                      </div>
                      <div id="{$i}" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
                        <div class="panel-body">
                          {if $qasaides}
                            {foreach $qasaides as $q}
                              {$qasaid_done = get_qasaid_is_done($q['id'])}

                              {$get_slider1_val = (get_log_qasaid_slider_value($user_its, $q['id'], '1')) ? get_log_qasaid_slider_value($user_its, $q['id'], '1') : 0}
                              {$get_slider2_val = (get_log_qasaid_slider_value($user_its, $q['id'], '2')) ? get_log_qasaid_slider_value($user_its, $q['id'], '2') : 0}
                              {$get_slider3_val = (get_log_qasaid_slider_value($user_its, $q['id'], '3')) ? get_log_qasaid_slider_value($user_its, $q['id'], '3') : 0}

                              {$total_sider_value = $get_slider1_val + $get_slider2_val + $get_slider3_val}
                              {$average_slider_value = 100 - ($total_sider_value / 3)}
                              {$progress = 100 - $average_slider_value}
                           
                              <div class="col-xs-8">
                                <a href="{$server_path}istifaadah/qasaid/{$q['id']}/">{$q['title']}</a>
                              </div>
                              <div class="col-xs-4">
                                <div class="progress">
                                  <div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="{$progress}" aria-valuemin="0" aria-valuemax="100" style="width:{$progress}%">                                                   {if ($progress == 0)}
                                      <span>{$progress}%</span>
                                    {else} 
                                      {number_format($progress, 2)}%
                                    {/if}
                                  </div>
                                </div>
                              </div>
                            <div class="clearfix"></div>
                            <hr class="sleek">
                          {/foreach}
                        {/if}
                      </div>
                    </div>
                  </div>
                {/foreach}
              {/if}
            </div>
          </div>
        </div>
      </form>
    </div>
  </div>
</div>
<style>
  .blue-box1-level a {
    color: #FFF !important;
    text-decoration: none;
  }
  .progress {
    color: #000;
    font-size: 12px;
    line-height: 20px;
    text-align: center;
  }
</style>
{include file="include/js_block.tpl"}
{include file="include/footer.tpl"}