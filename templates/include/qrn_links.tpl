<div class="clearfix"><br></div>
<div class="row">
  <div class="col-xs-12 col-sm-12">
    <div class="text-center qrn-nav-wrapper">
      <div class="qrn-nav-container">
          {if ($mumin_data_header == FALSE AND $muhafiz_data_header == FALSE)}
            {$enroll_text = 'Enroll'}
          {elseif ($mumin_data_header AND $muhafiz_data_header)}
            {$enroll_text = 'Unregister'}
          {else}
            {$enroll_text = 'Enroll / Unregister'}
          {/if}
        <a href="{$server_path}qrn_cover.php"><i class="fa fa-2x fa-question-circle"></i><span>Info</span></a> &nbsp;
        <a href="{$server_path}qrn_enroll.php"><i class="fa fa-2x fa-user"></i><span>{$enroll_text}</span></a> &nbsp;
        <span class="__separator"></span>
          {if $mumin_data_header}
        <a href="{$server_path}qrn_select_muhaffiz.php"><i class="fa fa-2x fa-user-plus"></i><span>Select Muhaffiz</span></a> &nbsp;
        <a href="{$server_path}qrn_report_mumin.php"><i class="fa fa-2x fa-line-chart"></i><span>Tasmee Report</span></a> &nbsp;
        <span class="__separator"></span>
          {/if}
          {if $muhafiz_data_header}
        <a href="{$server_path}qrn_tasmee.php"><i class="fa fa-2x fa-bookmark"></i><span>Listen</span></a> &nbsp;
        <a href="{$server_path}qrn_report_muhafiz.php"><i class="fa fa-2x fa-bar-chart"></i><span>Tehfeez Report</span></a> &nbsp;
        <a href="{$server_path}qrn_ikhwaan_report.php"><i class="fa fa-2x fa-users"></i><span>Ikhwan Report</span></a> &nbsp;
          {/if}
      </div>
    </div>
  </div>
</div>
<div class="clearfix"></div>
<div class="text-center qrn-reg-wrapper">
  <div class="qrn-reg-header">
    <span>For any complaints or queries Contact us at <a href="mailto:akhqurani@talabulilm.com">akhqurani@talabulilm.com</a></span>
  </div>
</div>
<div class="clearfix"></div>