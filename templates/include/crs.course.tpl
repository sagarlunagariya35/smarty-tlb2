{if $courses}
  {foreach $courses as $course}
    {$topic_title = $crs_topic->get_topic($course.topic_id)}
    <div class="course-block">
        <div class="course-sum">
            <h3 class="text-blue">{$course.title}</h3>
            <h5>{$topic_title.title}</h5>
            <p>{$course.description}</p>
            <ul class="course-count-list">
                <li><span class="count">6</span> Badges available</li>
                <li><span class="count">9</span> Assessments available</li>
            </ul>
        </div>
        {$course_chapters = $crs_chapter->get_all_chapters($course.id)}
        <div class="course-prog-wrap">
            <a href="#course-prog1" data-toggle="collapse" class="course-list-toggle">Course program <i class="glyphicon glyphicon-chevron-left"></i></a>
            <div class="course-progs-list collapse in" id="course-prog1">
                {if $course_chapters}
                  {assign var="c" value=1}
                    {foreach $course_chapters as $chapter_data}
                      <a class="prog-name" href="#html-{$c}" data-toggle="collapse"><span>{$c}.</span> {$chapter_data.title} <i class="glyphicon glyphicon-chevron-left"></i></a>
                      <div class="prog-content collapse {if $c == '1'}in{/if}" id="html-{$c}">
                        <ul>
                          {$chapter_elements = $crs_chapter->get_all_chapter_elements($chapter_data.id)}
                          {if $chapter_elements}
                            {foreach $chapter_elements as $element}
                              <li><a href="javascript:void(0)"><i class="fa fa-file-text-o"></i>  {$element.element_title}</a></li>
                            {/foreach}
                          {/if}
                        </ul>
                      </div>
                    {$c=$c+1}
                  {/foreach}
                {/if}
            </div>
            <div class="course-prog-foot clearfix">
                <ul>
                    <li><i class="fa fa-calendar"></i> Start Date: <b>{$course.start_date}</b></li>
                    <li><i class="fa fa-money"></i> Price: <b class="price-text">{$course.fee}</b></li>
                </ul>
                <div class="action">
                    <a class="orange-btn btn" href="javascript:void(0);">Enroll now</a>
                </div>
            </div>
        </div>
    </div>
  {/foreach}
{/if}