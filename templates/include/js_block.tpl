<script src="{$server_path}templates/js/jquery.prettyPhoto.js" type="text/javascript" charset="utf-8"></script>
<script src="{$server_path}bootstrap/js/bootstrap.min.js"></script>
<script src="{$server_path}templates/js/jquery.slicknav.js"></script>
<script src="{$server_path}templates/js/jquery.confirm.min.js"></script>
<script src="{$server_path}templates/js/jquery.nivo.slider.pack.js"></script>
<script src="{$server_path}templates/js/select2/select2.full.min.js"></script>
<script src="{$server_path}templates/js/scripts.js"></script>

<script>
  $(document).ready(function () {
    $('[data-toggle="popover"]').popover({
      placement: 'bottom'
    });
    $('body').on('click', function (e) {
      $('[data-toggle="popover"]').each(function () {
        //the 'is' for buttons that trigger popups
        //the 'has' for icons within a button that triggers a popup
        if (!$(this).is(e.target) && $(this).has(e.target).length === 0 && $('.popover').has(e.target).length === 0) {
          $(this).popover('hide');
        }
      });
    });
  });
</script>