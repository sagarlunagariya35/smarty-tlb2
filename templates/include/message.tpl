{if (isset($success_message) && $success_message != '')}

    <div class="col-md-5 col-md-offset-4 alert alert-success fade in alert-message">
        <button type="button" class="close" data-dismiss="alert">&times;</button>
        {$success_message}
        {unset($_SESSION[SUCCESS_MESSAGE])}
    </div>
{/if}

{if(isset($error_message) && $error_message != '')}

    <div class="col-md-5 col-md-offset-4 alert alert-danger alert-message">
        <button type="button" class="close" data-dismiss="alert">&times;</button>
        {$error_message}
        {unset($_SESSION[ERROR_MESSAGE])}
    </div>
{/if}
<div class="clearfix"></div>
