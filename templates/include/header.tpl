<!DOCTYPE html>
<html lang="en">
  <head>
    <!-- Meta Basics -->
    <meta charset="utf-8" />
    <title>Talabul Ilm</title>
    <meta name="description" content />
    <meta name="keywords" content />
    <meta name="author" content />
    <!-- Mobile Specific Metas -->
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
    <!-- Favicons -->
    
    <link rel="shortcut icon" href="images/favicons/favicon.png" />
    <link rel="apple-touch-icon" href="images/favicons/apple-touch-icon.png" />
    <link rel="apple-touch-icon" sizes="72x72" href="images/favicons/apple-touch-icon-72x72.png" />
    <link rel="apple-touch-icon" sizes="114x114" href="images/favicons/apple-touch-icon-114x114.png" />
    <!-- Windows Tiles -->
    <meta name="application-name" content="Talabul Ilm" />
    <meta name="msapplication-TileColor" content="#ffffff" />
    <meta name="msapplication-square70x70logo" content="images/favicons/msapplication-tiny.png" />
    <meta name="msapplication-square150x150logo" content="images/favicons/msapplication-square.png" />
    <!-- CSS -->
    <link rel="stylesheet" href="css/custom.css?v=13" type="text/css" media="screen" />
    <link href="css/temp_style.css" media="all" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="css/prettyPhoto.css" type="text/css" media="screen" title="prettyPhoto main stylesheet" charset="utf-8" />
    
  </head>

  <body>

    <!-- IE 8 Blocker-->
    <!-- ====================================================================================================== -->
    <!--[if lt IE 9]>
      <div class="ie-old">
        <p class="center"><img alt="" src="<?php echo $server_path; ?>images/ie-fossil.png" /></p>
        <p class="center bigger1">Your browser is extinct!!</p>
        <p class="center"><span class="color1">This Website is not compatible with Internet Explorer 8 or below. </span></p>
        <p class="center"><span class="color8">Please <a target="_blank" href="http://www.microsoft.com/en-us/download/internet-explorer.aspx"><span class="link_1">upgrade</span></a> your browser or Download 
        <a target="_blank" href="https://www.google.com/intl/en/chrome/browser/"><span class="link_1">Google Chrome</span></a> or <a target="_blank" href="http://www.mozilla.org/en-US/firefox/new/"><span class="link_1">Mozilla Firefox</span></a></span></p>
      </div>
      <![endif]-->

    <!-- Loaders -->
    <div id="preloader" style="display: block;">
      <div id="status" style="display: block;">
        <img src="images/loader.png" class="animated pulse infinite"/>
        <h2>Loading<br />
          Please Wait...</h2>
      </div>
    </div>
    <!-- Screen Tilt Fallback -->
    <!-- ====================================================================================================== -->
    <div class="tilt-now">
      <h4>Arrgh!<br />
        <small>It seems your screen size is small.<br />
          Hold your device in Potrait Mode to Continue</small><img src="images/smart-phone-128.png" class="img-responsive" /></h4>
    </div>

    <!-- Header -->
    <!-- ====================================================================================================== -->
    <div class="header-float">
      <div class="container">
        <nav class="nav"></nav> <!-- do not delete -->
        <header>
          <div class="col-md-4 col-sm-12">
            <a href="index.php" class="logo"></a>
          </div>
          <div class="col-md-8 col-sm-12" style="padding:0px;">

            <div class="text-right hidden-xs">
              <?php
              if (isset($_SESSION[USER_LOGGED_IN]) && $_SESSION[USER_LOGGED_IN] == TRUE) {
                $mumin_data_header = $qrn_mumin_data->get_all_mumin($_SESSION[USER_ITS]);
                $muhafiz_data_header = $qrn_muhafiz_data->get_all_muhafiz($_SESSION[USER_ITS]);
                
                if ($muhafiz_data_header) {
                  $link = $server_path.'qrn_tasmee.php';
                  
                } else if ($mumin_data_header) {
                  $link = $server_path.'qrn_report_mumin.php';
                  
                } else {
                  $link = $server_path.'qrn_cover.php';
                }
              ?>
              <a href="<?php echo $link; ?>" class="login">Al-Akh al-Qurani</a>
              <?php
                $my_araz = get_my_jawab_sent_araz_by_its($_SESSION[USER_ITS]);
                $hashcode = get_my_jawab_sent_araz_by_its($_SESSION[USER_ITS], 'code');
                if ($my_araz) {
                  ?>
                  <a href="<?php echo $server_path; ?>araz-preview/<?php echo $my_araz['id'] ?>" class="login">My Araz</a>
                  <?php
                }
                if ($hashcode) {
                  ?>
                  <a target="_blank" href="<?php echo $server_path; ?>araz-jawab/<?php echo $hashcode['code']; ?>" class="login">My Araz Jawab</a>
                <?php } ?>
                <a href="<?php echo $server_path; ?>marahil/" class="<?php
                if (@$page == 'raza-araz') {
                  echo 'visit';
                } else {
                  echo 'login';
                }
                ?>">Send Araz To Aqa Maula TUS</a>
                <a href="<?php echo $server_path; ?>log-out/" class="login">Log out</a>
                <?php
              } else {
                ?>
                <a href="http://www.its52.com/ServiceLogin.aspx?domain=www.talabulilm.com&amp;continue=http://www.talabulilm.com/ITS/Authentication/" class="login">Login</a>
                <?php
              }
              ?>
            </div>
            <nav class="appmenu"></nav> <!-- do not delete -->
            <!-- different columns for responsive setting ends -->
            <div class="clearfix"></div> <!-- do not delete -->

            <?php
            $menus = get_all_menu();
            ?>

            <ul class="app-menu">

              <?php
              if (isset($_SESSION[USER_LOGGED_IN]) && $_SESSION[USER_LOGGED_IN] == TRUE) {
                foreach ($menus as $menu) {
                  $submenus = get_all_submenu($menu['id']);
                  ?>

                  <li class="<?php if ($submenus != '') { echo 'dropdown'; } ?>"><a href="<?php echo $server_path.$menu['slug']; ?>"><?php echo $menu['name']; ?></a>

                    <?php
                    if ($submenus != '') {
                      echo '<ul>';
                      foreach ($submenus as $submenu) {
                        if ($menu['slug'] == 'activities') {
                          $sub_submenu = get_article_by_submenu('article', $submenu['id']);
                        } else {
                          $sub_submenu = get_article_by_submenu('events', $submenu['id']);
                        }

                        $third_step_menus = get_all_third_step_menu($submenu['id']);
                        ?>

                        <li class="<?php if ($third_step_menus != '') { echo 'dropdown'; } ?>"><a href="<?php echo $server_path . $menu['slug'] . '/' . $submenu['slug'] . '/'; ?>"><?php echo $submenu['name']; ?></a>

                            <?php
                              if ($third_step_menus != '') {
                                echo '<ul>';
                                foreach ($third_step_menus as $thirdmenu) {
                                  ?>

                                  <li><a href="<?php echo $server_path . $menu['slug'] . '/' . $submenu['slug'] . '/' . $thirdmenu['slug'] . '/'; ?>"><?php echo $thirdmenu['name']; ?></a></li>

                                <?php
                              }
                              echo '</ul>';
                            }
                            ?>
                        </li>

                      <?php
                    }
                    echo '</ul>';
                  }
                  ?>
                  </li>
                  <?php
                }
              } else {
                ?>
                  <li><a href="http://www.its52.com/ServiceLogin.aspx?domain=www.talabulilm.com&amp;continue=http://www.talabulilm.com/ITS/Authentication/">Click Here to Login</a></li>
              <?php
              }
              ?>
              
              <?php
              if (isset($_SESSION[USER_LOGGED_IN]) && $_SESSION[USER_LOGGED_IN] == TRUE) {
                $mumin_data_header = $qrn_mumin_data->get_all_mumin($_SESSION[USER_ITS]);
                $muhafiz_data_header = $qrn_muhafiz_data->get_all_muhafiz($_SESSION[USER_ITS]);
                
                if ($muhafiz_data_header) {
                  $link = $server_path.'qrn_tasmee.php';
                  
                } else if ($mumin_data_header) {
                  $link = $server_path.'qrn_report_mumin.php';
                  
                } else {
                  $link = $server_path.'qrn_cover.php';
                }
              ?>
                <li class="visible-xs-block"><a href="<?php echo $link; ?>">Al-Akh al-Qurani</a></li>
              <?php
                $my_araz = get_my_jawab_sent_araz_by_its($_SESSION[USER_ITS]);
                $hashcode = get_my_jawab_sent_araz_by_its($_SESSION[USER_ITS], 'code');
                if ($my_araz) {
                  ?>
                <li class="visible-xs-block"><a href="<?php echo $server_path; ?>my_araz_preview.php?araz=<?php echo $my_araz['id'] ?>">My Araz</a></li>
                  <?php
                }
                if ($hashcode) {
                  ?>
                <li class="visible-xs-block"><a target="_blank" href="<?php echo $server_path; ?>admin/print_araiz.php?code=<?php echo $hashcode['code']; ?>">My Araz Jawab</a></li>
                <?php } ?>
                <li class="visible-xs-block"><a href="<?php echo $server_path; ?>marahil/">Send Araz To Aqa Maula TUS</a></li>
                <li class="visible-xs-block"><a href="<?php echo $server_path; ?>log-out/">Log out</a></li>
                <?php
              } else {
                ?>
                <li class="visible-xs-block"><a href="http://www.its52.com/ServiceLogin.aspx?domain=www.talabulilm.com&amp;continue=http://www.talabulilm.com/ITS/Authentication/">Login</a></li>
                <?php
              }
              ?>
            </ul>
          </div>
        </header>
      </div>
    </div>
    <div class="clearfix"></div> <!-- do not delete -->

    <!-- Banners -->
    <!-- ====================================================================================================== -->
    <?php
      $images_carousal = get_carousal_images_by_category(HOME_CAROUSAL);
    ?>
    <div class="container white-bg">
      <div class="banners">
        <div id="banners" class="nivoSlider">
          <?php
            if ($images_carousal) {
              foreach ($images_carousal as $image) {
          ?>
            <a href="<?php 
            if (!isset($_SESSION[USER_LOGGED_IN]) || $_SESSION[USER_LOGGED_IN] == FALSE) {
              echo 'http://www.its52.com/ServiceLogin.aspx?domain=www.talabulilm.com&amp;continue=http://www.talabulilm.com/ITS/Authentication/';
            } else {
              echo $image['link'];
            }
            ?>"><img src="<?php echo $server_path.'images/banners/'.$image['image']; ?>" alt="" title="#htmlcaption2"/></a>
          <?php
              }
            }
          ?>
        </div>
      </div>
    </div>
    <div class="clearfix"></div> <!-- do not delete -->

