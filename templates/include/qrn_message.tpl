{if ($mumin_data_header AND $muhafiz_data_header)}
  {$message_text = 'Tehfeez and Tasmee'}
{elseif $muhafiz_data_header}
  {$message_text = 'Tehfeez'}
{elseif $mumin_data_header}
  {$message_text = 'Tasmee'}
{else}
  {$message_text = ''}
{/if}

{if $message_text != ''}
  <div class="text-right qrn-reg-wrapper">
    <div class="qrn-reg-header">
      <span>You are registered for {$message_text}.</span>
    </div>
  </div>
{/if}
