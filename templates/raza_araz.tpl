<style>
  .radiobuttons-holder1{
    margin: 20px 0px;
  }
  .ques{
    color: black;
    font-size: 16px;
    line-height: 1.4;
    padding-top: 20px;
  }
  .sub-btn{
    display: block;
    -webkit-transition: 0.4s;
    -moz-transition: 0.4s;
    -o-transition: 0.4s;
    transition: 0.4s;
    border: none;
    background-color: #033054;
    height: auto;
    margin: 20px 0;
    padding: 10px;
    outline: none;
    color: #fff;
    min-width: 150px;
    text-transform: uppercase;
    text-align: center;
    float: right;
    line-height: 20px;
  }
  .sub-btn:hover{
    color: #fff;
  }
  .sub-btn:active{
    color:#fff;
  }
  .lable-mrgn{
    margin-top:5px !important;
  }
  .red-star {
    color: red;
  }
  #load {
    color: #000 !important;
    margin: 20px auto 0;
    float: right;
    text-align: center;
  }
</style>
<div class="container white-bg">
  <div class="col-md-12 col-sm-12">

    <div class="page-title">
      <p style="margin-top:5px;">
        <a href="#" class="active">Personal Information</a></p>
      <h1>Personal Information<span class="alfatemi-text">معلومات ذاتية</span></h1>
    </div>
  </div>
  <div class="clearfix"></div> <!-- do not delete -->
  <form class="forms1 white" name="raza_form" action="{$server_path}marhala-{$group_id}/{get_marhala_slug($group_id)}/raza-araz/" method="post" onsubmit="return(validate());">
    <div class="col-md-12 col-sm-12">
      <div class="araz-is-for mb20" style="color:#444444;">
        <input type="checkbox" name="already_araz_done" id="is_for" class="css-checkbox1" />
        <label for="is_for" class="css-label1" style="font-size: 15px;">I understand that this araz is for <b>{$prefix} {$first_name} {$middle_prefix} {$middle_name} {$last_name}</b></label>
      </div>
      <p class="mb20" style="color:#444444;">if you are doing araz for your child or anyone else please log in with his or her ITS id.</p>
      <div class="clearfix"></div> <!-- do not delete -->
    </div>
    
    <div class="col-md-4 col-sm-12 show_content">
      <div class="clearfix"></div> <!-- do not delete -->
      <div class="profile-box">
        <div class="clearfix"></div> <!-- do not delete -->
        <img class="img-responsive" src="{$user->get_mumin_photo()}">
        <h5>Preferred Mode of contact</h5>
        <div class="inputs-with-tickbox">
          <div class="checkbox">
            <label>
              <input id="prefer_email" name="prefer_email" value="1" type="checkbox" checked>Email
            </label>
          </div>
          <div class="checkbox">
            <label>
              <input id="prefer_whatsapp" name="prefer_whatsapp" value="1" type="checkbox" checked>Whatsapp
            </label>
          </div>
          <div class="checkbox">
            <label>
              <input id="prefer_viber" name="prefer_viber" value="1" type="checkbox" {if ($user->get_prefer_viber() == '1')} checked {/if}>Skype
            </label>
          </div>
          <div class="checkbox">
            <label>
              <input id="prefer_call" name="prefer_call" value="1" type="checkbox" {if ($user->get_prefer_call() == '1')} checked {/if} >Phone Call
            </label>
          </div>
        </div>
      </div>
    </div>

    <div class="col-md-8 col-sm-12 show_content">

      <div class="row shift50" style="margin-top: 15px;">
        <div class="col-md-6 col-sm-12 profile">
          <lable class="color1 required-star"><b>E-mail:</b></lable>
          <div  class="shift20 lable-mrgn">
            <input name="email" type="email" class="form-control required" id="email" placeholder="eg. abc@gmail.com" value="{$user->get_email()}" selected required="required">
          </div>
        </div>
        <div class="col-md-6 col-sm-12 profile">
          <lable class="color1 required-star"><b>Mobile:</b></lable>
          <div  class="shift20 lable-mrgn">
            <input name="mobile" type="text" class="form-control required" id="mobile" placeholder="Current Mobile Number" data-toggle="tooltip" title="Format : ( +919876543210)" data-placement="bottom" value="{$user->get_mobile()}" required="required">
          </div>
        </div>
      </div>

      <div class="row shift50">
        <div class="col-md-3 col-sm-12 profile">
          <a href="#" id="same_as_mobile" style="color: #CF492A;" class="shift20 lable-mrgn">Same As Mobile</a>
        </div>
      </div>

      <div class="row shift50">
        <div class="col-md-6 col-sm-12 profile">
          <lable class="color1"><b>Whatsapp:</b></lable>
          <div  class="shift20 lable-mrgn">
            <input name="whatsapp" type="text" class="form-control" id="whatsapp" placeholder="Whatsapp No." data-toggle="tooltip" title="Format : ( +919876543210)" value="{$user->get_whatsapp()}">
          </div>
        </div>
        <div class="col-md-6 col-sm-12 profile">
          <lable class="color1"><b>Skype:</b></lable>
          <div  class="shift20 lable-mrgn">
            <input name="viber" type="text" class="form-control" id="viber" placeholder="Skype Id" data-toggle="tooltip" title="Format : ( +919876543210)" value="{$user->get_viber()}">
          </div>
        </div>
      </div>

<!--      <div class="row shift50">
        <div class="col-md-6 col-md-offset-3 col-sm-offset-0 col-sm-12 profile">
          <div  class="shift20">
            <a class="btn btn-block sub-btn" style="color:#fff;" role="button" data-toggle="collapse" href="#collapseExample" aria-expanded="false" aria-controls="collapseExample">Add more Profile details</a>
          </div>
        </div>
      </div>
      <div class="col-md-12"><br></div>
      <div class="clearfix"><br></div>-->
<!--      <div class="" id="collapseExample">
        <div class="well">
          <div class="col-md-12">
            <div class="col-md-12 ques">  Add class rtl 
              1. Are you interested in <strong>counseling</strong> through <strong>Talabulilm</strong>?
            </div>
            <div class="col-md-3 col-sm-12">
              <div class="radiobuttons-holder1">  Add Class .rtl 
                <input type="radio" name="want_assistance" value="1" id="radio9" class="css-radiobutton1" />
                <label for="radio9" style="color: #000;" class="css-label1 radGroup3">Yes</label>
              </div>
            </div>
            <div class="col-md-3 col-sm-12">
              <div class="radiobuttons-holder1">  Add Class .rtl 
                <input type="radio" name="want_assistance" value="2" id="radio10" class="css-radiobutton1" />
                <label for="radio10" style="color: #000;" class="css-label1 radGroup3">No</label>
              </div>
            </div>
          </div>
          <div class="col-md-12">
            <div class="col-md-12 ques">  Add class rtl 
              2. Do you require <strong>financial assistance</strong>?
            </div>
            <div class="col-md-3 col-sm-12">
              <div class="radiobuttons-holder1">  Add Class .rtl 
                <input type="radio" name="seek_funding_edu" value="1" id="radio7" class="css-radiobutton1" />
                <label for="radio7" style="color: #000;" class="css-label1 radGroup3">Yes</label>
              </div>
            </div>
            <div class="col-md-3 col-sm-12">
              <div class="radiobuttons-holder1">  Add Class .rtl 
                <input type="radio" name="seek_funding_edu" value="2" id="radio8" class="css-radiobutton1" />
                <label for="radio8" style="color: #000;" class="css-label1 radGroup3">No</label>
              </div>
            </div>
          </div>
          <div class="col-md-12">
            <div class="col-md-12 checkbox">
              <label class="ques">
                <input id="prefer_email" name="disclaimer" class="css-checkbox" value="1" type="checkbox" checked=""> I agree to share my information with Talabulilm 
              </label>
            </div>
          </div>
          <div class="clearfix"></div>
        </div>
        <div class="clearfix"></div>
      </div>-->

      {if ($group_id != '1')}

        <div class="row">
          <div class="col-md-12 col-sm-12 profile">
            <h3 style="color:#CF492A;">Hifz al Quraan al Kareem<span class="alfatemi-text">حفظ القراْن الكريم</span></h3>
          </div>
        </div>

        <div class="row">
          <div class="col-md-12 col-sm-12 profile">
            <div  class="shift">
              {if ($user->get_quran_sanad() != '')}
                <span class="color1"><b>Hifz al Quran Sanad:</b> {$user->get_quran_sanad()}</span>
              {else}
                <div class="well text-danger">{$hidayat_quran}</div>
              {/if}
            </div>
          </div>
        </div>

        {if (in_array($group_id, $marhala_array))}

          <div class="row">
            <div class="col-md-12 col-sm-12 profile">
              <h3 style="color:#CF492A;">{$madrasah_heading}<span class="alfatemi-text">{$madrasah_heading_ar}</span></h3>
            </div>
          </div>

          <div class="row">
            <div class="col-md-12 col-sm-12 profile">
              <div  class="shift">
                {if ($madrasah_darajah != '')}
                  <span class="color1">Darajah : {$madrasah_darajah}</span>
                {else}
                  <span>{$hidayat_madrasah}</span>
                {/if}
              </div>
            </div>
          </div>
        {else}
          <div class="row">
            <div class="col-md-12 col-sm-12 profile">
              <h3 style="color:#CF492A;">{$madrasah_heading}<span class="alfatemi-text">{$madrasah_heading_ar}</span></h3>
            </div>
          </div>

          <div class="row">
            <div class="col-md-12 col-sm-12 profile">
              <div  class="shift">
                {if ($madrasah_name != '')}
                  <span class="color1">Darajah : {$madrasah_name}</span>
                {else}
                    <div class="mb20" style="color:#444444;">
                      <label style="font-size: 15px;">Are you attending any sabaq?</label>
                      <div class="clearfix"><br></div>
                      <input type="radio" name="attending_sabaq" id="attending_sabaq" class="css-radiobutton1" value="attending sabaq" />
                      <label for="attending_sabaq" class="css-label1 css-label3 radGroup1">Yes</label>
                      <input type="radio" name="attending_sabaq" id="not_attending_sabaq" class="css-radiobutton1" value="" />
                      <label for="not_attending_sabaq" class="css-label1 css-label3 radGroup1">No</label>
                    </div>
                    <div class="clearfix"></div> <!-- do not delete -->
                {/if}
              </div>
            </div>
          </div>
          <!--<div class="col-md-6 col-sm-12 select-shift">
                <div  class="select">
                  <select name="madrasah_darajah" id="madrasah_darajah" class="required select-input" onchange="check(this.value);">
                    <option value="">Select Sabaq Nisaab</option>
                      <?php
                      foreach ($ary_darajah as $drj) {
                        ?>
                                            <option value="<?php //echo $drj;  ?>" <?php //if(@$_SESSION['madrasah_darajah'] == $drj) echo 'selected';  ?>><?php //echo $drj;  ?></option>
                        <?php
                      }
                      ?>
                    <option value="none">None</option>  
                  </select>
                </div>
              </div>
              <div class="clearfix"></div>  do not delete 
              <br>
              <div class="col-md-12 col-sm-12 deeni_text" style="display: none;">
                <span class="form-control-static"><?php //echo $hidayat_asbaaq;   ?></span>
              </div>-->
        {/if}

      {/if}

      <div class="col-md-12"></div>

      <div class="clearfix"></div>
      <div class="row shift50">
        <div class="col-md-12 col-sm-12 profile">
          {if ($is_araz_done < 1)}
            <div  cld ss="shift20">
              <input type="hidden" name="its_id" value="{$user->get_its_id()}">
              <input type="hidden" name="form_id" value="{$form_id}">
              <input type="submit" name="submit" class="submit1 col-md-4 col-xs-12" value="Proceed to Araz" onclick="return ray.ajax()"/>
              <div id="load" style="display: none;"><i class="fa fa-spinner fa-spin fa-3x"></i></div>
              <span class="page-steps">Step : 2 out of 5</span>
              <div class="clearfix"></div> <!-- do not delete -->
            </div>
          {else}
            <p>Your araz is already in process. Please wait for jawab mubarak.</p>
          {/if}
        </div>
      </div>
      
      <p class="mb20 show_content" style="color:#444444;"><strong>Note:</strong> Fields marked with <span class="red-star">*</span> are mandatory</p>
      <p class="mb20 show_content" style="color:#444444;"> <span class="red-star">*</span>Please update above details if required</p>
      <div class="clearfix"></div>
    </div>
    
    <div class="clearfix"></div>
  </form>
</div>

<div class="modal fade" id="NisaabHelp" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Nisaab Help</h4>
      </div>
      <div class="modal-body">
        <h1>Nisaab e Awwal:</h1>
        <p>Da'aim ul Islaam (1), etc.</p>
        <h1>Nisaab e Saani:</h1>
        <p>Da'aim ul Islaam (2), Kitaab al Himma, etc.</p>
        <h1>Nisaab e Saalis:</h1>
        <p>Tanbih alGhafeleen, Majmu Tarbiyah, etc.</p>
        <h1>Nisaab e Raabe:</h1>
        <p>Asaas al Ta'weel, etc.</p>
        <h1>Nisaab e Khaamis:</h1>
        <p>Ta'weel ud Da'aim (1), etc.</p>
        <h1>Nisaab e Saadis:</h1>
        <p>Ta'weel ul Da'aim (2), etc.</p>
        <br>
        <br>
      </div>
    </div>
  </div>
</div>

{include file="include/js_block.tpl"}

<script type="text/javascript">
  $(document).ready(function () {
    $('[data-toggle="tooltip"]').tooltip({
      placement: 'top'
    });

    $('#radio1').on('click', function () {
      $('#psychometric_aptitude').show(400);
    });

    $('#radio2').on('click', function () {
      $('#psychometric_aptitude').hide(400);
    });
    
    $('#same_as_mobile').on('click', function () {
      var mobile_val = $('#mobile').val();
      $('#whatsapp').val(mobile_val);
    });
    
    $('#is_for').change(function(){
        if(this.checked) {
            $('.show_content').show();
        } else {
            $('.show_content').hide();
        }
    });
    $('.show_content').hide();
  });
  
  var ray = {
    ajax: function(st)
    {
      this.show('load');
      $('.submit1').hide();
    },
    show: function(el)
    {
      this.getID(el).style.display = '';
    },
    getID: function(el)
    {
      return document.getElementById(el);
    }
  }

  /*function check(str)
  {
    if (document.raza_form.madrasah_darajah.value == 'none')
    {
      $('.deeni_text').show(600);
      return false;
    } else {
      $('.deeni_text').hide();
    }
    $('.deeni_text').hide();
  }*/

{if (in_array($group_id, $marhala_array))}
  if (document.raza_form.madrasah_darajah.value == 'none')
  {
    $('.deeni_text').show(600);
  }
{/if}
  
  function validate()
  {
    var prefer_email = document.raza_form.prefer_email.checked;
    var prefer_whatsapp = document.raza_form.prefer_whatsapp.checked;
    var prefer_call = document.raza_form.prefer_call.checked;
    var prefer_viber = document.raza_form.prefer_viber.checked;
    var error = 'Please Select any of Below :\n\n';
    var validate = true;

    if (prefer_email == '')
    {
      error += 'Please Select Email\n';
      if (prefer_whatsapp == '')
      {
        error += 'Please Select Whatsapp\n';
        if (prefer_call == '')
        {
          error += 'Please Select Call\n';
          if (prefer_viber == '')
          {
            error += 'Please Select Skype\n';
            validate = false;
          }
        }
      }
    }

    if (prefer_whatsapp != '') {
      if (document.raza_form.whatsapp.value == '') {
        error += 'Please Enter Whatsapp No.\n';
        validate = false;
      }
    }

    if (prefer_viber != '') {
      if (document.raza_form.viber.value == '') {
        error += 'Please Enter Skype Id.\n';
        validate = false;
      }
    }

    {if (!in_array($group_id, $marhala_array))}
    /*
      if (document.raza_form.madrasah_darajah.value == "")
      {
        error += "Please Select Nisaab!.\n";
        validate = false;
      }
    */  
    {/if}

    if (validate == false)
    {
      alert(error);
      return validate;
    }
  }
</script>
{include file="include/footer.tpl"}