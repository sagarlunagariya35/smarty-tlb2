<div class="container white-bg">
  <div class="col-md-12 col-sm-12">
    <div class="page-title">
      <p style="margin-top:5px;">
        <a href="#" class="active">Tehfeez</a></p>
      <h1>Tehfeez</h1>
    </div>
  </div>
  {if (isset($success_message) OR isset($error_message))}
    <div class="row">
      {include file="include/message.tpl"}
    </div>
  {/if}
  <div class="clearfix"></div> <!-- do not delete -->
  <div class="col-md-12 col-sm-12">
    <form action="" method="post" name="tasmee_form">
      <!-- actual body content starts here-->
      {include file="include/qrn_message.tpl"}     
      <div class="body-container white-bg">
        <div class="hifz-tasmee-details-wrapper">
          <div class="htd-section">
            {if $count_pending_request}
              <h2>Pending Requests <span class="badge">{$count_pending_request}</span></h2>
              {if $pending_request}
                <div class="row pending-request-wrapper">
                  {foreach $pending_request as $data}
                    {$mumin_data = $user_logedin->loaduser($data['mumin_its'])}
                    ?>
                    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                      <div class="pending-request">
                        <div class="__text">
                          <span class="__prhead">{$mumin_data->get_full_name()}</span>
                          {$mumin_data->get_jamaat()},{$mumin_data->get_jamiat()}<br> {$mumin_data->get_quran_sanad()}
                        </div>
                        <div class="__action">
                          <a href="#" id="{$data['id']}" class="accept">
                            <img src="{$server_path}img/hifz-tasmee-details/tick.png" alt="">
                          </a>
                          <a href="#" id="<?php echo $data['id']; ?>" class="delete">
                            <img src="{$server_path}img/hifz-tasmee-details/close.png" alt="">
                          </a>
                        <a href="mailto:{$mumin_data->get_email()}" id="{$data['id']}" class="email">
                            <img src="{$server_path}img/hifz-tasmee-details/msg.png" alt="">
                          </a>
                        </div>
                      </div>
                    </div>
                  {/foreach}
                </div>
                {/if}
              {/if}
            <div class="clearfix"></div>  
            <div class="htd-section">
              <h2>Enter Today's Tasmee <span class="tasmee-date-now"> ({date('jS M Y')})</span></h2>
              <div class="__message _note alert fade in">
                <a href="#" class="close text-center" data-dismiss="alert" aria-label="close">&times;</a>
                Note: On this page you can update tasmee details of your linked sadeeq. You also have an option to update any person's tasmee details by entering his ITS id in the text box.</div>
              <div class="htd-section">
                <div class="qrn-tabs">
                  <ul>
                    <li id="select_sadeeq" class="select_sadeeq _active">Select Sadeeq</li>
                    <li id="select_its_id" class="select_its_id">Enter ITS ID</li>
                  </ul>
                </div>
                <div class="htd-table">
                  <div class="row">
                    <div class="col-xs-12 col-md-3 view_sadeeq qrn-tasmee-col-margin">
                      <select name="s_mumin_its" id="s_mumin_its" class="form-control select-input">
                        <option value="">Select Mumin</option>
                        {if $select_mumin}
                          {foreach $select_mumin as $mumin_data}
                            <option value="{$mumin_data['its_id']}">{$mumin_data['mumin_full_name']}</option>
                          {/foreach}
                        {/if}
                      </select>
                    </div>
                    <div class="col-xs-12 col-md-3 view_its_id qrn-tasmee-col-margin">
                      <input type="text" class="form-control" name="t_mumin_its" id="t_mumin_its" placeholder="Enter Mumin ITS">
                    </div>
                    <div class="col-xs-12 col-md-3 qrn-tasmee-col-margin">
                      <a name="search" id="search" class="btn btn-success">Get Details</a>
                    </div>
                  </div>
                  <div class="show_ajx_result">
                    <div class="show_data">
                      <div class="select-muhaffix-wrapper">
                        <div class="select-muhaffix-list">
                          <div>
                            <span class="__enhead" id="res_name"></span>
                          </div>
                        </div>
                      </div>
                      <div class="row">
                        <div class="col-xs-12 col-md-12 qrn-tasmee-col-margin">
                          <span class="__last-entry">Last Entry: <b><span class="res_surah al-kanz"></span> - <span class="res_ayat"></span></b></span>

                        </div>
                      </div>
                      <div class="row">
                        <div class="col-xs-12 col-md-12 select-muhaffix-wrapper">
                          <div class="select-muhaffix-list">
                            <div>
                              <span class="__enhead">Sanad: <b><span id="res_sanad"></span></b></span>
                            </div>
                          </div>
                        </div>
                      </div>
                      <table class="last-entry-table dataTable table table-bordered" style="border-collapse:collapse;">
                        <thead>
                          <tr>
                            <th>Name</th>
                            <th>Sanad</th>
                            <th>Last Entry</th>
                          </tr>
                        </thead>
                        <tbody>
                          <tr>
                            <td id="sadeeq-name"></td>
                            <td id="sadeeq-sanad"></td>
                            <td><b><span class="res_surah al-kanz"></span> - <span class="res_ayat"></span></b></td>
                          </tr>
                        </tbody>
                      </table>
                      <hr>
                      <h2>The selected mumin / mumena did tasmee from <span class="al-kanz res_surah"></span> - <span class="res_ayat"></span> till . . .</h2>

                      <div class="row">
                        <div class="col-xs-12 col-md-5 qrn-tasmee-col-margin al-kanz">
                          <select name="surah" id="surah" class="select-input">
                            {include file="include/qrn_surat_list.tpl"}
                          </select>
                        </div>
                        <div class="col-xs-12 col-md-5 qrn-tasmee-col-margin">
                          <input type="text" name="ayat_to" class="qrn-text-box col-md-3" id="ayat_to" placeholder="Enter Ayat No" />
                        </div>
                        <div class="col-xs-12 col-md-2 qrn-tasmee-col-margin">
                          <button type="submit" name="submit" id="tasmee_log" class="btn btn-primary">Submit</button>
                        </div>
                      </div>
                    </div>
                    <div class="col-xs-12 col-md-6 show_data">
                      <div class="select-muhaffix-wrapper">
                        <div class="select-muhaffix-list">
                          <div>
                            <input type="hidden" name="from_surah" id="from_surah" value="">
                            <input type="hidden" name="from_ayat" id="from_ayat" value="">
                          </div>
                        </div>
                      </div>
                      <div class="col-xs-12 col-md-6">
                      </div>
                      <div class="col-xs-12 col-md-3">
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            {include file="include/qrn_links.tpl"}
          </div>
        </div>
      </div>
      <!-- actual body content Ends here-->
    </form>
  </div>
</div>
<form method="post" id="frm-del-grp">
  <input type="hidden" name="delete_id" id="del-tasmee-val" value="">
</form>
<form method="post" id="frm-acpt-grp">
  <input type="hidden" name="accept_id" id="acpt-tasmee-val" value="">
</form>
            
{include file="include/js_block.tpl"}

<script type="text/javascript">
  $(".delete").confirm({
    text: "Are you sure you want to reject this Akh e-qurani request?",
    title: "Confirmation required",
    confirm: function (button) {
      $('#del-tasmee-val').val($(button).attr('id'));
      $('#frm-del-grp').submit();
      alert('Are you Sure You want to delete: ' + id);
    },
    cancel: function (button) {
      // nothing to do
    },
    confirmButton: "Yes",
    cancelButton: "No",
    post: true,
    confirmButtonClass: "btn-danger"
  });

  $(".accept").confirm({
    text: "Are you sure you want to Accept this Akh e-qurani request?",
    title: "Confirmation required",
    confirm: function (button) {
      $('#acpt-tasmee-val').val($(button).attr('id'));
      $('#frm-acpt-grp').submit();
      alert('Are you Sure You want to accept: ' + id);
    },
    cancel: function (button) {
      // nothing to do
    },
    confirmButton: "Yes",
    cancelButton: "No",
    post: true,
    confirmButtonClass: "btn-success"
  });

  $('#select_sadeeq').on('click', function (e) {
    e.preventDefault();
    $("li.select_its_id").removeClass("_active");
    $("li.select_sadeeq").addClass("_active");
    $('.view_sadeeq').show(600);
    $('.view_its_id').hide(600);
    $('.show_ajx_result').hide();
  });

  $('#select_its_id').on('click', function (e) {
    e.preventDefault();
    $("li.select_its_id").addClass("_active");
    $("li.select_sadeeq").removeClass("_active");
    $('.view_sadeeq').hide(600);
    $('.view_its_id').show(600);
    $('.show_ajx_result').hide();
  });
  $('.view_its_id').hide();

  $('#search').on("click", function () {
    var s_mumin_its = $('#s_mumin_its').val();
    var t_mumin_its = $('#t_mumin_its').val();

    if (s_mumin_its != '') {
      var mumin_its = s_mumin_its;
    } else {
      var mumin_its = t_mumin_its;
    }
    
    if(mumin_its == '') {
      alert("Please select Sadeeq OR Enter ITS ID.");
      return false;
      
    } else {
      
      myApp.showPleaseWait();
      $.ajax({
        type: "POST",
        url: "ajax.php",
        data: 'query=get_mumin_records&mumin_its=' + mumin_its,
        cache: false,
        success: function (response) {
          myApp.hidePleaseWait();
          $('.show_ajx_result').show(600);

          if (response != '') {
            $('.show_data').show(600);

            var data = $.parseJSON(response);
            $('#res_name').html(data.FULL_NAME);
            $('.res_surah').html(data.SURAH);
            $('#from_surah').val(data.SURAH);
            $('#from_surah2').val(data.SURAH);
            $('.res_ayat').html(data.AYAT);
            $('#from_ayat').val(data.AYAT);
            $('#res_sanad').html(data.SANAD);
            $('#sadeeq-sanad').html(data.SANAD);
            $('#sadeeq-name').html(data.FULL_NAME);

          } else {

            var link = 'This person has not Enrolled in Al-Akh al-Quraani program. Please enter his current details <a href="{$server_path}qrn_mumin_enroll.php">Here</a> and Press Enroll.';
            $('.show_ajx_result').html(link);
          }
        }
      });
    }
  });

  $('#tasmee_log').on("click", function () {
    var s_mumin_its = $('#s_mumin_its').val();
    var t_mumin_its = $('#t_mumin_its').val();
    var from_surah = $('#from_surah').val();
    var from_ayat = $('#from_ayat').val();
    var to_surah = $('#surah').val();
    var to_ayat = $('#ayat_to').val();

    if (s_mumin_its != '') {
      var mumin_its = s_mumin_its;
    } else {
      var mumin_its = t_mumin_its;
    }

    if (surah == '') {
      alert("Please select Surah.");
      return false;
    } else if (ayat_to == '') {
      alert("Please Enter Ayat No.");
      return false;
    } else {

      $.ajax({
        type: "POST",
        url: "ajax.php",
        data: 'query=make_entry_of_tasmee_log&mumin_its=' + mumin_its + '&from_surah=' + from_surah + '&from_ayat=' + from_ayat + '&to_surah=' + to_surah + '&to_ayat=' + to_ayat,
        cache: false,
        success: function () {
        }
      });
    }
  });

  $('.show_ajx_result').hide();

  var myApp;
  myApp = myApp || (function () {
    var pleaseWaitDiv = $('<div class="modal fade" id="pleaseWaitDialog" data-backdrop="static" data-keyboard="false"><div class="modal-dialog"><div class="modal-content"><div class="modal-header"><h4 class="modal-title">Processing . . .</h4></div><div class="modal-body"><img src="images/loader.gif"></div></div></div></div>');
    return {
      showPleaseWait: function () {
        pleaseWaitDiv.modal();
      },
      hidePleaseWait: function () {
        pleaseWaitDiv.modal('hide');
      },
    };
  })();

</script>
{include file="include/footer.tpl"}