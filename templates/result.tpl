<div class="container white-bg">
  <div class="col-md-12 col-sm-12">

    <div class="page-title">
      <p style="margin-top:5px;">
        <a href="{$server_path}">Home</a> / 
        <a href="{$server_path}miqaat_content.php?miqaat_id={$course_data['miqaat_id']}">{$miqaat_istibsaar['miqaat_title']}</a> /
        <a href="#" class="active">{$course_data['course_title']}</a></p>
      <h1>{$miqaat_istibsaar['miqaat_title']}<!--span class="alfatemi-text">معلومات ذاتية</span--></h1>
    </div>
  </div>
  <div class="clearfix"></div> <!-- do not delete -->
  <form class="forms1 white" name="raza_form" method="post">
    <div class="col-md-3 col-sm-12 hidden-xs">
      <div class="profile-box-static">
        <h3 class="uppercase text-center">Quizzes</h3>
      </div>
      <div class="profile-box-static-bottom">
        {foreach $courses as $c}
          <a href="{$server_path}take_courses.php?course_id={$c['course_id']}">» {$c['course_title']}<hr></a>
        {/foreach}
      </div>
      
      <div class="clearfix"><br></div>
      <div class="profile-box-static">
        <h3 class="uppercase text-center">Scores</h3>
      </div>
      <div class="profile-box-static-bottom">
        {assign var="i" value=0}
          {foreach $log_courses as $c}
            {assign var="i" value=$i+1}
            <span class="pull-left">» Attempt {$i}</span><span class="pull-right">{$c['score']}</span>
            <div class="clearfix"><br></div>
            <hr>
          {/foreach}
        <span class="pull-right"><a href="{$server_path}take_courses.php?course_id={$course_id}" class="btn btn-primary">Attempt Again</a></span>
        <div class="clearfix"></div>
      </div>
    </div>

    <div class="col-md-9 col-sm-12">
      <div class="static-show-button lsd large-fonts col-xs-12 col-sm-5" dir="rtl">Correct {$total_score} of {count($course_questions)}</div>
      <div class="clearfix"></div>
      <div>
        <div class="col-xs-12 col-sm-5" id="group_list" dir="rtl"></div>
        <div class="quiz-point col-xs-12 col-sm-4 color1" dir="rtl"></div>
        <div class="clearfix"></div>

        {assign var="sr" value=0}
        {foreach $course_questions as $q}
          {assign var="sr" value=$sr+1}
          <div class="col-sm-12 col-md-12" style="color:#000;">
            <div class="panel panel-{$q['divclass']}">
              <div class="panel-heading">Question {$sr}</div>
              <div class="panel-body lsd" dir="rtl">
                <p class="lsd" style="font-size: 18px" dir="rtl">{$q['question']}</p>
                <p class="lsd" style="font-size: 18px" dir="rtl"><strong>Your Answer:</strong> {$q['user_answer']}</p>
                {if ($q['divclass'] == 'danger')}
                  <p class="lsd" style="font-size: 18px" dir="rtl"><strong>Correct Answers:</strong> {$q['correct_answers']}</p>
                {/if}
              </div>
            </div>
          </div>
        {/foreach}
      </div>
    </div>
    <div class="clearfix"></div>
  </form>
</div>
<style type="text/css">
  .panel-body p{
    color: #000;
  }
  
  .profile-box-static-bottom span {
    color: #444444 !important;
    font-size: 14px;
    text-align: left;
    line-height: 150%;
    text-decoration: none;
    margin-left: 15px;
    margin-right: 15px;
  }
  
  .profile-box-static-bottom span a {
    color: #FFF !important;
  }
</style>
{include file="include/js_block.tpl"}
{include file="include/footer.tpl"}