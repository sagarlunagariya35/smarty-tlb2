<?php require 'template_header.php'; ?>

<div class="container white-bg">
  <div class="col-md-12 col-sm-12">
    <div class="page-title">
      <p style="margin-top:5px;"><a href="#">Home</a> / <a href="#">Qasaid</a></p>
      <h1>Qasaid</h1>
    </div>
  </div>
  <div class="clearfix"></div>  
  <div class="col-md-12 col-sm-12">
	<div class="row">
		<div class="col-sm-3 side-bar">
			<div class="sidebar-block">
				<h3 class="sidebar-block-title">All courses <i class="fa fa-search"></i></h3>
				<ul class="courses-list">
					<li><a href="javascript:void(0);">Fiqh</a></li>
					<li>
						<a data-toggle="collapse" class="list-toggle" href="#sub-course1">Quran Majeed <i class="fa fa-angle-down"></i></a>
						<ul class="sub-courses collapse in" id="sub-course1">
							<li><a href="javascript:void(0);">Akh al Qurani</a></li>
							<li><a href="javascript:void(0);">Makharij</a></li>
						</ul>
					</li>
					<li><a href="javascript:void(0);">Adab</a></li>
					<li>
						<a data-toggle="collapse" class="list-toggle collapsed" href="#sub-course2">Akhbar <i class="fa fa-angle-down"></i></a>
						<ul class="sub-courses collapse" id="sub-course2">
							<li><a href="javascript:void(0);">Akh al Qurani</a></li>
							<li><a href="javascript:void(0);">Makharij</a></li>
						</ul>
					</li>
					<li><a href="javascript:void(0);">Hikam</a></li>
				</ul>
			</div>
			<div class="sidebar-block">
				<h3 class="sidebar-block-title">SORT BY <i class="fa fa-search"></i></h3>
				<ul class="courses-list">
					<li><a href="javascript:void(0);">Free Courses</a></li>
					<li><a href="javascript:void(0);" class="active">Courses Not Taken</a></li>
					<li><a href="javascript:void(0);">Courses in progress</a></li>
				</ul>
			</div>
			<div class="sidebar-block padding profile-block">
				<h3 class="sidebar-block-title">My Profile</h3>
				<h2 class="user-name">MUSTAFA ABBAS</h2>
				<p class="info-text">Progress tracker</p>
				<p class="info-text">My Achievements</p>
				<div class="achievement-list">
					<img src="images/badge1.png" alt="" />
					<img src="images/icn-trophy.png" alt="" />
				</div>
				<a href="javascript:void(0);" class="see-more">See more on my profile <i class="fa fa-arrow-right"></i></a>
			</div>
		</div>
		<div class="col-sm-9 right-side-wrap">
			<div class="title-wrap clearfix">
				<h2>Category Name - Courses Available</h2>
				<a href="javascript:void(0);" class="back-link">Return to Dashboard</a>
			</div>
			<div class="course-block">
				<div class="course-sum">
					<h3 class="text-blue">Basic Webdesign</h3>
					<h5>Tilawaat</h5>
					<p>HTML is written in the form of HTML elements consisting of tags enclosed in angle brackets (like &lt;html&gt;). HTML tags.</p>
					<ul class="course-count-list">
						<li><span class="count">6</span> Badges available</li>
						<li><span class="count">9</span> Assessments available</li>
					</ul>
				</div>
				<div class="course-prog-wrap">
					<a href="#course-prog1" data-toggle="collapse" class="course-list-toggle">Course program <i class="glyphicon glyphicon-chevron-left"></i></a>
					<div class="course-progs-list collapse in" id="course-prog1">
						<a class="prog-name" href="#html-basic" data-toggle="collapse"><span>1.</span> HTML Basic Examples <i class="glyphicon glyphicon-chevron-left"></i></a>
						<div class="prog-content collapse in" id="html-basic">
							<ul>
								<li><a href="javascript:void(0)"><i class="fa fa-file-text-o"></i>  HTML Attributes and Headings</a></li>
								<li><a href="javascript:void(0)"><i class="fa fa-play-circle"></i>  Form Elements</a></li>
								<li><a href="javascript:void(0)"><i class="fa fa-volume-up"></i>  HTML Comments (Audio Cast)</a></li>
								<li><a href="javascript:void(0)"><i class="fa fa-check-square-o"></i>  Quize Basic Examples</a></li>
								<li><a href="javascript:void(0)"><i class="fa fa-file-text-o"></i>  Paragraphs and formatting</a></li>
								<li><a href="javascript:void(0)"><i class="fa fa-file-text-o"></i>  HTML Colornames</a></li>
								<li><a href="javascript:void(0)"><i class="fa fa-play-circle"></i>  HTML Symbols</a></li>
								<li><a href="javascript:void(0)"><i class="fa fa-play-circle"></i>  HTML Charset</a></li>
							</ul>
						</div>
						<a class="prog-name collapsed" href="#html5-forms" data-toggle="collapse"><span>2.</span> HTML5 Forms <i class="glyphicon glyphicon-chevron-left"></i></a>
						<div class="prog-content collapse" id="html5-forms">
							<ul>
								<li><a href="javascript:void(0)"><i class="fa fa-file-text-o"></i>  HTML Attributes and Headings</a></li>
								<li><a href="javascript:void(0)"><i class="fa fa-play-circle"></i>  Form Elements</a></li>
								<li><a href="javascript:void(0)"><i class="fa fa-volume-up"></i>  HTML Comments (Audio Cast)</a></li>
								<li><a href="javascript:void(0)"><i class="fa fa-check-square-o"></i>  Quize Basic Examples</a></li>
								<li><a href="javascript:void(0)"><i class="fa fa-file-text-o"></i>  Paragraphs and formatting</a></li>
								<li><a href="javascript:void(0)"><i class="fa fa-file-text-o"></i>  HTML Colornames</a></li>
								<li><a href="javascript:void(0)"><i class="fa fa-play-circle"></i>  HTML Symbols</a></li>
								<li><a href="javascript:void(0)"><i class="fa fa-play-circle"></i>  HTML Charset</a></li>
							</ul>
						</div>
						<a class="prog-name collapsed" href="#html5-graphics" data-toggle="collapse"><span>3.</span> HTML5 Graphics <i class="glyphicon glyphicon-chevron-left"></i></a>
						<div class="prog-content collapse" id="html5-graphics">
							<ul>
								<li><a href="javascript:void(0)"><i class="fa fa-file-text-o"></i>  HTML Attributes and Headings</a></li>
								<li><a href="javascript:void(0)"><i class="fa fa-play-circle"></i>  Form Elements</a></li>
								<li><a href="javascript:void(0)"><i class="fa fa-volume-up"></i>  HTML Comments (Audio Cast)</a></li>
								<li><a href="javascript:void(0)"><i class="fa fa-check-square-o"></i>  Quize Basic Examples</a></li>
								<li><a href="javascript:void(0)"><i class="fa fa-file-text-o"></i>  Paragraphs and formatting</a></li>
								<li><a href="javascript:void(0)"><i class="fa fa-file-text-o"></i>  HTML Colornames</a></li>
								<li><a href="javascript:void(0)"><i class="fa fa-play-circle"></i>  HTML Symbols</a></li>
								<li><a href="javascript:void(0)"><i class="fa fa-play-circle"></i>  HTML Charset</a></li>
							</ul>
						</div>
						<a class="prog-name collapsed" href="#html-media" data-toggle="collapse"><span>4.</span> HTML Media <i class="glyphicon glyphicon-chevron-left"></i></a>
						<div class="prog-content collapse" id="html-media">
							<ul>
								<li><a href="javascript:void(0)"><i class="fa fa-file-text-o"></i>  HTML Attributes and Headings</a></li>
								<li><a href="javascript:void(0)"><i class="fa fa-play-circle"></i>  Form Elements</a></li>
								<li><a href="javascript:void(0)"><i class="fa fa-volume-up"></i>  HTML Comments (Audio Cast)</a></li>
								<li><a href="javascript:void(0)"><i class="fa fa-check-square-o"></i>  Quize Basic Examples</a></li>
								<li><a href="javascript:void(0)"><i class="fa fa-file-text-o"></i>  Paragraphs and formatting</a></li>
								<li><a href="javascript:void(0)"><i class="fa fa-file-text-o"></i>  HTML Colornames</a></li>
								<li><a href="javascript:void(0)"><i class="fa fa-play-circle"></i>  HTML Symbols</a></li>
								<li><a href="javascript:void(0)"><i class="fa fa-play-circle"></i>  HTML Charset</a></li>
							</ul>
						</div>
					</div>
					<div class="course-prog-foot clearfix">
						<ul>
							<li><i class="fa fa-calendar"></i> Start Date: <b>On Demand</b></li>
							<li><i class="fa fa-money"></i> Price: <b class="price-text">Free</b></li>
						</ul>
						<div class="action">
							<a class="orange-btn btn" href="javascript:void(0);">Enroll now</a>
						</div>
					</div>
				</div>
			</div>
			<div class="course-block">
				<div class="course-sum">
					<h3 class="text-orange">Advanced webdesign</h3>
					<h5>Advanced</h5>
					<p>Just about every creative digital project has Photoshop CC at its core. The world’s most advanced image editing app lets you enhance, retouch, and manipulate photographs and other images in any way you can imag</p>
				</div>
				<div class="course-prog-wrap">
					<a href="#course-prog2" data-toggle="collapse" class="course-list-toggle collapsed">Course program <i class="glyphicon glyphicon-chevron-left"></i></a>
					<div class="course-progs-list collapse" id="course-prog2">
						<a class="prog-name" href="#html-basic" data-toggle="collapse"><span>1.</span> HTML Basic Examples <i class="glyphicon glyphicon-chevron-left"></i></a>
						<div class="prog-content collapse in" id="html-basic">
							<ul>
								<li><a href="javascript:void(0)"><i class="fa fa-file-text-o"></i>  HTML Attributes and Headings</a></li>
								<li><a href="javascript:void(0)"><i class="fa fa-play-circle"></i>  Form Elements</a></li>
								<li><a href="javascript:void(0)"><i class="fa fa-volume-up"></i>  HTML Comments (Audio Cast)</a></li>
								<li><a href="javascript:void(0)"><i class="fa fa-check-square-o"></i>  Quize Basic Examples</a></li>
								<li><a href="javascript:void(0)"><i class="fa fa-file-text-o"></i>  Paragraphs and formatting</a></li>
								<li><a href="javascript:void(0)"><i class="fa fa-file-text-o"></i>  HTML Colornames</a></li>
								<li><a href="javascript:void(0)"><i class="fa fa-play-circle"></i>  HTML Symbols</a></li>
								<li><a href="javascript:void(0)"><i class="fa fa-play-circle"></i>  HTML Charset</a></li>
							</ul>
						</div>
						<a class="prog-name collapsed" href="#html5-forms" data-toggle="collapse"><span>2.</span> HTML5 Forms <i class="glyphicon glyphicon-chevron-left"></i></a>
						<div class="prog-content collapse" id="html5-forms">
							<ul>
								<li><a href="javascript:void(0)"><i class="fa fa-file-text-o"></i>  HTML Attributes and Headings</a></li>
								<li><a href="javascript:void(0)"><i class="fa fa-play-circle"></i>  Form Elements</a></li>
								<li><a href="javascript:void(0)"><i class="fa fa-volume-up"></i>  HTML Comments (Audio Cast)</a></li>
								<li><a href="javascript:void(0)"><i class="fa fa-check-square-o"></i>  Quize Basic Examples</a></li>
								<li><a href="javascript:void(0)"><i class="fa fa-file-text-o"></i>  Paragraphs and formatting</a></li>
								<li><a href="javascript:void(0)"><i class="fa fa-file-text-o"></i>  HTML Colornames</a></li>
								<li><a href="javascript:void(0)"><i class="fa fa-play-circle"></i>  HTML Symbols</a></li>
								<li><a href="javascript:void(0)"><i class="fa fa-play-circle"></i>  HTML Charset</a></li>
							</ul>
						</div>
						<a class="prog-name collapsed" href="#html5-graphics" data-toggle="collapse"><span>3.</span> HTML5 Graphics <i class="glyphicon glyphicon-chevron-left"></i></a>
						<div class="prog-content collapse" id="html5-graphics">
							<ul>
								<li><a href="javascript:void(0)"><i class="fa fa-file-text-o"></i>  HTML Attributes and Headings</a></li>
								<li><a href="javascript:void(0)"><i class="fa fa-play-circle"></i>  Form Elements</a></li>
								<li><a href="javascript:void(0)"><i class="fa fa-volume-up"></i>  HTML Comments (Audio Cast)</a></li>
								<li><a href="javascript:void(0)"><i class="fa fa-check-square-o"></i>  Quize Basic Examples</a></li>
								<li><a href="javascript:void(0)"><i class="fa fa-file-text-o"></i>  Paragraphs and formatting</a></li>
								<li><a href="javascript:void(0)"><i class="fa fa-file-text-o"></i>  HTML Colornames</a></li>
								<li><a href="javascript:void(0)"><i class="fa fa-play-circle"></i>  HTML Symbols</a></li>
								<li><a href="javascript:void(0)"><i class="fa fa-play-circle"></i>  HTML Charset</a></li>
							</ul>
						</div>
						<a class="prog-name collapsed" href="#html-media" data-toggle="collapse"><span>4.</span> HTML Media <i class="glyphicon glyphicon-chevron-left"></i></a>
						<div class="prog-content collapse" id="html-media">
							<ul>
								<li><a href="javascript:void(0)"><i class="fa fa-file-text-o"></i>  HTML Attributes and Headings</a></li>
								<li><a href="javascript:void(0)"><i class="fa fa-play-circle"></i>  Form Elements</a></li>
								<li><a href="javascript:void(0)"><i class="fa fa-volume-up"></i>  HTML Comments (Audio Cast)</a></li>
								<li><a href="javascript:void(0)"><i class="fa fa-check-square-o"></i>  Quize Basic Examples</a></li>
								<li><a href="javascript:void(0)"><i class="fa fa-file-text-o"></i>  Paragraphs and formatting</a></li>
								<li><a href="javascript:void(0)"><i class="fa fa-file-text-o"></i>  HTML Colornames</a></li>
								<li><a href="javascript:void(0)"><i class="fa fa-play-circle"></i>  HTML Symbols</a></li>
								<li><a href="javascript:void(0)"><i class="fa fa-play-circle"></i>  HTML Charset</a></li>
							</ul>
						</div>
					</div>
					<div class="course-prog-foot clearfix">
						<ul>
							<li><i class="fa fa-calendar"></i> Start Date: <b>12.09.2014</b></li>
							<li><i class="fa fa-user"></i> Instructor: <b><a href="javascript:void(0);">Prof. John Guttag</a></b></li>
							<li><i class="fa fa-clock-o"></i> Duration: <b>12.09.2014</b></li>
							<li><i class="fa fa-money"></i> Price: <b class="price-text">1450$</b></li>
						</ul>
						<div class="action">
							<a class="orange-btn btn" href="javascript:void(0);">Enroll on a course</a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
  </div>
</div>
<style>.blue-box1-level a{color:#FFF!important;text-decoration:none;}.progress{color:#000;font-size:12px;line-height:20px;text-align:center;}</style>
<script src="templates/js/jquery.prettyPhoto.js" type="text/javascript" charset="utf-8"></script>
<script src="bootstrap/js/bootstrap.min.js"></script>
<script src="templates/js/jquery.slicknav.js"></script>
<script src="templates/js/jquery.confirm.min.js"></script>
<script src="templates/js/jquery.nivo.slider.pack.js"></script>
<script src="templates/js/select2/select2.full.min.js"></script>
<script src="templates/js/scripts.js"></script>
<script>
  $(document).ready(function () {
    $('[data-toggle="popover"]').popover({
      placement: 'bottom'
    });
    $('body').on('click', function (e) {
      $('[data-toggle="popover"]').each(function () {
        //the 'is' for buttons that trigger popups
        //the 'has' for icons within a button that triggers a popup
        if (!$(this).is(e.target) && $(this).has(e.target).length === 0 && $('.popover').has(e.target).length === 0) {
          $(this).popover('hide');
        }
      });
    });
  });
</script>
<?php require 'template_footer.php'; ?>
