<?php
require './classes/class.database.php';
// start importing the file line by line
ob_start();

$file = fopen('cities.csv', 'r');

$html = "<!DOCTYPE html>
<html xmlns=\"http://www.w3.org/1999/xhtml\" lang=\"en\">
<head>

  <meta charset=\"utf-8\">
  </head>
  <body>";
$start = time();
echo "Start Time: $start<br>";
if($file){
  $data_set = '';
  $count = 0;

  $line = fgetcsv($file);
  
  while(!feof($file)){
    set_time_limit(30);
    
    $line = fgetcsv($file);
    if($line){
      $count++;
      $city = $db->clean_data($line[2]);
      $country = $db->clean_data($line[1]);
      
      $data_set .= ($city != '') ? "('$country', '$city'), " : '';
      
      if(strlen($data_set) > 4000){
        $data_set = trim($data_set, ', ');
        $query = "INSERT INTO tlb_city (`country_code`, `city`) VALUES $data_set";
        
        $ret = $db->query($query);
        $data_set = '';
        echo "$count records processed.<br>";
        ob_flush();
      }
    }
  }
}
$end = time();
$total = $end - $start;
echo "end : $end<br>";
echo "Time taken: $total";
?>