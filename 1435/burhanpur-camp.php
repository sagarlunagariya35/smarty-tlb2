<?php
// Page bady class
$body_class = 'page-sub-page';

require_once 'inc/inc.header.php';
?>

<!-- Breadcrumb -->
<div class="container">
  <ol class="breadcrumb">
    <li><a href="#">Home</a></li>
    <li class="active">1435 Burhanpur Camp</li>
  </ol>
</div>
<!-- end Breadcrumb -->

<!-- Page Content -->
<div id="page-content">
  <div class="container">
    <div class="row">
      <!--SIDEBAR Content-->
      <?php include_once 'inc.sidebar.php'; ?>
      <div class="col-md-9 col-sm-8">
        <!--MAIN Content-->
        <div id="page-main">
           <section id="gallery">
            <header>
              <h2>Gallery - 1435 Burhanpur Camp</h2>
            </header>
            <div class="section-content">
              <ul class="gallery-list">
                <?php
                for ($i = 1; $i < 9; $i++) {
                  ?>
                  <li><a href="<?php echo SERVER_PATH; ?>assets/slides/1435/burhanpur-camp/<?php echo $i; ?>.jpg" class="image-popup"><img src="<?php echo SERVER_PATH; ?>assets/slides/1435/burhanpur-camp/<?php echo $i; ?>.jpg" alt=""></a></li>
                  <?php
                }
                ?>
              </ul>
              <a href="" class="read-more">Go to Gallery</a>
            </div><!-- /.section-content -->
          </section><!-- /.gallery -->

        </div><!-- /#page-main -->
        <!-- end MAIN Content -->
      </div><!-- /.container -->
    </div>
    <!-- end Page Content -->

    <?php
    require_once 'inc/inc.footer.php';
    ?>