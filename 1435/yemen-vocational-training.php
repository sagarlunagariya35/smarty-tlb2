<?php
require_once 'session.php';
// Page bady class
$body_class = 'page-sub-page';

require_once 'inc/inc.header.php';
?>

<div class="container white-bg">
  <div class="col-md-12 col-sm-12 bayan">
    <div class="page-title">
      <h1>Gallery - 1435 Yemen Vocational Training</h1>
      <p><a href="#">Home</a> / <a href="#">1435 Yemen Vocational Training</a></p>
    </div>
  </div>
  <div class="clearfix"></div> <!-- do not delete -->
  <div class="col-md-3 col-sm-12">
    <div class="profile-box" style="margin: 0px; width: 250px;">
      <h5 style="padding: 2px;">Preferred Mode of contact</h5>
      <div class="section-content">
        <div class="clearfix"></div><br>
        <a href="<?php echo SERVER_PATH; ?>activities/1435/yemen-vocational-training/">Yemen Vocational Training</a>
        <div class="clearfix"></div><br>
        <a href="<?php echo SERVER_PATH; ?>activities/1435/SMART-park-visit/">SMART Park visit</a>
        <div class="clearfix"></div><br>
        <a href="<?php echo SERVER_PATH; ?>activities/1435/burhanpur-camp/">Burhanpur Camp</a>
        <div class="clearfix"></div><br>
        <a href="<?php echo SERVER_PATH; ?>activities/1435/seminar/">Seminar</a>
        <div class="clearfix"></div>
      </div>
    </div>
    <div class="clearfix"></div>
  </div>

  <div class="col-md-6 col-sm-12">
    <h2>Article - 1435 Yemen Vocational Training</h2><br>
    <span>
        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam gravida semper arcu, quis volutpat diam hendrerit vitae. Nulla id volutpat ex. Nulla facilisi. Nullam odio lectus, porta id mi quis, ullamcorper bibendum sem. Maecenas finibus risus augue, finibus varius nibh porttitor ac. Integer urna enim, porttitor pharetra eros finibus, pellentesque imperdiet augue. Quisque quis finibus metus. Vivamus vehicula ante vel nibh hendrerit, ornare lacinia nulla semper. Ut a turpis sit amet lectus mollis gravida auctor vel purus. Nulla cursus sem magna, et venenatis neque ultrices nec. Etiam sodales diam magna, eget fringilla magna imperdiet in. Praesent tortor sapien, accumsan eu nunc vitae, efficitur feugiat sem. Proin id ligula id lacus maximus scelerisque ac in risus. Proin facilisis ligula in tellus auctor, nec ullamcorper nibh pellentesque.
    </span><br>
    
    <br><h2>Videos - 1435 Yemen Vocational Training</h2><br>
    <ul class="gallery">
      <li><a href="http://www.youtube.com/watch?v=kh29_SERH0Y?rel=0" rel="prettyPhoto" title="YouTube demo"><img src="<?php echo SERVER_PATH; ?>images/flash-logo.png" width="60" alt="" /></a></li>
    </ul>
    
    <h2>Audio - 1435 Yemen Vocational Training</h2><br>
    <ul class="gallery">
        <audio loop="loop" controls="controls">  
            <source src="http://128.downloadming1.c om/bollywood%20mp3/A%20Strange%20Love%20Story%20%282011%29/02%20-%20More%20Piya.mp3" />  
            <source src="http://128.downloadming1.com/bollywood%20mp3/A%20Strange%20Love%20Story%20%282011%29/02%20-%20More%20Piya.mp3" />  
         </audio>
    </ul>
    
    <h2>Gallery - 1435 Yemen Vocational Training</h2><br>
    <ul class="gallery">
      <?php
      for ($i = 1; $i < 20; $i++) {
        ?>
        <li><a href="<?php echo SERVER_PATH; ?>assets/slides/1435/yemen-vocational-training/<?php echo $i; ?>.jpg" rel="prettyPhoto[gallery2]"><img src="<?php echo SERVER_PATH; ?>assets/slides/1435/yemen-vocational-training/<?php echo $i; ?>.jpg" alt=""></a></li>
        <?php
      }
      ?>
    </ul>
  </div>
  
  <div class="col-md-3 col-sm-12">
    <div class="col-md-12 blue-box1">
      <h3 class="bordered lsd white"></h3>
      <div class="clearfix"></div>
    </div>
  </div>
</div>

<script type="text/javascript" charset="utf-8">
  $(document).ready(function() {
    $("area[rel^='prettyPhoto']").prettyPhoto();

    $(".gallery:first a[rel^='prettyPhoto']").prettyPhoto({animation_speed: 'normal', theme: 'light_square', slideshow: 3000, autoplay_slideshow: true});
    $(".gallery:gt(0) a[rel^='prettyPhoto']").prettyPhoto({animation_speed: 'fast', slideshow: 10000, hideflash: true});

    $("#custom_content a[rel^='prettyPhoto']:first").prettyPhoto({
      custom_markup: '<div id="map_canvas" style="width:260px; height:265px"></div>',
      changepicturecallback: function() {
        initialize();
      }
    });

    $("#custom_content a[rel^='prettyPhoto']:last").prettyPhoto({
      custom_markup: '<div id="bsap_1259344" class="bsarocks bsap_d49a0984d0f377271ccbf01a33f2b6d6"></div><div id="bsap_1237859" class="bsarocks bsap_d49a0984d0f377271ccbf01a33f2b6d6" style="height:260px"></div><div id="bsap_1251710" class="bsarocks bsap_d49a0984d0f377271ccbf01a33f2b6d6"></div>',
      changepicturecallback: function() {
        _bsap.exec();
      }
    });
  });
</script>

<style>
  .profile-box a{
    margin-left: 30px;
    color: white;
  }
  .gallery {
    clear: both;
    display: table;
    list-style: none;
    padding: 0;
    margin: 0;
    margin-bottom: 20px;
  }
  .gallery li {
    float: left;
    margin-bottom: 7px;
    margin-right: 7px;
    height: 100px;
    width: 100px;
    overflow: hidden;
    text-align: center;
    position: relative;
    padding: inherit;
  }
  .gallery li:last-child {
    margin-right: 0;
  }
  .gallery li a {
    position: relative;
    left: 100%;
    margin-left: -200%;
    position: relative;
  }
  .gallery li a:after {
    text-shadow: none;
    -webkit-font-smoothing: antialiased;
    font-family: 'fontawesome';
    speak: none;
    font-weight: normal;
    font-variant: normal;
    line-height: 1;
    text-transform: none;
    -moz-transition: 0.6s;
    -o-transition: 0.6s;
    -webkit-transition: 0.6s;
    transition: 0.6s;
    filter: progid:DXImageTransform.Microsoft.Alpha(Opacity=0);
    opacity: 0;
    -moz-border-radius: 50%;
    -webkit-border-radius: 50%;
    border-radius: 50%;
    color: #fff;
    content: "\f06e";
    display: inline-block;
    font-size: 16px;
    position: absolute;
    width: 30px;
    height: 30px;
    top: 0;
    left: 0;
    right: 0;
    bottom: 0;
    margin: auto;
    background-color: #252525;
    padding-top: 7px;
    margin-top: -7px;
  }
  .gallery li a:hover img {
    -moz-transform: scale(1.08, 1.08);
    -ms-transform: scale(1.08, 1.08);
    -webkit-transform: scale(1.08, 1.08);
    transform: scale(1.08, 1.08);
  }
  .gallery li a img {
    -moz-transition: 0.3s;
    -o-transition: 0.3s;
    -webkit-transition: 0.3s;
    transition: 0.3s;
    height: 100%;
    width: auto;
  }
</style>

<?php
require_once 'inc/inc.footer.php';
?>