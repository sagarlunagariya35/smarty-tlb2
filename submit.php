<?php
require 'smarty/libs/Smarty.class.php';
require_once 'session.php';
require_once 'requires_login.php';
require_once 'classes/class.user.php';

$cls_user = new mtx_user();

// Page body class
$body_class = 'page-sub-page';
$araz_id = $_GET['araz_id'];
$araz_id = rtrim($araz_id,'/');
$marhala_id = get_marhala_from_araz($araz_id);

$ques = get_marhala_questions($marhala_id);
$total_ques = count($ques);
$ques_answers = get_marhala_question_answers($araz_id);
$total_ques_answers = count($ques_answers);

$marhala_array = array('5','6','7','8');
$page_carousal = SUBMIT_CAROUSAL;

$redirect_link = $cls_user->get_general_setting_data('submit_link');

require_once 'inc/inc.header2.php';
require_once 'inc/inc.footer.php';

$smarty = new Smarty;

// Header / Session variables
$smarty->assign("server_path", SERVER_PATH);
$smarty->assign("marhala_id", $marhala_id);
$smarty->assign("araz_id", $araz_id);
$smarty->assign("ques", $ques);
$smarty->assign("total_ques", $total_ques);
$smarty->assign("ques_answers", $ques_answers);
$smarty->assign("total_ques_answers", $total_ques_answers);
$smarty->assign("marhala_array", $marhala_array);
$smarty->assign("already_araz_done", @$_SESSION[already_araz_done]);
$smarty->assign("success_message", @$_SESSION[SUCCESS_MESSAGE]);
$smarty->assign("redirect_link", $redirect_link);

$smarty->display('submit.tpl');