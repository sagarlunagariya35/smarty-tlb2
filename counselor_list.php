<?php
require_once 'session.php';
require_once 'requires_login.php';
require_once 'classes/class.user.php';
require_once 'classes/class.araz.php';

$body_class = 'page-sub-page';

$user = new mtx_user();
$user->loaduser($_SESSION[USER_ITS]);

$counselor_data = $user->get_counselor_data();

require_once 'inc/inc.header2.php';
?>

<link href="assets/datatables/dataTables.bootstrap.css" rel="stylesheet" type="text/css" />
<script src="assets/datatables/jquery.dataTables.min.js" type="text/javascript"></script>
<script src="assets/datatables/dataTables.bootstrap.min.js" type="text/javascript"></script>

<!-- Contents -->
<!-- ====================================================================================================== -->
<div class="container white-bg">
  <div class="col-md-12 col-sm-12">

    <div class="page-title">
      <p style="margin-top:5px;">
        <a href="#" class="active">List of Counselors</a>
      </p>
      
      <h1>List of Counselors</h1>
    </div>
  </div>
  <div class="clearfix"></div> <!-- do not delete -->
  <div class="col-md-12 col-sm-12">
    <div class="page">
      
      <section class="content">
        <div class="row">
          <!-- Center Bar -->
          <div class="col-md-12">
            <br><br>
            <div class="box">
              <div class="box-body">
                <table id="counselors" class="table1 table table-responsive">
                  <thead>
                    <th>Counselor's Name</th>
                    <th>Place</th>
                    <th>Phone</th>
                    <th>E-mail</th>
                    <th>Website</th>
                  </thead>
                  <tbody>
                    <?php
                    foreach ($counselor_data as $cd) {
                      ?>
                      <tr>
                        <td><?php echo $cd['first_name'].' '.$cd['last_name'] ?></td>
                        <td><?php echo $cd['city'] ?></td>
                        <td><?php echo $cd['mobile'] ?></td>
                        <td><?php echo $cd['email'] ?></td>
                        <td><?php echo $cd['website'] ?></td>
                      </tr>
                      <?php
                    }
                    ?>
                  </tbody>
                </table>
              </div>
            </div>
            <br><br>
          </div>
          <!-- /Center Bar -->
        </div>
        <!-- /Content -->
      </section>
        
      <div class="clearfix"></div> <!-- do not delete -->
    </div>
  </div>
</div>

<script type="text/javascript">
  $(function () {
    $('#counselors').DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": true,
      "ordering": false,
      "info": true,
      "autoWidth": false
    });
  });
</script>

<?php
require_once 'inc/inc.footer.php';
?>