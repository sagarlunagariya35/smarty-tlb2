<?php
require_once 'session.php';
require_once 'requires_login.php';
require_once 'classes/class.user.php';
require_once 'classes/class.araz.php';

$body_class = 'page-sub-page';

$araz = $_GET['araz'];
$user_its = $_SESSION[USER_ITS];

$araz_data = get_my_araz_data_by_its_and_id($user_its,$araz);

$marhala_school_array = array('1','2','3','4');
$marhala_id = $araz_data['marhala'];

$user = new mtx_user();
$user->loaduser($_SESSION[USER_ITS]);

$institute_data = get_my_araz_institutions_data($araz);

require_once 'inc/inc.header2.php';
?>
<style>
  .skip{
  display:block;
  max-width:300px;
  -webkit-transition: 0.4s;
  -moz-transition: 0.4s;
  -o-transition: 0.4s;
  transition: 0.4s;
  border: none;
  background-color: #155485;
  height: auto;
  line-height:20px;
  margin:0px 5px 20px;
  padding:10px;
  outline: none;
  color:#FFF;
  text-transform:uppercase;
  text-align:center;
  font-weight:bold;
  font-size:18px;
  float:right;
  text-decoration: underline none;
}
</style>

<!-- Contents -->
<!-- ====================================================================================================== -->
<div class="container white-bg">
  <div class="col-md-12 col-sm-12">

    <div class="page-title">
      <p style="margin-top:5px;">
        <a href="#" class="active">Araz Preview</a>
      </p>
      
      <h1>Araz Preview<span class="alfatemi-text">عرض</span></h1>
    </div>
  </div>
  <div class="clearfix"></div> <!-- do not delete -->
  <div class="col-md-12 col-sm-12">
    <div class="page">
      <form class="forms1 white" action="" method="post">
        
        <a class="skip lsd" href="my_araz_preview.php?araz=<?php echo $araz; ?>" dir="rtl">لسان الدعوة واسطسس يظظاطط click كروو</a>
        
        <div class="clearfix"></div> <!-- do not delete -->
        
        <div class="block top-mgn10"></div>
        <div class="blue-box1">
          <h3 class="bordered">
            <span>
                <p class="large-fonts">Ghibb al-Sajadaat al-´Uboodiyah</p>
                <p class="large-fonts">Fi al-Hadrat al-´Aaliyah al-Imamiyah Ashraq Allah</p>
                <p class="large-fonts">Anwaarahaa</p>
                
                <br>
                <p class="large-fonts">Applicant/ Student : <?php echo $user->get_full_name(); ?></p>
                <p class="large-fonts">Jamaat/City : <?php echo $user->get_city(); ?></p>
                
                <?php if($marhala_id == '1'){ ?>
                <br>
                <p class="large-fonts">I humbly beseech/request the Dua Mubarak and Raza Mubarak of Aqa Moula TUS to begin my education at the following institution :</p>
                
                <?php 
                  }else {
                    if(count($institute_data) > 1){
                ?>
                <br>
                <p class="large-fonts">I humbly beseech/request the guidance of Aqa Moula TUS in acquiring an education at one of the following institutions :</p>
                
                <?php }else {  ?>
                <br>
                <p class="large-fonts">I humbly beseech/request Aqa Moula TUS to grant Dua Mubarak and Raza Mubarak for education at the following institutions :</p>
                
                  <?php } } ?>
                
            </span>
          </h3>
          <div class="clearfix"></div> <!-- do not delete -->
        </div>
        
        <?php include 'my_araz_institution_preview.php'; ?>
        
      </form>
      <div class="clearfix"></div> <!-- do not delete -->
    </div>
  </div>
</div>

<?php
require_once 'inc/inc.footer.php';
?>