<?php
$its = $_GET['its'];

$ip = "162.208.53.164";
//Data, connection, auth
$soapUrl = "http://www.attalimims.com/AttalimWebService.asmx"; // asmx URL of WSDL

// xml post structure
$xml_post_string = '<?xml version="1.0" encoding="utf-8"?>
<soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
  <soap:Body>
    <GetStudentITSForTalabulilm xmlns="http://www.attalimims.com/">
      <IPAddress>'.$ip.'</IPAddress>
      <StrKey>TASub052</StrKey>
      <ITSNo>'.$its.'</ITSNo>
    </GetStudentITSForTalabulilm>
  </soap:Body>
</soap:Envelope>';

$headers = array(
    "Content-type: text/xml;charset=\"utf-8\"",
    "Accept: text/xml",
    "Cache-Control: no-cache",
    "Pragma: no-cache",
    "SOAPAction: http://www.attalimims.com/GetStudentITSForTalabulilm",
    "Content-length: " . strlen($xml_post_string)
);

// PHP cURL  for https connection with auth
$ch = curl_init();
curl_setopt($ch, CURLOPT_URL, $soapUrl);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
//curl_setopt($ch, CURLOPT_USERPWD, $soapUser . ":" . $soapPassword); // username and password - declared at the top of the doc
curl_setopt($ch, CURLOPT_POST, true);
curl_setopt($ch, CURLOPT_POSTFIELDS, $xml_post_string); // the SOAP request
curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

// converting
$response = curl_exec($ch);
curl_close($ch);

print_r($response);
?>
