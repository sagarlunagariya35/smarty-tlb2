<?php
require 'smarty/libs/Smarty.class.php';
require_once 'session.php';
require_once 'requires_login.php';
require_once 'classes/class.survey.php';

$srv_survey = new Mtx_Survey();
$user_its = $_SESSION[USER_ITS];

require_once 'inc/inc.header2.php';
require_once 'inc/inc.footer.php';

$body_class = 'page-sub-page';

// All variables are declare in header
$survey_list = $srv_survey->get_open_surveys($_SESSION[USER_ITS], $srv_jamaat, $srv_jamiat, $srv_marhala, $srv_school);

$smarty = new Smarty;

// Header / Session variables
$smarty->assign("server_path", SERVER_PATH);
$smarty->assign("user_its", $user_its);
$smarty->assign("surveys", $survey_list);

$smarty->display('srv_survey_master.tpl');