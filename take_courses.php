<?php
require 'smarty/libs/Smarty.class.php';
require_once 'session.php';
require_once 'requires_login.php';
require_once 'classes/class.quiz.php';

$user_id = $_SESSION[USER_ID];
$user_its = $_SESSION[USER_ITS];

$quiz = new Quiz();

$pre_fill_user_answers = FALSE;

$len = mt_rand(4, 10);
$form_id = '';
for ($i = 0; $i < $len; $i++) {
  $d = rand(1, 30) % 2;
  $form_id .= $d ? chr(rand(65, 90)) : chr(rand(48, 57));
}

$_SESSION['form_id'] = md5(time() . $form_id);

if (isset($_REQUEST['course_id']) && $_REQUEST['course_id'] != '') {
  $course_id = $_REQUEST['course_id'];
} else {
  $course_id = $quiz->get_single_course();
}
// Get the total questions count
$total_course_que = Quiz::total_course_questions($course_id);
// get the first question
$questions = Quiz::get_course_questions($course_id, 0);
// group contains how many questions
$min_que_id = '0';
$max_que_id = $total_course_que['MAX_QUE_ID'];

//print_r($questions);
if($questions === FALSE){
  header('location: result.php?cid='.$course_id);
}
//if ($cleared_last_course_id != $questions['course_id'])
  //$cleared_last_que_id = 0;

//$user_acquired_points = Quiz::get_group_acquired_points($cleared_last_group_id, $user_id);
$course_data = $quiz->get_course_by_id($course_id);
$pre_fill_user_answers = $quiz->pre_fill_user_answers_for_course($course_id, $user_its, $questions['id']);   
$miqaat_istibsaar = get_single_miqaat_istibsaar($course_data['miqaat_id']);
$courses = $quiz->get_all_courses();

require_once 'inc/inc.header2.php';
require_once 'inc/inc.footer.php';

$smarty = new Smarty;

// Header / Session variables
$smarty->assign("server_path", SERVER_PATH);
$smarty->assign("course_id", $course_id);
$smarty->assign("quiz", $quiz);
$smarty->assign("course_data", $course_data);
$smarty->assign("total_course_que", $total_course_que);
$smarty->assign("questions", $questions);
$smarty->assign("min_que_id", $min_que_id);
$smarty->assign("max_que_id", $max_que_id);
$smarty->assign("course_data", $course_data);
$smarty->assign("pre_fill_user_answers", $pre_fill_user_answers);
$smarty->assign("miqaat_istibsaar", $miqaat_istibsaar);
$smarty->assign("courses", $courses);

$smarty->display('take_courses.tpl');
