<?php
  require 'smarty/libs/Smarty.class.php';
  require_once 'session.php';
  require_once 'requires_login.php';
  require_once 'classes/class.qrn_mumin.php';
  require_once 'classes/class.qrn_tasmee.php';
  
  $qrn_mumin = new Qrn_Mumin();
  $qrn_tasmee = new Qrn_Tasmee();
  
  $my_last_entry = FALSE;
  
  if (isset($_POST['submit'])) {
    $log_id = $_POST['log_id'];
    $to_surah = $_POST['surah'];
    $to_ayat = $_POST['ayat_to'];
    $from_surah = $_POST['from_surah'];
    $from_ayat = $_POST['from_ayat'];
    
    $from_surah_details = $qrn_tasmee->get_quran_data_of_surah($from_surah, $from_ayat);
    $from_id = $from_surah_details['id'];
    
    $to_surah_details = $qrn_tasmee->get_quran_data_of_surah($to_surah, $to_ayat);
    $to_id = $to_surah_details['id'];

    if ($from_id AND $to_id) {
      if ($from_id < $to_id) {
        $total_ayat = $to_id - $from_id;
        $update_tasmee = $qrn_tasmee->update_last_tasmee_log($to_surah, $to_ayat, $total_ayat, $log_id);

        $result = $qrn_mumin->update_surah_and_ayat($_SESSION[USER_ITS], $to_surah, $to_ayat);

        $_SESSION[SUCCESS_MESSAGE] = 'Tasmee Log Successfully Updated';
      } else {
        $_SESSION[ERROR_MESSAGE] = 'Please Enter Correct Surah and Ayat.';
      }
    } else {
      $_SESSION[ERROR_MESSAGE] = 'Please Enter Correct Ayat No.';
    }
  }
  
  $my_last_entry = $qrn_tasmee->get_last_tasmee_entry($_SESSION[USER_ITS]);
  $list_of_surahs = $qrn_tasmee->get_surat_list();
  
  require_once 'inc/inc.header2.php';
  require_once 'inc/inc.footer.php';
  
  $smarty = new Smarty;

  // Header / Session variables
  $smarty->assign("server_path", SERVER_PATH);
  $smarty->assign("mumin_data_header", $mumin_data_header);
  $smarty->assign("muhafiz_data_header", $muhafiz_data_header);
  $smarty->assign("qrn_tasmee", $qrn_tasmee);
  $smarty->assign("my_last_entry", $my_last_entry);
  $smarty->assign("list_of_surahs", $list_of_surahs);
  $smarty->assign("success_message", $_SESSION['SUCCESS_MESSAGE']);
  $smarty->assign("error_message", $_SESSION['ERROR_MESSAGE']);

  $smarty->display('qrn_edit_last_tasmee_entry.tpl');