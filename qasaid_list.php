<?php
require 'smarty/libs/Smarty.class.php';
require_once 'session.php';
require_once 'requires_login.php';

$user_its = $_SESSION[USER_ITS];

$tag = 'ashara-mubarakah';

$body_class = 'page-sub-page';
$qasaid_alams = get_all_alam_of_qasaid();

require_once 'inc/inc.header2.php';
require_once 'inc/inc.footer.php';

$smarty = new Smarty;

// Header / Session variables
$smarty->assign("server_path", SERVER_PATH);
$smarty->assign("user_its", $user_its);
$smarty->assign("qasaid_alams", $qasaid_alams);

$smarty->display('qasaid_list.tpl');