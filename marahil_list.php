<?php
require 'smarty/libs/Smarty.class.php';
require_once 'session.php';
require_once 'requires_login.php';
require_once 'classes/class.user.php';

$user = new mtx_user();
$user->loaduser($_SESSION[USER_ITS]);

$ary_done_araz = FALSE;

//header('location: '. SERVER_PATH . 'maintenance.php');
//exit();

//$araz_pending = check_pending_araz($_SESSION[USER_ITS]);
//if($araz_pending)
//{
//  header('location: '. SERVER_PATH . 'review.php');
//  exit();
//}


$get_all_araz = get_all_jawab_sent_araz_by_its($_SESSION[USER_ITS], 6);
if($get_all_araz) {
  foreach ($get_all_araz as $data) {
    $ary_done_araz[] = $data['marhala'];
  }
}

$save_araz_data = check_save_araz_data($_SESSION[USER_ITS]);
if($save_araz_data){
  
  $retrive_data = unserialize($save_araz_data['araz_data']);
  
  $_SESSION['already_araz_done'] = $retrive_data['already_araz_done'];
  $_SESSION['step'] = $retrive_data['step'];
  $_SESSION['msb_student_info'] = $retrive_data['msb_student_info'];
  $_SESSION['madrasah_name'] = $retrive_data['madrasah_name'];
  $_SESSION['madrasah_name_ar'] = $retrive_data['madrasah_name_ar'];
  $_SESSION['madrasah_darajah'] = $retrive_data['madrasah_darajah'];
  $_SESSION['madrasah_city'] = $retrive_data['madrasah_city'];
  $_SESSION['hifz_sanad'] = $retrive_data['hifz_sanad'];
  $_SESSION['form_id'] = $retrive_data['form_id'];
  $_SESSION['taken_psychometric_aptitude_test'] = $retrive_data['taken_psychometric_aptitude_test'];
  $_SESSION['text_psychometric_aptitude'] = $retrive_data['text_psychometric_aptitude'];
  $_SESSION['prefer_more_clarity'] = $retrive_data['prefer_more_clarity'];
  $_SESSION['want_further_guidence'] = $retrive_data['want_further_guidence'];
  $_SESSION['seek_funding_edu'] = $retrive_data['seek_funding_edu'];
  $_SESSION['want_assistance'] = $retrive_data['want_assistance'];
  $_SESSION['received_sponsorship'] = $retrive_data['received_sponsorship'];
  $_SESSION['follow_shariaa'] = $retrive_data['follow_shariaa'];
  $_SESSION['disclaimer'] = $retrive_data['disclaimer'];
  $_SESSION[MARHALA_ID] = $marhala_id = $retrive_data['marhala_id'];
  $_SESSION['ques'] = $retrive_data['ques'];
  
  if($marhala_id < 5){
    $_SESSION['madrasah_country'] = $retrive_data['madrasah_country'];
    $_SESSION['school_city'] = $retrive_data['school_city'];
    $_SESSION['school_standard'] = $retrive_data['school_standard'];
    $_SESSION['school_name'] = $retrive_data['school_name'];
    $_SESSION['school_filter'] = $retrive_data['school_filter'];
    $_SESSION['school_pincode'] = $retrive_data['school_pincode'];
  }else {
    $_SESSION['marhala_group'] = $retrive_data['marhala_group'];
    $_SESSION['generate_course_key'] = $retrive_data['generate_course_key'];
    $_SESSION['is_institute_selected'] = $retrive_data['is_institute_selected'];
    $_SESSION['save_institute_data'] = $retrive_data['save_institute_data'];
  }
  
  header('location: ' . SERVER_PATH . "marhala-$marhala_id/" . get_marhala_slug($marhala_id) . '/preview/');  
  exit();
}

$user_age = $user->get_dob();
$age = ageCalculator($user_age);

if (isset($_REQUEST['submit'])) {
  if (isset($_POST['already_araz_done'])) {
    $_SESSION['already_araz_done'] = $_POST['already_araz_done'];
  } else {
    $_SESSION['already_araz_done'] = 0;
  }

  $_SESSION['step'] = '1';
  
  // redirect here
  $marhala_id = $_POST['submit'];
  switch ($marhala_id) {
    case 1:
    case 2:
    case 3:
    case 4:
    case 5:
    case 6:
    case 7:
    case 8:
      header('location: ' . SERVER_PATH . 'marhala-' . $marhala_id . '/' . get_marhala_slug($marhala_id) . '/raza-araz/');
      break;
  }
}
// Page body class
$body_class = 'page-sub-page';

require_once 'inc/inc.header2.php';
require_once 'inc/inc.footer.php';

$smarty = new Smarty;

// Header / Session variables
$smarty->assign("server_path", SERVER_PATH);
$smarty->assign("age", $age);
$smarty->assign("ary_done_araz", $ary_done_araz);

$smarty->display('marahil_list.tpl');