<?php
require 'smarty/libs/Smarty.class.php';
require_once 'session.php';
require_once 'requires_login.php';
require_once 'classes/class.user.php';
require_once 'classes/class.araz.php';

$user = new mtx_user();
$user->loaduser($_SESSION[USER_ITS]);

// Page body class
$body_class = 'page-sub-page';

$group_id = $marhala_id = $panel_heading = $std_heading = $panel_heading_ar = $kalemaat_nooraniyah = FALSE;

if(@$_GET['group']){
  $group_id = $_GET['group'];
  $marhala_id = $_SESSION[MARHALA_ID];
  $heading_msg = 'Araz Message';
}else {
  $heading_msg = 'Araz Under Process';
}

switch ($group_id) {
  case 1:
    $panel_heading = 'Pre-Primary Araz';
    $std_heading = 'Pre-Primary';
    $panel_heading_ar = 'الحضانة';
    $kalemaat_nooraniyah = 'kalemaat nooraniyah marhala 1';
    break;
  case 2:
    $panel_heading = 'Primary Araz';
    $std_heading = 'Primary';
    $panel_heading_ar = 'الابتدائية الأولى';
    $kalemaat_nooraniyah = 'kalemaat nooraniyah marhala 2';
    break;
  case 3:
    $panel_heading = 'Std 5th to Std 7th Araz';
    $std_heading = 'Std 5th to Std 7th';
    $panel_heading_ar = 'الابتدائية الثانية';
    $kalemaat_nooraniyah = 'kalemaat nooraniyah marhala 3';
    break;
  case 4:
    $panel_heading = 'Std 8th to 10th Araz';
    $std_heading = 'Std 8th to Std 10th';
    $panel_heading_ar = 'الثانوية الأولى';
    $kalemaat_nooraniyah = 'kalemaat nooraniyah marhala 4';
    break;
  case 5:
    $panel_heading = 'Std 11th - 12th Araz';
    $std_heading = 'Std 11th - 12th';
    $panel_heading_ar = 'الثانوية الثانية';
    $kalemaat_nooraniyah = 'kalemaat nooraniyah marhala 5';
    break;
  case 6:
    $panel_heading = 'Graduation Araz';
    $std_heading = 'Graduation';
    $panel_heading_ar = 'البكالوريا';
    $kalemaat_nooraniyah = 'kalemaat nooraniyah marhala 6';
    break;
  case 7:
    $panel_heading = 'Post Graduation Araz';
    $std_heading = 'Post Graduation';
    $panel_heading_ar = 'الماجيستار';
    $kalemaat_nooraniyah = 'kalemaat nooraniyah marhala 7';
    break;
  case 8:
    $panel_heading = 'Diploma Araz';
    $std_heading = 'Diploma';
    $panel_heading_ar = 'الدبلوم';
    $kalemaat_nooraniyah = 'kalemaat nooraniyah marhala 8';
    break;
}

$marhala_school_array = array('5','6','7','8');

require_once 'inc/inc.header2.php';
require_once 'inc/inc.footer.php';

$smarty = new Smarty;

// Header / Session variables
$smarty->assign("server_path", SERVER_PATH);
$smarty->assign("heading_msg", $heading_msg);
$smarty->assign("group_id", $group_id);
$smarty->assign("marhala_id", $marhala_id);
$smarty->assign("panel_heading", $panel_heading);
$smarty->assign("std_heading", $std_heading);
$smarty->assign("panel_heading_ar", $panel_heading_ar);
$smarty->assign("kalemaat_nooraniyah", $kalemaat_nooraniyah);
$smarty->assign("marhala_school_array", $marhala_school_array);
$smarty->assign("already_araz_done", @$_SESSION['already_araz_done']);

$smarty->display('review.tpl');