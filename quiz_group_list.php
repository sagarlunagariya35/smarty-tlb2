<?php
require_once 'session.php';
require_once 'requires_login.php';
require_once 'classes/class.quiz.php';

$user_id = $_SESSION[USER_ID];
$user_its = $_SESSION[USER_ITS];

if (isset($_POST['btn_next'])) {
    foreach ($_POST['btn_next'] as $key => $val) {
        $group_id = $val;
    }
    $_SESSION[GROUP_ID] = $group_id;
    header('Location: take_quiz.php');
    exit();
}

// get groups
$groups = Quiz::get_groups_list();

$completed_stages = Quiz::get_completed_stages($user_id);
require_once 'inc/inc.header2.php';
?>

<!-- Contents -->
<!-- ====================================================================================================== -->
<div class="container white-bg">
  <div class="col-md-12 col-sm-12">

    <div class="page-title">
      <p style="margin-top:5px;">
        <a href="#" class="active">Asharah Mubarakah 1436 H</a></p>
      <h1>Asharah Mubarakah 1436 H<span class="alfatemi-text">معلومات ذاتية</span></h1>
    </div>
  </div>
  <div class="clearfix"></div> <!-- do not delete -->
  <form class="forms1 white" name="raza_form" method="post">
    <div class="col-md-3 col-sm-12 hidden-xs">
      <div class="profile-box-static">
        <h3 class="uppercase text-center">Tareekhi Event</h3>
      </div>
      <div class="profile-box-static-bottom">
        <a href="#">» 1435 H<hr></a>
        <a href="#">» 1436 H<hr></a>
        <a href="#">» 1437 H<hr></a>
      </div>
    </div>

    <div class="col-md-8 col-sm-12">
      <div class="static-show-button lsd large-fonts col-xs-12 col-sm-5" dir="rtl">معلومات ذاتية</div>
      <span style="background-color: #155485;" class="multitextbuttton uppercase col-xs-12 col-sm-5">Take Quiz</span>
      <div class="clearfix"></div>
      <div>
        <h2 class="text-center">Pick Your Quiz Level<hr></h2>
        <?php
        foreach ($groups as $grp) {
          if (in_array($grp['group_id'], $completed_stages)) {
              $tick = 'tick';
              $btn_cls = 'btn-complete';
          } else {
              $tick = '';
              $btn_cls = 'btn-pending';
          }
            ?>
        <div <?php if($tick){ echo 'style="background-color: #549BC7;"'; } ?> class="blue-box1 blue-box1-level <?php echo $tick ?> col-md-5 col-sm-12">
                <button name="btn_next[<?php echo $grp['group_id']; ?>]" class="btn" value="<?php echo $grp['group_id']; ?>">Level <?php echo $grp['group_id']; ?></button>
            </div>
            <?php
        }
        ?>
        <div class="multitextbuttton blue-box1-button">
          <a class="text-center" href="quiz_top_score_list.php">Top Scores</a>
        </div>
        <div class="clearfix"></div>
      </div>
    </div>
    <div class="clearfix"></div>
  </form>
</div>
<style>
  .multitextbuttton a{
    color: #FFF;
    text-decoration: none;
  }
</style>

<?php
require_once 'inc/inc.footer.php';
?>
