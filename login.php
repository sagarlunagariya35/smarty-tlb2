<?php
session_start();
require_once 'classes/class.user.php';
require_once 'classes/gen_functions.php';

if (isset($_POST['btnLogin'])) {
  $data = $db->clean_data($_POST);
  $user_its = (int) $data['its'];
  if (!is_int($user_its)) {
    echo 'It should be interger';
    //Error 
    exit();
  }

  $password = $data['password'];

  $user = new mtx_user();

  $user_exists = $user->check_user_exist($user_its);
  if ($user_exists) {
    
    // if user type is not set then redirect user to select his primary role

    $_SESSION[USER_LOGGED_IN] = TRUE;
    $_SESSION[USER_ID] = $user->get_user_id();
    $_SESSION[USER_ITS] = $user_its;
    
    //$user_temps = get_temp_session_data($user_its);
    $araz_pending = check_pending_araz($user_its);
    if($araz_pending)
    {
        //$user_temp_session = unserialize($user_temps[0]['session_data']);
        
        //header('location: ' . SERVER_PATH . 'marhala-' . $user_temp_session['marhala_id'] . '/' . get_marhala_slug($user_temp_session['marhala_id']) . '/select-course/');
       
        //$its_exists_in_counselor_db = check_member_already_in_coun_data($_SESSION[USER_ITS]);

        //if($its_exists_in_counselor_db) {
          //header('location: '. SERVER_PATH . 'review.php');
          header('location: '. SERVER_PATH);
        //}
      
    }
    else
    {
        //(isset($_SESSION['from'])) ? header('location:'. SERVER_PATH.$redirect) : header('location:'. SERVER_PATH);
        header('location: '. SERVER_PATH);
    }
    
    //$user_type = $user->get_user_type();
    //if ($user_type == '---') { //if user type is empty then set it
    // header('location: ' . SERVER_PATH . 'my-role/');
      //exit();
    //}    
    
    
    // if(isset($_SESSION['from'])){
    //   $redirect = str_replace('/dev/', '', $_SESSION['from']);
    // }

    

    // if($user->get_user_type() == "-") { //if user type is empty then set it 
    //    header('location: ' . SERVER_PATH . 'my-role/');
    //  } else { //user type is not empty. it is set. goto index page
    //    header('Location: ' . SERVER_PATH);
    //  }

    
  } else {
    $_SESSION[ERROR_MESSAGE] = 'No Data Found in this ITS';
  }
}
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content=""><meta name="author" content="">
    <title>Talabul-ilm</title> 
    <link href="<?php echo SERVER_PATH; ?>templates/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo SERVER_PATH; ?>assets/css/login.css" rel="stylesheet">
    <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
    <style>
      body {
        background: url('<?php echo SERVER_PATH; ?>assets/img/login.png');
        background-repeat: no-repeat;
        background-size: cover;
      }
      .error_msg {
        padding: 5px;
      }
      .btn-orange{
        color: #fff;
        background-color: #cf4914;
        border-color: #cf4914;
      }
      .login_form{
        background-color: #fff;
      }
    </style> 
<!--[if lt IE 9]><script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script> <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script> <![endif]-->
  </head>
  <body style="background-color:#07294d">
    <div class="container">
      <?php if(isset($_SESSION[SUCCESS_MESSAGE]) OR isset($_SESSION[ERROR_MESSAGE])) { ?>
        <div class="row">
            <?php include_once 'inc/message.php'; ?>
        </div>
      <?php } ?>
      
      <div class="col-md-4 col-sm-6 col-xs-10 col-md-offset-4 col-sm-offset-3 col-xs-offset-1 login_form">
        <div class="text-center">
          <image src="<?php echo SERVER_PATH; ?>images/pen_logo.png" />
          <div class="text-center col-md-12">
            <h1 style="color: #cf4914">Talab ul ilm</h1>
          </div>
        </div>
        <form method="post">
          <div class="form-group">
            <label>ITS ID</label>
            <input class="form-control" type="text" name="its" id="its" type="text" required maxlength="8">
          </div>
          <div class="form-group">
            <label>Password</label>
            <input class="form-control" type="password" name="password" type="password" required="">
          </div>
          <div class="form-group col-md-12">
            <button class="btn btn-block btn-orange" name="btnLogin"> Login</button>
          </div>
        </form>
      </div>
    </div><!--//-->
    <script src="<?php echo SERVER_PATH; ?>assets/js/jquery-2.1.0.min.js"></script>
    <!--//-->
    <script src="<?php echo SERVER_PATH; ?>assets/bootstrap/js/bootstrap.min.js"></script>
  </body>
</html>