<?php
require 'smarty/libs/Smarty.class.php';
require_once 'session.php';
require_once 'requires_login.php';
require_once 'classes/class.qrn_mumin.php';
require_once 'classes/class.qrn_muhafiz.php';
require_once 'classes/class.user.php';

$qrn_mumin = new Qrn_Mumin();
$qrn_muhafiz = new Qrn_Muhafiz();
$user_logedin = new mtx_user();
$user_logedin->loaduser($_SESSION[USER_ITS]);
$gender = $user_logedin->get_gender();

$rdmuhafiz = $muhafiz_its = $user_data = $request_its = $count_pending_request = FALSE;
$already_set_muhafiz = $muhaffiz_name = FALSE;
$muhafiz_full_name = $muhafiz_jamaat = $muhafiz_jamiat = $muhafiz_quran_sanad = $muhafiz_gender = FALSE;

if (isset($_POST['remove_muhafiz'])) {
  $remove_muhafiz_its = $_POST['remove_muhafiz'];
  
  $exist_muhafiz_data = $user_logedin->loaduser($remove_muhafiz_its);
  $exist_muhafiz_email = $exist_muhafiz_data->get_email();
  $exist_mumin_name = $qrn_mumin->get_mumin_name($_SESSION[USER_ITS]);
          
  if ($exist_muhafiz_email) {
    $exist_muhafiz_subject = 'Sadeeq Removed You Successfully';
    $exist_muhafiz_message = 'Your sadeeq '. $exist_mumin_name .' is not linked with you anymore.';

    $sent_exist_muhafiz_mail = $qrn_muhafiz->insert_cron_emails('akhequrani@talabulilm.com', $exist_muhafiz_email, $exist_muhafiz_subject, $exist_muhafiz_message);
  }

  $remove_muhafiz_data = $qrn_mumin->update_muhafiz_data_of_mumin($_SESSION[USER_ITS], $muhafiz_its, $muhafiz_full_name, $muhafiz_jamaat, $muhafiz_jamiat, $muhafiz_quran_sanad, $muhafiz_gender);
}

if (isset($_POST['submit'])) {
  $rdmuhafiz = @$_POST['rdmuhafiz'];
  $muhafiz_its = @$_POST['muhafiz_its'];

  if ($muhafiz_its != 0) {
    $request_its = $muhafiz_its;
  } else if ($rdmuhafiz != 0) {
    $request_its = $rdmuhafiz;
  }
  
  $pending_req = $qrn_muhafiz->count_all_tasmee_request_of_mumin($_SESSION[USER_ITS]);
  if ($pending_req < 3) {
    if ($request_its != FALSE) {
      if ($request_its != $_SESSION[USER_ITS]) {

        $select = $qrn_muhafiz->get_all_muhafiz($request_its);
        if ($select) {
          $insert = $qrn_muhafiz->sent_request_for_tasmee($_SESSION[USER_ITS], $request_its);

          $muhafiz_user_data = $user_logedin->loaduser($request_its);
          $email = $muhafiz_user_data->get_email();
          $subject = 'Tasmee Request';
          $message = '<p>You have received a request for Tasmee from ' . $qrn_mumin->get_mumin_name($_SESSION[USER_ITS]) . '. Click <a href="http://www.talabulilm.com">here</a> and go to the website. <br>Click on the "Akh e qurani" button and accept or reject this persons request.<br>After you have accepted this request, we will show you his/her email id and contact details. You may setup yours and your sadeeqs ideal tasmee time after that.</p><p> Remember to update his/her tasmee every day on the akh e qurani tasmee page.</p>';

          $sent_mail = $qrn_muhafiz->insert_cron_emails('akhequrani@talabulilm.com', $email, $subject, $message);
          $_SESSION[SUCCESS_MESSAGE] = 'Your Akh e Qurani request has been sent to ' . $muhafiz_user_data->get_full_name() . '. You will be notified by email when he/she accepts your request.';
        } else {
          $_SESSION[ERROR_MESSAGE] = 'This person has not Enrolled in Al-Akh al-Quraani program. Select another Muhaffiz';
        }
      } else {
        $_SESSION[ERROR_MESSAGE] = 'You have entered your own ITS ID.';
      }
    } else {
      $_SESSION[ERROR_MESSAGE] = 'Please Enter correct ITS ID.';
    }
  } else {
    $_SESSION[ERROR_MESSAGE] = 'Sorry you are not allowed to send anymore request.';
  }
}

$pending_request = $qrn_muhafiz->get_all_tasmee_request_of_mumin($_SESSION[USER_ITS]);
if ($pending_request) {
  $count_pending_request = count($pending_request);
}

$select_muhafiz = $qrn_muhafiz->get_all_muhafiz_view($gender);

$get_mumin_data = $qrn_mumin->get_all_mumin($_SESSION[USER_ITS]);
if ($get_mumin_data[0]['muhafiz_id'] != 0) {
  $already_set_muhafiz = $get_mumin_data[0]['muhafiz_id'];
  $muhaffiz_name = $get_mumin_data[0]['muhafiz_full_name'];
}

require_once 'inc/inc.header2.php';
require_once 'inc/inc.footer.php';

$smarty = new Smarty;

// Header / Session variables
$smarty->assign("server_path", SERVER_PATH);
$smarty->assign("mumin_data_header", $mumin_data_header);
$smarty->assign("muhafiz_data_header", $muhafiz_data_header);
$smarty->assign("select_muhafiz", $select_muhafiz);
$smarty->assign("pending_request", $pending_request);
$smarty->assign("count_pending_request", $count_pending_request);
$smarty->assign("get_mumin_data", $get_mumin_data);
$smarty->assign("already_set_muhafiz", $already_set_muhafiz);
$smarty->assign("muhaffiz_name", $muhaffiz_name);
$smarty->assign("user_logedin", $user_logedin);
$smarty->assign("success_message", $_SESSION['SUCCESS_MESSAGE']);
$smarty->assign("error_message", $_SESSION['ERROR_MESSAGE']);

$smarty->display('qrn_select_muhaffiz.tpl');