<?php
require 'smarty/libs/Smarty.class.php';
require_once 'session.php';
require_once 'requires_login.php';
require_once 'classes/class.quiz.php';

$user_id = $_SESSION[USER_ID];
$user_its = $_SESSION[USER_ITS];
$course_id = (int) $_GET['cid'];

$quiz = new Quiz();

$course_questions = $quiz->get_courses_question_by_course_id($course_id);
$user_answers = $quiz->get_user_answers_for_course($course_id, $user_its);

$total_score = 0; // variable to calculate the user score for this course;
// merge the user answer into the course questions array
foreach ($course_questions as $k => $q) {
  $course_questions[$k]['user_answer'] = $user_answers[$k]['submitted_answer'];
  $course_questions[$k]['divclass'] = ($user_answers[$k]['correct']) ? 'success' : 'danger';
  $total_score += $user_answers[$k]['correct'];
  $ary_correct_answers = unserialize($q['correct_answer']);
  $course_questions[$k]['correct_answers'] = join(', ', $ary_correct_answers);
}

$result = get_id_from_form_processes($_SESSION['form_id']);
if(!$result){
  $score = $total_score . ' / ' . count($course_questions);
  $insert_log = $quiz->insert_log_courses($user_its, $course_id, $score);
  
  $time = time();
  $query = "INSERT INTO forms_processed VALUES ('".$_SESSION['form_id']."','$user_its','$time')";
  $db->query($query);
}


$courses = $quiz->get_all_courses();
$log_courses = $quiz->get_log_courses($user_its, $course_id);
$course_data = $quiz->get_course_by_id($course_id);
$miqaat_istibsaar = get_single_miqaat_istibsaar($course_data['miqaat_id']);

require_once 'inc/inc.header2.php';
require_once 'inc/inc.footer.php';

$smarty = new Smarty;

// Header / Session variables
$smarty->assign("server_path", SERVER_PATH);
$smarty->assign("course_id", $course_id);
$smarty->assign("log_courses", $log_courses);
$smarty->assign("course_data", $course_data);
$smarty->assign("total_score", $total_score);
$smarty->assign("course_questions", $course_questions);
$smarty->assign("miqaat_istibsaar", $miqaat_istibsaar);
$smarty->assign("courses", $courses);

$smarty->display('result.tpl');