<?php
  $user_logedin->loaduser($mumin_its);
?>
<div class="col-xs-12 col-md-12">
  <div class="col-xs-12 col-md-9">
    <?php
    if ($tasmee_records) {
      echo '<div id="columnchart_values"></div>';
    } else {
      echo 'No Data Available.';
    }
    ?>
    <div class="clearfix"></div>
    <div id="donutchart" style="height: 400px;"></div>
  </div>
  <div class="col-xs-12 col-md-3">
    <div class="text-center">
      <p>Current Sanad</p>
      <p><strong><?php echo $mumin_data[0]['mumin_quran_sanad']; ?></strong></p>

      <div class="clearfix"><br></div>

      <p>Currently at</p>
      <p>Surah: <strong><?php echo $qrn_tasmee->get_surat_name($mumin_data[0]['last_tasmee_surat']); ?></strong></p>
      <p>Aayaah: <strong><?php echo $mumin_data[0]['last_tasmee_ayat']; ?></strong></p>

      <p>Your Sadeeq Name</p>
      <p><strong><?php echo $mumin_data[0]['mumin_full_name']; ?></strong><p>
      <p>Email: <?php echo $user_logedin->get_email(); ?><p>
      <p>Mobile: <?php echo $user_logedin->get_mobile(); ?><p>
      <p>Whatsapp: <?php echo $user_logedin->get_whatsapp(); ?><p>
      <p>Skype: <?php echo $user_logedin->get_viber(); ?><p>
    </div>
  </div>
</div>

<script type="text/javascript">

  google.charts.setOnLoadCallback(drawColumnChart);
  function drawColumnChart() {
    var data = google.visualization.arrayToDataTable([
      ["Date", "Total Aayaat"]
        <?php
        if ($tasmee_records) {
          echo ',';
          foreach ($tasmee_records as $data) {
            echo '["' . date('d, M Y', strtotime($data['timestamp'])) . '", ' . $data['total_ayat'] . ']';
            if (count($tasmee_records) - 1) {
              echo ',';
            }
          }
        }
        ?>
    ]);

    var options = {
      colors: ['#337AB7'],
      pointSize: 10,
      pointShape: {type: 'point', rotation: 180},
      hAxis: {
        title: 'Date'
      },
      vAxis: {
        title: 'No. of Aayaat recited'
      }
    };

    var chart = new google.visualization.AreaChart(document.getElementById("columnchart_values"));
    chart.draw(data, options);
  }

  $(window).resize(function () {
    drawColumnChart();
  });

  var tasmee_ayat = <?php echo $mumin_data[0]['last_tasmee_ayat']; ?>;
  var remain_ayat = 6236 - tasmee_ayat;
  google.charts.setOnLoadCallback(drawPieChart);
  function drawPieChart() {
    var data = google.visualization.arrayToDataTable([
      ['Remaining Aayaat', 'Tasmee Aayaat'],
      ['Remaining Aayaat', remain_ayat],
      ['Tasmee Aayaat', tasmee_ayat]
    ]);

    var options = {
      title: 'Aayaat Completed',
      pieSliceText: 'value',
      pieHole: 0.4,
    };

    var chart = new google.visualization.PieChart(document.getElementById('donutchart'));
    chart.draw(data, options);
  }
</script>