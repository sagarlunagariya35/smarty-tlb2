<?php
if ($mumin_data_header AND $muhafiz_data_header) {
  $message_text = 'Tehfeez and Tasmee';
} else if ($muhafiz_data_header) {
  $message_text = 'Tehfeez';
} else if ($mumin_data_header) {
  $message_text = 'Tasmee';
} else {
  $message_text = '';
}

if ($message_text != '') {
  ?>
  <div class="text-right qrn-reg-wrapper">
    <div class="qrn-reg-header">
      <span>You are registered for <?php echo $message_text; ?>.</span>
    </div>
  </div>
  <?php
}
?>