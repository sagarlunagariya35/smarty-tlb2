<?php
//require_once '../classes/constants.php';
require_once './classes/class.qrn_mumin.php';
require_once './classes/class.qrn_muhafiz.php';
require_once './classes/class.survey.php';
require_once './classes/class.crs_topic.php';
require_once 'classes/class.user.php';

$qrn_mumin_data = new Qrn_Mumin();
$qrn_muhafiz_data = new Qrn_Muhafiz();
$srv_survey = new Mtx_Survey();
$mtx_crs_topic = new Crs_Topic();

$user_header = new mtx_user();
if (isset($_SESSION[USER_ITS])) {
  $user_header->loaduser($_SESSION[USER_ITS]);
}
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <!-- Meta Basics -->
    <!-- ====================================================================================================== -->
    <meta charset="utf-8" />
    <title>Talabul Ilm</title>
    <meta name="description" content />
    <meta name="keywords" content />
    <meta name="author" content />
    <!-- Mobile Specific Metas -->
    <!-- ====================================================================================================== -->
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
    <!-- Favicons -->
    <!-- ====================================================================================================== -->
    <link rel="shortcut icon" href="<?php echo SERVER_PATH; ?>images/favicons/favicon.png" />
    <link rel="apple-touch-icon" href="<?php echo SERVER_PATH; ?>images/favicons/apple-touch-icon.png" />
    <link rel="apple-touch-icon" sizes="72x72" href="<?php echo SERVER_PATH; ?>images/favicons/apple-touch-icon-72x72.png" />
    <link rel="apple-touch-icon" sizes="114x114" href="<?php echo SERVER_PATH; ?>images/favicons/apple-touch-icon-114x114.png" />
    <!-- Windows Tiles -->
    <!-- ====================================================================================================== -->
    <meta name="application-name" content="Talabul Ilm" />
    <meta name="msapplication-TileColor" content="#ffffff" />
    <meta name="msapplication-square70x70logo" content="<?php echo SERVER_PATH; ?>images/favicons/msapplication-tiny.png" />
    <meta name="msapplication-square150x150logo" content="<?php echo SERVER_PATH; ?>images/favicons/msapplication-square.png" />
    <!-- CSS -->
    <!-- ====================================================================================================== -->
    <link rel="stylesheet" href="<?php echo SERVER_PATH; ?>templates/css/custom.css?v=22" type="text/css" media="screen" />

    <!-- Fallbacks -->
    <!-- ====================================================================================================== -->
    <!--[if lt IE 9]>
    <script src="<?php echo SERVER_PATH; ?>templates/js/html5shiv.js"></script>
    <![endif]-->

    <script src="<?php echo SERVER_PATH; ?>templates/js/jquery.min.js"></script>
    <link rel="stylesheet" href="<?php echo SERVER_PATH; ?>templates/css/prettyPhoto.css" type="text/css" media="screen" title="prettyPhoto main stylesheet" charset="utf-8" />
    <script src="<?php echo SERVER_PATH; ?>templates/js/jquery.prettyPhoto.js" type="text/javascript" charset="utf-8"></script>


    <script src="<?php echo SERVER_PATH; ?>bootstrap/js/bootstrap.min.js"></script>
    <script src="<?php echo SERVER_PATH; ?>templates/js/jquery.slicknav.js"></script>
    <script src="<?php echo SERVER_PATH; ?>templates/js/jquery.nivo.slider.pack.js"></script>
    <script src="<?php echo SERVER_PATH; ?>templates/js/selectize.min.js"></script>
    <script src="<?php echo SERVER_PATH; ?>templates/js/scripts.js"></script>
    <script>
      $(document).ready(function () {
        $('[data-toggle="popover"]').popover({
          placement: 'bottom'
        });
        $('body').on('click', function (e) {
          $('[data-toggle="popover"]').each(function () {
            //the 'is' for buttons that trigger popups
            //the 'has' for icons within a button that triggers a popup
            if (!$(this).is(e.target) && $(this).has(e.target).length === 0 && $('.popover').has(e.target).length === 0) {
              $(this).popover('hide');
            }
          });
        });
      });
    </script>
    <link href="<?php echo SERVER_PATH; ?>templates/css/temp_style.css" media="all" rel="stylesheet" type="text/css" />
  </head>

  <body>

    <!-- IE 8 Blocker-->
    <!-- ====================================================================================================== -->
    <!--[if lt IE 9]>
      <div class="ie-old">
        <p class="center"><img alt="" src="<?php echo SERVER_PATH; ?>images/ie-fossil.png" /></p>
        <p class="center bigger1">Your browser is extinct!!</p>
        <p class="center"><span class="color1">This Website is not compatible with Internet Explorer 8 or below. </span></p>
        <p class="center"><span class="color8">Please <a target="_blank" href="http://www.microsoft.com/en-us/download/internet-explorer.aspx"><span class="link_1">upgrade</span></a> your browser or Download 
        <a target="_blank" href="https://www.google.com/intl/en/chrome/browser/"><span class="link_1">Google Chrome</span></a> or <a target="_blank" href="http://www.mozilla.org/en-US/firefox/new/"><span class="link_1">Mozilla Firefox</span></a></span></p>
      </div>
      <![endif]-->

    <!-- Loaders -->
    <!-- ====================================================================================================== -->
    <div id="preloader" style="display: block;">
      <div id="status" style="display: block;">
        <img src="<?php echo SERVER_PATH; ?>images/loader.png" class="animated pulse infinite"/>
        <h2>Loading<br />
          Please Wait...</h2>
      </div>
    </div>
    <!-- Screen Tilt Fallback -->
    <!-- ====================================================================================================== -->
    <div class="tilt-now">
      <h4>Arrgh!<br />
        <small>It seems your screen size is small.<br />
          Hold your device in Potrait Mode to Continue</small><img src="<?php echo SERVER_PATH; ?>images/smart-phone-128.png" class="img-responsive" /></h4>
    </div>

    <!-- Header -->
    <!-- ====================================================================================================== -->
    <div class="header-float">
      <div class="container">
        <nav class="nav"></nav> <!-- do not delete -->
        <header>
          <div class="col-md-4 col-sm-12">
            <a href="<?php echo SERVER_PATH.'index.php'; ?>" class="logo"></a>
          </div>
          <div class="col-md-8 col-sm-12" style="padding:0px;">

            <!-- different columns for responsive setting -->
            <!--            <div class="col-md-5 col-sm-12">
                          <ul class="top-menu">
                            <li><a href="<?php echo SERVER_PATH; ?>" class="active">Home</a></li>
                            <li><a href="<?php echo SERVER_PATH; ?>/about-us/">About Us</a></li>
                          </ul>
                        </div>-->
            <div class="text-right hidden-xs">
              <?php
              if (isset($_SESSION[USER_LOGGED_IN]) && $_SESSION[USER_LOGGED_IN] == TRUE) {
                $mumin_data_header = $qrn_mumin_data->get_all_mumin($_SESSION[USER_ITS]);
                $muhafiz_data_header = $qrn_muhafiz_data->get_all_muhafiz($_SESSION[USER_ITS]);
                
                if ($muhafiz_data_header) {
                  $link = SERVER_PATH.'qrn_tasmee.php';
                  
                } else if ($mumin_data_header) {
                  $link = SERVER_PATH.'qrn_report_mumin.php';
                  
                } else {
                  $link = SERVER_PATH.'qrn_cover.php';
                }
              ?>
              <a href="<?php echo $link; ?>" class="login">Al-Akh al-Qurani</a>
              <?php
                $my_araz = get_my_jawab_sent_araz_by_its($_SESSION[USER_ITS]);
                $hashcode = get_my_jawab_sent_araz_by_its($_SESSION[USER_ITS], 'code');
                $srv_jamaat = $user->get_jamaat();
                $srv_jamiat = $user->get_jamiat();
                $srv_marhala = $hashcode['marhala'];
                if ($srv_marhala < 5) {
                  $srv_school = $hashcode['school_name'];
                } else {
                  $aprv_inst = get_my_araz_approved_institutions_data($hashcode['id']);
                  $srv_school = $aprv_inst['institute_name'];
                }
                
                $survey_result = $srv_survey->get_open_surveys($_SESSION[USER_ITS], $srv_jamaat, $srv_jamiat, $srv_marhala, $srv_school);
                
                if ($survey_result) {
                  ?>
                  <a href="<?php echo SERVER_PATH; ?>srv_take_survey_mahad.php" class="login">Take Survey</a>
                  <?php
                }
                if ($my_araz) {
                  ?>
                  <a href="<?php echo SERVER_PATH; ?>araz-preview/<?php echo $my_araz['id'] ?>" class="login">My Araz</a>
                  <?php
                }
                if ($hashcode) {
                  ?>
                  <a target="_blank" href="<?php echo SERVER_PATH; ?>araz-jawab/<?php echo $hashcode['code']; ?>" class="login">My Araz Jawab</a>
                <?php } ?>
                <a href="<?php echo SERVER_PATH; ?>marahil/" class="<?php
                if (@$page == 'raza-araz') {
                  echo 'visit';
                } else {
                  echo 'login';
                }
                ?>">Send Araz To Aqa Maula TUS</a>
                <a href="<?php echo SERVER_PATH; ?>log-out/" class="login">Log out</a>
                <?php
              } else {
                ?>
                <a href="http://www.its52.com/ServiceLogin.aspx?domain=www.talabulilm.com&amp;continue=http://www.talabulilm.com/ITS/Authentication/" class="login">Login</a>
                <?php
              }
              ?>
            </div>
            <nav class="appmenu"></nav> <!-- do not delete -->
            <!-- different columns for responsive setting ends -->
            <div class="clearfix"></div> <!-- do not delete -->

            <?php
            $menus = get_all_menu();
            ?>

            <ul class="app-menu">

              <?php
              if (isset($_SESSION[USER_LOGGED_IN]) && $_SESSION[USER_LOGGED_IN] == TRUE) {
                foreach ($menus as $menu) {
                  $submenus = get_all_submenu($menu['id']);
                  ?>

                  <li class="<?php if ($submenus != '') { echo 'dropdown'; } ?>"><a href="<?php echo SERVER_PATH.$menu['slug']; ?>"><?php echo $menu['name']; ?></a>

                    <?php
                    if ($submenus != '') {
                      echo '<ul>';
                      foreach ($submenus as $submenu) {
                        if ($menu['slug'] == 'activities') {
                          $sub_submenu = get_article_by_submenu('article', $submenu['id']);
                        } else {
                          $sub_submenu = get_article_by_submenu('events', $submenu['id']);
                        }

                        $third_step_menus = get_all_third_step_menu($submenu['id']);
                        ?>

                        <li class="<?php if ($third_step_menus != '') { echo 'dropdown'; } ?>"><a href="<?php echo SERVER_PATH . $menu['slug'] . '/' . $submenu['slug'] . '/'; ?>"><?php echo $submenu['name']; ?></a>

                            <?php
                              if ($third_step_menus != '') {
                                echo '<ul>';
                                foreach ($third_step_menus as $thirdmenu) {
                                  ?>

                                  <li><a href="<?php echo SERVER_PATH . $menu['slug'] . '/' . $submenu['slug'] . '/' . $thirdmenu['slug'] . '/'; ?>"><?php echo $thirdmenu['name']; ?></a></li>

                                <?php
                              }
                              echo '</ul>';
                            }
                            ?>
                        </li>

                      <?php
                      if ($menu['name'] == 'Istifaadah') {
                      ?>
                        <li style="border-bottom: 1px dashed #FFF;">
                          <a href="<?php echo SERVER_PATH.$menu['slug']; ?>">Dashboard</a>
                        </li>
                      <?php
                        $mtx_topics = $mtx_crs_topic->get_all_topics();
                        if ($mtx_topics) {
                          foreach ($mtx_topics as $topic_name_header) {
                            ?>
                              <li><a href="<?php echo SERVER_PATH . 'crs_topics.php?topic_id=' . $topic_name_header['id']; ?>"><?php echo $topic_name_header['title']; ?></a></li>
                            <?php
                          }
                        }
                      }
                    }
                    echo '</ul>';
                  }
                  ?>
                  </li>
                  <?php
                }
              } else {
                ?>
                  <li><a href="http://www.its52.com/ServiceLogin.aspx?domain=www.talabulilm.com&amp;continue=http://www.talabulilm.com/ITS/Authentication/">Click Here to Login</a></li>
              <?php
              }
              ?>
              
              <?php
              if (isset($_SESSION[USER_LOGGED_IN]) && $_SESSION[USER_LOGGED_IN] == TRUE) {
                $mumin_data_header = $qrn_mumin_data->get_all_mumin($_SESSION[USER_ITS]);
                $muhafiz_data_header = $qrn_muhafiz_data->get_all_muhafiz($_SESSION[USER_ITS]);
                
                if ($muhafiz_data_header) {
                  $link = SERVER_PATH.'qrn_tasmee.php';
                  
                } else if ($mumin_data_header) {
                  $link = SERVER_PATH.'qrn_report_mumin.php';
                  
                } else {
                  $link = SERVER_PATH.'qrn_cover.php';
                }
              ?>
                <li class="visible-xs-block"><a href="<?php echo $link; ?>">Al-Akh al-Qurani</a></li>
              <?php
                $my_araz = get_my_jawab_sent_araz_by_its($_SESSION[USER_ITS]);
                $hashcode = get_my_jawab_sent_araz_by_its($_SESSION[USER_ITS], 'code');
                $srv_jamaat = $user_header->get_jamaat();
                $srv_jamiat = $user_header->get_jamiat();
                $srv_marhala = $hashcode['marhala'];
                if ($srv_marhala < 5) {
                  $srv_school = $hashcode['school_name'];
                } else {
                  $aprv_inst = get_my_araz_approved_institutions_data($hashcode['id']);
                  $srv_school = $aprv_inst['institute_name'];
                }
                
                $survey_result = $srv_survey->get_open_surveys($_SESSION[USER_ITS], $srv_jamaat, $srv_jamiat, $srv_marhala, $srv_school);
                
                if ($survey_result) {
                  ?>
                <li class="visible-xs-block"><a href="<?php echo SERVER_PATH; ?>srv_take_survey_mahad.php">Take Survey</a></li>
                  <?php
                }
                if ($my_araz) {
                  ?>
                <li class="visible-xs-block"><a href="<?php echo SERVER_PATH; ?>my_araz_preview.php?araz=<?php echo $my_araz['id'] ?>">My Araz</a></li>
                  <?php
                }
                if ($hashcode) {
                  ?>
                <li class="visible-xs-block"><a target="_blank" href="<?php echo SERVER_PATH; ?>admin/print_araiz.php?code=<?php echo $hashcode['code']; ?>">My Araz Jawab</a></li>
                <?php } ?>
                <li class="visible-xs-block"><a href="<?php echo SERVER_PATH; ?>marahil/">Send Araz To Aqa Maula TUS</a></li>
                <li class="visible-xs-block"><a href="<?php echo SERVER_PATH; ?>log-out/">Log out</a></li>
                <?php
              } else {
                ?>
                <li class="visible-xs-block"><a href="http://www.its52.com/ServiceLogin.aspx?domain=www.talabulilm.com&amp;continue=http://www.talabulilm.com/ITS/Authentication/">Login</a></li>
                <?php
              }
              ?>
            </ul>
          </div>
        </header>
      </div>
    </div>
    <div class="clearfix"></div> <!-- do not delete -->

    <!-- Banners -->
    <!-- ====================================================================================================== -->
    <?php
      $images_carousal = get_carousal_images_by_category(HOME_CAROUSAL);
    ?>
    <div class="container white-bg">
      <div class="banners">
        <div id="banners" class="nivoSlider">
          <?php
            if ($images_carousal) {
              foreach ($images_carousal as $image) {
          ?>
            <a href="<?php 
            if (!isset($_SESSION[USER_LOGGED_IN]) || $_SESSION[USER_LOGGED_IN] == FALSE) {
              echo 'http://www.its52.com/ServiceLogin.aspx?domain=www.talabulilm.com&amp;continue=http://www.talabulilm.com/ITS/Authentication/';
            } else {
              echo $image['link'];
            }
            ?>"><img src="<?php echo SERVER_PATH.'images/banners/'.$image['image']; ?>" alt="" title="#htmlcaption2"/></a>
          <?php
              }
            }
          ?>
        </div>
      </div>
    </div>
    <div class="clearfix"></div> <!-- do not delete -->
