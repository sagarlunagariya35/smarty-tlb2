<div class="col-xs-12 col-md-12">
  <div class="table-responsive">
    <table class="table table-bordered table-hover tlb_muhafiz">
      <thead>
        <tr>
          <th>Sr No</th>
          <th>Date</th>
          <th>Mumin ITS</th>
          <th>Surah</th>
          <th>Aayat</th>
          <th>Total No. of Aayaat</th>
        </tr>
      </thead>
      <tbody>
        <?php
        if ($tasmee_records) {
          $i = 1;
          foreach ($tasmee_records as $tr) {
            ?>
            <tr>
              <td><?php echo $i++; ?></td>
              <td><?php echo date('d, F Y', strtotime($tr['timestamp'])); ?></td>
              <td><?php echo $tr['mumin_its'] ?></td>
              <td><?php echo $qrn_tasmee->get_surat_name($tr['to_surat']); ?></td>
              <td><?php echo $tr['to_ayat'] ?></td>
              <td><?php echo $tr['total_ayat'] ?></td>
            </tr>
            <?php
          }
        }
        ?>
      </tbody>
    </table>
  </div>
</div>
<script>
  $('.tlb_muhafiz').DataTable({
    "paging": true,
    "lengthChange": false,
    "searching": true,
    "ordering": false,
    "info": true,
    "autoWidth": true,
    language: {
      searchPlaceholder: "Search"
    }
  });
</script>