<div class="col-xs-12 col-md-12 table-responsive">
    <table class="table table-bordered table-hover tlb_mumin">
      <thead>
        <tr>
          <th rowspan="2">Date</th>
          <th rowspan="2">Muhafiz ITS</th>
          <th colspan="2">Status</th>
          <th rowspan="2">Total Tasmee Ayat</th>
        </tr>
        <tr>
          <th>Surah</th>
          <th>Ayat</th>
        </tr>
      </thead>
      <tbody>
        <?php
        if ($tasmee_records) {
          $i = 1;
          foreach ($tasmee_records as $tr) {
            ?>
            <tr>
              <td><?php echo date('d, F Y', strtotime($tr['timestamp'])); ?></td>
              <td><?php echo $tr['muhafiz_its'] ?></td>
              <td><?php echo $qrn_tasmee->get_surat_name($tr['to_surat']); ?></td>
              <td><?php echo $tr['to_ayat'] ?></td>
              <td><?php echo $tr['total_ayat'] ?></td>
            </tr>
            <?php
          }
        }
        ?>
      </tbody>
    </table>
</div>
<script>
  $('.tlb_mumin').DataTable({
    "paging": true,
    "lengthChange": false,
    "searching": true,
    "ordering": false,
    "info": true,
    "autoWidth": true,
    language: {
      searchPlaceholder: "Search"
    }
  });
</script>