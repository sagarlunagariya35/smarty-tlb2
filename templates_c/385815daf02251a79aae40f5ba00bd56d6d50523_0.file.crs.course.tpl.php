<?php
/* Smarty version 3.1.29, created on 2016-04-11 15:28:48
  from "/var/www/html/smarty_tlb2/templates/include/crs.course.tpl" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.29',
  'unifunc' => 'content_570b75584702c0_15834041',
  'file_dependency' => 
  array (
    '385815daf02251a79aae40f5ba00bd56d6d50523' => 
    array (
      0 => '/var/www/html/smarty_tlb2/templates/include/crs.course.tpl',
      1 => 1460368692,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_570b75584702c0_15834041 ($_smarty_tpl) {
if ($_smarty_tpl->tpl_vars['courses']->value) {?>
  <?php
$_from = $_smarty_tpl->tpl_vars['courses']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_course_0_saved_item = isset($_smarty_tpl->tpl_vars['course']) ? $_smarty_tpl->tpl_vars['course'] : false;
$_smarty_tpl->tpl_vars['course'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['course']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['course']->value) {
$_smarty_tpl->tpl_vars['course']->_loop = true;
$__foreach_course_0_saved_local_item = $_smarty_tpl->tpl_vars['course'];
?>
    <?php $_smarty_tpl->tpl_vars['topic_title'] = new Smarty_Variable($_smarty_tpl->tpl_vars['crs_topic']->value->get_topic($_smarty_tpl->tpl_vars['course']->value['topic_id']), null);
$_smarty_tpl->ext->_updateScope->updateScope($_smarty_tpl, 'topic_title', 0);?>
    <div class="course-block">
        <div class="course-sum">
            <h3 class="text-blue"><?php echo $_smarty_tpl->tpl_vars['course']->value['title'];?>
</h3>
            <h5><?php echo $_smarty_tpl->tpl_vars['topic_title']->value['title'];?>
</h5>
            <p><?php echo $_smarty_tpl->tpl_vars['course']->value['description'];?>
</p>
            <ul class="course-count-list">
                <li><span class="count">6</span> Badges available</li>
                <li><span class="count">9</span> Assessments available</li>
            </ul>
        </div>
        <?php $_smarty_tpl->tpl_vars['course_chapters'] = new Smarty_Variable($_smarty_tpl->tpl_vars['crs_chapter']->value->get_all_chapters($_smarty_tpl->tpl_vars['course']->value['id']), null);
$_smarty_tpl->ext->_updateScope->updateScope($_smarty_tpl, 'course_chapters', 0);?>
        <div class="course-prog-wrap">
            <a href="#course-prog1" data-toggle="collapse" class="course-list-toggle">Course program <i class="glyphicon glyphicon-chevron-left"></i></a>
            <div class="course-progs-list collapse in" id="course-prog1">
                <?php if ($_smarty_tpl->tpl_vars['course_chapters']->value) {?>
                  <?php $_smarty_tpl->tpl_vars["c"] = new Smarty_Variable(1, null);
$_smarty_tpl->ext->_updateScope->updateScope($_smarty_tpl, "c", 0);?>
                    <?php
$_from = $_smarty_tpl->tpl_vars['course_chapters']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_chapter_data_1_saved_item = isset($_smarty_tpl->tpl_vars['chapter_data']) ? $_smarty_tpl->tpl_vars['chapter_data'] : false;
$_smarty_tpl->tpl_vars['chapter_data'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['chapter_data']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['chapter_data']->value) {
$_smarty_tpl->tpl_vars['chapter_data']->_loop = true;
$__foreach_chapter_data_1_saved_local_item = $_smarty_tpl->tpl_vars['chapter_data'];
?>
                      <a class="prog-name" href="#html-<?php echo $_smarty_tpl->tpl_vars['c']->value;?>
" data-toggle="collapse"><span><?php echo $_smarty_tpl->tpl_vars['c']->value;?>
.</span> <?php echo $_smarty_tpl->tpl_vars['chapter_data']->value['title'];?>
 <i class="glyphicon glyphicon-chevron-left"></i></a>
                      <div class="prog-content collapse <?php if ($_smarty_tpl->tpl_vars['c']->value == '1') {?>in<?php }?>" id="html-<?php echo $_smarty_tpl->tpl_vars['c']->value;?>
">
                        <ul>
                          <?php $_smarty_tpl->tpl_vars['chapter_elements'] = new Smarty_Variable($_smarty_tpl->tpl_vars['crs_chapter']->value->get_all_chapter_elements($_smarty_tpl->tpl_vars['chapter_data']->value['id']), null);
$_smarty_tpl->ext->_updateScope->updateScope($_smarty_tpl, 'chapter_elements', 0);?>
                          <?php if ($_smarty_tpl->tpl_vars['chapter_elements']->value) {?>
                            <?php
$_from = $_smarty_tpl->tpl_vars['chapter_elements']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_element_2_saved_item = isset($_smarty_tpl->tpl_vars['element']) ? $_smarty_tpl->tpl_vars['element'] : false;
$_smarty_tpl->tpl_vars['element'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['element']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['element']->value) {
$_smarty_tpl->tpl_vars['element']->_loop = true;
$__foreach_element_2_saved_local_item = $_smarty_tpl->tpl_vars['element'];
?>
                              <li><a href="javascript:void(0)"><i class="fa fa-file-text-o"></i>  <?php echo $_smarty_tpl->tpl_vars['element']->value['element_title'];?>
</a></li>
                            <?php
$_smarty_tpl->tpl_vars['element'] = $__foreach_element_2_saved_local_item;
}
if ($__foreach_element_2_saved_item) {
$_smarty_tpl->tpl_vars['element'] = $__foreach_element_2_saved_item;
}
?>
                          <?php }?>
                        </ul>
                      </div>
                    <?php $_smarty_tpl->tpl_vars['c'] = new Smarty_Variable($_smarty_tpl->tpl_vars['c']->value+1, null);
$_smarty_tpl->ext->_updateScope->updateScope($_smarty_tpl, 'c', 0);?>
                  <?php
$_smarty_tpl->tpl_vars['chapter_data'] = $__foreach_chapter_data_1_saved_local_item;
}
if ($__foreach_chapter_data_1_saved_item) {
$_smarty_tpl->tpl_vars['chapter_data'] = $__foreach_chapter_data_1_saved_item;
}
?>
                <?php }?>
            </div>
            <div class="course-prog-foot clearfix">
                <ul>
                    <li><i class="fa fa-calendar"></i> Start Date: <b><?php echo $_smarty_tpl->tpl_vars['course']->value['start_date'];?>
</b></li>
                    <li><i class="fa fa-money"></i> Price: <b class="price-text"><?php echo $_smarty_tpl->tpl_vars['course']->value['fee'];?>
</b></li>
                </ul>
                <div class="action">
                    <a class="orange-btn btn" href="javascript:void(0);">Enroll now</a>
                </div>
            </div>
        </div>
    </div>
  <?php
$_smarty_tpl->tpl_vars['course'] = $__foreach_course_0_saved_local_item;
}
if ($__foreach_course_0_saved_item) {
$_smarty_tpl->tpl_vars['course'] = $__foreach_course_0_saved_item;
}
}
}
}
