<?php
/* Smarty version 3.1.29, created on 2016-03-25 16:57:54
  from "/var/www/html/smarty_tlb2/templates/miqaat_istibsaar.tpl" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.29',
  'unifunc' => 'content_56f520ba396cc6_36295601',
  'file_dependency' => 
  array (
    '9a8ce297a30f273b1a6f046588ae5ed0ad84eee4' => 
    array (
      0 => '/var/www/html/smarty_tlb2/templates/miqaat_istibsaar.tpl',
      1 => 1456833535,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:include/js_block.tpl' => 1,
    'file:include/footer.tpl' => 1,
  ),
),false)) {
function content_56f520ba396cc6_36295601 ($_smarty_tpl) {
?>
<div class="container white-bg">
  <div class="col-md-12 col-sm-12">

    <div class="page-title">
      <p style="margin-top:5px;">
        <a href="#" class="active">Istibsaar <?php echo $_smarty_tpl->tpl_vars['year']->value;?>
 H</a></p>
      <h1>Istibsaar <?php echo $_smarty_tpl->tpl_vars['year']->value;?>
 H <span class="alfatemi-text">الاستبصار</span></h1>
    </div>
  </div>
  <div class="clearfix"></div> <!-- do not delete -->
  <form class="forms1 white" name="raza_form" method="post">
    <div class="col-md-3 col-sm-12 hidden-xs">
      <div class="profile-box-static">
        <h3 class="uppercase text-center">Years</h3>
      </div>
      <div class="profile-box-static-bottom">
        <a href="<?php echo SERVER_PATH;?>
istibsaar/1436/">» 1436 H<hr></a>
        <a href="<?php echo SERVER_PATH;?>
istibsaar/1437/">» 1437 H<hr></a>
      </div>
    </div>

    <div class="col-md-8 col-sm-12">      
        <?php if ($_smarty_tpl->tpl_vars['miqaat_istibsaars']->value) {?>
          <?php $_smarty_tpl->tpl_vars["i"] = new Smarty_Variable(1, null);
$_smarty_tpl->ext->_updateScope->updateScope($_smarty_tpl, "i", 0);?>
          <?php
$_from = $_smarty_tpl->tpl_vars['miqaat_istibsaars']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_mi_0_saved_item = isset($_smarty_tpl->tpl_vars['mi']) ? $_smarty_tpl->tpl_vars['mi'] : false;
$_smarty_tpl->tpl_vars['mi'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['mi']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['mi']->value) {
$_smarty_tpl->tpl_vars['mi']->_loop = true;
$__foreach_mi_0_saved_local_item = $_smarty_tpl->tpl_vars['mi'];
?>
            <div class="col-md-4">
              <a href="<?php echo SERVER_PATH;?>
miqaat_content.php?miqaat_id=<?php echo $_smarty_tpl->tpl_vars['mi']->value['id'];?>
">
              <div class="istibsaar-boxes text-center">
                <img src="<?php echo SERVER_PATH;?>
upload/miqaat_upload/logo/<?php echo $_smarty_tpl->tpl_vars['mi']->value['logo'];?>
" class="img-responsive img-mts">
                <div class="istibsaar-boxes-bottom">
                  <p class="text-center"><?php echo $_smarty_tpl->tpl_vars['mi']->value['miqaat_title'];?>
</p>
                  <div class="clearfix"></div>
                  <p></p>
                  <p class="text-center black_font" style="color:#000;"><?php echo $_smarty_tpl->tpl_vars['mi']->value['desc'];?>
</p>
                </div>
              </div>
              </a>
              <div class="clearfix"></div>
            </div>
          <?php if (($_smarty_tpl->tpl_vars['i']->value++%3 == 0)) {?> 
            <div class="clearfix"><br></div><div class="clearfix"><br></div>
          <?php }?>
          <?php
$_smarty_tpl->tpl_vars['mi'] = $__foreach_mi_0_saved_local_item;
}
if ($__foreach_mi_0_saved_item) {
$_smarty_tpl->tpl_vars['mi'] = $__foreach_mi_0_saved_item;
}
?>
        <?php }?>
    </div>
  </form>
</div>

<?php $_smarty_tpl->smarty->ext->_subtemplate->render($_smarty_tpl, "file:include/js_block.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>


<style>
  .istibsaar-boxes-bottom:hover{
    background-color: #549BC7;
  }
</style>
<?php $_smarty_tpl->smarty->ext->_subtemplate->render($_smarty_tpl, "file:include/footer.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
}
}
