<?php
/* Smarty version 3.1.29, created on 2016-04-01 16:34:25
  from "/var/www/html/smarty_tlb2/templates/include/qrn_links.tpl" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.29',
  'unifunc' => 'content_56fe55b97243b0_16792434',
  'file_dependency' => 
  array (
    'ae46f7ff8067b56fb7a3dfca9aac2f63e0f97721' => 
    array (
      0 => '/var/www/html/smarty_tlb2/templates/include/qrn_links.tpl',
      1 => 1459318074,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_56fe55b97243b0_16792434 ($_smarty_tpl) {
?>
<div class="clearfix"><br></div>
<div class="row">
  <div class="col-xs-12 col-sm-12">
    <div class="text-center qrn-nav-wrapper">
      <div class="qrn-nav-container">
          <?php if (($_smarty_tpl->tpl_vars['mumin_data_header']->value == FALSE && $_smarty_tpl->tpl_vars['muhafiz_data_header']->value == FALSE)) {?>
            <?php $_smarty_tpl->tpl_vars['enroll_text'] = new Smarty_Variable('Enroll', null);
$_smarty_tpl->ext->_updateScope->updateScope($_smarty_tpl, 'enroll_text', 0);?>
          <?php } elseif (($_smarty_tpl->tpl_vars['mumin_data_header']->value && $_smarty_tpl->tpl_vars['muhafiz_data_header']->value)) {?>
            <?php $_smarty_tpl->tpl_vars['enroll_text'] = new Smarty_Variable('Unregister', null);
$_smarty_tpl->ext->_updateScope->updateScope($_smarty_tpl, 'enroll_text', 0);?>
          <?php } else { ?>
            <?php $_smarty_tpl->tpl_vars['enroll_text'] = new Smarty_Variable('Enroll / Unregister', null);
$_smarty_tpl->ext->_updateScope->updateScope($_smarty_tpl, 'enroll_text', 0);?>
          <?php }?>
        <a href="<?php echo $_smarty_tpl->tpl_vars['server_path']->value;?>
qrn_cover.php"><i class="fa fa-2x fa-question-circle"></i><span>Info</span></a> &nbsp;
        <a href="<?php echo $_smarty_tpl->tpl_vars['server_path']->value;?>
qrn_enroll.php"><i class="fa fa-2x fa-user"></i><span><?php echo $_smarty_tpl->tpl_vars['enroll_text']->value;?>
</span></a> &nbsp;
        <span class="__separator"></span>
          <?php if ($_smarty_tpl->tpl_vars['mumin_data_header']->value) {?>
        <a href="<?php echo $_smarty_tpl->tpl_vars['server_path']->value;?>
qrn_select_muhaffiz.php"><i class="fa fa-2x fa-user-plus"></i><span>Select Muhaffiz</span></a> &nbsp;
        <a href="<?php echo $_smarty_tpl->tpl_vars['server_path']->value;?>
qrn_report_mumin.php"><i class="fa fa-2x fa-line-chart"></i><span>Tasmee Report</span></a> &nbsp;
        <span class="__separator"></span>
          <?php }?>
          <?php if ($_smarty_tpl->tpl_vars['muhafiz_data_header']->value) {?>
        <a href="<?php echo $_smarty_tpl->tpl_vars['server_path']->value;?>
qrn_tasmee.php"><i class="fa fa-2x fa-bookmark"></i><span>Listen</span></a> &nbsp;
        <a href="<?php echo $_smarty_tpl->tpl_vars['server_path']->value;?>
qrn_report_muhafiz.php"><i class="fa fa-2x fa-bar-chart"></i><span>Tehfeez Report</span></a> &nbsp;
        <a href="<?php echo $_smarty_tpl->tpl_vars['server_path']->value;?>
qrn_ikhwaan_report.php"><i class="fa fa-2x fa-users"></i><span>Ikhwan Report</span></a> &nbsp;
          <?php }?>
      </div>
    </div>
  </div>
</div>
<div class="clearfix"></div>
<div class="text-center qrn-reg-wrapper">
  <div class="qrn-reg-header">
    <span>For any complaints or queries Contact us at <a href="mailto:akhqurani@talabulilm.com">akhqurani@talabulilm.com</a></span>
  </div>
</div>
<div class="clearfix"></div><?php }
}
