<?php
/* Smarty version 3.1.29, created on 2016-03-25 12:42:05
  from "/var/www/html/smarty_tlb2/templates/courses_question.tpl" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.29',
  'unifunc' => 'content_56f4e4c5cfba87_91095753',
  'file_dependency' => 
  array (
    '582f9838e59987ae3ee0a6c706e630c6014ed256' => 
    array (
      0 => '/var/www/html/smarty_tlb2/templates/courses_question.tpl',
      1 => 1458369215,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_56f4e4c5cfba87_91095753 ($_smarty_tpl) {
?>
<div id="question_panel">
  <?php if (($_smarty_tpl->tpl_vars['questions']->value['question_type'] != QUIZ_GENERAL_HTML)) {?>
  <div class="blue-box1 rtl border"> <!-- Add class rtl -->
    <h3><span class="inline-block lh3 arb_ques"><?php echo $_smarty_tpl->tpl_vars['questions']->value['question'];?>
</span></h3>
  </div>

  <input type="hidden" id="current_que_id" value="<?php echo $_smarty_tpl->tpl_vars['questions']->value['id'];?>
">
  <input type="hidden" id="current_course_id" value="<?php echo $_smarty_tpl->tpl_vars['questions']->value['course_id'];?>
">

  <?php $_smarty_tpl->tpl_vars['options'] = new Smarty_Variable(unserialize($_smarty_tpl->tpl_vars['questions']->value['options']), null);
$_smarty_tpl->ext->_updateScope->updateScope($_smarty_tpl, 'options', 0);?>
  <?php if (($_smarty_tpl->tpl_vars['questions']->value['question_type'] == QUIZ_MULTIPLE_CHOICE || $_smarty_tpl->tpl_vars['questions']->value['question_type'] == QUIZ_TRUE_AND_FALSE)) {?>
    <?php $_smarty_tpl->tpl_vars["m"] = new Smarty_Variable(0, null);
$_smarty_tpl->ext->_updateScope->updateScope($_smarty_tpl, "m", 0);?>
    <?php $_smarty_tpl->tpl_vars["selected"] = new Smarty_Variable('', null);
$_smarty_tpl->ext->_updateScope->updateScope($_smarty_tpl, "selected", 0);?>
    <?php
$_from = $_smarty_tpl->tpl_vars['options']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_opt_0_saved_item = isset($_smarty_tpl->tpl_vars['opt']) ? $_smarty_tpl->tpl_vars['opt'] : false;
$_smarty_tpl->tpl_vars['opt'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['opt']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['opt']->value) {
$_smarty_tpl->tpl_vars['opt']->_loop = true;
$__foreach_opt_0_saved_local_item = $_smarty_tpl->tpl_vars['opt'];
?>
      <?php $_smarty_tpl->tpl_vars["m"] = new Smarty_Variable($_smarty_tpl->tpl_vars['m']->value+1, null);
$_smarty_tpl->ext->_updateScope->updateScope($_smarty_tpl, "m", 0);?>
      <?php if (($_smarty_tpl->tpl_vars['pre_fill_user_answers']->value['submitted_answer'] == $_smarty_tpl->tpl_vars['opt']->value)) {?>
        <?php $_smarty_tpl->tpl_vars["selected"] = new Smarty_Variable("checked", null);
$_smarty_tpl->ext->_updateScope->updateScope($_smarty_tpl, "selected", 0);?>
      <?php }?>
      <div class="col-md-12 col-sm-12">
        <div class="quiz-point-radio rtl"> <!-- Add Class .rtl -->
          <input type="radio" name="your_answer" value="<?php echo $_smarty_tpl->tpl_vars['opt']->value;?>
" id="radio1<?php echo $_smarty_tpl->tpl_vars['m']->value;?>
" class="" <?php echo $_smarty_tpl->tpl_vars['selected']->value;?>
/>
          <label for="radio1<?php echo $_smarty_tpl->tpl_vars['m']->value;?>
" style="color: #000;" class="radGroup3 lsd"><?php echo $_smarty_tpl->tpl_vars['opt']->value;?>
</label>
        </div>
      </div>
    <?php
$_smarty_tpl->tpl_vars['opt'] = $__foreach_opt_0_saved_local_item;
}
if ($__foreach_opt_0_saved_item) {
$_smarty_tpl->tpl_vars['opt'] = $__foreach_opt_0_saved_item;
}
?>
  <?php }?>

  <?php if (($_smarty_tpl->tpl_vars['questions']->value['question_type'] == QUIZ_FILL_IN_THE_BLANK)) {?>
    <?php if ((count($_smarty_tpl->tpl_vars['options']->value) == 1 && $_smarty_tpl->tpl_vars['options']->value[0] == '')) {?>
      <div class="col-md-12 col-sm-12">&nbsp;</div>
      <div class="col-md-12 col-sm-12">
        <input type="text" name="your_answer" id="your_answer" class="form-control" value="<?php echo $_smarty_tpl->tpl_vars['pre_fill_user_answers']->value['submitted_answer'];?>
">
      </div>
    <?php } else { ?>
      <?php $_smarty_tpl->tpl_vars["selected"] = new Smarty_Variable('', null);
$_smarty_tpl->ext->_updateScope->updateScope($_smarty_tpl, "selected", 0);?>
      <div class="col-md-12 col-sm-12">&nbsp;</div>
      <div class="col-md-12 col-sm-12">
        <select name="your_answer" id="your_answer" class="form-control select-input">
          <option value="">Select Answer</option>
            <?php
$_from = $_smarty_tpl->tpl_vars['options']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_opt_1_saved_item = isset($_smarty_tpl->tpl_vars['opt']) ? $_smarty_tpl->tpl_vars['opt'] : false;
$_smarty_tpl->tpl_vars['opt'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['opt']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['opt']->value) {
$_smarty_tpl->tpl_vars['opt']->_loop = true;
$__foreach_opt_1_saved_local_item = $_smarty_tpl->tpl_vars['opt'];
?>
              <?php if (($_smarty_tpl->tpl_vars['pre_fill_user_answers']->value['submitted_answer'] == $_smarty_tpl->tpl_vars['opt']->value)) {?>
                <?php $_smarty_tpl->tpl_vars["selected"] = new Smarty_Variable("checked", null);
$_smarty_tpl->ext->_updateScope->updateScope($_smarty_tpl, "selected", 0);?>
              <?php }?>
            <option value="<?php echo $_smarty_tpl->tpl_vars['opt']->value;?>
" <?php echo $_smarty_tpl->tpl_vars['selected']->value;?>
><?php echo $_smarty_tpl->tpl_vars['opt']->value;?>
</option>
            <?php
$_smarty_tpl->tpl_vars['opt'] = $__foreach_opt_1_saved_local_item;
}
if ($__foreach_opt_1_saved_item) {
$_smarty_tpl->tpl_vars['opt'] = $__foreach_opt_1_saved_item;
}
?>
        </select>
      </div>
    <?php }?>
  <?php }?>

  <?php if (($_smarty_tpl->tpl_vars['questions']->value['question_type'] == QUIZ_MATCH_THE_FOLLOWING)) {?>
    <div class="col-md-12 col-sm-12">
      <div id="dragScriptContainer">
        <div id="questionDiv">
        <?php $_smarty_tpl->tpl_vars["m"] = new Smarty_Variable(0, null);
$_smarty_tpl->ext->_updateScope->updateScope($_smarty_tpl, "m", 0);?>
        <?php $_smarty_tpl->tpl_vars["selected"] = new Smarty_Variable('', null);
$_smarty_tpl->ext->_updateScope->updateScope($_smarty_tpl, "selected", 0);?>
          <?php
$_from = $_smarty_tpl->tpl_vars['options']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_opt_2_saved_item = isset($_smarty_tpl->tpl_vars['opt']) ? $_smarty_tpl->tpl_vars['opt'] : false;
$_smarty_tpl->tpl_vars['opt'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['opt']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['opt']->value) {
$_smarty_tpl->tpl_vars['opt']->_loop = true;
$__foreach_opt_2_saved_local_item = $_smarty_tpl->tpl_vars['opt'];
?>
            <?php $_smarty_tpl->tpl_vars["m"] = new Smarty_Variable($_smarty_tpl->tpl_vars['m']->value+1, null);
$_smarty_tpl->ext->_updateScope->updateScope($_smarty_tpl, "m", 0);?>
            <div class="dragDropSmallBox" id="q<?php echo $_smarty_tpl->tpl_vars['m']->value;?>
"><?php echo $_smarty_tpl->tpl_vars['opt']->value;?>
</div>
            <div class="destinationBox"></div>
          <?php
$_smarty_tpl->tpl_vars['opt'] = $__foreach_opt_2_saved_local_item;
}
if ($__foreach_opt_2_saved_item) {
$_smarty_tpl->tpl_vars['opt'] = $__foreach_opt_2_saved_item;
}
?>
        </div>
        <?php $_smarty_tpl->tpl_vars['ans_options'] = new Smarty_Variable(unserialize($_smarty_tpl->tpl_vars['questions']->value['correct_answer']), null);
$_smarty_tpl->ext->_updateScope->updateScope($_smarty_tpl, 'ans_options', 0);?>
        
        <div id="answerDiv">
          <?php $_smarty_tpl->tpl_vars["a"] = new Smarty_Variable(0, null);
$_smarty_tpl->ext->_updateScope->updateScope($_smarty_tpl, "a", 0);?>
          <?php
$_from = $_smarty_tpl->tpl_vars['ans_options']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_opt_3_saved_item = isset($_smarty_tpl->tpl_vars['opt']) ? $_smarty_tpl->tpl_vars['opt'] : false;
$_smarty_tpl->tpl_vars['opt'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['opt']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['opt']->value) {
$_smarty_tpl->tpl_vars['opt']->_loop = true;
$__foreach_opt_3_saved_local_item = $_smarty_tpl->tpl_vars['opt'];
?>
            <?php $_smarty_tpl->tpl_vars["a"] = new Smarty_Variable($_smarty_tpl->tpl_vars['a']->value+1, null);
$_smarty_tpl->ext->_updateScope->updateScope($_smarty_tpl, "a", 0);?>
            <div class="dragDropSmallBox" id="a<?php echo $_smarty_tpl->tpl_vars['a']->value;?>
"><?php echo $_smarty_tpl->tpl_vars['opt']->value;?>
</div>
          <?php
$_smarty_tpl->tpl_vars['opt'] = $__foreach_opt_3_saved_local_item;
}
if ($__foreach_opt_3_saved_item) {
$_smarty_tpl->tpl_vars['opt'] = $__foreach_opt_3_saved_item;
}
?>
        </div>
        <div id="dragContent"></div>
      </div>
      <input type="hidden" value="<?php echo $_smarty_tpl->tpl_vars['pre_fill_user_answers']->value['submitted_answer'];?>
" name="your_answer" id="your_answer" class="form-control">
    </div>

  <?php }?>
  
  <?php if (($_smarty_tpl->tpl_vars['questions']->value['question_type'] == QUIZ_OPEN_ANSWERS)) {?>
    <div class="col-md-12 col-sm-12">&nbsp;</div>
    <div class="col-md-12 col-sm-12">
      <input type="text" name="your_answer" id="your_answer" class="form-control" value="<?php echo $_smarty_tpl->tpl_vars['pre_fill_user_answers']->value['submitted_answer'];?>
">
    </div>
  <?php }
} else { ?>
  <?php echo $_smarty_tpl->tpl_vars['questions']->value['question'];?>

<?php }?>
</div>

<div class="col-md-12 col-sm-12"><br></div>
<div class="col-md-12 col-sm-12">
  <button type="button" id="next" class="btn btn-success pull-right col-md-2">Next</button>
</div>
<div class="col-md-12 col-sm-12"><br></div>
<div class="clearfix"></div>

<?php if (($_smarty_tpl->tpl_vars['questions']->value['timer'] != 0)) {?>
  <?php echo '<script'; ?>
 type="text/javascript">
    secs = <?php echo $_smarty_tpl->tpl_vars['questions']->value['timer'];?>
;
    timer = setInterval(function () {
        if(secs < 1){
            clearInterval(timer);
            document.getElementById('next').click();
        }
        secs--;
    }, 1000);
  <?php echo '</script'; ?>
>
<?php }
}
}
