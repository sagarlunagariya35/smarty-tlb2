<?php
/* Smarty version 3.1.29, created on 2016-02-23 14:24:26
  from "/var/www/html/smarty_tlb2/templates/institutes.tpl" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.29',
  'unifunc' => 'content_56cc1e4276d534_42394115',
  'file_dependency' => 
  array (
    'c5c20140bf435c689eeed240cf1786f69585dbbf' => 
    array (
      0 => '/var/www/html/smarty_tlb2/templates/institutes.tpl',
      1 => 1456217631,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_56cc1e4276d534_42394115 ($_smarty_tpl) {
if (!is_callable('smarty_function_counter')) require_once '/var/www/html/smarty_tlb2/smarty/libs/plugins/function.counter.php';
?>
<div class="container white-bg">
  <div class="col-md-12 col-sm-12">

    <div class="page-title">
      <p style="margin-top:5px;">
        <a href="#" class="active">Academic Institutes - List Of Imaani Schools</a>
      </p>

      <h1>Academic Institutes - List Of Imaani Schools</h1>
    </div>
  </div>
  <div class="clearfix"></div> <!-- do not delete -->
  <div class="col-md-12 col-sm-12">
    <div class="">
      <form class="white">
        <br>
        <div class="clearfix"></div> <!-- do not delete -->
        <table id="institutes" class="table2 table table-responsive" data-page-length='20'>
          <thead class="first-child">
          <th class="first-child blue">Sr. No.</th>
          <th class="blue1">Institute Name</th>
          <th class="blue2">City</th>
          <th class="blue3">Address</th>
          <th class="blue3">Contact No.</th>
          <th class="blue2">Email</th>
          <th class="blue1">Website</th>
          </thead>
          <?php if ($_smarty_tpl->tpl_vars['institutes']->value) {?>
            <?php echo smarty_function_counter(array('start'=>0,'print'=>false),$_smarty_tpl);?>

            <?php
$_from = $_smarty_tpl->tpl_vars['institutes']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_inst_0_saved_item = isset($_smarty_tpl->tpl_vars['inst']) ? $_smarty_tpl->tpl_vars['inst'] : false;
$_smarty_tpl->tpl_vars['inst'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['inst']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['inst']->value) {
$_smarty_tpl->tpl_vars['inst']->_loop = true;
$__foreach_inst_0_saved_local_item = $_smarty_tpl->tpl_vars['inst'];
?>
            <tr>
              <td><?php echo smarty_function_counter(array(),$_smarty_tpl);?>
</td>
              <td><?php echo $_smarty_tpl->tpl_vars['inst']->value['inst_name'];?>
</td>
              <td><?php echo $_smarty_tpl->tpl_vars['inst']->value['center_name'];?>
</td>
              <td><?php echo $_smarty_tpl->tpl_vars['inst']->value['address'];?>
</td>
              <td><?php echo $_smarty_tpl->tpl_vars['inst']->value['principal_contact'];?>
</td>
              <td class="emailadd"><a href="mailto:<?php echo $_smarty_tpl->tpl_vars['inst']->value['email'];?>
" target="_blank"><?php echo $_smarty_tpl->tpl_vars['inst']->value['email'];?>
</a></td>
              <td class="webadd"><a href="http://<?php echo $_smarty_tpl->tpl_vars['inst']->value['website'];?>
" target="_blank"><?php echo $_smarty_tpl->tpl_vars['inst']->value['website'];?>
</a></td>
            </tr>
           <?php
$_smarty_tpl->tpl_vars['inst'] = $__foreach_inst_0_saved_local_item;
}
if ($__foreach_inst_0_saved_item) {
$_smarty_tpl->tpl_vars['inst'] = $__foreach_inst_0_saved_item;
}
?>
          <?php }?>
        </table>        
        <div class="clearfix"></div> <!-- do not delete -->
      </form>
    </div>
  </div>
</div>

<link href="assets/datatables/dataTables.bootstrap.css" rel="stylesheet" type="text/css" />
<?php echo '<script'; ?>
 src="assets/datatables/jquery.dataTables.min.js" type="text/javascript"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 src="assets/datatables/dataTables.bootstrap.min.js" type="text/javascript"><?php echo '</script'; ?>
>

<?php echo '<script'; ?>
 type="text/javascript">
  $(function () {
    $('#institutes').DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": true,
      "ordering": false,
      "info": true,
      "autoWidth": false,
      language: {
        searchPlaceholder: "Search"
      }
    });
  });
<?php echo '</script'; ?>
>
<?php }
}
