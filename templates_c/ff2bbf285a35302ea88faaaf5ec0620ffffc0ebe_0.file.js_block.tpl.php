<?php
/* Smarty version 3.1.29, created on 2016-03-23 22:12:45
  from "/var/www/html/smarty-tlb2/templates/include/js_block.tpl" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.29',
  'unifunc' => 'content_56f2c7858a6085_51826938',
  'file_dependency' => 
  array (
    'ff2bbf285a35302ea88faaaf5ec0620ffffc0ebe' => 
    array (
      0 => '/var/www/html/smarty-tlb2/templates/include/js_block.tpl',
      1 => 1458751363,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_56f2c7858a6085_51826938 ($_smarty_tpl) {
echo '<script'; ?>
 src="<?php echo SERVER_PATH;?>
templates/js/jquery.prettyPhoto.js" type="text/javascript" charset="utf-8"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 src="<?php echo SERVER_PATH;?>
bootstrap/js/bootstrap.min.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 src="<?php echo SERVER_PATH;?>
templates/js/jquery.slicknav.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 src="<?php echo SERVER_PATH;?>
templates/js/jquery.confirm.min.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 src="<?php echo SERVER_PATH;?>
templates/js/jquery.nivo.slider.pack.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 src="<?php echo SERVER_PATH;?>
templates/js/select2/select2.full.min.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 src="<?php echo SERVER_PATH;?>
templates/js/scripts.js"><?php echo '</script'; ?>
>

<?php echo '<script'; ?>
>
  $(document).ready(function () {
    $('[data-toggle="popover"]').popover({
      placement: 'bottom'
    });
    $('body').on('click', function (e) {
      $('[data-toggle="popover"]').each(function () {
        //the 'is' for buttons that trigger popups
        //the 'has' for icons within a button that triggers a popup
        if (!$(this).is(e.target) && $(this).has(e.target).length === 0 && $('.popover').has(e.target).length === 0) {
          $(this).popover('hide');
        }
      });
    });
  });
<?php echo '</script'; ?>
><?php }
}
