<?php
/* Smarty version 3.1.29, created on 2016-02-28 21:03:04
  from "/var/www/html/smarty_tlb2/templates/submit.tpl" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.29',
  'unifunc' => 'content_56d31330c8dc25_61546350',
  'file_dependency' => 
  array (
    '3b6808cac14600f2c19e0c92554737e44a0bb42c' => 
    array (
      0 => '/var/www/html/smarty_tlb2/templates/submit.tpl',
      1 => 1456673579,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_56d31330c8dc25_61546350 ($_smarty_tpl) {
?>
<div class="container white-bg">
  <div class="col-md-12 col-sm-12">

    <div class="page-title">
      <p style="margin-top:5px;">
        <a href="#" class="active">Araz Submitted</a></p>
      <h1>Araz Submitted - Track ID: <strong><?php echo $_smarty_tpl->tpl_vars['araz_id']->value;?>
</strong><span class="alfatemi-text">عرض</span></h1>
    </div>
  </div>
  <div class="clearfix"></div> <!-- do not delete -->
  <div class="col-md-12 col-sm-12">
    <div class="page">
      <form class="forms1 white" action="<?php echo SERVER_PATH;?>
istibsaar/" method="post">
        <div class="block top-mgn10"></div>
        <div class="blue-box1">
          <h3 class="bordered">
            <span class="lsd">
              <?php if ((isset($_smarty_tpl->tpl_vars['already_araz_done']->value) && $_smarty_tpl->tpl_vars['already_araz_done']->value == 1)) {?>
              <p class="large-fonts text-center">The Raza that you had been granted has been registered with talabulilm.com and further communication will be done through <a href="<?php echo SERVER_PATH;?>
">www.talabulilm.com</p>
              <?php } else { ?>
              <p class="lsd large-fonts text-center" dir="rtl">ان شاء الله تعالى تماري عرض حضرة امامية نورانية ما عرض كروا ما اْؤسسس،
انسس جواب فضل تهاسسس ته وقت تمنسس خبر كروا ما اْؤسسس، تماري عرض مطابق مزيد كوئي سؤال هوئي تو <a href="mailto:education@alvazarat.org">education@alvazarat.org</a> ثثر email كروو.</p>
              
                <?php if ((in_array($_smarty_tpl->tpl_vars['marhala_id']->value,$_smarty_tpl->tpl_vars['marhala_array']->value))) {?>
                  <?php if (($_smarty_tpl->tpl_vars['success_message']->value)) {?>
                	<p>&nbsp;</p>
                	<p class="lsd large-fonts text-center" dir="rtl"><?php echo $_smarty_tpl->tpl_vars['success_message']->value;?>
</p>
                  <?php }?>
                <?php }?>
              
              <p class="large-fonts text-center">Inshallah o taala your araz will be presented in Hadrat Imamiyah Nooraniyah. For any further queries related to your araz please contact <a href="mailto:education@alvazarat.org">education@alvazarat.org</a></p>
              
              <p class="large-fonts text-center">You may be contacted regarding your araz.</p>
			  <?php }?>
              
              <p class="large-fonts text-center">Your araz track id is <strong style="color:#FFB99E;"><?php echo $_smarty_tpl->tpl_vars['araz_id']->value;?>
</strong>.</p>
              
              <?php if (($_smarty_tpl->tpl_vars['total_ques']->value > $_smarty_tpl->tpl_vars['total_ques_answers']->value)) {?>
              	<p class="large-fonts text-center"><a href="<?php echo SERVER_PATH;?>
complete_ques_details.php?araz_id=<?php echo $_smarty_tpl->tpl_vars['araz_id']->value;?>
">Would you like to complete your azaim questionnaire?</a></p>
              <?php }?>
            </span>
          </h3>
          <div class="clearfix"></div> <!-- do not delete -->
        </div>

        <a href="<?php echo $_smarty_tpl->tpl_vars['redirect_link']->value;?>
"  class="skip lsd"> تفكر ساعة خير من عبادة سنة</a>
        <div class="clearfix"></div> <!-- do not delete -->
      </form>
      <div class="clearfix"></div> <!-- do not delete -->
    </div>
  </div>
</div>

<style>
  .skip{
    display: block;
    -webkit-transition: 0.4s;
    -moz-transition: 0.4s;
    -o-transition: 0.4s;
    transition: 0.4s;
    border: none;
    background-color: #033054;
    height: auto;
    margin: 20px auto 0;
    padding: 10px;
    outline: none;
    color: #fff !important;
    min-width: 150px;
    text-transform: uppercase;
    text-align: center;
    float: right;
    line-height: 20px;
    text-decoration: none !important;
  }
  
</style><?php }
}
