<?php
/* Smarty version 3.1.29, created on 2016-02-28 19:57:34
  from "/var/www/html/smarty_tlb2/templates/review.tpl" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.29',
  'unifunc' => 'content_56d303d6606ed8_13924263',
  'file_dependency' => 
  array (
    '19d61eff1c00a1f10c5ee74201b44cb33697038c' => 
    array (
      0 => '/var/www/html/smarty_tlb2/templates/review.tpl',
      1 => 1456669652,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_56d303d6606ed8_13924263 ($_smarty_tpl) {
?>
<div class="container white-bg">
  <div class="col-md-12 col-sm-12">

    <div class="page-title">
      <?php if (($_smarty_tpl->tpl_vars['already_araz_done']->value == '1')) {?>
      
        <p style="margin-top:5px;">
          <a href="<?php echo SERVER_PATH;?>
marahil/">Raza Araz for</a> <?php if (($_smarty_tpl->tpl_vars['marhala_id']->value != '1')) {?> / <a href="<?php echo SERVER_PATH;?>
marhala-<?php echo $_smarty_tpl->tpl_vars['marhala_id']->value;?>
/<?php echo get_marhala_slug($_smarty_tpl->tpl_vars['marhala_id']->value);?>
/madrasah"><?php echo $_smarty_tpl->tpl_vars['panel_heading']->value;?>
 ( Madrasah )</a> <?php }?> / <a href="<?php echo SERVER_PATH;?>
marhala-<?php echo $_smarty_tpl->tpl_vars['marhala_id']->value;?>
/<?php echo get_marhala_slug($_smarty_tpl->tpl_vars['marhala_id']->value);?>
/school"><?php echo $_smarty_tpl->tpl_vars['panel_heading']->value;?>
 ( School )</a> / <a href="<?php echo SERVER_PATH;?>
marhala-<?php echo $_smarty_tpl->tpl_vars['marhala_id']->value;?>
/<?php echo get_marhala_slug($_smarty_tpl->tpl_vars['marhala_id']->value);?>
/ques">Istibsaar</a> / <a href="#" class="active"><?php echo $_smarty_tpl->tpl_vars['heading_msg']->value;?>
</a></p>
        <h1><?php echo $_smarty_tpl->tpl_vars['heading_msg']->value;?>
<span class="alfatemi-text">&#1575;&#1604;&#1571;&#1606;&#1588;&#1591;&#1577;</span></h1>
        
      <?php } else { ?>
        
        <p style="margin-top:5px;">
          <a href="#" class="active"><?php echo $_smarty_tpl->tpl_vars['heading_msg']->value;?>
</a></p>
        <h1><?php echo $_smarty_tpl->tpl_vars['heading_msg']->value;?>
</h1>
        
      <?php }?>
      
    </div>
  </div>
  <div class="clearfix"></div> <!-- do not delete -->
  <div class="col-md-12 col-sm-12">
    <div class="page">
      <form class="forms1 white" action="<?php echo SERVER_PATH;?>
marhala-<?php echo $_smarty_tpl->tpl_vars['marhala_id']->value;?>
/<?php echo get_marhala_slug($_smarty_tpl->tpl_vars['marhala_id']->value);?>
/message/" method="post">
        <div class="block top-mgn10"></div>
        <div class="blue-box1">
          <div class="block bot-mgn20 hmgn20">
            
            <?php if (($_smarty_tpl->tpl_vars['already_araz_done']->value == '1')) {?>
              <p>Your Araz has been Registered with us.</p>
            <?php } else { ?>
              <p>Your Araz is in under process. We will contact you within 72 hours.</p>
            <?php }?>
            
          </div>
          <div class="clearfix"></div> <!-- do not delete -->
        </div>
        </div>
        <div class="clearfix"></div> <!-- do not delete -->
      </form>
      <div class="clearfix"></div> <!-- do not delete -->
    </div>
  </div>
</div><?php }
}
