<?php
/* Smarty version 3.1.29, created on 2016-02-28 20:59:38
  from "/var/www/html/smarty_tlb2/templates/preview_master_english.tpl" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.29',
  'unifunc' => 'content_56d3126288f466_61875788',
  'file_dependency' => 
  array (
    '92a49ab928d2a952bb7a394ffe52e641b5b94100' => 
    array (
      0 => '/var/www/html/smarty_tlb2/templates/preview_master_english.tpl',
      1 => 1456673367,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:araz_preview.tpl' => 1,
  ),
),false)) {
function content_56d3126288f466_61875788 ($_smarty_tpl) {
?>
<style>
  .skip{
    display:block;
    max-width:300px;
    -webkit-transition: 0.4s;
    -moz-transition: 0.4s;
    -o-transition: 0.4s;
    transition: 0.4s;
    border: none;
    background-color: #155485;
    height: auto;
    line-height:20px;
    margin:0px auto 20px;
    padding:10px;
    outline: none;
    color:#FFF;
    text-transform:uppercase;
    text-align:center;
    font-weight:bold;
    font-size:18px;
    float:right;
    text-decoration: underline none;
  }
  
  .set{
    background-color: #549BC7 !important;
  }
  
  .skip:hover{
    background-color: #549BC7;
  }
  
  #load {
    color: #000 !important;
    margin: 20px auto 0;
    float: right;
    text-align: center;
  }
</style>

<!-- Contents -->
<!-- ====================================================================================================== -->
<div class="container white-bg">
  <div class="col-md-12 col-sm-12">

    <div class="page-title">
      <p style="margin-top:5px;">
        <a href="<?php echo SERVER_PATH;?>
marahil/">My Araz for education</a> 
        <?php if ((in_array($_smarty_tpl->tpl_vars['marhala_id']->value,$_smarty_tpl->tpl_vars['marhala_school_array']->value))) {?>
          / <a href="<?php echo SERVER_PATH;?>
marhala-<?php echo $_smarty_tpl->tpl_vars['marhala_id']->value;?>
/<?php echo get_marhala_slug($_smarty_tpl->tpl_vars['marhala_id']->value);?>
/school"><?php echo $_smarty_tpl->tpl_vars['panel_heading']->value;?>
 ( School )</a> 
        <?php } else { ?>
          / <a href="<?php echo SERVER_PATH;?>
marhala-<?php echo $_smarty_tpl->tpl_vars['marhala_id']->value;?>
/<?php echo get_marhala_slug($_smarty_tpl->tpl_vars['marhala_id']->value);?>
/select-institute/"><?php echo $_smarty_tpl->tpl_vars['panel_heading']->value;?>
 (Select Institute)</a>
        <?php }?>
        / <a href="<?php echo SERVER_PATH;?>
marhala-<?php echo $_smarty_tpl->tpl_vars['marhala_id']->value;?>
/<?php echo get_marhala_slug($_smarty_tpl->tpl_vars['marhala_id']->value);?>
/ques/">Azaaim</a> / <a href="#" class="active">Araz Preview</a>
      </p>
      
      <h1>Araz Preview<span class="alfatemi-text">عرض</span></h1>
    </div>
  </div>
  <div class="clearfix"></div> <!-- do not delete -->
  <div class="col-md-12 col-sm-12">
    <div class="page">
      
      <div class="col-md-3 pull-right hidden-xs"></div>
      <div class="col-md-3 col-xs-12 pull-right text-center"><br>
        <button type="button" name="for_english" id="for_arabic" class="skip lsd btn btn-block" value="1" dir="rtl" onClick="MM_goToURL('parent','<?php echo SERVER_PATH;?>
marhala-<?php echo $_smarty_tpl->tpl_vars['marhala_id']->value;?>
/<?php echo get_marhala_slug($_smarty_tpl->tpl_vars['marhala_id']->value);?>
/preview/');return document.MM_returnValue">لسان الدعوة واسطسس يظظاطط click كروو</button>
      </div>
      
      <div class="col-md-3 col-xs-12 pull-right text-center"><br>
        <button type="button" name="for_english" id="for_english" class="skip set lsd btn btn-block">For English Click Here</button>
      </div><br><br>
      <div class="clearfix"></div>
      
      <form class="forms1 white" action="<?php echo SERVER_PATH;?>
marhala-<?php echo $_smarty_tpl->tpl_vars['marhala_id']->value;?>
/<?php echo get_marhala_slug($_smarty_tpl->tpl_vars['marhala_id']->value);?>
/preview-english/" method="post">
        
        <div class="clearfix"></div> <!-- do not delete -->
        
        <div class="block top-mgn10"></div>
        <div class="blue-box1">
          <h3 class="bordered">
            <span>
                <p class="large-fonts">Ghibb al-Sajadaat al-´Uboodiyah</p>
                <p class="large-fonts">Fi al-Hadrat al-´Aaliyah al-Imamiyah Ashraq Allah</p>
                <p class="large-fonts">Anwaarahaa</p>
                
                <br>
                <p class="large-fonts">Applicant/ Student : <?php echo $_smarty_tpl->tpl_vars['user']->value->get_full_name();?>
</p>
                <p class="large-fonts">Jamaat/City : <?php echo $_smarty_tpl->tpl_vars['user']->value->get_city();?>
</p>
                
                <?php if (($_smarty_tpl->tpl_vars['marhala_id']->value == '1')) {?>
                <br>
                <p class="large-fonts">I humbly request the raza and dua mubarak of Aqa Maula TUS for my education as follows :</p>
                
                <?php } else { ?>
                  <?php if ((count($_smarty_tpl->tpl_vars['institute_data']->value) > 1)) {?>
                	<br>
                	<p class="large-fonts">I humbly request the raza Mubarak of Aqa Maula TUS for one of the following options :</p>
                  <?php } else { ?>
                	<br>
                	<p class="large-fonts">I humbly request the raza and dua mubarak of Aqa Maula TUS for my education as follows :</p>
                  <?php }?>
                <?php }?>
            </span>
          </h3>
          <div class="clearfix"></div> <!-- do not delete -->
        </div>
        
        <?php $_smarty_tpl->smarty->ext->_subtemplate->render($_smarty_tpl, "file:araz_preview.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

        
        <input type="submit" class="submit1 btn col-md-3 col-xs-12 pull-right" name="submit" value="Submit Araz" onclick="return ray.ajax()"/>
        <div id="load" style="display: none;"><i class="fa fa-spinner fa-spin fa-3x"></i></div>
        <a class="pull-left col-md-3 col-xs-12 start-again" onclick="start_again();">Start Over Again</a>
        <span class="page-steps">Step : 5 out of 5</span>
        <div class="clearfix"></div> <!-- do not delete -->
      </form>
      <div class="clearfix"></div> <!-- do not delete -->
    </div>
  </div>
</div>

<?php echo '<script'; ?>
 language="JavaScript">

  function MM_goToURL() { //v3.0
    var i, args=MM_goToURL.arguments; document.MM_returnValue = false;
    for (i=0; i<(args.length-1); i+=2) eval(args[i]+".location='"+args[i+1]+"'");
  }

  function start_again() {
      var ask = window.confirm("Are you sure you want to start again?");
      if (ask) {
          document.location.href = "<?php echo SERVER_PATH;?>
start_over_again.php";
      }
  }
  
  var ray = {
    ajax: function(st)
    {
      this.show('load');
      $('.submit1').hide();
    },
    show: function(el)
    {
      this.getID(el).style.display = '';
    },
    getID: function(el)
    {
      return document.getElementById(el);
    }
  }
<?php echo '</script'; ?>
>

<style>
  .orange-btn {
    margin-bottom: 20px;
  }
</style><?php }
}
