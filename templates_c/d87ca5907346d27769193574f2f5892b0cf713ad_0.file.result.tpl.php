<?php
/* Smarty version 3.1.29, created on 2016-03-25 12:42:00
  from "/var/www/html/smarty_tlb2/templates/result.tpl" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.29',
  'unifunc' => 'content_56f4e4c0baed80_81462198',
  'file_dependency' => 
  array (
    'd87ca5907346d27769193574f2f5892b0cf713ad' => 
    array (
      0 => '/var/www/html/smarty_tlb2/templates/result.tpl',
      1 => 1456833771,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:include/js_block.tpl' => 1,
    'file:include/footer.tpl' => 1,
  ),
),false)) {
function content_56f4e4c0baed80_81462198 ($_smarty_tpl) {
?>
<div class="container white-bg">
  <div class="col-md-12 col-sm-12">

    <div class="page-title">
      <p style="margin-top:5px;">
        <a href="<?php echo SERVER_PATH;?>
">Home</a> / 
        <a href="<?php echo SERVER_PATH;?>
miqaat_content.php?miqaat_id=<?php echo $_smarty_tpl->tpl_vars['course_data']->value['miqaat_id'];?>
"><?php echo $_smarty_tpl->tpl_vars['miqaat_istibsaar']->value['miqaat_title'];?>
</a> /
        <a href="#" class="active"><?php echo $_smarty_tpl->tpl_vars['course_data']->value['course_title'];?>
</a></p>
      <h1><?php echo $_smarty_tpl->tpl_vars['miqaat_istibsaar']->value['miqaat_title'];?>
<!--span class="alfatemi-text">معلومات ذاتية</span--></h1>
    </div>
  </div>
  <div class="clearfix"></div> <!-- do not delete -->
  <form class="forms1 white" name="raza_form" method="post">
    <div class="col-md-3 col-sm-12 hidden-xs">
      <div class="profile-box-static">
        <h3 class="uppercase text-center">Quizzes</h3>
      </div>
      <div class="profile-box-static-bottom">
        <?php
$_from = $_smarty_tpl->tpl_vars['courses']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_c_0_saved_item = isset($_smarty_tpl->tpl_vars['c']) ? $_smarty_tpl->tpl_vars['c'] : false;
$_smarty_tpl->tpl_vars['c'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['c']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['c']->value) {
$_smarty_tpl->tpl_vars['c']->_loop = true;
$__foreach_c_0_saved_local_item = $_smarty_tpl->tpl_vars['c'];
?>
          <a href="<?php echo SERVER_PATH;?>
take_courses.php?course_id=<?php echo $_smarty_tpl->tpl_vars['c']->value['course_id'];?>
">» <?php echo $_smarty_tpl->tpl_vars['c']->value['course_title'];?>
<hr></a>
        <?php
$_smarty_tpl->tpl_vars['c'] = $__foreach_c_0_saved_local_item;
}
if ($__foreach_c_0_saved_item) {
$_smarty_tpl->tpl_vars['c'] = $__foreach_c_0_saved_item;
}
?>
      </div>
      
      <div class="clearfix"><br></div>
      <div class="profile-box-static">
        <h3 class="uppercase text-center">Scores</h3>
      </div>
      <div class="profile-box-static-bottom">
        <?php $_smarty_tpl->tpl_vars["i"] = new Smarty_Variable(0, null);
$_smarty_tpl->ext->_updateScope->updateScope($_smarty_tpl, "i", 0);?>
          <?php
$_from = $_smarty_tpl->tpl_vars['log_courses']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_c_1_saved_item = isset($_smarty_tpl->tpl_vars['c']) ? $_smarty_tpl->tpl_vars['c'] : false;
$_smarty_tpl->tpl_vars['c'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['c']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['c']->value) {
$_smarty_tpl->tpl_vars['c']->_loop = true;
$__foreach_c_1_saved_local_item = $_smarty_tpl->tpl_vars['c'];
?>
            <?php $_smarty_tpl->tpl_vars["i"] = new Smarty_Variable($_smarty_tpl->tpl_vars['i']->value+1, null);
$_smarty_tpl->ext->_updateScope->updateScope($_smarty_tpl, "i", 0);?>
            <span class="pull-left">» Attempt <?php echo $_smarty_tpl->tpl_vars['i']->value;?>
</span><span class="pull-right"><?php echo $_smarty_tpl->tpl_vars['c']->value['score'];?>
</span>
            <div class="clearfix"><br></div>
            <hr>
          <?php
$_smarty_tpl->tpl_vars['c'] = $__foreach_c_1_saved_local_item;
}
if ($__foreach_c_1_saved_item) {
$_smarty_tpl->tpl_vars['c'] = $__foreach_c_1_saved_item;
}
?>
        <span class="pull-right"><a href="<?php echo SERVER_PATH;?>
take_courses.php?course_id=<?php echo $_smarty_tpl->tpl_vars['course_id']->value;?>
" class="btn btn-primary">Attempt Again</a></span>
        <div class="clearfix"></div>
      </div>
    </div>

    <div class="col-md-9 col-sm-12">
      <div class="static-show-button lsd large-fonts col-xs-12 col-sm-5" dir="rtl">Correct <?php echo $_smarty_tpl->tpl_vars['total_score']->value;?>
 of <?php echo count($_smarty_tpl->tpl_vars['course_questions']->value);?>
</div>
      <div class="clearfix"></div>
      <div>
        <div class="col-xs-12 col-sm-5" id="group_list" dir="rtl"></div>
        <div class="quiz-point col-xs-12 col-sm-4 color1" dir="rtl"></div>
        <div class="clearfix"></div>

        <?php $_smarty_tpl->tpl_vars["sr"] = new Smarty_Variable(0, null);
$_smarty_tpl->ext->_updateScope->updateScope($_smarty_tpl, "sr", 0);?>
        <?php
$_from = $_smarty_tpl->tpl_vars['course_questions']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_q_2_saved_item = isset($_smarty_tpl->tpl_vars['q']) ? $_smarty_tpl->tpl_vars['q'] : false;
$_smarty_tpl->tpl_vars['q'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['q']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['q']->value) {
$_smarty_tpl->tpl_vars['q']->_loop = true;
$__foreach_q_2_saved_local_item = $_smarty_tpl->tpl_vars['q'];
?>
          <?php $_smarty_tpl->tpl_vars["sr"] = new Smarty_Variable($_smarty_tpl->tpl_vars['sr']->value+1, null);
$_smarty_tpl->ext->_updateScope->updateScope($_smarty_tpl, "sr", 0);?>
          <div class="col-sm-12 col-md-12" style="color:#000;">
            <div class="panel panel-<?php echo $_smarty_tpl->tpl_vars['q']->value['divclass'];?>
">
              <div class="panel-heading">Question <?php echo $_smarty_tpl->tpl_vars['sr']->value;?>
</div>
              <div class="panel-body lsd" dir="rtl">
                <p class="lsd" style="font-size: 18px" dir="rtl"><?php echo $_smarty_tpl->tpl_vars['q']->value['question'];?>
</p>
                <p class="lsd" style="font-size: 18px" dir="rtl"><strong>Your Answer:</strong> <?php echo $_smarty_tpl->tpl_vars['q']->value['user_answer'];?>
</p>
                <?php if (($_smarty_tpl->tpl_vars['q']->value['divclass'] == 'danger')) {?>
                  <p class="lsd" style="font-size: 18px" dir="rtl"><strong>Correct Answers:</strong> <?php echo $_smarty_tpl->tpl_vars['q']->value['correct_answers'];?>
</p>
                <?php }?>
              </div>
            </div>
          </div>
        <?php
$_smarty_tpl->tpl_vars['q'] = $__foreach_q_2_saved_local_item;
}
if ($__foreach_q_2_saved_item) {
$_smarty_tpl->tpl_vars['q'] = $__foreach_q_2_saved_item;
}
?>
      </div>
    </div>
    <div class="clearfix"></div>
  </form>
</div>
<style type="text/css">
  .panel-body p{
    color: #000;
  }
  
  .profile-box-static-bottom span {
    color: #444444 !important;
    font-size: 14px;
    text-align: left;
    line-height: 150%;
    text-decoration: none;
    margin-left: 15px;
    margin-right: 15px;
  }
  
  .profile-box-static-bottom span a {
    color: #FFF !important;
  }
</style>
<?php $_smarty_tpl->smarty->ext->_subtemplate->render($_smarty_tpl, "file:include/js_block.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

<?php $_smarty_tpl->smarty->ext->_subtemplate->render($_smarty_tpl, "file:include/footer.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
}
}
