<?php
/* Smarty version 3.1.29, created on 2016-03-16 11:39:38
  from "/var/www/html/smarty_tlb2/templates/qrn_cover.tpl" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.29',
  'unifunc' => 'content_56e8f8a236ae72_36162778',
  'file_dependency' => 
  array (
    'd5ef9e357730a65a46ee3bc40cc2631cd0c9e62b' => 
    array (
      0 => '/var/www/html/smarty_tlb2/templates/qrn_cover.tpl',
      1 => 1457697002,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:include/qrn_links.tpl' => 1,
    'file:include/js_block.tpl' => 1,
    'file:include/footer.tpl' => 1,
  ),
),false)) {
function content_56e8f8a236ae72_36162778 ($_smarty_tpl) {
?>
<div class="container white-bg">
  <div class="col-md-12 col-sm-12">
    <div class="page-title">
      <p style="margin-top:5px;">
        <a href="#" class="active">Al-Akh al-Qurani</a></p>
      <h1>Al-Akh al-Qurani</h1>
    </div>
  </div>
  <div class="clearfix"></div> <!-- do not delete -->
  <div class="col-md-12 col-sm-12">
    <form action="" method="post" name="enroll_form">
      <div class="body-container white-bg">
        <div class="enroll-wrapper">
          <div class="en-left qrn-intro">
            <p dir="ltr">
              <b>I want to perform Tasmee’ of My Hifz to My sadeeq:</b>
              <br/>
              (Note We will use the word Sadeeq for the person you connect with for tasmee and tehfeez. Sadeeq means friend) al Akh al Qurani)
            </p>
            <p dir="ltr">
              This is a tehfeez program based on the concept that everyone of us should help each other achieve the goal of Hifz al Quran given to us by Moula TUS, which
              would finally lead to the ultimate goal; “Har ghar ma ek Hafiz”.
            </p>
            <p dir="ltr">
              In this program, you will register yourself as a Talib e Ilm doing Hifz. You can then choose someone from a provided list to perform tasmee’ of your Quran
              in front of the person you chose. In simple words, you will be doing Hifz on your own, but for the assurance that you have memorized correctly, you will
              perform tasmee to your brother. He will take note of how much tasmee’ you have done on the <b>talabulilm</b> portal.
            </p>
            <p dir="ltr">
              This exercise will eventually lead to a daily routine of tasmee’ and a habitual way of memorizing the Quran. You will be able to see your daily progress,
              your overall graph, how many pages have you memorized and other beneficial details.
            </p>
            <p dir="ltr">
              When you reach the stage of progressing from one Sanaa’ to the next you will be prompted to attempt an Ikhtebar in the Mahad al Zahra elearning programme
              and will be able to obtain your Sanad on passing the Ikhtebaar. The breakdown of the four Sanaa’ are as follows:
            </p>
            <p dir="ltr">
              La Uqsemo
            </p>
            <p dir="ltr">
              Juz Amma
            </p>
            <p dir="ltr">
              Sanaa' 1: 1-5 Siparas
            </p>
            <p dir="ltr">
              Sanaa' 2:  6-12 Siparas
            </p>
            <p dir="ltr">
              Sanaa' 3: 13-21 Siparas
            </p>
            <p dir="ltr">
              Sanaa' 4:  22- Completion of Quran
            </p>
            <br/>
            <p dir="ltr">
              <b>I am also willing to listen to the Hifz of My Brothers.</b>
            </p>
            <p dir="ltr">
              This is a tehfeez program based on the concept that everyone of us should help each other achieve the goal of Hifz al Quran given to us by Moula TUS, which
              would finally lead to the ultimate goal; “Har ghar ma ek Hafiz”.
            </p>
            <p dir="ltr">
              While doing Hifz you should not forget to help your brothers do Hifz al Quran as well.
            </p>
            <p dir="ltr">
              In the very same manner you are performing tasmee’, you can help your brothers perform tasmee’ as well.
            </p>
            <p dir="ltr">
              To help a brother do Hifz al Quran, follow the given instructions:
            </p>
            <ul>
              <li dir="ltr">
                <p dir="ltr">
                  Log on to Talabulilm
                </p>
              </li>
              <li dir="ltr">
                <p dir="ltr">
                  Go to Hifz al Quran section in the Istifdaah menu
                </p>
              </li>
              <li dir="ltr">
                <p dir="ltr">
                  Select the check box
                </p>
              </li>
              <li dir="ltr">
                <p dir="ltr">
                  Enroll yourself by pressing the enroll button
                </p>
              </li>
              <li dir="ltr">
                <p dir="ltr">
                  You can then type in the ITS ID of the persons’ Quran you want to listen to
                </p>
              </li>
              <li dir="ltr">
                <p dir="ltr">
                  Select the Surat and Ayat till where your brother has reached
                </p>
              </li>
              <li dir="ltr">
                <p dir="ltr">
                  Press the Submit button.
                </p>
              </li>
            </ul>
            <p dir="ltr">
              As soon as you press the button you will have his details logged beneath the form. You will be able to see the details of all the persons you have done
              tasmee’ today.
            </p>
            <p dir="ltr">
              Detailed log will be available in the left hand panel in graphical and statistical form.
            </p>
            
            <?php if (($_smarty_tpl->tpl_vars['muhafiz_data_header']->value == FALSE && $_smarty_tpl->tpl_vars['mumin_data_header']->value == FALSE)) {?>
              <div class="__action">
                <a href="qrn_enroll.php" class="btn _wahid" style="text-align:center;">Proceed to Enroll</a>
              </div>
            <?php }?>
            
          </div>
          <div class="en-right">
            <img src="img/qrn-intro-banner.jpg" alt="Banner" width="100%" />
          </div>
            <?php if (($_smarty_tpl->tpl_vars['muhafiz_data_header']->value || $_smarty_tpl->tpl_vars['mumin_data_header']->value)) {?>
              <?php $_smarty_tpl->smarty->ext->_subtemplate->render($_smarty_tpl, "file:include/qrn_links.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

            <?php }?>
        </div>
      </div>
    </form>
  </div>
</div>
<?php $_smarty_tpl->smarty->ext->_subtemplate->render($_smarty_tpl, "file:include/js_block.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

<?php $_smarty_tpl->smarty->ext->_subtemplate->render($_smarty_tpl, "file:include/footer.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
}
}
