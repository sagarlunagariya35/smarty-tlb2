<?php
/* Smarty version 3.1.29, created on 2016-04-01 16:34:25
  from "/var/www/html/smarty_tlb2/templates/qrn_tasmee.tpl" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.29',
  'unifunc' => 'content_56fe55b96c1fc8_51306633',
  'file_dependency' => 
  array (
    'cfd8cc23a429ee9d9336e980d1a2551f1f6319ba' => 
    array (
      0 => '/var/www/html/smarty_tlb2/templates/qrn_tasmee.tpl',
      1 => 1459318234,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:include/message.tpl' => 1,
    'file:include/qrn_message.tpl' => 1,
    'file:include/qrn_surat_list.tpl' => 1,
    'file:include/qrn_links.tpl' => 1,
    'file:include/js_block.tpl' => 1,
    'file:include/footer.tpl' => 1,
  ),
),false)) {
function content_56fe55b96c1fc8_51306633 ($_smarty_tpl) {
?>
<div class="container white-bg">
  <div class="col-md-12 col-sm-12">
    <div class="page-title">
      <p style="margin-top:5px;">
        <a href="#" class="active">Tehfeez</a></p>
      <h1>Tehfeez</h1>
    </div>
  </div>
  <?php if ((isset($_smarty_tpl->tpl_vars['success_message']->value) || isset($_smarty_tpl->tpl_vars['error_message']->value))) {?>
    <div class="row">
      <?php $_smarty_tpl->smarty->ext->_subtemplate->render($_smarty_tpl, "file:include/message.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

    </div>
  <?php }?>
  <div class="clearfix"></div> <!-- do not delete -->
  <div class="col-md-12 col-sm-12">
    <form action="" method="post" name="tasmee_form">
      <!-- actual body content starts here-->
      <?php $_smarty_tpl->smarty->ext->_subtemplate->render($_smarty_tpl, "file:include/qrn_message.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>
     
      <div class="body-container white-bg">
        <div class="hifz-tasmee-details-wrapper">
          <div class="htd-section">
            <?php if ($_smarty_tpl->tpl_vars['count_pending_request']->value) {?>
              <h2>Pending Requests <span class="badge"><?php echo $_smarty_tpl->tpl_vars['count_pending_request']->value;?>
</span></h2>
              <?php if ($_smarty_tpl->tpl_vars['pending_request']->value) {?>
                <div class="row pending-request-wrapper">
                  <?php
$_from = $_smarty_tpl->tpl_vars['pending_request']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_data_0_saved_item = isset($_smarty_tpl->tpl_vars['data']) ? $_smarty_tpl->tpl_vars['data'] : false;
$_smarty_tpl->tpl_vars['data'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['data']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['data']->value) {
$_smarty_tpl->tpl_vars['data']->_loop = true;
$__foreach_data_0_saved_local_item = $_smarty_tpl->tpl_vars['data'];
?>
                    <?php $_smarty_tpl->tpl_vars['mumin_data'] = new Smarty_Variable($_smarty_tpl->tpl_vars['user_logedin']->value->loaduser($_smarty_tpl->tpl_vars['data']->value['mumin_its']), null);
$_smarty_tpl->ext->_updateScope->updateScope($_smarty_tpl, 'mumin_data', 0);?>
                    <?php echo '?>';?>
                    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                      <div class="pending-request">
                        <div class="__text">
                          <span class="__prhead"><?php echo $_smarty_tpl->tpl_vars['mumin_data']->value->get_full_name();?>
</span>
                          <?php echo $_smarty_tpl->tpl_vars['mumin_data']->value->get_jamaat();?>
,<?php echo $_smarty_tpl->tpl_vars['mumin_data']->value->get_jamiat();?>
<br> <?php echo $_smarty_tpl->tpl_vars['mumin_data']->value->get_quran_sanad();?>

                        </div>
                        <div class="__action">
                          <a href="#" id="<?php echo $_smarty_tpl->tpl_vars['data']->value['id'];?>
" class="accept">
                            <img src="<?php echo $_smarty_tpl->tpl_vars['server_path']->value;?>
img/hifz-tasmee-details/tick.png" alt="">
                          </a>
                          <a href="#" id="<?php echo '<?php ';?>echo $data['id']; <?php echo '?>';?>" class="delete">
                            <img src="<?php echo $_smarty_tpl->tpl_vars['server_path']->value;?>
img/hifz-tasmee-details/close.png" alt="">
                          </a>
                        <a href="mailto:<?php echo $_smarty_tpl->tpl_vars['mumin_data']->value->get_email();?>
" id="<?php echo $_smarty_tpl->tpl_vars['data']->value['id'];?>
" class="email">
                            <img src="<?php echo $_smarty_tpl->tpl_vars['server_path']->value;?>
img/hifz-tasmee-details/msg.png" alt="">
                          </a>
                        </div>
                      </div>
                    </div>
                  <?php
$_smarty_tpl->tpl_vars['data'] = $__foreach_data_0_saved_local_item;
}
if ($__foreach_data_0_saved_item) {
$_smarty_tpl->tpl_vars['data'] = $__foreach_data_0_saved_item;
}
?>
                </div>
                <?php }?>
              <?php }?>
            <div class="clearfix"></div>  
            <div class="htd-section">
              <h2>Enter Today's Tasmee <span class="tasmee-date-now"> (<?php echo date('jS M Y');?>
)</span></h2>
              <div class="__message _note alert fade in">
                <a href="#" class="close text-center" data-dismiss="alert" aria-label="close">&times;</a>
                Note: On this page you can update tasmee details of your linked sadeeq. You also have an option to update any person's tasmee details by entering his ITS id in the text box.</div>
              <div class="htd-section">
                <div class="qrn-tabs">
                  <ul>
                    <li id="select_sadeeq" class="select_sadeeq _active">Select Sadeeq</li>
                    <li id="select_its_id" class="select_its_id">Enter ITS ID</li>
                  </ul>
                </div>
                <div class="htd-table">
                  <div class="row">
                    <div class="col-xs-12 col-md-3 view_sadeeq qrn-tasmee-col-margin">
                      <select name="s_mumin_its" id="s_mumin_its" class="form-control select-input">
                        <option value="">Select Mumin</option>
                        <?php if ($_smarty_tpl->tpl_vars['select_mumin']->value) {?>
                          <?php
$_from = $_smarty_tpl->tpl_vars['select_mumin']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_mumin_data_1_saved_item = isset($_smarty_tpl->tpl_vars['mumin_data']) ? $_smarty_tpl->tpl_vars['mumin_data'] : false;
$_smarty_tpl->tpl_vars['mumin_data'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['mumin_data']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['mumin_data']->value) {
$_smarty_tpl->tpl_vars['mumin_data']->_loop = true;
$__foreach_mumin_data_1_saved_local_item = $_smarty_tpl->tpl_vars['mumin_data'];
?>
                            <option value="<?php echo $_smarty_tpl->tpl_vars['mumin_data']->value['its_id'];?>
"><?php echo $_smarty_tpl->tpl_vars['mumin_data']->value['mumin_full_name'];?>
</option>
                          <?php
$_smarty_tpl->tpl_vars['mumin_data'] = $__foreach_mumin_data_1_saved_local_item;
}
if ($__foreach_mumin_data_1_saved_item) {
$_smarty_tpl->tpl_vars['mumin_data'] = $__foreach_mumin_data_1_saved_item;
}
?>
                        <?php }?>
                      </select>
                    </div>
                    <div class="col-xs-12 col-md-3 view_its_id qrn-tasmee-col-margin">
                      <input type="text" class="form-control" name="t_mumin_its" id="t_mumin_its" placeholder="Enter Mumin ITS">
                    </div>
                    <div class="col-xs-12 col-md-3 qrn-tasmee-col-margin">
                      <a name="search" id="search" class="btn btn-success">Get Details</a>
                    </div>
                  </div>
                  <div class="show_ajx_result">
                    <div class="show_data">
                      <div class="select-muhaffix-wrapper">
                        <div class="select-muhaffix-list">
                          <div>
                            <span class="__enhead" id="res_name"></span>
                          </div>
                        </div>
                      </div>
                      <div class="row">
                        <div class="col-xs-12 col-md-12 qrn-tasmee-col-margin">
                          <span class="__last-entry">Last Entry: <b><span class="res_surah al-kanz"></span> - <span class="res_ayat"></span></b></span>

                        </div>
                      </div>
                      <div class="row">
                        <div class="col-xs-12 col-md-12 select-muhaffix-wrapper">
                          <div class="select-muhaffix-list">
                            <div>
                              <span class="__enhead">Sanad: <b><span id="res_sanad"></span></b></span>
                            </div>
                          </div>
                        </div>
                      </div>
                      <table class="last-entry-table dataTable table table-bordered" style="border-collapse:collapse;">
                        <thead>
                          <tr>
                            <th>Name</th>
                            <th>Sanad</th>
                            <th>Last Entry</th>
                          </tr>
                        </thead>
                        <tbody>
                          <tr>
                            <td id="sadeeq-name"></td>
                            <td id="sadeeq-sanad"></td>
                            <td><b><span class="res_surah al-kanz"></span> - <span class="res_ayat"></span></b></td>
                          </tr>
                        </tbody>
                      </table>
                      <hr>
                      <h2>The selected mumin / mumena did tasmee from <span class="al-kanz res_surah"></span> - <span class="res_ayat"></span> till . . .</h2>

                      <div class="row">
                        <div class="col-xs-12 col-md-5 qrn-tasmee-col-margin al-kanz">
                          <select name="surah" id="surah" class="select-input">
                            <?php $_smarty_tpl->smarty->ext->_subtemplate->render($_smarty_tpl, "file:include/qrn_surat_list.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

                          </select>
                        </div>
                        <div class="col-xs-12 col-md-5 qrn-tasmee-col-margin">
                          <input type="text" name="ayat_to" class="qrn-text-box col-md-3" id="ayat_to" placeholder="Enter Ayat No" />
                        </div>
                        <div class="col-xs-12 col-md-2 qrn-tasmee-col-margin">
                          <button type="submit" name="submit" id="tasmee_log" class="btn btn-primary">Submit</button>
                        </div>
                      </div>
                    </div>
                    <div class="col-xs-12 col-md-6 show_data">
                      <div class="select-muhaffix-wrapper">
                        <div class="select-muhaffix-list">
                          <div>
                            <input type="hidden" name="from_surah" id="from_surah" value="">
                            <input type="hidden" name="from_ayat" id="from_ayat" value="">
                          </div>
                        </div>
                      </div>
                      <div class="col-xs-12 col-md-6">
                      </div>
                      <div class="col-xs-12 col-md-3">
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <?php $_smarty_tpl->smarty->ext->_subtemplate->render($_smarty_tpl, "file:include/qrn_links.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

          </div>
        </div>
      </div>
      <!-- actual body content Ends here-->
    </form>
  </div>
</div>
<form method="post" id="frm-del-grp">
  <input type="hidden" name="delete_id" id="del-tasmee-val" value="">
</form>
<form method="post" id="frm-acpt-grp">
  <input type="hidden" name="accept_id" id="acpt-tasmee-val" value="">
</form>
            
<?php $_smarty_tpl->smarty->ext->_subtemplate->render($_smarty_tpl, "file:include/js_block.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>


<?php echo '<script'; ?>
 type="text/javascript">
  $(".delete").confirm({
    text: "Are you sure you want to reject this Akh e-qurani request?",
    title: "Confirmation required",
    confirm: function (button) {
      $('#del-tasmee-val').val($(button).attr('id'));
      $('#frm-del-grp').submit();
      alert('Are you Sure You want to delete: ' + id);
    },
    cancel: function (button) {
      // nothing to do
    },
    confirmButton: "Yes",
    cancelButton: "No",
    post: true,
    confirmButtonClass: "btn-danger"
  });

  $(".accept").confirm({
    text: "Are you sure you want to Accept this Akh e-qurani request?",
    title: "Confirmation required",
    confirm: function (button) {
      $('#acpt-tasmee-val').val($(button).attr('id'));
      $('#frm-acpt-grp').submit();
      alert('Are you Sure You want to accept: ' + id);
    },
    cancel: function (button) {
      // nothing to do
    },
    confirmButton: "Yes",
    cancelButton: "No",
    post: true,
    confirmButtonClass: "btn-success"
  });

  $('#select_sadeeq').on('click', function (e) {
    e.preventDefault();
    $("li.select_its_id").removeClass("_active");
    $("li.select_sadeeq").addClass("_active");
    $('.view_sadeeq').show(600);
    $('.view_its_id').hide(600);
    $('.show_ajx_result').hide();
  });

  $('#select_its_id').on('click', function (e) {
    e.preventDefault();
    $("li.select_its_id").addClass("_active");
    $("li.select_sadeeq").removeClass("_active");
    $('.view_sadeeq').hide(600);
    $('.view_its_id').show(600);
    $('.show_ajx_result').hide();
  });
  $('.view_its_id').hide();

  $('#search').on("click", function () {
    var s_mumin_its = $('#s_mumin_its').val();
    var t_mumin_its = $('#t_mumin_its').val();

    if (s_mumin_its != '') {
      var mumin_its = s_mumin_its;
    } else {
      var mumin_its = t_mumin_its;
    }
    
    if(mumin_its == '') {
      alert("Please select Sadeeq OR Enter ITS ID.");
      return false;
      
    } else {
      
      myApp.showPleaseWait();
      $.ajax({
        type: "POST",
        url: "ajax.php",
        data: 'query=get_mumin_records&mumin_its=' + mumin_its,
        cache: false,
        success: function (response) {
          myApp.hidePleaseWait();
          $('.show_ajx_result').show(600);

          if (response != '') {
            $('.show_data').show(600);

            var data = $.parseJSON(response);
            $('#res_name').html(data.FULL_NAME);
            $('.res_surah').html(data.SURAH);
            $('#from_surah').val(data.SURAH);
            $('#from_surah2').val(data.SURAH);
            $('.res_ayat').html(data.AYAT);
            $('#from_ayat').val(data.AYAT);
            $('#res_sanad').html(data.SANAD);
            $('#sadeeq-sanad').html(data.SANAD);
            $('#sadeeq-name').html(data.FULL_NAME);

          } else {

            var link = 'This person has not Enrolled in Al-Akh al-Quraani program. Please enter his current details <a href="<?php echo $_smarty_tpl->tpl_vars['server_path']->value;?>
qrn_mumin_enroll.php">Here</a> and Press Enroll.';
            $('.show_ajx_result').html(link);
          }
        }
      });
    }
  });

  $('#tasmee_log').on("click", function () {
    var s_mumin_its = $('#s_mumin_its').val();
    var t_mumin_its = $('#t_mumin_its').val();
    var from_surah = $('#from_surah').val();
    var from_ayat = $('#from_ayat').val();
    var to_surah = $('#surah').val();
    var to_ayat = $('#ayat_to').val();

    if (s_mumin_its != '') {
      var mumin_its = s_mumin_its;
    } else {
      var mumin_its = t_mumin_its;
    }

    if (surah == '') {
      alert("Please select Surah.");
      return false;
    } else if (ayat_to == '') {
      alert("Please Enter Ayat No.");
      return false;
    } else {

      $.ajax({
        type: "POST",
        url: "ajax.php",
        data: 'query=make_entry_of_tasmee_log&mumin_its=' + mumin_its + '&from_surah=' + from_surah + '&from_ayat=' + from_ayat + '&to_surah=' + to_surah + '&to_ayat=' + to_ayat,
        cache: false,
        success: function () {
        }
      });
    }
  });

  $('.show_ajx_result').hide();

  var myApp;
  myApp = myApp || (function () {
    var pleaseWaitDiv = $('<div class="modal fade" id="pleaseWaitDialog" data-backdrop="static" data-keyboard="false"><div class="modal-dialog"><div class="modal-content"><div class="modal-header"><h4 class="modal-title">Processing . . .</h4></div><div class="modal-body"><img src="images/loader.gif"></div></div></div></div>');
    return {
      showPleaseWait: function () {
        pleaseWaitDiv.modal();
      },
      hidePleaseWait: function () {
        pleaseWaitDiv.modal('hide');
      },
    };
  })();

<?php echo '</script'; ?>
>
<?php $_smarty_tpl->smarty->ext->_subtemplate->render($_smarty_tpl, "file:include/footer.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
}
}
