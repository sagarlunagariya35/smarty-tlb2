<?php
/* Smarty version 3.1.29, created on 2016-02-23 13:39:51
  from "/var/www/html/smarty_tlb2/templates/js_block.tpl" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.29',
  'unifunc' => 'content_56cc13cfcfc972_27339137',
  'file_dependency' => 
  array (
    'a60ef8fe859728ca04ba3830a889d87ff8667270' => 
    array (
      0 => '/var/www/html/smarty_tlb2/templates/js_block.tpl',
      1 => 1456214987,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_56cc13cfcfc972_27339137 ($_smarty_tpl) {
echo '<script'; ?>
 src="<?php echo SERVER_PATH;?>
js/jquery.min.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 src="<?php echo SERVER_PATH;?>
js/jquery.prettyPhoto.js" type="text/javascript" charset="utf-8"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 src="<?php echo SERVER_PATH;?>
bootstrap/js/bootstrap.min.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 src="<?php echo SERVER_PATH;?>
js/jquery.slicknav.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 src="<?php echo SERVER_PATH;?>
js/jquery.nivo.slider.pack.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 src="<?php echo SERVER_PATH;?>
js/selectize.min.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 src="<?php echo SERVER_PATH;?>
js/scripts.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
>
  $(document).ready(function () {
    $('[data-toggle="popover"]').popover({
      placement: 'bottom'
    });
    $('body').on('click', function (e) {
      $('[data-toggle="popover"]').each(function () {
        //the 'is' for buttons that trigger popups
        //the 'has' for icons within a button that triggers a popup
        if (!$(this).is(e.target) && $(this).has(e.target).length === 0 && $('.popover').has(e.target).length === 0) {
          $(this).popover('hide');
        }
      });
    });
  });
<?php echo '</script'; ?>
><?php }
}
