<?php
/* Smarty version 3.1.29, created on 2016-02-23 18:16:34
  from "/var/www/html/smarty_tlb2/templates/events.tpl" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.29',
  'unifunc' => 'content_56cc54aae04e71_36179466',
  'file_dependency' => 
  array (
    '51603d6b3be4a98e9253d71236da7d4d154c013d' => 
    array (
      0 => '/var/www/html/smarty_tlb2/templates/events.tpl',
      1 => 1456231592,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_56cc54aae04e71_36179466 ($_smarty_tpl) {
?>
<div class="container white-bg">
  <div class="col-md-12 col-sm-12 bayan">
    <div class="page-title">
      <p style="margin-top:5px;"><a href="#">Home</a> / <a href="#"><?php echo $_smarty_tpl->tpl_vars['year']->value;?>
 <?php echo $_smarty_tpl->tpl_vars['event']->value['event_title'];?>
</a></p>
      <h1>Event Gallery - <?php echo $_smarty_tpl->tpl_vars['year']->value;?>
 <?php echo $_smarty_tpl->tpl_vars['event']->value['event_title'];?>
</h1>
    </div>
  </div>
  <div class="clearfix"></div> <!-- do not delete -->
  <form class="forms" name="raza_form" method="post">
    <div class="col-md-3 col-sm-12 hidden-xs">
      <div class="profile-box-static">
        <h3 class="uppercase text-center">Events - <?php echo $_smarty_tpl->tpl_vars['year']->value;?>
</h3>
      </div>
      <div class="profile-box-static-bottom">
        <?php if ($_smarty_tpl->tpl_vars['all_events']->value) {?>
          <?php
$_from = $_smarty_tpl->tpl_vars['all_events']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_evnt_0_saved_item = isset($_smarty_tpl->tpl_vars['evnt']) ? $_smarty_tpl->tpl_vars['evnt'] : false;
$_smarty_tpl->tpl_vars['evnt'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['evnt']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['evnt']->value) {
$_smarty_tpl->tpl_vars['evnt']->_loop = true;
$__foreach_evnt_0_saved_local_item = $_smarty_tpl->tpl_vars['evnt'];
?>
            <a href="<?php echo SERVER_PATH;?>
events/<?php echo $_smarty_tpl->tpl_vars['year']->value;?>
/<?php echo $_smarty_tpl->tpl_vars['evnt']->value['slug'];?>
/<?php echo $_smarty_tpl->tpl_vars['evnt']->value['id'];?>
/">» <?php echo $_smarty_tpl->tpl_vars['evnt']->value['event_title'];?>
<hr></a>
          <?php
$_smarty_tpl->tpl_vars['evnt'] = $__foreach_evnt_0_saved_local_item;
}
if ($__foreach_evnt_0_saved_item) {
$_smarty_tpl->tpl_vars['evnt'] = $__foreach_evnt_0_saved_item;
}
?>
        <?php }?>
      </div>
    </div>

  <?php if ($_smarty_tpl->tpl_vars['event']->value) {?>

    <div class="col-md-6 col-sm-12">
      <h2><span class="label label-primary"><?php echo $_smarty_tpl->tpl_vars['year']->value;?>
 <?php echo $_smarty_tpl->tpl_vars['event']->value['event_title'];?>
</span></h2><br>
      <span>
        <?php echo $_smarty_tpl->tpl_vars['event']->value['text'];?>

      </span><br>

      <?php if ($_smarty_tpl->tpl_vars['videos']->value) {?>
      <br><h2><span class="label label-success">Videos - <?php echo $_smarty_tpl->tpl_vars['year']->value;?>
 <?php echo $_smarty_tpl->tpl_vars['event']->value['event_title'];?>
</span></h2><br>

        <div style="display:none;" class="html5gallery" data-skin="horizontal" data-width="480" data-height="272" data-resizemode="fill">
          <?php
$_from = $_smarty_tpl->tpl_vars['videos']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_video_1_saved_item = isset($_smarty_tpl->tpl_vars['video']) ? $_smarty_tpl->tpl_vars['video'] : false;
$_smarty_tpl->tpl_vars['video'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['video']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['video']->value) {
$_smarty_tpl->tpl_vars['video']->_loop = true;
$__foreach_video_1_saved_local_item = $_smarty_tpl->tpl_vars['video'];
?>

            <a href="<?php echo SERVER_PATH;?>
upload/events/video/<?php echo $_smarty_tpl->tpl_vars['video']->value['event_video_url'];?>
"><img src="
              <?php if (($_smarty_tpl->tpl_vars['video']->value['event_video_thumb'] != '')) {?>
                <?php echo SERVER_PATH;?>
upload/events/video/images/<?php echo $_smarty_tpl->tpl_vars['video']->value['event_video_thumb'];?>

              <?php } else { ?>
                <?php echo SERVER_PATH;?>
images/flash-logo.png
              <?php }?>
              "></a>      

          <?php
$_smarty_tpl->tpl_vars['video'] = $__foreach_video_1_saved_local_item;
}
if ($__foreach_video_1_saved_item) {
$_smarty_tpl->tpl_vars['video'] = $__foreach_video_1_saved_item;
}
?>
        </div>

        <?php }?>

        <?php if ($_smarty_tpl->tpl_vars['audios']->value) {?>
        <br><h2><span class="label label-danger">Audios - <?php echo $_smarty_tpl->tpl_vars['year']->value;?>
 <?php echo $_smarty_tpl->tpl_vars['event']->value['event_title'];?>
</span></h2><br>

        <div id="jquery_jplayer_1" class="jp-jplayer"></div>
        <div id="jp_container_1" class="jp-audio" role="application" aria-label="media player">
          <div class="jp-type-playlist">
            <div class="jp-gui jp-interface">
              <div class="jp-controls">
                <button class="jp-previous" role="button" tabindex="0">previous</button>
                <button class="jp-play" role="button" tabindex="0">play</button>
                <button class="jp-next" role="button" tabindex="0">next</button>
                <button class="jp-stop" role="button" tabindex="0">stop</button>
              </div>
              <div class="jp-progress">
                <div class="jp-seek-bar">
                  <div class="jp-play-bar"></div>
                </div>
              </div>
              <div class="jp-volume-controls">
                <button class="jp-mute" role="button" tabindex="0">mute</button>
                <button class="jp-volume-max" role="button" tabindex="0">max volume</button>
                <div class="jp-volume-bar">
                  <div class="jp-volume-bar-value"></div>
                </div>
              </div>
              <div class="jp-time-holder">
                <div class="jp-current-time" role="timer" aria-label="time">&nbsp;</div>
                <div class="jp-duration" role="timer" aria-label="duration">&nbsp;</div>
              </div>
              <div class="jp-toggles">
                <button class="jp-repeat" role="button" tabindex="0">repeat</button>
                <button class="jp-shuffle" role="button" tabindex="0">shuffle</button>
              </div>
            </div>
            <div class="jp-playlist">
              <ul>
                <li>&nbsp;</li>
              </ul>
            </div>
            <div class="jp-no-solution">
              <span>Update Required</span>
              To play the media you will need to either update your browser to a recent version or update your <a href="http://get.adobe.com/flashplayer/" target="_blank">Flash plugin</a>.
            </div>
          </div>
        </div>

      <?php }?>

      <?php if ($_smarty_tpl->tpl_vars['images']->value) {?>
        <br><h2><span class="label label-info">Images - <?php echo $_smarty_tpl->tpl_vars['year']->value;?>
 <?php echo $_smarty_tpl->tpl_vars['event']->value['event_title'];?>
</span></h2><br>
        <ul class="gallery">
          <?php
$_from = $_smarty_tpl->tpl_vars['images']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_image_2_saved_item = isset($_smarty_tpl->tpl_vars['image']) ? $_smarty_tpl->tpl_vars['image'] : false;
$_smarty_tpl->tpl_vars['image'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['image']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['image']->value) {
$_smarty_tpl->tpl_vars['image']->_loop = true;
$__foreach_image_2_saved_local_item = $_smarty_tpl->tpl_vars['image'];
?>
            <li><a href="<?php echo SERVER_PATH;?>
upload/events/image/<?php echo $_smarty_tpl->tpl_vars['image']->value['event_image_url'];?>
" rel="prettyPhoto[gallery1]"><img src="<?php echo SERVER_PATH;?>
upload/events/image/<?php echo $_smarty_tpl->tpl_vars['image']->value['event_image_url'];?>
" alt=""></a></li>
          <?php
$_smarty_tpl->tpl_vars['image'] = $__foreach_image_2_saved_local_item;
}
if ($__foreach_image_2_saved_item) {
$_smarty_tpl->tpl_vars['image'] = $__foreach_image_2_saved_item;
}
?>
        </ul>
      <?php }?>

    </div>
  
      <div class="col-md-3 col-sm-12 hidden-xs">
        <div class="profile-box-static">
          <h3 class="uppercase text-center">Registration</h3>
        </div>
        <div class="profile-box-static-bottom">
          <span><strong>Start Date: </strong></span><br><br><span> <?php echo date('d, F Y',strtotime($_smarty_tpl->tpl_vars['event']->value['start_date']));?>
</span>
          <hr>
          <span><strong>End Date: </strong></span><br><br><span> <?php echo date('d, F Y',strtotime($_smarty_tpl->tpl_vars['event']->value['end_date']));?>
</span>
          <hr>
          <span><strong>Capacity: </strong><?php echo $_smarty_tpl->tpl_vars['event']->value['capacity'];?>
</span>
          <hr>
          <span><strong>Venue: </strong><?php echo $_smarty_tpl->tpl_vars['event']->value['venue'];?>
</span>
          <hr>
          <span><strong>Contact Person: </strong></span><br><br><span> <?php echo $_smarty_tpl->tpl_vars['event']->value['contact_person'];?>
</span>
          <hr>
          <span><strong>Email: </strong></span><br><br><span> <?php echo $_smarty_tpl->tpl_vars['event']->value['email'];?>
</span>
          <hr>
          <span><strong>Mobile: </strong></span><br><br><span> <?php echo $_smarty_tpl->tpl_vars['event']->value['mobile'];?>
</span>
          <hr><br>
            <?php if ($_smarty_tpl->tpl_vars['event_register']->value) {?>
            <div class="text-center">
              <span>You have already Registered for the Event.</span><br><br>
                <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#myModal">Cancel Registration</button>
                
                <a target="_blank" href="<?php echo SERVER_PATH;?>
print_event_details.php?event_id=<?php echo $_smarty_tpl->tpl_vars['event_id']->value;?>
" class="btn btn-primary pull-right" style="color:#FFF !important;">Print</a>
                <div class="clearfix"></div>
                <input type="hidden" name="event_id" value="<?php echo $_smarty_tpl->tpl_vars['event']->value['id'];?>
">

                <div id="myModal" class="modal fade" role="dialog">
                  <div class="modal-dialog">

                    <!-- Modal content-->
                    <div class="modal-content">
                      <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Reason For Cancel</h4>
                      </div>
                      <div class="modal-body">
                        <textarea name="reason" class="form-control"></textarea>
                      </div>
                      <div class="modal-footer">
                        <input type="submit" name="cancel_event" class="btn btn-success" value="Submit">
                      </div>
                    </div>

                  </div>
                </div>
            </div>
          
            <?php } else { ?>
              <?php if (($_smarty_tpl->tpl_vars['event']->value['capacity'] > $_smarty_tpl->tpl_vars['total_event_register']->value)) {?>
        
            <div class="text-center">
                <input type="submit" class="btn btn-success" name="register" value="Register For Event">
                <input type="hidden" name="event_id" value="<?php echo $_smarty_tpl->tpl_vars['event']->value['id'];?>
">
            </div>
              <?php }?> 
            <?php }?>
        </div>
      </div>
          
    <?php } else { ?>
      
    <div class="col-md-6 col-sm-12">
      <?php if ($_smarty_tpl->tpl_vars['all_events']->value) {?>
        <?php
$_from = $_smarty_tpl->tpl_vars['all_events']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_evnt_3_saved_item = isset($_smarty_tpl->tpl_vars['evnt']) ? $_smarty_tpl->tpl_vars['evnt'] : false;
$_smarty_tpl->tpl_vars['evnt'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['evnt']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['evnt']->value) {
$_smarty_tpl->tpl_vars['evnt']->_loop = true;
$__foreach_evnt_3_saved_local_item = $_smarty_tpl->tpl_vars['evnt'];
?>
          <?php $_smarty_tpl->tpl_vars['evnt_id'] = new Smarty_Variable($_smarty_tpl->tpl_vars['evnt']->value['id'], null);
$_smarty_tpl->ext->_updateScope->updateScope($_smarty_tpl, 'evnt_id', 0);?>
          <?php $_smarty_tpl->tpl_vars['img_art'] = new Smarty_Variable(get_first_image_from_event($_smarty_tpl->tpl_vars['evnt_id']->value), null);
$_smarty_tpl->ext->_updateScope->updateScope($_smarty_tpl, 'img_art', 0);?>

          <a href="<?php echo SERVER_PATH;?>
events/<?php echo $_smarty_tpl->tpl_vars['year']->value;?>
/<?php echo $_smarty_tpl->tpl_vars['evnt']->value['slug'];?>
/<?php echo $_smarty_tpl->tpl_vars['evnt']->value['id'];?>
/">
            <div class="col-sm-3 col-xs-12">
              <div class="istibsaar-boxes text-center">
                <img src="<?php echo SERVER_PATH;?>
upload/events/image/<?php echo $_smarty_tpl->tpl_vars['img_art']->value['event_image_url'];?>
" class="img-responsive img-mts" style="border: 1px solid #09386c">
                <div>
                  <p class="text-center"><?php echo $_smarty_tpl->tpl_vars['evnt']->value['event_title'];?>
</p>
                  <p>&nbsp;</p>
                  <p class="text-center" style="color:#000;"><?php echo $_smarty_tpl->tpl_vars['evnt']->value['desc'];?>
</p>
                </div>
              </div>
            </div>
          </a>
        <?php
$_smarty_tpl->tpl_vars['evnt'] = $__foreach_evnt_3_saved_local_item;
}
if ($__foreach_evnt_3_saved_item) {
$_smarty_tpl->tpl_vars['evnt'] = $__foreach_evnt_3_saved_item;
}
?>
      <?php }?>
    </div>
    <?php }?>
  </form>
</div>

<?php echo '<script'; ?>
 type="text/javascript" src="<?php echo SERVER_PATH;?>
js/jquery.leanModal.min.js"><?php echo '</script'; ?>
>
<link href="<?php echo SERVER_PATH;?>
css/popup_style.css" rel="stylesheet" type="text/css" />

<?php echo '<script'; ?>
 type="text/javascript">
  $("#modal_trigger").leanModal({
    top: 200, overlay: 0.6, closeButton: ".modal_close"
  });
<?php echo '</script'; ?>
>

<?php echo '<script'; ?>
 type="text/javascript" src="<?php echo SERVER_PATH;?>
html5gallery/html5gallery.js"><?php echo '</script'; ?>
>


<link href="<?php echo SERVER_PATH;?>
dist/skin/blue.monday/css/jplayer.blue.monday.min.css" rel="stylesheet" type="text/css" />
<?php echo '<script'; ?>
 type="text/javascript" src="<?php echo SERVER_PATH;?>
dist/jplayer/jquery.jplayer.min.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 type="text/javascript" src="<?php echo SERVER_PATH;?>
dist/add-on/jplayer.playlist.min.js"><?php echo '</script'; ?>
>



<?php echo '<script'; ?>
 type="text/javascript">
  $(document).ready(function () {

    new jPlayerPlaylist({
    jPlayer: "#jquery_jplayer_1",
            cssSelectorAncestor: "#jp_container_1"
    }, [
      <?php if ($_smarty_tpl->tpl_vars['audios']->value) {?>
        <?php
$_from = $_smarty_tpl->tpl_vars['audios']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_audio_4_saved_item = isset($_smarty_tpl->tpl_vars['audio']) ? $_smarty_tpl->tpl_vars['audio'] : false;
$_smarty_tpl->tpl_vars['audio'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['audio']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['audio']->value) {
$_smarty_tpl->tpl_vars['audio']->_loop = true;
$__foreach_audio_4_saved_local_item = $_smarty_tpl->tpl_vars['audio'];
?>
            {
            title:"<?php echo $_smarty_tpl->tpl_vars['audio']->value['event_audio_url'];?>
",
                    mp3: "<?php echo SERVER_PATH;?>
upload/events/audio/<?php echo $_smarty_tpl->tpl_vars['audio']->value['event_audio_url'];?>
",
            }
          <?php if ((count($_smarty_tpl->tpl_vars['audios']->value)-1)) {?>
            <?php echo ',';?>

          <?php }?>
        <?php
$_smarty_tpl->tpl_vars['audio'] = $__foreach_audio_4_saved_local_item;
}
if ($__foreach_audio_4_saved_item) {
$_smarty_tpl->tpl_vars['audio'] = $__foreach_audio_4_saved_item;
}
?>
      <?php }?>
    ], {
    swfPath: "../../dist/jplayer",
            supplied: "oga, mp3",
            wmode: "window",
            useStateClassSkin: true,
            autoBlur: false,
            smoothPlayBar: true,
            keyEnabled: true
    });
  });
<?php echo '</script'; ?>
>

<?php echo '<script'; ?>
 type="text/javascript" charset="utf-8">
  $(document).ready(function () {
    $("area[rel^='prettyPhoto']").prettyPhoto();

    $(".gallery:first a[rel^='prettyPhoto']").prettyPhoto({
      animation_speed: 'normal', theme: 'light_square', slideshow: 5000, autoplay_slideshow: true
    });
    $(".gallery:gt(0) a[rel^='prettyPhoto']").prettyPhoto({
      animation_speed: 'normal', slideshow: 10000, hideflash: true
    });

    $("#custom_content a[rel^='prettyPhoto']:first").prettyPhoto({
      custom_markup: '<div id="map_canvas" style="width:260px; height:265px"></div>',
      changepicturecallback: function () {
        initialize();
      }
    });

    $("#custom_content a[rel^='prettyPhoto']:last").prettyPhoto({
      custom_markup: '<div id="bsap_1259344" class="bsarocks bsap_d49a0984d0f377271ccbf01a33f2b6d6"></div><div id="bsap_1237859" class="bsarocks bsap_d49a0984d0f377271ccbf01a33f2b6d6" style="height:260px"></div><div id="bsap_1251710" class="bsarocks bsap_d49a0984d0f377271ccbf01a33f2b6d6"></div>',
      changepicturecallback: function () {
        _bsap.exec();
      }
    });
  });
<?php echo '</script'; ?>
>

<style>
  .profile-box-static-bottom span{
    text-align: center !important;
    padding: 10px;
  }
  .profile-box-static h3{
    display: block;
    font-family: "Lato",sans-serif;
    font-size: 18px;
    line-height: 150%;
    padding: 0px 0px 20px;
    text-align: left;
    color: #FFF !important;
    text-transform: uppercase;
    font-weight: 600;
  }
  .actv{
    background-color:#cf4914;
    display:block;
    font-family: 'Lato', sans-serif;
    font-size:14px;
    padding:10px;
    text-align:center;
    color:white;
    text-transform:uppercase;
    font-weight:600;

  }

  .ntactv{
    display:block;
    font-family: 'Lato', sans-serif;
    font-size:14px;
    text-align:center;
    color:white;
    text-transform:uppercase;
    font-weight:600;

  }

  .profile-box1 a{
    color: white;
    padding: 5px;
  }

  .gallery {
    clear: both;
    display: table;
    list-style: none;
    padding: 0;
    margin: 0;
    margin-bottom: 20px;
  }
  .gallery li {
    float: left;
    margin-bottom: 7px;
    margin-right: 7px;
    height: 100px;
    width: 100px;
    overflow: hidden;
    text-align: center;
    position: relative;
    padding: inherit;
  }
  .gallery li:last-child {
    margin-right: 0;
  }
  .gallery li a {
    position: relative;
    left: 100%;
    margin-left: -200%;
    position: relative;
  }
  .gallery li a:after {
    text-shadow: none;
    -webkit-font-smoothing: antialiased;
    font-family: 'fontawesome';
    speak: none;
    font-weight: normal;
    font-variant: normal;
    line-height: 1;
    text-transform: none;
    -moz-transition: 0.6s;
    -o-transition: 0.6s;
    -webkit-transition: 0.6s;
    transition: 0.6s;
    filter: progid:DXImageTransform.Microsoft.Alpha(Opacity=0);
    opacity: 0;
    -moz-border-radius: 50%;
    -webkit-border-radius: 50%;
    border-radius: 50%;
    color: #fff;
    content: "\f06e";
    display: inline-block;
    font-size: 16px;
    position: absolute;
    width: 30px;
    height: 30px;
    top: 0;
    left: 0;
    right: 0;
    bottom: 0;
    margin: auto;
    background-color: #252525;
    padding-top: 7px;
    margin-top: -7px;
  }
  .gallery li a:hover img {
    -moz-transform: scale(1.08, 1.08);
    -ms-transform: scale(1.08, 1.08);
    -webkit-transform: scale(1.08, 1.08);
    transform: scale(1.08, 1.08);
  }
  .gallery li a img {
    -moz-transition: 0.3s;
    -o-transition: 0.3s;
    -webkit-transition: 0.3s;
    transition: 0.3s;
    height: 100%;
    width: auto;
  }
</style><?php }
}
