<?php
/* Smarty version 3.1.29, created on 2016-02-28 21:19:01
  from "/var/www/html/smarty_tlb2/templates/my_araz_preview.tpl" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.29',
  'unifunc' => 'content_56d316edc78e96_89709327',
  'file_dependency' => 
  array (
    'b0b66dbc1fac226bbceebba6869f02320b74c5b2' => 
    array (
      0 => '/var/www/html/smarty_tlb2/templates/my_araz_preview.tpl',
      1 => 1456674183,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:my_araz_institution_preview.tpl' => 1,
  ),
),false)) {
function content_56d316edc78e96_89709327 ($_smarty_tpl) {
?>
<style>
  .skip{
  display:block;
  max-width:300px;
  -webkit-transition: 0.4s;
  -moz-transition: 0.4s;
  -o-transition: 0.4s;
  transition: 0.4s;
  border: none;
  background-color: #155485;
  height: auto;
  line-height:20px;
  margin:0px 5px 20px;
  padding:10px;
  outline: none;
  color:#FFF;
  text-transform:uppercase;
  text-align:center;
  font-weight:bold;
  font-size:12px;
  float:right;
  text-decoration: underline none;
}
</style>

<div class="container white-bg">
  <div class="col-md-12 col-sm-12">

    <div class="page-title">
      <p style="margin-top:5px;">
        <a href="#" class="active">Araz Preview</a>
      </p>
      
      <h1>Araz Preview - Track ID: <?php echo $_smarty_tpl->tpl_vars['araz']->value;?>
<span class="alfatemi-text">عرض</span></h1>
    </div>
  </div>
  <div class="clearfix"></div> <!-- do not delete -->
  <div class="col-md-12 col-sm-12">
    <div class="page">
      <form class="forms1 white" action="" method="post">
        
        <!--a class="skip" href="my_araz_preview_english.php?araz=<?php echo '<?php ';?>echo $araz; <?php echo '?>';?>" class="btn btn-color-grey-light">For English Click Here</a-->
        
        <div class="clearfix"></div> <!-- do not delete -->
        
        <div class="block top-mgn10"></div>
        <div class="blue-box1">
          <h3 class="bordered">
            <span class="lsd">
                <p class="lsd large-fonts text-center" dir="rtl">غب السجدات العبودية</p>
                <p class="lsd large-fonts text-center" dir="rtl">في الحضرة العالية الامامية اشرق الله انوارها</p>
                <p class="lsd large-fonts text-center" dir="rtl">عرض كرنار: <?php echo $_smarty_tpl->tpl_vars['user']->value->get_full_name_ar();?>
</p>
                <p class="lsd large-fonts text-center" dir="rtl">موضع: <?php echo $_smarty_tpl->tpl_vars['user']->value->get_city();?>
</p>
                <p class="lsd large-fonts text-center" dir="rtl">ادبًـا عرض كه</p>
            </span>
          </h3>
          <div class="clearfix"></div> <!-- do not delete -->
        </div>
        
        <?php $_smarty_tpl->smarty->ext->_subtemplate->render($_smarty_tpl, "file:my_araz_institution_preview.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

        
        <div class="blue-box1">
          <h3 class="bordered">
            <span class="lsd">
              
                <?php if (($_smarty_tpl->tpl_vars['marhala_id']->value == '1')) {?>
                  <p class="lsd large-fonts text-center" dir="rtl">تعليم ني ابتداء واسطسس مع الدعاء المبارك رزا مبارك فضل فرماوا ادبًا عرض؛</p>
                <?php } else { ?>
                  <?php if ((count($_smarty_tpl->tpl_vars['institute_data']->value) > 1)) {?>
                  <p class="lsd large-fonts text-center" dir="rtl">ما تعليم حاصل كرواني استرشادًا عرض ححهسس</p>
                  <?php } else { ?>
                   <p class="lsd large-fonts text-center" dir="rtl">مع الدعاء المبارك رزا مبارك فضل فرماوا ادبًـا عرض ححهسس</p>
                  <?php }?>
                <?php }?>
                <p class="lsd large-fonts text-center" dir="rtl">والسجدات</p>
            </span>
          </h3>
          <div class="clearfix"></div> <!-- do not delete -->
        </div>
      </form>
      <div class="clearfix"></div> <!-- do not delete -->
    </div>
  </div>
</div><?php }
}
