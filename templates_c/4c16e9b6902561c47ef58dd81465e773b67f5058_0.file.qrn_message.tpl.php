<?php
/* Smarty version 3.1.29, created on 2016-02-24 17:07:19
  from "/var/www/html/smarty_tlb2/templates/include/qrn_message.tpl" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.29',
  'unifunc' => 'content_56cd95efa16327_76012153',
  'file_dependency' => 
  array (
    '4c16e9b6902561c47ef58dd81465e773b67f5058' => 
    array (
      0 => '/var/www/html/smarty_tlb2/templates/include/qrn_message.tpl',
      1 => 1456313150,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_56cd95efa16327_76012153 ($_smarty_tpl) {
if (($_smarty_tpl->tpl_vars['mumin_data_header']->value && $_smarty_tpl->tpl_vars['muhafiz_data_header']->value)) {?>
  <?php $_smarty_tpl->tpl_vars['message_text'] = new Smarty_Variable('Tehfeez and Tasmee', null);
$_smarty_tpl->ext->_updateScope->updateScope($_smarty_tpl, 'message_text', 0);
} elseif ($_smarty_tpl->tpl_vars['muhafiz_data_header']->value) {?>
  <?php $_smarty_tpl->tpl_vars['message_text'] = new Smarty_Variable('Tehfeez', null);
$_smarty_tpl->ext->_updateScope->updateScope($_smarty_tpl, 'message_text', 0);
} elseif ($_smarty_tpl->tpl_vars['mumin_data_header']->value) {?>
  <?php $_smarty_tpl->tpl_vars['message_text'] = new Smarty_Variable('Tasmee', null);
$_smarty_tpl->ext->_updateScope->updateScope($_smarty_tpl, 'message_text', 0);
} else { ?>
  <?php $_smarty_tpl->tpl_vars['message_text'] = new Smarty_Variable('', null);
$_smarty_tpl->ext->_updateScope->updateScope($_smarty_tpl, 'message_text', 0);
}?>

<?php if ($_smarty_tpl->tpl_vars['message_text']->value != '') {?>
  <div class="text-right qrn-reg-wrapper">
    <div class="qrn-reg-header">
      <span>You are registered for <?php echo $_smarty_tpl->tpl_vars['message_text']->value;?>
.</span>
    </div>
  </div>
<?php }
}
}
