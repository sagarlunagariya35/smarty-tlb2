<?php
/* Smarty version 3.1.29, created on 2016-02-28 21:05:52
  from "/var/www/html/smarty_tlb2/templates/complete_ques_details.tpl" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.29',
  'unifunc' => 'content_56d313d8534df5_05560059',
  'file_dependency' => 
  array (
    'cb50bfe2f7baa05b947b92345350725df88d8db1' => 
    array (
      0 => '/var/www/html/smarty_tlb2/templates/complete_ques_details.tpl',
      1 => 1456673748,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:include/message.tpl' => 1,
  ),
),false)) {
function content_56d313d8534df5_05560059 ($_smarty_tpl) {
?>
<style>
  .skip{
    display:block;
    max-width:300px;
    -webkit-transition: 0.4s;
    -moz-transition: 0.4s;
    -o-transition: 0.4s;
    transition: 0.4s;
    border: none;
    background-color: #155485;
    height: auto;
    line-height:20px;
    margin:0px auto 20px;
    padding:10px;
    outline: none;
    color:#FFF;
    text-transform:uppercase;
    text-align:center;
    font-weight:bold;
    font-size:18px;
    float:right;
    text-decoration: underline none;
  }
  
  .set{
    background-color: #549BC7 !important;
  }
  
  .skip:hover{
    background-color: #549BC7;
  }
</style>

<div class="container white-bg">
  <div class="col-md-12 col-sm-12">
    <div class="page-title">
      <p style="margin-top:5px;"><a href="#" class="active"><?php echo $_smarty_tpl->tpl_vars['page_heading']->value;?>
</a></p>
      <h1><?php echo $_smarty_tpl->tpl_vars['page_heading']->value;?>
<span class="alfatemi-text"><?php echo $_smarty_tpl->tpl_vars['page_heading_ar']->value;?>
</span></h1>
    </div>
  </div>
  <?php if ((isset($_smarty_tpl->tpl_vars['_SESSION']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_SUCCESS_MESSAGE']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_SUCCESS_MESSAGE']->value['index'] : null)]) || isset($_smarty_tpl->tpl_vars['_SESSION']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_ERROR_MESSAGE']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_ERROR_MESSAGE']->value['index'] : null)]))) {?>
    <div class="row">
      <?php $_smarty_tpl->smarty->ext->_subtemplate->render($_smarty_tpl, "file:include/message.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

    </div>
  <?php }?>
  <div class="clearfix"></div> <!-- do not delete -->

  <div class="col-md-12 col-sm-12">
    <div class="page">
      <span class="block vmgn20"></span>
      <div class="clearfix"></div> <!-- do not delete -->

      <h4><span class="alfatemi-text">تماري تعليم ني عرض قبل اْفارم ثثورو كرو</span></h4><br>
      
      <div class="clearfix"></div> <!-- do not delete -->
      <div class="col-md-3 pull-right hidden-xs"></div>
      
      <div class="col-md-3 col-xs-12 pull-right text-center"><br>
        <button type="button" name="for_arabic" id="for_arabic" class="btn btn-block skip lsd set" dir="rtl" value="1" <?php if (($_smarty_tpl->tpl_vars['page']->value == 'ques_ar')) {?> checked <?php }?>>سان الدعوة واسطسس يظظاطط click كروو</button>
      </div>
      
      <div class="col-md-3 col-xs-12 pull-right text-center"><br>
        <button type="button" name="for_arabic" id="for_english" class="btn btn-block skip lsd" onClick="MM_goToURL('parent','<?php echo SERVER_PATH;?>
complete_ques_details_english.php?araz_id=<?php echo $_smarty_tpl->tpl_vars['araz_id']->value;?>
');return document.MM_returnValue">For English Click Here</button>
      </div><br><br>
      <div class="clearfix"></div>
      
      <form name="ques_form" class="forms1 white" action="" method="post" onsubmit="return(validate());">

  <?php $_smarty_tpl->tpl_vars["i"] = new Smarty_Variable(0, null);
$_smarty_tpl->ext->_updateScope->updateScope($_smarty_tpl, "i", 0);?>
  <?php if ($_smarty_tpl->tpl_vars['ques_preffered']->value) {?>
    <?php
$_from = $_smarty_tpl->tpl_vars['ques_preffered']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_q_0_saved_item = isset($_smarty_tpl->tpl_vars['q']) ? $_smarty_tpl->tpl_vars['q'] : false;
$_smarty_tpl->tpl_vars['q'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['q']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['q']->value) {
$_smarty_tpl->tpl_vars['q']->_loop = true;
$__foreach_q_0_saved_local_item = $_smarty_tpl->tpl_vars['q'];
?>

      <?php if ((isset($_smarty_tpl->tpl_vars['araz_data']->value[$_smarty_tpl->tpl_vars['i']->value]['answer']))) {?>
        <?php if (($_smarty_tpl->tpl_vars['araz_data']->value[$_smarty_tpl->tpl_vars['i']->value]['answer'] == '1')) {?>
          <?php $_smarty_tpl->tpl_vars['check1'] = new Smarty_Variable('checked', null);
$_smarty_tpl->ext->_updateScope->updateScope($_smarty_tpl, 'check1', 0);?>
        <?php } elseif (($_smarty_tpl->tpl_vars['araz_data']->value[$_smarty_tpl->tpl_vars['i']->value]['answer'] == '2')) {?>
          $check2 = 'checked';
        <?php } elseif (($_smarty_tpl->tpl_vars['araz_data']->value[$_smarty_tpl->tpl_vars['i']->value]['answer'] == '3')) {?>
          $check3 = 'checked';
        <?php } elseif (($_smarty_tpl->tpl_vars['araz_data']->value[$_smarty_tpl->tpl_vars['i']->value]['answer'] == '4')) {?>
          $check4 = 'checked';
        <?php }?>
      <?php }?>
      <?php $_smarty_tpl->tpl_vars["i"] = new Smarty_Variable($_smarty_tpl->tpl_vars['i']->value+1, null);
$_smarty_tpl->ext->_updateScope->updateScope($_smarty_tpl, "i", 0);?>
        <div class="blue-box1 rtl"> <!-- Add class rtl -->
          <h3><span class="inline-block lh3 arb_ques"><?php echo $_smarty_tpl->tpl_vars['i']->value;?>
. <?php echo $_smarty_tpl->tpl_vars['q']->value['question_preview'];?>
</span></h3>
        </div>
        <div class="clearfix"></div> <!-- do not delete -->
        <div class="col-md-3 col-sm-12 pull-right">
          <div class="radiobuttons-holder1 rtl"> <!-- Add Class .rtl -->
            <input type="radio" name="q_<?php echo $_smarty_tpl->tpl_vars['q']->value['id'];?>
" value="1" id="radio1<?php echo $_smarty_tpl->tpl_vars['q']->value['id'];?>
" class="css-radiobutton2" <?php echo $_smarty_tpl->tpl_vars['check1']->value;?>
 />
            <label for="radio1<?php echo $_smarty_tpl->tpl_vars['q']->value['id'];?>
" style="color: #000;" class="css-label2 radGroup3 lsd">ميں عزم كروطط ححهوطط</label>
          </div>
        </div>
        <div class="clearfix"></div>
    <?php
$_smarty_tpl->tpl_vars['q'] = $__foreach_q_0_saved_local_item;
}
if ($__foreach_q_0_saved_item) {
$_smarty_tpl->tpl_vars['q'] = $__foreach_q_0_saved_item;
}
?>
  <?php }?>
        
  <p style="color:#444444;">We recommend you to complete the remaining questionnaire pertaining your educational journey. <a data-toggle="collapse" href="#collapseExample" aria-expanded="false" aria-controls="collapseExample" style="color:#155485;">Show / Hide</a></p><br>
  
  <div class="collapse" id="collapseExample">      
    <?php if ($_smarty_tpl->tpl_vars['ques']->value) {?>
      <?php
$_from = $_smarty_tpl->tpl_vars['ques']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_q_1_saved_item = isset($_smarty_tpl->tpl_vars['q']) ? $_smarty_tpl->tpl_vars['q'] : false;
$_smarty_tpl->tpl_vars['q'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['q']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['q']->value) {
$_smarty_tpl->tpl_vars['q']->_loop = true;
$__foreach_q_1_saved_local_item = $_smarty_tpl->tpl_vars['q'];
?>

        <?php if ((isset($_smarty_tpl->tpl_vars['araz_data']->value[$_smarty_tpl->tpl_vars['i']->value]['answer']))) {?>
           <?php if (($_smarty_tpl->tpl_vars['araz_data']->value[$_smarty_tpl->tpl_vars['i']->value]['answer'] == '1')) {?>
             <?php $_smarty_tpl->tpl_vars['check1'] = new Smarty_Variable('checked', null);
$_smarty_tpl->ext->_updateScope->updateScope($_smarty_tpl, 'check1', 0);?>
           <?php } elseif (($_smarty_tpl->tpl_vars['araz_data']->value[$_smarty_tpl->tpl_vars['i']->value]['answer'] == '2')) {?>
             $check2 = 'checked';
           <?php } elseif (($_smarty_tpl->tpl_vars['araz_data']->value[$_smarty_tpl->tpl_vars['i']->value]['answer'] == '3')) {?>
             $check3 = 'checked';
           <?php } elseif (($_smarty_tpl->tpl_vars['araz_data']->value[$_smarty_tpl->tpl_vars['i']->value]['answer'] == '4')) {?>
             $check4 = 'checked';
            <?php }?>
          <?php }?>
          <?php $_smarty_tpl->tpl_vars["i"] = new Smarty_Variable($_smarty_tpl->tpl_vars['i']->value+1, null);
$_smarty_tpl->ext->_updateScope->updateScope($_smarty_tpl, "i", 0);?>
          <div class="blue-box1 rtl"> <!-- Add class rtl -->
            <h3><span class="inline-block lh3 arb_ques"><?php echo $_smarty_tpl->tpl_vars['i']->value;?>
. <?php echo $_smarty_tpl->tpl_vars['q']->value['question_preview'];?>
</span></h3>
          </div>
          <div class="clearfix"></div> <!-- do not delete -->
          <div class="col-md-3 col-sm-12 pull-right">
            <div class="radiobuttons-holder1 rtl"> <!-- Add Class .rtl -->
              <input type="radio" name="q_<?php echo $_smarty_tpl->tpl_vars['q']->value['id'];?>
" value="1" id="radio1<?php echo $_smarty_tpl->tpl_vars['q']->value['id'];?>
" class="css-radiobutton2" <?php echo $_smarty_tpl->tpl_vars['check1']->value;?>
 />
              <label for="radio1<?php echo $_smarty_tpl->tpl_vars['q']->value['id'];?>
" style="color: #000;" class="css-label2 radGroup3 lsd">ميں عزم كروطط ححهوطط</label>
            </div>
          </div>
          <div class="clearfix"></div>
        <?php
$_smarty_tpl->tpl_vars['q'] = $__foreach_q_1_saved_local_item;
}
if ($__foreach_q_1_saved_item) {
$_smarty_tpl->tpl_vars['q'] = $__foreach_q_1_saved_item;
}
?>
  	<?php }?>
  </div>
  <?php if ((!$_smarty_tpl->tpl_vars['ques_preffered']->value && !$_smarty_tpl->tpl_vars['ques']->value)) {?>
      <div class="blue-box1 rtl"> <!-- Add class rtl -->
        <h3><span class="inline-block bigger1 lh1 w500 left-mgn20">1</span>
          <span class="inline-block lh3">&#1587;&#1608;&#1601; &#1578;&#1603;&#1608;&#1606; &#1602;&#1575;&#1583;&#1585;&#1577; &#1593;&#1604;&#1609; &#1581;&#1590;&#1608;&#1585; waaz &#1575;&#1604;&#1603;&#1575;&#1605;&#1604; &#1605;&#1606; &#1603;&#1604; &#1578;&#1587;&#1593;&#1577; &#1571;&#1610;&#1575;&#1605; &#1605;&#1606; &#1575;&#1588;&#1575;&#1585;&#1575; &#1605;&#1576;&#1575;&#1585;&#1603;&#1577; &#1591;&#1601;&#1604;&#1603;&#1567;</span>
        </h3>
      </div>
      <div class="clearfix"></div> <!-- do not delete -->
      <div class="col-md-3 col-sm-12">
        <div class="radiobuttons-holder1 rtl"> <!-- Add Class .rtl -->
          <input type="radio" name="q_1" value="1" id="radio11" class="css-radiobutton1" />
          <label for="radio11" style="color: #000;" class="css-label1 radGroup3 lsd">ميں عزم كروطط ححهوطط</label>
        </div>
      </div>
      <div class="clearfix"></div>
  <?php }?>

        <!--div class="blue-box1">
          <h3 class="bordered">
            <span class="lsd">
              <div class="show lsd large-fonts text-center" dir="rtl"><?php echo '<?php ';?>echo $araz_notes[0]['note_ar']; <?php echo '?>';?></div>
            </span>
          </h3>
          <div class="clearfix"></div> <!-- do not delete -->
        <!--/div-->

        <div class="clearfix"></div> <!-- do not delete -->
        <input type="submit" class="submit1" name="submit" value="Proceed with Araz"/>
        <div class="clearfix"></div> <!-- do not delete -->
      </form>
      <div class="clearfix"></div> <!-- do not delete -->
    </div>
  </div>
</div>

<style>
  .morectnt span {
    display: none;
  }
  .showmoretxt {
    text-decoration: none;
  }
</style>

<?php echo '<script'; ?>
 type="text/javascript">
// Form validation code will come here.
  function validate()
  {
	//<?php
$_from = $_smarty_tpl->tpl_vars['ques']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_q_2_saved_item = isset($_smarty_tpl->tpl_vars['q']) ? $_smarty_tpl->tpl_vars['q'] : false;
$_smarty_tpl->tpl_vars['q'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['q']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['q']->value) {
$_smarty_tpl->tpl_vars['q']->_loop = true;
$__foreach_q_2_saved_local_item = $_smarty_tpl->tpl_vars['q'];
?>

//      if ($('input[name=q_<?php echo '<?php ';?>echo $q['id']; <?php echo '?>';?>]:checked').length <= 0)
//      {
//        alert("Please Select any Answer!");
//        return false;
//      }

	//<?php
$_smarty_tpl->tpl_vars['q'] = $__foreach_q_2_saved_local_item;
}
if ($__foreach_q_2_saved_item) {
$_smarty_tpl->tpl_vars['q'] = $__foreach_q_2_saved_item;
}
?>
  }
//-->
<?php echo '</script'; ?>
>

<?php echo '<script'; ?>
>
  $(function() {
    var showTotalChar = 200, showChar = "Read More", hideChar = "Hide";
    $('.show').each(function() {
      var content = $(this).text();
      if (content.length > showTotalChar) {
        var con = content.substr(0, showTotalChar);
        var hcon = content.substr(showTotalChar, content.length - showTotalChar);
        var txt = con + '<span class="dots">...</span><span class="morectnt"><span>' + hcon + '</span>&nbsp;&nbsp;<a href="" class="showmoretxt">' + showChar + '</a></span>';
        $(this).html(txt);
      }
    });
    $(".showmoretxt").click(function() {
      if ($(this).hasClass("sample")) {
        $(this).removeClass("sample");
        $(this).text(showChar);
      } else {
        $(this).addClass("sample");
        $(this).text(hideChar);
      }
      $(this).parent().prev().toggle();
      $(this).prev().toggle();
      return false;
    });
  });

  function MM_goToURL() { //v3.0
    var i, args=MM_goToURL.arguments; document.MM_returnValue = false;
    for (i=0; i<(args.length-1); i+=2) eval(args[i]+".location='"+args[i+1]+"'");
  }

<?php echo '</script'; ?>
><?php }
}
