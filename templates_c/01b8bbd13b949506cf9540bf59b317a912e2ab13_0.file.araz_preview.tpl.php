<?php
/* Smarty version 3.1.29, created on 2016-02-28 20:51:24
  from "/var/www/html/smarty_tlb2/templates/araz_preview.tpl" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.29',
  'unifunc' => 'content_56d31074c17f88_25166365',
  'file_dependency' => 
  array (
    '01b8bbd13b949506cf9540bf59b317a912e2ab13' => 
    array (
      0 => '/var/www/html/smarty_tlb2/templates/araz_preview.tpl',
      1 => 1456480868,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_56d31074c17f88_25166365 ($_smarty_tpl) {
?>
/*<?php echo $_smarty_tpl->tpl_vars['hifz_table']->value;?>

<?php echo $_smarty_tpl->tpl_vars['madrasah_table']->value;?>
*/
<?php echo $_smarty_tpl->tpl_vars['institute_table']->value;?>


<?php if ((in_array($_smarty_tpl->tpl_vars['marhala_id']->value,array(6,7,8)))) {?>
  
  $min_date = FALSE;
  <?php if ($_smarty_tpl->tpl_vars['institute_data']->value) {?>
    <?php
$_from = $_smarty_tpl->tpl_vars['institute_data']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_inst_0_saved_item = isset($_smarty_tpl->tpl_vars['inst']) ? $_smarty_tpl->tpl_vars['inst'] : false;
$_smarty_tpl->tpl_vars['inst'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['inst']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['inst']->value) {
$_smarty_tpl->tpl_vars['inst']->_loop = true;
$__foreach_inst_0_saved_local_item = $_smarty_tpl->tpl_vars['inst'];
?>
      <?php $_smarty_tpl->smarty->ext->_var->createLocalArrayVariable($_smarty_tpl, 'all_dates', null);
$_smarty_tpl->tpl_vars['all_dates']->value[] = $_smarty_tpl->tpl_vars['inst']->value['course_started'];
$_smarty_tpl->ext->_updateScope->updateScope($_smarty_tpl, 'all_dates', 0);?>
      <?php $_smarty_tpl->tpl_vars['tmp_dt'] = new Smarty_Variable(strtotime($_smarty_tpl->tpl_vars['inst']->value['course_started']), null);
$_smarty_tpl->ext->_updateScope->updateScope($_smarty_tpl, 'tmp_dt', 0);?>
      <?php if (($_smarty_tpl->tpl_vars['min_date']->value > $_smarty_tpl->tpl_vars['tmp_dt']->value || $_smarty_tpl->tpl_vars['min_date']->value == FALSE)) {?>
      	<?php $_smarty_tpl->tpl_vars['min_date'] = new Smarty_Variable($_smarty_tpl->tpl_vars['tmp_dt']->value, null);
$_smarty_tpl->ext->_updateScope->updateScope($_smarty_tpl, 'min_date', 0);?>
      <?php } else { ?>
        <?php $_smarty_tpl->tpl_vars['min_date'] = new Smarty_Variable($_smarty_tpl->tpl_vars['min_date']->value, null);
$_smarty_tpl->ext->_updateScope->updateScope($_smarty_tpl, 'min_date', 0);?>
      <?php }?>
    <?php
$_smarty_tpl->tpl_vars['inst'] = $__foreach_inst_0_saved_local_item;
}
if ($__foreach_inst_0_saved_item) {
$_smarty_tpl->tpl_vars['inst'] = $__foreach_inst_0_saved_item;
}
?>
  <?php }?>
  
  /*usort($all_dates, function($a, $b) {
    return strtotime($a) < strtotime($b) ? -1: 1;
  });

  $lowest_date = $all_dates[0];
  foreach($institute_data as $elementKey => $element) {
    foreach($element as $key => $value) {
      if($key == 'course_started' && $value == $lowest_date){
        $current_study_array = $institute_data[$elementKey];
      } 
    }
  }*/
    
  <?php $_smarty_tpl->tpl_vars['current_date'] = new Smarty_Variable(time(), null);
$_smarty_tpl->ext->_updateScope->updateScope($_smarty_tpl, 'current_date', 0);?>
    <?php if (($_smarty_tpl->tpl_vars['min_date']->value > $_smarty_tpl->tpl_vars['current_date']->value)) {?>
    
      <div class="blue-box1" style="background-color: #cf7f15">
      <h3>Current study details:</h3>
      <p>If you have completed a course, and are waiting for the pursuing of your next course, fill in the last course details.</p>
      <p>&nbsp;</p>
      <div class="col-md-6 col-sm-12 shift">
        <div class="shift10">
          <input type="text" name="course_name" placeholder="Course Name" id="search_course" class="search_course required" required>
          <div id="result_course"></div>
        </div>
      </div>
      <div class="col-md-6 col-sm-12 shift">
        <div class="shift10">
          <input type="text" name="institute_name" placeholder="Institute Name" id="search_inst" class="search_institute required" required>
          <div id="result_institute"></div>
        </div>
      </div>
      <div class="col-md-6 col-sm-12 shift">
        <div class="shift10">
          <select name="institute_country" onChange="showcity(this.value)" class="required" required>
            <option value="">Institute Country</option>
            <?php if (($_smarty_tpl->tpl_vars['countries']->value != '')) {?>
              <?php
$_from = $_smarty_tpl->tpl_vars['countries']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_cntry_1_saved_item = isset($_smarty_tpl->tpl_vars['cntry']) ? $_smarty_tpl->tpl_vars['cntry'] : false;
$_smarty_tpl->tpl_vars['cntry'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['cntry']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['cntry']->value) {
$_smarty_tpl->tpl_vars['cntry']->_loop = true;
$__foreach_cntry_1_saved_local_item = $_smarty_tpl->tpl_vars['cntry'];
?>
                <option value="<?php echo $_smarty_tpl->tpl_vars['cntry']->value['name'];?>
"><?php echo $_smarty_tpl->tpl_vars['cntry']->value['name'];?>
</option>
              <?php
$_smarty_tpl->tpl_vars['cntry'] = $__foreach_cntry_1_saved_local_item;
}
if ($__foreach_cntry_1_saved_item) {
$_smarty_tpl->tpl_vars['cntry'] = $__foreach_cntry_1_saved_item;
}
?>
            <?php }?>
          </select>
        </div>
      </div>
      <div class="col-md-6 col-sm-12 shift">
        <div  class="shift10" id="your_city">
          <select name="madrasah_city" class="required" required>
            <option value="">Institute City</option>
          </select>
        </div>
      </div>
      <div class="row"></div>
      <div class="row"></div>
      <?php $_smarty_tpl->tpl_vars['cur_year'] = new Smarty_Variable(date('Y'), null);
$_smarty_tpl->ext->_updateScope->updateScope($_smarty_tpl, 'cur_year', 0);?>
      <?php $_smarty_tpl->tpl_vars['rec_year'] = new Smarty_Variable($_smarty_tpl->tpl_vars['cur_year']->value-4, null);
$_smarty_tpl->ext->_updateScope->updateScope($_smarty_tpl, 'rec_year', 0);?>
      <?php $_smarty_tpl->tpl_vars['nxt_year'] = new Smarty_Variable($_smarty_tpl->tpl_vars['cur_year']->value+4, null);
$_smarty_tpl->ext->_updateScope->updateScope($_smarty_tpl, 'nxt_year', 0);?>
    <!--      <div class="col-md-6 col-sm-12 shift">
        <div  class="shift10">
          <select name="course_start_month" class="required" required>
            <option value="">Course Start Month</option>
            <?php echo '<?php
            ';?>foreach ($course_month_array as $str_mnth) {
              <?php echo '?>';?>
              <option value="<?php echo '<?php ';?>//echo $str_mnth; <?php echo '?>';?>"><?php echo '<?php ';?>//echo $str_mnth; <?php echo '?>';?></option>
              <?php echo '<?php
            ';?>}
            <?php echo '?>';?>
          </select>
        </div>
      </div>
      <div class="col-md-6 col-sm-12 shift">
        <div  class="shift10">
          <select name="course_start_year" class="required" required>
            <option value="">Course Start Year</option>
            <?php echo '<?php
            ';?>for ($i = $rec_year; $i <= $nxt_year; $i++) {
              <?php echo '?>';?>
              <option value="<?php echo '<?php ';?>//echo $i; <?php echo '?>';?>"><?php echo '<?php ';?>//echo $i; <?php echo '?>';?></option>
              <?php echo '<?php
            ';?>}
            <?php echo '?>';?>
          </select>
        </div>
      </div>
      <div class="col-md-6 col-sm-12 shift">
        <div  class="shift10">
          <select name="course_end_month">
            <option value="">Course End Month</option>
            <?php echo '<?php
            ';?>foreach ($course_month_array as $end_mnth) {
              <?php echo '?>';?>
              <option value="em_<?php echo '<?php ';?>//echo $end_mnth; <?php echo '?>';?>"><?php echo '<?php ';?>//echo $end_mnth; <?php echo '?>';?></option>
              <?php echo '<?php
            ';?>}
            <?php echo '?>';?>
          </select>
        </div>
      </div>
      <div class="col-md-6 col-sm-12 shift">
        <div  class="shift10">
          <select name="course_end_year">
            <option value="">Course End Year</option>
            <?php echo '<?php
            ';?>for ($j = $rec_year; $j <= $nxt_year; $j++) {
              <?php echo '?>';?>
              <option value="ey_<?php echo '<?php ';?>//echo $j; <?php echo '?>';?>"><?php echo '<?php ';?>//echo $j; <?php echo '?>';?></option>
              <?php echo '<?php
            ';?>}
            <?php echo '?>';?>
          </select>
        </div>
      </div>-->
      <div class="clearfix"></div> <!-- do not delete -->
    <div class="clearfix"></div> <!-- do not delete -->
    </div><br>
    
      <?php echo '<script'; ?>
>
        $(document).ready(function () {
          //Institute - Search
          $(".search_institute").keyup(function ()
          {
            var search_inst = $(this).val();
            if (search_inst == '') {
              $("#result_institute").fadeOut();
            } else {
              var dataString = search_inst;
              if (search_inst != '')
              {
                $.ajax({
                  type: "POST",
                  url: "ajax_search_institute/" + dataString,
                  cache: false,
                  success: function (html)
                  {
                    if (html != '') $("#result_institute").html(html).show();
                  }
                });
              }
              return false;
            }
          });
          
          $('#search_inst').click(function () {
            if ($(this).val() == '') {
              $("#result_institute").fadeOut();
            } else {
              $("#result_institute").fadeIn();
            }
          });
          
          $('#search_inst').focusout(function(){
            $("#result_institute").fadeOut(); 
          });
          
          // Course - Search
          $(".search_course").keyup(function ()
          {
            var search_course = $(this).val();
            if (search_course == '') {
              $("#result_course").fadeOut();
            } else {
              var datacourse = search_course;
              if (search_course != '')
              {
                $.ajax({
                  type: "POST",
                  url: "ajax_search_course/" + datacourse,
                  cache: false,
                  success: function (html)
                  {
                    if (html != '') $("#result_course").html(html).show();
                  }
                });
              }
              return false;
            }
          });
    
          $('#search_course').click(function () {
            if ($(this).val() == '') {
              $("#result_course").fadeOut();
            } else {
              $("#result_course").fadeIn();
            }
          });
          
          $('#search_course').focusout(function(){
            $("#result_course").fadeOut(); 
          });
    
        });
    
        function get_value_institute(str) {
          $('#search_inst').val(str);
          $("#result_institute").fadeOut();
        }
        
        function get_value_course(str) {
          $('#search_course').val(str);
          $("#result_course").fadeOut();
        }
        
        function showcity(str)
        {
          if (str == "")
          {
            document.getElementById("your_city").innerHTML = "";
            document.getElementById("mname").innerHTML = "";
            return;
          }
    
          if (window.XMLHttpRequest)
          {
            xmlhttp = new XMLHttpRequest();
          }
          else
          {
            xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
          }
    
    
    
          xmlhttp.onreadystatechange = function ()
          {
            if (xmlhttp.readyState == 4 && xmlhttp.status == 200)
            {
              document.getElementById("your_city").innerHTML = xmlhttp.responseText;
            }
          }
          xmlhttp.open("GET", "get_city_by_country/" + str, true);
          xmlhttp.send();
        }
      <?php echo '</script'; ?>
>
    
      <style>
        #result_institute
        {
          position:relative;
          width:auto;
          padding:10px;
          display:none;
          margin-top:-1px;
          border-top:0px;
          overflow:hidden;
          border:1px #CCC solid;
          background-color: white;
          height: 350;
          max-height: 350px;
          overflow: scroll;
        }
        #result_course
        {
          position:relative;
          width:auto;
          padding:10px;
          display:none;
          margin-top:-1px;
          border-top:0px;
          overflow:hidden;
          border:1px #CCC solid;
          background-color: white;
          height: 350;
          max-height: 350px;
          overflow: scroll;
        }
        .show
        {
          border-bottom:1px #999 dashed;
          font-size:13px; 
          color: #000;
          font-weight: 500;
        }
        .show:hover
        {
          background:#eee;
          color:#000;
          cursor:pointer;
        }
        .search_institute{
          color: #000;
        }
        .search_course{
          color: #000;
        }
      </style>
    <?php }?>
 <?php }
}
}
