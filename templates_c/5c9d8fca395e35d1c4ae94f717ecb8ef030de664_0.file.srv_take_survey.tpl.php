<?php
/* Smarty version 3.1.29, created on 2016-03-25 16:31:34
  from "/var/www/html/smarty_tlb2/templates/srv_take_survey.tpl" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.29',
  'unifunc' => 'content_56f51a8edde645_72597098',
  'file_dependency' => 
  array (
    '5c9d8fca395e35d1c4ae94f717ecb8ef030de664' => 
    array (
      0 => '/var/www/html/smarty_tlb2/templates/srv_take_survey.tpl',
      1 => 1458903644,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:srv_survey_question.tpl' => 1,
    'file:include/js_block.tpl' => 1,
    'file:include/footer.tpl' => 1,
  ),
),false)) {
function content_56f51a8edde645_72597098 ($_smarty_tpl) {
?>
<div class="container white-bg">
  <div class="col-md-12 col-sm-12">
    <div class="page-title">
      <p style="margin-top:5px;">
        <a href="<?php echo SERVER_PATH;?>
">Home</a> / 
        <a href="<?php echo SERVER_PATH;?>
srv_take_survey.php?survey_id=<?php echo $_smarty_tpl->tpl_vars['survey_id']->value;?>
"><?php echo $_smarty_tpl->tpl_vars['survey']->value['title'];?>
</a>
      <h1><?php echo $_smarty_tpl->tpl_vars['survey']->value['title'];?>
<!--span class="alfatemi-text">معلومات ذاتية</span--></h1>
    </div>
  </div>
  <div class="clearfix"></div> <!-- do not delete -->
  <form class="forms1 white" id="ques_form" name="raza_form" method="post">
    <div class="col-md-3 col-sm-12 hidden-xs">
      <div class="profile-box-static">
        <h3 class="uppercase text-center">Quizes</h3>
      </div>
      <div class="profile-box-static-bottom">
        <?php if ($_smarty_tpl->tpl_vars['surveys']->value) {?>
          <?php
$_from = $_smarty_tpl->tpl_vars['surveys']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_srv_0_saved_item = isset($_smarty_tpl->tpl_vars['srv']) ? $_smarty_tpl->tpl_vars['srv'] : false;
$_smarty_tpl->tpl_vars['srv'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['srv']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['srv']->value) {
$_smarty_tpl->tpl_vars['srv']->_loop = true;
$__foreach_srv_0_saved_local_item = $_smarty_tpl->tpl_vars['srv'];
?>
            <a href="<?php echo SERVER_PATH;?>
srv_take_survey.php?survey_id=<?php echo $_smarty_tpl->tpl_vars['srv']->value['survey_id'];?>
">» <?php echo $_smarty_tpl->tpl_vars['srv']->value['title'];?>
<hr></a>
          <?php
$_smarty_tpl->tpl_vars['srv'] = $__foreach_srv_0_saved_local_item;
}
if ($__foreach_srv_0_saved_item) {
$_smarty_tpl->tpl_vars['srv'] = $__foreach_srv_0_saved_item;
}
?>
        <?php }?>
      </div>
    </div>

    <div class="col-md-9 col-sm-12">
      <div class="row">
        <div class="col-xs-12 col-sm-12">
          <div class="static-show-button lsd large-fonts col-xs-12 col-sm-5" style="float:left;"><span class="pull-left"><?php echo $_smarty_tpl->tpl_vars['survey']->value['title'];?>
</span></div>
        </div>
        <div class="clearfix"></div>
        <div class="col-xs-12 col-sm-12">
          <div class="col-xs-12 col-sm-6">
            <!--img src="upload/courses_quiz/" class="img-responsive"-->
          </div>
          <div class="col-xs-12 col-sm-6">
            <h1 style="color:#000;"><b></b></h1>
          </div>
        </div>
        <div class="clearfix"></div>
        <div class="clearfix"><br></div>
          <?php $_smarty_tpl->smarty->ext->_subtemplate->render($_smarty_tpl, "file:srv_survey_question.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

      </div>
    </div>
    <div class="clearfix"></div>
  </form>
</div>
<style type="text/css">
.quiz-point span {
    background-color: #fff;
    color: #cf4914;
}
</style>
<style type="text/css">

  #dragScriptContainer{	/* BIG DIV containing HTML needed for the entire script */
    width:550px;
    margin:0 auto;
    border:1px solid #000;
    height:auto;
    margin-top:20px;
    padding:3px;
    -moz-user-select:no;
    overflow:hidden;
    background-color:#FFF;

  }
  h1{
    margin:5px;

  }
  #questionDiv{	/* Big div for all the questions */
    float:left;
    height:100%;
    width:300px;
    padding:2px;
  }

  #answerDiv{	/* Big div for all the answers */
    float:right;
    height:400px;
    width:150px;
    border:1px solid #FFF;
    padding:2px;	
  }

  #questionDiv div,#answerDiv div,#dragContent div{	/* General rules for small divs - i.e. specific questions and answers */
    width:145px;
    height:20px;
    line-height:20px;
    float:left;
    margin-right:2px;
    margin-bottom:2px;
    text-align:center;

  }

  #dragContent div{	/* Drag content div - i.e. specific answers when they are beeing dragged */
    border:1px solid #000;
  }

  #answerDiv .dragDropSmallBox{	/* Small answer divs */
    border:1px solid #000;
    cursor:pointer;
    background-color:#CF4914;
  }

  #questionDiv .dragDropSmallBox{	/* Small answer divs */
    border:1px solid #000;
    cursor:pointer;
    background-color:#549BC7;
  }

  #questionDiv div div{	/* DIV after it has been dragged from right to left box */
    margin:0px;
    border:0px;
    padding:0px;
    background-color:#FFF;
    cursor:pointer;
  }
  #questionDiv .destinationBox{	/* Small empty boxes for the questions - i.e. where answers could be dragged */
    border:0px;
    background-color:#DDD;
    width:147px;	/* Width of #questionDiv div + 2 */
    height:20px;



  }
  #questionDiv .correctAnswer{	/* CSS indicating correct answer */
    background-color:green;
    color:#fff;
    border:1px solid #000;
  }
  #questionDiv .wrongAnswer{	/* CSS indicating wrong answer */
    background-color:red;
    color:#fff;
    border:1px solid #000;
  }

  #dragContent div{
    background-color:#FFF;
  }

  #questionDiv .dragContentOver{	/* Mouse over question boxes - i.e. indicating where dragged element will be appended if mouse button is relased */
    border:1px solid #F00;
  }

  #answerDiv.dragContentOver{	/* Mouse over answer box - i.e. indicating where dragged element will be appended if mouse button is relased */
    border:1px solid #F00;
  }

  /* NEVER CHANGE THIS */
  #dragContent{
    position:absolute;
    display:none;
  }	
  p{
    margin:2px;
    font-size:0.9em;
  }

</style>
<?php $_smarty_tpl->smarty->ext->_subtemplate->render($_smarty_tpl, "file:include/js_block.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

<?php echo '<script'; ?>
 type="text/javascript">

  var shuffleQuestions = true;	/* Shuffle questions ? */
  var shuffleAnswers = true;	/* Shuffle answers ? */
  var lockedAfterDrag = false;	/* Lock items after they have been dragged, i.e. the user get's only one shot for the correct answer */

  function quizIsFinished()
  {
    $('#your_answer').val('1');
  }

  /* Don't change anything below here */
  var dragContentDiv = false;
  var dragContent = false;

  var dragSource = false;
  var dragDropTimer = -1;
  var destinationObjArray = new Array();
  var destination = false;
  var dragSourceParent = false;
  var dragSourceNextSibling = false;
  var answerDiv;
  var questionDiv;
  var sourceObjectArray = new Array();
  var arrayOfEmptyBoxes = new Array();
  var arrayOfAnswers = new Array();

  function getTopPos(inputObj)
  {
    if (!inputObj || !inputObj.offsetTop)
      return 0;
    var returnValue = inputObj.offsetTop;
    while ((inputObj = inputObj.offsetParent) != null)
      returnValue += inputObj.offsetTop;
    return returnValue;
  }

  function getLeftPos(inputObj)
  {
    if (!inputObj || !inputObj.offsetLeft)
      return 0;
    var returnValue = inputObj.offsetLeft;
    while ((inputObj = inputObj.offsetParent) != null)
      returnValue += inputObj.offsetLeft;
    return returnValue;
  }

  function cancelEvent()
  {
    return false;
  }

  function initDragDrop(e)
  {
    if (document.all)
      e = event;
    if (lockedAfterDrag && this.parentNode.parentNode.id == 'questionDiv')
      return;
    dragContentDiv.style.left = e.clientX + Math.max(document.documentElement.scrollLeft, document.body.scrollLeft) + 'px';
    dragContentDiv.style.top = e.clientY + Math.max(document.documentElement.scrollTop, document.body.scrollTop) + 'px';
    dragSource = this;
    dragSourceParent = this.parentNode;
    dragSourceNextSibling = false;
    if (this.nextSibling)
      dragSourceNextSibling = this.nextSibling;
    if (!dragSourceNextSibling.tagName)
      dragSourceNextSibling = dragSourceNextSibling.nextSibling;

    dragDropTimer = 0;
    timeoutBeforeDrag();

    return false;
  }

  function timeoutBeforeDrag() {
    if (dragDropTimer >= 0 && dragDropTimer < 10) {
      dragDropTimer = dragDropTimer + 1;
      setTimeout('timeoutBeforeDrag()', 10);
      return;
    }
    if (dragDropTimer >= 10) {
      dragContentDiv.style.display = 'block';
      dragContentDiv.innerHTML = '';
      dragContentDiv.appendChild(dragSource);


    }
  }

  function dragDropMove(e)
  {
    if (dragDropTimer < 10) {
      return;
    }

    if (document.all)
      e = event;

    var scrollTop = Math.max(document.documentElement.scrollTop, document.body.scrollTop);
    var scrollLeft = Math.max(document.documentElement.scrollLeft, document.body.scrollLeft);

    dragContentDiv.style.left = e.clientX + scrollLeft + 'px';
    dragContentDiv.style.top = e.clientY + scrollTop + 'px';

    var dragWidth = dragSource.offsetWidth;
    var dragHeight = dragSource.offsetHeight;


    var objFound = false;

    var mouseX = e.clientX + scrollLeft;
    var mouseY = e.clientY + scrollTop;

    destination = false;
    for (var no = 0; no < destinationObjArray.length; no++) {
      var left = destinationObjArray[no]['left'];
      var top = destinationObjArray[no]['top'];
      var width = destinationObjArray[no]['width'];
      var height = destinationObjArray[no]['height'];

      destinationObjArray[no]['obj'].className = 'destinationBox';
      var subs = destinationObjArray[no]['obj'].getElementsByTagName('DIV');
      if (!objFound && subs.length == 0) {
        if (mouseX < (left / 1 + width / 1) && (mouseX + dragWidth / 1) > left && mouseY < (top / 1 + height / 1) && (mouseY + dragHeight / 1) > top) {
          destinationObjArray[no]['obj'].className = 'dragContentOver';
          destination = destinationObjArray[no]['obj'];
          objFound = true;
        }
      }
    }

    sourceObjectArray['obj'].className = '';

    if (!objFound) {
      var left = sourceObjectArray['left'];
      var top = sourceObjectArray['top'];
      var width = sourceObjectArray['width'];
      var height = sourceObjectArray['height'];

      if (mouseX < (left / 1 + width / 1) && (mouseX + dragWidth / 1) > left && mouseY < (top / 1 + height / 1) && (mouseY + dragHeight / 1) > top) {
        destination = sourceObjectArray['obj'];
        sourceObjectArray['obj'].className = 'dragContentOver';
      }
    }
    return false;
  }


  function dragDropEnd()
  {
    if (dragDropTimer < 10) {
      dragDropTimer = -1;
      return;
    }
    dragContentDiv.style.display = 'none';
    sourceObjectArray['obj'].style.backgroundColor = '#FFF';
    if (destination) {
      destination.appendChild(dragSource);
      destination.className = 'destinationBox';

      // Check if position is correct, i.e. correct answer to the question

      if (!destination.id || destination.id != 'answerDiv') {
        var previousEl = dragSource.parentNode.previousSibling;
        if (!previousEl.tagName)
          previousEl = previousEl.previousSibling;
        var numericId = previousEl.id.replace(/[^0-9]/g, '');
        var numericIdSource = dragSource.id.replace(/[^0-9]/g, '');

        if (numericId == numericIdSource) {
          dragSource.className = 'correctAnswer';
          checkAllAnswers();
        }
        else
          dragSource.className = 'wrongAnswer';
      }

      if (destination.id && destination.id == 'answerDiv') {
        dragSource.className = 'dragDropSmallBox';
      }

    } else {
      if (dragSourceNextSibling)
        dragSourceNextSibling.parentNode.insertBefore(dragSource, dragSourceNextSibling);
      else
        dragSourceParent.appendChild(dragSource);
    }
    dragDropTimer = -1;
    dragSourceNextSibling = false;
    dragSourceParent = false;
    destination = false;
  }

  function checkAllAnswers()
  {
    for (var no = 0; no < arrayOfEmptyBoxes.length; no++) {
      var sub = arrayOfEmptyBoxes[no].getElementsByTagName('DIV');
      if (sub.length == 0)
        return;

      if (sub[0].className != 'correctAnswer') {
        return;
      }

    }

    quizIsFinished();
  }



  function resetPositions()
  {
    if (dragDropTimer >= 10)
      return;

    for (var no = 0; no < destinationObjArray.length; no++) {
      if (destinationObjArray[no]['obj']) {
        destinationObjArray[no]['left'] = getLeftPos(destinationObjArray[no]['obj'])
        destinationObjArray[no]['top'] = getTopPos(destinationObjArray[no]['obj'])
      }

    }
    sourceObjectArray['left'] = getLeftPos(answerDiv);
    sourceObjectArray['top'] = getTopPos(answerDiv);
  }


  function initDragDropScript()
  {
    dragContentDiv = document.getElementById('dragContent');

    answerDiv = document.getElementById('answerDiv');
    answerDiv.onselectstart = cancelEvent;
    var divs = answerDiv.getElementsByTagName('DIV');
    var answers = new Array();

    for (var no = 0; no < divs.length; no++) {
      if (divs[no].className == 'dragDropSmallBox') {
        divs[no].onmousedown = initDragDrop;
        answers[answers.length] = divs[no];
        arrayOfAnswers[arrayOfAnswers.length] = divs[no];
      }

    }

    if (shuffleAnswers) {
      for (var no = 0; no < (answers.length * 10); no++) {
        var randomIndex = Math.floor(Math.random() * answers.length);
        answerDiv.appendChild(answers[randomIndex]);
      }
    }

    sourceObjectArray['obj'] = answerDiv;
    sourceObjectArray['left'] = getLeftPos(answerDiv);
    sourceObjectArray['top'] = getTopPos(answerDiv);
    sourceObjectArray['width'] = answerDiv.offsetWidth;
    sourceObjectArray['height'] = answerDiv.offsetHeight;


    questionDiv = document.getElementById('questionDiv');

    questionDiv.onselectstart = cancelEvent;
    var divs = questionDiv.getElementsByTagName('DIV');

    var questions = new Array();
    var questionsOpenBoxes = new Array();


    for (var no = 0; no < divs.length; no++) {
      if (divs[no].className == 'destinationBox') {
        var index = destinationObjArray.length;
        destinationObjArray[index] = new Array();
        destinationObjArray[index]['obj'] = divs[no];
        destinationObjArray[index]['left'] = getLeftPos(divs[no])
        destinationObjArray[index]['top'] = getTopPos(divs[no])
        destinationObjArray[index]['width'] = divs[no].offsetWidth;
        destinationObjArray[index]['height'] = divs[no].offsetHeight;
        questionsOpenBoxes[questionsOpenBoxes.length] = divs[no];
        arrayOfEmptyBoxes[arrayOfEmptyBoxes.length] = divs[no];
      }
      if (divs[no].className == 'dragDropSmallBox') {
        questions[questions.length] = divs[no];
      }

    }

    if (shuffleQuestions) {
      for (var no = 0; no < (questions.length * 10); no++) {
        var randomIndex = Math.floor(Math.random() * questions.length);

        questionDiv.appendChild(questions[randomIndex]);
        questionDiv.appendChild(questionsOpenBoxes[randomIndex]);

        destinationObjArray[destinationObjArray.length] = destinationObjArray[randomIndex];
        destinationObjArray.splice(randomIndex, 1);

        questionsOpenBoxes[questionsOpenBoxes.length] = questionsOpenBoxes[randomIndex];
        questionsOpenBoxes.splice(randomIndex, 1);
        questions[questions.length] = questions[randomIndex];
        questions.splice(randomIndex, 1);


      }
    }

    questionDiv.style.visibility = 'visible';
    answerDiv.style.visibility = 'visible';

    document.documentElement.onmouseup = dragDropEnd;
    document.documentElement.onmousemove = dragDropMove;
    setTimeout('resetPositions()', 150);
    window.onresize = resetPositions;
  }

  /* Reset the form */
  function dragDropResetForm()
  {
    for (var no = 0; no < arrayOfAnswers.length; no++) {
      arrayOfAnswers[no].className = 'dragDropSmallBox'
      answerDiv.appendChild(arrayOfAnswers[no]);
    }
  }

  window.onload = initDragDropScript;

<?php echo '</script'; ?>
>
<?php $_smarty_tpl->smarty->ext->_subtemplate->render($_smarty_tpl, "file:include/footer.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
}
}
