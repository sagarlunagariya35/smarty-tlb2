<?php
/* Smarty version 3.1.29, created on 2016-04-09 12:33:14
  from "/var/www/html/smarty_tlb2/templates/marahil_list.tpl" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.29',
  'unifunc' => 'content_5708a93233fd80_98138032',
  'file_dependency' => 
  array (
    '8383812ed31b1212870ffadf64407c888d4c7810' => 
    array (
      0 => '/var/www/html/smarty_tlb2/templates/marahil_list.tpl',
      1 => 1459318096,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:include/js_block.tpl' => 1,
    'file:include/footer.tpl' => 1,
  ),
),false)) {
function content_5708a93233fd80_98138032 ($_smarty_tpl) {
?>
<div class="container white-bg">
  <div class="col-md-12 col-sm-12">

    <div class="page-title">
      <p style="margin-top:5px;">
        <a href="#" class="active">My Araz for education</a></p>
      <h1>My Araz for education<span class="alfatemi-text">ماري تعليم واسطسس عرض</span></h1>
    </div>
  </div>
  <p>&nbsp;</p>
  <div class="clearfix"></div> <!-- do not delete -->
  <div class="col-md-12 col-sm-12">
    <form class="forms" action="<?php echo $_smarty_tpl->tpl_vars['server_path']->value;?>
marahil/" method="post">

        <div class="col-md-3 col-xs-12 mb20">
          <input type="radio" name="already_araz_done" id="already_araz_not_done" class="css-radiobutton1" />
          <label for="already_araz_not_done" class="css-label1 css-label3 radGroup1">Araz for New Raza</label><i class="fa fa-question-circle help-icon" data-toggle="popover" data-content="I want to do araz for education in Hadrat aaliyah."></i>
        </div>
        <div class="col-md-3 col-xs-12">
          <input type="radio" name="already_araz_done" id="already_araz_done" class="css-radiobutton1" value="1" />
          <label for="already_araz_done" class="css-label1 css-label3 radGroup1">Register existing Raza</label><i class="fa fa-question-circle help-icon" data-toggle="popover" data-content="I have already obtained raza from hadrat aaliyah through sources other than this website and would like to register my raza on this portal."></i>
        </div>
      <p>&nbsp;</p>
      <div class="clearfix"></div> <!-- do not delete -->
      <?php if (($_smarty_tpl->tpl_vars['age']->value > 17)) {?>
      <!--<div class="arz-list">If you want to do araz for your child, Please log out and log in again with your child's ITS ID.</div>-->
      <?php }?>
      
        <?php if (($_smarty_tpl->tpl_vars['age']->value < 17)) {?>
          <?php $_smarty_tpl->tpl_vars['marhala_text'] = new Smarty_Variable('below_5', null);
$_smarty_tpl->ext->_updateScope->updateScope($_smarty_tpl, 'marhala_text', 0);?>
        <?php } else { ?>
          <?php $_smarty_tpl->tpl_vars['marhala_text'] = new Smarty_Variable('above_5', null);
$_smarty_tpl->ext->_updateScope->updateScope($_smarty_tpl, 'marhala_text', 0);?>
        <?php }?>

      <ul class="arz-list">
        <a href="#" class="marhala-show-hide pull-right">Show</a>
        <div class="clearfix"><br><br></div>
        <li class="col-md-6 col-sm-12 below_5">
          <button type="submit"  class="col-xs-12" name="submit" value="1">Pre Primary <span>Marhala 1</span></button> 
        </li>
        <li class="col-md-6 col-sm-12 below_5">
          <button type="submit"  class="col-xs-12" name="submit" value="2">Primary <span>Marhala 2</span></button>
        </li>
        <li class="col-md-6 col-sm-12 below_5">
          <button type="submit"  class="col-xs-12" name="submit" value="3">Std 5<sup>th</sup>  - 7<sup>th</sup><span>Marhala 3</span></button>
        </li>
        <li class="col-md-6 col-sm-12 below_5">
          <button type="submit"  class="col-xs-12" name="submit" value="4">Std 8<sup>th</sup>  - 10<sup>th</sup> <span>Marhala 4</span></button>
        </li>
        <li class="col-md-6 col-sm-12 above_5">
          <button type="submit"  class="col-xs-12" name="submit" value="5">Std 11<sup>th</sup>  - 12<sup>th</sup> <span>Marhala 5</span></button>
        </li>
        <li class="col-md-6 col-sm-12 above_5">
          <button type="submit" class="col-xs-12" name="submit" value="6">Graduation <span>Marhala 6</span></button>
        </li>
        <li class="col-md-12 col-sm-12 above_5">
          <button type="submit" class="col-md-6 col-md-offset-3 col-xs-12" name="submit" value="7">Post Grad. <span>Marhala 7</span></button>
        </li>
        <li class="col-md-12 col-sm-12 above_5">
          <button type="submit" class="col-md-6 col-md-offset-3 col-xs-12" name="submit" value="8">Diploma / other courses<span>Others</span></button>
        </li>
        <span class="page-steps">Step : 1 out of 5</span>
      </ul>
      <div class="clearfix"></div>
    </form>
  </div>
  <div class="col-md-12 spacer"></div>
</div>

<?php $_smarty_tpl->smarty->ext->_subtemplate->render($_smarty_tpl, "file:include/js_block.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>


<?php echo '<script'; ?>
 type="text/javascript">
  $(document).ready(function () {

    $('input[type="radio"]').click(function () {
      $('.arz-list').show(600);
      $('.spacer').hide();
    });

    $('.arz-list').hide();
    
    $('.marhala-show-hide').click(function () {
      <?php if (($_smarty_tpl->tpl_vars['age']->value < 17)) {?>
         $(".above_5").show(600);
      <?php } else { ?>
         $(".below_5").show(600);
      <?php }?>
    });
       
    <?php if (($_smarty_tpl->tpl_vars['marhala_text']->value == 'below_5')) {?>
        $(".above_5").hide();
    <?php } else { ?>
        $(".below_5").hide();
    <?php }?>
  });
<?php echo '</script'; ?>
>
<?php $_smarty_tpl->smarty->ext->_subtemplate->render($_smarty_tpl, "file:include/footer.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

<?php }
}
