<?php
/* Smarty version 3.1.29, created on 2016-03-16 11:54:44
  from "/var/www/html/smarty_tlb2/templates/qrn_mumin_enroll.tpl" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.29',
  'unifunc' => 'content_56e8fc2cec2993_94335042',
  'file_dependency' => 
  array (
    '36b0db277d70f012b65450df5448ece0c0d62e9c' => 
    array (
      0 => '/var/www/html/smarty_tlb2/templates/qrn_mumin_enroll.tpl',
      1 => 1458109481,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:include/message.tpl' => 1,
    'file:include/qrn_message.tpl' => 1,
    'file:include/qrn_surat_list.tpl' => 1,
    'file:include/qrn_links.tpl' => 1,
    'file:include/js_block.tpl' => 1,
    'file:include/footer.tpl' => 1,
  ),
),false)) {
function content_56e8fc2cec2993_94335042 ($_smarty_tpl) {
?>
<div class="container white-bg">
  <div class="col-md-12 col-sm-12">
    <div class="page-title">
      <p style="margin-top:5px;">
        <a href="#" class="active">Mumin Enroll</a></p>
        <h1>Mumin Enroll</h1>
    </div>
  </div>
  <?php if ((isset($_smarty_tpl->tpl_vars['_SESSION']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_SUCCESS_MESSAGE']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_SUCCESS_MESSAGE']->value['index'] : null)]) || isset($_smarty_tpl->tpl_vars['_SESSION']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_ERROR_MESSAGE']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_ERROR_MESSAGE']->value['index'] : null)]))) {?>
    <div class="row">
      <?php $_smarty_tpl->smarty->ext->_subtemplate->render($_smarty_tpl, "file:include/message.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

    </div>
  <?php }?>
  <div class="clearfix"></div> <!-- do not delete -->
  <div class="col-md-12 col-sm-12">
    <form action="" method="post" name="mumin_form">
      <?php $_smarty_tpl->smarty->ext->_subtemplate->render($_smarty_tpl, "file:include/qrn_message.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

      <div class="body-container white-bg">
        <div class="hifz-tasmee-details-wrapper">
          <div class="htd-section">
            <div class="row">
              <div class="col-xs-12 col-md-3">
                <input type="text" class="form-control" value="<?php echo $_smarty_tpl->tpl_vars['mumin_its']->value;?>
" name="mumin_its" placeholder="Enter Mumin ITS">
              </div>
              <div class="col-xs-12 col-md-2">
                <button type="submit" name="search" class="btn btn-success">Search</button>
              </div>
            </div>
            <div class="clearfix"><br></div>
            
            <?php if ($_smarty_tpl->tpl_vars['mumin_user_data']->value) {?>
              <div class="row">
                <div class="col-xs-12 col-md-5">
                  <div class="select-muhaffix-wrapper">
                    <div class="select-muhaffix-list">
                      <div><span class="__enhead"><?php echo $_smarty_tpl->tpl_vars['user_logedin']->value->get_full_name();?>
</span>  <?php echo $_smarty_tpl->tpl_vars['user_logedin']->value->get_jamaat();?>
, <?php echo $_smarty_tpl->tpl_vars['user_logedin']->value->get_jamiat();?>
<br> <?php echo $_smarty_tpl->tpl_vars['user_logedin']->value->get_quran_sanad();?>
</div>
                    </div>
                  </div>
                </div>
                <div class="col-xs-12 col-md-3">
                  <select name="surah" id="surah" class="form-control">
                    <?php $_smarty_tpl->smarty->ext->_subtemplate->render($_smarty_tpl, "file:include/qrn_surat_list.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

                  </select>
                </div>
                <div class="col-xs-12 col-md-2">
                  <input type="text" name="ayat_to" class="qrn-text-box" id="ayat_to" placeholder="Enter Ayat No">
                </div>
                <div class="col-xs-12 col-md-2">
                  <button type="submit" name="submit" class="btn btn-primary pull-right">Enroll</button>
                </div>
              </div>
              <div class="clearfix"><br></div>
            <?php }?>
          </div>
          <div class="clearfix"><br></div>
          <?php $_smarty_tpl->smarty->ext->_subtemplate->render($_smarty_tpl, "file:include/qrn_links.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

        </div>
      </div>
    </form>
  </div>
</div>
<?php $_smarty_tpl->smarty->ext->_subtemplate->render($_smarty_tpl, "file:include/js_block.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

<?php $_smarty_tpl->smarty->ext->_subtemplate->render($_smarty_tpl, "file:include/footer.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
}
}
