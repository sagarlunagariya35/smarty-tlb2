<?php
/* Smarty version 3.1.29, created on 2016-04-11 14:58:42
  from "/var/www/html/smarty_tlb2/templates/crs_topics.tpl" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.29',
  'unifunc' => 'content_570b6e4a307835_11361964',
  'file_dependency' => 
  array (
    'd3cb60ab0d309ec09d152a6a6f1ddf72907a37f6' => 
    array (
      0 => '/var/www/html/smarty_tlb2/templates/crs_topics.tpl',
      1 => 1460024986,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:include/js_block.tpl' => 1,
  ),
),false)) {
function content_570b6e4a307835_11361964 ($_smarty_tpl) {
?>
<div class="container white-bg">
  <div class="col-md-12 col-sm-12">
    <div class="page-title">
      <p style="margin-top:5px;"><a href="#">Home</a> / <a href="#">Qasaid</a></p>
      <h1>Qasaid</h1>
    </div>
  </div>
  <div class="clearfix"></div>  
  <div class="col-md-12 col-sm-12">
	<div class="row">
		<div class="col-sm-3 side-bar">
			<div class="sidebar-block">
				<h3 class="sidebar-block-title">All courses <i class="fa fa-search"></i></h3>
				<ul class="courses-list">
                  <?php if ($_smarty_tpl->tpl_vars['topics']->value) {?>
                    <?php $_smarty_tpl->tpl_vars["i"] = new Smarty_Variable(1, null);
$_smarty_tpl->ext->_updateScope->updateScope($_smarty_tpl, "i", 0);?>
                    <?php
$_from = $_smarty_tpl->tpl_vars['topics']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_topic_0_saved_item = isset($_smarty_tpl->tpl_vars['topic']) ? $_smarty_tpl->tpl_vars['topic'] : false;
$_smarty_tpl->tpl_vars['topic'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['topic']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['topic']->value) {
$_smarty_tpl->tpl_vars['topic']->_loop = true;
$__foreach_topic_0_saved_local_item = $_smarty_tpl->tpl_vars['topic'];
?>
                      <?php $_smarty_tpl->tpl_vars['sub_topics'] = new Smarty_Variable($_smarty_tpl->tpl_vars['crs_topic']->value->get_all_topics($_smarty_tpl->tpl_vars['topic']->value['id']), null);
$_smarty_tpl->ext->_updateScope->updateScope($_smarty_tpl, 'sub_topics', 0);?>
                      <li>
                        <a <?php if ($_smarty_tpl->tpl_vars['sub_topics']->value) {?> data-toggle="collapse" class="list-toggle <?php if ($_smarty_tpl->tpl_vars['i']->value != '1') {?>collapsed<?php }?>" href="#sub-<?php echo $_smarty_tpl->tpl_vars['i']->value;?>
" <?php } else { ?> href="javascript:void(0);" <?php }?> ><?php echo $_smarty_tpl->tpl_vars['topic']->value['title'];?>
 <?php if ($_smarty_tpl->tpl_vars['sub_topics']->value) {?> <i class="fa fa-angle-down"></i> <?php }?></a>

                        <?php if ($_smarty_tpl->tpl_vars['sub_topics']->value) {?>
                          <ul class="sub-courses collapse <?php if ($_smarty_tpl->tpl_vars['i']->value == '1') {?>in<?php }?>" id="sub-<?php echo $_smarty_tpl->tpl_vars['i']->value;?>
">
                            <?php
$_from = $_smarty_tpl->tpl_vars['sub_topics']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_st_1_saved_item = isset($_smarty_tpl->tpl_vars['st']) ? $_smarty_tpl->tpl_vars['st'] : false;
$_smarty_tpl->tpl_vars['st'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['st']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['st']->value) {
$_smarty_tpl->tpl_vars['st']->_loop = true;
$__foreach_st_1_saved_local_item = $_smarty_tpl->tpl_vars['st'];
?>
                              <li><a href="<?php echo $_smarty_tpl->tpl_vars['server_path']->value;?>
crs_topics.php?topic_id=<?php echo $_smarty_tpl->tpl_vars['st']->value['id'];?>
"><?php echo $_smarty_tpl->tpl_vars['st']->value['title'];?>
</a></li>
                            <?php
$_smarty_tpl->tpl_vars['st'] = $__foreach_st_1_saved_local_item;
}
if ($__foreach_st_1_saved_item) {
$_smarty_tpl->tpl_vars['st'] = $__foreach_st_1_saved_item;
}
?>
                          </ul>
                        <?php }?>
                      </li>
                      <?php $_smarty_tpl->tpl_vars['i'] = new Smarty_Variable($_smarty_tpl->tpl_vars['i']->value+1, null);
$_smarty_tpl->ext->_updateScope->updateScope($_smarty_tpl, 'i', 0);?>
                    <?php
$_smarty_tpl->tpl_vars['topic'] = $__foreach_topic_0_saved_local_item;
}
if ($__foreach_topic_0_saved_item) {
$_smarty_tpl->tpl_vars['topic'] = $__foreach_topic_0_saved_item;
}
?>
                  <?php }?>
				</ul>
			</div>
			<div class="sidebar-block">
				<h3 class="sidebar-block-title">SORT BY <i class="fa fa-search"></i></h3>
				<ul class="courses-list">
					<li><a href="javascript:void(0);">Free Courses</a></li>
					<li><a href="javascript:void(0);" class="active">Courses Not Taken</a></li>
					<li><a href="javascript:void(0);">Courses in progress</a></li>
				</ul>
			</div>
			<div class="sidebar-block padding resume-card">
				<div class="r-name">
					<h2>FiQH</h2>
					<h5>Beginner</h5>
				</div>
				<span class="r-btn">50/250</span>
				<div class="progress">
					<div class="progress-bar progress-bar-danger" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width: 40%"></div>
				</div>
				<a href="javascript:void(0);" class="orange-btn btn">Resume</a>
			</div>
			<div class="sidebar-block profile-sum">
				<h3 class="sidebar-block-title">My Profile</h3>
				<img src="images/user-profile.jpg" />
				<div class="profile-sum-foot">
					<h2 class="user-name">MUSTAFA ABBAS</h2>
					<p>Courses completed: <i>3 of 26</i></p>
					<p>Courses in progress: <i>8 of 26</i></p>
				</div>
			</div>
		</div>
		<div class="col-sm-9 right-side-wrap">
			<div class="title-wrap clearfix">
				<h2><?php echo $_smarty_tpl->tpl_vars['topic_details']->value['title'];?>
</h2>
			</div>
			<div class="row category-wrap">
                <?php if ($_smarty_tpl->tpl_vars['sub_topics_of_topic']->value) {?>
                  <?php
$_from = $_smarty_tpl->tpl_vars['sub_topics_of_topic']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_sub_topic_2_saved_item = isset($_smarty_tpl->tpl_vars['sub_topic']) ? $_smarty_tpl->tpl_vars['sub_topic'] : false;
$_smarty_tpl->tpl_vars['sub_topic'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['sub_topic']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['sub_topic']->value) {
$_smarty_tpl->tpl_vars['sub_topic']->_loop = true;
$__foreach_sub_topic_2_saved_local_item = $_smarty_tpl->tpl_vars['sub_topic'];
?>
                    <?php $_smarty_tpl->tpl_vars['sub_sub_topics'] = new Smarty_Variable($_smarty_tpl->tpl_vars['crs_topic']->value->get_all_topics($_smarty_tpl->tpl_vars['sub_topic']->value['id']), null);
$_smarty_tpl->ext->_updateScope->updateScope($_smarty_tpl, 'sub_sub_topics', 0);?>
                    <div class="col-md-4 col-sm-6">
                      <div class="category-card">
                        <h2 class="category-name"><?php echo $_smarty_tpl->tpl_vars['sub_topic']->value['title'];?>
</h2>
                        <h4 class="category-subtitle">Tajweed</h4>
                        <p><?php echo $_smarty_tpl->tpl_vars['sub_topic']->value['description'];?>
</p>
                        <?php if ($_smarty_tpl->tpl_vars['sub_sub_topics']->value) {?>
                          <a href="<?php echo $_smarty_tpl->tpl_vars['server_path']->value;?>
crs_topics.php?topic_id=<?php echo $_smarty_tpl->tpl_vars['sub_topic']->value['id'];?>
" class="orange-btn">View Sub Topics </a>
                        <?php } else { ?>
                          <a href="<?php echo $_smarty_tpl->tpl_vars['server_path']->value;?>
crs_courses.php?topic_id=<?php echo $_smarty_tpl->tpl_vars['sub_topic']->value['id'];?>
" class="orange-btn">View Courses </a>
                        <?php }?>
                      </div>
                    </div>
                  <?php
$_smarty_tpl->tpl_vars['sub_topic'] = $__foreach_sub_topic_2_saved_local_item;
}
if ($__foreach_sub_topic_2_saved_item) {
$_smarty_tpl->tpl_vars['sub_topic'] = $__foreach_sub_topic_2_saved_item;
}
?>
                <?php }?>
			</div>
		</div>
	</div>
  </div>
</div>

<?php $_smarty_tpl->smarty->ext->_subtemplate->render($_smarty_tpl, "file:include/js_block.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>


<style>
  .blue-box1-level a{
    color:#FFF!important;
    text-decoration:none;
  }
  .progress{
    color:#000;
    font-size:12px;
    line-height:20px;
    text-align:center;
  }
</style>

<?php echo '<script'; ?>
>
  $(document).ready(function () {
    $('[data-toggle="popover"]').popover({
      placement: 'bottom'
    });
    $('body').on('click', function (e) {
      $('[data-toggle="popover"]').each(function () {
        //the 'is' for buttons that trigger popups
        //the 'has' for icons within a button that triggers a popup
        if (!$(this).is(e.target) && $(this).has(e.target).length === 0 && $('.popover').has(e.target).length === 0) {
          $(this).popover('hide');
        }
      });
    });

  });
  
<?php echo '</script'; ?>
><?php }
}
