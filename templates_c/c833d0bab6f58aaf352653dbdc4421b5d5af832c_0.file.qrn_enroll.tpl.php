<?php
/* Smarty version 3.1.29, created on 2016-03-16 16:07:44
  from "/var/www/html/smarty_tlb2/templates/qrn_enroll.tpl" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.29',
  'unifunc' => 'content_56e93778a79fa7_44225859',
  'file_dependency' => 
  array (
    'c833d0bab6f58aaf352653dbdc4421b5d5af832c' => 
    array (
      0 => '/var/www/html/smarty_tlb2/templates/qrn_enroll.tpl',
      1 => 1458124617,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:include/message.tpl' => 1,
    'file:include/qrn_message.tpl' => 1,
    'file:include/qrn_surat_list.tpl' => 1,
    'file:include/qrn_links.tpl' => 1,
    'file:include/js_block.tpl' => 1,
    'file:include/footer.tpl' => 1,
  ),
),false)) {
function content_56e93778a79fa7_44225859 ($_smarty_tpl) {
?>
<style>
  .lable-mrgn{
    margin-top:5px !important;
  }
</style>

<div class="container white-bg">
  <div class="col-md-12 col-sm-12">
    <div class="page-title">
      <p style="margin-top:5px;">
        <a href="#" class="active">Al-Akh al-Qurani</a></p>
      <h1>Al-Akh al-Qurani</h1>
    </div>
  </div>
  <?php if ((isset($_smarty_tpl->tpl_vars['success_message']->value) || isset($_smarty_tpl->tpl_vars['error_message']->value))) {?>
    <div class="row">
      <?php $_smarty_tpl->smarty->ext->_subtemplate->render($_smarty_tpl, "file:include/message.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

    </div>
  <?php }?>
  <div class="clearfix"></div> <!-- do not delete -->
  <div class="col-md-12 col-sm-12">
    <form action="" method="post" name="enroll_form" onsubmit="return(validate());">
      <?php $_smarty_tpl->smarty->ext->_subtemplate->render($_smarty_tpl, "file:include/qrn_message.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

      <div class="body-container white-bg">
        <div class="enroll-wrapper">
          <div class="en-left">
            <ul class="enroll-list no-style">
              <li>
                <input type="checkbox" name="mumin" value="1" <?php echo $_smarty_tpl->tpl_vars['mumin_selected']->value;?>
 id="chRecite"> <label for="chRecite"> <span class="__enhead">Recite / Tasmee</span> <?php echo $_smarty_tpl->tpl_vars['mumin_text']->value;?>
</label>
                <?php if ($_smarty_tpl->tpl_vars['show_surat_ayat_options']->value) {?>
                  <div class="col-xs-12 col-sm-12 show_mumin">
                    <div class="clearfix"></div>
                    <div class="col-xs-12 col-sm-6 al-kanz">
                      <select name="surah" id="" class="form-control">
                        <?php $_smarty_tpl->smarty->ext->_subtemplate->render($_smarty_tpl, "file:include/qrn_surat_list.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

                      </select>
                    </div>
                    <div class="col-xs-12 col-sm-6">
                      <input type="text" name="ayat_to" class="qrn-text-box" id="" placeholder="Enter Ayat No">
                    </div>
                  </div>
                <?php }?>
                <div class="clearfix"></div>
              </li>
              <div class="clearfix"></div>
              <li>
                <input type="checkbox" name="muhafiz" value="2" <?php echo $_smarty_tpl->tpl_vars['muhafiz_selected']->value;?>
 id="chListen"> <label for="chListen"> <span class="__enhead">Listen / Tehfeez</span> <?php echo $_smarty_tpl->tpl_vars['muhafiz_text']->value;?>
</label>
              </li>
            </ul>
            <hr>
            <h2>Before You Enroll Please Update Your Details If Required</h2>
            <div class="row">
              <div class="col-md-12">&nbsp;</div>
              <div class="form-group col-md-6">
                <lable class="color1"><b>E-mail:</b></lable>
                <div  class="shift20 lable-mrgn">
                  <input type="text" name="email" class="form-control" value="<?php echo $_smarty_tpl->tpl_vars['user_email']->value;?>
" placeholder="eg. abc@gmail.com">
                </div>
              </div>
              <div class="form-group col-md-6">
                <lable class="color1"><b>Mobile:</b></lable>
                <div  class="shift20 lable-mrgn">
                  <input type="text" name="mobile" class="form-control" value="<?php echo $_smarty_tpl->tpl_vars['user_contact']->value;?>
" placeholder="Current Mobile Number">
                </div>
              </div>
              <div class="form-group col-md-6">
                <lable class="color1"><b>Whatsapp:</b></lable>
                <div  class="shift20 lable-mrgn">
                  <input type="text" name="whatsapp" class="form-control" value="<?php echo $_smarty_tpl->tpl_vars['user_whatsapp']->value;?>
" placeholder="Whatsapp No.">
                </div>
              </div>
              <div class="form-group col-md-6">
                <lable class="color1"><b>Skype:</b></lable>
                <div  class="shift20 lable-mrgn">
                  <input type="text" name="viber" class="form-control" value="<?php echo $_smarty_tpl->tpl_vars['user_skype']->value;?>
" placeholder="Skype Id">
                </div>
              </div>
            </div>
            <?php if (($_smarty_tpl->tpl_vars['select_mumin']->value == FALSE || $_smarty_tpl->tpl_vars['select_muhafiz']->value == FALSE)) {?>
              <div class="__action">
                <button type="submit" name="submit" class="btn _wahid">Enroll for Al-Akh al-Quraani program</button>
              </div>
            <?php }?>
          </div>
          <div class="en-right">
            <img src="./img/qrn-intro-banner.jpg" alt="Banner" width="100%" />
          </div>
        </div>
      </div>
      <?php $_smarty_tpl->smarty->ext->_subtemplate->render($_smarty_tpl, "file:include/qrn_links.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

      <div class="clearfix"><br></div>
    </form>
    <form method="post" id="frm-del-mumin-grp">
      <input type="hidden" name="delete_mumin" id="del-mumin-val" value="">
    </form>
    <form method="post" id="frm-del-muhafiz-grp">
      <input type="hidden" name="delete_muhafiz" id="del-muhafiz-val" value="">
    </form>
  </div>
</div>
      
<?php $_smarty_tpl->smarty->ext->_subtemplate->render($_smarty_tpl, "file:include/js_block.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>


<?php echo '<script'; ?>
 type="text/javascript">
  $(document).ready(function () {
    $('#chRecite').change(function () {
      if (this.checked) {
        $('.show_mumin').show(600);
      } else {
        $('.show_mumin').hide(600);
      }
    });
  });

  $(".delete_mumin").confirm({
    text: "Are you sure you want to unregister from Al Akh al-Qurani program?",
    title: "Confirmation required",
    confirm: function (button) {
      $('#del-mumin-val').val($(button).attr('id'));
      $('#frm-del-mumin-grp').submit();
      alert('Are you sure you want to unregister from Al Akh al-Qurani program: ' + id);
    },
    cancel: function (button) {
      // nothing to do
    },
    confirmButton: "Yes",
    cancelButton: "No",
    post: true,
    confirmButtonClass: "btn-danger"
  });

  $(".delete_muhafiz").confirm({
    text: "Are you sure you want to unregister from Al Akh al-Qurani program?",
    title: "Confirmation required",
    confirm: function (button) {
      $('#del-muhafiz-val').val($(button).attr('id'));
      $('#frm-del-muhafiz-grp').submit();
      alert('Are you sure you want to unregister from Al Akh al-Qurani program: ' + id);
    },
    cancel: function (button) {
      // nothing to do
    },
    confirmButton: "Yes",
    cancelButton: "No",
    post: true,
    confirmButtonClass: "btn-danger"
  });

  function validate()
  {
    var mumin = document.enroll_form.mumin.checked;
    var muhafiz = document.enroll_form.muhafiz.checked;

    var error = 'Please Select any of Below :\n\n';
    var validate = true;

    if (mumin == '')
    {
      error += 'Please Select Recite\n';
      if (muhafiz == '')
      {
        error += 'Please Select Listen\n';
        validate = false;
      }
    }

    if (mumin != '') {
      if (document.enroll_form.surah.value == '') {
        error += 'Please select Surah.\n';
        validate = false;
      }

      if (document.enroll_form.ayat_to.value == '') {
        error += 'Please Enter Ayat No.\n';
        validate = false;
      }
    }

    if (validate == false)
    {
      alert(error);
      return validate;
    }
  }
<?php echo '</script'; ?>
>
<?php $_smarty_tpl->smarty->ext->_subtemplate->render($_smarty_tpl, "file:include/footer.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
}
}
