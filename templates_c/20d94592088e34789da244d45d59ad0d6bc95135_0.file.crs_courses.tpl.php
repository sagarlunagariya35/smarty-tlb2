<?php
/* Smarty version 3.1.29, created on 2016-04-11 15:28:48
  from "/var/www/html/smarty_tlb2/templates/crs_courses.tpl" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.29',
  'unifunc' => 'content_570b75583f4811_66543383',
  'file_dependency' => 
  array (
    '20d94592088e34789da244d45d59ad0d6bc95135' => 
    array (
      0 => '/var/www/html/smarty_tlb2/templates/crs_courses.tpl',
      1 => 1460368723,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:include/crs.course.tpl' => 1,
    'file:include/js_block.tpl' => 1,
  ),
),false)) {
function content_570b75583f4811_66543383 ($_smarty_tpl) {
?>
<div class="container white-bg">
  <div class="col-md-12 col-sm-12">
    <div class="page-title">
      <p style="margin-top:5px;"><a href="#">Home</a> / <a href="#">Qasaid</a></p>
      <h1>Qasaid</h1>
    </div>
  </div>
  <div class="clearfix"></div>  
  <div class="col-md-12 col-sm-12">
	<div class="row">
		<div class="col-sm-3 side-bar">
			<div class="sidebar-block">
				<h3 class="sidebar-block-title">All courses <i class="fa fa-search"></i></h3>
				<ul class="courses-list">
                  <?php if ($_smarty_tpl->tpl_vars['topics']->value) {?>
                    <?php $_smarty_tpl->tpl_vars["i"] = new Smarty_Variable(1, null);
$_smarty_tpl->ext->_updateScope->updateScope($_smarty_tpl, "i", 0);?>
                    <?php
$_from = $_smarty_tpl->tpl_vars['topics']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_topic_0_saved_item = isset($_smarty_tpl->tpl_vars['topic']) ? $_smarty_tpl->tpl_vars['topic'] : false;
$_smarty_tpl->tpl_vars['topic'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['topic']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['topic']->value) {
$_smarty_tpl->tpl_vars['topic']->_loop = true;
$__foreach_topic_0_saved_local_item = $_smarty_tpl->tpl_vars['topic'];
?>
                      <?php $_smarty_tpl->tpl_vars['sub_topics'] = new Smarty_Variable($_smarty_tpl->tpl_vars['crs_topic']->value->get_all_topics($_smarty_tpl->tpl_vars['topic']->value['id']), null);
$_smarty_tpl->ext->_updateScope->updateScope($_smarty_tpl, 'sub_topics', 0);?>
                      <li>
                        <a <?php if ($_smarty_tpl->tpl_vars['sub_topics']->value) {?> data-toggle="collapse" class="list-toggle <?php if ($_smarty_tpl->tpl_vars['i']->value != '1') {?>collapsed<?php }?>" href="#sub-<?php echo $_smarty_tpl->tpl_vars['i']->value;?>
" <?php } else { ?> href="javascript:void(0);" <?php }?> ><?php echo $_smarty_tpl->tpl_vars['topic']->value['title'];?>
 <?php if ($_smarty_tpl->tpl_vars['sub_topics']->value) {?> <i class="fa fa-angle-down"></i> <?php }?></a>

                        <?php if ($_smarty_tpl->tpl_vars['sub_topics']->value) {?>
                          <ul class="sub-courses collapse <?php if ($_smarty_tpl->tpl_vars['i']->value == '1') {?>in<?php }?>" id="sub-<?php echo $_smarty_tpl->tpl_vars['i']->value;?>
">
                            <?php
$_from = $_smarty_tpl->tpl_vars['sub_topics']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_st_1_saved_item = isset($_smarty_tpl->tpl_vars['st']) ? $_smarty_tpl->tpl_vars['st'] : false;
$_smarty_tpl->tpl_vars['st'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['st']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['st']->value) {
$_smarty_tpl->tpl_vars['st']->_loop = true;
$__foreach_st_1_saved_local_item = $_smarty_tpl->tpl_vars['st'];
?>
                              <li><a href="<?php echo $_smarty_tpl->tpl_vars['server_path']->value;?>
crs_topics.php?topic_id=<?php echo $_smarty_tpl->tpl_vars['st']->value['id'];?>
"><?php echo $_smarty_tpl->tpl_vars['st']->value['title'];?>
</a></li>
                            <?php
$_smarty_tpl->tpl_vars['st'] = $__foreach_st_1_saved_local_item;
}
if ($__foreach_st_1_saved_item) {
$_smarty_tpl->tpl_vars['st'] = $__foreach_st_1_saved_item;
}
?>
                          </ul>
                        <?php }?>
                      </li>
                      <?php $_smarty_tpl->tpl_vars['i'] = new Smarty_Variable($_smarty_tpl->tpl_vars['i']->value+1, null);
$_smarty_tpl->ext->_updateScope->updateScope($_smarty_tpl, 'i', 0);?>
                    <?php
$_smarty_tpl->tpl_vars['topic'] = $__foreach_topic_0_saved_local_item;
}
if ($__foreach_topic_0_saved_item) {
$_smarty_tpl->tpl_vars['topic'] = $__foreach_topic_0_saved_item;
}
?>
                  <?php }?>
				</ul>
			</div>
			<div class="sidebar-block">
				<h3 class="sidebar-block-title">SORT BY <i class="fa fa-search"></i></h3>
				<ul class="courses-list">
					<li><a href="javascript:void(0);">Free Courses</a></li>
					<li><a href="javascript:void(0);" class="active">Courses Not Taken</a></li>
					<li><a href="javascript:void(0);">Courses in progress</a></li>
				</ul>
			</div>
			<div class="sidebar-block padding profile-block">
				<h3 class="sidebar-block-title">My Profile</h3>
				<h2 class="user-name">MUSTAFA ABBAS</h2>
				<p class="info-text">Progress tracker</p>
				<div class="chart-holder clearfix">
					<canvas id="chart-area" width="85" height="85"></canvas>
					<div class="chart-data">
						<p><span class="completed-text">Completed: </span>30/90</p>
						<p><span class="progress-text">In Progress: </span>10/90</p>
						<p><span class="nottaken-text">Not taken: </span>30/90</p>
					</div>
				</div>
				<p class="info-text">My Achievements</p>
				<div class="achievement-list">
					<img src="images/badge1.png" alt="" />
					<img src="images/icn-trophy.png" alt="" />
				</div>
				<a href="javascript:void(0);" class="see-more">See more on my profile <i class="fa fa-arrow-right"></i></a>
			</div>
		</div>
		<div class="col-sm-9 right-side-wrap">
			<div class="title-wrap clearfix">
				<h2><?php echo $_smarty_tpl->tpl_vars['topic_details']->value['title'];?>
 - Courses Available</h2>
				<a href="<?php echo $_smarty_tpl->tpl_vars['server_path']->value;?>
crs_dashboard.php" class="back-link">Return to Dashboard</a>
			</div>
            <?php $_smarty_tpl->smarty->ext->_subtemplate->render($_smarty_tpl, "file:include/crs.course.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

		</div>
	</div>
  </div>
</div>

<?php $_smarty_tpl->smarty->ext->_subtemplate->render($_smarty_tpl, "file:include/js_block.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>


<style>
  .blue-box1-level a{
    color:#FFF!important;
    text-decoration:none;
  }
  .progress{
    color:#000;
    font-size:12px;
    line-height:20px;
    text-align:center;
  }
</style>

<?php echo '<script'; ?>
>
  $(document).ready(function () {
    $('[data-toggle="popover"]').popover({
      placement: 'bottom'
    });
    $('body').on('click', function (e) {
      $('[data-toggle="popover"]').each(function () {
        //the 'is' for buttons that trigger popups
        //the 'has' for icons within a button that triggers a popup
        if (!$(this).is(e.target) && $(this).has(e.target).length === 0 && $('.popover').has(e.target).length === 0) {
          $(this).popover('hide');
        }
      });
    });

	// chart
	var doughnutData = [
			{
				value: 33,
				color:"#cccccc",
				highlight: "#cccccc"
			},
			{
				value: 20,
				color: "#cf4914",
				highlight: "#cf4914"
			},
			{
				value: 33,
				color: "#337ab7",
				highlight: "#337ab7"
			}
		];

		window.onload = function(){
			var ctx = document.getElementById("chart-area").getContext("2d");
			window.myDoughnut = new Chart(ctx).Doughnut(doughnutData, {
				responsive : false,
				segmentStrokeWidth : 5,
				percentageInnerCutout : 80,
				animateRotate : false,
				scaleStartValue: 0,
				showScale: false,
				showTooltips: false,
				onAnimationComplete: addText
			});
		};
		
		function addText() {
		  var canvas = document.getElementById("chart-area");
		  var ctx = document.getElementById("chart-area").getContext("2d");

		  var cx = canvas.width / 2;
		  var cy = canvas.height / 2;

		  ctx.textAlign = 'center';
		  ctx.textBaseline = 'middle';
		  ctx.font = '18px Ubuntu';
		  ctx.fillStyle = 'black';
		  ctx.fillText("33%", cx, cy);

		}
  });
  
<?php echo '</script'; ?>
><?php }
}
