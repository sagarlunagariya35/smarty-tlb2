<?php
/* Smarty version 3.1.29, created on 2016-03-02 13:31:17
  from "/var/www/html/smarty_tlb2/templates/qrn_select_muhaffiz.tpl" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.29',
  'unifunc' => 'content_56d69dcdd1e4d2_33433123',
  'file_dependency' => 
  array (
    '9d1f6799fbedc968fcfbba13912cb205601c6c16' => 
    array (
      0 => '/var/www/html/smarty_tlb2/templates/qrn_select_muhaffiz.tpl',
      1 => 1456833707,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:include/message.tpl' => 1,
    'file:include/qrn_message.tpl' => 1,
    'file:include/qrn_links.tpl' => 1,
    'file:include/js_block.tpl' => 1,
    'file:include/footer.tpl' => 1,
  ),
),false)) {
function content_56d69dcdd1e4d2_33433123 ($_smarty_tpl) {
?>
<div class="container white-bg">
  <div class="col-md-12 col-sm-12">
    <div class="page-title">
      <p style="margin-top:5px;">
        <a href="#" class="active">Send Request for Tasmee</a></p>
      <h1>Send Request for Tasmee</h1>
    </div>
  </div>
  <?php if ((isset($_smarty_tpl->tpl_vars['_SESSION']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_SUCCESS_MESSAGE']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_SUCCESS_MESSAGE']->value['index'] : null)]) || isset($_smarty_tpl->tpl_vars['_SESSION']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_ERROR_MESSAGE']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_ERROR_MESSAGE']->value['index'] : null)]))) {?>
    <div class="row">
      <?php $_smarty_tpl->smarty->ext->_subtemplate->render($_smarty_tpl, "file:include/message.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

    </div>
  <?php }?>
  <div class="clearfix"></div> <!-- do not delete -->
  <?php $_smarty_tpl->smarty->ext->_subtemplate->render($_smarty_tpl, "file:include/qrn_message.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

  <div class="col-md-12 col-sm-12">
    <form action="" method="post" name="enroll_form" onsubmit="return(validate());">
      <div class="body-container">
        <div class="hifz-tasmee-details-wrapper">
          <div class="htd-section">
          <div class="qrn-reg-wrapper">
            <div class="qrn-reg-header">
              <span><strong>You can send 3 request only.</strong></span>
            </div>
          </div>
          <div class="clearfix"><br></div>
            
          <?php if ($_smarty_tpl->tpl_vars['count_pending_request']->value) {?>
            <h2>Pending Requests <span class="badge"><?php echo $_smarty_tpl->tpl_vars['count_pending_request']->value;?>
</span></h2>
            <?php if ($_smarty_tpl->tpl_vars['pending_request']->value) {?>
              <div class="row pending-request-wrapper">
                <?php
$_from = $_smarty_tpl->tpl_vars['pending_request']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_data_0_saved_item = isset($_smarty_tpl->tpl_vars['data']) ? $_smarty_tpl->tpl_vars['data'] : false;
$_smarty_tpl->tpl_vars['data'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['data']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['data']->value) {
$_smarty_tpl->tpl_vars['data']->_loop = true;
$__foreach_data_0_saved_local_item = $_smarty_tpl->tpl_vars['data'];
?>
                  <?php $_smarty_tpl->tpl_vars['muhaffiz_data'] = new Smarty_Variable($_smarty_tpl->tpl_vars['user_logedin']->value->loaduser($_smarty_tpl->tpl_vars['data']->value['muhafiz_its']), null);
$_smarty_tpl->ext->_updateScope->updateScope($_smarty_tpl, 'muhaffiz_data', 0);?>
                  <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                    <div class="pending-request">
                      <div class="__text">
                        <span class="__prhead"><?php echo $_smarty_tpl->tpl_vars['muhaffiz_data']->value->get_full_name();?>
</span>
                        <?php echo $_smarty_tpl->tpl_vars['muhaffiz_data']->value->get_jamaat();?>
, <?php echo $_smarty_tpl->tpl_vars['muhaffiz_data']->value->get_jamiat();?>
<br> <?php echo $_smarty_tpl->tpl_vars['muhaffiz_data']->value->get_quran_sanad();?>

                      </div>
                    </div>
                  </div>
                <?php
$_smarty_tpl->tpl_vars['data'] = $__foreach_data_0_saved_local_item;
}
if ($__foreach_data_0_saved_item) {
$_smarty_tpl->tpl_vars['data'] = $__foreach_data_0_saved_item;
}
?>
              </div>
            <?php }?>
          <?php }?>
          </div>
        </div>
        <div class="clearfix"></div>
        <div class="select-muhaffix-wrapper">
          <h2>Select Al-Akh Al-Quraani</h2>
          <div class="qrn-tabs">
            <ul>
              <li id="select_sadeeq" class="select_sadeeq _active">Select from List</li>
              <li id="select_its_id" class="select_its_id">Enter ITS ID</li>
            </ul>
          </div>
          
            <?php if ($_smarty_tpl->tpl_vars['already_set_muhafiz']->value) {?>
            <span>You are now connected with <?php echo $_smarty_tpl->tpl_vars['muhaffiz_name']->value;?>
 - <a href="#" class="remove_muhafiz" id="<?php echo $_smarty_tpl->tpl_vars['already_set_muhafiz']->value;?>
">Change Muhaffiz click here</a></span>
            <?php } else { ?>
            <div class="select-muhaffix-list view_sadeeq">
              <ul class="no-style clearfix row">
                <?php if ($_smarty_tpl->tpl_vars['select_muhafiz']->value) {?>
                  <?php $_smarty_tpl->tpl_vars["i"] = new Smarty_Variable(0, null);
$_smarty_tpl->ext->_updateScope->updateScope($_smarty_tpl, "i", 0);?>
                    <?php
$_from = $_smarty_tpl->tpl_vars['select_muhafiz']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_data_1_saved_item = isset($_smarty_tpl->tpl_vars['data']) ? $_smarty_tpl->tpl_vars['data'] : false;
$_smarty_tpl->tpl_vars['data'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['data']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['data']->value) {
$_smarty_tpl->tpl_vars['data']->_loop = true;
$__foreach_data_1_saved_local_item = $_smarty_tpl->tpl_vars['data'];
?>
                      <?php $_smarty_tpl->tpl_vars["i"] = new Smarty_Variable($_smarty_tpl->tpl_vars['i']->value+1, null);
$_smarty_tpl->ext->_updateScope->updateScope($_smarty_tpl, "i", 0);?>
                        <li class="col-lg-3 col-md-4 col-sm-6 col-xs-12">
                          <div><input type="radio" id="rdMember<?php echo $_smarty_tpl->tpl_vars['i']->value;?>
" name="rdmuhafiz" value="<?php echo $_smarty_tpl->tpl_vars['data']->value['muhafiz_its'];?>
"> <label for="rdMember<?php echo $_smarty_tpl->tpl_vars['i']->value;?>
"> <span class="__enhead"><?php echo $_smarty_tpl->tpl_vars['data']->value['muhafiz_full_name'];?>
</span>  <?php echo $_smarty_tpl->tpl_vars['data']->value['muhafiz_jamaat'];?>
, <?php echo $_smarty_tpl->tpl_vars['data']->value['muhafiz_jamiat'];?>
<br> <?php echo $_smarty_tpl->tpl_vars['data']->value['muhafiz_quran_sanad'];?>
<br></label></div>
                        </li>
                    <?php
$_smarty_tpl->tpl_vars['data'] = $__foreach_data_1_saved_local_item;
}
if ($__foreach_data_1_saved_item) {
$_smarty_tpl->tpl_vars['data'] = $__foreach_data_1_saved_item;
}
?>
                <?php } else { ?>
                  <li>Currently Muhafiz not here Check again later..</li>
                <?php }?>
              </ul>
            </div>
            <div class="row view_its_id">
              <div class="col-lg-3 col-md-4 col-sm-6 col-xs-12">
                <input type="text" class="form-control" name="muhafiz_its" placeholder="Enter Muhafiz ITS">
                <div class="clearfix"><br></div>
              </div>
            </div>
            <div class="__action">
              <button type="submit" name="submit" class="btn _wahid pull-left">Send Request for Tasmee</button>
            </div>
          <?php }?>
        </div>
        <div class="clearfix"><br></div>
          <?php $_smarty_tpl->smarty->ext->_subtemplate->render($_smarty_tpl, "file:include/qrn_links.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

      </div>
    </form>
    <form method="post" id="frm-del-muhafiz-grp">
      <input type="hidden" name="remove_muhafiz" id="del-muhafiz-val" value="">
    </form>
  </div>
</div>
          
<?php $_smarty_tpl->smarty->ext->_subtemplate->render($_smarty_tpl, "file:include/js_block.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>


<?php echo '<script'; ?>
 type="text/javascript">
  $('#select_sadeeq').on('click', function (e) {
    e.preventDefault();
    $("li.select_its_id").removeClass("_active");
    $("li.select_sadeeq").addClass("_active");
    $('.view_sadeeq').show(600);
    $('.view_its_id').hide(600);
    $('.show_ajx_result').hide();
  });

  $('#select_its_id').on('click', function (e) {
    e.preventDefault();
    $("li.select_its_id").addClass("_active");
    $("li.select_sadeeq").removeClass("_active");
    $('.view_sadeeq').hide(600);
    $('.view_its_id').show(600);
    $('.show_ajx_result').hide();
  });
  $('.view_its_id').hide();
  
  $(".remove_muhafiz").confirm({
    text: "Are you sure you want to change muhaffiz?",
    title: "Confirmation required",
    confirm: function (button) {
      $('#del-muhafiz-val').val($(button).attr('id'));
      $('#frm-del-muhafiz-grp').submit();
      alert('Are you sure you want to change muhaffiz: ' + id);
    },
    cancel: function (button) {
      // nothing to do
    },
    confirmButton: "Yes",
    cancelButton: "No",
    post: true,
    confirmButtonClass: "btn-danger"
  });
<?php echo '</script'; ?>
>
<?php $_smarty_tpl->smarty->ext->_subtemplate->render($_smarty_tpl, "file:include/footer.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
}
}
