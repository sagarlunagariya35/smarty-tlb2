<?php
/* Smarty version 3.1.29, created on 2016-04-07 13:39:07
  from "/var/www/html/smarty_tlb2/templates/qasaid_list.tpl" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.29',
  'unifunc' => 'content_570615a3459d39_17159468',
  'file_dependency' => 
  array (
    '456baac2b34106de462bf225bed37d9ffd5b1581' => 
    array (
      0 => '/var/www/html/smarty_tlb2/templates/qasaid_list.tpl',
      1 => 1459318175,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:include/js_block.tpl' => 1,
    'file:include/footer.tpl' => 1,
  ),
),false)) {
function content_570615a3459d39_17159468 ($_smarty_tpl) {
?>
<div class="container white-bg">
  <div class="col-md-12 col-sm-12">
    <div class="page-title">
      <p style="margin-top:5px;"><a href="#">Home</a> / <a href="#">Qasaid</a></p>
      <h1>Qasaid</h1>
    </div>
  </div>
  <div class="clearfix"></div> <!-- do not delete -->
  <div class="col-md-12 col-sm-12">
    <div class="page">
      <form class="form1 white" method="post" action="">

        <div class="clearfix"></div> <!-- do not delete -->  
        <div class="row">
          <div class="col-md-3 col-xs-12 hidden-xs">
            <div class="profile-box-static">
              <h3 class="uppercase text-center">Istifadah</h3>
            </div>
            <div class="profile-box-static-bottom">

              <a href="<?php echo $_smarty_tpl->tpl_vars['server_path']->value;?>
istifaadah/qasaid/">» Qasaid<hr></a>
            </div>
          </div>
          <div class="col-md-9 col-xs-12" >
            <div class="col-xs-12">&nbsp;</div>
            <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
              <?php if ($_smarty_tpl->tpl_vars['qasaid_alams']->value) {?>
                <?php $_smarty_tpl->tpl_vars["i"] = new Smarty_Variable(1, null);
$_smarty_tpl->ext->_updateScope->updateScope($_smarty_tpl, "i", 0);?>
                  <?php
$_from = $_smarty_tpl->tpl_vars['qasaid_alams']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_qa_0_saved_item = isset($_smarty_tpl->tpl_vars['qa']) ? $_smarty_tpl->tpl_vars['qa'] : false;
$_smarty_tpl->tpl_vars['qa'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['qa']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['qa']->value) {
$_smarty_tpl->tpl_vars['qa']->_loop = true;
$__foreach_qa_0_saved_local_item = $_smarty_tpl->tpl_vars['qa'];
?>
                    <?php $_smarty_tpl->tpl_vars["i"] = new Smarty_Variable($_smarty_tpl->tpl_vars['i']->value+1, null);
$_smarty_tpl->ext->_updateScope->updateScope($_smarty_tpl, "i", 0);?>
                    <?php $_smarty_tpl->tpl_vars['qasaides'] = new Smarty_Variable(get_all_qasaid($_smarty_tpl->tpl_vars['qa']->value['alam']), null);
$_smarty_tpl->ext->_updateScope->updateScope($_smarty_tpl, 'qasaides', 0);?>
                    <div class="panel panel-default">
                      <div class="panel-heading" role="tab" id="headingOne">
                        <h4 class="panel-title">
                          <a role="button" data-toggle="collapse" data-parent="#accordion" href="#<?php echo $_smarty_tpl->tpl_vars['i']->value;?>
" aria-expanded="true" aria-controls="collapseOne">
                            <?php echo $_smarty_tpl->tpl_vars['qa']->value['alam'];?>

                          </a>
                        </h4>
                      </div>
                      <div id="<?php echo $_smarty_tpl->tpl_vars['i']->value;?>
" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
                        <div class="panel-body">
                          <?php if ($_smarty_tpl->tpl_vars['qasaides']->value) {?>
                            <?php
$_from = $_smarty_tpl->tpl_vars['qasaides']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_q_1_saved_item = isset($_smarty_tpl->tpl_vars['q']) ? $_smarty_tpl->tpl_vars['q'] : false;
$_smarty_tpl->tpl_vars['q'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['q']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['q']->value) {
$_smarty_tpl->tpl_vars['q']->_loop = true;
$__foreach_q_1_saved_local_item = $_smarty_tpl->tpl_vars['q'];
?>
                              <?php $_smarty_tpl->tpl_vars['qasaid_done'] = new Smarty_Variable(get_qasaid_is_done($_smarty_tpl->tpl_vars['q']->value['id']), null);
$_smarty_tpl->ext->_updateScope->updateScope($_smarty_tpl, 'qasaid_done', 0);?>

                              <?php $_smarty_tpl->tpl_vars['get_slider1_val'] = new Smarty_Variable(get_log_qasaid_slider_value($_smarty_tpl->tpl_vars['user_its']->value,$_smarty_tpl->tpl_vars['q']->value['id'],'1') ? get_log_qasaid_slider_value($_smarty_tpl->tpl_vars['user_its']->value,$_smarty_tpl->tpl_vars['q']->value['id'],'1') : 0, null);
$_smarty_tpl->ext->_updateScope->updateScope($_smarty_tpl, 'get_slider1_val', 0);?>
                              <?php $_smarty_tpl->tpl_vars['get_slider2_val'] = new Smarty_Variable(get_log_qasaid_slider_value($_smarty_tpl->tpl_vars['user_its']->value,$_smarty_tpl->tpl_vars['q']->value['id'],'2') ? get_log_qasaid_slider_value($_smarty_tpl->tpl_vars['user_its']->value,$_smarty_tpl->tpl_vars['q']->value['id'],'2') : 0, null);
$_smarty_tpl->ext->_updateScope->updateScope($_smarty_tpl, 'get_slider2_val', 0);?>
                              <?php $_smarty_tpl->tpl_vars['get_slider3_val'] = new Smarty_Variable(get_log_qasaid_slider_value($_smarty_tpl->tpl_vars['user_its']->value,$_smarty_tpl->tpl_vars['q']->value['id'],'3') ? get_log_qasaid_slider_value($_smarty_tpl->tpl_vars['user_its']->value,$_smarty_tpl->tpl_vars['q']->value['id'],'3') : 0, null);
$_smarty_tpl->ext->_updateScope->updateScope($_smarty_tpl, 'get_slider3_val', 0);?>

                              <?php $_smarty_tpl->tpl_vars['total_sider_value'] = new Smarty_Variable($_smarty_tpl->tpl_vars['get_slider1_val']->value+$_smarty_tpl->tpl_vars['get_slider2_val']->value+$_smarty_tpl->tpl_vars['get_slider3_val']->value, null);
$_smarty_tpl->ext->_updateScope->updateScope($_smarty_tpl, 'total_sider_value', 0);?>
                              <?php $_smarty_tpl->tpl_vars['average_slider_value'] = new Smarty_Variable(100-($_smarty_tpl->tpl_vars['total_sider_value']->value/3), null);
$_smarty_tpl->ext->_updateScope->updateScope($_smarty_tpl, 'average_slider_value', 0);?>
                              <?php $_smarty_tpl->tpl_vars['progress'] = new Smarty_Variable(100-$_smarty_tpl->tpl_vars['average_slider_value']->value, null);
$_smarty_tpl->ext->_updateScope->updateScope($_smarty_tpl, 'progress', 0);?>
                           
                              <div class="col-xs-8">
                                <a href="<?php echo $_smarty_tpl->tpl_vars['server_path']->value;?>
istifaadah/qasaid/<?php echo $_smarty_tpl->tpl_vars['q']->value['id'];?>
/"><?php echo $_smarty_tpl->tpl_vars['q']->value['title'];?>
</a>
                              </div>
                              <div class="col-xs-4">
                                <div class="progress">
                                  <div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="<?php echo $_smarty_tpl->tpl_vars['progress']->value;?>
" aria-valuemin="0" aria-valuemax="100" style="width:<?php echo $_smarty_tpl->tpl_vars['progress']->value;?>
%">                                                   <?php if (($_smarty_tpl->tpl_vars['progress']->value == 0)) {?>
                                      <span><?php echo $_smarty_tpl->tpl_vars['progress']->value;?>
%</span>
                                    <?php } else { ?> 
                                      <?php echo number_format($_smarty_tpl->tpl_vars['progress']->value,2);?>
%
                                    <?php }?>
                                  </div>
                                </div>
                              </div>
                            <div class="clearfix"></div>
                            <hr class="sleek">
                          <?php
$_smarty_tpl->tpl_vars['q'] = $__foreach_q_1_saved_local_item;
}
if ($__foreach_q_1_saved_item) {
$_smarty_tpl->tpl_vars['q'] = $__foreach_q_1_saved_item;
}
?>
                        <?php }?>
                      </div>
                    </div>
                  </div>
                <?php
$_smarty_tpl->tpl_vars['qa'] = $__foreach_qa_0_saved_local_item;
}
if ($__foreach_qa_0_saved_item) {
$_smarty_tpl->tpl_vars['qa'] = $__foreach_qa_0_saved_item;
}
?>
              <?php }?>
            </div>
          </div>
        </div>
      </form>
    </div>
  </div>
</div>
<style>
  .blue-box1-level a {
    color: #FFF !important;
    text-decoration: none;
  }
  .progress {
    color: #000;
    font-size: 12px;
    line-height: 20px;
    text-align: center;
  }
</style>
<?php $_smarty_tpl->smarty->ext->_subtemplate->render($_smarty_tpl, "file:include/js_block.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

<?php $_smarty_tpl->smarty->ext->_subtemplate->render($_smarty_tpl, "file:include/footer.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
}
}
