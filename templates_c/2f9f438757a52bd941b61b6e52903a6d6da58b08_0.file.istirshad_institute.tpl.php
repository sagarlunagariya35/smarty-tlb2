<?php
/* Smarty version 3.1.29, created on 2016-02-29 09:36:56
  from "/var/www/html/smarty_tlb2/templates/istirshad_institute.tpl" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.29',
  'unifunc' => 'content_56d3c3e0743ba5_38562339',
  'file_dependency' => 
  array (
    '2f9f438757a52bd941b61b6e52903a6d6da58b08' => 
    array (
      0 => '/var/www/html/smarty_tlb2/templates/istirshad_institute.tpl',
      1 => 1456718797,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:include/message.tpl' => 1,
  ),
),false)) {
function content_56d3c3e0743ba5_38562339 ($_smarty_tpl) {
?>
<div class="container white-bg">
  <div class="col-md-12 col-sm-12">

    <div class="page-title">
      <p style="margin-top:5px;">

        <a href="<?php echo SERVER_PATH;?>
marahil/">My Araz for education</a> / <a href="#" class="active"><?php echo $_smarty_tpl->tpl_vars['panel_heading']->value;?>
</a></p>
      <h1><?php echo $_smarty_tpl->tpl_vars['panel_heading']->value;?>
<span class="alfatemi-text"><?php echo $_smarty_tpl->tpl_vars['panel_heading_ar']->value;?>
</span></h1>
    </div>
  </div>
  <?php if ((isset($_smarty_tpl->tpl_vars['_SESSION']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_SUCCESS_MESSAGE']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_SUCCESS_MESSAGE']->value['index'] : null)]) || isset($_smarty_tpl->tpl_vars['_SESSION']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_ERROR_MESSAGE']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_ERROR_MESSAGE']->value['index'] : null)]))) {?>
    <div class="row">
      <?php $_smarty_tpl->smarty->ext->_subtemplate->render($_smarty_tpl, "file:include/message.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

    </div>
  <?php }?>
  <div class="clearfix"></div> <!-- do not delete -->
  <div class="col-md-12 col-sm-12">
    <div class="page">

      <h6><span class="alfatemi-text" dir="rtl"><?php echo $_smarty_tpl->tpl_vars['kalemaat_nooraniyah']->value;?>
 - من الكلمات النورانية</span></h6>

<?php if (($_smarty_tpl->tpl_vars['is_institute_selected']->value != '1')) {?>
        <h4>Complete this form to submit your araz for <?php echo $_smarty_tpl->tpl_vars['panel_heading']->value;?>
. <span class="alfatemi-text" dir="rtl">تماري تعليم ني عرض واسطے اْ فارم بهري عرض كر</span></h4>

        <form class="forms1" name="inst_form" method="post" action="<?php echo SERVER_PATH;?>
marhala-<?php echo $_smarty_tpl->tpl_vars['group_id']->value;?>
/<?php echo get_marhala_slug($_smarty_tpl->tpl_vars['group_id']->value);?>
/select-institute/" onsubmit="return(validate());">
      <?php if ((count($_smarty_tpl->tpl_vars['institute_data']->value) < 1)) {?>
            <h3> 
              <div style="float: left;text-align: left" class="radiobuttons-holder">
                <input type="radio" name="is_institute_selected" id="course_decided" value="1" class="css-radiobutton" <?php if (($_smarty_tpl->tpl_vars['already_araz_done']->value == 1)) {?> checked <?php }?> />
                <label for="course_decided" class="pull-left css-label radGroup1 mb10">Raza mubarak </label><i class="fa fa-question-circle help-icon white-help example" rel="popover" data-html="true" data-placement="top" data-content="If you want to do araz for raza mubarak for a course you would like to pursue click here.<br />NOTE: If you have already decided which Institute and/or which place you would like to pursue the desired course, select them where and when prompted.<br />If you are not sure about INSTITUTE and/or PLACE, you may leave them blank and send araz for that later."></i><br>
                <div class="clearfix"></div>
                <input type="radio" name="is_institute_selected" id="course_not_decided" value="2" class="css-radiobutton" />
                <label for="course_not_decided" class="css-label radGroup1">Istirshad </label><i class="fa fa-question-circle help-icon white-help example" rel="popover" data-html="true" data-placement="top" data-content="If you want to do araz for Maula's TUS raye mubarak from a few choices (istirshad) you may have (i.e. Course, City, Institution options), click here."></i>
              </div>
            </h3>
            <div class="clearfix"></div>
            <div class="course_yes">
              <p class="bg-info">You have indicated that you would like to submit an araz to Aqa maula TUS for: <br>
1. A specific course <br>
2. A specific course and place <br>
3. A specific course, place and institute</p>
            </div>

            <div class="course_no">
              <p class="bg-info">“You have indicated that you would like to submit an araz to Aqa Maula TUS listing multiple courses/institutes and locations.”</p><br>
              <p class="course_no bg-info institute">Complete this form to submit your <?php echo $_smarty_tpl->tpl_vars['newArray']->value;?>
 option</p>
            </div>
            <?php }?>
            <?php if ((count($_smarty_tpl->tpl_vars['institute_data']->value) >= 1)) {?>
          		<br><p class="bg-info">Complete this form to submit your <?php echo $_smarty_tpl->tpl_vars['newArray']->value;?>
 option</p>
          	<?php }?>
          	<br><br>

          <?php if ($_smarty_tpl->tpl_vars['is_institute_selected']->value != '2') {?><div class="col-md-12 shift"></div><?php }?>
            
           <?php if (($_smarty_tpl->tpl_vars['group_id']->value == '5')) {?>
          
            <div class="col-md-6 col-sm-12 shift institute">
              <lable class="white required-star"><b>Stream:</b></lable>
              <div class="shift10">
                <select name="course_name" id="course-sel" class="form-control required lable-mrgn" autocomplete="off" required>
                  <option value="">Stream</option>
                  <?php
$_from = $_smarty_tpl->tpl_vars['stream_array']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_crs_0_saved_item = isset($_smarty_tpl->tpl_vars['crs']) ? $_smarty_tpl->tpl_vars['crs'] : false;
$_smarty_tpl->tpl_vars['crs'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['crs']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['crs']->value) {
$_smarty_tpl->tpl_vars['crs']->_loop = true;
$__foreach_crs_0_saved_local_item = $_smarty_tpl->tpl_vars['crs'];
?>
                    <option value="<?php echo $_smarty_tpl->tpl_vars['crs']->value;?>
"><?php echo $_smarty_tpl->tpl_vars['crs']->value;?>
</option>
                  <?php
$_smarty_tpl->tpl_vars['crs'] = $__foreach_crs_0_saved_local_item;
}
if ($__foreach_crs_0_saved_item) {
$_smarty_tpl->tpl_vars['crs'] = $__foreach_crs_0_saved_item;
}
?>
                </select>
              </div>
            </div>
          
            <div class="col-md-6 col-sm-12 shift institute">
              <lable class="white required-star"><b>Standard:</b></lable>
              <div  class="shift10 lable-mrgn">
                <select name="course_duration" class="required" autocomplete="off" required>
                  <option value="">Standard</option>
                  <option value="11">11</option>
                  <option value="12">12</option>
                </select>
              </div>
            </div>
          
            <?php } else { ?>
          
            <div class="col-md-6 col-sm-12 shift institute">
              <lable class="white required-star"><b>Course:</b></lable>
              <div class="shift10 lable-mrgn">
                <input type="text" name="course_name" id="search_course" class="search_course" placeholder="Enter Course" data-toggle="tooltip" title="Please fill up the course with the correct spelling." data-placement="top"/>
                <div id="result_course"></div>
              </div>
            </div>
          
            <div class="col-md-6 col-sm-12 institute mb20">
              <lable class="white required-star"><b>Course Duration:</b></lable>
              <div  class="shift10 lable-mrgn">
                <select name="course_duration" class="required" autocomplete="off" required>
                  <option value="">Course Duration</option>
                  <option value="3 months">3 months</option>
                  <option value="6 months">6 months</option>
                  <option value="9 months">9 months</option>
                  <option value="1 year">1 year</option>
                  <option value="1 year 3 months">1 year 3 months</option>
                  <option value="1 year 6 months">1 year 6 months</option>
                  <option value="1 year 9 months">1 year 9 months</option>
                  <option value="2 years">2 years</option>
                  <option value="2 year 3 months">2 year 3 months</option>
                  <option value="2 year 6 months">2 year 6 months</option>
                  <option value="2 year 9 months">2 year 9 months</option>
                  <option value="3 years">3 years</option>
                  <option value="3 year 3 months">3 year 3 months</option>
                  <option value="3 year 6 months">3 year 6 months</option>
                  <option value="3 year 9 months">3 year 9 months</option>
                  <option value="4 years">4 years</option>
                  <option value="4 year 3 months">4 year 3 months</option>
                  <option value="4 year 6 months">4 year 6 months</option>
                  <option value="4 year 9 months">4 year 9 months</option>
                  <option value="5 years">5 years</option>
                </select>
              </div>
            </div>
          <?php }?>
          
          <div class="clearfix"></div> <!-- do not delete -->
          <span class="divider01"></span>

          <div class="clearfix"></div> <!-- do not delete -->
          <div class="institute">
            <h3>Institute's Details</h3>
          </div>

          <div class="col-md-6 col-sm-12 shift institute">
            <lable class="white"><b>Country:</b></lable>
            <div class="shift10 lable-mrgn">
              <select name="institute_country" onChange="showcity(this.value)" autocomplete="off">
                <option value="">Country</option>
                <?php if (($_smarty_tpl->tpl_vars['countries']->value != '')) {?>
                  <?php
$_from = $_smarty_tpl->tpl_vars['countries']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_cntry_1_saved_item = isset($_smarty_tpl->tpl_vars['cntry']) ? $_smarty_tpl->tpl_vars['cntry'] : false;
$_smarty_tpl->tpl_vars['cntry'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['cntry']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['cntry']->value) {
$_smarty_tpl->tpl_vars['cntry']->_loop = true;
$__foreach_cntry_1_saved_local_item = $_smarty_tpl->tpl_vars['cntry'];
?>
                    <option value="<?php echo $_smarty_tpl->tpl_vars['cntry']->value['name'];?>
" <?php if (($_smarty_tpl->tpl_vars['user']->value->get_country() == $_smarty_tpl->tpl_vars['cntry']->value['name'])) {?> selected <?php }?>><?php echo $_smarty_tpl->tpl_vars['cntry']->value['name'];?>
</option>
                  <?php
$_smarty_tpl->tpl_vars['cntry'] = $__foreach_cntry_1_saved_local_item;
}
if ($__foreach_cntry_1_saved_item) {
$_smarty_tpl->tpl_vars['cntry'] = $__foreach_cntry_1_saved_item;
}
?>
                <?php }?>
              </select>
            </div>
          </div>
          <div class="col-md-6 col-sm-12 shift institute">
            <lable class="white"><b>City:</b></lable>
            <div  class="shift10 lable-mrgn" id="your_city">
              <select name="madrasah_city" autocomplete="off">
                <option value="">City - Where institute is located</option>
                  <?php if ($_smarty_tpl->tpl_vars['ary_cities']->value) {?>
                    <?php
$_from = $_smarty_tpl->tpl_vars['ary_cities']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_city_2_saved_item = isset($_smarty_tpl->tpl_vars['city']) ? $_smarty_tpl->tpl_vars['city'] : false;
$_smarty_tpl->tpl_vars['city'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['city']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['city']->value) {
$_smarty_tpl->tpl_vars['city']->_loop = true;
$__foreach_city_2_saved_local_item = $_smarty_tpl->tpl_vars['city'];
?>
                      <option value="<?php echo $_smarty_tpl->tpl_vars['city']->value['city'];?>
" <?php if (($_smarty_tpl->tpl_vars['city']->value['city'] == $_smarty_tpl->tpl_vars['user']->value->get_city())) {?> selected <?php }?>><?php echo $_smarty_tpl->tpl_vars['city']->value['city'];?>
</option>
                    <?php
$_smarty_tpl->tpl_vars['city'] = $__foreach_city_2_saved_local_item;
}
if ($__foreach_city_2_saved_item) {
$_smarty_tpl->tpl_vars['city'] = $__foreach_city_2_saved_item;
}
?>
                  <?php }?>
              </select>
            </div>
          </div>

          <div class="col-md-6 col-sm-12 shift institute">
            <lable class="white"><b>Institute Name:</b></lable>
            <div class="shift10 lable-mrgn">
              <input type="text" name="institute_name" id="search_inst" class="search_institute" placeholder="Full name of the institute" data-toggle="tooltip" title="Please fill up the name with the correct spelling." data-placement="top"/>
              <div id="result"></div>
            </div>
          </div>

          <div class="col-md-6 col-sm-12 shift institute">
            <lable class="white required-star"><b>Accommodation:</b></lable>
            <div class="shift10 lable-mrgn">
              <select name="accomodation" class="required" autocomplete="off" required>
                <option value="">Accommodation</option>
                <option value="Own House">Own House</option>
                <option value="Relatives House">Relative's House</option>
                <option value="Hostel">Hostel</option>
                <option value="Individual Rental">Individual Rental</option>
                <option value="Sharing Rental with Mumin">Sharing Rental with Mumin</option>
                <option value="Sharing Rental with Non Mumin">Sharing Rental with Non Mumin</option>
                <option value="Not Decided">Not Decided</option>
                <option value="Distance Learning">Distance Learning</option>
              </select>
            </div>
          </div>
          
          <div class="col-md-6 col-sm-12 shift institute">
            <lable class="white required-star"><b>Course Start Month:</b></lable>
            <div  class="shift10 lable-mrgn">
              <select name="course_start_month" class="required" autocomplete="off" required>
                <option value="">Course Start Month</option>
                <?php
$_from = $_smarty_tpl->tpl_vars['course_month_array']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_str_mnth_3_saved_item = isset($_smarty_tpl->tpl_vars['str_mnth']) ? $_smarty_tpl->tpl_vars['str_mnth'] : false;
$_smarty_tpl->tpl_vars['str_mnth'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['str_mnth']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['str_mnth']->value) {
$_smarty_tpl->tpl_vars['str_mnth']->_loop = true;
$__foreach_str_mnth_3_saved_local_item = $_smarty_tpl->tpl_vars['str_mnth'];
?>
                  <option value="<?php echo $_smarty_tpl->tpl_vars['str_mnth']->value;?>
" ><?php echo $_smarty_tpl->tpl_vars['str_mnth']->value;?>
</option>
                <?php
$_smarty_tpl->tpl_vars['str_mnth'] = $__foreach_str_mnth_3_saved_local_item;
}
if ($__foreach_str_mnth_3_saved_item) {
$_smarty_tpl->tpl_vars['str_mnth'] = $__foreach_str_mnth_3_saved_item;
}
?>
              </select>
            </div>
          </div>
          <div class="col-md-6 col-sm-12 shift institute">
            <lable class="white required-star"><b>Course Start Year:</b></lable>
            <div  class="shift10 lable-mrgn">
              <select name="course_start_year" class="required" autocomplete="off" required>
                <option value="">Course Start Year</option>
                <?php $_smarty_tpl->tpl_vars['cur_year'] = new Smarty_Variable(date('Y'), null);
$_smarty_tpl->ext->_updateScope->updateScope($_smarty_tpl, 'cur_year', 0);?>
                <?php $_smarty_tpl->tpl_vars['rec_year'] = new Smarty_Variable($_smarty_tpl->tpl_vars['cur_year']->value-4, null);
$_smarty_tpl->ext->_updateScope->updateScope($_smarty_tpl, 'rec_year', 0);?>
                <?php $_smarty_tpl->tpl_vars['nxt_year'] = new Smarty_Variable($_smarty_tpl->tpl_vars['cur_year']->value+4, null);
$_smarty_tpl->ext->_updateScope->updateScope($_smarty_tpl, 'nxt_year', 0);?>
                <?php
$_smarty_tpl->tpl_vars['i'] = new Smarty_Variable;$_smarty_tpl->tpl_vars['i']->step = 1;$_smarty_tpl->tpl_vars['i']->total = (int) ceil(($_smarty_tpl->tpl_vars['i']->step > 0 ? $_smarty_tpl->tpl_vars['nxt_year']->value+1 - ($_smarty_tpl->tpl_vars['rec_year']->value) : $_smarty_tpl->tpl_vars['rec_year']->value-($_smarty_tpl->tpl_vars['nxt_year']->value)+1)/abs($_smarty_tpl->tpl_vars['i']->step));
if ($_smarty_tpl->tpl_vars['i']->total > 0) {
for ($_smarty_tpl->tpl_vars['i']->value = $_smarty_tpl->tpl_vars['rec_year']->value, $_smarty_tpl->tpl_vars['i']->iteration = 1;$_smarty_tpl->tpl_vars['i']->iteration <= $_smarty_tpl->tpl_vars['i']->total;$_smarty_tpl->tpl_vars['i']->value += $_smarty_tpl->tpl_vars['i']->step, $_smarty_tpl->tpl_vars['i']->iteration++) {
$_smarty_tpl->tpl_vars['i']->first = $_smarty_tpl->tpl_vars['i']->iteration == 1;$_smarty_tpl->tpl_vars['i']->last = $_smarty_tpl->tpl_vars['i']->iteration == $_smarty_tpl->tpl_vars['i']->total;?>
                  <option value="<?php echo $_smarty_tpl->tpl_vars['i']->value;?>
" ><?php echo $_smarty_tpl->tpl_vars['i']->value;?>
</option>
                <?php }
}
?>

              </select>
            </div>
          </div>
          <div class="clearfix"></div> <!-- do not delete -->
           <div class="col-xs-12 col-md-9">
             <span class="page-steps" style="color: #fff">Step : 3 out of 5</span>
           </div>
           <div class="col-xs-12 col-md-3 pull-right">
          <?php if ($_smarty_tpl->tpl_vars['show_next']->value) {?>
            <input type="submit" name="submit" class="submit1 institute btn btn-block" value="add <?php if (($_smarty_tpl->tpl_vars['is_institute_selected']->value != '2')) {?> / proceed <?php }?>" onclick="return ray.ajax()"/>
            <div id="load" style="display: none;"><i class="fa fa-spinner fa-spin fa-3x"></i></div>
           </div>
  		  <?php }?>
			<div class="clearfix"></div>
      			<p class="mb20 institute">Note: Fields marked with <span class="red-star">*</span> are mandatory</p>
			<div class="clearfix"></div>
        </form>
        <?php }?>
		<div class="clearfix"></div>
      <form method="post" action="<?php echo SERVER_PATH;?>
marhala-<?php echo $_smarty_tpl->tpl_vars['group_id']->value;?>
/<?php echo get_marhala_slug($_smarty_tpl->tpl_vars['group_id']->value);?>
/select-institute/">

		<?php if ($_smarty_tpl->tpl_vars['institute_data']->value) {?>
          <div class="block top-mgn20">
            <h4>Following is the list of records you are doing araz for</h4>
          </div>
          <div class="row">
            <div class="col-md-12">
              <div class="table-responsive">
                <table class="table1 table table-responsive">
                  <tr class="first-child">
                    <td class="first-child">Degree / Course</td>
                    <td>Institute Name</td>
                    <td>City</td>
                    <td>Accommodation</td>
                    <td><?php if (($_smarty_tpl->tpl_vars['group_id']->value == '5')) {?> Standard <?php } else { ?> Duration <?php }?></td>
                    <td>Course Start Date</td>
                    <td>Delete</td>
                  </tr>
                  	<?php
$_from = $_smarty_tpl->tpl_vars['institute_data']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_inst_4_saved_item = isset($_smarty_tpl->tpl_vars['inst']) ? $_smarty_tpl->tpl_vars['inst'] : false;
$_smarty_tpl->tpl_vars['inst'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['inst']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['inst']->value) {
$_smarty_tpl->tpl_vars['inst']->_loop = true;
$__foreach_inst_4_saved_local_item = $_smarty_tpl->tpl_vars['inst'];
?>
                    <tr>
                      <td><?php echo $_smarty_tpl->tpl_vars['inst']->value['course_name'];?>
</td>
                      <td><?php echo $_smarty_tpl->tpl_vars['inst']->value['inst_name'];?>
</td>
                      <td><?php echo $_smarty_tpl->tpl_vars['inst']->value['inst_city'];?>
</td>
                      <td><?php echo $_smarty_tpl->tpl_vars['inst']->value['accommodation'];?>
</td>
                      <td><?php echo $_smarty_tpl->tpl_vars['inst']->value['duration'];?>
</td>
                      <td><?php echo $_smarty_tpl->tpl_vars['inst']->value['course_started'];?>
</td>
                      <td style="padding:4px 0 0 0;"><a href="<?php echo SERVER_PATH;?>
marhala-<?php echo $_smarty_tpl->tpl_vars['group_id']->value;?>
/<?php echo get_marhala_slug($_smarty_tpl->tpl_vars['group_id']->value);?>
/select-institute/remove/<?php echo $_smarty_tpl->tpl_vars['inst']->value['id'];?>
" style="color: #FFF;"><i class="fa fa-remove"></i></a></td>
                    </tr>
    				<?php
$_smarty_tpl->tpl_vars['inst'] = $__foreach_inst_4_saved_local_item;
}
if ($__foreach_inst_4_saved_item) {
$_smarty_tpl->tpl_vars['inst'] = $__foreach_inst_4_saved_item;
}
?>
                </table>
              </div>
            </div>
          </div>
          <div class="row">
            <input type="submit" name="proceed_with_araz" class="skip proceed col-md-3 col-xs-12" value="Proceed with araz" onclick="return ray.ajax('proceed')"/>
            <div id="load1" style="display: none;"><i class="fa fa-spinner fa-spin fa-3x"></i></div>
            <input type="submit" name="save_araz" class="skip col-md-3 col-xs-12" value="Save Araz" style="margin-right: 5px;" />
          </div>
          <div class="row">
            <a class="pull-left col-md-3 col-xs-12 start-again" style="padding: 10px;" onclick="start_again();">Start Over Again</a>
            <span class="page-steps">Step : 3 out of 5</span>
          </div>
          <br>
        <?php }?>
      </form> 
      <div class="clearfix"></div> <!-- do not delete -->
    </div>
  </div>

  <?php echo '<script'; ?>
 type="text/javascript">
    $(document).ready(function(){
      $(".example").popover();
      $('[data-toggle="tooltip"]').tooltip({
        placement: 'top'
      });
    });
    
    var ray = {
      ajax: function(st)
      {
        if(st == 'proceed'){
          this.show('load1');
          $('.proceed').hide();
        }else {
          this.show('load');
          $('.submit1').hide();
        }
      },
      show: function(el)
      {
        this.getID(el).style.display = '';
      },
      getID: function(el)
      {
        return document.getElementById(el);
      }
    }
  
    $(document).ready(function () {
      $(".search_institute").keyup(function ()
      {
        var search_inst = $(this).val();
        if (search_inst == '') {
          $("#result").fadeOut();
        } else {
          var dataString = search_inst;
          if (search_inst != '')
          {
            $.ajax({
              type: "POST",
              url: "ajax_search_institute/" + dataString,
              cache: false,
              success: function (html)
              {
                $("#result").html(html).show();
              }
            });
          }
          return false;
        }
      });

      $('#search_inst').click(function () {
        if ($(this).val() == '') {
          $("#result").fadeOut();
        } else {
          $("#result").fadeIn();
        }
      });

      $('#search_inst').focusout(function () {
        $("#result").fadeOut();
      });
      
      $(".search_course").keyup(function ()
      {
        var search_course = $(this).val();
        if (search_course == '') {
          $("#result_course").fadeOut();
          $('#search_course').attr('class', 'search_course required');
          alert('Please enter course');
          return false;
        } else {
          if (search_course != '')
          {
            $.ajax({
              type: "POST",
              url: "ajax_search_course/" + search_course,
              cache: false,
              success: function (html)
              {
                $("#result_course").html(html).show();
              }
            });
          }
          return false;
        }
      });

      $('#search_course').click(function () {
        if ($(this).val() == '') {
          $("#result_course").fadeOut();
        } else {
          $("#result_course").fadeIn();
        }
      });

      $('#search_course').focusout(function () {
        $("#result_course").fadeOut();
      });

    });

    function get_value_institute(str) {
      $('#search_inst').val(str);
      $("#result").fadeOut();
      $('#search_course').attr('class', 'search_course success');
    }
    
    function get_value_course(str) {
      $('#search_course').val(str);
      $("#result_course").fadeOut();
    }
    
    function start_again() {
      var ask = window.confirm("Are you sure you want to start again?");
      if (ask) {
        document.location.href = "<?php echo SERVER_PATH;?>
start_over_again.php";
      }
    }

    function validate()
    {
      var course_name = document.inst_form.course_name.value;
      var institute_name = document.inst_form.institute_name.value;
      var place = document.inst_form.madrasah_city.value;
      var error = 'Following error(s) are occurred\n\n';
      var validate = true;

      if (course_name == '')
      {
        <?php if (($_smarty_tpl->tpl_vars['group_id']->value == '5')) {?>
          error += 'Please Select Stream\n';
        <?php } else { ?>
          error += 'Please Select Course\n';
        <?php }?>
        
        if (institute_name == '')
        {
          error += 'Please Enter Institute Name\n';
          if (place == '')
          {
            error += 'Please Select City\n';
            validate = false;
          }
        }
      }

      if (validate == false)
      {
        alert(error);
        return validate;
      }
    }

    function showcity(str)
    {
      if (str == "")
      {
        document.getElementById("your_city").innerHTML = "";
        document.getElementById("mname").innerHTML = "";
        return;
      }

      if (window.XMLHttpRequest)
      {
        xmlhttp = new XMLHttpRequest();
      }
      else
      {
        xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
      }



      xmlhttp.onreadystatechange = function ()
      {
        if (xmlhttp.readyState == 4 && xmlhttp.status == 200)
        {
          document.getElementById("your_city").innerHTML = xmlhttp.responseText;
        }
      }
      xmlhttp.open("GET", "get_city_by_country/" + str, true);
      xmlhttp.send();
    }
    
    function get_courses(str)
    {
      if (str == "")
      {
        document.getElementById("p_course").innerHTML = "";
        return;
      }

      if (window.XMLHttpRequest)
      {
        xmlhttp = new XMLHttpRequest();
      }
      else
      {
        xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
      }



      xmlhttp.onreadystatechange = function ()
      {
        if (xmlhttp.readyState == 4 && xmlhttp.status == 200)
        {
          document.getElementById("p_course").innerHTML = xmlhttp.responseText;
        }
      }
      xmlhttp.open("GET", "get_courses_by_degree/" + str, true);
      xmlhttp.send();
    }
    

    $('input:radio[name="is_institute_selected"]').click(function (e) {
      if ($("input[name='is_institute_selected']:checked").val() == '1') {
        $('.course_yes').show(600);
        $('.course_no').hide();
		<?php if ((count($_smarty_tpl->tpl_vars['institute_data']->value) >= 1)) {?>
          $('.institute').hide();
		<?php } else { ?>
          $('.institute').show(600);
		<?php }?>
      } else {
        $('.course_no').show(600);
        $('.course_yes').hide();
        $('.institute').show();
      }
    });

    if ($('#course_decided').is(":checked")) {
	  <?php if ((count($_smarty_tpl->tpl_vars['institute_data']->value) >= 1)) {?>
        $('.institute').hide();
      <?php } else { ?>
        $('.institute').show(600);
        $('.course_yes').show(600);
        $('.course_no').hide();
	  <?php }?>
    } else if ($('#course_not_decided').is(":checked")) {
      $('.course_no').show();
      $('.course_yes').hide();
      $('.institute').show();
    } else {
      $('.institute').hide();
		<?php if (($_smarty_tpl->tpl_vars['is_institute_selected']->value == '1')) {?>
			$('.course_yes').show();
			$('.course_no').hide();
		<?php } elseif (($_smarty_tpl->tpl_vars['is_institute_selected']->value == '2')) {?>
			$('.course_no').show();
			$('.course_yes').hide();
			$('.institute').show();
		<?php } else { ?>
			$('.course_yes').hide();
			$('.course_no').hide();
		<?php }?>
    }

  <?php echo '</script'; ?>
>

  <style>
    .skip{
      display:block;
      max-width:300px;
      -webkit-transition: 0.4s;
      -moz-transition: 0.4s;
      -o-transition: 0.4s;
      transition: 0.4s;
      border: none;
      background-color: #033054;
      height: auto;
      line-height:20px;
      margin:20px auto 0;
      padding:10px;
      outline: none;
      color:#fff;
      text-transform:uppercase;
      text-align:center;
      font-weight:bold;
      font-size:12px;
      float:right;
      text-decoration: underline none;
    }

    .skip:hover{
      color:#FFF;
    }

    #result
    {
      position:relative;
      width:auto;
      padding:10px;
      display:none;
      margin-top:-1px;
      border-top:0px;
      overflow:hidden;
      border:1px #CCC solid;
      background-color: white;
      height: auto;
      max-height: 350px;
      overflow: scroll;
    }
    
    #result_course
    {
      position:relative;
      width:auto;
      padding:10px;
      display:none;
      margin-top:-1px;
      border-top:0px;
      overflow:hidden;
      border:1px #CCC solid;
      background-color: white;
      height: auto;
      max-height: 350px;
      overflow: scroll;
    }
    .show
    {
      padding:5px; 
      border-bottom:1px #999 dashed;
      font-size:12px; 
      height:30px;
      overflow: hidden;
    }
    .show:hover
    {
      background:#eee;
      color:#000;
      cursor:pointer;
    }
    .lable-mrgn{
      margin-top:5px !important;
    }
    .red-star {
      color: red;
    }
    
    #load {
      color: #FFF !important;
      margin: 20px auto 0;
      float: right;
      text-align: center;
    }
    
    #load1 {
      color: #000 !important;
      margin: 20px auto 0;
      float: right;
      text-align: center;
    }
  </style>

</div><?php }
}
