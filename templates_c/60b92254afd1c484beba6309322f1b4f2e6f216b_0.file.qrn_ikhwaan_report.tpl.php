<?php
/* Smarty version 3.1.29, created on 2016-03-16 11:56:57
  from "/var/www/html/smarty_tlb2/templates/qrn_ikhwaan_report.tpl" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.29',
  'unifunc' => 'content_56e8fcb1f35e11_14174589',
  'file_dependency' => 
  array (
    '60b92254afd1c484beba6309322f1b4f2e6f216b' => 
    array (
      0 => '/var/www/html/smarty_tlb2/templates/qrn_ikhwaan_report.tpl',
      1 => 1457697047,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:include/js_block.tpl' => 1,
    'file:include/message.tpl' => 1,
    'file:include/qrn_message.tpl' => 1,
    'file:include/qrn_links.tpl' => 1,
    'file:include/footer.tpl' => 1,
  ),
),false)) {
function content_56e8fcb1f35e11_14174589 ($_smarty_tpl) {
$_smarty_tpl->smarty->ext->_subtemplate->render($_smarty_tpl, "file:include/js_block.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

<link href="<?php echo SERVER_PATH;?>
assets/datatables/dataTables.bootstrap.css" rel="stylesheet" type="text/css" />
<?php echo '<script'; ?>
 src="<?php echo SERVER_PATH;?>
assets/datatables/jquery.dataTables.min.js" type="text/javascript"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 src="<?php echo SERVER_PATH;?>
assets/datatables/dataTables.bootstrap.min.js" type="text/javascript"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 type="text/javascript" src="<?php echo SERVER_PATH;?>
js/loader.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 type="text/javascript">
  google.charts.load("current", {
    packages: ['corechart']
  });
<?php echo '</script'; ?>
>

<div class="container white-bg">
  <div class="col-md-12 col-sm-12">
    <div class="page-title">
      <p style="margin-top:5px;">
        <a href="#" class="active">My Ikhwaan Report</a></p>
      <h1>My Ikhwaan Report</h1>
    </div>
  </div>
  <?php if ((isset($_smarty_tpl->tpl_vars['_SESSION']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_SUCCESS_MESSAGE']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_SUCCESS_MESSAGE']->value['index'] : null)]) || isset($_smarty_tpl->tpl_vars['_SESSION']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_ERROR_MESSAGE']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_ERROR_MESSAGE']->value['index'] : null)]))) {?>
    <div class="row">
      <?php $_smarty_tpl->smarty->ext->_subtemplate->render($_smarty_tpl, "file:include/message.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

    </div>
  <?php }?>
  <div class="clearfix"></div> <!-- do not delete -->
  <div class="col-md-12 col-sm-12">
    <form action="" method="post" name="tasmee_form">
      <?php $_smarty_tpl->smarty->ext->_subtemplate->render($_smarty_tpl, "file:include/qrn_message.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

      <div class="body-container white-bg">
        <div class="hifz-tasmee-details-wrapper">
          <div class="htd-section">
            <?php if ($_smarty_tpl->tpl_vars['select_mumin']->value) {?>
              <h2>You are currently linked with <span class="badge"><?php echo count($_smarty_tpl->tpl_vars['select_mumin']->value);?>
</span> Ikhwaan</h2>
            <?php }?>
            <h2>How many aayaat I do tasmee daily (<?php echo date('jS M Y');?>
)</h2>
            <div class="row">
              <div class="col-xs-12 col-md-12">
                <div class="col-xs-12 col-md-9">
                  <?php if ($_smarty_tpl->tpl_vars['select_mumin']->value) {?>
                    <div id="barchart"></div>
                  <?php } else { ?>
                    No Data Available.
                  <?php }?>
                </div>
              </div>
            </div>
            <div class="clearfix"><br></div>
            <h2>Set Time Schedule</h2>
            <div class="row">
              <div class="col-xs-12 col-md-3">
                <select name="sch_mumin_its" class="form-control">
                  <option value="">Select Mumin</option>
                  <?php if ($_smarty_tpl->tpl_vars['select_mumin']->value) {?>
                    <?php
$_from = $_smarty_tpl->tpl_vars['select_mumin']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_mumin_data_0_saved_item = isset($_smarty_tpl->tpl_vars['mumin_data']) ? $_smarty_tpl->tpl_vars['mumin_data'] : false;
$_smarty_tpl->tpl_vars['mumin_data'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['mumin_data']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['mumin_data']->value) {
$_smarty_tpl->tpl_vars['mumin_data']->_loop = true;
$__foreach_mumin_data_0_saved_local_item = $_smarty_tpl->tpl_vars['mumin_data'];
?>
                      <option value="<?php echo $_smarty_tpl->tpl_vars['mumin_data']->value['its_id'];?>
"><?php echo $_smarty_tpl->tpl_vars['mumin_data']->value['mumin_full_name'];?>
</option>
                    <?php
$_smarty_tpl->tpl_vars['mumin_data'] = $__foreach_mumin_data_0_saved_local_item;
}
if ($__foreach_mumin_data_0_saved_item) {
$_smarty_tpl->tpl_vars['mumin_data'] = $__foreach_mumin_data_0_saved_item;
}
?>
                  <?php }?>
                </select>
              </div>
              <div class="col-xs-12 col-md-3">
                <select name="sch_days[]" multiple class="form-control">
                  <option value="">Days Entry</option>
                  <option value="Everyday">Everyday</option>
                  <option value="Sunday">Sunday</option>
                  <option value="Monday">Monday</option>
                  <option value="Tuesday">Tuesday</option>
                  <option value="Wednesday">Wednesday</option>
                  <option value="Thursday">Thursday</option>
                  <option value="Friday">Friday</option>
                  <option value="Saturday">Saturday</option>
                  <option value="Excluding Sunday">Excluding Sunday</option>
                  <option value="Excluding Weekend">Excluding Weekend</option>
                </select>
              </div>
              <div class="col-xs-12 col-md-2">
                <select name="sch_hour" class="form-control">
                  <option value="">Hour Entry</option>
                  <?php
$_smarty_tpl->tpl_vars['i'] = new Smarty_Variable;$_smarty_tpl->tpl_vars['i']->step = 1;$_smarty_tpl->tpl_vars['i']->total = (int) ceil(($_smarty_tpl->tpl_vars['i']->step > 0 ? 24+1 - (1) : 1-(24)+1)/abs($_smarty_tpl->tpl_vars['i']->step));
if ($_smarty_tpl->tpl_vars['i']->total > 0) {
for ($_smarty_tpl->tpl_vars['i']->value = 1, $_smarty_tpl->tpl_vars['i']->iteration = 1;$_smarty_tpl->tpl_vars['i']->iteration <= $_smarty_tpl->tpl_vars['i']->total;$_smarty_tpl->tpl_vars['i']->value += $_smarty_tpl->tpl_vars['i']->step, $_smarty_tpl->tpl_vars['i']->iteration++) {
$_smarty_tpl->tpl_vars['i']->first = $_smarty_tpl->tpl_vars['i']->iteration == 1;$_smarty_tpl->tpl_vars['i']->last = $_smarty_tpl->tpl_vars['i']->iteration == $_smarty_tpl->tpl_vars['i']->total;?>
                  <option value="<?php echo $_smarty_tpl->tpl_vars['i']->value;?>
"><?php echo $_smarty_tpl->tpl_vars['i']->value;?>
</option>
                  <?php }
}
?>

                </select>
              </div>
              <div class="col-xs-12 col-md-2">
                <select name="sch_time" class="form-control">
                  <option value="">Time Entry</option>
                  <option value="00">00</option>
                  <option value="15">15</option>
                  <option value="30">30</option>
                  <option value="45">45</option>
                </select>
              </div>
              <div class="col-xs-12 col-md-1">
                <input type="submit" name="set_schedule" value="Set Schedule" class="btn btn-success">
              </div>
            </div>
            <div class="clearfix"><br></div>
            <div class="row">
              <div class="col-xs-12 col-md-12 table-responsive">
                <table class="table table-bordered table-hover tlb_schedule">
                  <thead>
                    <tr>
                      <th>Sr No</th>
                      <th>Mumin ITS</th>
                      <th>Days</th>
                      <th>Time</th>
                      <th class="text-center">Delete</th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php if ($_smarty_tpl->tpl_vars['schedule_records']->value) {?>
                      <?php $_smarty_tpl->tpl_vars["s"] = new Smarty_Variable(1, null);
$_smarty_tpl->ext->_updateScope->updateScope($_smarty_tpl, "s", 0);?>
                      <?php
$_from = $_smarty_tpl->tpl_vars['schedule_records']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_sr_1_saved_item = isset($_smarty_tpl->tpl_vars['sr']) ? $_smarty_tpl->tpl_vars['sr'] : false;
$_smarty_tpl->tpl_vars['sr'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['sr']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['sr']->value) {
$_smarty_tpl->tpl_vars['sr']->_loop = true;
$__foreach_sr_1_saved_local_item = $_smarty_tpl->tpl_vars['sr'];
?>
                        <tr>
                          <td><?php echo $_smarty_tpl->tpl_vars['s']->value++;?>
</td>
                          <td><?php echo $_smarty_tpl->tpl_vars['sr']->value['mumin_its'];?>
</td>
                          <td><?php echo $_smarty_tpl->tpl_vars['sr']->value['days'];?>
</td>
                          <td><?php echo $_smarty_tpl->tpl_vars['sr']->value['hour'];?>
:<?php echo $_smarty_tpl->tpl_vars['sr']->value['time'];?>
</td>
                          <td class="text-center"><a href="#" id="<?php echo $_smarty_tpl->tpl_vars['sr']->value['id'];?>
" class="delete_schedule"><i class="fa fa-remove"></i></a></td>
                        </tr>
                      <?php
$_smarty_tpl->tpl_vars['sr'] = $__foreach_sr_1_saved_local_item;
}
if ($__foreach_sr_1_saved_item) {
$_smarty_tpl->tpl_vars['sr'] = $__foreach_sr_1_saved_item;
}
?>
                    <?php }?>
                  </tbody>
                </table>
              </div>
            </div>
            <div class="clearfix"><br></div>
            <h2>Select Sadeeq for his/her Details &amp; Reports</h2>
            <div class="row">
              <div class="col-xs-12 col-md-3">
                <select name="s_mumin_its" id="s_mumin_its" class="form-control">
                  <option value="">Select Mumin</option>
                  <?php if ($_smarty_tpl->tpl_vars['select_mumin']->value) {?>
                    <?php
$_from = $_smarty_tpl->tpl_vars['select_mumin']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_mumin_data_2_saved_item = isset($_smarty_tpl->tpl_vars['mumin_data']) ? $_smarty_tpl->tpl_vars['mumin_data'] : false;
$_smarty_tpl->tpl_vars['mumin_data'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['mumin_data']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['mumin_data']->value) {
$_smarty_tpl->tpl_vars['mumin_data']->_loop = true;
$__foreach_mumin_data_2_saved_local_item = $_smarty_tpl->tpl_vars['mumin_data'];
?>
                      <option value="<?php echo $_smarty_tpl->tpl_vars['mumin_data']->value['its_id'];?>
"><?php echo $_smarty_tpl->tpl_vars['mumin_data']->value['mumin_full_name'];?>
</option>
                    <?php
$_smarty_tpl->tpl_vars['mumin_data'] = $__foreach_mumin_data_2_saved_local_item;
}
if ($__foreach_mumin_data_2_saved_item) {
$_smarty_tpl->tpl_vars['mumin_data'] = $__foreach_mumin_data_2_saved_item;
}
?>
                  <?php }?>
                </select>
              </div>
              <div class="col-xs-12 col-md-2">
                <a name="search" id="search" class="btn btn-success">Get Details</a>
              </div>
            </div>
            <div class="clearfix"><br></div>
            <div class="row" id="show_mumin_graph"></div>
          </div>
          <?php $_smarty_tpl->smarty->ext->_subtemplate->render($_smarty_tpl, "file:include/qrn_links.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

        </div>
      </div>
    </form>
    <form method="post" id="frm-del-schedule">
      <input type="hidden" name="delete_schedule" id="del-schedule" value="">
    </form>    
  </div>
</div>

<?php echo '<script'; ?>
 type="text/javascript">
  google.charts.setOnLoadCallback(drawChart);
  function drawChart() {
    var data = google.visualization.arrayToDataTable([
      ['ITS', 'Days Past', 'Total Record', 'Total Ayat']
      <?php if ($_smarty_tpl->tpl_vars['select_mumin']->value) {?>
        <?php echo ',';?>

        <?php
$_from = $_smarty_tpl->tpl_vars['select_mumin']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_data_3_saved_item = isset($_smarty_tpl->tpl_vars['data']) ? $_smarty_tpl->tpl_vars['data'] : false;
$_smarty_tpl->tpl_vars['data'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['data']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['data']->value) {
$_smarty_tpl->tpl_vars['data']->_loop = true;
$__foreach_data_3_saved_local_item = $_smarty_tpl->tpl_vars['data'];
?>
          <?php $_smarty_tpl->tpl_vars['result'] = new Smarty_Variable($_smarty_tpl->tpl_vars['qrn_tasmee']->value->get_tasmee_log_of_mumin($_smarty_tpl->tpl_vars['data']->value['its_id'],$_smarty_tpl->tpl_vars['_SESSION']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_USER_ITS']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_USER_ITS']->value['index'] : null)]), null);
$_smarty_tpl->ext->_updateScope->updateScope($_smarty_tpl, 'result', 0);?>
          <?php if ($_smarty_tpl->tpl_vars['result']->value) {?>
            <?php ob_start();
echo $_smarty_tpl->tpl_vars['result']->value['Days_Past'];
$_tmp1=ob_get_clean();
ob_start();
echo $_smarty_tpl->tpl_vars['result']->value['Total_Record'];
$_tmp2=ob_get_clean();
ob_start();
echo $_smarty_tpl->tpl_vars['result']->value['Total_Ayat'];
$_tmp3=ob_get_clean();
echo array(((string)$_smarty_tpl->tpl_vars['data']->value['mumin_full_name']),$_tmp1,$_tmp2,$_tmp3);?>

            <?php if ((count($_smarty_tpl->tpl_vars['select_mumin']->value)-1)) {?>
              <?php echo ',';?>

            <?php }?>
          <?php }?>
        <?php
$_smarty_tpl->tpl_vars['data'] = $__foreach_data_3_saved_local_item;
}
if ($__foreach_data_3_saved_item) {
$_smarty_tpl->tpl_vars['data'] = $__foreach_data_3_saved_item;
}
?>
      <?php }?>
    ]);

    var options = {
      bars: 'horizontal' // Required for Material Bar Charts.
    };

    var chart = new google.visualization.BarChart(document.getElementById('barchart'));

    chart.draw(data, options);
  }
  
  $(window).resize(function () {
    drawChart();
  });
  
  $(".sch_mumin_its").select2({
    placeholder: "Select Mumin",
    allowClear: true
  });
  
  $(".sch_days").select2({
    placeholder: "Days Entry",
    allowClear: true
  });

  $('#search').on("click", function (e) {
    e.preventDefault();
    var mumin_its = $('#s_mumin_its').val();

    $.ajax({
      type: "POST",
      url: "ajax.php",
      data: 'query=get_mumin_graph&mumin_its=' + mumin_its,
      cache: false,
      success: function (response) {
        $('.show_mumin_graph').show(600);

        if (response != '') {
          $('#show_mumin_graph').html(response);
        }
      }
    });
  });
  
  $('.tlb_schedule').DataTable({
    "paging": true,
    "lengthChange": false,
    "searching": true,
    "ordering": false,
    "info": true,
    "autoWidth": true,
    language: {
      searchPlaceholder: "Search"
    }
  });
  
  $(".delete_schedule").confirm({
    text: "Are you sure you want to delete time schedule?",
    title: "Confirmation required",
    confirm: function (button) {
      $('#del-schedule').val($(button).attr('id'));
      $('#frm-del-schedule').submit();
      alert('Are you sure you want to delete time schedule: ' + id);
    },
    cancel: function (button) {
      // nothing to do
    },
    confirmButton: "Yes",
    cancelButton: "No",
    post: true,
    confirmButtonClass: "btn-danger"
  });

<?php echo '</script'; ?>
>

<style>
  p {
    padding: 5px;
  }
</style>
<?php $_smarty_tpl->smarty->ext->_subtemplate->render($_smarty_tpl, "file:include/footer.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
}
}
