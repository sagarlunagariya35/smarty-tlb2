<?php
/* Smarty version 3.1.29, created on 2016-02-28 21:20:14
  from "/var/www/html/smarty_tlb2/templates/my_araz_institution_preview.tpl" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.29',
  'unifunc' => 'content_56d317369631d5_87621952',
  'file_dependency' => 
  array (
    '078326b01c986cf244babe89e68212645c253645' => 
    array (
      0 => '/var/www/html/smarty_tlb2/templates/my_araz_institution_preview.tpl',
      1 => 1456674610,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_56d317369631d5_87621952 ($_smarty_tpl) {
if (($_smarty_tpl->tpl_vars['marhala_id']->value != '1' && in_array($_smarty_tpl->tpl_vars['marhala_id']->value,$_smarty_tpl->tpl_vars['marhala_school_array']->value))) {?>
  <table class="table1 table table-responsive">
    <tr class="first-child">
      <td class="first-child">Hifz al Quran</td>
    </tr>
    <tr>
      <td><?php if (($_smarty_tpl->tpl_vars['araz_data']->value['hifz_sanad'] != '')) {?> <?php echo $_smarty_tpl->tpl_vars['araz_data']->value['hifz_sanad'];?>
 <?php } else { ?> NA <?php }?></td>
    </tr>
  </table>

  <table class="table1 table table-responsive">
    <tr class="first-child">
      <td class="first-child">Madrasah name</td>
      <td>Darajah</td>
    </tr>
    <tr>
      <td><?php if (($_smarty_tpl->tpl_vars['araz_data']->value['madrasah_name'] != '')) {?> <?php echo $_smarty_tpl->tpl_vars['araz_data']->value['madrasah_name'];?>
 <?php } else { ?> NA <?php }?></td>
      <td><?php if (($_smarty_tpl->tpl_vars['araz_data']->value['madrasah_darajah'] != '')) {?> <?php echo $_smarty_tpl->tpl_vars['araz_data']->value['madrasah_darajah'];?>
 <?php } else { ?> NA <?php }?></td>
    </tr>
  </table>

<?php } else { ?>
  <table class="table1 table table-responsive">
    <tr class="first-child">
      <td class="first-child">Sabaq</td>
    </tr>
    <tr>
      <td><?php echo $_smarty_tpl->tpl_vars['araz_data']->value['madrasah_darajah'];?>
</td>
    </tr>
  </table>
<?php }?>

<?php if ((in_array($_smarty_tpl->tpl_vars['marhala_id']->value,$_smarty_tpl->tpl_vars['marhala_school_array']->value))) {?>

  <table class="table1 table table-responsive">
    <tr class="first-child">
      <td class="first-child">Institution name</td>
      <td>Place</td>
      <td>Standard</td>
    </tr>
    <tr>
      <td><?php echo $_smarty_tpl->tpl_vars['araz_data']->value['school_name'];?>
</td>
      <td><?php echo $_smarty_tpl->tpl_vars['araz_data']->value['school_city'];?>
</td>
      <td><?php echo $_smarty_tpl->tpl_vars['araz_data']->value['school_standard'];?>
</td>
    </tr>
  </table>

<?php } else { ?>
  <?php if (($_smarty_tpl->tpl_vars['institute_data']->value)) {?>
    <table class="table1 table table-responsive">
      <tr class="first-child">
        <td class="first-child">Institute name</td>
        <td>Place</td>
        <td>Degree / Course</td>
        <td>Accomodation</td>
        <td>Duration</td>
        <td>Course Start Date</td>
      </tr>
      <?php
$_from = $_smarty_tpl->tpl_vars['institute_data']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_inst_0_saved_item = isset($_smarty_tpl->tpl_vars['inst']) ? $_smarty_tpl->tpl_vars['inst'] : false;
$_smarty_tpl->tpl_vars['inst'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['inst']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['inst']->value) {
$_smarty_tpl->tpl_vars['inst']->_loop = true;
$__foreach_inst_0_saved_local_item = $_smarty_tpl->tpl_vars['inst'];
?>
        <tr>
          <td><?php echo $_smarty_tpl->tpl_vars['inst']->value['institute_name'];?>
</td>
          <td><?php echo $_smarty_tpl->tpl_vars['inst']->value['institute_city'];?>
</td>
          <td><?php echo $_smarty_tpl->tpl_vars['inst']->value['course_name'];?>
</td>
          <td><?php echo $_smarty_tpl->tpl_vars['inst']->value['accomodation'];?>
</td>
          <td><?php echo $_smarty_tpl->tpl_vars['inst']->value['course_duration'];?>
</td>
          <td><?php echo $_smarty_tpl->tpl_vars['inst']->value['course_started'];?>
</td>
        </tr>
      <?php
$_smarty_tpl->tpl_vars['inst'] = $__foreach_inst_0_saved_local_item;
}
if ($__foreach_inst_0_saved_item) {
$_smarty_tpl->tpl_vars['inst'] = $__foreach_inst_0_saved_item;
}
?>
    </table>
  <?php }
}
}
}
