<?php
/* Smarty version 3.1.29, created on 2016-04-07 13:38:59
  from "/var/www/html/smarty_tlb2/templates/qasaid.tpl" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.29',
  'unifunc' => 'content_5706159b772510_63242853',
  'file_dependency' => 
  array (
    '070d151ab7fe2a794402fbe1046680394486f359' => 
    array (
      0 => '/var/www/html/smarty_tlb2/templates/qasaid.tpl',
      1 => 1459318165,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:include/js_block.tpl' => 1,
    'file:include/footer.tpl' => 1,
  ),
),false)) {
function content_5706159b772510_63242853 ($_smarty_tpl) {
?>
<div class="container white-bg">
  <div class="col-md-12 col-sm-12">
    <div class="page-title">
      <p style="margin-top:5px;"><a href="<?php echo $_smarty_tpl->tpl_vars['server_path']->value;?>
index.php">Home</a> / <a href="<?php echo $_smarty_tpl->tpl_vars['server_path']->value;?>
istifaadah/qasaid/">Qasaid</a> / <a href="#" class="active"><?php echo $_smarty_tpl->tpl_vars['this_qasida']->value['title'];?>
</a></p>
      <h1>Qasaid</h1>
    </div>
  </div>
  <div class="clearfix"></div> <!-- do not delete -->
  <div class="col-md-12 col-sm-12">
    <form class="forms1 white" name="raza_form" method="post">
      <div class="col-md-3 hidden-xs">
        <div class="profile-box-static">
          <h3 class="uppercase text-center">Qasaid</h3>
        </div>
        <div class="profile-box-static-bottom">
          <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
              <?php if ($_smarty_tpl->tpl_vars['qasaid_alams']->value) {?>
                <?php $_smarty_tpl->tpl_vars["i"] = new Smarty_Variable(1, null);
$_smarty_tpl->ext->_updateScope->updateScope($_smarty_tpl, "i", 0);?>
                 <?php
$_from = $_smarty_tpl->tpl_vars['qasaid_alams']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_qa_0_saved_item = isset($_smarty_tpl->tpl_vars['qa']) ? $_smarty_tpl->tpl_vars['qa'] : false;
$_smarty_tpl->tpl_vars['qa'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['qa']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['qa']->value) {
$_smarty_tpl->tpl_vars['qa']->_loop = true;
$__foreach_qa_0_saved_local_item = $_smarty_tpl->tpl_vars['qa'];
?>
                   <?php $_smarty_tpl->tpl_vars["i"] = new Smarty_Variable($_smarty_tpl->tpl_vars['i']->value+1, null);
$_smarty_tpl->ext->_updateScope->updateScope($_smarty_tpl, "i", 0);?>
                    <?php $_smarty_tpl->tpl_vars['list_qasaides'] = new Smarty_Variable(get_all_qasaid($_smarty_tpl->tpl_vars['qa']->value['alam']), null);
$_smarty_tpl->ext->_updateScope->updateScope($_smarty_tpl, 'list_qasaides', 0);?>
                      <div class="panel panel-default">
                        <div class="panel-heading" role="tab" id="headingOne">
                          <h4 class="panel-title">
                            <a role="button" data-toggle="collapse" data-parent="#accordion" href="#<?php echo $_smarty_tpl->tpl_vars['i']->value;?>
" aria-expanded="true" aria-controls="collapseOne">
                              <?php echo $_smarty_tpl->tpl_vars['qa']->value['alam'];?>

                            </a>
                          </h4>
                        </div>
                        <div id="<?php echo $_smarty_tpl->tpl_vars['i']->value;?>
" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
                          <div class="panel-body">
                            
                              <?php if ($_smarty_tpl->tpl_vars['list_qasaides']->value) {?>
                                <?php
$_from = $_smarty_tpl->tpl_vars['list_qasaides']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_q_1_saved_item = isset($_smarty_tpl->tpl_vars['q']) ? $_smarty_tpl->tpl_vars['q'] : false;
$_smarty_tpl->tpl_vars['q'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['q']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['q']->value) {
$_smarty_tpl->tpl_vars['q']->_loop = true;
$__foreach_q_1_saved_local_item = $_smarty_tpl->tpl_vars['q'];
?>
                                  <?php $_smarty_tpl->tpl_vars['qasaid_done'] = new Smarty_Variable(get_qasaid_is_done($_smarty_tpl->tpl_vars['q']->value['id']), null);
$_smarty_tpl->ext->_updateScope->updateScope($_smarty_tpl, 'qasaid_done', 0);?>
                                    <a href="<?php echo $_smarty_tpl->tpl_vars['server_path']->value;?>
istifaadah/qasaid/<?php echo $_smarty_tpl->tpl_vars['q']->value['id'];?>
/"><?php echo $_smarty_tpl->tpl_vars['q']->value['title'];?>
</a>
                                    <hr class="sleek">
                                <?php
$_smarty_tpl->tpl_vars['q'] = $__foreach_q_1_saved_local_item;
}
if ($__foreach_q_1_saved_item) {
$_smarty_tpl->tpl_vars['q'] = $__foreach_q_1_saved_item;
}
?>
                              <?php }?>
                          </div>
                        </div>
                      </div>
                <?php
$_smarty_tpl->tpl_vars['qa'] = $__foreach_qa_0_saved_local_item;
}
if ($__foreach_qa_0_saved_item) {
$_smarty_tpl->tpl_vars['qa'] = $__foreach_qa_0_saved_item;
}
?>
              <?php }?>              
            </div>
        </div>
      </div>
      <div class="col-md-6">
        <div class="col-md-3 col-xs-12 text-center">
          <input class="form-control" type="checkbox" name="alfazmana" checked>
          <div class="text-center" style="padding-bottom: 20px;color: #000">Alfaaz Maana</div>
        </div>
        <div class="col-md-3 col-xs-12 text-center">
          <input class="form-control" type="checkbox" name="lafzi" checked>
          <div class="text-center" style="padding-bottom: 20px;color: #000">Phrase Maana</div>
        </div>
        <div class="col-md-3 col-xs-12 text-center">
          <input class="form-control" type="checkbox" name="fehwa" checked>
          <div class="text-center" style="padding-bottom: 20px;color: #000">Fehwa</div>
        </div>
        <div class="col-md-3 col-xs-12 text-center">
          <input class="form-control" type="checkbox" name="taleeq" checked>
          <div class="text-center" style="padding-bottom: 20px;color: #000">Taleeq</div>
        </div>
        <div class="clearfix"></div>
        <?php echo $_smarty_tpl->tpl_vars['this_qasida']->value['text'];?>

        <div class="clearfix"><br></div>
        <div style='width:100%;text-align:center'><br>
          <audio id='sound1' preload='auto' src='' controls style='width:100%'>
        </div>
        <div class="clearfix"><br></div>
      </div>
      <div class="col-md-3">
        <div class="profile-box-static">
          <h3 class="uppercase text-center">Progress</h3>
        </div>
        <div class="profile-box-static-bottom">
          <div class="text-center">
            <div style="margin:0px 30px;" class="text-center">
              <label style="padding-bottom: 5px;" class="sparkline">&nbsp;</label>
            </div>
            <div class="col-xs-12 text center prgupdate"></div>
          </div>
          <div class="clearfix"></div>
          <hr>
          <div class="text-center">
            <div class="noui-header">Qasida Recited %</div>
            <div class="clearfix"></div>
            <div id="slider1" class="sliders"></div>
            <span class="nouivalspan" id="slider-range-value1">
              <?php if ($_smarty_tpl->tpl_vars['get_slider1_val']->value) {?>
                <?php echo number_format($_smarty_tpl->tpl_vars['get_slider1_val']->value,2);?>
%
              <?php } else { ?>
                0.00%
              <?php }?>
            </span>
          </div>
          <div class="clearfix"></div>
          <hr>
          <div class="text-center">
            <div class="noui-header">Mana Understood %</div>
            <div class="clearfix"></div>
            <div id="slider2" class="sliders"></div>
            <span class="nouivalspan" id="slider-range-value2">
              <?php if ($_smarty_tpl->tpl_vars['get_slider2_val']->value) {?>
                <?php echo number_format($_smarty_tpl->tpl_vars['get_slider2_val']->value,2);?>
%
              <?php } else { ?>
                0.00%
              <?php }?>
            </span>
          </div>
          <div class="clearfix"></div>
          <hr>
          <div class="text-center">
            <div class="noui-header">Qasida Hifz %</div>
            <div class="clearfix"></div>
            <div id="slider3" class="sliders"></div>
            <span class="nouivalspan" id="slider-range-value3">
              <?php if ($_smarty_tpl->tpl_vars['get_slider3_val']->value) {?>
                <?php echo number_format($_smarty_tpl->tpl_vars['get_slider3_val']->value,2);?>
%
              <?php } else { ?>
                0.00%
              <?php }?>
            </span>
          </div>
        </div>
      </div>
      <input type="hidden" value="<?php echo $_smarty_tpl->tpl_vars['server_path']->value;?>
" name="path" id="path">
    </form>
  </div>
</div>
      
<?php $_smarty_tpl->smarty->ext->_subtemplate->render($_smarty_tpl, "file:include/js_block.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>


<link href="<?php echo $_smarty_tpl->tpl_vars['server_path']->value;?>
css/bootstrap-switch.min.css" rel="stylesheet" type="text/css"/>
<link href="<?php echo $_smarty_tpl->tpl_vars['server_path']->value;?>
noUiSlider.8.0.2/nouislider.min.css" rel="stylesheet">
<?php echo '<script'; ?>
 src="<?php echo $_smarty_tpl->tpl_vars['server_path']->value;?>
noUiSlider.8.0.2/nouislider.min.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 src="<?php echo $_smarty_tpl->tpl_vars['server_path']->value;?>
js/bootstrap-switch.min.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 src="<?php echo $_smarty_tpl->tpl_vars['server_path']->value;?>
js/jquery.sparkline.min.js" type="text/javascript"><?php echo '</script'; ?>
>

<?php echo '<script'; ?>
>
  var slider1Val = <?php echo $_smarty_tpl->tpl_vars['get_slider1_val']->value;?>
, slider2Val = <?php echo $_smarty_tpl->tpl_vars['get_slider2_val']->value;?>
, slider3Val = <?php echo $_smarty_tpl->tpl_vars['get_slider3_val']->value;?>
;
  update_pie();

  function update_pie() {

    var AvgVal = (parseInt(slider1Val) + parseInt(slider2Val) + parseInt(slider3Val)) / 3;
    var RemVal = 100 - AvgVal;

    $('.sparkline').sparkline([AvgVal, RemVal], {
      type: "pie",
      width: "70px",
      sliceColors: ['#549BC7', '#09386c'],
      height: "70px",
      tooltipFormat: ''
    });
    $('.prgupdate').html("<strong>Completed:</strong> " + AvgVal.toFixed(2) + "%");
  }

  $(function (argument) {
    $('[type="checkbox"]').bootstrapSwitch();
  })
  $('input[name="alfazmana"]').on('switchChange.bootstrapSwitch', function (event, state) {
    if (state) {
      $('.mana').show();
    } else {
      $('.mana').hide();
    }
  });
  $('input[name="lafzi"]').on('switchChange.bootstrapSwitch', function (event, state) {
    if (state) {
      $('.lafzi').show();
    } else {
      $('.lafzi').hide();
    }
  });
  $('input[name="fehwa"]').on('switchChange.bootstrapSwitch', function (event, state) {
    if (state) {
      $('.fehwa').show();
    } else {
      $('.fehwa').hide();
    }
  });
  $('input[name="taleeq"]').on('switchChange.bootstrapSwitch', function (event, state) {
    if (state) {
      $('.taleeq').show();
    } else {
      $('.taleeq').hide();
    }
  });

  var slider1 = document.getElementById('slider1');
  var slider2 = document.getElementById('slider2');
  var slider3 = document.getElementById('slider3');

  var rangeSliderValueElement1 = document.getElementById('slider-range-value1');
  var rangeSliderValueElement2 = document.getElementById('slider-range-value2');
  var rangeSliderValueElement3 = document.getElementById('slider-range-value3');

  noUiSlider.create(slider1, {
    start: [<?php if ($_smarty_tpl->tpl_vars['get_slider1_val']->value) {?>
              <?php echo $_smarty_tpl->tpl_vars['get_slider1_val']->value;?>

            <?php } else { ?>
              <?php echo 0;?>

            <?php }?>
           ],
    range: {
      'min': 0,
      '25%': 25,
      '50%': 50,
      '75%': 75,
      'max': 100
    },
    connect: 'lower',
    snap: true
  });

  slider1.noUiSlider.on('change', function (values, handle) {
    rangeSliderValueElement1.innerHTML = values[handle] + '%';
    slider1Val = values[handle];
    update_pie();
    var str = $('#path').val();
    var dataString = 'qasaid_id=<?php echo $_smarty_tpl->tpl_vars['qasaid_id']->value;?>
&query=make_entry_of_qasaid&slider_id=1&data=' + values[handle];

    $.ajax({
      type: "POST",
      url: str + "ajax.php",
      data: dataString,
      cache: false,
      success: function () {
      }
    });
  });

  noUiSlider.create(slider2, {
    start: [<?php if ($_smarty_tpl->tpl_vars['get_slider2_val']->value) {?>
              <?php echo $_smarty_tpl->tpl_vars['get_slider2_val']->value;?>

            <?php } else { ?>
              <?php echo 0;?>

            <?php }?>],
    range: {
      'min': 0,
      '25%': 25,
      '50%': 50,
      '75%': 75,
      'max': 100
    },
    connect: 'lower',
    snap: true
  });
  slider2.noUiSlider.on('change', function (values, handle) {
    rangeSliderValueElement2.innerHTML = values[handle] + '%';
    slider2Val = values[handle];
    update_pie();
    var str = $('#path').val();
    var dataString = 'qasaid_id=<?php echo $_smarty_tpl->tpl_vars['qasaid_id']->value;?>
&query=make_entry_of_qasaid&slider_id=2&data=' + values[handle];

    $.ajax({
      type: "POST",
      url: str + "ajax.php",
      data: dataString,
      cache: false,
      success: function () {
      }
    });
  });

  noUiSlider.create(slider3, {
    start: [<?php if ($_smarty_tpl->tpl_vars['get_slider3_val']->value) {?>
              <?php echo $_smarty_tpl->tpl_vars['get_slider3_val']->value;?>

            <?php } else { ?>
              <?php echo 0;?>

            <?php }?>],
    range: {
      'min': 0,
      '25%': 25,
      '50%': 50,
      '75%': 75,
      'max': 100
    },
    connect: 'lower',
    snap: true
  });
  slider3.noUiSlider.on('change', function (values, handle) {
    rangeSliderValueElement3.innerHTML = values[handle] + '%';
    slider3Val = values[handle];
    update_pie();
    var str = $('#path').val();
    var dataString = 'qasaid_id=<?php echo $_smarty_tpl->tpl_vars['qasaid_id']->value;?>
&query=make_entry_of_qasaid&slider_id=3&data=' + values[handle];

    $.ajax({
      type: "POST",
      url: str + "ajax.php",
      data: dataString,
      cache: false,
      success: function () {
      }
    });
  });
  
  function pa(file) {
    audio = document.getElementById('sound1');
    audio.src = '<?php echo $_smarty_tpl->tpl_vars['server_path']->value;?>
/istifaadah/qasaid/<?php echo $_smarty_tpl->tpl_vars['this_qasida']->value['folder'];?>
/' + file + '.mp3';
    audio.load();
    audio.play();
  }
<?php echo '</script'; ?>
>

<style type='text/css'>
  Table{
    border-collapse:collapse;
    width: 100%;
    margin-top:5px;
  }
  Table , td, th{
    border:1px solid black;
    text-align: center;
    line-height: 1.6;
  }
  .bayt td {
    line-height: 70px;
  }
  .bayt{    
    font-family:  'lsdqasaid'; 
    font-size: 24px; 
    font-weight: bold;
    color: #009;
    text-shadow: 2px 2px 10px grey;
  }
  .mana{
    font-family: 'lsarabic';
    font-size: 14pt;
    color: #666;
    background-color: lightGoldenrodYellow;
  }
  .lafzi{
    font-family: 'lsarabic';
    font-size: 16pt;
    color: #111;
    background-color: paleGoldenrod;
  }
  .fehwa{
    font-family: 'lsarabic';
    font-size: 18pt;
    color: #000;
    background-color: lightYellow;
  }
  #container{
    width: 530px;
  }
  #mawad{
    float:right;
    width: 410px;
  }
  #comment-container{
    float: left;
    width:110px;
    clear: left;
  }
  .taleeq{
    width: 110px;
    font-family: 'lsarabic';
    font-size: 18pt;
    color: black;
    text-align: right;
    background-color: gold;
    border-radius: 5px;
    margin-top: 5px;
    margin-left: 2px;
    padding: 5px;
    line-height: 1.6;
  }
  .btn {
    display: block;
    width: auto;
    -webkit-transition: 0.4s;
    -moz-transition: 0.4s;
    -o-transition: 0.4s;
    transition: 0.4s;
    border: none;
    background-color: #033054;
    height: auto;
    padding: 10px;
    outline: none;
    color: #fff;
    text-transform: uppercase;
    text-align: center;
    float: right;
    line-height: 20px;
    border: 2px solid #fff;
  }
  .btn:active{
    color: #fff;
  }
  .btn:hover{
    background-color: #fff;
    color: #033054;
    border: 2px solid #033054;
  }
  @media screen and (max-width: 1200px) {
    #container {
      width: 100%;
    }
    #mawad {
      width: 100%;
    } 
    #comment-container {
      width: 100%;
    }
    .taleeq {
      width: 99%;
    }
  }
  .aud{
    cursor: pointer;
    width:15px;
  }
  .erab {
    font-family: 'lsdqasaid';
    font-weight: bold;
  }
  td{
    cursor: pointer;
  }
  .nouitooltip {
    display: block;
    position: absolute;
    border: 1px solid #D9D9D9;
    font: 400 12px/12px Arial;
    border-radius: 3px;
    background: #fff;
    top: -43px;
    padding: 5px;
    left: -9px;
    text-align: center;
    width: 50px;
    color: #000;
  }
  .nouitooltip strong {
    display: block;
    padding: 2px;
  }
</style>
<?php $_smarty_tpl->smarty->ext->_subtemplate->render($_smarty_tpl, "file:include/footer.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
}
}
