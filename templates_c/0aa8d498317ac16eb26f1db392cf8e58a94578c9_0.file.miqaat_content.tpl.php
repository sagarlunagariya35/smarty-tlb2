<?php
/* Smarty version 3.1.29, created on 2016-02-23 14:55:20
  from "/var/www/html/smarty_tlb2/templates/miqaat_content.tpl" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.29',
  'unifunc' => 'content_56cc2580455b31_34696191',
  'file_dependency' => 
  array (
    '0aa8d498317ac16eb26f1db392cf8e58a94578c9' => 
    array (
      0 => '/var/www/html/smarty_tlb2/templates/miqaat_content.tpl',
      1 => 1456219518,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_56cc2580455b31_34696191 ($_smarty_tpl) {
?>
<style>
  .orange{
    color: #cf4914;
  }
</style>
<div class="container white-bg">
  <div class="col-md-12 col-sm-12">

    <div class="page-title">
      <p style="margin-top:5px;">
        <a href="#" class="active">Istibsaar <?php echo $_smarty_tpl->tpl_vars['miqaat_istibsaar']->value['year'];?>
 H</a></p>
      <h1>Istibsaar <?php echo $_smarty_tpl->tpl_vars['miqaat_istibsaar']->value['year'];?>
 H<span class="alfatemi-text">الاستبصار</span></h1>
    </div>
  </div>
  <div class="clearfix"></div> <!-- do not delete -->
  <form class="forms1 white" name="raza_form" method="post">
    <div class="col-md-3 col-sm-12 hidden-xs">
      <div class="profile-box-static">
        <h3 class="uppercase text-center">Mawaqeet <?php echo $_smarty_tpl->tpl_vars['miqaat_istibsaar']->value['year'];?>
 H</h3>
      </div>
      <div class="profile-box-static-bottom">
        <?php
$_from = $_smarty_tpl->tpl_vars['mawaqeet']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_mqt_0_saved_item = isset($_smarty_tpl->tpl_vars['mqt']) ? $_smarty_tpl->tpl_vars['mqt'] : false;
$_smarty_tpl->tpl_vars['mqt'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['mqt']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['mqt']->value) {
$_smarty_tpl->tpl_vars['mqt']->_loop = true;
$__foreach_mqt_0_saved_local_item = $_smarty_tpl->tpl_vars['mqt'];
?>
          <a href="<?php echo SERVER_PATH;?>
istibsaar/<?php echo $_smarty_tpl->tpl_vars['mqt']->value['id'];?>
/<?php echo $_smarty_tpl->tpl_vars['mqt']->value['slug'];?>
/<?php echo $_smarty_tpl->tpl_vars['mqt']->value['year'];?>
/">» <?php echo $_smarty_tpl->tpl_vars['mqt']->value['miqaat_title'];?>
<hr></a>
        
        <?php
$_smarty_tpl->tpl_vars['mqt'] = $__foreach_mqt_0_saved_local_item;
}
if ($__foreach_mqt_0_saved_item) {
$_smarty_tpl->tpl_vars['mqt'] = $__foreach_mqt_0_saved_item;
}
?>

        <?php $_smarty_tpl->tpl_vars["make_pre_active"] = new Smarty_Variable('', null);
$_smarty_tpl->ext->_updateScope->updateScope($_smarty_tpl, "make_pre_active", 0);?>
        <?php $_smarty_tpl->tpl_vars["make_in_active"] = new Smarty_Variable('', null);
$_smarty_tpl->ext->_updateScope->updateScope($_smarty_tpl, "make_in_active", 0);?>
        <?php $_smarty_tpl->tpl_vars["make_post_active"] = new Smarty_Variable('', null);
$_smarty_tpl->ext->_updateScope->updateScope($_smarty_tpl, "make_post_active", 0);?>

        <?php if (($_smarty_tpl->tpl_vars['miqaat_istibsaar']->value['show_post_event'] == '1')) {?>
          <?php $_smarty_tpl->tpl_vars['make_post_active'] = new Smarty_Variable(' active', null);
$_smarty_tpl->ext->_updateScope->updateScope($_smarty_tpl, 'make_post_active', 0);?>
        <?php } elseif (($_smarty_tpl->tpl_vars['miqaat_istibsaar']->value['show_in_event'] == '1')) {?>
          <?php $_smarty_tpl->tpl_vars['make_in_active'] = new Smarty_Variable(' active', null);
$_smarty_tpl->ext->_updateScope->updateScope($_smarty_tpl, 'make_in_active', 0);?>
        <?php } elseif (($_smarty_tpl->tpl_vars['miqaat_istibsaar']->value['show_pre_event'] == '1')) {?>
          <?php $_smarty_tpl->tpl_vars['make_pre_active'] = new Smarty_Variable(' active', null);
$_smarty_tpl->ext->_updateScope->updateScope($_smarty_tpl, 'make_pre_active', 0);?>
        <?php }?>
      </div>
    </div>

    <div class="col-md-8 col-sm-12">
      <div class="static-show-button large-fonts col-xs-12 col-sm-12 pull-left"><?php echo '<?php ';?>echo $miqaat_istibsaar['miqaat_title']; <?php echo '?>';?></div>
      <div class="clearfix"></div>

      <div>
        <ul class="nav nav-tabs" role="tablist">

          <?php echo '<?php ';?>if ($miqaat_istibsaar['show_pre_event'] == '1') { <?php echo '?>';?><li role="presentation" class="<?php echo '<?php ';?>echo $make_pre_active; <?php echo '?>';?>"><a href="#pre" aria-controls="pre" role="tab" data-toggle="tab">Ohbat</a></li><?php echo '<?php ';?>} <?php echo '?>';?>

          <?php echo '<?php ';?>if ($miqaat_istibsaar['show_in_event'] == '1') { <?php echo '?>';?><li role="presentation" class="<?php echo '<?php ';?>echo $make_in_active; <?php echo '?>';?>"><a href="#in" aria-controls="in" role="tab" data-toggle="tab">Talaqqi</a></li><?php echo '<?php ';?>} <?php echo '?>';?>

          <?php echo '<?php ';?>if ($miqaat_istibsaar['show_post_event'] == '1') { <?php echo '?>';?><li role="presentation" class="<?php echo '<?php ';?>echo $make_post_active; <?php echo '?>';?>"><a href="#post" aria-controls="post" role="tab" data-toggle="tab">Tizkaar</a></li><?php echo '<?php ';?>} <?php echo '?>';?>

        </ul>

        <?php echo '<?php
        ';?>$video_pre = $video_in = $video_post = FALSE;
        $audio_pre = $audio_in = $audio_post = FALSE;
        $audio_pre2 = $audio_in2 = $audio_post2 = FALSE;
        $stat_pre = $stat_in = $stat_post = FALSE;
        $ohbat_pre = $ohbat_in = $ohbat_post = FALSE;
        $sentences_pre = $sentences_in = $sentences_post = FALSE;
        $quiz_pre = $quiz_in = $quiz_post = FALSE;
        $course_pre = $course_in = $course_post = FALSE;
        $youtube_pre = $youtube_in = $youtube_post = FALSE;

        if ($miqaat_videos) {
          foreach ($miqaat_videos as $video) {

            $video_table = '<a href="' . SERVER_PATH . 'upload/miqaat_upload/video/' . $video['video_url'] . '"><img src="' . SERVER_PATH . 'upload/miqaat_upload/video/images/' . $video['video_thumb'] . '"  title=""></a>';

            if ($video['event_time'] == 'pre') {
              $video_pre .= $video_table;
            } else if ($video['event_time'] == 'in') {
              $video_in .= $video_table;
            } else if ($video['event_time'] == 'post') {
              $video_post .= $video_table;
            }
          }
        }
        
        if ($miqaat_youtube){
          foreach ($miqaat_youtube as $you_vid){
            if ($you_vid['event_time'] == 'pre') {
              $youtube_pre .= $you_vid['video_url'];
            } elseif($you_vid['event_time'] == 'in'){
              $youtube_in .= $you_vid['video_url'];
            } elseif ($you_vid['event_time'] == 'post') {
              $youtube_post .= $you_vid['video_url'];
            }
          }
        }

        if ($miqaat_audios) {
          foreach ($miqaat_audios as $audio) {

            if ($audio['event_time'] == 'pre') {
              $player_id = 'jquery_jplayer_1';
              $css_id = 'jp_container_1';
            } else if ($audio['event_time'] == 'in') {
              $player_id = 'jquery_jplayer_2';
              $css_id = 'jp_container_2';
            } else if ($audio['event_time'] == 'post') {
              $player_id = 'jquery_jplayer_3';
              $css_id = 'jp_container_3';
            }

            $audio_table = '<div id="' . $player_id . '" class="jp-jplayer"></div>
          <div id="' . $css_id . '" class="jp-audio" role="application" aria-label="media player">
            <div class="jp-type-playlist">
              <div class="jp-gui jp-interface">
                <div class="jp-controls">
                  <button class="jp-previous" role="button" tabindex="0">previous</button>
                  <button class="jp-play" role="button" tabindex="0">play</button>
                  <button class="jp-next" role="button" tabindex="0">next</button>
                  <button class="jp-stop" role="button" tabindex="0">stop</button>
                </div>
                <div class="jp-progress">
                  <div class="jp-seek-bar">
                    <div class="jp-play-bar"></div>
                  </div>
                </div>
                <div class="jp-volume-controls">
                  <button class="jp-mute" role="button" tabindex="0">mute</button>
                  <button class="jp-volume-max" role="button" tabindex="0">max volume</button>
                  <div class="jp-volume-bar">
                    <div class="jp-volume-bar-value"></div>
                  </div>
                </div>
                <div class="jp-time-holder">
                  <div class="jp-current-time" role="timer" aria-label="time">&nbsp;</div>
                  <div class="jp-duration" role="timer" aria-label="duration">&nbsp;</div>
                </div>
                <div class="jp-toggles">
                  <button class="jp-repeat" role="button" tabindex="0">repeat</button>
                  <button class="jp-shuffle" role="button" tabindex="0">shuffle</button>
                </div>
              </div>
              <div class="jp-playlist">
                <ul>
                  <li>&nbsp;</li>
                </ul>
              </div>
              <div class="jp-no-solution">
                <span>Update Required</span>
                To play the media you will need to either update your browser to a recent version or update your <a href="http://get.adobe.com/flashplayer/" target="_blank">Flash plugin</a>.
              </div>
            </div>
          </div>';

            $audio_table2 = '{
                        title:"' . $audio['audio_url'] . '",
                        mp3: "' . SERVER_PATH . 'upload/miqaat_upload/audio/' . $audio['audio_url'] . '",
                      }';
            if (count($miqaat_audios) - 1) {
              $audio_table2 .= ',';
            }

            if ($audio['event_time'] == 'pre') {
              $audio_pre = $audio_table;
              $audio_pre2 .= $audio_table2;
            } else if ($audio['event_time'] == 'in') {
              $audio_in = $audio_table;
              $audio_in2 .= $audio_table2;
            } else if ($audio['event_time'] == 'post') {
              $audio_post = $audio_table;
              $audio_post2 .= $audio_table2;
            }
          }
        }

        if ($miqaat_stationaries) {
          foreach ($miqaat_stationaries as $stat_files) {

            //$filearray = explode('.', $stat_files['stationary_url']);
            //$ext = explode('-', $filearray[0]);
            //$file_name = $ext[1];
            $file_name = $stat_files['name'];
            
            $stat_category = explode(',', $stat_files['category']);
            foreach ($stat_category as $st_cat){
              $stat_categories[] = $st_cat;
            }
            
            $stat_cls = implode(' ', $stat_category);
            $logo_image = ($stat_files['logo_image']) ? $stat_files['logo_image'] : 'images/waaz_pdf.png';
            $stat_table = '<div class="col-md-4 all '. $stat_cls .'">
              <a href="http://docs.google.com/gview?embedded=true&url='.SERVER_PATH.'upload/miqaat_upload/stationary/' . $stat_files['stationary_url'] . '" target="_blank">
                  <div class="istibsaar-boxes text-center">
                    <img src="' . SERVER_PATH . $logo_image . '" class="img-responsive" style="max-height: 100px; margin: 0 auto" >
                    <div class="istibsaar-boxes-bottom">
                      <p class="text-center">' . $file_name . '</p>
                    </div>
                  </div>
              </a>
                </div>';

            if ($stat_files['event_time'] == 'pre') {
              $stat_pre .= $stat_table;
            } else if ($stat_files['event_time'] == 'in') {
              $stat_in .= $stat_table;
            } else if ($stat_files['event_time'] == 'post') {
              $stat_post .= $stat_table;
            }
          }
          $stationary_categories = array_unique($stat_categories);
        }

        if ($miqaat_ohbats) {
          foreach ($miqaat_ohbats as $mo) {

            $ohbat_table = '<div class="col-md-4">
                <a href="' . SERVER_PATH . 'daily_sentences_ar.php?mid=' . $miqat_id . '">
              <div class="istibsaar-boxes text-center">
                <img src="' . SERVER_PATH . 'images/beer.png" class="img-responsive" style="max-height: 100px; margin: 0 auto" >
                <div class="istibsaar-boxes-bottom">
                  <p class="text-center">Ohbat Kalemaat Nooraniyah</p>
                </div>
              </div>
                </a>
            </div>';

            if ($mo['event_time'] == 'pre') {
              $ohbat_pre = $ohbat_table;
            } else if ($mo['event_time'] == 'in') {
              $ohbat_in = $ohbat_table;
            } else if ($mo['event_time'] == 'post') {
              $ohbat_post = $ohbat_table;
            }
          }
        }

        if ($miqat_id == 5) {
          $tasavvuraat_in = '<div class="col-md-4">
                <a href="' . SERVER_PATH . 'tasavvuraat_araz.php">
              <div class="istibsaar-boxes text-center">
                <i class="fa fa-microphone  fa-5x dark-blue"></i>
                <div class="istibsaar-boxes-bottom">
                  <p class="text-center">Tasawwuraat Araz</p>
                </div>
              </div>
                </a>
            </div>';
          
        }


        if ($miqaat_sentences) {
          foreach ($miqaat_sentences as $ms) {

            $sentences_table = '<div class="col-md-4">
                <a href="' . SERVER_PATH . 'istibsaar/ashara-mubarakah/">
              <div class="istibsaar-boxes text-center">
                <img src="' . SERVER_PATH . 'images/beer.png" class="img-responsive" style="max-height: 100px; margin: 0 auto" >
                <div class="istibsaar-boxes-bottom">
                  <p class="text-center">Sentences</p>
                </div>
              </div>
                </a>
            </div>';

            if ($ms['event_time'] == 'pre') {
              $sentences_pre = $sentences_table;
            } else if ($ms['event_time'] == 'in') {
              $sentences_in = $sentences_table;
            } else if ($ms['event_time'] == 'post') {
              $sentences_post = $sentences_table;
            }
          }
        }

        if ($miqaat_quizes) {
          foreach ($miqaat_quizes as $mq) {

            $quiz_table = '<div class="col-md-4">
                <a href="quiz_group_list.php">
              <div class="istibsaar-boxes text-center">
                <i class="fa fa-list-alt   fa-5x dark-blue"></i>
                <div class="istibsaar-boxes-bottom">
                  <p class="text-center">Take Quiz</p>
                </div>
              </div>
                </a>
            </div>';

            if ($mq['event_time'] == 'pre') {
              $quiz_pre = $quiz_table;
            } else if ($mq['event_time'] == 'in') {
              $quiz_in = $quiz_table;
            } else if ($mq['event_time'] == 'post') {
              $quiz_post = $quiz_table;
            }
          }
        }

        if ($miqaat_courses) {
          $course_table = '';
          $first_child = TRUE;
          $tmp_quiz = new Quiz();
          foreach ($miqaat_courses as $mc) {
            $course_id = $mc['course_id'];
            $user_its = $_SESSION['user_its'];
            
            $course_questions = $tmp_quiz->get_courses_question_by_course_id($course_id);
            $user_answers = $tmp_quiz->get_user_answers_for_course($course_id, $user_its);
            
            if($user_answers) {
              $correct_ans = 0;
              foreach($user_answers as $ua){
                ($ua['correct'] == 1) ? $correct_ans++ : '';
              }
              $user_quiz_interaction = '<p class="text-center" style="padding-left: 25px;"><span class="pull-left">' . $mc['course_title'] . '</span><span class="pull-right">' . $correct_ans . ' / ' . count($course_questions) . '</span><div class="clearfix"></div></p>';
            } else {
              $user_quiz_interaction = '<p class="text-center">' . $mc['course_title'] . '</p>';
            }
            if($mc['active'] == 1) {
              $color = 'dark-blue';
              $img = SERVER_PATH . 'assets/img/icon_tizkaar_act.png';
            
            if ($first_child) {
              $color = 'orange';
              $first_child = FALSE;
              //$img = SERVER_PATH . 'assets/img/icon_tizkaar.png';
            }
            
            $course_table = '<div class="col-md-4">
                <a href="take_courses.php?course_id=' . $mc['course_id'] . '">
              <div class="istibsaar-boxes text-center">
                <img class="img-responsive" style="max-height: 100px; margin: 0 auto" src="' . $img . '">
                <div class="istibsaar-boxes-bottom">' . $user_quiz_interaction . '<p class="text-center black_font">' . $mc['description'] . '</p>
                </div>
              </div>
                </a>
            </div>';

            if ($mc['event_time'] == 'pre') {
              $course_pre .= $course_table;
            } else if ($mc['event_time'] == 'in') {
              $course_in .= $course_table;
            } else if ($mc['event_time'] == 'post') {
              $course_post .= $course_table;
            }
            }
          }
        }

        $no_videos = '<p style="margin-left:10px;">No Videos</p>';
        $no_audios = '<p style="margin-left:10px;">No Audios</p>';
        $no_stationaries = '<p style="margin-left:10px;">No Stationaries</p>';
        <?php echo '?>';?>

        <div class="tab-content">
          <?php echo '<?php ';?>if ($miqaat_istibsaar['show_pre_event'] == '1') { <?php echo '?>';?>
            <div role="tabpanel" class="tab-pane <?php echo '<?php ';?>echo $make_pre_active; <?php echo '?>';?>" id="pre">
              <?php echo '<?php
              ';?>if ($stat_pre) {
                echo '<div class="static-show-button2 large-fonts col-xs-12 col-sm-12 pull-left">Downloads</div><div class="clearfix"></div>';
                $sp = 1;
                echo '<div class="black_font pull-right">';
                echo '<a href="#" class="black_font" id="all"> All </a> | ';
                foreach ($stationary_categories as $stat_cat){
                  echo '<a href="#" class="black_font" id="'.$stat_cat.'"> '. $stat_cat .' </a>';
                  if (count($stationary_categories) > $sp++) {
                    echo ' | ';
                  }
                }
                echo '</div>';
                echo '<div class="clearfix"><br></div>';
                echo $stat_pre;
                echo '<div class="clearfix"></div>';
              }

              if ($video_pre) {

                echo '<div class="static-show-button2 large-fonts col-xs-12 col-sm-12 pull-left">Videos</div><div class="clearfix"></div>
                  <div style="display:none;" class="html5gallery" data-skin="horizontal" data-width="290" data-height="200" data-resizemode="fill">' . $video_pre . '</div>';
                echo '<div class="clearfix"></div>';
              }

              if ($youtube_pre) {

                echo '<div class="static-show-button2 large-fonts col-xs-12 col-sm-12 pull-left">Videos</div><div class="clearfix"></div>';
                echo $youtube_pre;
                echo '<div class="clearfix"></div>';
              }

              if ($ohbat_pre || $sentences_pre || $quiz_pre || $course_pre) {
                echo '<div class="static-show-button2 large-fonts col-xs-12 col-sm-12 pull-left">Interaction</div><div class="clearfix"></div>';

                if ($ohbat_pre) {
                  echo $ohbat_pre;
                }
                if ($sentences_pre) {
                  echo $sentences_pre;
                }
                if ($quiz_pre) {
                  echo $quiz_pre;
                }
                if ($course_pre) {
                  echo $course_pre;
                }
                echo '<div class="clearfix"></div>';
              }

              if ($audio_pre) {

                echo '<div class="static-show-button2 large-fonts col-xs-12 col-sm-12 pull-left">Audios</div><div class="clearfix"></div>' . $audio_pre;
                echo '<div class="clearfix"></div>';
              }
              <?php echo '?>';?>
            </div>
          <?php echo '<?php ';?>} <?php echo '?>';?>

          <?php echo '<?php
          ';?>if ($miqaat_istibsaar['show_in_event'] == '1') {
            <?php echo '?>';?>  
            <div role="tabpanel" class="tab-pane<?php echo '<?php ';?>echo $make_in_active; <?php echo '?>';?>" id="in">
              <?php echo '<?php
              ';?>if ($stat_in) {
                echo '<div class="static-show-button2 large-fonts col-xs-12 col-sm-12 pull-left">Downloads</div><div class="clearfix"></div>';
                $sn = 1;
                echo '<div class="black_font pull-right">';
                echo '<a href="#" class="black_font" id="all"> All </a> | ';
                foreach ($stationary_categories as $stat_cat){
                  echo '<a href="#" class="black_font" id="'.$stat_cat.'"> '. $stat_cat .' </a>';
                  if (count($stationary_categories) > $sn++) {
                    echo ' | ';
                  }
                }
                echo '</div>';
                echo '<div class="clearfix"><br></div>';
                echo $stat_in;
                echo '<div class="clearfix"></div>';
              }


              if ($video_in) {
                echo '<div class="static-show-button2 large-fonts col-xs-12 col-sm-12 pull-left">Videos</div><div class="clearfix"></div>
                <div style="display:none;" class="html5gallery" data-skin="horizontal" data-width="290" data-height="200" data-resizemode="fill">' . $video_in . '</div>';
                echo '<div class="clearfix"></div>';
              }

              if ($youtube_in) {
                echo '<div class="static-show-button2 large-fonts col-xs-12 col-sm-12 pull-left">Videos</div><div class="clearfix"></div>';
                echo $youtube_in;
                echo '<div class="clearfix"></div>';
              }

              if ($ohbat_in || $sentences_in || $quiz_in || $course_in) {
                echo '<div class="static-show-button2 large-fonts col-xs-12 col-sm-12 pull-left">Interaction</div><div class="clearfix"></div>';

                if ($ohbat_in) {
                  echo $ohbat_in;
                }
                if ($sentences_in) {
                  echo $sentences_in;
                }
                if ($quiz_in) {
                  echo $quiz_in;
                }
                /*if ($tasavvuraat_in) {
                  echo $tasavvuraat_in;
                }*/
                if ($course_in) {
                  echo $course_in;
                }
                echo '<div class="clearfix"></div>';
              }

              if ($audio_in) {

                echo '<div class="static-show-button2 large-fonts col-xs-12 col-sm-12 pull-left">Audios</div><div class="clearfix"></div>' . $audio_in;
                echo '<div class="clearfix"></div>';
              }
              <?php echo '?>';?>

            </div>
          <?php echo '<?php ';?>} <?php echo '?>';?>

          <?php echo '<?php
          ';?>if ($miqaat_istibsaar['show_post_event'] == '1') {
            <?php echo '?>';?>
            <div role="tabpanel" class="tab-pane<?php echo '<?php ';?>echo $make_post_active; <?php echo '?>';?>" id="post">
              <?php echo '<?php
              ';?>if ($stat_post) {
                echo '<div class="static-show-button2 large-fonts col-xs-12 col-sm-12 pull-left">Downloads</div><div class="clearfix"></div>';
                $st = 1;
                echo '<div class="black_font pull-right">';
                echo '<a href="#" class="black_font" id="all"> All </a> | ';
                foreach ($stationary_categories as $stat_cat){
                  echo '<a href="#" class="black_font" id="'.$stat_cat.'"> '. $stat_cat .' </a>';
                  if (count($stationary_categories) > $st++) {
                    echo ' | ';
                  }
                }
                echo '</div>';
                echo '<div class="clearfix"><br></div>';
                echo $stat_post;
                echo '<div class="clearfix"></div>';
              }

              if ($video_post) {
                echo '<div class="static-show-button2 large-fonts col-xs-12 col-sm-12 pull-left">Videos</div><div class="clearfix"></div>
                <div style="display:none;" class="html5gallery" data-skin="horizontal" data-width="290" data-height="200" data-resizemode="fill">' . $video_post . '</div>';
                echo '<div class="clearfix"></div>';
              }

              if ($youtube_post) {
                echo '<div class="static-show-button2 large-fonts col-xs-12 col-sm-12 pull-left">Videos</div><div class="clearfix"></div>';
                echo $youtube_post;
                echo '<div class="clearfix"></div>';
              }

              if ($ohbat_post || $sentences_post || $quiz_post || $course_post) {
                echo '<div class="static-show-button2 large-fonts col-xs-12 col-sm-12 pull-left">Interaction</div><div class="clearfix"></div>';

                if ($ohbat_post) {
                  echo $ohbat_post;
                }
                if ($sentences_post) {
                  echo $sentences_post;
                }
                if ($quiz_post) {
                  echo $quiz_post;
                }
                if ($course_post) {
                  echo $course_post;
                }
                echo '<div class="clearfix"></div>';
              }

              if ($audio_post) {

                echo '<div class="static-show-button2 large-fonts col-xs-12 col-sm-12 pull-left">Audios</div><div class="clearfix"></div>' . $audio_post;
                echo '<div class="clearfix"></div>';
              }
              <?php echo '?>';?>
            </div>
          <?php echo '<?php ';?>} <?php echo '?>';?>
        </div>
      </div>
    </div>
    <div class="clearfix"></div>
  </form>
</div>
<?php echo '<script'; ?>
>
  $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
    e.target // newly activated tab
    e.relatedTarget // previous active tab
  })
<?php echo '</script'; ?>
>

<?php echo '<script'; ?>
 type="text/javascript" src="<?php echo '<?php ';?>echo SERVER_PATH; <?php echo '?>';?>html5gallery/html5gallery.js"><?php echo '</script'; ?>
>

<link href="<?php echo '<?php ';?>echo SERVER_PATH; <?php echo '?>';?>dist/skin/blue.monday/css/jplayer.blue.monday.min.css" rel="stylesheet" type="text/css" />
<?php echo '<script'; ?>
 type="text/javascript" src="<?php echo '<?php ';?>echo SERVER_PATH; <?php echo '?>';?>dist/jplayer/jquery.jplayer.min.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 type="text/javascript" src="<?php echo '<?php ';?>echo SERVER_PATH; <?php echo '?>';?>dist/add-on/jplayer.playlist.min.js"><?php echo '</script'; ?>
>

<?php echo '<script'; ?>
 type="text/javascript">
//<![CDATA[
  $(document).ready(function () {
<?php echo '<?php ';?>if ($audio_pre2) { <?php echo '?>';?>
      new jPlayerPlaylist({
        jPlayer: "#jquery_jplayer_1",
        cssSelectorAncestor: "#jp_container_1"
      }, [
  <?php echo '<?php
  ';?>if ($audio_pre2) {
    echo $audio_pre2;
  }
  <?php echo '?>';?>
      ], {
        swfPath: "../../dist/jplayer",
        supplied: "oga, mp3",
        wmode: "window",
        useStateClassSkin: true,
        autoBlur: false,
        smoothPlayBar: true,
        keyEnabled: true
      });

<?php echo '<?php ';?>} if ($audio_in2) { <?php echo '?>';?>
      new jPlayerPlaylist({
        jPlayer: "#jquery_jplayer_2",
        cssSelectorAncestor: "#jp_container_2"
      }, [
  <?php echo '<?php
  ';?>if ($audio_in2) {
    echo $audio_in2;
  }
  <?php echo '?>';?>
      ], {
        swfPath: "../../dist/jplayer",
        supplied: "oga, mp3",
        wmode: "window",
        useStateClassSkin: true,
        autoBlur: false,
        smoothPlayBar: true,
        keyEnabled: true
      });

<?php echo '<?php ';?>} if ($audio_post2) { <?php echo '?>';?>
      new jPlayerPlaylist({
        jPlayer: "#jquery_jplayer_3",
        cssSelectorAncestor: "#jp_container_3"
      }, [
  <?php echo '<?php
  ';?>if ($audio_post2) {
    echo $audio_post2;
  }
  <?php echo '?>';?>
      ], {
        swfPath: "../../dist/jplayer",
        supplied: "oga, mp3",
        wmode: "window",
        useStateClassSkin: true,
        autoBlur: false,
        smoothPlayBar: true,
        keyEnabled: true
      });

<?php echo '<?php 
  ';?>} 
    if($stationary_categories){
      <?php echo '?>';?>
      $('#all').click(function(e){
        e.preventDefault();
        $('.all').show(300);
      });
      <?php echo '<?php
      ';?>foreach ($stationary_categories as $cnt){
  <?php echo '?>';?>
     $('#<?php echo '<?php ';?>echo $cnt; <?php echo '?>';?>').click(function(e){
       e.preventDefault();
       $('.all').hide(300);
       $('.<?php echo '<?php ';?>echo $cnt; <?php echo '?>';?>').show(300);
     });
  <?php echo '<?php
      ';?>}
    }
  <?php echo '?>';?>
  });
  //]]>
<?php echo '</script'; ?>
>

<style>
  .miqaat_stat:hover{
    background-color: #549BC7;
  }
  .black_font{
    color:#000 !important;
  }
</style><?php }
}
