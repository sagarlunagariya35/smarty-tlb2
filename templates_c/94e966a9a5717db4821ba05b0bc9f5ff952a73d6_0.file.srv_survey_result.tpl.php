<?php
/* Smarty version 3.1.29, created on 2016-03-25 16:17:01
  from "/var/www/html/smarty_tlb2/templates/srv_survey_result.tpl" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.29',
  'unifunc' => 'content_56f5172538a1a1_37867594',
  'file_dependency' => 
  array (
    '94e966a9a5717db4821ba05b0bc9f5ff952a73d6' => 
    array (
      0 => '/var/www/html/smarty_tlb2/templates/srv_survey_result.tpl',
      1 => 1458902814,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:include/js_block.tpl' => 1,
    'file:include/footer.tpl' => 1,
  ),
),false)) {
function content_56f5172538a1a1_37867594 ($_smarty_tpl) {
?>
<div class="container white-bg">
  <div class="col-md-12 col-sm-12">
    <div class="page-title">
      <p style="margin-top:5px;">
        <a href="<?php echo SERVER_PATH;?>
">Home</a> / 
        <a href="<?php echo SERVER_PATH;?>
srv_take_survey.php?survey_id=<?php echo $_smarty_tpl->tpl_vars['survey_id']->value;?>
"><?php echo $_smarty_tpl->tpl_vars['survey']->value['title'];?>
</a>
      <h1><?php echo $_smarty_tpl->tpl_vars['survey']->value['title'];?>
<!--span class="alfatemi-text">معلومات ذاتية</span--></h1>
    </div>
  </div>
  <div class="clearfix"></div> <!-- do not delete -->
  <form class="forms1 white" name="raza_form" method="post">
    <div class="col-md-3 col-sm-12 hidden-xs">
      <div class="profile-box-static">
        <h3 class="uppercase text-center">Quizzes</h3>
      </div>
      <div class="profile-box-static-bottom">
        <?php if ($_smarty_tpl->tpl_vars['surveys']->value) {?>
          <?php
$_from = $_smarty_tpl->tpl_vars['surveys']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_srv_0_saved_item = isset($_smarty_tpl->tpl_vars['srv']) ? $_smarty_tpl->tpl_vars['srv'] : false;
$_smarty_tpl->tpl_vars['srv'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['srv']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['srv']->value) {
$_smarty_tpl->tpl_vars['srv']->_loop = true;
$__foreach_srv_0_saved_local_item = $_smarty_tpl->tpl_vars['srv'];
?>
            <a href="<?php echo SERVER_PATH;?>
srv_take_survey.php?survey_id=<?php echo $_smarty_tpl->tpl_vars['srv']->value['survey_id'];?>
">» <?php echo $_smarty_tpl->tpl_vars['srv']->value['title'];?>
<hr></a>
          <?php
$_smarty_tpl->tpl_vars['srv'] = $__foreach_srv_0_saved_local_item;
}
if ($__foreach_srv_0_saved_item) {
$_smarty_tpl->tpl_vars['srv'] = $__foreach_srv_0_saved_item;
}
?>
        <?php }?>
      </div>
      <div class="clearfix"><br></div>
    </div>

    <div class="col-md-9 col-sm-12">
      <div class="static-show-button lsd large-fonts col-xs-12 col-sm-5" dir="rtl">Correct <?php echo $_smarty_tpl->tpl_vars['total_score']->value;?>
 of <?php echo count($_smarty_tpl->tpl_vars['survey_questions']->value);?>
</div>
      <div class="clearfix"></div>
      <div>
        <div class="col-xs-12 col-sm-5" id="group_list" dir="rtl"></div>
        <div class="quiz-point col-xs-12 col-sm-4 color1" dir="rtl"></div>
        <div class="clearfix"></div>

        <?php $_smarty_tpl->tpl_vars["sr"] = new Smarty_Variable(0, null);
$_smarty_tpl->ext->_updateScope->updateScope($_smarty_tpl, "sr", 0);?>
        <?php
$_from = $_smarty_tpl->tpl_vars['survey_questions']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_sq_1_saved_item = isset($_smarty_tpl->tpl_vars['sq']) ? $_smarty_tpl->tpl_vars['sq'] : false;
$_smarty_tpl->tpl_vars['sq'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['sq']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['sq']->value) {
$_smarty_tpl->tpl_vars['sq']->_loop = true;
$__foreach_sq_1_saved_local_item = $_smarty_tpl->tpl_vars['sq'];
?>
          <?php $_smarty_tpl->tpl_vars["sr"] = new Smarty_Variable($_smarty_tpl->tpl_vars['sr']->value+1, null);
$_smarty_tpl->ext->_updateScope->updateScope($_smarty_tpl, "sr", 0);?>
          <div class="col-sm-12 col-md-12" style="color:#000;">
            <div class="panel panel-<?php echo $_smarty_tpl->tpl_vars['sq']->value['divclass'];?>
">
              <div class="panel-heading">Question <?php echo $_smarty_tpl->tpl_vars['sr']->value;?>
</div>
              <div class="panel-body lsd" dir="rtl">
                <p class="lsd" style="font-size: 18px" dir="rtl"><?php echo $_smarty_tpl->tpl_vars['sq']->value['question'];?>
</p>
                <p class="lsd" style="font-size: 18px" dir="rtl"><strong>Your Answer:</strong> <?php echo $_smarty_tpl->tpl_vars['sq']->value['user_answer'];?>
</p>
                <?php if (($_smarty_tpl->tpl_vars['sq']->value['divclass'] == 'danger')) {?>
                  <p class="lsd" style="font-size: 18px" dir="rtl"><strong>Correct Answers:</strong> <?php echo $_smarty_tpl->tpl_vars['sq']->value['correct_answers'];?>
</p>
                <?php }?>
              </div>
            </div>
          </div>
        <?php
$_smarty_tpl->tpl_vars['sq'] = $__foreach_sq_1_saved_local_item;
}
if ($__foreach_sq_1_saved_item) {
$_smarty_tpl->tpl_vars['sq'] = $__foreach_sq_1_saved_item;
}
?>
      </div>
    </div>
    <div class="clearfix"></div>
  </form>
</div>
<style type="text/css">
  .panel-body p{
    color: #000;
  }
  
  .profile-box-static-bottom span {
    color: #444444 !important;
    font-size: 14px;
    text-align: left;
    line-height: 150%;
    text-decoration: none;
    margin-left: 15px;
    margin-right: 15px;
  }
  
  .profile-box-static-bottom span a {
    color: #FFF !important;
  }
</style>
<?php $_smarty_tpl->smarty->ext->_subtemplate->render($_smarty_tpl, "file:include/js_block.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

<?php $_smarty_tpl->smarty->ext->_subtemplate->render($_smarty_tpl, "file:include/footer.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
}
}
