<?php
/* Smarty version 3.1.29, created on 2016-02-28 20:48:06
  from "/var/www/html/smarty_tlb2/templates/ques_details_english.tpl" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.29',
  'unifunc' => 'content_56d30faecd43e0_71770458',
  'file_dependency' => 
  array (
    '23f14c1dff7bda8577f1af921642a3128b75d09a' => 
    array (
      0 => '/var/www/html/smarty_tlb2/templates/ques_details_english.tpl',
      1 => 1456672682,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:include/message.tpl' => 1,
  ),
),false)) {
function content_56d30faecd43e0_71770458 ($_smarty_tpl) {
?>
<style>
  .skip{
    display:block;
    max-width:300px;
    -webkit-transition: 0.4s;
    -moz-transition: 0.4s;
    -o-transition: 0.4s;
    transition: 0.4s;
    border: none;
    background-color: #155485;
    height: auto;
    line-height:20px;
    margin:0px auto 20px;
    padding:10px;
    outline: none;
    color:#FFF;
    text-transform:uppercase;
    text-align:center;
    font-weight:bold;
    font-size:18px;
    float:right;
    text-decoration: underline none;
  }
  
  .set{
    background-color: #549BC7 !important;
  }
  
  .skip:hover{
    background-color: #549BC7;
  }
  
  #load {
    color: #000 !important;
    margin: 20px auto 0;
    float: right;
    text-align: center;
  }
</style>

<div class="container white-bg">
  <div class="col-md-12 col-sm-12">

    <div class="page-title">
      <p style="margin-top:5px;">
        <a href="<?php echo SERVER_PATH;?>
marahil/">My Araz for education</a> 
        <?php if ((in_array($_smarty_tpl->tpl_vars['marhala_id']->value,$_smarty_tpl->tpl_vars['marhala_school_array']->value))) {?>
          / <a href="<?php echo SERVER_PATH;?>
marhala-<?php echo $_smarty_tpl->tpl_vars['marhala_id']->value;?>
/<?php echo get_marhala_slug($_smarty_tpl->tpl_vars['marhala_id']->value);?>
/school"><?php echo $_smarty_tpl->tpl_vars['panel_heading']->value;?>
 ( School )</a> 
        <?php } else { ?>
          / <a href="<?php echo SERVER_PATH;?>
marhala-<?php echo $_smarty_tpl->tpl_vars['marhala_id']->value;?>
/<?php echo get_marhala_slug($_smarty_tpl->tpl_vars['marhala_id']->value);?>
/select-institute/"><?php echo $_smarty_tpl->tpl_vars['panel_heading']->value;?>
 (Select Institute)</a>
        <?php }?>
        / <a href="#" class="active"><?php echo $_smarty_tpl->tpl_vars['panel_heading']->value;?>
</a></p>

      <h1><?php echo $_smarty_tpl->tpl_vars['panel_heading']->value;?>
<span class="alfatemi-text"><?php echo $_smarty_tpl->tpl_vars['page_heading_ar']->value;?>
</span></h1>
    </div>
  </div>
  <?php if ((isset($_smarty_tpl->tpl_vars['_SESSION']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_SUCCESS_MESSAGE']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_SUCCESS_MESSAGE']->value['index'] : null)]) || isset($_smarty_tpl->tpl_vars['_SESSION']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_ERROR_MESSAGE']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_ERROR_MESSAGE']->value['index'] : null)]))) {?>
    <div class="row">
      <?php $_smarty_tpl->smarty->ext->_subtemplate->render($_smarty_tpl, "file:include/message.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

    </div>
  <?php }?>
  <div class="clearfix"></div> <!-- do not delete -->  
  
  <div class="col-md-12 col-sm-12">
    <div class="page">
      <span class="block vmgn20"></span>
      <div class="clearfix"></div> <!-- do not delete -->
      <h4>If you would like to submit your azaim (resolutions) before proceeding for raza araz, complete this questionnaire.</h4>
      
      <div class="col-md-3 pull-right hidden-xs"></div>
      <div class="col-md-3 col-xs-12 pull-right text-center"><br>
        <button type="button" name="for_english" id="for_arabic" class="btn btn-block skip lsd" value="1" dir="rtl" onClick="MM_goToURL('parent','<?php echo SERVER_PATH;?>
marhala-<?php echo $_smarty_tpl->tpl_vars['marhala_id']->value;?>
/<?php echo get_marhala_slug($_smarty_tpl->tpl_vars['marhala_id']->value);?>
/ques/');return document.MM_returnValue">لسان الدعوة واسطسس يظظاطط click كروو</button>
      </div>
      
      <div class="col-md-3 col-xs-12 pull-right text-center"><br>
        <button type="button" name="for_english" id="for_english" class="btn btn-block skip set lsd" <?php if (($_smarty_tpl->tpl_vars['page']->value == 'ques_eng')) {?> checked <?php }?>>For English Click Here</button>
      </div><br><br>
      <div class="clearfix"></div>
      
      <form class="forms1 white" action="<?php echo SERVER_PATH;?>
marhala-<?php echo $_smarty_tpl->tpl_vars['marhala_id']->value;?>
/<?php echo get_marhala_slug($_smarty_tpl->tpl_vars['marhala_id']->value);?>
/ques_english/" method="post" onsubmit="return(validate());">
                
        <div class="clearfix"></div> <!-- do not delete -->
        
        <?php $_smarty_tpl->tpl_vars["i"] = new Smarty_Variable(0, null);
$_smarty_tpl->ext->_updateScope->updateScope($_smarty_tpl, "i", 0);?>
          <?php if ($_smarty_tpl->tpl_vars['ques_preffered']->value) {?>
            <?php
$_from = $_smarty_tpl->tpl_vars['ques_preffered']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_q_0_saved_item = isset($_smarty_tpl->tpl_vars['q']) ? $_smarty_tpl->tpl_vars['q'] : false;
$_smarty_tpl->tpl_vars['q'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['q']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['q']->value) {
$_smarty_tpl->tpl_vars['q']->_loop = true;
$__foreach_q_0_saved_local_item = $_smarty_tpl->tpl_vars['q'];
?>
        
              <?php if ((isset($_smarty_tpl->tpl_vars['araz_data']->value[$_smarty_tpl->tpl_vars['i']->value]['answer']))) {?>
                <?php if (($_smarty_tpl->tpl_vars['araz_data']->value[$_smarty_tpl->tpl_vars['i']->value]['answer'] == '1')) {?>
                  <?php $_smarty_tpl->tpl_vars['check1'] = new Smarty_Variable('checked', null);
$_smarty_tpl->ext->_updateScope->updateScope($_smarty_tpl, 'check1', 0);?>
                <?php } elseif (($_smarty_tpl->tpl_vars['araz_data']->value[$_smarty_tpl->tpl_vars['i']->value]['answer'] == '2')) {?>
                  $check2 = 'checked';
                <?php } elseif (($_smarty_tpl->tpl_vars['araz_data']->value[$_smarty_tpl->tpl_vars['i']->value]['answer'] == '3')) {?>
                  $check3 = 'checked';
                <?php } elseif (($_smarty_tpl->tpl_vars['araz_data']->value[$_smarty_tpl->tpl_vars['i']->value]['answer'] == '4')) {?>
                  $check4 = 'checked';
                <?php }?>
              <?php }?>
              <?php $_smarty_tpl->tpl_vars["i"] = new Smarty_Variable($_smarty_tpl->tpl_vars['i']->value+1, null);
$_smarty_tpl->ext->_updateScope->updateScope($_smarty_tpl, "i", 0);?>
        
        <div class="blue-box1"> <!-- Add class rtl -->
          <h3><span class="inline-block lh3" style="text-transform:none;"><?php echo $_smarty_tpl->tpl_vars['i']->value;?>
. <?php echo $_smarty_tpl->tpl_vars['q']->value['question'];?>
</span>
          </h3>
        </div>
            <div class="clearfix"></div> <!-- do not delete -->
            <div class="col-md-3 col-sm-12">
              <div class="radiobuttons-holder1"> <!-- Add Class .rtl -->
                <input type="radio" name="q_<?php echo $_smarty_tpl->tpl_vars['q']->value['id'];?>
" value="1" id="radio1<?php echo $_smarty_tpl->tpl_vars['q']->value['id'];?>
" class="css-radiobutton1" <?php echo $_smarty_tpl->tpl_vars['check1']->value;?>
 />
                <label for="radio1<?php echo $_smarty_tpl->tpl_vars['q']->value['id'];?>
" style="color: #000;" class="css-label1 radGroup3 lsd">I will, by all means.</label>
              </div>
            </div>
            <div class="clearfix"></div>
         <?php
$_smarty_tpl->tpl_vars['q'] = $__foreach_q_0_saved_local_item;
}
if ($__foreach_q_0_saved_item) {
$_smarty_tpl->tpl_vars['q'] = $__foreach_q_0_saved_item;
}
?>
      <?php }?>
            
        <p style="color:#444444;">We recommend you to complete the remaining questionnaire pertaining your educational journey. <a data-toggle="collapse" href="#collapseExample" aria-expanded="false" aria-controls="collapseExample" style="color:#155485;">Show / Hide</a></p><br>

        <div class="collapse" id="collapseExample">
          <?php if ($_smarty_tpl->tpl_vars['ques']->value) {?>
      		<?php
$_from = $_smarty_tpl->tpl_vars['ques']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_q_1_saved_item = isset($_smarty_tpl->tpl_vars['q']) ? $_smarty_tpl->tpl_vars['q'] : false;
$_smarty_tpl->tpl_vars['q'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['q']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['q']->value) {
$_smarty_tpl->tpl_vars['q']->_loop = true;
$__foreach_q_1_saved_local_item = $_smarty_tpl->tpl_vars['q'];
?>

            <?php if ((isset($_smarty_tpl->tpl_vars['araz_data']->value[$_smarty_tpl->tpl_vars['i']->value]['answer']))) {?>
               <?php if (($_smarty_tpl->tpl_vars['araz_data']->value[$_smarty_tpl->tpl_vars['i']->value]['answer'] == '1')) {?>
                 <?php $_smarty_tpl->tpl_vars['check1'] = new Smarty_Variable('checked', null);
$_smarty_tpl->ext->_updateScope->updateScope($_smarty_tpl, 'check1', 0);?>
               <?php } elseif (($_smarty_tpl->tpl_vars['araz_data']->value[$_smarty_tpl->tpl_vars['i']->value]['answer'] == '2')) {?>
                 $check2 = 'checked';
               <?php } elseif (($_smarty_tpl->tpl_vars['araz_data']->value[$_smarty_tpl->tpl_vars['i']->value]['answer'] == '3')) {?>
                 $check3 = 'checked';
               <?php } elseif (($_smarty_tpl->tpl_vars['araz_data']->value[$_smarty_tpl->tpl_vars['i']->value]['answer'] == '4')) {?>
                 $check4 = 'checked';
                <?php }?>
              <?php }?>
              <?php $_smarty_tpl->tpl_vars["i"] = new Smarty_Variable($_smarty_tpl->tpl_vars['i']->value+1, null);
$_smarty_tpl->ext->_updateScope->updateScope($_smarty_tpl, "i", 0);?>

              <div class="blue-box1"> <!-- Add class rtl -->
                <h3><span class="inline-block lh3" style="text-transform:none;"><?php echo $_smarty_tpl->tpl_vars['i']->value;?>
. <?php echo $_smarty_tpl->tpl_vars['q']->value['question'];?>
</span>
                </h3>
              </div>
              <div class="clearfix"></div> <!-- do not delete -->
              <div class="col-md-3 col-sm-12">
                <div class="radiobuttons-holder1"> <!-- Add Class .rtl -->
                  <input type="radio" name="q_<?php echo $_smarty_tpl->tpl_vars['q']->value['id'];?>
" value="1" id="radio1<?php echo $_smarty_tpl->tpl_vars['q']->value['id'];?>
" class="css-radiobutton1" <?php echo $_smarty_tpl->tpl_vars['check1']->value;?>
 />
                  <label for="radio1<?php echo $_smarty_tpl->tpl_vars['q']->value['id'];?>
" style="color: #000;" class="css-label1 radGroup3 lsd">I will, by all means.</label>
                </div>
              </div>
              <div class="clearfix"></div>
          <?php
$_smarty_tpl->tpl_vars['q'] = $__foreach_q_1_saved_local_item;
}
if ($__foreach_q_1_saved_item) {
$_smarty_tpl->tpl_vars['q'] = $__foreach_q_1_saved_item;
}
?>
  		<?php }?>
        </div>
              
        <?php if ((!$_smarty_tpl->tpl_vars['ques_preffered']->value && !$_smarty_tpl->tpl_vars['ques']->value)) {?>
            <div class="blue-box1"> <!-- Add class rtl -->
          <h3><span class="inline-block bigger1 lh1 w500 left-mgn20">1</span>
            <span class="inline-block lh3">Will your Child be able to attend full waaz of all nine days of Asharah Mubarakah?</span>
          </h3>
        </div>
            <div class="clearfix"></div> <!-- do not delete -->
            <div class="col-md-3 col-sm-12">
              <div class="radiobuttons-holder1"> <!-- Add Class .rtl -->
                <input type="radio" name="q_1" value="1" id="radio11" class="css-radiobutton1" />
                <label for="radio11" style="color: #000;" class="css-label1 radGroup3 lsd">I will, by all means.</label>
              </div>
            </div>
            <div class="clearfix"></div>
        <?php }?>
        
         <!--div class="blue-box1">
          <h3 class="bordered">
            <span class="lsd">
                <div class="show lsd font18 text-center"><?php echo '<?php ';?>echo $araz_notes[0]['note']; <?php echo '?>';?></div>
            </span>
          </h3>
          <div class="clearfix"></div> <!-- do not delete -->
        <!--/div-->
        
        <div class="clearfix"></div> <!-- do not delete -->
        <input type="submit" class="submit1 proceed col-md-3 col-xs-12" name="submit" value="Proceed with Araz" onclick="return ray.ajax()"/>
        <div id="load" style="display: none;"><i class="fa fa-spinner fa-spin fa-3x"></i></div>
        <input type="submit" class="submit1 col-md-3 col-xs-12" name="save_araz"  value="Save Araz" style="margin-right: 5px;" />
        <a class="pull-left col-md-3 col-xs-12 start-again" onclick="start_again();">Start Over Again</a>
        <span class="page-steps">Step : 4 out of 5</span>
        <div class="clearfix"></div> <!-- do not delete -->
      </form>
      <div class="clearfix"></div> <!-- do not delete -->
    </div>
  </div>
</div>

<style>

  .morectnt span {
    display: none;
  }
  .showmoretxt {
    text-decoration: none;
  }
</style>

<?php echo '<script'; ?>
 type="text/javascript">
// Form validation code will come here.
  //function validate() {
    //<?php
$_from = $_smarty_tpl->tpl_vars['ques']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_q_2_saved_item = isset($_smarty_tpl->tpl_vars['q']) ? $_smarty_tpl->tpl_vars['q'] : false;
$_smarty_tpl->tpl_vars['q'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['q']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['q']->value) {
$_smarty_tpl->tpl_vars['q']->_loop = true;
$__foreach_q_2_saved_local_item = $_smarty_tpl->tpl_vars['q'];
?>
		//      if ($('input[name=q_<?php echo '<?php ';?>echo $q['id']; <?php echo '?>';?>]:checked').length <= 0)
		//      {
		//        alert("Please Select any Answer!");
		//        return false;
		//      }
	//<?php
$_smarty_tpl->tpl_vars['q'] = $__foreach_q_2_saved_local_item;
}
if ($__foreach_q_2_saved_item) {
$_smarty_tpl->tpl_vars['q'] = $__foreach_q_2_saved_item;
}
?>
  //}
//-->
  var ray = {
    ajax: function(st)
    {
      this.show('load');
      $('.proceed').hide();
    },
    show: function(el)
    {
      this.getID(el).style.display = '';
    },
    getID: function(el)
    {
      return document.getElementById(el);
    }
  }
<?php echo '</script'; ?>
>

<?php echo '<script'; ?>
 src="http://code.jquery.com/jquery-1.8.2.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
>
  $(function() {
    var showTotalChar = 200, showChar = "Read More", hideChar = "Hide";
    $('.show').each(function() {
      var content = $(this).text();
      if (content.length > showTotalChar) {
        var con = content.substr(0, showTotalChar);
        var hcon = content.substr(showTotalChar, content.length - showTotalChar);
        var txt = con + '<span class="dots">...</span><span class="morectnt"><span>' + hcon + '</span>&nbsp;&nbsp;<a href="" class="showmoretxt">' + showChar + '</a></span>';
        $(this).html(txt);
      }
    });
    $(".showmoretxt").click(function() {
      if ($(this).hasClass("sample")) {
        $(this).removeClass("sample");
        $(this).text(showChar);
      } else {
        $(this).addClass("sample");
        $(this).text(hideChar);
      }
      $(this).parent().prev().toggle();
      $(this).prev().toggle();
      return false;
    });
  });
  
  function MM_goToURL() { //v3.0
    var i, args=MM_goToURL.arguments; document.MM_returnValue = false;
    for (i=0; i<(args.length-1); i+=2) eval(args[i]+".location='"+args[i+1]+"'");
  }
  
  function start_again() {
    var ask = window.confirm("Are you sure you want to start again?");
    if (ask) {
        document.location.href = "<?php echo SERVER_PATH;?>
start_over_again.php";
    }
  }
<?php echo '</script'; ?>
><?php }
}
