<?php
/* Smarty version 3.1.29, created on 2016-04-09 12:58:20
  from "/var/www/html/smarty_tlb2/templates/school_details.tpl" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.29',
  'unifunc' => 'content_5708af14e44e60_92638528',
  'file_dependency' => 
  array (
    'c6d530b1e70fba091b9f3f41e83acb4cf55b3823' => 
    array (
      0 => '/var/www/html/smarty_tlb2/templates/school_details.tpl',
      1 => 1460186898,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:include/message.tpl' => 1,
    'file:include/js_block.tpl' => 1,
    'file:include/footer.tpl' => 1,
    'file:sent_queries.tpl' => 1,
  ),
),false)) {
function content_5708af14e44e60_92638528 ($_smarty_tpl) {
?>
<div class="container white-bg">
  <div class="col-md-12 col-sm-12">

    <div class="page-title">
      <p style="margin-top:5px;">
        <a href="<?php echo $_smarty_tpl->tpl_vars['server_path']->value;?>
marahil/">My Araz for education</a> / 
        <a href="#" class="active"><?php echo $_smarty_tpl->tpl_vars['panel_heading']->value;?>
</a>
      </p>
      <h1><?php echo $_smarty_tpl->tpl_vars['panel_heading']->value;?>
<span class="alfatemi-text"><?php echo $_smarty_tpl->tpl_vars['panel_heading_ar']->value;?>
</span></h1>
    </div>
  </div>
  <?php if ((isset($_smarty_tpl->tpl_vars['success_message']->value) || isset($_smarty_tpl->tpl_vars['error_message']->value))) {?>
    <div class="row">
      <?php $_smarty_tpl->smarty->ext->_subtemplate->render($_smarty_tpl, "file:include/message.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

    </div>
  <?php }?>
  <div class="clearfix"></div> <!-- do not delete -->
  <div class="col-md-12 col-sm-12">
    <div class="page">

      <h6><span class="alfatemi-text" dir="rtl"><?php echo $_smarty_tpl->tpl_vars['kalemaat_nooraniyah']->value;?>
 - من الكلمات النورانية</span></h6>

      <h4>Complete this form to submit your araz for <?php echo $_smarty_tpl->tpl_vars['std_heading']->value;?>
 Education. <span class="alfatemi-text">تماري تعليم ني عرض واسطے اْ فارم بهري عرض كرو</span></h4>

      <form name="school_form" class="forms1" action="<?php echo $_smarty_tpl->tpl_vars['server_path']->value;?>
marhala-<?php echo $_smarty_tpl->tpl_vars['group_id']->value;?>
/<?php echo get_marhala_slug($_smarty_tpl->tpl_vars['group_id']->value);?>
/school/" method="post" onsubmit="return(validate());">

        <h3>School / Grade<span class="alfatemi-text">اسكول</span></h3>

        <div class="clearfix"></div> <!-- do not delete -->
        <div class="col-md-6 col-sm-12 shift">
          <lable class="white required-star"><b>Is your school:</b></lable>
          <div  class="shift10 lable-mrgn">
            <select name="filter_school" class="required select-input">
              <option value="">Is your school</option>
              <?php
$_from = $_smarty_tpl->tpl_vars['filter_school_array']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_scl_0_saved_item = isset($_smarty_tpl->tpl_vars['scl']) ? $_smarty_tpl->tpl_vars['scl'] : false;
$_smarty_tpl->tpl_vars['scl'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['scl']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['scl']->value) {
$_smarty_tpl->tpl_vars['scl']->_loop = true;
$__foreach_scl_0_saved_local_item = $_smarty_tpl->tpl_vars['scl'];
?>
                <option value="<?php echo $_smarty_tpl->tpl_vars['scl']->value;?>
" <?php if (($_smarty_tpl->tpl_vars['school_filter']->value == $_smarty_tpl->tpl_vars['scl']->value)) {?> selected <?php }?>><?php echo $_smarty_tpl->tpl_vars['scl']->value;?>
</option>
              <?php
$_smarty_tpl->tpl_vars['scl'] = $__foreach_scl_0_saved_local_item;
}
if ($__foreach_scl_0_saved_item) {
$_smarty_tpl->tpl_vars['scl'] = $__foreach_scl_0_saved_item;
}
?>
            </select>
          </div>
        </div>
        <div class="col-md-6 col-sm-12 shift">
          <lable class="white required-star"><b>Standard:</b></lable>
          <div  class="shift10 lable-mrgn">
            <select name="school_standard" class="required select-input">
              <option value="">Standard</option>
              <?php
$_from = $_smarty_tpl->tpl_vars['ary_standard']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_std_1_saved_item = isset($_smarty_tpl->tpl_vars['std']) ? $_smarty_tpl->tpl_vars['std'] : false;
$_smarty_tpl->tpl_vars['std'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['std']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['std']->value) {
$_smarty_tpl->tpl_vars['std']->_loop = true;
$__foreach_std_1_saved_local_item = $_smarty_tpl->tpl_vars['std'];
?>
              	<?php $_smarty_tpl->tpl_vars["school_standard"] = new Smarty_Variable('', null);
$_smarty_tpl->ext->_updateScope->updateScope($_smarty_tpl, "school_standard", 0);?>
                <?php if (($_smarty_tpl->tpl_vars['msb_student_info']->value == '1')) {?>
                  <?php $_smarty_tpl->tpl_vars["school_standard"] = new Smarty_Variable($_smarty_tpl->tpl_vars['madrasah_darajah']->value, null);
$_smarty_tpl->ext->_updateScope->updateScope($_smarty_tpl, "school_standard", 0);?>
                <?php } else { ?>
                  <?php $_smarty_tpl->tpl_vars["school_standard"] = new Smarty_Variable($_smarty_tpl->tpl_vars['school_standard']->value, null);
$_smarty_tpl->ext->_updateScope->updateScope($_smarty_tpl, "school_standard", 0);?>
                <?php }?>
                <option value="<?php echo $_smarty_tpl->tpl_vars['std']->value;?>
" <?php if (($_smarty_tpl->tpl_vars['school_standard']->value == $_smarty_tpl->tpl_vars['std']->value)) {?> selected <?php }?>><?php echo $_smarty_tpl->tpl_vars['std']->value;?>
</option>
              <?php
$_smarty_tpl->tpl_vars['std'] = $__foreach_std_1_saved_local_item;
}
if ($__foreach_std_1_saved_item) {
$_smarty_tpl->tpl_vars['std'] = $__foreach_std_1_saved_item;
}
?>
            </select>
          </div>
        </div>

        <div class = "col-md-6 col-sm-12 shift">
          <lable class="white required-star"><b>Country:</b></lable>
          <div class="shift10 lable-mrgn">
            <select name="madrasah_country" onChange="showcity(this.value)" class="required select-input">
              <option value="">Country</option>
              <?php if (($_smarty_tpl->tpl_vars['countries']->value != '')) {?>
                <?php
$_from = $_smarty_tpl->tpl_vars['countries']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_cntry_2_saved_item = isset($_smarty_tpl->tpl_vars['cntry']) ? $_smarty_tpl->tpl_vars['cntry'] : false;
$_smarty_tpl->tpl_vars['cntry'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['cntry']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['cntry']->value) {
$_smarty_tpl->tpl_vars['cntry']->_loop = true;
$__foreach_cntry_2_saved_local_item = $_smarty_tpl->tpl_vars['cntry'];
?>
                  <option value="<?php echo $_smarty_tpl->tpl_vars['cntry']->value['name'];?>
" <?php if (($_smarty_tpl->tpl_vars['cntry']->value['name'] == $_smarty_tpl->tpl_vars['scl_country']->value)) {?> selected <?php }?> ><?php echo $_smarty_tpl->tpl_vars['cntry']->value['name'];?>
</option>
                <?php
$_smarty_tpl->tpl_vars['cntry'] = $__foreach_cntry_2_saved_local_item;
}
if ($__foreach_cntry_2_saved_item) {
$_smarty_tpl->tpl_vars['cntry'] = $__foreach_cntry_2_saved_item;
}
?>
              <?php }?>
            </select>
          </div>
        </div>


        <div class="col-md-6 col-sm-12 shift">
          <lable class="white required-star"><b>City:</b></lable>
          <div  class="shift10 lable-mrgn" id="your_city">
            <select name="madrasah_city" id="select-city" class="required select-input">
              <option value="">City - Where your school is located</option>
              <?php if (($_smarty_tpl->tpl_vars['ary_cities']->value)) {?>
                <?php $_smarty_tpl->tpl_vars["scl_city"] = new Smarty_Variable('', null);
$_smarty_tpl->ext->_updateScope->updateScope($_smarty_tpl, "scl_city", 0);?>
                <?php
$_from = $_smarty_tpl->tpl_vars['ary_cities']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_city_3_saved_item = isset($_smarty_tpl->tpl_vars['city']) ? $_smarty_tpl->tpl_vars['city'] : false;
$_smarty_tpl->tpl_vars['city'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['city']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['city']->value) {
$_smarty_tpl->tpl_vars['city']->_loop = true;
$__foreach_city_3_saved_local_item = $_smarty_tpl->tpl_vars['city'];
?>
                  <?php if (($_smarty_tpl->tpl_vars['school_city']->value != '')) {?>
                    <?php $_smarty_tpl->tpl_vars["scl_city"] = new Smarty_Variable($_smarty_tpl->tpl_vars['school_city']->value, null);
$_smarty_tpl->ext->_updateScope->updateScope($_smarty_tpl, "scl_city", 0);?>
                  <?php } elseif (($_smarty_tpl->tpl_vars['my_araz']->value['school_city'] != '')) {?>
                    <?php $_smarty_tpl->tpl_vars["scl_city"] = new Smarty_Variable($_smarty_tpl->tpl_vars['my_araz']->value['school_city'], null);
$_smarty_tpl->ext->_updateScope->updateScope($_smarty_tpl, "scl_city", 0);?>
                  <?php } else { ?>
                    <?php $_smarty_tpl->tpl_vars["scl_city"] = new Smarty_Variable($_smarty_tpl->tpl_vars['user']->value->get_city(), null);
$_smarty_tpl->ext->_updateScope->updateScope($_smarty_tpl, "scl_city", 0);?>
                  <?php }?>
                  <option value="<?php echo $_smarty_tpl->tpl_vars['city']->value['city'];?>
" <?php if (($_smarty_tpl->tpl_vars['city']->value['city'] == $_smarty_tpl->tpl_vars['scl_city']->value)) {?> selected <?php }?> ><?php echo $_smarty_tpl->tpl_vars['city']->value['city'];?>
</option>
                <?php
$_smarty_tpl->tpl_vars['city'] = $__foreach_city_3_saved_local_item;
}
if ($__foreach_city_3_saved_item) {
$_smarty_tpl->tpl_vars['city'] = $__foreach_city_3_saved_item;
}
?>
              <?php }?>
            </select>
          </div>
        </div>

        <div class="col-md-6 col-sm-12 shift">
          <lable class="white required-star"><b>School Name:</b></lable>
          <div  class="shift10 lable-mrgn">
            <select name="school_name" id="school_name" class="required school_name">
              <option value="">Please write the School name with correct spelling</option>
               
            </select>
          </div>
        </div>
        <div class="col-md-6 col-sm-12 shift">
          <lable class="white required-star"><b>Pincode:</b></lable>
          <div  class="shift10 lable-mrgn">
            <input type="text" name="school_pincode" placeholder="Pincode/Zipcode" value="<?php echo $_smarty_tpl->tpl_vars['school_pincode']->value;?>
">
          </div>
        </div>
        <div class="clearfix"></div> <!-- do not delete -->
        <input type="submit" class="submit1" name="submit" value="next" onclick="return ray.ajax()"/>
        <div id="load" style="display: none;"><i class="fa fa-spinner fa-spin fa-3x"></i></div>
        <input type="submit" name="save_araz" class="submit1" value="Save Araz" style="margin-right: 5px;" />
        <a class="pull-left col-md-3 col-xs-12 start-again" onclick="start_again();">Start Over Again</a>
        <span style="color:#FFF;" class="page-steps">Step : 3 out of 5</span>
        <div class="clearfix"></div> <!-- do not delete -->
      </form>
      <div class="clearfix"></div> <!-- do not delete -->
    </div>
  </div>
</div>
            
<?php $_smarty_tpl->smarty->ext->_subtemplate->render($_smarty_tpl, "file:include/js_block.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>


<?php echo '<script'; ?>
 type="text/javascript">
// Form validation code will come here.
  function validate()
  {

    if (document.school_form.filter_school.value == "")
    {
      alert("Please Select Your School!");
      document.school_form.filter_school.focus();
      return false;
    }
    if (document.school_form.school_standard.value == "")
    {
      alert("Please Select Your Standard!");
      document.school_form.school_standard.focus();
      return false;
    }
    if (document.school_form.madrasah_country.value == "")
    {
      alert("Please Select Country!");
      document.school_form.madrasah_country.focus();
      return false;
    }
    if (document.school_form.madrasah_city.value == "")
    {
      alert("Please Enter City where your School is Located!");
      document.school_form.madrasah_city.focus();
      return false;
    }
    if (document.school_form.school_name.value == "")
    {
      alert("Please Enter School Name!");
      document.school_form.school_name.focus();
      return false;
    }
    return(true);
  }
//-->
<?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 type="text/javascript">
  function formatRepo(repo) {
    if (repo.loading)
      return repo.text;

    var markup = '<div class="clearfix">' +
            '<div class="col-sm-12">' + repo.school_name + '</div>' +
            '</div>';

    return markup;
  }

  function formatRepoSelection(repo) {
    return repo.school_name;
  }

  $(".school_name").select2({
    ajax: {
      url: "<?php echo $_smarty_tpl->tpl_vars['server_path']->value;?>
ajax_search.php?cmd=get_school_list",
      dataType: 'json',
      delay: 250,
      data: function(params) {
        alert("hi");
        return {
          q: params.term, // search term
          page: params.page
        };
      },
      processResults: function (data, params) {
        // parse the results into the format expected by Select2
        // since we are using custom formatting functions we do not need to
        // alter the remote JSON data, except to indicate that infinite
        // scrolling can be used
        params.page = params.page || 1;

        alert(data);
        return {
          results: data.items,
          pagination: {
            more: (params.page * 30) < data.total_count
          }
        };
      },
      cache: true
    },
    escapeMarkup: function(markup) {
      return markup;
    }, // let our custom formatter work
    minimumInputLength: 1,
    templateResult: formatRepo, // omitted for brevity, see the source of this page
    templateSelection: formatRepoSelection // omitted for brevity, see the source of this page
  });
  
  function start_again() {
    var ask = window.confirm("Are you sure you want to start again?");
    if (ask) {
      document.location.href = "<?php echo $_smarty_tpl->tpl_vars['server_path']->value;?>
start_over_again.php";
    }
  }
  
  var ray = {
    ajax: function(st)
    {
      this.show('load');
      $('.submit1').hide();
    },
    show: function(el)
    {
      this.getID(el).style.display = '';
    },
    getID: function(el)
    {
      return document.getElementById(el);
    }
  }
<?php echo '</script'; ?>
>

<?php echo '<script'; ?>
>
  function showcity(str)
  {
    if (str == "")
    {
      document.getElementById("your_city").innerHTML = "";
      document.getElementById("mname").innerHTML = "";
      return;
    }

    if (window.XMLHttpRequest)
    {
      xmlhttp = new XMLHttpRequest();
    }
    else
    {
      xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
    }

    xmlhttp.onreadystatechange = function ()
    {
      if (xmlhttp.readyState == 4 && xmlhttp.status == 200)
      {
        document.getElementById("your_city").innerHTML = xmlhttp.responseText;
      }
    }
    xmlhttp.open("GET", "get_city_by_country/" + str, true);
    xmlhttp.send();
  }
<?php echo '</script'; ?>
>

<style>
  #result
  {
    position:relative;
    width:auto;
    padding:10px;
    display:none;
    margin-top:-1px;
    border-top:0px;
    overflow:hidden;
    border:1px #CCC solid;
    background-color: white;
    height: auto;
    max-height: 350px;
    overflow: scroll;
  }
  .show
  {
    padding:5px; 
    border-bottom:1px #999 dashed;
    font-size:15px; 
    height:30px;
  }
  .show:hover
  {
    background:#eee;
    color:#000;
    cursor:pointer;
  }
  .lable-mrgn{
    margin-top:5px !important;
  }
  
  #load {
    color: #FFF !important;
    margin: 20px auto 0;
    float: right;
    text-align: center;
  }
</style>
<?php $_smarty_tpl->smarty->ext->_subtemplate->render($_smarty_tpl, "file:include/footer.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

<?php $_smarty_tpl->smarty->ext->_subtemplate->render($_smarty_tpl, "file:sent_queries.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
}
}
