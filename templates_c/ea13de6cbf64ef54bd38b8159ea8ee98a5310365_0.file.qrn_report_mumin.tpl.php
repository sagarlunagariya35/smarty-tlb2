<?php
/* Smarty version 3.1.29, created on 2016-03-16 11:57:48
  from "/var/www/html/smarty_tlb2/templates/qrn_report_mumin.tpl" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.29',
  'unifunc' => 'content_56e8fce4418de0_82267126',
  'file_dependency' => 
  array (
    'ea13de6cbf64ef54bd38b8159ea8ee98a5310365' => 
    array (
      0 => '/var/www/html/smarty_tlb2/templates/qrn_report_mumin.tpl',
      1 => 1457697062,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:include/js_block.tpl' => 1,
    'file:include/message.tpl' => 1,
    'file:include/qrn_message.tpl' => 1,
    'file:include/qrn_links.tpl' => 1,
    'file:include/footer.tpl' => 1,
  ),
),false)) {
function content_56e8fce4418de0_82267126 ($_smarty_tpl) {
$_smarty_tpl->smarty->ext->_subtemplate->render($_smarty_tpl, "file:include/js_block.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

<link href="<?php echo SERVER_PATH;?>
assets/datatables/dataTables.bootstrap.css" rel="stylesheet" type="text/css" />
<?php echo '<script'; ?>
 src="<?php echo SERVER_PATH;?>
assets/datatables/jquery.dataTables.min.js" type="text/javascript"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 src="<?php echo SERVER_PATH;?>
assets/datatables/dataTables.bootstrap.min.js" type="text/javascript"><?php echo '</script'; ?>
>

<div class="container white-bg">
  <div class="col-md-12 col-sm-12">
    <div class="page-title">
      <p style="margin-top:5px;">
        <a href="#" class="active">My Tasmee Report</a></p>
        <h1>My Tasmee Report</h1>
    </div>
  </div>
  <?php if ((isset($_smarty_tpl->tpl_vars['_SESSION']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_SUCCESS_MESSAGE']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_SUCCESS_MESSAGE']->value['index'] : null)]) || isset($_smarty_tpl->tpl_vars['_SESSION']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_ERROR_MESSAGE']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_ERROR_MESSAGE']->value['index'] : null)]))) {?>
    <div class="row">
      <?php $_smarty_tpl->smarty->ext->_subtemplate->render($_smarty_tpl, "file:include/message.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

    </div>
  <?php }?>
  <div class="clearfix"></div> <!-- do not delete -->
  <div class="col-md-12 col-sm-12">
    <form action="" method="post" name="tasmee_form">
      <?php $_smarty_tpl->smarty->ext->_subtemplate->render($_smarty_tpl, "file:include/qrn_message.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

      <div class="body-container white-bg">
        <div class="hifz-tasmee-details-wrapper">
          <div class="htd-section">
            <h2>How many aayaat I do tasmee daily (<?php echo date('jS M Y');?>
)</h2>
            <div class="row">
              <div class="col-xs-12 col-md-12">
                <div class="col-xs-12 col-md-9">
                  <?php if ($_smarty_tpl->tpl_vars['tasmee_records']->value) {?>
                    <div id="columnchart_values"></div>
                  <?php } else { ?>
                    No Data Available.
                  <?php }?>
                  <div class="clearfix"></div>
                  <div id="donutchart" style="height: 400px;"></div>
                </div>
                <div class="col-xs-12 col-md-3">
                  <div class="text-center">
                    <p>Current Sanad</p>
                    <p><strong><?php echo $_smarty_tpl->tpl_vars['mumin_data']->value[0]['mumin_quran_sanad'];?>
</strong></p>

                    <div class="clearfix"><br></div>

                    <p>Currently at</p>
                    <p>Surah: <strong><?php echo $_smarty_tpl->tpl_vars['qrn_tasmee']->value->get_surat_name($_smarty_tpl->tpl_vars['mumin_data']->value[0]['last_tasmee_surat']);?>
</strong></p>
                    <p>Aayaah: <strong><?php echo $_smarty_tpl->tpl_vars['mumin_data']->value[0]['last_tasmee_ayat'];?>
</strong></p>

                    <p>Your Sadeeq Name</p>
                    <p><strong><?php echo $_smarty_tpl->tpl_vars['mumin_data']->value[0]['muhafiz_full_name'];?>
</strong><p>
                    <p>Email: <?php echo $_smarty_tpl->tpl_vars['user_logedin']->value->get_email();?>
<p>
                    <p>Mobile: <?php echo $_smarty_tpl->tpl_vars['user_logedin']->value->get_mobile();?>
<p>
                    <p>Whatsapp: <?php echo $_smarty_tpl->tpl_vars['user_logedin']->value->get_whatsapp();?>
<p>
                    <p>Skype: <?php echo $_smarty_tpl->tpl_vars['user_logedin']->value->get_viber();?>
<p>
                      
                    <p>Your Schedule</p>
                    <p>Days: <?php echo $_smarty_tpl->tpl_vars['schedule_record']->value['days'];?>
</p>
                    <p>Hour: <?php echo $_smarty_tpl->tpl_vars['schedule_record']->value['hour'];?>
</p>
                    <p>Time: <?php echo $_smarty_tpl->tpl_vars['schedule_record']->value['time'];?>
</p>
                  </div>
                </div>
              </div>
            </div>
            <div class="clearfix"><br></div>
            <h2><a href="#" id="tlb_mumin">Show Tasmee Details</a><a href="<?php echo SERVER_PATH;?>
qrn_edit_last_tasmee_entry.php" class="pull-right">Edit my last Tasmee Entry</a></h2>
            <div class="row" id="show_mumin_table"></div>
          </div>
          <?php $_smarty_tpl->smarty->ext->_subtemplate->render($_smarty_tpl, "file:include/qrn_links.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

        </div>
      </div>
    </form>
  </div>
</div>

<?php echo '<script'; ?>
 type="text/javascript" src="<?php echo SERVER_PATH;?>
js/loader.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 type="text/javascript">
  google.charts.load("current", {
    packages:['corechart']
  });
  google.charts.setOnLoadCallback(drawChart);
  function drawChart() {
    var data = google.visualization.arrayToDataTable([
      ["Element", "Total Aayaat"]
        <?php if ($_smarty_tpl->tpl_vars['tasmee_records']->value) {?>
          <?php echo ',';?>

          <?php
$_from = $_smarty_tpl->tpl_vars['tasmee_records']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_data_0_saved_item = isset($_smarty_tpl->tpl_vars['data']) ? $_smarty_tpl->tpl_vars['data'] : false;
$_smarty_tpl->tpl_vars['data'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['data']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['data']->value) {
$_smarty_tpl->tpl_vars['data']->_loop = true;
$__foreach_data_0_saved_local_item = $_smarty_tpl->tpl_vars['data'];
?>
            <?php ob_start();
echo date('d, M Y',strtotime($_smarty_tpl->tpl_vars['data']->value['timestamp']));
$_tmp1=ob_get_clean();
ob_start();
echo $_smarty_tpl->tpl_vars['data']->value['sum_total_ayat'];
$_tmp2=ob_get_clean();
echo array($_tmp1,$_tmp2);?>

            <?php if ((count($_smarty_tpl->tpl_vars['tasmee_records']->value)-1)) {?>
              <?php echo ',';?>

            <?php }?>
          <?php
$_smarty_tpl->tpl_vars['data'] = $__foreach_data_0_saved_local_item;
}
if ($__foreach_data_0_saved_item) {
$_smarty_tpl->tpl_vars['data'] = $__foreach_data_0_saved_item;
}
?>
        <?php }?>
      ]);

    var options = {
      legend: 'none',
      colors: ['#337AB7'],
      pointSize: 10,
      pointShape: {
        type: 'point', rotation: 180
      }
    };

    var chart = new google.visualization.AreaChart(document.getElementById("columnchart_values"));
    chart.draw(data, options);
  }

  $(window).resize(function () {
    drawChart();
  });

  var tasmee_ayat = <?php echo $_smarty_tpl->tpl_vars['total_ayat_count']->value;?>
;
  var remain_ayat = 6236 - tasmee_ayat;
  google.charts.setOnLoadCallback(drawPieChart);
  function drawPieChart() {
    var data = google.visualization.arrayToDataTable([
      ['Remaining Aayaat', 'Tasmee Aayaat'],
      ['Remaining Aayaat <?php echo (6236-$_smarty_tpl->tpl_vars['total_ayat_count']->value);?>
', remain_ayat],
      ['Tasmee Aayaat <?php echo $_smarty_tpl->tpl_vars['total_ayat_count']->value;?>
', tasmee_ayat]
    ]);

    var options = {
      title: 'Aayaat Completed',
      pieSliceText: 'value',
      pieHole: 0.4,
    };

    var chart = new google.visualization.PieChart(document.getElementById('donutchart'));
    chart.draw(data, options);
  }
  
  $('#tlb_mumin').on("click", function(e){
    e.preventDefault();
    
    $.ajax({
      type: "POST",
      url: "ajax.php",
      data: 'query=get_mumin_table&mumin_table=mumin_table',
      cache: false,
      success: function (response) {
        $('.show_mumin_table').show(600);
        
        if (response != '') {
          $('#show_mumin_table').html(response);
        } 
      }
    });
  });

<?php echo '</script'; ?>
>

<style>
  p {
    padding: 5px;
  }
</style>
<?php $_smarty_tpl->smarty->ext->_subtemplate->render($_smarty_tpl, "file:include/footer.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
}
}
