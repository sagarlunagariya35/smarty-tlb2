<?php
/* Smarty version 3.1.29, created on 2016-03-16 11:58:34
  from "/var/www/html/smarty_tlb2/templates/qrn_edit_last_tasmee_entry.tpl" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.29',
  'unifunc' => 'content_56e8fd128e0e56_16480811',
  'file_dependency' => 
  array (
    '183d6874c26544074f096e0f4e54a8a1ebc7289d' => 
    array (
      0 => '/var/www/html/smarty_tlb2/templates/qrn_edit_last_tasmee_entry.tpl',
      1 => 1458109711,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:include/message.tpl' => 1,
    'file:include/qrn_message.tpl' => 1,
    'file:include/qrn_surat_list.tpl' => 1,
    'file:include/qrn_links.tpl' => 1,
    'file:include/js_block.tpl' => 1,
    'file:include/footer.tpl' => 1,
  ),
),false)) {
function content_56e8fd128e0e56_16480811 ($_smarty_tpl) {
?>
<div class="container white-bg">
  <div class="col-md-12 col-sm-12">
    <div class="page-title">
      <p style="margin-top:5px;">
        <a href="#" class="active">My Last Tasmee Entry</a></p>
        <h1>My Last Tasmee Entry</h1>
    </div>
  </div>
  <?php if ((isset($_smarty_tpl->tpl_vars['_SESSION']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_SUCCESS_MESSAGE']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_SUCCESS_MESSAGE']->value['index'] : null)]) || isset($_smarty_tpl->tpl_vars['_SESSION']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_ERROR_MESSAGE']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_ERROR_MESSAGE']->value['index'] : null)]))) {?>
    <div class="row">
      <?php $_smarty_tpl->smarty->ext->_subtemplate->render($_smarty_tpl, "file:include/message.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

    </div>
  <?php }?>
  <div class="clearfix"></div> <!-- do not delete -->
  <div class="col-md-12 col-sm-12">
    <form action="" method="post" name="mumin_form">
      <?php $_smarty_tpl->smarty->ext->_subtemplate->render($_smarty_tpl, "file:include/qrn_message.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

      <div class="body-container white-bg">
        <div class="hifz-tasmee-details-wrapper">
          <div class="htd-section">
            <div class="row">
              <table class="last-entry-table dataTable table table-bordered" style="border-collapse:collapse;">
                <thead>
                  <tr>
                    <th>Muhafiz ITS</th>
                    <th>Date</th>
                    <th>Last Entry From Surah</th>
                    <th>Last Entry To Surah</th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <td><?php echo $_smarty_tpl->tpl_vars['my_last_entry']->value['muhafiz_its'];?>
</td>
                    <td><?php echo date('d, F Y',strtotime($_smarty_tpl->tpl_vars['my_last_entry']->value['timestamp']));?>
</td>
                    <td><b><span class="al-kanz"><?php echo $_smarty_tpl->tpl_vars['qrn_tasmee']->value->get_surat_name($_smarty_tpl->tpl_vars['my_last_entry']->value['from_surat']);?>
</span> - <?php echo $_smarty_tpl->tpl_vars['my_last_entry']->value['from_ayat'];?>
</b></td>
                    <td><b><span class="al-kanz"><?php echo $_smarty_tpl->tpl_vars['qrn_tasmee']->value->get_surat_name($_smarty_tpl->tpl_vars['my_last_entry']->value['to_surat']);?>
</span> - <?php echo $_smarty_tpl->tpl_vars['my_last_entry']->value['to_ayat'];?>
</b></td>
                  </tr>
                </tbody>
              </table>
              <hr>
              <h2>The selected mumin / mumena did tasmee from <span class="al-kanz"><?php echo $_smarty_tpl->tpl_vars['qrn_tasmee']->value->get_surat_name($_smarty_tpl->tpl_vars['my_last_entry']->value['to_surat']);?>
</span> - <span><<?php echo $_smarty_tpl->tpl_vars['my_last_entry']->value['to_ayat'];?>
</span> till . . .</h2>

              <div class="row">
                <div class="col-xs-12 col-md-5 qrn-tasmee-col-margin al-kanz">
                  <select name="surah" id="surah" class="form-control">
                    <?php $_smarty_tpl->smarty->ext->_subtemplate->render($_smarty_tpl, "file:include/qrn_surat_list.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

                  </select>
                </div>
                <div class="col-xs-12 col-md-5 qrn-tasmee-col-margin">
                  <input type="text" name="ayat_to" class="qrn-text-box col-md-3" id="ayat_to" placeholder="Enter Ayat No" />
                </div>
                <div class="col-xs-12 col-md-2 qrn-tasmee-col-margin">
                <input type="hidden" name="log_id" value="<?php echo $_smarty_tpl->tpl_vars['my_last_entry']->value['id'];?>
">
                <input type="hidden" name="from_surah" value="<?php echo $_smarty_tpl->tpl_vars['my_last_entry']->value['from_surat'];?>
">
                <input type="hidden" name="from_ayat" value="<?php echo $_smarty_tpl->tpl_vars['my_last_entry']->value['from_ayat'];?>
">
                  <button type="submit" name="submit" id="tasmee_log" class="btn btn-primary">Submit</button>
                </div>
              </div>
            </div>
          </div>
          <div class="clearfix"><br></div>
          <?php $_smarty_tpl->smarty->ext->_subtemplate->render($_smarty_tpl, "file:include/qrn_links.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

        </div>
      </div>
    </form>
  </div>
</div>
<?php $_smarty_tpl->smarty->ext->_subtemplate->render($_smarty_tpl, "file:include/js_block.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

<?php $_smarty_tpl->smarty->ext->_subtemplate->render($_smarty_tpl, "file:include/footer.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
}
}
