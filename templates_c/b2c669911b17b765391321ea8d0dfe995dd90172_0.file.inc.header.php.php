<?php
/* Smarty version 3.1.29, created on 2016-02-23 11:15:59
  from "/var/www/html/smarty_tlb2/inc/inc.header.php" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.29',
  'unifunc' => 'content_56cbf217258ac0_09442842',
  'file_dependency' => 
  array (
    'b2c669911b17b765391321ea8d0dfe995dd90172' => 
    array (
      0 => '/var/www/html/smarty_tlb2/inc/inc.header.php',
      1 => 1454650137,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_56cbf217258ac0_09442842 ($_smarty_tpl) {
echo '<?php
';?>//require_once '../classes/constants.php';
require_once './classes/class.qrn_mumin.php';
require_once './classes/class.qrn_muhafiz.php';

$qrn_mumin_data = new Qrn_Mumin();
$qrn_muhafiz_data = new Qrn_Muhafiz();
<?php echo '?>';?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <!-- Meta Basics -->
    <!-- ====================================================================================================== -->
    <meta charset="utf-8" />
    <title>Talabul Ilm</title>
    <meta name="description" content />
    <meta name="keywords" content />
    <meta name="author" content />
    <!-- Mobile Specific Metas -->
    <!-- ====================================================================================================== -->
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
    <!-- Favicons -->
    <!-- ====================================================================================================== -->
    <link rel="shortcut icon" href="<?php echo '<?php ';?>echo SERVER_PATH; <?php echo '?>';?>images/favicons/favicon.png" />
    <link rel="apple-touch-icon" href="<?php echo '<?php ';?>echo SERVER_PATH; <?php echo '?>';?>images/favicons/apple-touch-icon.png" />
    <link rel="apple-touch-icon" sizes="72x72" href="<?php echo '<?php ';?>echo SERVER_PATH; <?php echo '?>';?>images/favicons/apple-touch-icon-72x72.png" />
    <link rel="apple-touch-icon" sizes="114x114" href="<?php echo '<?php ';?>echo SERVER_PATH; <?php echo '?>';?>images/favicons/apple-touch-icon-114x114.png" />
    <!-- Windows Tiles -->
    <!-- ====================================================================================================== -->
    <meta name="application-name" content="Talabul Ilm" />
    <meta name="msapplication-TileColor" content="#ffffff" />
    <meta name="msapplication-square70x70logo" content="<?php echo '<?php ';?>echo SERVER_PATH; <?php echo '?>';?>images/favicons/msapplication-tiny.png" />
    <meta name="msapplication-square150x150logo" content="<?php echo '<?php ';?>echo SERVER_PATH; <?php echo '?>';?>images/favicons/msapplication-square.png" />
    <!-- CSS -->
    <!-- ====================================================================================================== -->
    <link rel="stylesheet" href="<?php echo '<?php ';?>echo SERVER_PATH; <?php echo '?>';?>css/custom.css?v=13" type="text/css" media="screen" />

    <!-- Fallbacks -->
    <!-- ====================================================================================================== -->
    <!--[if lt IE 9]>
    <?php echo '<script'; ?>
 src="<?php echo '<?php ';?>echo SERVER_PATH; <?php echo '?>';?>js/html5shiv.js"><?php echo '</script'; ?>
>
    <![endif]-->

    <?php echo '<script'; ?>
 src="<?php echo '<?php ';?>echo SERVER_PATH; <?php echo '?>';?>js/jquery.min.js"><?php echo '</script'; ?>
>
    <link rel="stylesheet" href="<?php echo '<?php ';?>echo SERVER_PATH; <?php echo '?>';?>css/prettyPhoto.css" type="text/css" media="screen" title="prettyPhoto main stylesheet" charset="utf-8" />
    <?php echo '<script'; ?>
 src="<?php echo '<?php ';?>echo SERVER_PATH; <?php echo '?>';?>js/jquery.prettyPhoto.js" type="text/javascript" charset="utf-8"><?php echo '</script'; ?>
>


    <?php echo '<script'; ?>
 src="<?php echo '<?php ';?>echo SERVER_PATH; <?php echo '?>';?>bootstrap/js/bootstrap.min.js"><?php echo '</script'; ?>
>
    <?php echo '<script'; ?>
 src="<?php echo '<?php ';?>echo SERVER_PATH; <?php echo '?>';?>js/jquery.slicknav.js"><?php echo '</script'; ?>
>
    <?php echo '<script'; ?>
 src="<?php echo '<?php ';?>echo SERVER_PATH; <?php echo '?>';?>js/jquery.nivo.slider.pack.js"><?php echo '</script'; ?>
>
    <?php echo '<script'; ?>
 src="<?php echo '<?php ';?>echo SERVER_PATH; <?php echo '?>';?>js/selectize.min.js"><?php echo '</script'; ?>
>
    <?php echo '<script'; ?>
 src="<?php echo '<?php ';?>echo SERVER_PATH; <?php echo '?>';?>js/scripts.js"><?php echo '</script'; ?>
>
    <?php echo '<script'; ?>
>
      $(document).ready(function () {
        $('[data-toggle="popover"]').popover({
          placement: 'bottom'
        });
        $('body').on('click', function (e) {
          $('[data-toggle="popover"]').each(function () {
            //the 'is' for buttons that trigger popups
            //the 'has' for icons within a button that triggers a popup
            if (!$(this).is(e.target) && $(this).has(e.target).length === 0 && $('.popover').has(e.target).length === 0) {
              $(this).popover('hide');
            }
          });
        });
      });
    <?php echo '</script'; ?>
>
    <link href="<?php echo '<?php ';?>echo SERVER_PATH; <?php echo '?>';?>css/temp_style.css" media="all" rel="stylesheet" type="text/css" />
  </head>

  <body>

    <!-- IE 8 Blocker-->
    <!-- ====================================================================================================== -->
    <!--[if lt IE 9]>
      <div class="ie-old">
        <p class="center"><img alt="" src="<?php echo '<?php ';?>echo SERVER_PATH; <?php echo '?>';?>images/ie-fossil.png" /></p>
        <p class="center bigger1">Your browser is extinct!!</p>
        <p class="center"><span class="color1">This Website is not compatible with Internet Explorer 8 or below. </span></p>
        <p class="center"><span class="color8">Please <a target="_blank" href="http://www.microsoft.com/en-us/download/internet-explorer.aspx"><span class="link_1">upgrade</span></a> your browser or Download 
        <a target="_blank" href="https://www.google.com/intl/en/chrome/browser/"><span class="link_1">Google Chrome</span></a> or <a target="_blank" href="http://www.mozilla.org/en-US/firefox/new/"><span class="link_1">Mozilla Firefox</span></a></span></p>
      </div>
      <![endif]-->

    <!-- Loaders -->
    <!-- ====================================================================================================== -->
    <div id="preloader" style="display: block;">
      <div id="status" style="display: block;">
        <img src="<?php echo '<?php ';?>echo SERVER_PATH; <?php echo '?>';?>images/loader.png" class="animated pulse infinite"/>
        <h2>Loading<br />
          Please Wait...</h2>
      </div>
    </div>
    <!-- Screen Tilt Fallback -->
    <!-- ====================================================================================================== -->
    <div class="tilt-now">
      <h4>Arrgh!<br />
        <small>It seems your screen size is small.<br />
          Hold your device in Potrait Mode to Continue</small><img src="<?php echo '<?php ';?>echo SERVER_PATH; <?php echo '?>';?>images/smart-phone-128.png" class="img-responsive" /></h4>
    </div>

    <!-- Header -->
    <!-- ====================================================================================================== -->
    <div class="header-float">
      <div class="container">
        <nav class="nav"></nav> <!-- do not delete -->
        <header>
          <div class="col-md-4 col-sm-12">
            <a href="<?php echo '<?php ';?>echo SERVER_PATH.'index.php'; <?php echo '?>';?>" class="logo"></a>
          </div>
          <div class="col-md-8 col-sm-12" style="padding:0px;">

            <!-- different columns for responsive setting -->
            <!--            <div class="col-md-5 col-sm-12">
                          <ul class="top-menu">
                            <li><a href="<?php echo '<?php ';?>echo SERVER_PATH; <?php echo '?>';?>" class="active">Home</a></li>
                            <li><a href="<?php echo '<?php ';?>echo SERVER_PATH; <?php echo '?>';?>/about-us/">About Us</a></li>
                          </ul>
                        </div>-->
            <div class="text-right hidden-xs">
              <?php echo '<?php
              ';?>if (isset($_SESSION[USER_LOGGED_IN]) && $_SESSION[USER_LOGGED_IN] == TRUE) {
                $mumin_data_header = $qrn_mumin_data->get_all_mumin($_SESSION[USER_ITS]);
                $muhafiz_data_header = $qrn_muhafiz_data->get_all_muhafiz($_SESSION[USER_ITS]);
                
                if ($muhafiz_data_header) {
                  $link = SERVER_PATH.'qrn_tasmee.php';
                  
                } else if ($mumin_data_header) {
                  $link = SERVER_PATH.'qrn_report_mumin.php';
                  
                } else {
                  $link = SERVER_PATH.'qrn_cover.php';
                }
              <?php echo '?>';?>
              <a href="<?php echo '<?php ';?>echo $link; <?php echo '?>';?>" class="login">Al-Akh al-Qurani</a>
              <?php echo '<?php
                ';?>$my_araz = get_my_jawab_sent_araz_by_its($_SESSION[USER_ITS]);
                $hashcode = get_my_jawab_sent_araz_by_its($_SESSION[USER_ITS], 'code');
                if ($my_araz) {
                  <?php echo '?>';?>
                  <a href="<?php echo '<?php ';?>echo SERVER_PATH; <?php echo '?>';?>araz-preview/<?php echo '<?php ';?>echo $my_araz['id'] <?php echo '?>';?>" class="login">My Araz</a>
                  <?php echo '<?php
                ';?>}
                if ($hashcode) {
                  <?php echo '?>';?>
                  <a target="_blank" href="<?php echo '<?php ';?>echo SERVER_PATH; <?php echo '?>';?>araz-jawab/<?php echo '<?php ';?>echo $hashcode['code']; <?php echo '?>';?>" class="login">My Araz Jawab</a>
                <?php echo '<?php ';?>} <?php echo '?>';?>
                <a href="<?php echo '<?php ';?>echo SERVER_PATH; <?php echo '?>';?>marahil/" class="<?php echo '<?php
                ';?>if (@$page == 'raza-araz') {
                  echo 'visit';
                } else {
                  echo 'login';
                }
                <?php echo '?>';?>">Send Araz To Aqa Maula TUS</a>
                <a href="<?php echo '<?php ';?>echo SERVER_PATH; <?php echo '?>';?>log-out/" class="login">Log out</a>
                <?php echo '<?php
              ';?>} else {
                <?php echo '?>';?>
                <a href="http://www.its52.com/ServiceLogin.aspx?domain=www.talabulilm.com&amp;continue=http://www.talabulilm.com/ITS/Authentication/" class="login">Login</a>
                <?php echo '<?php
              ';?>}
              <?php echo '?>';?>
            </div>
            <nav class="appmenu"></nav> <!-- do not delete -->
            <!-- different columns for responsive setting ends -->
            <div class="clearfix"></div> <!-- do not delete -->

            <?php echo '<?php
            ';?>$menus = get_all_menu();
            <?php echo '?>';?>

            <ul class="app-menu">

              <?php echo '<?php
              ';?>if (isset($_SESSION[USER_LOGGED_IN]) && $_SESSION[USER_LOGGED_IN] == TRUE) {
                foreach ($menus as $menu) {
                  $submenus = get_all_submenu($menu['id']);
                  <?php echo '?>';?>

                  <li class="<?php echo '<?php ';?>if ($submenus != '') { echo 'dropdown'; } <?php echo '?>';?>"><a href="<?php echo '<?php ';?>echo SERVER_PATH.$menu['slug']; <?php echo '?>';?>"><?php echo '<?php ';?>echo $menu['name']; <?php echo '?>';?></a>

                    <?php echo '<?php
                    ';?>if ($submenus != '') {
                      echo '<ul>';
                      foreach ($submenus as $submenu) {
                        if ($menu['slug'] == 'activities') {
                          $sub_submenu = get_article_by_submenu('article', $submenu['id']);
                        } else {
                          $sub_submenu = get_article_by_submenu('events', $submenu['id']);
                        }

                        $third_step_menus = get_all_third_step_menu($submenu['id']);
                        <?php echo '?>';?>

                        <li class="<?php echo '<?php ';?>if ($third_step_menus != '') { echo 'dropdown'; } <?php echo '?>';?>"><a href="<?php echo '<?php ';?>echo SERVER_PATH . $menu['slug'] . '/' . $submenu['slug'] . '/'; <?php echo '?>';?>"><?php echo '<?php ';?>echo $submenu['name']; <?php echo '?>';?></a>

                            <?php echo '<?php
                              ';?>if ($third_step_menus != '') {
                                echo '<ul>';
                                foreach ($third_step_menus as $thirdmenu) {
                                  <?php echo '?>';?>

                                  <li><a href="<?php echo '<?php ';?>echo SERVER_PATH . $menu['slug'] . '/' . $submenu['slug'] . '/' . $thirdmenu['slug'] . '/'; <?php echo '?>';?>"><?php echo '<?php ';?>echo $thirdmenu['name']; <?php echo '?>';?></a></li>

                                <?php echo '<?php
                              ';?>}
                              echo '</ul>';
                            }
                            <?php echo '?>';?>
                        </li>

                      <?php echo '<?php
                    ';?>}
                    echo '</ul>';
                  }
                  <?php echo '?>';?>
                  </li>
                  <?php echo '<?php
                ';?>}
              } else {
                <?php echo '?>';?>
                  <li><a href="http://www.its52.com/ServiceLogin.aspx?domain=www.talabulilm.com&amp;continue=http://www.talabulilm.com/ITS/Authentication/">Click Here to Login</a></li>
              <?php echo '<?php
              ';?>}
              <?php echo '?>';?>
              
              <?php echo '<?php
              ';?>if (isset($_SESSION[USER_LOGGED_IN]) && $_SESSION[USER_LOGGED_IN] == TRUE) {
                $mumin_data_header = $qrn_mumin_data->get_all_mumin($_SESSION[USER_ITS]);
                $muhafiz_data_header = $qrn_muhafiz_data->get_all_muhafiz($_SESSION[USER_ITS]);
                
                if ($muhafiz_data_header) {
                  $link = SERVER_PATH.'qrn_tasmee.php';
                  
                } else if ($mumin_data_header) {
                  $link = SERVER_PATH.'qrn_report_mumin.php';
                  
                } else {
                  $link = SERVER_PATH.'qrn_cover.php';
                }
              <?php echo '?>';?>
                <li class="visible-xs-block"><a href="<?php echo '<?php ';?>echo $link; <?php echo '?>';?>">Al-Akh al-Qurani</a></li>
              <?php echo '<?php
                ';?>$my_araz = get_my_jawab_sent_araz_by_its($_SESSION[USER_ITS]);
                $hashcode = get_my_jawab_sent_araz_by_its($_SESSION[USER_ITS], 'code');
                if ($my_araz) {
                  <?php echo '?>';?>
                <li class="visible-xs-block"><a href="<?php echo '<?php ';?>echo SERVER_PATH; <?php echo '?>';?>my_araz_preview.php?araz=<?php echo '<?php ';?>echo $my_araz['id'] <?php echo '?>';?>">My Araz</a></li>
                  <?php echo '<?php
                ';?>}
                if ($hashcode) {
                  <?php echo '?>';?>
                <li class="visible-xs-block"><a target="_blank" href="<?php echo '<?php ';?>echo SERVER_PATH; <?php echo '?>';?>admin/print_araiz.php?code=<?php echo '<?php ';?>echo $hashcode['code']; <?php echo '?>';?>">My Araz Jawab</a></li>
                <?php echo '<?php ';?>} <?php echo '?>';?>
                <li class="visible-xs-block"><a href="<?php echo '<?php ';?>echo SERVER_PATH; <?php echo '?>';?>marahil/">Send Araz To Aqa Maula TUS</a></li>
                <li class="visible-xs-block"><a href="<?php echo '<?php ';?>echo SERVER_PATH; <?php echo '?>';?>log-out/">Log out</a></li>
                <?php echo '<?php
              ';?>} else {
                <?php echo '?>';?>
                <li class="visible-xs-block"><a href="http://www.its52.com/ServiceLogin.aspx?domain=www.talabulilm.com&amp;continue=http://www.talabulilm.com/ITS/Authentication/">Login</a></li>
                <?php echo '<?php
              ';?>}
              <?php echo '?>';?>
            </ul>
          </div>
        </header>
      </div>
    </div>
    <div class="clearfix"></div> <!-- do not delete -->

    <!-- Banners -->
    <!-- ====================================================================================================== -->
    <?php echo '<?php
      ';?>$images_carousal = get_carousal_images_by_category(HOME_CAROUSAL);
    <?php echo '?>';?>
    <div class="container white-bg">
      <div class="banners">
        <div id="banners" class="nivoSlider">
          <?php echo '<?php
            ';?>if ($images_carousal) {
              foreach ($images_carousal as $image) {
          <?php echo '?>';?>
            <a href="<?php echo '<?php 
            ';?>if (!isset($_SESSION[USER_LOGGED_IN]) || $_SESSION[USER_LOGGED_IN] == FALSE) {
              echo 'http://www.its52.com/ServiceLogin.aspx?domain=www.talabulilm.com&amp;continue=http://www.talabulilm.com/ITS/Authentication/';
            } else {
              echo $image['link'];
            }
            <?php echo '?>';?>"><img src="<?php echo '<?php ';?>echo SERVER_PATH.'images/banners/'.$image['image']; <?php echo '?>';?>" alt="" title="#htmlcaption2"/></a>
          <?php echo '<?php
              ';?>}
            }
          <?php echo '?>';?>
        </div>
      </div>
    </div>
    <div class="clearfix"></div> <!-- do not delete -->
<?php }
}
