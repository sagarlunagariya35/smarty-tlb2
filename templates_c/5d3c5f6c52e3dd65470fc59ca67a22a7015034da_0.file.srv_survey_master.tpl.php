<?php
/* Smarty version 3.1.29, created on 2016-03-25 14:16:48
  from "/var/www/html/smarty_tlb2/templates/srv_survey_master.tpl" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.29',
  'unifunc' => 'content_56f4faf8a3ec61_50463354',
  'file_dependency' => 
  array (
    '5d3c5f6c52e3dd65470fc59ca67a22a7015034da' => 
    array (
      0 => '/var/www/html/smarty_tlb2/templates/srv_survey_master.tpl',
      1 => 1458895607,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:include/js_block.tpl' => 1,
    'file:include/footer.tpl' => 1,
  ),
),false)) {
function content_56f4faf8a3ec61_50463354 ($_smarty_tpl) {
?>
<div class="container white-bg">
  <div class="col-md-12 col-sm-12">
    <div class="page-title">
      <p style="margin-top:5px;"><a href="#">Home</a> / <a href="#">Survey</a></p>
      <h1>Survey</h1>
    </div>
  </div>
  <div class="clearfix"></div> <!-- do not delete -->
  <div class="col-md-12 col-sm-12">
    <div class="page">
      <form class="form1 white" method="post" action="">

        <div class="clearfix"></div> <!-- do not delete -->  
        <div class="row">
          <div class="col-md-3 col-xs-12 hidden-xs">
            <div class="profile-box-static">
              <h3 class="uppercase text-center">List of Surveys</h3>
            </div>
            <div class="profile-box-static-bottom">
              <?php if ($_smarty_tpl->tpl_vars['surveys']->value) {?>
                <?php
$_from = $_smarty_tpl->tpl_vars['surveys']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_srv_0_saved_item = isset($_smarty_tpl->tpl_vars['srv']) ? $_smarty_tpl->tpl_vars['srv'] : false;
$_smarty_tpl->tpl_vars['srv'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['srv']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['srv']->value) {
$_smarty_tpl->tpl_vars['srv']->_loop = true;
$__foreach_srv_0_saved_local_item = $_smarty_tpl->tpl_vars['srv'];
?>
                  <a href="<?php echo SERVER_PATH;?>
srv_take_survey.php?survey_id=<?php echo $_smarty_tpl->tpl_vars['srv']->value['survey_id'];?>
">» <?php echo $_smarty_tpl->tpl_vars['srv']->value['title'];?>
<hr></a>
                <?php
$_smarty_tpl->tpl_vars['srv'] = $__foreach_srv_0_saved_local_item;
}
if ($__foreach_srv_0_saved_item) {
$_smarty_tpl->tpl_vars['srv'] = $__foreach_srv_0_saved_item;
}
?>
              <?php }?>
            </div>
          </div>
          <div class="col-md-9 col-xs-12" >
            <div class="col-xs-12">&nbsp;</div>
            <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
              <?php if ($_smarty_tpl->tpl_vars['surveys']->value) {?>
                <?php $_smarty_tpl->tpl_vars["i"] = new Smarty_Variable(1, null);
$_smarty_tpl->ext->_updateScope->updateScope($_smarty_tpl, "i", 0);?>
                  <?php
$_from = $_smarty_tpl->tpl_vars['surveys']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_srv_1_saved_item = isset($_smarty_tpl->tpl_vars['srv']) ? $_smarty_tpl->tpl_vars['srv'] : false;
$_smarty_tpl->tpl_vars['srv'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['srv']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['srv']->value) {
$_smarty_tpl->tpl_vars['srv']->_loop = true;
$__foreach_srv_1_saved_local_item = $_smarty_tpl->tpl_vars['srv'];
?>
                    <?php $_smarty_tpl->tpl_vars["i"] = new Smarty_Variable($_smarty_tpl->tpl_vars['i']->value+1, null);
$_smarty_tpl->ext->_updateScope->updateScope($_smarty_tpl, "i", 0);?>
                    <div class="panel panel-default">
                      <div class="panel-heading" role="tab" id="headingOne">
                        <h4 class="panel-title">
                          <a href="<?php echo SERVER_PATH;?>
srv_take_survey.php?survey_id=<?php echo $_smarty_tpl->tpl_vars['srv']->value['survey_id'];?>
">
                            <?php echo $_smarty_tpl->tpl_vars['srv']->value['title'];?>

                          </a>
                        </h4>
                      </div>
                  </div>
                <?php
$_smarty_tpl->tpl_vars['srv'] = $__foreach_srv_1_saved_local_item;
}
if ($__foreach_srv_1_saved_item) {
$_smarty_tpl->tpl_vars['srv'] = $__foreach_srv_1_saved_item;
}
?>
              <?php }?>
            </div>
          </div>
        </div>
      </form>
    </div>
  </div>
</div>
<style>
  .blue-box1-level a {
    color: #FFF !important;
    text-decoration: none;
  }
  .progress {
    color: #000;
    font-size: 12px;
    line-height: 20px;
    text-align: center;
  }
</style>
<?php $_smarty_tpl->smarty->ext->_subtemplate->render($_smarty_tpl, "file:include/js_block.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

<?php $_smarty_tpl->smarty->ext->_subtemplate->render($_smarty_tpl, "file:include/footer.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
}
}
