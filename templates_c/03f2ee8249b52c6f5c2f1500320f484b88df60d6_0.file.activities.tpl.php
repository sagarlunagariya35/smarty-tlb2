<?php
/* Smarty version 3.1.29, created on 2016-04-02 11:27:19
  from "/var/www/html/smarty_tlb2/templates/activities.tpl" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.29',
  'unifunc' => 'content_56ff5f3fdb0437_27267870',
  'file_dependency' => 
  array (
    '03f2ee8249b52c6f5c2f1500320f484b88df60d6' => 
    array (
      0 => '/var/www/html/smarty_tlb2/templates/activities.tpl',
      1 => 1459317942,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:include/js_block.tpl' => 1,
    'file:include/footer.tpl' => 1,
  ),
),false)) {
function content_56ff5f3fdb0437_27267870 ($_smarty_tpl) {
if (!is_callable('smarty_function_counter')) require_once '/var/www/html/smarty_tlb2/smarty/libs/plugins/function.counter.php';
?>
<div class="container white-bg">
  <div class="col-md-12 col-sm-12 bot-mgn20">
    <div class="page-title">
      <p style="margin-top:5px;"><a href="#">Home</a> / <a href="#"><?php echo $_smarty_tpl->tpl_vars['year']->value;?>
 <?php echo $_smarty_tpl->tpl_vars['article']->value['article_title'];?>
</a></p>
      <h1>Gallery - <?php echo $_smarty_tpl->tpl_vars['year']->value;?>
 <?php echo $_smarty_tpl->tpl_vars['article']->value['article_title'];?>
</h1>
    </div>
  </div>
  <div class="clearfix"></div> <!-- do not delete -->

  <div class="col-md-3 col-sm-12 hidden-xs">
    <div class="profile-box-static">
      <h3 class="uppercase text-center">Activities - <?php echo $_smarty_tpl->tpl_vars['year']->value;?>
</h3>
    </div>
    <div class="profile-box-static-bottom">
      <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
        
        <?php if ($_smarty_tpl->tpl_vars['all_categories']->value) {?>
          <?php echo smarty_function_counter(array('start'=>0,'print'=>false),$_smarty_tpl);?>

          <?php
$_from = $_smarty_tpl->tpl_vars['all_categories']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_ac_0_saved_item = isset($_smarty_tpl->tpl_vars['ac']) ? $_smarty_tpl->tpl_vars['ac'] : false;
$_smarty_tpl->tpl_vars['ac'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['ac']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['ac']->value) {
$_smarty_tpl->tpl_vars['ac']->_loop = true;
$__foreach_ac_0_saved_local_item = $_smarty_tpl->tpl_vars['ac'];
?>
            <?php $_smarty_tpl->tpl_vars['cat_articles'] = new Smarty_Variable(get_article_by_submenu('article',$_smarty_tpl->tpl_vars['menu_id']->value[0]['id'],$_smarty_tpl->tpl_vars['ac']->value['category']), null);
$_smarty_tpl->ext->_updateScope->updateScope($_smarty_tpl, 'cat_articles', 0);?>
              <div class="panel panel-default" onclick="get_articles('<?php echo $_smarty_tpl->tpl_vars['ac']->value['category'];?>
')">
                <div class="panel-heading" role="tab" id="headingOne">
                  <h4 class="panel-title">
                    <a role="button" data-toggle="collapse" data-parent="#accordion" href="#<?php echo smarty_function_counter(array(),$_smarty_tpl);?>
" aria-expanded="true" aria-controls="collapseOne">
                      <?php echo $_smarty_tpl->tpl_vars['ac']->value['category'];?>

                    </a>
                  </h4>
                </div>
                <div id="<?php echo smarty_function_counter(array(),$_smarty_tpl);?>
" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
                  <div class="panel-body">
                    
                      <?php if ($_smarty_tpl->tpl_vars['cat_articles']->value) {?>
                        <?php
$_from = $_smarty_tpl->tpl_vars['cat_articles']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_cat_a_1_saved_item = isset($_smarty_tpl->tpl_vars['cat_a']) ? $_smarty_tpl->tpl_vars['cat_a'] : false;
$_smarty_tpl->tpl_vars['cat_a'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['cat_a']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['cat_a']->value) {
$_smarty_tpl->tpl_vars['cat_a']->_loop = true;
$__foreach_cat_a_1_saved_local_item = $_smarty_tpl->tpl_vars['cat_a'];
?>
                          <a href="<?php echo $_smarty_tpl->tpl_vars['server_path']->value;?>
activities/<?php echo $_smarty_tpl->tpl_vars['year']->value;?>
/<?php echo $_smarty_tpl->tpl_vars['cat_a']->value['slug'];?>
/<?php echo $_smarty_tpl->tpl_vars['cat_a']->value['id'];?>
/">» <?php echo $_smarty_tpl->tpl_vars['cat_a']->value['article_title'];?>
<hr></a>
                        <?php
$_smarty_tpl->tpl_vars['cat_a'] = $__foreach_cat_a_1_saved_local_item;
}
if ($__foreach_cat_a_1_saved_item) {
$_smarty_tpl->tpl_vars['cat_a'] = $__foreach_cat_a_1_saved_item;
}
?>
                      <?php }?>
                  </div>
                </div>
              </div>
          <?php
$_smarty_tpl->tpl_vars['ac'] = $__foreach_ac_0_saved_local_item;
}
if ($__foreach_ac_0_saved_item) {
$_smarty_tpl->tpl_vars['ac'] = $__foreach_ac_0_saved_item;
}
?>
        <?php }?>              
      </div>
    </div>
  </div>


  
  <?php if ($_smarty_tpl->tpl_vars['article']->value) {?>
    <div class="col-md-8 col-sm-12">
      <h2><span class="label label-primary lsd"><?php echo $_smarty_tpl->tpl_vars['year']->value;?>
 <?php echo $_smarty_tpl->tpl_vars['article']->value['article_title'];?>
</span></h2>
      <br>
      <span>
        <?php echo $_smarty_tpl->tpl_vars['article']->value['text'];?>

      </span><br>

      <?php if ($_smarty_tpl->tpl_vars['videos']->value) {?>
        <br><h2><span class="label label-success">Videos - <?php echo $_smarty_tpl->tpl_vars['year']->value;?>
 <?php echo $_smarty_tpl->tpl_vars['article']->value['article_title'];?>
</span></h2><br>

        <div style="display:none;" class="html5gallery" data-skin="horizontal" data-width="480" data-height="272" data-resizemode="fill">
          
          <?php
$_from = $_smarty_tpl->tpl_vars['videos']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_video_2_saved_item = isset($_smarty_tpl->tpl_vars['video']) ? $_smarty_tpl->tpl_vars['video'] : false;
$_smarty_tpl->tpl_vars['video'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['video']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['video']->value) {
$_smarty_tpl->tpl_vars['video']->_loop = true;
$__foreach_video_2_saved_local_item = $_smarty_tpl->tpl_vars['video'];
?>
            <a href="<?php echo $_smarty_tpl->tpl_vars['server_path']->value;?>
upload/video/<?php echo $_smarty_tpl->tpl_vars['video']->value['video_url'];?>
"><img src="
              <?php if ($_smarty_tpl->tpl_vars['video']->value['video_thumb'] != '') {?>
                <?php echo $_smarty_tpl->tpl_vars['server_path']->value;?>
upload/articles/video/images/<?php echo $_smarty_tpl->tpl_vars['video']->value['video_thumb'];?>

              <?php } else { ?>
                 <?php echo $_smarty_tpl->tpl_vars['server_path']->value;?>
images/flash-logo.png
              <?php }?>
              "  title=""></a>
          <?php
$_smarty_tpl->tpl_vars['video'] = $__foreach_video_2_saved_local_item;
}
if ($__foreach_video_2_saved_item) {
$_smarty_tpl->tpl_vars['video'] = $__foreach_video_2_saved_item;
}
?>

        </div>
      <?php }?>
        
      <?php if ($_smarty_tpl->tpl_vars['audios']->value) {?>
        <br><h2><span class="label label-danger">Audios - <?php echo $_smarty_tpl->tpl_vars['year']->value;?>
 <?php echo $_smarty_tpl->tpl_vars['article']->value['article_title'];?>
</span></h2><br>

        <div id="jquery_jplayer_1" class="jp-jplayer"></div>
        <div id="jp_container_1" class="jp-audio" role="application" aria-label="media player">
          <div class="jp-type-playlist">
            <div class="jp-gui jp-interface">
              <div class="jp-controls">
                <button class="jp-previous" role="button" tabindex="0">previous</button>
                <button class="jp-play" role="button" tabindex="0">play</button>
                <button class="jp-next" role="button" tabindex="0">next</button>
                <button class="jp-stop" role="button" tabindex="0">stop</button>
              </div>
              <div class="jp-progress">
                <div class="jp-seek-bar">
                  <div class="jp-play-bar"></div>
                </div>
              </div>
              <div class="jp-volume-controls">
                <button class="jp-mute" role="button" tabindex="0">mute</button>
                <button class="jp-volume-max" role="button" tabindex="0">max volume</button>
                <div class="jp-volume-bar">
                  <div class="jp-volume-bar-value"></div>
                </div>
              </div>
              <div class="jp-time-holder">
                <div class="jp-current-time" role="timer" aria-label="time">&nbsp;</div>
                <div class="jp-duration" role="timer" aria-label="duration">&nbsp;</div>
              </div>
              <div class="jp-toggles">
                <button class="jp-repeat" role="button" tabindex="0">repeat</button>
                <button class="jp-shuffle" role="button" tabindex="0">shuffle</button>
              </div>
            </div>
            <div class="jp-playlist">
              <ul>
                <li>&nbsp;</li>
              </ul>
            </div>
            <div class="jp-no-solution">
              <span>Update Required</span>
              To play the media you will need to either update your browser to a recent version or update your <a href="http://get.adobe.com/flashplayer/" target="_blank">Flash plugin</a>.
            </div>
          </div>
        </div>
      <?php }?>

      <?php if ($_smarty_tpl->tpl_vars['images']->value) {?>
        <br><h2><span class="label label-info">Images - <?php echo $_smarty_tpl->tpl_vars['year']->value;?>
 <?php echo $_smarty_tpl->tpl_vars['article']->value['article_title'];?>
</span></h2><br>
        <ul class="gallery">
          <?php
$_from = $_smarty_tpl->tpl_vars['images']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_image_3_saved_item = isset($_smarty_tpl->tpl_vars['image']) ? $_smarty_tpl->tpl_vars['image'] : false;
$_smarty_tpl->tpl_vars['image'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['image']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['image']->value) {
$_smarty_tpl->tpl_vars['image']->_loop = true;
$__foreach_image_3_saved_local_item = $_smarty_tpl->tpl_vars['image'];
?>
            <li><a href="<?php echo $_smarty_tpl->tpl_vars['server_path']->value;?>
upload/articles/image/<?php echo $_smarty_tpl->tpl_vars['image']->value['image_url'];?>
" rel="prettyPhoto[gallery1]"><img src="<?php echo $_smarty_tpl->tpl_vars['server_path']->value;?>
upload/articles/image/<?php echo $_smarty_tpl->tpl_vars['image']->value['image_url'];?>
" alt=""></a></li>
          <?php
$_smarty_tpl->tpl_vars['image'] = $__foreach_image_3_saved_local_item;
}
if ($__foreach_image_3_saved_item) {
$_smarty_tpl->tpl_vars['image'] = $__foreach_image_3_saved_item;
}
?>
        </ul>
      <?php }?>
      <br>
    </div>
  <?php } else { ?>
    <div class="col-md-8 col-sm-12">
      <?php
$_from = $_smarty_tpl->tpl_vars['all_articles']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_art_4_saved_item = isset($_smarty_tpl->tpl_vars['art']) ? $_smarty_tpl->tpl_vars['art'] : false;
$_smarty_tpl->tpl_vars['art'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['art']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['art']->value) {
$_smarty_tpl->tpl_vars['art']->_loop = true;
$__foreach_art_4_saved_local_item = $_smarty_tpl->tpl_vars['art'];
?>
        <?php $_smarty_tpl->tpl_vars['img_art'] = new Smarty_Variable(get_first_image_from_article($_smarty_tpl->tpl_vars['art']->value['id']), null);
$_smarty_tpl->ext->_updateScope->updateScope($_smarty_tpl, 'img_art', 0);?>
        
        <a href="<?php echo $_smarty_tpl->tpl_vars['server_path']->value;?>
activities/<?php echo $_smarty_tpl->tpl_vars['year']->value;?>
/<?php echo $_smarty_tpl->tpl_vars['art']->value['slug'];?>
/<?php echo $_smarty_tpl->tpl_vars['art']->value['id'];?>
/">
          <div class="col-sm-3 col-xs-12 boxes-<?php echo $_smarty_tpl->tpl_vars['art']->value['category'];?>
">
            <div class="istibsaar-boxes text-center">
              <img src="<?php echo $_smarty_tpl->tpl_vars['server_path']->value;?>
upload/articles/image/<?php echo $_smarty_tpl->tpl_vars['img_art']->value['image_url'];?>
" class="img-responsive img-mts" style="border: 1px solid #09386c">
              <div>
                <p class="text-center"><?php echo $_smarty_tpl->tpl_vars['art']->value['article_title'];?>
</p>
                <p>&nbsp;</p>
                <p class="text-center" style="color:#000;"><?php echo $_smarty_tpl->tpl_vars['art']->value['desc'];?>
</p>
              </div>
            </div>
          </div>
        </a>
      <?php
$_smarty_tpl->tpl_vars['art'] = $__foreach_art_4_saved_local_item;
}
if ($__foreach_art_4_saved_item) {
$_smarty_tpl->tpl_vars['art'] = $__foreach_art_4_saved_item;
}
?>
    </div>
  <?php }?>
</div>

<?php $_smarty_tpl->smarty->ext->_subtemplate->render($_smarty_tpl, "file:include/js_block.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>


<?php echo '<script'; ?>
 type="text/javascript" src="<?php echo $_smarty_tpl->tpl_vars['server_path']->value;?>
html5gallery/html5gallery.js"><?php echo '</script'; ?>
>

<link href="<?php echo $_smarty_tpl->tpl_vars['server_path']->value;?>
dist/skin/blue.monday/css/jplayer.blue.monday.min.css" rel="stylesheet" type="text/css" />
<?php echo '<script'; ?>
 type="text/javascript" src="<?php echo $_smarty_tpl->tpl_vars['server_path']->value;?>
dist/jplayer/jquery.jplayer.min.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 type="text/javascript" src="<?php echo $_smarty_tpl->tpl_vars['server_path']->value;?>
dist/add-on/jplayer.playlist.min.js"><?php echo '</script'; ?>
>

<?php echo '<script'; ?>
 type="text/javascript">
//<![CDATA[
  $(document).ready(function () {

    new jPlayerPlaylist({
    jPlayer: "#jquery_jplayer_1",
            cssSelectorAncestor: "#jp_container_1"
    }, [

    <?php if ($_smarty_tpl->tpl_vars['audios']->value) {?>
      <?php
$_from = $_smarty_tpl->tpl_vars['audios']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_audio_5_saved_item = isset($_smarty_tpl->tpl_vars['audio']) ? $_smarty_tpl->tpl_vars['audio'] : false;
$_smarty_tpl->tpl_vars['audio'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['audio']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['audio']->value) {
$_smarty_tpl->tpl_vars['audio']->_loop = true;
$__foreach_audio_5_saved_local_item = $_smarty_tpl->tpl_vars['audio'];
?>
            {
            title:"<?php echo $_smarty_tpl->tpl_vars['audio']->value['audio_url'];?>
",
                    mp3: "<?php echo $_smarty_tpl->tpl_vars['server_path']->value;?>
upload/articles/audio/<?php echo $_smarty_tpl->tpl_vars['audio']->value['audio_url'];?>
",
            }
        <?php if ((count($_smarty_tpl->tpl_vars['audios']->value)-1)) {?>
          <?php echo ',';?>

        <?php }?>
      <?php
$_smarty_tpl->tpl_vars['audio'] = $__foreach_audio_5_saved_local_item;
}
if ($__foreach_audio_5_saved_item) {
$_smarty_tpl->tpl_vars['audio'] = $__foreach_audio_5_saved_item;
}
?>
    <?php }?>

    ], {
    swfPath: "../../dist/jplayer",
            supplied: "oga, mp3",
            wmode: "window",
            useStateClassSkin: true,
            autoBlur: false,
            smoothPlayBar: true,
            keyEnabled: true
    });
  });
//]]>
<?php echo '</script'; ?>
>

<?php echo '<script'; ?>
 type="text/javascript" charset="utf-8">
  $(document).ready(function () {
    $("area[rel^='prettyPhoto']").prettyPhoto();

    $(".gallery:first a[rel^='prettyPhoto']").prettyPhoto({
      animation_speed: 'normal', theme: 'light_square', slideshow: 5000, autoplay_slideshow: true
    });
    
    $(".gallery:gt(0) a[rel^='prettyPhoto']").prettyPhoto({
      animation_speed: 'normal', slideshow: 10000, hideflash: true
    });

    $("#custom_content a[rel^='prettyPhoto']:first").prettyPhoto({
      custom_markup: '<div id="map_canvas" style="width:260px; height:265px"></div>',
      changepicturecallback: function () {
        initialize();
      }
    });

    $("#custom_content a[rel^='prettyPhoto']:last").prettyPhoto({
      custom_markup: '<div id="bsap_1259344" class="bsarocks bsap_d49a0984d0f377271ccbf01a33f2b6d6"></div><div id="bsap_1237859" class="bsarocks bsap_d49a0984d0f377271ccbf01a33f2b6d6" style="height:260px"></div><div id="bsap_1251710" class="bsarocks bsap_d49a0984d0f377271ccbf01a33f2b6d6"></div>',
      changepicturecallback: function () {
        _bsap.exec();
      }
    });
  });
  
  function get_articles(str){
    $('.boxes-' + str).hide();
  }
<?php echo '</script'; ?>
>

<style>

  .actv{
    background-color:#cf4914;
    display:block;
    font-family: 'Lato', sans-serif;
    font-size:14px;
    padding:10px;
    text-align:center;
    color:white;
    text-transform:uppercase;
    font-weight:600;

  }

  .ntactv{
    display:block;
    font-family: 'Lato', sans-serif;
    font-size:14px;
    text-align:center;
    color:white;
    text-transform:uppercase;
    font-weight:600;

  }

  .profile-box1 a{
    color: white;
    padding: 5px;
  }

  .gallery {
    clear: both;
    display: table;
    list-style: none;
    padding: 0;
    margin: 0;
    margin-bottom: 20px;
  }
  .gallery li {
    float: left;
    margin-bottom: 7px;
    margin-right: 7px;
    height: 100px;
    width: 100px;
    overflow: hidden;
    text-align: center;
    position: relative;
    padding: inherit;
  }
  .gallery li:last-child {
    margin-right: 0;
  }
  .gallery li a {
    position: relative;
    left: 100%;
    margin-left: -200%;
    position: relative;
  }
  .gallery li a:after {
    text-shadow: none;
    -webkit-font-smoothing: antialiased;
    font-family: 'fontawesome';
    speak: none;
    font-weight: normal;
    font-variant: normal;
    line-height: 1;
    text-transform: none;
    -moz-transition: 0.6s;
    -o-transition: 0.6s;
    -webkit-transition: 0.6s;
    transition: 0.6s;
    filter: progid:DXImageTransform.Microsoft.Alpha(Opacity=0);
    opacity: 0;
    -moz-border-radius: 50%;
    -webkit-border-radius: 50%;
    border-radius: 50%;
    color: #fff;
    content: "\f06e";
    display: inline-block;
    font-size: 16px;
    position: absolute;
    width: 30px;
    height: 30px;
    top: 0;
    left: 0;
    right: 0;
    bottom: 0;
    margin: auto;
    background-color: #252525;
    padding-top: 7px;
    margin-top: -7px;
  }
  .gallery li a:hover img {
    -moz-transform: scale(1.08, 1.08);
    -ms-transform: scale(1.08, 1.08);
    -webkit-transform: scale(1.08, 1.08);
    transform: scale(1.08, 1.08);
  }
  .gallery li a img {
    -moz-transition: 0.3s;
    -o-transition: 0.3s;
    -webkit-transition: 0.3s;
    transition: 0.3s;
    height: 100%;
    width: auto;
  }
</style>
<?php $_smarty_tpl->smarty->ext->_subtemplate->render($_smarty_tpl, "file:include/footer.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

<?php }
}
