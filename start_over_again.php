<?php
require_once 'session.php';
require_once 'requires_login.php';
require_once 'classes/class.user.php';
require_once 'classes/class.araz.php';

$delete_save_araz_data = delete_save_araz_data($_SESSION[MARHALA_ID], $_SESSION[USER_ITS]);
$delete_temp_data = delete_temp_institute_data($_SESSION[USER_ITS]);

if($_SESSION['madrasah_darajah']){ unset($_SESSION['madrasah_darajah']); }
if(@$_SESSION['madrasah_country']){ unset($_SESSION['madrasah_country']); }
if($_SESSION['madrasah_city']){ unset($_SESSION['madrasah_city']); }
if($_SESSION['madrasah_name']){ unset($_SESSION['madrasah_name']); }
if($_SESSION['madrasah_name_ar']){ unset($_SESSION['madrasah_name_ar']); }
if($_SESSION['hifz_sanad']){ unset($_SESSION['hifz_sanad']); }
if(@$_SESSION['school_city']){ unset($_SESSION['school_city']); }
if(@$_SESSION['school_standard']){ unset($_SESSION['school_standard']); }
if(@$_SESSION['school_name']){ unset($_SESSION['school_name']); }
if(@$_SESSION['school_filter']){ unset($_SESSION['school_filter']); }
if(@$_SESSION['school_pincode']){ unset($_SESSION['school_pincode']); }
if(@$_SESSION['ques']){ unset($_SESSION['ques']); }
if($_SESSION['is_institute_selected']){ unset($_SESSION['is_institute_selected']); }
if($_SESSION['taken_psychometric_aptitude_test']){ unset($_SESSION['taken_psychometric_aptitude_test']); }
if($_SESSION['text_psychometric_aptitude']){ unset($_SESSION['text_psychometric_aptitude']); }
if($_SESSION['prefer_more_clarity']){ unset($_SESSION['prefer_more_clarity']); }
if($_SESSION['want_further_guidence']){ unset($_SESSION['want_further_guidence']); }
if($_SESSION['seek_funding_edu']){ unset($_SESSION['seek_funding_edu']); }
if($_SESSION['want_assistance']){ unset($_SESSION['want_assistance']); }
if($_SESSION['received_sponsorship']){ unset($_SESSION['received_sponsorship']); }
if($_SESSION['follow_shariaa']){ unset($_SESSION['follow_shariaa']); }
if($_SESSION['ques']){ unset($_SESSION['ques']); }
if($_SESSION['disclaimer']){ unset($_SESSION['disclaimer']); }

header('location: ' . SERVER_PATH . 'marahil/');
?>