<?php
require_once 'session.php';
require_once 'requires_login.php';
require_once 'classes/gen_functions.php';

$degree = $_GET["degree"];
$group_id = $_SESSION['marhala_group'];

$courses = array_sort(get_courses_by_degree($degree, $group_id),'course_name',SORT_ASC);

echo '<select name="course_name" class="select-input">
        <option value="">Course</option>';
      if($courses)
      {
        foreach ($courses as $c) 
        {
          if($c['course_name'] != ''){
  ?>
          <option value="<?php echo $c['course_name']; ?>"><?php echo $c['course_name']; ?></option>
  <?php
          }
        }
      }
  echo '</select>';
?>