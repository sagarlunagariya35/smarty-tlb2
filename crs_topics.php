<?php
require 'smarty/libs/Smarty.class.php';
require_once 'session.php';
require_once 'requires_login.php';
require_once 'classes/class.user.php';
require_once 'classes/class.crs_topic.php';

$user_logedin = new mtx_user();
$crs_topic = new Crs_Topic();

if (isset($_REQUEST['topic_id'])) {
  $topic_id = $_REQUEST['topic_id'];
}

$topics_list = $crs_topic->get_all_topics();
$topic_details = $crs_topic->get_topic($topic_id);
$sub_topics_of_topic = $crs_topic->get_all_topics($topic_id);

require_once 'inc/inc.header2.php';
require_once 'inc/inc.footer.php';

$smarty = new Smarty;

// Header / Session variables
$smarty->assign("server_path", SERVER_PATH);
$smarty->assign("crs_topic", $crs_topic);
$smarty->assign("topics", $topics_list);
$smarty->assign("topic_details", $topic_details);
$smarty->assign("sub_topics_of_topic", $sub_topics_of_topic);

$smarty->display('crs_topics.tpl');