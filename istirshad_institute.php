<?php
require 'smarty/libs/Smarty.class.php';
require_once 'session.php';
require_once 'requires_login.php';
require_once 'classes/class.user.php';
// Page body class
$body_class = 'page-sub-page';

$user = new mtx_user;
$user->loaduser($_SESSION[USER_ITS]);

$user_its = $_SESSION[USER_ITS];
$group_id = $_GET['group'];
$success_message = $course_exist = FALSE;

$_SESSION['marhala_group'] = $group_id;

if(!@$_SESSION['generate_course_key']){
  $_SESSION['generate_course_key'] = '';
}

$marhala_school_array = array('5', '6', '7', '8');
if (in_array($group_id, $marhala_school_array)) {
  if ($_SESSION['step'] != '4') {
    if ($_SESSION['step'] <= '4') {
      if ($_SESSION['step'] != '2') {
        header('location: ' . SERVER_PATH . 'marhala-' . $group_id . '/' . get_marhala_slug($group_id) . '/choose-stream/');
      }
    }
  }
} else {
  header('location: ' . SERVER_PATH . 'marhala-' . $group_id . '/' . get_marhala_slug($group_id) . '/choose-stream/');
}

switch ($group_id) {
  case 5:
    $panel_heading = 'Std 11th - 12th';
    $panel_heading_ar = 'الثانوية الثانية';
    $ary_standard = array('11th', '12th');
    $kalemaat_nooraniyah = ' تعليم نا ضمن ما زبان ني اهمية چھے، لسان الدعوة سگلا بولے انے فرزندو نے سكھاوے';
    $sub_title = "Stream";
    break;
  case 6:
    $panel_heading = 'Graduation';
    $panel_heading_ar = 'البكالوريا';
    $ary_standard = array();
    $kalemaat_nooraniyah = 'قوم  انے وطن ني بهبودگي واسطے هر ممكن سعي كرے';
    $sub_title = "Course";
    break;
  case 7:
    $panel_heading = 'Post Graduation';
    $panel_heading_ar = 'الماجيستار';
    $ary_standard = array();
    $kalemaat_nooraniyah = 'ويپار واسطے  تعليم،  ويپار برابر كرو، دنيا ما ويپاري قوم اپن مشهور  چھے';
    $sub_title = "Course";
    break;
  case 8:
    $panel_heading = 'Diploma';
    $panel_heading_ar = 'الدبلوم';
    $ary_standard = array();
    $kalemaat_nooraniyah = 'ويپار كيم كرو تھ سيكھي لو، اهنا واسطے  گهنو كروو جوئيے، degree  سي كام ليوائي';
    $sub_title = "Course";
    break;
}
$_SESSION[MARHALA_ID] = $group_id;
$show_next = TRUE;  // trigger to verify if the araz type is raza or istirshad

if (isset($_POST['submit'])) {
  $course_name = $_POST['course_name'];
  $institute_name = $db->clean_data($_POST['institute_name']);
  $institute_country = $_POST['institute_country'];
  $institute_city = $_POST['madrasah_city'];
  $accomodation = $_POST['accomodation'];
  $course_duration = $_POST['course_duration'];
  $course_started = $_POST['course_start_month'] . '-' . $_POST['course_start_year'];
  
  if($course_name != ''){
    if ($group_id != '5') {
      $course_exist = check_finalize_courses_exist($course_name);
    }

    if ($course_exist == TRUE OR $group_id == '5') {
      
      $base_string = $course_name.', '.$institute_name.', '.$institute_country.', '.$institute_city.', '.$accomodation.', '.$course_duration.', '.$course_started;
      $rand_key = md5($base_string);

      if($_SESSION['generate_course_key'] == ''){
        $_SESSION['generate_course_key'] = $rand_key;
        $rand_key = FALSE;
      }else {
        if($_SESSION['generate_course_key'] != $rand_key)
        {
          $_SESSION['generate_course_key'] = $rand_key;
          $rand_key = FALSE;
        }
      }

      if (@$_SESSION['is_institute_selected'] == '') {
        $_SESSION['is_institute_selected'] = $_POST['is_institute_selected'];
      }

      $_SESSION['step'] = '5';
      $com_subject = FALSE;

      if($_SESSION['generate_course_key'] == $rand_key){
        $_SESSION[ERROR_MESSAGE] = 'This Course is Already in List.<br> Please Add another choice or scroll down and proceed.';
      }else {
        $result = insert_temp_institute_data($group_id, $user_its, FALSE, $course_name, $institute_name, $institute_country, $institute_city, $accomodation, $course_duration, $course_started, $com_subject);

        ($result && $_SESSION['is_institute_selected'] == '2') ? $_SESSION[SUCCESS_MESSAGE] = '<strong>Istirshad</strong><br>Add another choice or scroll down and proceed.' : '';
      }
    } else {
      $_SESSION[ERROR_MESSAGE] = 'Your course is not listed. Please select a course from the predefined list or submit a request for adding your course';
    }
  }else {
    $_SESSION[ERROR_MESSAGE] = 'Please Add '.$sub_title;
  }
}

@$inst_id = $_GET['s'];

if ($inst_id) {
  if ($_SESSION['is_institute_selected'] == '1') {
    unset($_SESSION['is_institute_selected']);
  } else if ($_SESSION['is_institute_selected'] == '2') {

    $institutions = get_temp_institute_data($group_id, $user_its);
    if (count($institutions) < 1) {
      unset($_SESSION['is_institute_selected']);
    }
  }
  
  $array = $_SESSION['save_institute_data'];
  if($array) {
    foreach($array as $elementKey => $element) {
      foreach($element as $key => $value) {
        if($key == 'id' && $value == $inst_id){
          unset($array[$elementKey]);
          $_SESSION['save_institute_data'] = $array;
          
          header('location: ' . SERVER_PATH . 'marhala-' . $group_id . '/' . get_marhala_slug($group_id) . '/select-institute/');
        } 
      }
    }
  }else {
    $delete = delete_single_temp_institute_data($group_id, $user_its, $inst_id);
    header('location: ' . SERVER_PATH . 'marhala-' . $group_id . '/' . get_marhala_slug($group_id) . '/select-institute/');
  }
}

if (isset($_POST['proceed_with_araz'])) {
  $chk_institutes_data = get_temp_institute_data($group_id, $user_its);
  if ($chk_institutes_data) {
    foreach ($chk_institutes_data as $cid) {
      if ($cid['inst_name'] == '' || $cid['inst_city'] == '') {

        $message = 'في الحضرة العالية تميطط فقط course/stream واسطسس عرض كري رهيا ححهو، اْئنده جه وقت تميططcity انسس college اختيار كرو يا shortlist كرو ته وقت دوبارهcity انسسcollege واسطسس عرض كروو.';
      }
    }
    $_SESSION[SUCCESS_MESSAGE] = $message;
    header('location: ' . SERVER_PATH . 'marhala-' . $group_id . '/' . get_marhala_slug($group_id) . '/ques/');
  }else {
    header('location: ' . SERVER_PATH . 'marhala-' . $group_id . '/' . get_marhala_slug($group_id) . '/ques/');
  }
}

$countries = get_all_countries();
$country_iso = get_country_iso_by_name($user->get_country());
$ary_cities = array_sort(get_all_city_by_country_iso($country_iso[0]['ISO2']),'city',SORT_ASC);

if($group_id != '5'){
  $stream_array = get_degrees_by_marhala($group_id);
}else {
  $stream_array = array('Arts', 'Commerce', 'Science', 'Not Applicable');
}

//$ary_cities = get_madrasah_city_by_country_all('any');
$institute_data = get_temp_institute_data($group_id, $user_its);
if(!$institute_data){
  $institute_data = @$_SESSION['save_institute_data'];
}
$records_count = count($institute_data);

if (isset($_POST['save_araz'])) {
  $_SESSION['save_institute_data'] = $institute_data;
  $araz_data = serialize($_SESSION);

  $save_araz = save_tlb_araz($user_its, $group_id, $araz_data);
  if ($save_araz) {
    $_SESSION[SUCCESS_MESSAGE] = "Your Araz Data Save Successfully";
  }else {
    $_SESSION[ERROR_MESSAGE] = "Error In Saving Data";
  }
}

$record_array = array('0'=>'First','1'=>'Second','2'=>'Third','3'=>'Fourth','4'=>'Fifth','5'=>'Sixth','6'=>'Seventh','7'=>'Eighth','8'=>'Ninth','9'=>'Tenth','10'=>'Eleventh','11'=>'Twelfth','12'=>'Thirteenth','13'=>'Fourteenth','14'=>'Fifteenth','15'=>'Sixteenth','16'=>'Seventeenth','17'=>'Eighteenth','18'=>'Nineteenth','19'=>'Twentieth');
$newArray = $record_array[$records_count];

$course_month_array = array('January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December');

require_once 'inc/inc.header2.php';
require_once 'inc/inc.footer.php';

$smarty = new Smarty;

// Header / Session variables
$smarty->assign("server_path", SERVER_PATH);
$smarty->assign("user", $user);
$smarty->assign("group_id", $group_id);
$smarty->assign("panel_heading", $panel_heading);
$smarty->assign("panel_heading_ar", $panel_heading_ar);
$smarty->assign("ary_standard", $ary_standard);
$smarty->assign("sub_title", $sub_title);
$smarty->assign("kalemaat_nooraniyah", $kalemaat_nooraniyah);
$smarty->assign("institute_data", $institute_data);
$smarty->assign("countries", $countries);
$smarty->assign("ary_cities", $ary_cities);
$smarty->assign("stream_array", $stream_array);
$smarty->assign("record_array", $record_array);
$smarty->assign("newArray", $newArray);
$smarty->assign("course_month_array", $course_month_array);
$smarty->assign("show_next", $show_next);
$smarty->assign("save_institute_data", @$_SESSION['save_institute_data']);
$smarty->assign("generate_course_key", @$_SESSION['generate_course_key']);
$smarty->assign("is_institute_selected", @$_SESSION['is_institute_selected']);
$smarty->assign("already_araz_done", @$_SESSION['already_araz_done']);
$smarty->assign("success_message", $_SESSION['SUCCESS_MESSAGE']);
$smarty->assign("error_message", $_SESSION['ERROR_MESSAGE']);

$smarty->display('istirshad_institute.tpl');