<!DOCTYPE html>
<html lang="en">
  <head>


    <meta charset="utf-8"/>
    <title>Talabul Ilm</title>
    <meta name="description" content />
    <meta name="keywords" content />
    <meta name="author" content />


    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1"/>


    <link rel="shortcut icon" href="images/favicons/favicon.png"/>
    <link rel="apple-touch-icon" href="images/favicons/apple-touch-icon.png"/>
    <link rel="apple-touch-icon" sizes="72x72" href="images/favicons/apple-touch-icon-72x72.png"/>
    <link rel="apple-touch-icon" sizes="114x114" href="images/favicons/apple-touch-icon-114x114.png"/>


    <meta name="application-name" content="Talabul Ilm"/>
    <meta name="msapplication-TileColor" content="#ffffff"/>
    <meta name="msapplication-square70x70logo" content="images/favicons/msapplication-tiny.png"/>
    <meta name="msapplication-square150x150logo" content="images/favicons/msapplication-square.png"/>


    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="templates/css/custom.css?v=17" type="text/css" media="screen"/>
    <link href="templates/css/temp_style.css" media="all" rel="stylesheet" type="text/css"/>
    <link rel="stylesheet" href="templates/js/select2/select2.min.css">


    <!--[if lt IE 9]>
        <script src="templates/js/html5shiv.js"></script>
        <![endif]-->
    <script src="templates/js/jquery.min.js"></script>
    <link rel="stylesheet" href="templates/css/prettyPhoto.css" type="text/css" media="screen" title="prettyPhoto main stylesheet" charset="utf-8"/>
    <script>
      $(document).ready(function () {
        $('[data-toggle="popover"]').popover({
          placement: 'bottom'
        });
        $('body').on('click', function (e) {
          $('[data-toggle="popover"]').each(function () {
            //the 'is' for buttons that trigger popups
            //the 'has' for icons within a button that triggers a popup
            if (!$(this).is(e.target) && $(this).has(e.target).length === 0 && $('.popover').has(e.target).length === 0) {
              $(this).popover('hide');
            }
          });
        });
      });
    </script>
  </head>
  <body>


    <!--[if lt IE 9]>
          <div class="ie-old">
            <p class="center"><img alt="" src="images/ie-fossil.png" /></p>
            <p class="center bigger1">Your browser is extinct!!</p>
            <p class="center"><span class="color1">This Website is not compatible with Internet Explorer 8 or below. </span></p>
            <p class="center"><span class="color8">Please <a target="_blank" href="http://www.microsoft.com/en-us/download/internet-explorer.aspx"><span class="link_1">upgrade</span></a> your browser or Download 
            <a target="_blank" href="https://www.google.com/intl/en/chrome/browser/"><span class="link_1">Google Chrome</span></a> or <a target="_blank" href="http://www.mozilla.org/en-US/firefox/new/"><span class="link_1">Mozilla Firefox</span></a></span></p>
          </div>
          <![endif]-->


    <div id="preloader" style="display: block;">
      <div id="status" style="display: block;">
        <img src="images/loader.png" class="animated pulse infinite"/>
        <h2>Loading<br/>
          Please Wait...</h2>
      </div>
    </div>


    <div class="tilt-now">
      <h4>Arrgh!<br/>
        <small>It seems your screen size is small.<br/>
          Hold your device in Potrait Mode to Continue</small><img src="images/smart-phone-128.png" class="img-responsive"/></h4>
    </div>


    <div class="header-float">
      <div class="container">
        <nav class="nav"></nav>  
        <header>
          <div class="col-md-4 col-sm-12">
            <a href="index.php" class="logo"></a>
          </div>
          <div class="col-md-8 col-sm-12">


            <div class="text-right hidden-xs">
              <a href="qrn_tasmee.php" class="login">Al-Akh al-Qurani</a>
              <a href="srv_take_survey_mahad.php" class="login">Take Survey</a>
              <a href="marahil/" class="login">Send Araz To Aqa Maula TUS</a>
              <a href="log-out/" class="login">Log out</a>
            </div>
            <nav class="appmenu"></nav>  

            <div class="clearfix"></div>  
            <ul class="app-menu">
              <li class="dropdown"><a href="activities">Activities</a>
                <ul>
                  <li class=""><a href="activities/1435/">1435</a>
                  </li>
                  <li class=""><a href="activities/1436/">1436</a>
                  </li>
                </ul> </li>
              <li class="dropdown"><a href="istibsaar">Istibsaar</a>
                <ul>
                  <li class=""><a href="istibsaar/1436/">1436</a>
                  </li>
                  <li class=""><a href="istibsaar/1437/">1437</a>
                  </li>
                </ul> </li>
              <li class=""><a href="institutes">Institutes</a>
              </li>
              <li class="dropdown"><a href="events">Events</a>
                <ul>
                  <li class=""><a href="events/1436/">1436</a>
                  </li>
                </ul> </li>
              <li class="dropdown"><a href="istifaadah">Istifaadah</a>
                <ul>
                  <li class=""><a href="istifaadah/qasaid/">qasaid</a>
                  </li>
                </ul> </li>
              <li class="visible-xs-block"><a href="qrn_tasmee.php">Al-Akh al-Qurani</a></li>
              <li class="visible-xs-block"><a href="marahil/">Send Araz To Aqa Maula TUS</a></li>
              <li class="visible-xs-block"><a href="log-out/">Log out</a></li>
            </ul>
          </div>
        </header>
      </div>
    </div>
    <div class="clearfix"></div>  


    <div class="container white-bg">
      <div class="banners hidden-xs">
        <div id="banners" class="nivoSlider">
          <p class="hidden-sm" style="height: 300px;"></p>
        </div>
      </div>
    </div>
    <div class="clearfix"></div>
