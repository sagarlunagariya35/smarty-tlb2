<?php
require_once 'session.php';
require_once 'requires_login.php';
require_once 'classes/class.quiz.php';
require_once 'classes/class.qrn_mumin.php';
require_once 'classes/class.qrn_muhafiz.php';
require_once 'classes/class.qrn_tasmee.php';
require_once 'classes/class.user.php';

$qrn_mumin = new Qrn_Mumin();
$qrn_muhafiz = new Qrn_Muhafiz();
$qrn_tasmee = new Qrn_Tasmee();
$user_logedin = new mtx_user();

$user_id = $_SESSION[USER_ID];
$user_its = $_SESSION[USER_ITS];

$data = $db->clean_data($_REQUEST);
$query = $data['query'];

switch ($query) {
    case 'get_next_questions':
        $group_id = $data['group_id'];
        $que_id = $data['que_id'];
        $answer = $data['answer'];
        $user_answer = Quiz::insert_user_answer($user_id, $group_id, $que_id, $answer);
        $next_question = Quiz::get_questions($group_id, $que_id);
        //print_r($next_question);
        if($group_id != $next_question['group_id']) {
            $_SESSION[GROUP_ID] = $group_id;
            return FALSE;// '<script>window.location.href="index.php";</script>';
        }
        $total_group_que = Quiz::total_group_questions($group_id);
        $groups = Quiz::get_groups();
        $next_question['acquired_points'] = Quiz::get_group_acquired_points($group_id, $user_id);
        $next_question['min_que_id'] = $total_group_que['MIN_QUE_ID'];
        $next_question['max_que_id'] = $total_group_que['MAX_QUE_ID'];
        $next_question['min_group_id'] = $groups['MIN_GROUP_ID'];
        $next_question['max_group_id'] = $groups['MAX_GROUP_ID'];
        //print_r($next_question);
        echo json_encode($next_question);
        break;
    
    case 'make_entry_of_qasaid':
        $slider_value = $data['data'];
        $qasaid_id = $data['qasaid_id'];
        $slider_id = $data['slider_id'];
        
        $result = insert_qasaid_log($qasaid_id, $user_its, $slider_id, $slider_value); 
        break;
      
    case 'get_next_course_questions':
        $course_id = $data['course_id'];
        $que_id = $data['que_id'];
        if($data['answer'] == 'undefined'){
          $data['answer'] = '';
        }
        $answer = htmlentities($data['answer']);
        $fh = fopen('mtx_log.txt', 'a');
        fwrite($fh, "CID: $course_id , Ques: $que_id , Ans: $answer");
        $user_answer = Quiz::insert_user_courses_ques_answer($user_its, $course_id, $que_id, $answer);
        $questions = Quiz::get_course_questions($course_id, $que_id);
        $pre_fill_user_answers = Quiz::pre_fill_user_answers_for_course($course_id, $user_its, $questions['id']);
        //print_r($next_question);
        if($course_id != $questions['course_id']) {
            $_REQUEST['course_id'] = $course_id;
            return FALSE;// '<script>window.location.href="index.php";</script>';
        }
        //$total_course_que = Quiz::total_course_questions($course_id);
        //$questions['min_que_id'] = '0';
        //$questions['max_que_id'] = $total_course_que['MAX_QUE_ID'];
        //print_r($next_question);
        include 'courses_question.php';
        //echo json_encode($questions);
        break;
        
    case 'get_mumin_records':
        $mumin_its = $data['mumin_its'];
        
        $result = $qrn_mumin->get_all_mumin($mumin_its);
        if ($result) {
          $mumin_name = $result[0]['mumin_full_name'];
          $surah = $qrn_tasmee->get_surat_name($result[0]['last_tasmee_surat']);
          $ayat = $result[0]['last_tasmee_ayat'];
          $sanad = $result[0]['mumin_quran_sanad'];
          echo json_encode(array("FULL_NAME" => $mumin_name, "SURAH" => $surah, "AYAT" => $ayat, "SANAD" => $sanad, "SURAH_NO" => $result[0]['last_tasmee_surat']));
        } else {
          return FALSE;
        }
        
        break;
    
    case 'get_mumin_graph':
        $mumin_its = $data['mumin_its'];
        $timestamp = date('Y-m-d');
        
        $mumin_data = $qrn_mumin->get_all_mumin($mumin_its);
        if ($mumin_data) {
          $tasmee_records = $qrn_tasmee->get_tasmee_log_of_mumin_date_wise_for_graph($mumin_its, $timestamp);
          $total_ayat_count = $qrn_tasmee->get_mumin_total_ayat_count($mumin_its);
      
          include 'inc/qrn_mumin_graph.php';
        }
        break;
      
    case 'get_muhafiz_table':
        $timestamp = date('Y-m-d');
      
        $muhafiz_data = $qrn_muhafiz->get_all_muhafiz($user_its);
        if ($muhafiz_data) {
          $tasmee_records = $qrn_tasmee->get_tasmee_log_of_muhafiz_date_wise_for_table($user_its, $timestamp);

          include 'inc/qrn_tlb_muhafiz.php';
        }
        break;
      
    case 'get_mumin_table':
        $timestamp = date('Y-m-d');
      
        $mumin_data = $qrn_mumin->get_all_mumin($user_its);
        if ($mumin_data) {
          
          $tasmee_records = $qrn_tasmee->get_tasmee_log_of_mumin_date_wise_for_table($user_its, $timestamp);
          include 'inc/qrn_tlb_mumin.php';
        }
        break;
        
    case 'make_entry_of_tasmee_log':
        $mumin_its = $data['mumin_its'];
        $from_surah = $data['from_surah'];
        $from_ayat = $data['from_ayat'];
        $to_surah = $data['to_surah'];
        $to_ayat = $data['to_ayat'];
        $timestamp = date('Y-m-d');
       
        if ($user_its != $mumin_its) {
          $from_surah_details = $qrn_tasmee->get_quran_data_of_surah($from_surah, $from_ayat);
          $from_id = $from_surah_details['id'];
          
          $to_surah_details = $qrn_tasmee->get_quran_data_of_surah($to_surah, $to_ayat);
          $to_id = $to_surah_details['id'];
          
          if ($from_id AND $to_id) {
            if ($from_id < $to_id) {
              $total_ayat = $to_id - $from_id;

              $result = $qrn_tasmee->insert_tasmee_log($user_its, $mumin_its, $timestamp, $from_surah, $from_ayat, $to_surah, $to_ayat, $total_ayat, FALSE);

              $result = $qrn_mumin->update_surah_and_ayat($mumin_its, $to_surah, $to_ayat);

              $_SESSION[SUCCESS_MESSAGE] = 'Tasmee Log Successfully Inserted';
            } else {
              $_SESSION[ERROR_MESSAGE] = 'Please Enter Correct Surah and Ayat.';
            }
          } else {
            $_SESSION[ERROR_MESSAGE] = 'Please Enter Correct Ayat No.';
          }
        } else {
          $_SESSION[ERROR_MESSAGE] = 'Muhafiz and Sadeeq ITS can\'t be same Please Enter other ITS';
        }
        break;
}
?>