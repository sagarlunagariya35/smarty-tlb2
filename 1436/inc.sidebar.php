<div class="col-md-3 col-sm-4">
  <div id="page-sidebar" class="sidebar">
    <aside class="news-small" id="news-small">
      <header>
        <h2>Activities - 1435 H</h2>
      </header>
      <div class="section-content">
        <article>
          <header class="title-header">Asharah Mubarakah</header>
        </article><!-- /article -->
        <article>
          <header class="sub-title"><a href="<?php echo SERVER_PATH; ?>activities/1436/maaraz/">Maaraz</a></header>
        </article><!-- /article -->
        <article>
          <header class="sub-title"><a href="<?php echo SERVER_PATH; ?>activities/1436/waaz-mubarak-student-block/">Waaz Mubarak Student Block</a></header>
        </article><!-- /article -->
        <article>
          <header class="sub-title"><a href="<?php echo SERVER_PATH; ?>activities/1436/waaz-istibsaar-seesions/">Waaz Istibsaar sessions</a></header>
        </article><!-- /article -->
        <article>
          <header class="sub-title"><a href="<?php echo SERVER_PATH; ?>activities/1436/sair-e-ilmi/">Sair e Ilmi</a></header>
        </article><!-- /article -->
        <article>
          <header class="sub-title"><a href="<?php echo SERVER_PATH; ?>activities/1436/tasavvuraat-araz/">Tasavvuraat Araz</a></header>
        </article><!-- /article -->
        <article>
          <header class="title-header">Urs Mubarak Ayyam</header>
        </article><!-- /article -->
        <article>
          <header class="sub-title"><a href="<?php echo SERVER_PATH; ?>activities/1436/3-days-seminar/">3 Days Seminar</a></header>
      </div><!-- /.section-content -->
    </aside><!-- /.news-small -->
  </div><!-- /#sidebar -->
</div><!-- /.col-md-4 -->