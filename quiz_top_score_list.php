<?php
require_once 'session.php';
require_once 'requires_login.php';
require_once 'classes/class.quiz.php';

$top_lists = Quiz::get_top_10_users();

require_once 'inc/inc.header2.php';
?>

<!-- Contents -->
<!-- ====================================================================================================== -->
<div class="container white-bg">
  <div class="col-md-12 col-sm-12">

    <div class="page-title">
      <p style="margin-top:5px;">
        <a href="#" class="active">Asharah Mubarakah 1436 H</a></p>
      <h1>Asharah Mubarakah 1436 H<span class="alfatemi-text">معلومات ذاتية</span></h1>
    </div>
  </div>
  <div class="clearfix"></div> <!-- do not delete -->
  <form class="forms1 white" name="raza_form" method="post">
    <div class="col-md-3 col-sm-12 hidden-xs">
      <div class="profile-box-static">
        <h3 class="uppercase text-center">Tareekhi Event</h3>
      </div>
      <div class="profile-box-static-bottom">
        <a href="#">» 1435 H<hr></a>
        <a href="#">» 1436 H<hr></a>
        <a href="#">» 1437 H<hr></a>
      </div>
    </div>

    <div class="col-md-8 col-sm-12">
      <div>
        <h2 class="text-center">Top Score List<hr></h2>
        
        <div class="row">
          <?php
          $i = 1;
          foreach ($top_lists as $t) {
            $btn_cls = 'btn-complete';
            ?>
            <div class="col-xs-10 col-xs-offset-1 leader">
              <?php echo $t['user_name']; ?> <?php echo ($i == 11) ? '(OTHER)' : ''; ?> <span class="badge pull-right"><?php echo $t['total_points']; ?></span>
            </div>
            <?php $i++;
          }
          ?>
        </div>
        
        <div class="clearfix"></div>
      </div>
    </div>
    <div class="clearfix"></div>
  </form>
</div>
<style>
  .multitextbuttton a{
    color: #FFF;
    text-decoration: none;
  }
</style>

<?php
require_once 'inc/inc.footer.php';
?>
