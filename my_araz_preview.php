<?php
require 'smarty/libs/Smarty.class.php';
require_once 'session.php';
require_once 'requires_login.php';
require_once 'classes/class.user.php';
require_once 'classes/class.araz.php';

$body_class = 'page-sub-page';

$araz = $_GET['araz'];
$user_its = $_SESSION[USER_ITS];

$araz_data = get_my_araz_data_by_its_and_id($user_its,$araz);

$marhala_school_array = array('1','2','3','4');
$marhala_id = $araz_data['marhala'];

$user = new mtx_user();
$user->loaduser($_SESSION[USER_ITS]);

$institute_data = get_my_araz_institutions_data($araz);

require_once 'inc/inc.header2.php';
require_once 'inc/inc.footer.php';

$smarty = new Smarty;

// Header / Session variables
$smarty->assign("server_path", SERVER_PATH);
$smarty->assign("araz", $araz);
$smarty->assign("user_its", $user_its);
$smarty->assign("marhala_id", $marhala_id);
$smarty->assign("araz_data", $araz_data);
$smarty->assign("marhala_school_array", $marhala_school_array);
$smarty->assign("user", $user);
$smarty->assign("institute_data", $institute_data);

$smarty->display('my_araz_preview.tpl');