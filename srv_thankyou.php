<?php
require 'smarty/libs/Smarty.class.php';
require_once 'session.php';
require_once 'requires_login.php';
require_once 'classes/class.survey.php';

$srv_survey = new Mtx_Survey();

$user_id = $_SESSION[USER_ID];
$user_its = $_SESSION[USER_ITS];

require_once 'inc/inc.header2.php';


$smarty = new Smarty;

// Header / Session variables
$smarty->assign("server_path", SERVER_PATH);

$smarty->display('srv_thankyou.tpl');