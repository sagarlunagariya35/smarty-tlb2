<?php

$its = $_GET['its'];

//Data, connection, auth
$soapUrl = "http://www.its52.com/eJas/EjamaatServices.asmx"; // asmx URL of WSDL
// xml post structure
$xml_post_string = '<?xml version="1.0" encoding="utf-8"?>
<soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
  <soap:Body>
    <GetMuminNewPhoto xmlns="http://localhost/eJAS/EjamaatServices">
      <EjamaatId>' . $its . '</EjamaatId>
      <strKey>Talabulilm562138</strKey>
    </GetMuminNewPhoto>
  </soap:Body>
</soap:Envelope>';

$headers = array(
    "Content-type: text/xml;charset=\"utf-8\"",
    "Accept: text/xml",
    "Cache-Control: no-cache",
    "Pragma: no-cache",
    "SOAPAction: http://localhost/eJAS/EjamaatServices/GetMuminNewPhoto",
    "Content-length: " . strlen($xml_post_string)
);

// PHP cURL  for https connection with auth
$ch = curl_init();
curl_setopt($ch, CURLOPT_URL, $soapUrl);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
//curl_setopt($ch, CURLOPT_USERPWD, $soapUser . ":" . $soapPassword); // username and password - declared at the top of the doc
curl_setopt($ch, CURLOPT_POST, true);
curl_setopt($ch, CURLOPT_POSTFIELDS, $xml_post_string); // the SOAP request
curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

// converting
$response = curl_exec($ch);
curl_close($ch);

$XML = str_replace('&', '&amp;', $response);
$XML = str_replace('<', '&lt;', $XML);
echo '<pre>' . $XML . '</pre>';

$result = preg_match_all('/<GetMuminNewPhotoResult>(.*)<\/GetMuminNewPhotoResult>/imUs', $response, $imgs);

if (is_array($imgs) && isset($imgs[1][0]))
  $photo = 'data:image/png;base64,' . $imgs[1][0];

echo "<img src=\"$photo\">";
?>
