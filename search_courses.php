<?php
include('session.php');
require_once 'classes/gen_functions.php';

$marhala = $_GET['group'];
$courses = $_GET['s'];

$search_courses = search_courses($courses);

if ($search_courses) {
  foreach ($search_courses as $c) {
    $course_name = $c['course_name'];
    ?>
    <div class="show text-center">
      <span class="name" onclick="get_value_course('<?php echo $course_name; ?>');"><?php echo $c['course_name']; ?></span>
    </div>

    <?php
  }
}
?>