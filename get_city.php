<?php
require_once 'session.php';
require_once 'requires_login.php';
require_once 'classes/class.user.php';
require_once 'classes/gen_functions.php';
// Page body class
$body_class = 'page-sub-page';

$user = new mtx_user;
$user->loaduser($_SESSION[USER_ITS]);


$country = $_GET["country"];

$ary_cities = get_madrasah_city_by_country_all($country);

echo '<select name="madrasah_city" class="select-input" id="select-city" required="required" onchange="showmadrasah(this.value)">'
. '<option value="">Select City</option>';

foreach ($ary_cities as $city) {
  ?>
  <option value="<?php echo $city['city']; ?>" <?php echo ($city['city'] == $user->get_city()) ? 'checked' : ''; ?> ><?php echo $city['city']; ?></option>
  <?php
}

echo '</select>';

?>