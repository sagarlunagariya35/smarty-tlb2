<?php
require 'smarty/libs/Smarty.class.php';
require_once 'session.php';
require_once 'requires_login.php';

$page = 'ques_eng';
$body_class = 'page-sub-page';

$marhala_id = $_SESSION[MARHALA_ID];
$check1 = $check2 = $check3 = $check4 = FALSE;
$marhala_school_array = array('1', '2', '3', '4');

if($_SESSION['step'] != '3') {
  if($_SESSION['step'] != '5') {
    if($_SESSION['step'] <= '5') {
      if(in_array($marhala_id,$marhala_school_array)) {
        header('location: ' . SERVER_PATH . 'marhala-' . $marhala_id . '/' . get_marhala_slug($marhala_id) . '/school/');
      }else {
        header('location: ' . SERVER_PATH . 'marhala-' . $marhala_id . '/' . get_marhala_slug($marhala_id) . '/select-institute/');
      }
    }
  }  
}

switch ($marhala_id) {
  case 1:
    $panel_heading = 'Pre-Primary Araz';
    $std_heading = 'Pre-Primary';
    $panel_heading_ar = 'الحضانة';
    break;
  case 2:
    $panel_heading = 'Primary Araz';
    $std_heading = 'Primary';
    $panel_heading_ar = 'الابتدائية الأولى';
    break;
  case 3:
    $panel_heading = 'Std 5th to Std 7th Araz';
    $std_heading = 'Std 5th to Std 7th';
    $panel_heading_ar = 'الابتدائية الثانية';
    break;
  case 4:
    $panel_heading = 'Std 8th to 10th Araz';
    $std_heading = 'Std 8th to Std 10th';
    $panel_heading_ar = 'الثانوية الأولى';
    break;
  case 5:
    $panel_heading = 'Std 11th - 12th';
    $std_heading = 'Std 11th - 12th';
    $panel_heading_ar = 'الثانوية الثانية';
    break;
  case 6:
    $panel_heading = 'Graduation';
    $std_heading = 'Graduation';
    $panel_heading_ar = 'البكالوريا';
    break;
  case 7:
    $panel_heading = 'Post Graduation';
    $std_heading = 'Post Graduation';
    $panel_heading_ar = 'الماجيستار';
    break;
  case 8:
    $panel_heading = 'Diploma';
    $std_heading = 'Diploma';
    $panel_heading_ar = 'الدبلوم';
    break;
}

if (isset($_POST['submit'])) {
  // store all data to the session
  $_SESSION['ques'] = serialize($_POST);
  $_SESSION['step'] = '7';
  
  switch ($marhala_id) {
    case 1:
    case 2:
    case 3:
    case 4:
    case 5:
    case 6:
    case 7:
    case 8:
      header('location: ' . SERVER_PATH . "marhala-$marhala_id/" . get_marhala_slug($marhala_id) . '/preview/');
      break;
  }
}

if (isset($_POST['save_araz'])) {
  $institute_data = get_temp_institute_data($marhala_id, $_SESSION[USER_ITS]);
  $_SESSION['save_institute_data'] = $institute_data;
  
  $_SESSION['ques'] = serialize($_POST);
  $_SESSION['step'] = '7';
  
  $araz_data = serialize($_SESSION);

  $save_araz = save_tlb_araz($_SESSION[USER_ITS], $marhala_id, $araz_data);
  if ($save_araz) {
    $_SESSION[SUCCESS_MESSAGE] = "Your Araz Data Save Successfully";
  }else {
    $_SESSION[ERROR_MESSAGE] = "Error In Saving Data";
  }
}

$page_heading = 'Azaaim';
$page_heading_ar = 'عزائم';
$araz_data = get_araz_data_by_join($_SESSION[USER_ITS]);

$araz_notes = get_notes_by_marhala($marhala_id);
$ques_preffered = get_marhala_ques_preffered($marhala_id);
$ques = get_marhala_ques($marhala_id);

require_once 'inc/inc.header2.php';
require_once 'inc/inc.footer.php';

$smarty = new Smarty;

// Header / Session variables
$smarty->assign("server_path", SERVER_PATH);
$smarty->assign("page", $page);
$smarty->assign("marhala_id", $marhala_id);
$smarty->assign("panel_heading", $panel_heading);
$smarty->assign("panel_heading_ar", $panel_heading_ar);
$smarty->assign("marhala_school_array", $marhala_school_array);
$smarty->assign("std_heading", $std_heading);
$smarty->assign("page_heading", $page_heading);
$smarty->assign("page_heading_ar", $page_heading_ar);
$smarty->assign("araz_data", $araz_data);
$smarty->assign("araz_notes", $araz_notes);
$smarty->assign("ques_preffered", $ques_preffered);
$smarty->assign("ques", $ques);
$smarty->assign("check1", $check1);
$smarty->assign("check2", $check2);
$smarty->assign("check3", $check3);
$smarty->assign("check4", $check4);
$smarty->assign("success_message", $_SESSION['SUCCESS_MESSAGE']);
$smarty->assign("error_message", $_SESSION['ERROR_MESSAGE']);

$smarty->display('ques_details_english.tpl');