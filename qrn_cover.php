<?php
require 'smarty/libs/Smarty.class.php';
require_once 'session.php';
require_once 'requires_login.php';
require_once 'classes/class.user.php';

$user_logedin = new mtx_user();

require_once 'inc/inc.header2.php';
require_once 'inc/inc.footer.php';

$smarty = new Smarty;

// Header / Session variables
$smarty->assign("server_path", SERVER_PATH);
$smarty->assign("mumin_data_header", $mumin_data_header);
$smarty->assign("muhafiz_data_header", $muhafiz_data_header);

$smarty->display('qrn_cover.tpl');