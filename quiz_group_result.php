<?php
require_once 'session.php';
require_once 'requires_login.php';
require_once 'classes/class.quiz.php';

$user_id = $_SESSION[USER_ID];
$user_its = $_SESSION[USER_ITS];

if(isset($_POST['next_group'])) {
    $_SESSION[GROUP_ID] = $_SESSION[GROUP_ID] + 1;
    header('Location: take_quiz.php');
    exit();
}

if (isset($_SESSION[GROUP_ID])) {
    $group_id = (int) $_SESSION[GROUP_ID];
} else {
    $group_id = '';
}

$questions = Quiz::get_list_questions($group_id);

require_once 'inc/inc.header2.php';
?>

<!-- Contents -->
<!-- ====================================================================================================== -->
<div class="container white-bg">
  <div class="col-md-12 col-sm-12">

    <div class="page-title">
      <p style="margin-top:5px;">
        <a href="#" class="active">Asharah Mubarakah 1436 H</a></p>
      <h1>Asharah Mubarakah 1436 H<span class="alfatemi-text">معلومات ذاتية</span></h1>
    </div>
  </div>
  <div class="clearfix"></div> <!-- do not delete -->
  <form class="forms1 white" name="raza_form" method="post">
    <div class="col-md-3 col-sm-12 hidden-xs">
      <div class="profile-box-static">
        <h3 class="uppercase text-center">Tareekhi Event</h3>
      </div>
      <div class="profile-box-static-bottom">
        <a href="#">» 1435 H<hr></a>
        <a href="#">» 1436 H<hr></a>
        <a href="#">» 1437 H<hr></a>
      </div>
    </div>

    <div class="col-md-8">

        <div class="col-md-4 col-xs-4 col-sm-4">
          <a href="quiz_group_list.php" style="color:#FFF;" class="btn btn-warning btn-block">List Groups</a>
        </div>
        <div class="col-md-4 col-xs-4 col-sm-4">
            <form method="post">
            <button type="submit" name="next_group" class="btn btn-warning btn-block" id="show2">Next Group</button>
            </form>
        </div>
        <div class="col-md-4 col-xs-4 col-sm-4 pull-right">
          <div style="margin:5px;" class="quiz-point color1" dir="rtl">Total Point&nbsp;<span id="points"><?php echo Quiz::get_group_acquired_points($group_id, $user_id); ?></span></div>
        </div>
        <div class="clearfix"></div>
        <hr>

        <div class="col-md-12 col-lg-12">&nbsp;</div>

        <?php
        if ($questions) {
            foreach ($questions as $q) {
                $answer = Quiz::get_user_answer($group_id, $q['que_id'], $user_id);
                $is_right_class = $q['correct_answer'] == $answer['submitted_answer'] ? 'tick' : 'cross';
                ?>
                <div class="panel panel-info lsd" dir="rtl">
                  <div class="panel-heading <?php echo $is_right_class; ?> text-center" data-toggle="collapse" href="#question-<?php echo $q['que_id']; ?>" aria-expanded="false" aria-controls="question-<?php echo $q['que_id']; ?>"><span class="pull-right">Question <?php echo $q['que_id']; ?> </span>click here for more details</div>
                    <div class="panel-body collapse" id="question-<?php echo $q['que_id']; ?>">
                      <p class="lsd"><?php echo $q['question']; ?></p>

                        <?php if($answer['submitted_answer'] != ''){ ?>
                        <p class="<?php echo $is_right_class; ?> lsd"><?php echo $q['opt' . $answer['submitted_answer']]; ?></p>
                        <?php } ?>

                        <?php if($is_right_class == 'cross') { ?>
                        <p class="alert alert-success lsd"><?php echo $q['opt'.$q['correct_answer']]; ?></p>
                        <?php } ?>

                        <p class="lsd"><?php echo $q['bayan']; ?></p>

                    </div>
                    <div class="panel-footer">
                      <span class="color1">Points: <?php echo $answer['points']; ?></span>
                    </div>
                </div>
            <?php }
        } ?>
      </div>
    <div class="clearfix"></div>
  </form>
</div>
<style>
  .panel-body p{
    color: #000 !important;
    font-size: 20px;
    text-align: right;
  }
  .pull-right{
    float:right !important;
  }
</style>

<?php
require_once 'inc/inc.footer.php';
?>
