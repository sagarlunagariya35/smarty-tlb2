<?php
require_once 'classes/class.user.php';
require_once 'classes/class.qrn_mumin.php';
require_once 'classes/class.qrn_muhafiz.php';

$qrn_mumin = new Qrn_Mumin();
$qrn_muhafiz = new Qrn_Muhafiz();
$user_logedin = new mtx_user();

$mumin_its = $muhafiz_its = 0;
$muhafiz_data = $muhafiz_full_name = $muhafiz_jamaat = $muhafiz_jamiat = $muhafiz_quran_sanad = $muhafiz_gender = FALSE;

// Update ITS data of mumin and muhafiz on qrn_mumin
$mumin_query = "SELECT `its_id`, `muhafiz_id` FROM `qrn_mumin`";
$mumin_result = $db->query_fetch_full_result($mumin_query);

if ($mumin_result) {
  foreach ($mumin_result as $data) {
    
    $mumin_its = $data['its_id'];
    $muhafiz_its = $data['muhafiz_id'];
    
    if ($mumin_its) {
      $mumin_data = $user_logedin->loaduser($mumin_its);
      $mumin_full_name = $mumin_data->get_full_name();
      $mumin_jamaat = $mumin_data->get_jamaat();
      $mumin_jamiat = $mumin_data->get_jamiat();
      $mumin_quran_sanad = $mumin_data->get_quran_sanad();
      $mumin_gender = $mumin_data->get_gender();
    }
    
    if ($muhafiz_its) {
      $muhafiz_data = $user_logedin->loaduser($muhafiz_its);
      $muhafiz_full_name = $muhafiz_data->get_full_name();
      $muhafiz_jamaat = $muhafiz_data->get_jamaat();
      $muhafiz_jamiat = $muhafiz_data->get_jamiat();
      $muhafiz_quran_sanad = $muhafiz_data->get_quran_sanad();
      $muhafiz_gender = $muhafiz_data->get_gender();
    }
    
  $update_query = "UPDATE `qrn_mumin` SET `mumin_full_name` = '$mumin_full_name', `mumin_jamaat` = '$mumin_jamaat', `mumin_jamiat` = '$mumin_jamiat', `mumin_quran_sanad` = '$mumin_quran_sanad', `mumin_gender` = '$mumin_gender', `muhafiz_full_name` = '$muhafiz_full_name', `muhafiz_jamaat` = '$muhafiz_jamaat', `muhafiz_jamiat` = '$muhafiz_jamiat', `muhafiz_quran_sanad` = '$muhafiz_quran_sanad', `muhafiz_gender` = '$muhafiz_gender' WHERE `its_id` LIKE '$mumin_its' AND `muhafiz_id` LIKE '$muhafiz_its' ";
  $result = $db->query($update_query);
  
  }
}

?>
