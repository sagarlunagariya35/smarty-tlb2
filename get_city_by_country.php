<?php
require_once 'session.php';
require_once 'requires_login.php';
require_once 'classes/class.user.php';
require_once 'classes/gen_functions.php';
// Page body class
$body_class = 'page-sub-page';

$user = new mtx_user;
$user->loaduser($_SESSION[USER_ITS]);

$country = $_GET["country"];
$country_iso = get_country_iso_by_name($country);
$ary_cities = array_sort(get_all_city_by_country_iso($country_iso[0]['ISO2']),'city',SORT_ASC);

echo '<select name="madrasah_city" id="select-city" class="required select-input" required>
        <option value="">Select City</option>';
      if($ary_cities){
        foreach ($ary_cities as $city) {
  ?>
  <option value="<?php echo $city['city']; ?>" <?php echo ($city['city'] == $user->get_city()) ? 'checked' : ''; ?> ><?php echo $city['city']; ?></option>
  <?php
        }
      }
  echo '</select>';
?>