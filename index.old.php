<?php
require_once 'session.php';
// Page body class
$body_class = 'page-homepage-carousel';

require_once 'inc/inc.header.php';
require_once 'classes/class.user.php';

$user = new mtx_user();
if (isset($_SESSION[USER_ITS])) {
  $user->loaduser($_SESSION[USER_ITS]);
}

$user_type = $user->get_user_type();

switch ($user_type) {
  case 'Student':
    $tag_line = '<span class="tag-author">المجلس السادس-1413</span><br>
تمام علوم نو جملة قراْن، لوگو جھ نے  دنيا نا علوم كهے  چھے يھ بھي سگلا قراْن ما چھے}، تارے  كوئي ايك نے  رواں نهيں  كھ امام الزمان ني رزا بغير، داعي ني رزا بغير ايك حرف بھي پڑهاوي سكے،';
    break;
  case 'Teacher':
    $tag_line = '<span class="tag-author">المجلس السابع-1428</span><br>
اهوو كرو كھ دنيا بندگي كرے، مدارس اسكولو ما بھي يه}مثل جانفشاني كرو، يه}مثل معلم معلمين ٹيچرو جوئيے، teachers اهنا پر جتنو كروو هوئي تھ كرو';
    break;
  case 'Professional':
    $tag_line = '<span class="tag-author">المجلس الثاني-1436</span><br>
رسول الله يھ فرمايوچھے كه"ان الله يحب الصانع المتقن في صنعه"، كھ خدا تعالى وه كاريگر نے  پسند كرے  چھے كھ جھ اهني كاريگري نے  اتقان سي كرے، مضبوطي سي كرے،
تواے  داودي بهره قوم ني جماعة! تميں جھ بھي كام كرتا هوئي اهنے  اتقان سي كرجو، خدا ني طاعة كري نے  حلال سي كرجو، حرام سي دور رهجو، انے  دنيا ماهر هنر ما تميں ذروة پر هوئي، top پر هوئي.
';
    break;
  case 'Counselor':
    $tag_line = '<span class="tag-author">المجلس الثاني-1436</span><br>
موعظة حسنة سوں؟ كھ كهواني ڈهب، اهوي شاكلة سي اداء كرے  كھ سامنے  والا قبول كري لے';
    break;
  case 'Mentor':
    $tag_line = '<span class="tag-author">المجلس الرابع-1421</span><br>
فرزند نے   اْج مھوٹا تھيا پچھي بھي ناهنا نے   ناهنا} سمجهے، يھ برابر نهيں، مھوٹا تھيا تو هوے   اهنے   مھوٹا سمجهوو لازم چھے، تو اهنے   يھ طريقة سي سمجهاوو جوئيے   كھ جھ نا سي اهنا گلے   يھ وات اترے، انے   اولياء الله ني سكهامنو بتاوي چھے يھ كهوي جوئيے';
    break;
  case 'Sponsor':
    $tag_line = '<span class="tag-author">المجلس السابع-1432</span><br>
اْ دنو ما نية كر ي لو كھ  اْ مثل نا مؤمنين نے  هميں  مدد كريسوں، انے  تميں  يھ سگلا نے  اْپو  تو عزة انے  كرامة سي، جمنا هاتھــ سي اْپو تو دابا هاتھــ نے  خبر نھ پڑے، انے  هرگز گناؤجو  مت، تميں  اْ مثل نا عمل كرسو تو يھ مؤمن گهنا خوش تھاسے، مگر مھوٹي وات يھ چھے كھ ميں  گهنو خوش تھئيس.';
    break;
  case 'Foster':
    $tag_line = '<span class="tag-author">المجلس السابع-1426</span><br>
ضرورة مندو مؤمنين نا رهائشي امور ساتھے، تمام ديني، دنيوي، تعليمي انے  اقتصادي امور اُستوار  تھائے، بهتر تھائے، انے  يھ خدمة جھ مستطيع  چھے يھ لئي اُٹھے، كوئي ايك گهر، كوئي بے  گهر، يا زيادة، يھ مثل ضرورة مند نے  Fostership اْپے، كھ پورا سال لگ يھ نو خيال راكهے';
    break;
}
?>

<!-- Contents -->
<!-- ====================================================================================================== -->
<div class="container white-bg">
  <div class="col-md-12 col-sm-12">

    <div class="page-title">
      <h1>select your primary level</h1>
      <p><a href="#">Home</a> / <a href="#">Istabsaar</a> / <a href="#" class="active">select your primary level</a></p>
    </div>
  </div>
  <div class="clearfix"></div> <!-- do not delete -->
  <div class="col-md-3 col-sm-12 bayan">
    <div class="col-md-12 blue-box1">
      <h3 class="bordered lsd white">
        اْ دنو ما نية كر ي لو كھ  اْ مثل نا مؤمنين نے  هميں  مدد كريسوں، انے  تميں  يھ سگلا نے  اْپو  تو عزة انے  كرامة سي، جمنا هاتھــ سي اْپو تو دابا هاتھــ نے  خبر نھ پڑے، انے  هرگز گناؤجو  مت، تميں  اْ مثل نا عمل كرسو تو يھ مؤمن گهنا خوش تھاسے، مگر مھوٹي وات يھ چھے كھ ميں  گهنو خوش تھئيس.</h3>
      <div class="clearfix"></div>
    </div>
  </div>
  <div class="col-md-6 col-sm-12">
    <ul class="roles">
      <li class="col-md-6 col-sm-6">
        <a href="#"><span class="student-icon"></span>Student</a>
      </li>
      <li class="col-md-6 col-sm-6">
        <a href="#"><span class="teacher-icon"></span>teacher</a>
      </li>
      <li class="col-md-6 col-sm-6">
        <a href="#"><span class="counselor-icon"></span>Counselor</a>
      </li>
      <li class="col-md-6 col-sm-6">
        <a href="#"><span class="sponsor-icon"></span>Sponsor</a>
      </li>
      <li class="col-md-6 col-sm-6">
        <a href="#"><span class="mentor-icon"></span>Mentor</a>
      </li>
      <li class="col-md-6 col-sm-6">
        <a href="#"><span class="foster-parent-icon"></span>Foster Parent</a>
      </li>
      <li class="col-md-6 col-sm-6">
        <a href="#"><span class="business-man-icon"></span>Business Person</a>
      </li>
      <li class="col-md-6 col-sm-6">
        <a href="#"><span class="business-man-icon"></span>Professional</a>
      </li>
    </ul>
  </div>
  <div class="col-md-3 col-sm-12 bayan">
    <div class="col-md-12 blue-box1">
      <h3 class="bordered lsd white">
        اْ دنو ما نية كر ي لو كھ  اْ مثل نا مؤمنين نے  هميں  مدد كريسوں، انے  تميں  يھ سگلا نے  اْپو  تو عزة انے  كرامة سي، جمنا هاتھــ سي اْپو تو دابا هاتھــ نے  خبر نھ پڑے، انے  هرگز گناؤجو  مت، تميں  اْ مثل نا عمل كرسو تو يھ مؤمن گهنا خوش تھاسے، مگر مھوٹي وات يھ چھے كھ ميں  گهنو خوش تھئيس.</h3>
      <div class="clearfix"></div>
    </div>
  </div>
</div>
<?php
include_once 'inc/inc.footer.php';
?>
