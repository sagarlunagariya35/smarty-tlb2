<?php
require 'smarty/libs/Smarty.class.php';
require_once 'session.php';
require_once 'requires_login.php';

$qasaid_id = $_GET['tag'];
$result = FALSE;
// Page body class
$body_class = 'page-sub-page';

$user_its = $_SESSION[USER_ITS];
$user_its_exist = check_user_its_exist_in_log($user_its);

if (!$user_its_exist) {
  $result = insert_user_its_log(ISTIFAADAH, $user_its, $qasaid_id);
}

$qasaides = get_all_qasaid();
$this_qasida = get_qasida_by_id($qasaid_id);
$get_slider1_val = (get_log_qasaid_slider_value($user_its, $qasaid_id, '1')) ? get_log_qasaid_slider_value($user_its, $qasaid_id, '1') : 0;
$get_slider2_val = (get_log_qasaid_slider_value($user_its, $qasaid_id, '2')) ? get_log_qasaid_slider_value($user_its, $qasaid_id, '2') : 0;
$get_slider3_val = (get_log_qasaid_slider_value($user_its, $qasaid_id, '3')) ? get_log_qasaid_slider_value($user_its, $qasaid_id, '3') : 0;

$total_sider_value = $get_slider1_val + $get_slider2_val + $get_slider3_val;
$average_slider_value = $total_sider_value / 3;

$qasaid_alams = get_all_alam_of_qasaid();

require_once 'inc/inc.header2.php';
require_once 'inc/inc.footer.php';

$smarty = new Smarty;

// Header / Session variables
$smarty->assign("server_path", SERVER_PATH);
$smarty->assign("qasaid_id", $qasaid_id);
$smarty->assign("result", $result);
$smarty->assign("qasaides", $qasaides);
$smarty->assign("this_qasida", $this_qasida);
$smarty->assign("get_slider1_val", $get_slider1_val);
$smarty->assign("get_slider2_val", $get_slider2_val);
$smarty->assign("get_slider3_val", $get_slider3_val);
$smarty->assign("total_sider_value", $total_sider_value);
$smarty->assign("average_slider_value", $average_slider_value);
$smarty->assign("qasaid_alams", $qasaid_alams);

$smarty->display('qasaid.tpl');