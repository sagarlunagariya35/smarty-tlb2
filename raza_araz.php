<?php
require 'smarty/libs/Smarty.class.php';
require_once 'session.php';
require_once 'requires_login.php';
require_once 'classes/class.user.php';
require_once 'classes/class.araz.php';

// Page body class
$body_class = 'page-sub-page';
$page = 'raza-araz';

$user = new mtx_user;
$araz = new mtx_araz;

$user->loaduser($_SESSION[USER_ITS]);
$prefix = $user->get_first_prefix();
$first_name = $user->get_first_name();
$middle_prefix = $user->get_middle_prefix();
$middle_name = $user->get_middle_name();
$last_name = $user->get_last_name();

$ary_darajah = FALSE;

$group_id = $_GET['group'];

$len = mt_rand(4, 10);
$form_id = '';
for ($i = 0; $i < $len; $i++) {
  $d = rand(1, 30) % 2;
  $form_id .= $d ? chr(rand(65, 90)) : chr(rand(48, 57));
}

$form_id = md5(time() . $form_id);

switch ($group_id) {
  case 1:
  case 2:
  case 3:
  case 4:
    $madrasah_heading = 'Madrasah';
    $madrasah_heading_ar = 'المدرسة';
    break;
  case 5:
  case 6:
  case 7:
  case 8:
    $ary_darajah = array('Nisaab e Awwal', 'Nisaab e Saani', 'Nisaab e Saalis', 'Nisaab e Raabe', 'Nisaab e Khaamis', 'Nisaab e Saadis');
    $madrasah_heading = 'Deeni Taalim <i class="fa fa-question-circle help-icon" data-toggle="modal" data-target="#NisaabHelp"></i>';
    $madrasah_heading_ar = 'ديني تعليم';
    break;
}

$msb_student_info = $araz->get_madrasah_details_from_ejamaat_its($_SESSION[USER_ITS]);
if ($msb_student_info) {
  $_SESSION['msb_student_info'] = '1';
}

$madrasah = $araz->get_madrasah_details_from_attalim_its($_SESSION[USER_ITS]);
if ($madrasah) {
  $_SESSION['madrasah_name'] = (string) $madrasah->MadrasaName;
  $_SESSION['madrasah_name_ar'] = (string) $madrasah->ArabicMadrasaName;
  $_SESSION['madrasah_darajah'] = (string) $madrasah->Darajah;
  $_SESSION['madrasah_city'] = (string) $madrasah->City;
} else {
  $madrasah_data = $araz->get_madrasah_details_from_ejamaat_its($_SESSION[USER_ITS]);

  if (!$madrasah_data) {
    $_SESSION['madrasah_name'] = '';
    $_SESSION['madrasah_name_ar'] = '';
    $_SESSION['madrasah_darajah'] = '';
    $_SESSION['madrasah_city'] = '';
  }else {
    $_SESSION['madrasah_name'] = '';
    $_SESSION['madrasah_name_ar'] = '';
    $_SESSION['madrasah_darajah'] = '';
    $_SESSION['madrasah_city'] = '';
  }
}

if (isset($_POST['submit'])) {
  $_SESSION['step'] = '2';

  $_SESSION['hifz_sanad'] = $user->get_quran_sanad();

  if (@$_POST['madrasah_darajah']) {
    if ($_POST['madrasah_darajah'] == 'none') {
      $_SESSION['madrasah_darajah'] = '';
    } else {
      $_SESSION['madrasah_darajah'] = $_POST['madrasah_darajah'];
    }
  }
  $data = $_POST;
  
  if($_SESSION['madrasah_name'] == ''){
    $_SESSION['madrasah_name'] = $data['attending_sabaq'];
  }
  
  $_SESSION['form_id'] = $data['form_id'];
  $_SESSION['taken_psychometric_aptitude_test'] = '';
  $_SESSION['text_psychometric_aptitude'] = '';
  $_SESSION['prefer_more_clarity'] = '';
  $_SESSION['want_further_guidence'] = '';
  $_SESSION['seek_funding_edu'] = '';
  $_SESSION['want_assistance'] = '';
  $_SESSION['received_sponsorship'] = '';
  $_SESSION['follow_shariaa'] = '';
  $_SESSION['disclaimer'] = '';

  $ret = $user->update_data($data);
  if ($ret['status'] == 1) {

    switch ($group_id) {
      case 1:
      case 2:
      case 3:
      case 4:
        header('location: ' . SERVER_PATH . 'marhala-' . $group_id . '/' . get_marhala_slug($group_id) . '/school/');
        exit();
        break;
      case 5:
      case 6:
      case 7:
      case 8:
        header('location: ' . SERVER_PATH . 'marhala-' . $group_id . '/' . get_marhala_slug($group_id) . '/select-institute/');
        exit();
        break;
    }
  } else {
    echo $ret['error'];
  }
}

$hidayat_quran = '<p style="color:#444444;">“According to our records, your hifz details are pending”</p>';
$hidayat_madrasah = '<p style="color:#444444;">“According to our records your madrasah  or sabak details are pending”</p>';
$hidayat_sabaaq = '<p style="color:#444444;">“According to our records, your sabak details are pending”</p>';

$marhala_array = array('1', '2', '3', '4');
$is_araz_done = $araz->check_if_araz_pending();

require_once 'inc/inc.header2.php';
require_once 'inc/inc.footer.php';

$smarty = new Smarty;

// Header / Session variables
$smarty->assign("server_path", SERVER_PATH);
$smarty->assign("user", $user);
$smarty->assign("prefix", $prefix);
$smarty->assign("first_name", $first_name);
$smarty->assign("middle_prefix", $middle_prefix);
$smarty->assign("middle_name", $middle_name);
$smarty->assign("last_name", $last_name);
$smarty->assign("group_id", $group_id);
$smarty->assign("form_id", $form_id);
$smarty->assign("madrasah_heading", $madrasah_heading);
$smarty->assign("madrasah_heading_ar", $madrasah_heading_ar);
$smarty->assign("ary_darajah", $ary_darajah);
$smarty->assign("hidayat_quran", $hidayat_quran);
$smarty->assign("hidayat_madrasah", $hidayat_madrasah);
$smarty->assign("hidayat_sabaaq", $hidayat_sabaaq);
$smarty->assign("marhala_array", $marhala_array);
$smarty->assign("hidayat_madrasah", $hidayat_madrasah);
$smarty->assign("madrasah_darajah", $_SESSION['madrasah_darajah']);
$smarty->assign("madrasah_name", $_SESSION['madrasah_name']);
$smarty->assign("is_araz_done", $is_araz_done);

$smarty->display('raza_araz.tpl');