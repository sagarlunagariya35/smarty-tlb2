<?php
require_once 'classes/class.user.php';
require_once 'classes/class.qrn_mumin.php';
require_once 'classes/class.qrn_muhafiz.php';

$qrn_mumin = new Qrn_Mumin();
$qrn_muhafiz = new Qrn_Muhafiz();
$user_logedin = new mtx_user();

$to_date = date('Y-m-d', strtotime('-3 days'));

//Delete pending request of 3 days ago
$request_query = "SELECT * FROM `qrn_tasmee_request` WHERE `request_ts`<='$to_date' AND `status`='0'";
$request_result = $db->query_fetch_full_result($request_query);

if ($request_result) {
  foreach ($request_result as $data) {
    
    $mumin_its = $data['mumin_its'];
    if ($mumin_its) {
      $mumin_data = $user_logedin->loaduser($mumin_its);
      $mumin_email = $mumin_data->get_email();
      if ($mumin_email) {
        $mumin_subject = "Request Removed";
        $mumin_message = "Your pending request removed successfully.";
        
        $mumin_email_sent = $qrn_muhafiz->insert_cron_emails('akhequrani@talabulilm.com', $mumin_email, $mumin_subject, $mumin_message);
      }
    }
    
    $muhafiz_its = $data['muhafiz_its'];
    if ($muhafiz_its) {
      $muhafiz_data = $user_logedin->loaduser($muhafiz_its);
      $muhafiz_email = $muhafiz_data->get_email();
      if ($muhafiz_email) {
        $muhafiz_subject = "Request Removed";
        $muhafiz_message = "Your pending request removed successfully.";
        
        $muhafiz_email_sent = $qrn_muhafiz->insert_cron_emails('akhequrani@talabulilm.com', $muhafiz_email, $muhafiz_subject, $muhafiz_message);
      }
    }
    
    $delete_query = "DELETE FROM `qrn_tasmee_request` WHERE `id` LIKE '$data[id]'";
    $delete_result = $db->query($delete_query);
  }
}

?>
