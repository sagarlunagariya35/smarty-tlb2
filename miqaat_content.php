<?php
require_once 'session.php';
require_once 'requires_login.php';
require_once 'classes/class.quiz.php';

$body_class = 'page-sub-page';
$miqat_id = $_GET['miqaat_id'];

$miqaat_istibsaar_id = FALSE;

$miqaat_istibsaar = get_single_miqaat_istibsaar($miqat_id);

$miqaat_audios = get_all_miqaat_files('miqaat_audio', $miqat_id);
$miqaat_videos = get_all_miqaat_files('miqaat_video', $miqat_id);
$miqaat_stationaries = get_all_miqaat_files('miqaat_stationary', $miqat_id);
$miqaat_ohbats = get_all_miqaat_data('tlb_ohbat_daily_sentences', $miqat_id);
$miqaat_sentences = get_all_miqaat_data('tlb_sentences', $miqat_id);
$miqaat_quizes = get_all_miqaat_data('questions', $miqat_id);
$miqaat_courses = get_all_miqaat_data('courses', $miqat_id);
$miqaat_youtube = get_all_miqaat_files('miqaat_youtube', $miqat_id);

$mawaqeet = get_all_mawaqeet_by_year($miqaat_istibsaar['year']);

require_once 'inc/inc.header2.php';
?>

<!-- Contents -->
<!-- ====================================================================================================== -->
<style>
  .orange{
    color: #cf4914;
  }
</style>
<div class="container white-bg">
  <div class="col-md-12 col-sm-12">

    <div class="page-title">
      <p style="margin-top:5px;">
        <a href="#" class="active">Istibsaar <?php echo $miqaat_istibsaar['year']; ?> H</a></p>
      <h1>Istibsaar <?php echo $miqaat_istibsaar['year']; ?> H<span class="alfatemi-text">الاستبصار</span></h1>
    </div>
  </div>
  <div class="clearfix"></div> <!-- do not delete -->
  <form class="forms1 white" name="raza_form" method="post">
    <div class="col-md-3 col-sm-12 hidden-xs">
      <div class="profile-box-static">
        <h3 class="uppercase text-center">Mawaqeet <?php echo $miqaat_istibsaar['year']; ?> H</h3>
      </div>
      <div class="profile-box-static-bottom">
        <?php
        foreach ($mawaqeet as $mqt) {
          echo "<a href=\"" . SERVER_PATH . "istibsaar/$mqt[id]/$mqt[slug]/$mqt[year]/" . "\">» $mqt[miqaat_title]<hr></a>";
        }

        $make_pre_active = '';
        $make_in_active = '';
        $make_post_active = '';

        if ($miqaat_istibsaar['show_post_event'] == '1') {
          $make_post_active = ' active';
        } elseif ($miqaat_istibsaar['show_in_event'] == '1') {
          $make_in_active = ' active';
        } elseif ($miqaat_istibsaar['show_pre_event'] == '1') {
          $make_pre_active = ' active';
        }
        ?>
      </div>
    </div>

    <div class="col-md-8 col-sm-12">
      <div class="static-show-button large-fonts col-xs-12 col-sm-12 pull-left"><?php echo $miqaat_istibsaar['miqaat_title']; ?></div>
      <div class="clearfix"></div>

      <div>
        <ul class="nav nav-tabs" role="tablist">

          <?php if ($miqaat_istibsaar['show_pre_event'] == '1') { ?><li role="presentation" class="<?php echo $make_pre_active; ?>"><a href="#pre" aria-controls="pre" role="tab" data-toggle="tab">Ohbat</a></li><?php } ?>

          <?php if ($miqaat_istibsaar['show_in_event'] == '1') { ?><li role="presentation" class="<?php echo $make_in_active; ?>"><a href="#in" aria-controls="in" role="tab" data-toggle="tab">Talaqqi</a></li><?php } ?>

          <?php if ($miqaat_istibsaar['show_post_event'] == '1') { ?><li role="presentation" class="<?php echo $make_post_active; ?>"><a href="#post" aria-controls="post" role="tab" data-toggle="tab">Tizkaar</a></li><?php } ?>

        </ul>

        <?php
        $video_pre = $video_in = $video_post = FALSE;
        $audio_pre = $audio_in = $audio_post = FALSE;
        $audio_pre2 = $audio_in2 = $audio_post2 = FALSE;
        $stat_pre = $stat_in = $stat_post = FALSE;
        $ohbat_pre = $ohbat_in = $ohbat_post = FALSE;
        $sentences_pre = $sentences_in = $sentences_post = FALSE;
        $quiz_pre = $quiz_in = $quiz_post = FALSE;
        $course_pre = $course_in = $course_post = FALSE;
        $youtube_pre = $youtube_in = $youtube_post = FALSE;

        if ($miqaat_videos) {
          foreach ($miqaat_videos as $video) {

            $video_table = '<a href="' . SERVER_PATH . 'upload/miqaat_upload/video/' . $video['video_url'] . '"><img src="' . SERVER_PATH . 'upload/miqaat_upload/video/images/' . $video['video_thumb'] . '"  title=""></a>';

            if ($video['event_time'] == 'pre') {
              $video_pre .= $video_table;
            } else if ($video['event_time'] == 'in') {
              $video_in .= $video_table;
            } else if ($video['event_time'] == 'post') {
              $video_post .= $video_table;
            }
          }
        }
        
        if ($miqaat_youtube){
          foreach ($miqaat_youtube as $you_vid){
            if ($you_vid['event_time'] == 'pre') {
              $youtube_pre .= $you_vid['video_url'];
            } elseif($you_vid['event_time'] == 'in'){
              $youtube_in .= $you_vid['video_url'];
            } elseif ($you_vid['event_time'] == 'post') {
              $youtube_post .= $you_vid['video_url'];
            }
          }
        }

        if ($miqaat_audios) {
          foreach ($miqaat_audios as $audio) {

            if ($audio['event_time'] == 'pre') {
              $player_id = 'jquery_jplayer_1';
              $css_id = 'jp_container_1';
            } else if ($audio['event_time'] == 'in') {
              $player_id = 'jquery_jplayer_2';
              $css_id = 'jp_container_2';
            } else if ($audio['event_time'] == 'post') {
              $player_id = 'jquery_jplayer_3';
              $css_id = 'jp_container_3';
            }

            $audio_table = '<div id="' . $player_id . '" class="jp-jplayer"></div>
          <div id="' . $css_id . '" class="jp-audio" role="application" aria-label="media player">
            <div class="jp-type-playlist">
              <div class="jp-gui jp-interface">
                <div class="jp-controls">
                  <button class="jp-previous" role="button" tabindex="0">previous</button>
                  <button class="jp-play" role="button" tabindex="0">play</button>
                  <button class="jp-next" role="button" tabindex="0">next</button>
                  <button class="jp-stop" role="button" tabindex="0">stop</button>
                </div>
                <div class="jp-progress">
                  <div class="jp-seek-bar">
                    <div class="jp-play-bar"></div>
                  </div>
                </div>
                <div class="jp-volume-controls">
                  <button class="jp-mute" role="button" tabindex="0">mute</button>
                  <button class="jp-volume-max" role="button" tabindex="0">max volume</button>
                  <div class="jp-volume-bar">
                    <div class="jp-volume-bar-value"></div>
                  </div>
                </div>
                <div class="jp-time-holder">
                  <div class="jp-current-time" role="timer" aria-label="time">&nbsp;</div>
                  <div class="jp-duration" role="timer" aria-label="duration">&nbsp;</div>
                </div>
                <div class="jp-toggles">
                  <button class="jp-repeat" role="button" tabindex="0">repeat</button>
                  <button class="jp-shuffle" role="button" tabindex="0">shuffle</button>
                </div>
              </div>
              <div class="jp-playlist">
                <ul>
                  <li>&nbsp;</li>
                </ul>
              </div>
              <div class="jp-no-solution">
                <span>Update Required</span>
                To play the media you will need to either update your browser to a recent version or update your <a href="http://get.adobe.com/flashplayer/" target="_blank">Flash plugin</a>.
              </div>
            </div>
          </div>';

            $audio_table2 = '{
                        title:"' . $audio['audio_url'] . '",
                        mp3: "' . SERVER_PATH . 'upload/miqaat_upload/audio/' . $audio['audio_url'] . '",
                      }';
            if (count($miqaat_audios) - 1) {
              $audio_table2 .= ',';
            }

            if ($audio['event_time'] == 'pre') {
              $audio_pre = $audio_table;
              $audio_pre2 .= $audio_table2;
            } else if ($audio['event_time'] == 'in') {
              $audio_in = $audio_table;
              $audio_in2 .= $audio_table2;
            } else if ($audio['event_time'] == 'post') {
              $audio_post = $audio_table;
              $audio_post2 .= $audio_table2;
            }
          }
        }

        if ($miqaat_stationaries) {
          foreach ($miqaat_stationaries as $stat_files) {

            //$filearray = explode('.', $stat_files['stationary_url']);
            //$ext = explode('-', $filearray[0]);
            //$file_name = $ext[1];
            $file_name = $stat_files['name'];
            
            $stat_category = explode(',', $stat_files['category']);
            foreach ($stat_category as $st_cat){
              $stat_categories[] = $st_cat;
            }
            
            $stat_cls = implode(' ', $stat_category);
            $logo_image = ($stat_files['logo_image']) ? $stat_files['logo_image'] : 'images/waaz_pdf.png';
            $stat_table = '<div class="col-md-4 all '. $stat_cls .'">
              <a href="http://docs.google.com/gview?embedded=true&url='.SERVER_PATH.'upload/miqaat_upload/stationary/' . $stat_files['stationary_url'] . '" target="_blank">
                  <div class="istibsaar-boxes text-center">
                    <img src="' . SERVER_PATH . $logo_image . '" class="img-responsive" style="max-height: 100px; margin: 0 auto" >
                    <div class="istibsaar-boxes-bottom">
                      <p class="text-center">' . $file_name . '</p>
                    </div>
                  </div>
              </a>
                </div>';

            if ($stat_files['event_time'] == 'pre') {
              $stat_pre .= $stat_table;
            } else if ($stat_files['event_time'] == 'in') {
              $stat_in .= $stat_table;
            } else if ($stat_files['event_time'] == 'post') {
              $stat_post .= $stat_table;
            }
          }
          $stationary_categories = array_unique($stat_categories);
        }

        if ($miqaat_ohbats) {
          foreach ($miqaat_ohbats as $mo) {

            $ohbat_table = '<div class="col-md-4">
                <a href="' . SERVER_PATH . 'daily_sentences_ar.php?mid=' . $miqat_id . '">
              <div class="istibsaar-boxes text-center">
                <img src="' . SERVER_PATH . 'images/beer.png" class="img-responsive" style="max-height: 100px; margin: 0 auto" >
                <div class="istibsaar-boxes-bottom">
                  <p class="text-center">Ohbat Kalemaat Nooraniyah</p>
                </div>
              </div>
                </a>
            </div>';

            if ($mo['event_time'] == 'pre') {
              $ohbat_pre = $ohbat_table;
            } else if ($mo['event_time'] == 'in') {
              $ohbat_in = $ohbat_table;
            } else if ($mo['event_time'] == 'post') {
              $ohbat_post = $ohbat_table;
            }
          }
        }

        if ($miqat_id == 5) {
          $tasavvuraat_in = '<div class="col-md-4">
                <a href="' . SERVER_PATH . 'tasavvuraat_araz.php">
              <div class="istibsaar-boxes text-center">
                <i class="fa fa-microphone  fa-5x dark-blue"></i>
                <div class="istibsaar-boxes-bottom">
                  <p class="text-center">Tasawwuraat Araz</p>
                </div>
              </div>
                </a>
            </div>';
          
        }


        if ($miqaat_sentences) {
          foreach ($miqaat_sentences as $ms) {

            $sentences_table = '<div class="col-md-4">
                <a href="' . SERVER_PATH . 'istibsaar/ashara-mubarakah/">
              <div class="istibsaar-boxes text-center">
                <img src="' . SERVER_PATH . 'images/beer.png" class="img-responsive" style="max-height: 100px; margin: 0 auto" >
                <div class="istibsaar-boxes-bottom">
                  <p class="text-center">Sentences</p>
                </div>
              </div>
                </a>
            </div>';

            if ($ms['event_time'] == 'pre') {
              $sentences_pre = $sentences_table;
            } else if ($ms['event_time'] == 'in') {
              $sentences_in = $sentences_table;
            } else if ($ms['event_time'] == 'post') {
              $sentences_post = $sentences_table;
            }
          }
        }

        if ($miqaat_quizes) {
          foreach ($miqaat_quizes as $mq) {

            $quiz_table = '<div class="col-md-4">
                <a href="quiz_group_list.php">
              <div class="istibsaar-boxes text-center">
                <i class="fa fa-list-alt   fa-5x dark-blue"></i>
                <div class="istibsaar-boxes-bottom">
                  <p class="text-center">Take Quiz</p>
                </div>
              </div>
                </a>
            </div>';

            if ($mq['event_time'] == 'pre') {
              $quiz_pre = $quiz_table;
            } else if ($mq['event_time'] == 'in') {
              $quiz_in = $quiz_table;
            } else if ($mq['event_time'] == 'post') {
              $quiz_post = $quiz_table;
            }
          }
        }

        if ($miqaat_courses) {
          $course_table = '';
          $first_child = TRUE;
          $tmp_quiz = new Quiz();
          foreach ($miqaat_courses as $mc) {
            $course_id = $mc['course_id'];
            $user_its = $_SESSION['user_its'];
            
            $course_questions = $tmp_quiz->get_courses_question_by_course_id($course_id);
            $user_answers = $tmp_quiz->get_user_answers_for_course($course_id, $user_its);
            
            if($user_answers) {
              $correct_ans = 0;
              foreach($user_answers as $ua){
                ($ua['correct'] == 1) ? $correct_ans++ : '';
              }
              $user_quiz_interaction = '<p class="text-center" style="padding-left: 25px;"><span class="pull-left">' . $mc['course_title'] . '</span><span class="pull-right">' . $correct_ans . ' / ' . count($course_questions) . '</span><div class="clearfix"></div></p>';
            } else {
              $user_quiz_interaction = '<p class="text-center">' . $mc['course_title'] . '</p>';
            }
            if($mc['active'] == 1) {
              $color = 'dark-blue';
              $img = SERVER_PATH . 'assets/img/icon_tizkaar_act.png';
            
            if ($first_child) {
              $color = 'orange';
              $first_child = FALSE;
              //$img = SERVER_PATH . 'assets/img/icon_tizkaar.png';
            }
            
            $course_table = '<div class="col-md-4">
                <a href="' . SERVER_PATH . 'take_courses.php?course_id=' . $mc['course_id'] . '">
              <div class="istibsaar-boxes text-center">
                <img class="img-responsive" style="max-height: 100px; margin: 0 auto" src="' . $img . '">
                <div class="istibsaar-boxes-bottom">' . $user_quiz_interaction . '<p class="text-center black_font">' . $mc['description'] . '</p>
                </div>
              </div>
                </a>
            </div>';

            if ($mc['event_time'] == 'pre') {
              $course_pre .= $course_table;
            } else if ($mc['event_time'] == 'in') {
              $course_in .= $course_table;
            } else if ($mc['event_time'] == 'post') {
              $course_post .= $course_table;
            }
            }
          }
        }

        $no_videos = '<p style="margin-left:10px;">No Videos</p>';
        $no_audios = '<p style="margin-left:10px;">No Audios</p>';
        $no_stationaries = '<p style="margin-left:10px;">No Stationaries</p>';
        ?>

        <div class="tab-content">
          <?php if ($miqaat_istibsaar['show_pre_event'] == '1') { ?>
            <div role="tabpanel" class="tab-pane <?php echo $make_pre_active; ?>" id="pre">
              <?php
              if ($stat_pre) {
                echo '<div class="static-show-button2 large-fonts col-xs-12 col-sm-12 pull-left">Downloads</div><div class="clearfix"></div>';
                $sp = 1;
                echo '<div class="black_font pull-right">';
                echo '<a href="#" class="black_font" id="all"> All </a> | ';
                foreach ($stationary_categories as $stat_cat){
                  echo '<a href="#" class="black_font" id="'.$stat_cat.'"> '. $stat_cat .' </a>';
                  if (count($stationary_categories) > $sp++) {
                    echo ' | ';
                  }
                }
                echo '</div>';
                echo '<div class="clearfix"><br></div>';
                echo $stat_pre;
                echo '<div class="clearfix"></div>';
              }

              if ($video_pre) {

                echo '<div class="static-show-button2 large-fonts col-xs-12 col-sm-12 pull-left">Videos</div><div class="clearfix"></div>
                  <div style="display:none;" class="html5gallery" data-skin="horizontal" data-width="290" data-height="200" data-resizemode="fill">' . $video_pre . '</div>';
                echo '<div class="clearfix"></div>';
              }

              if ($youtube_pre) {

                echo '<div class="static-show-button2 large-fonts col-xs-12 col-sm-12 pull-left">Videos</div><div class="clearfix"></div>';
                echo $youtube_pre;
                echo '<div class="clearfix"></div>';
              }

              if ($ohbat_pre || $sentences_pre || $quiz_pre || $course_pre) {
                echo '<div class="static-show-button2 large-fonts col-xs-12 col-sm-12 pull-left">Interaction</div><div class="clearfix"></div>';

                if ($ohbat_pre) {
                  echo $ohbat_pre;
                }
                if ($sentences_pre) {
                  echo $sentences_pre;
                }
                if ($quiz_pre) {
                  echo $quiz_pre;
                }
                if ($course_pre) {
                  echo $course_pre;
                }
                echo '<div class="clearfix"></div>';
              }

              if ($audio_pre) {

                echo '<div class="static-show-button2 large-fonts col-xs-12 col-sm-12 pull-left">Audios</div><div class="clearfix"></div>' . $audio_pre;
                echo '<div class="clearfix"></div>';
              }
              ?>
            </div>
          <?php } ?>

          <?php
          if ($miqaat_istibsaar['show_in_event'] == '1') {
            ?>  
            <div role="tabpanel" class="tab-pane<?php echo $make_in_active; ?>" id="in">
              <?php
              if ($stat_in) {
                echo '<div class="static-show-button2 large-fonts col-xs-12 col-sm-12 pull-left">Downloads</div><div class="clearfix"></div>';
                $sn = 1;
                echo '<div class="black_font pull-right">';
                echo '<a href="#" class="black_font" id="all"> All </a> | ';
                foreach ($stationary_categories as $stat_cat){
                  echo '<a href="#" class="black_font" id="'.$stat_cat.'"> '. $stat_cat .' </a>';
                  if (count($stationary_categories) > $sn++) {
                    echo ' | ';
                  }
                }
                echo '</div>';
                echo '<div class="clearfix"><br></div>';
                echo $stat_in;
                echo '<div class="clearfix"></div>';
              }


              if ($video_in) {
                echo '<div class="static-show-button2 large-fonts col-xs-12 col-sm-12 pull-left">Videos</div><div class="clearfix"></div>
                <div style="display:none;" class="html5gallery" data-skin="horizontal" data-width="290" data-height="200" data-resizemode="fill">' . $video_in . '</div>';
                echo '<div class="clearfix"></div>';
              }

              if ($youtube_in) {
                echo '<div class="static-show-button2 large-fonts col-xs-12 col-sm-12 pull-left">Videos</div><div class="clearfix"></div>';
                echo $youtube_in;
                echo '<div class="clearfix"></div>';
              }

              if ($ohbat_in || $sentences_in || $quiz_in || $course_in) {
                echo '<div class="static-show-button2 large-fonts col-xs-12 col-sm-12 pull-left">Interaction</div><div class="clearfix"></div>';

                if ($ohbat_in) {
                  echo $ohbat_in;
                }
                if ($sentences_in) {
                  echo $sentences_in;
                }
                if ($quiz_in) {
                  echo $quiz_in;
                }
                /*if ($tasavvuraat_in) {
                  echo $tasavvuraat_in;
                }*/
                if ($course_in) {
                  echo $course_in;
                }
                echo '<div class="clearfix"></div>';
              }

              if ($audio_in) {

                echo '<div class="static-show-button2 large-fonts col-xs-12 col-sm-12 pull-left">Audios</div><div class="clearfix"></div>' . $audio_in;
                echo '<div class="clearfix"></div>';
              }
              ?>

            </div>
          <?php } ?>

          <?php
          if ($miqaat_istibsaar['show_post_event'] == '1') {
            ?>
            <div role="tabpanel" class="tab-pane<?php echo $make_post_active; ?>" id="post">
              <?php
              if ($stat_post) {
                echo '<div class="static-show-button2 large-fonts col-xs-12 col-sm-12 pull-left">Downloads</div><div class="clearfix"></div>';
                $st = 1;
                echo '<div class="black_font pull-right">';
                echo '<a href="#" class="black_font" id="all"> All </a> | ';
                foreach ($stationary_categories as $stat_cat){
                  echo '<a href="#" class="black_font" id="'.$stat_cat.'"> '. $stat_cat .' </a>';
                  if (count($stationary_categories) > $st++) {
                    echo ' | ';
                  }
                }
                echo '</div>';
                echo '<div class="clearfix"><br></div>';
                echo $stat_post;
                echo '<div class="clearfix"></div>';
              }

              if ($video_post) {
                echo '<div class="static-show-button2 large-fonts col-xs-12 col-sm-12 pull-left">Videos</div><div class="clearfix"></div>
                <div style="display:none;" class="html5gallery" data-skin="horizontal" data-width="290" data-height="200" data-resizemode="fill">' . $video_post . '</div>';
                echo '<div class="clearfix"></div>';
              }

              if ($youtube_post) {
                echo '<div class="static-show-button2 large-fonts col-xs-12 col-sm-12 pull-left">Videos</div><div class="clearfix"></div>';
                echo $youtube_post;
                echo '<div class="clearfix"></div>';
              }

              if ($ohbat_post || $sentences_post || $quiz_post || $course_post) {
                echo '<div class="static-show-button2 large-fonts col-xs-12 col-sm-12 pull-left">Interaction</div><div class="clearfix"></div>';

                if ($ohbat_post) {
                  echo $ohbat_post;
                }
                if ($sentences_post) {
                  echo $sentences_post;
                }
                if ($quiz_post) {
                  echo $quiz_post;
                }
                if ($course_post) {
                  echo $course_post;
                }
                echo '<div class="clearfix"></div>';
              }

              if ($audio_post) {

                echo '<div class="static-show-button2 large-fonts col-xs-12 col-sm-12 pull-left">Audios</div><div class="clearfix"></div>' . $audio_post;
                echo '<div class="clearfix"></div>';
              }
              ?>
            </div>
          <?php } ?>
        </div>
      </div>
    </div>
    <div class="clearfix"></div>
  </form>
</div>

<script src="<?php echo SERVER_PATH; ?>templates/js/jquery.prettyPhoto.js" type="text/javascript" charset="utf-8"></script>
<script src="<?php echo SERVER_PATH; ?>bootstrap/js/bootstrap.min.js"></script>
<script src="<?php echo SERVER_PATH; ?>templates/js/jquery.slicknav.js"></script>
<script src="<?php echo SERVER_PATH; ?>templates/js/jquery.confirm.min.js"></script>
<script src="<?php echo SERVER_PATH; ?>templates/js/jquery.nivo.slider.pack.js"></script>
<script src="<?php echo SERVER_PATH; ?>templates/js/select2/select2.full.min.js"></script>
<script src="<?php echo SERVER_PATH; ?>templates/js/scripts.js"></script>

<script>
  $(document).ready(function () {
    $('[data-toggle="popover"]').popover({
      placement: 'bottom'
    });
    $('body').on('click', function (e) {
      $('[data-toggle="popover"]').each(function () {
        //the 'is' for buttons that trigger popups
        //the 'has' for icons within a button that triggers a popup
        if (!$(this).is(e.target) && $(this).has(e.target).length === 0 && $('.popover').has(e.target).length === 0) {
          $(this).popover('hide');
        }
      });
    });
  });
</script>

<script>
  $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
    e.target // newly activated tab
    e.relatedTarget // previous active tab
  })
</script>

<script type="text/javascript" src="<?php echo SERVER_PATH; ?>html5gallery/html5gallery.js"></script>

<link href="<?php echo SERVER_PATH; ?>dist/skin/blue.monday/css/jplayer.blue.monday.min.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="<?php echo SERVER_PATH; ?>dist/jplayer/jquery.jplayer.min.js"></script>
<script type="text/javascript" src="<?php echo SERVER_PATH; ?>dist/add-on/jplayer.playlist.min.js"></script>

<script type="text/javascript">
//<![CDATA[
  $(document).ready(function () {
<?php if ($audio_pre2) { ?>
      new jPlayerPlaylist({
        jPlayer: "#jquery_jplayer_1",
        cssSelectorAncestor: "#jp_container_1"
      }, [
  <?php
  if ($audio_pre2) {
    echo $audio_pre2;
  }
  ?>
      ], {
        swfPath: "../../dist/jplayer",
        supplied: "oga, mp3",
        wmode: "window",
        useStateClassSkin: true,
        autoBlur: false,
        smoothPlayBar: true,
        keyEnabled: true
      });

<?php } if ($audio_in2) { ?>
      new jPlayerPlaylist({
        jPlayer: "#jquery_jplayer_2",
        cssSelectorAncestor: "#jp_container_2"
      }, [
  <?php
  if ($audio_in2) {
    echo $audio_in2;
  }
  ?>
      ], {
        swfPath: "../../dist/jplayer",
        supplied: "oga, mp3",
        wmode: "window",
        useStateClassSkin: true,
        autoBlur: false,
        smoothPlayBar: true,
        keyEnabled: true
      });

<?php } if ($audio_post2) { ?>
      new jPlayerPlaylist({
        jPlayer: "#jquery_jplayer_3",
        cssSelectorAncestor: "#jp_container_3"
      }, [
  <?php
  if ($audio_post2) {
    echo $audio_post2;
  }
  ?>
      ], {
        swfPath: "../../dist/jplayer",
        supplied: "oga, mp3",
        wmode: "window",
        useStateClassSkin: true,
        autoBlur: false,
        smoothPlayBar: true,
        keyEnabled: true
      });

<?php 
  } 
    if($stationary_categories){
      ?>
      $('#all').click(function(e){
        e.preventDefault();
        $('.all').show(300);
      });
      <?php
      foreach ($stationary_categories as $cnt){
  ?>
     $('#<?php echo $cnt; ?>').click(function(e){
       e.preventDefault();
       $('.all').hide(300);
       $('.<?php echo $cnt; ?>').show(300);
     });
  <?php
      }
    }
  ?>
  });
  //]]>
</script>

<style>
  .miqaat_stat:hover{
    background-color: #549BC7;
  }
  .black_font{
    color:#000 !important;
  }
</style>

<?php
require_once 'inc/inc.footer.php';
?>
<div class="container white-bg" style="padding:0px;">
<!-- <div class="footer-upper">
<div class="clearfix"></div>
<div class="col-md-4 contents">
	<h2>About Talabulilm</h2>
	<p>With the Raza and Dua mubarak of Aaqa maula tus this is an initiative for combining the efforts of all the students and the faculties for the upliftment of education in mumineen.</p></div>
<div class="col-md-4 contents">
<h2>sitemap</h2>
	<ul class="footer-links">
<li><a href="#">Home</a></li>
<li><a href="#">About Us</a></li>
<li><a href="#">News events</a></li>
<li><a href="#">Admissions</a></li>
<li><a href="#">Contact us</a></li>
<li><a href="#">Activities</a></li>
<li><a href="#">Istibsaar</a></li>
<li><a href="#">Academic institutes</a></li>
<li><a href="#">Marahil</a></li>
<li><a href="#">Istifaadah</a></li>
	</ul>
	</div>
<div class="col-md-4 contents">
	<h2>Contact uS</h2>
	<p>Mahad al-Hasanaat al-Burhaniyah<br>
Al Jamea tus Saifiyah<br>Al-Shu'oon al-'Aammah, Education<br>
department.<br><br>
Taj Building, B 2, Fort, Dr. D.N Road, Mumbai 400 001h<br>
Telephone: 022 4921 6552, 4921 6515<br>
Email: <a href="mailto:education@alvazarat.org">education@alvazarat.org</a></p></div>
</div>
<div class="clearfix"></div> -->

<footer>
<!--  <form class="form1 white" method="post" action="">
    <div class="col-md-12">
      <h2>Query Form :</h2><br>
      <div class="clearfix"></div>  do not delete 
      <div class="col-md-4">
        <select name="f_ques" class="form-control" required> 
          <option value="">Select Any</option>
          <option value="1">Course not in the List</option>
          <option value="2">Institute not in the List</option>
          <option value="3">Country not in the List</option>
          <option value="4">City not in the List</option>
        </select>
      </div>
      <div class="col-md-4">
        <textarea name="f_ans" class="form-control" required></textarea>
      </div>
      <div class="col-md-2">
        <input type="submit" value="Submit" name="que_submit" class="btn btn-block">
      </div>
    </div>
  </form>-->
  <p>&copy; Mahad al-Hasanaat al-Burhaniyah, All Rights Reserved.<span>Designed by:  <a href="http://bohradevelopers.com" target="_blank">Bohradevelopers</a><br>Developed By: <a href="http://www.matrixsoftwares.com/" target="_blank">Matrix Software Solutions</a></span></p>
  <div class="clearfix"></div>
</footer>
</div>
<!-- Javascript-->
<!-- ====================================================================================================== -->
<script>
  $('.select-input').select2({ width: '100%' });
  
  setTimeout(function() {
    $(".alert-message").fadeTo(500, 0).slideUp(500, function(){
        $(this).remove(); 
    });
  }, 60000);

  $('.carousel').carousel({
    interval: 5000
  });

</script>
</body>
</html>

