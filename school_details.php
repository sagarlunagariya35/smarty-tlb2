<?php
require 'smarty/libs/Smarty.class.php';
require_once 'session.php';
require_once 'requires_login.php';
require_once 'classes/class.user.php';

$user = new mtx_user;
$user->loaduser($_SESSION[USER_ITS]);

// Page bady class
$body_class = 'page-sub-page';

$group_id = $_GET['group'];
if (isset($_GET['s'])) {
  $skip = $_GET['s'];
} else {
  $skip = 'f';
}

if ($_SESSION['step'] != '2') {
  if ($_SESSION['step'] <= '2') {
    if ($_SESSION['step'] == '1' && $group_id != '1') {
      header('location: ' . SERVER_PATH . 'marhala-' . $group_id . '/' . get_marhala_slug($group_id) . '/raza-araz');
    }
  }
}

$filter_school_array = array('School run by Dawat e Hadiyah - Boys', 'School run by Dawat e Hadiyah - Girls', 'School run by Dawat e Hadiyah - Co-Ed', 'Private School - Boys', 'Private School - Girls', 'Private School - Co-Ed', 'Public School - Boys', 'Public School - Girls', 'Public School - Co-Ed');

$ary_standard = get_standards_by_marhala($group_id);

switch ($group_id) {
  case 1:
    $panel_heading = 'Pre-Primary Araz';
    $std_heading = 'Pre-Primary';
    $panel_heading_ar = 'الحضانة';
    $kalemaat_nooraniyah = ' تمارا بچه ؤ مارا بچه ؤ  چھے، اهني قدر جانو خدا ني نعمة  چھے';
    $form_heading = '';
    break;
  case 2:
    $panel_heading = 'Primary Araz';
    $std_heading = 'Primary';
    $panel_heading_ar = 'الابتدائية الأولى';
    $kalemaat_nooraniyah = 'تميں هميشھ علم طلب كرجو، اچھا ما اچھي تعليم ليجو،  education ليجو';
    break;
  case 3:
    $panel_heading = 'Std 5th to Std 7th Araz';
    $std_heading = 'Std 5th to Std 7th';
    $panel_heading_ar = 'الابتدائية الثانية';
    $kalemaat_nooraniyah = 'تعليم صحيح اسلامي منطلق سي هوئے  ايمان ني منزل  اهني منزل هوئے ';
    break;
  case 4:
    $panel_heading = 'Std 8th to 10th Araz';
    $std_heading = 'Std 8th to Std 10th';
    $panel_heading_ar = 'الثانوية الأولى';
    $kalemaat_nooraniyah = 'تمام علوم نو جملة قراْن، لوگو جھ نے  دنيا نا علوم كهے   چھے يھ بھي سگلا قراْن ما  چھے';
    break;
  case 5:
    $panel_heading = 'Std 11th - 12th Araz';
    $std_heading = 'Std 11th - 12th';
    $panel_heading_ar = 'الثانوية الثانية';
    $kalemaat_nooraniyah = ' تعليم نا ضمن ما زبان ني اهمية چھے، لسان الدعوة سگلا بولے انے فرزندو نے سكھاوے';
    break;
  case 6:
    $panel_heading = 'Graduation Araz';
    $std_heading = 'Graduation';
    $panel_heading_ar = 'البكالوريا';
    $kalemaat_nooraniyah = 'قوم  انے وطن ني بظظبودگي واسطے هر ممكن سعي كرے';
    break;
  case 7:
    $panel_heading = 'Post Graduation Araz';
    $std_heading = 'Post Graduation';
    $panel_heading_ar = 'الماجيستار';
    $kalemaat_nooraniyah = 'ويپار واسطے  تعليم،  ويپار برابر كرو، دنيا ما ويپاري قوم اپن مشهور  چھے';
    break;
  case 8:
    $panel_heading = 'Diploma Araz';
    $std_heading = 'Diploma';
    $panel_heading_ar = 'الدبلوم';
    $kalemaat_nooraniyah = 'ويپار كيم كرو تھ سيكھي لو، اهنا واسطے  گهنو كروو جوئيے، degree  سي كام ليوائي';
    break;
}
require_once './classes/class.araz.php';

if (isset($_POST['submit'])) {
  //store in session only
  //$araz = new mtx_araz();

  $_SESSION['marhala_id'] = $group_id;
  $_SESSION['madrasah_country'] = $_POST['madrasah_country'];
  $_SESSION['school_city'] = $_POST['madrasah_city'];
  $_SESSION['school_standard'] = $_POST['school_standard'];
  $_SESSION['school_name'] = $_POST['school_name'];
  $_SESSION['school_filter'] = $_POST['filter_school'];
  $_SESSION['school_pincode'] = $_POST['school_pincode'];

  $_SESSION['step'] = '3';

  header('location: ' . SERVER_PATH . 'marhala-' . $group_id . '/' . get_marhala_slug($group_id) . '/ques/');
}

if (isset($_POST['save_araz'])) {
  $_SESSION['marhala_id'] = $group_id;
  $_SESSION['madrasah_country'] = $_POST['madrasah_country'];
  $_SESSION['school_city'] = $_POST['madrasah_city'];
  $_SESSION['school_standard'] = $_POST['school_standard'];
  $_SESSION['school_name'] = $_POST['school_name'];
  $_SESSION['school_filter'] = $_POST['filter_school'];
  $_SESSION['school_pincode'] = $_POST['school_pincode'];

  $_SESSION['step'] = '3';
  
  $araz_data = serialize($_SESSION);

  $save_araz = save_tlb_araz($_SESSION[USER_ITS], $group_id, $araz_data);
  if ($save_araz) {
    $_SESSION[SUCCESS_MESSAGE] = "Your Araz Data Save Successfully";
  }else {
    $_SESSION[ERROR_MESSAGE] = "Error In Saving Data";
  }
}

require_once 'inc/inc.header2.php';
require_once 'inc/inc.footer.php';

if (@$_SESSION['madrasah_country'] != '') {
  $scl_country = $_SESSION['madrasah_country'];
} else if ($my_araz['madrasah_country'] != '') {
  $scl_country = $my_araz['madrasah_country'];
} else {
  $scl_country = $user->get_country();
}


$countries = get_all_countries();
$country_iso = get_country_iso_by_name($scl_country);
$ary_cities = array_sort(get_all_city_by_country_iso($country_iso[0]['ISO2']), 'city', SORT_ASC);

$url = SERVER_PATH . 'marhala-' . $group_id . '/' . get_marhala_slug($group_id) . '/school/';

$smarty = new Smarty;

// Header / Session variables
$smarty->assign("server_path", SERVER_PATH);
$smarty->assign("user", $user);
$smarty->assign("ary_standard", $ary_standard);
$smarty->assign("group_id", $group_id);
$smarty->assign("panel_heading", $panel_heading);
$smarty->assign("panel_heading_ar", $panel_heading_ar);
$smarty->assign("std_heading", $std_heading);
$smarty->assign("kalemaat_nooraniyah", $kalemaat_nooraniyah);
$smarty->assign("filter_school_array", $filter_school_array);
$smarty->assign("scl_country", $scl_country);
$smarty->assign("countries", $countries);
$smarty->assign("ary_cities", $ary_cities);
$smarty->assign("school_filter", $_SESSION['school_filter']);
$smarty->assign("msb_student_info", $_SESSION['msb_student_info']);
$smarty->assign("madrasah_darajah", $_SESSION['madrasah_darajah']);
$smarty->assign("school_standard", $_SESSION['school_standard']);
$smarty->assign("school_pincode", $_SESSION['school_pincode']);
$smarty->assign("school_city", $_SESSION['school_city']);
$smarty->assign("school_name", $_SESSION['school_name']);
$smarty->assign("success_message", $_SESSION['SUCCESS_MESSAGE']);
$smarty->assign("error_message", $_SESSION['ERROR_MESSAGE']);
$smarty->assign("my_araz", $my_araz);
$smarty->assign("url", $url);

$smarty->display('school_details.tpl');