<?php
require 'smarty/libs/Smarty.class.php';
require_once 'session.php';
require_once 'requires_login.php';
require_once 'classes/class.qrn_mumin.php';
require_once 'classes/class.qrn_muhafiz.php';
require_once 'classes/class.qrn_tasmee.php';
require_once 'classes/class.user.php';

$qrn_mumin = new Qrn_Mumin();
$qrn_muhafiz = new Qrn_Muhafiz();
$qrn_tasmee = new Qrn_Tasmee();
$user_logedin = new mtx_user();

$timestamp = date('Y-m-d');

if (isset($_POST['delete_schedule'])) {
  $delete_schedule_id = $_POST['delete_schedule'];

  $delete_schedule = $qrn_tasmee->delete_time_schedule($delete_schedule_id);
  
  if ($delete_schedule) {
    $_SESSION[SUCCESS_MESSAGE] = 'Time schedule deleted successfully.';
  } else {
    $_SESSION[ERROR_MESSAGE] = 'Error encounter while deleting time schedule';
  }
}

if (isset($_POST['set_schedule'])) {
  $sch_mumin_its = $_POST['sch_mumin_its'];
  $sch_days = implode(', ', $_POST['sch_days']);
  $sch_hour = $_POST['sch_hour'];
  $sch_time = $_POST['sch_time'];
  
  $insert_schedule = $qrn_tasmee->insert_mumin_time_schedule($_SESSION[USER_ITS], $sch_mumin_its, $sch_days, $sch_hour, $sch_time, $timestamp);
  
  if ($insert_schedule) {
    $_SESSION[SUCCESS_MESSAGE] = 'Time schedule inserted successfully.';
  } else {
    $_SESSION[ERROR_MESSAGE] = 'Error encounter while inserting time schedule';
  }
}

$select_mumin = $qrn_mumin->get_all_mumin_by_muhafiz($_SESSION[USER_ITS]);
$schedule_records = $qrn_tasmee->get_my_time_schedule($_SESSION[USER_ITS]);

require_once 'inc/inc.header2.php';
require_once 'inc/inc.footer.php';

$smarty = new Smarty;

// Header / Session variables
$smarty->assign("server_path", SERVER_PATH);
$smarty->assign("mumin_data_header", $mumin_data_header);
$smarty->assign("muhafiz_data_header", $muhafiz_data_header);
$smarty->assign("select_mumin", $select_mumin);
$smarty->assign("schedule_records", $schedule_records);
$smarty->assign("qrn_tasmee", $qrn_tasmee);
$smarty->assign("success_message", $_SESSION['SUCCESS_MESSAGE']);
$smarty->assign("error_message", $_SESSION['ERROR_MESSAGE']);

$smarty->display('qrn_ikhwaan_report.tpl');