<?php
require_once 'session.php';
require_once 'requires_login.php';
require_once 'classes/class.user.php';
require_once 'classes/gen_functions.php';
// Page body class
$body_class = 'page-sub-page';

$user = new mtx_user;
$user->loaduser($_SESSION[USER_ITS]);


$city = $_GET["city"];

$ary_mnames = get_madrasah_list_by_country($city);
  
echo '<select name="mname" class="select-input">
                  <option value="">Select Madrasah</option>';

foreach ($ary_mnames as $mname) {
  ?>
  <option value="<?php echo $mname['madrasah_name_eng']; ?>" <?php echo ($mname['madrasah_name_eng'] == $user->get_city()) ? 'checked' : ''; ?> ><?php echo $mname['madrasah_name_eng']; ?></option>
  <?php
}
  echo '</select>';
?>