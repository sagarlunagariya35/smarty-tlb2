<?php
require_once 'session.php';
require_once 'requires_login.php';
require_once 'admin/classes/class.ohbat_sentences.php';

$miqat_id = (int) $_GET['mid'];
$sid = (isset($_GET['sid'])) ? (int) $_GET['sid'] : FALSE;

$cls_os = new Mtx_Ohbat_Sentences();
$sentences = array_reverse($cls_os->get_all_articles_by_miqat_id($miqat_id));

$miqaat_istibsaar = get_single_miqaat_istibsaar($miqat_id);

// calculate the current day
$today = strtotime(date('Y-m-d'));
$start_date = ($miqaat_istibsaar['year'] == '1437') ? strtotime("2015-10-6") : strtotime("2014-10-6");
$show_days_left = ($miqaat_istibsaar['year'] == '1437') ? TRUE : FALSE;
$datediff = $today - $start_date;
$sr = floor($datediff / (86400));
$days_left = 7 - $sr;
// reset the sr to the user selected question, if its below the current sr
$show_id = ($sid && $sid <= $sr) ? $sid - 1 : $sr;

// If the $show_id is greater then the count of the entries found in database, bring it down to the total count of the database entries
(count($sentences) <= $show_id) ? $show_id = count($sentences) - 1 : '';

$day_num = $sentences[$show_id]['sr'];
$title = $sentences[$show_id]['title'];
$bayan = $sentences[$show_id]['bayan'];
$bayan_title = $sentences[$show_id]['bayan_title'];
$bayan_image = $sentences[$show_id]['ar_bayan_image'];
$bayan_title_image = $sentences[$show_id]['ar_bayan_title_image'];
$question = $sentences[$show_id]['question'];
$question_image = $sentences[$show_id]['ar_ques_image'];
$words = $sentences[$show_id]['word_meaning'];
$words_image = $sentences[$show_id]['ar_word_image'];

$miqaat_istibsaars = get_all_miqaat_istibsaar($miqaat_istibsaar['year']);
$mawaqeet = get_all_mawaqeet_by_year($miqaat_istibsaar['year']);
$tabs = array();

($mawaqeet['show_pre_event']) ? $tabs[] = 'pre' : '';
($mawaqeet['show_in_event']) ? $tabs[] = 'in' : '';
($mawaqeet['show_post_event']) ? $tabs[] = 'post' : '';

function show_audio_player($audio_file) {
  echo "<div class=\"col-xs-1 col-md-1 text-center pull-left\">
          <i class=\"fa fa-play-circle-o fa-3x play\"OnClick=pa('$audio_file')></i>
        </div>";
}

require_once 'inc/inc.header2.php';
?>

<!-- Contents -->
<!-- ====================================================================================================== -->
<style>
  .forms1 p{
    text-align: right;
  }
</style>
<div class="container white-bg">
  <div class="col-md-12 col-sm-12">

    <div class="page-title">
      <p style="margin-top:5px;">
        <a href="#" class="active"><?php echo $miqaat_istibsaar['miqaat_title'] . ' ' . $miqaat_istibsaar['year']; ?> H</a></p>
      <h1><?php echo $miqaat_istibsaar['miqaat_title'] . ' ' . $miqaat_istibsaar['year']; ?> H<span class="alfatemi-text"></span></h1>
    </div>
  </div>
  <div class="clearfix"></div> <!-- do not delete -->
  <form class="forms1 white" name="raza_form" method="post">
    <div class="col-md-3 col-sm-12 hidden-xs">
      <div class="profile-box-static">
        <h3 class="uppercase text-center">Mawaqeet</h3>
      </div>
      <div class="profile-box-static-bottom">
        <?php
        foreach ($mawaqeet as $mqt) {
          echo "<a href=\"" . SERVER_PATH . "istibsaar/$mqt[id]/$mqt[slug]/$mqt[year]/" . "\">» $mqt[miqaat_title]<hr></a>";
        }

        $make_pre_active = '';
        $make_in_active = '';
        $make_post_active = '';

        if ($miqaat_istibsaar['show_pre_event'] == '1') {
          $make_pre_active = ' active';
        } elseif ($miqaat_istibsaar['show_in_event'] == '1') {
          $make_in_active = ' active';
        } elseif ($miqaat_istibsaar['show_post_event'] == '1') {
          $make_post_active = ' active';
        }
        ?>
      </div>
    </div>

    <div class="col-md-9 col-sm-12">
      <div class="row">
        <?php
        $audio_file = 'istibsaar/' . $miqaat_istibsaar['year'] . '/ohbat/' . $day_num . '/t.mp3';
        $audio_path = SERVER_PATH . '/istibsaar/' . $miqaat_istibsaar['year'] . '/ohbat/' . $day_num . '/t.mp3';
        if (file_exists($audio_file)) {
          show_audio_player($audio_path);
        }
        ?>
        <div style="background-color: #549BC7;" class="blue-box1 blue-box1-level col-md-10 col-sm-10 col-xs-11 text-center pull-right" dir="rtl">
          <span class="title lsd large-fonts" dir="rtl"><?php echo $title; ?></span>
        </div>
      </div>
      <div class="clearfix"></div>
      <?php
      // Check if the sentence has a bayan title or an image. If so then display them
      if ($bayan_title_image) {
        ?>
        <div class="row">
          <?php
          $audio_file = 'istibsaar/' . $miqaat_istibsaar['year'] . '/ohbat/' . $day_num . '/i.mp3';
          $audio_path = SERVER_PATH . '/istibsaar/' . $miqaat_istibsaar['year'] . '/ohbat/' . $day_num . '/i.mp3';
          if (file_exists($audio_file)) {
            show_audio_player($audio_path);
          }
          ?>
          <div class="col-md-10 col-sm-10 col-xs-11 text-center pull-right">
            <img class="img-responsive img-center" src="<?php echo 'upload/ohbat_upload/' . $bayan_title_image; ?>" >
          </div>
          <div class="clearfix"></div>
        </div>
        <?php
      } elseif ($bayan_title) {
        ?>
        <div class="row">
          <?php
          $audio_file = 'istibsaar/' . $miqaat_istibsaar['year'] . '/ohbat/' . $day_num . '/i.mp3';
          $audio_path = SERVER_PATH . '/istibsaar/' . $miqaat_istibsaar['year'] . '/ohbat/' . $day_num . '/i.mp3';
          if (file_exists($audio_file)) {
            show_audio_player($audio_path);
          }
          ?>
          <div class="blue-box1 blue-box1-level col-md-10 col-sm-10 col-xs-11 text-right pull-right" dir="rtl">
            <span class="title lsd large-fonts" style=" text-align: right !important;"><?php echo $bayan_title; ?></span>
          </div>
          <div class="clearfix"></div>
        </div>
        <?php
      }

      // Now do same with bayan
      if ($bayan_image) {
        ?>
        <div class="row">
          <?php
          $audio_file = 'istibsaar/' . $miqaat_istibsaar['year'] . '/ohbat/' . $day_num . '/b.mp3';
          $audio_path = SERVER_PATH . '/istibsaar/' . $miqaat_istibsaar['year'] . '/ohbat/' . $day_num . '/b.mp3';
          if (file_exists($audio_file)) {
            echo"this";
            show_audio_player($audio_path);
            echo"that";
          }
          ?>
          <div class="col-md-10 col-sm-10 col-xs-11 text-center pull-right" dir="rtl">
            <img class="img-responsive img-center" src="<?php echo 'upload/ohbat_upload/' . $bayan_image; ?>" >
          </div>
          <div class="clearfix"></div>
        </div>
        <?php
      } elseif ($bayan) {
        ?>
        <div class="row">
          <?php
          $audio_file = 'istibsaar/' . $miqaat_istibsaar['year'] . '/ohbat/' . $day_num . '/b.mp3';
          $audio_path = SERVER_PATH . '/istibsaar/' . $miqaat_istibsaar['year'] . '/ohbat/' . $day_num . '/b.mp3';
          if (file_exists($audio_file)) {
            show_audio_player($audio_path);
          }
          ?>
          <div class="blue-box1 blue-box1-level col-md-10 col-sm-10 col-xs-11 text-right pull-right" dir="rtl">
            <span class="title lsd large-fonts" style=" text-align: right !important;"><?php echo $bayan; ?></span>
          </div>
          <div class="clearfix"></div>
        </div>
        <?php
      }

      // Now do same with question
      if ($question_image) {
        ?>
        <div class="row visible-xs">
          <?php
          $audio_file = 'istibsaar/' . $miqaat_istibsaar['year'] . '/ohbat/' . $day_num . '/q.mp3';
          $audio_path = SERVER_PATH . '/istibsaar/' . $miqaat_istibsaar['year'] . '/ohbat/' . $day_num . '/q.mp3';
          if (file_exists($audio_file)) {
            show_audio_player($audio_path);
          }
          ?>
          <div class="col-md-10 col-sm-10 col-xs-11 text-center pull-right" dir="rtl">
            <img class="img-responsive img-center" src="<?php echo 'upload/ohbat_upload/' . $question_image; ?>" >
          </div>
          <div class="clearfix"></div>
        </div>
        <?php
      }
      if ($question) {
        ?>
        <div class="row hidden-xs">
          <?php
          $audio_file = 'istibsaar/' . $miqaat_istibsaar['year'] . '/ohbat/' . $day_num . '/q.mp3';
          $audio_path = SERVER_PATH . '/istibsaar/' . $miqaat_istibsaar['year'] . '/ohbat/' . $day_num . '/q.mp3';
          if (file_exists($audio_file)) {
            show_audio_player($audio_path);
          }
          ?>
          <div style="margin:13px !important;" class="multitextbuttton blue-box1-button col-md-10 col-sm-10 col-xs-11 lsd large-fonts text-right pull-right" dir="rtl">
            <span class="title text-right" style=" text-align: right !important; border: none"><?php echo $question; ?></span>
          </div>
          <div class="clearfix"></div>
        </div>
        <?php
      }

      // Now do same with words
      if ($words_image) {
        ?>
        <div class="row">
          <?php
          $audio_file = 'istibsaar/' . $miqaat_istibsaar['year'] . '/ohbat/' . $day_num . '/w.mp3';
          $audio_path = SERVER_PATH . '/istibsaar/' . $miqaat_istibsaar['year'] . '/ohbat/' . $day_num . '/w.mp3';
          if (file_exists($audio_path)) {
            show_audio_player($audio_file);
          }
          ?>
          <div class="col-md-10 col-sm-10 col-xs-11 text-center pull-right" dir="rtl">
            <img class="img-responsive img-center" src="<?php echo 'upload/ohbat_upload/' . $words_image; ?>" >
          </div>
          <div class="clearfix"></div>
        </div>
        <?php
      } elseif ($words) {
        ?>
        <div class="row">
          <?php
          $audio_file = 'istibsaar/' . $miqaat_istibsaar['year'] . '/ohbat/' . $day_num . '/w.mp3';
          $audio_path = SERVER_PATH . '/istibsaar/' . $miqaat_istibsaar['year'] . '/ohbat/' . $day_num . '/w.mp3';
          if (file_exists($audio_file)) {
            show_audio_player($audio_path);
          }
          ?>
          <div class="col-md-10 col-sm-10 col-xs-11 text-right" dir="rtl" style="margin-top: 20px;">
            <span class="title black_font lsd large-fonts"><?php echo $words; ?></span>
          </div>
          <div class="clearfix"></div>
        </div>
        <?php
      }
      ?>

      <div class="clearfix"></div>
      <div class="row">
          <div class="col-xs-12 col-md-12 text-center">
        <?php
        $cnt = 1;
        $total = (count($sentences) <= $sr) ? count($sentences) : $sr + 1;
        while ($cnt <= $total) {
          $link = SERVER_PATH . 'daily_sentences_ar.php?mid=' . $miqat_id . '&sid=' . $cnt;
          ?>
            <a href="<?php echo $link; ?>">
              <?php echo $cnt; ?>
            </a>
          <?php
          $cnt++;
        }
        ?>
          </div>
        <div class="clearfix"></div>
      </div>
    </div>
    <div class="clearfix"></div>
  </form>
</div>

<div style='width:100%;text-align:center'>
  <audio id='sound1' preload='auto' src='' controls style='width:100%'>
</div>

<script src="<?php echo SERVER_PATH; ?>templates/js/jquery.prettyPhoto.js" type="text/javascript" charset="utf-8"></script>
<script src="<?php echo SERVER_PATH; ?>bootstrap/js/bootstrap.min.js"></script>
<script src="<?php echo SERVER_PATH; ?>templates/js/jquery.slicknav.js"></script>
<script src="<?php echo SERVER_PATH; ?>templates/js/jquery.confirm.min.js"></script>
<script src="<?php echo SERVER_PATH; ?>templates/js/jquery.nivo.slider.pack.js"></script>
<script src="<?php echo SERVER_PATH; ?>templates/js/select2/select2.full.min.js"></script>
<script src="<?php echo SERVER_PATH; ?>templates/js/scripts.js"></script>

<script>
  $(document).ready(function () {
    $('[data-toggle="popover"]').popover({
      placement: 'bottom'
    });
    $('body').on('click', function (e) {
      $('[data-toggle="popover"]').each(function () {
        //the 'is' for buttons that trigger popups
        //the 'has' for icons within a button that triggers a popup
        if (!$(this).is(e.target) && $(this).has(e.target).length === 0 && $('.popover').has(e.target).length === 0) {
          $(this).popover('hide');
        }
      });
    });
  });
</script>
<script src="js/jquery.sparkline.min.js" type="text/javascript"></script>

<style>
  .multitextbuttton a{
    color: #FFF;
    text-decoration: none;
  }

  .circle
  {
    background-color: #549BC7;
    color: #FFF;
    padding: 12px;
    font-size: 35px;
    border: 1px solid #437C9F;
    border-radius: 3px;
    -moz-border-radius: 3px;
    -webkit-border-radius: 3px;
    margin: 0 auto;
  }

  .forms1 a{
    color: #337ab7;
    padding: 3px;
  }
  .blue-box1 .title{
    text-align: center !important;
    font-size: 24px;
    width: 100%;
    line-height: 20px;
  }
  .play{
    color: #155485;
    cursor: pointer;
    margin-top: 20px;
  }

  .black_font, .black_font p{
    color:#000 !important;
  }

  .lsd p {
    font-family: lsarabic;
    font-size: 20px;
  }

  .lables label {
    color:#000 !important;
    padding: 5px;
  }
</style>

<script type="text/javascript">
  var values = [<?php echo "$days_left , $sr" ?>];

  $('.sparkline').sparkline(values, {
    type: "pie",
    width: "70px",
    height: "70px",
    sliceColors: ['#549BC7', '#09386c'],
    tooltipFormat: '',
    tooltipValueLookups: {
      'offset': {
<?php echo $days_left; ?>: 'Days Left: ',
<?php echo $sr; ?>: 'Days Past: '
      }
    },
  });
  function pa(file) {
    audio = document.getElementById('sound1');
    audio.src = file;
    audio.load();
    audio.play();
  }
</script>

<?php
require_once 'inc/inc.footer.php';
?>