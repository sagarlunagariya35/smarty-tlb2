<?php
require_once 'session.php';
require_once 'requires_login.php';
require_once 'admin/classes/class.ohbat_sentences.php';

$miqat_id = $_GET['mid'];

$cls_os = new Mtx_Ohbat_Sentences();
$sentences = array_reverse($cls_os->get_all_articles_by_miqat_id($miqat_id));

// calculate the current day
$today = strtotime(date('Y-m-d'));
$start_date = strtotime("2015-09-24");
$datediff = $today - $start_date;
$sr = floor($datediff / (86400));
$days_left = 30 - $sr;

$day_num = $sentences[$sr]['sr'];
$title = $sentences[$sr]['title_eng'];
$bayan = $sentences[$sr]['bayan_eng'];
$bayan_image = $sentences[$sr]['en_bayan_image'];
$question = $sentences[$sr]['question_eng'];
$words = $sentences[$sr]['word_meaning_eng'];

$miqaat_istibsaar = get_single_miqaat_istibsaar($miqat_id);
$miqaat_istibsaars = get_all_miqaat_istibsaar($miqaat_istibsaar['year']);
$mawaqeet = get_all_mawaqeet_by_year($miqaat_istibsaar['year']);
$tabs = array();

($mawaqeet['show_pre_event']) ? $tabs[] = 'pre' : '';
($mawaqeet['show_in_event']) ? $tabs[] = 'in' : '';
($mawaqeet['show_post_event']) ? $tabs[] = 'post' : '';

require_once 'inc/inc.header2.php';
?>

<!-- Contents -->
<!-- ====================================================================================================== -->
<div class="container white-bg">
  <div class="col-md-12 col-sm-12">

    <div class="page-title">
      <p style="margin-top:5px;">
        <a href="#" class="active"><?php echo $miqaat_istibsaar['miqaat_title'] . ' ' . $miqaat_istibsaar['year']; ?> H</a></p>
      <h1><?php echo $miqaat_istibsaar['miqaat_title'] . ' ' . $miqaat_istibsaar['year']; ?> H<span class="alfatemi-text"></span></h1>
    </div>
  </div>
  <div class="clearfix"></div> <!-- do not delete -->
  <form class="forms1 white" name="raza_form" method="post">
    <div class="col-md-3 col-sm-12 hidden-xs">
      <div class="profile-box-static">
        <h3 class="uppercase text-center">Mawaqeet</h3>
      </div>
      <div class="profile-box-static-bottom">
        <?php
        foreach ($mawaqeet as $mqt) {
          echo "<a href=\"" . SERVER_PATH . "istibsaar/$mqt[id]/$mqt[slug]/$mqt[year]/" . "\">» $mqt[miqaat_title]<hr></a>";
        }
        foreach ($tabs as $tb) {
          echo "<a href=\"" . SERVER_PATH . "istibsaar/$mqt[id]/$mqt[slug]/$mqt[year]/" . "\">» $mqt[miqaat_title]<hr></a>";
        }

        $make_pre_active = '';
        $make_in_active = '';
        $make_post_active = '';

        if ($miqaat_istibsaar['show_pre_event'] == '1') {
          $make_pre_active = ' active';
        } elseif ($miqaat_istibsaar['show_in_event'] == '1') {
          $make_in_active = ' active';
        } elseif ($miqaat_istibsaar['show_post_event'] == '1') {
          $make_post_active = ' active';
        }
        ?>
      </div>
    </div>

    <div class="col-md-8 col-sm-12">
      <div class="row" style="background-color: #cf4914; border-radius: 5px;">
        <div class="col-xs-4 col-sm-4" style="cursor: pointer;">
          <a href="daily_sentences_ar.php?mid=<?php echo $miqat_id; ?>">
            <img src="images/lang_change.png" height="70px" width="70px" />
          </a>
        </div>
        <div class="col-xs-4 col-sm-4 text-center">
          <span class="circle"><?php echo $day_num; ?></span>
        </div>
        <div class="col-xs-4 col-sm-4 text-right"><label style="border:1px solid #07294E;padding: 1px;" class="sparkline">&nbsp;</label></div>
      </div>
      <div class="clearfix"><br></div>
      <div class="pull-right lables">
        <label>Days Left : <?php echo $days_left; ?></label><br>
        <label>Days Past : <?php echo $sr; ?></label>
      </div>
      <div class="clearfix"></div>
      <div class="row">
        <div style="background-color: #549BC7;" class="blue-box1 blue-box1-level col-md-10 col-sm-10 col-xs-11">
          <?php if($bayan_image){ ?>
          <img src="<?php echo 'upload/ohbat_upload/'.$bayan_image; ?>" width="610" height="150">
          <?php }else { ?>
          <p class="title"><?php echo $title; ?></p>
          <?php } ?>
        </div>
        <div class="col-xs-1 col-md-1 text-center">
          <i class="fa fa-play-circle-o fa-3x play"></i>
        </div>
      </div>
      <div class="row">
        <div class="blue-box1 blue-box1-level col-md-10 col-sm-10 col-xs-11">
          <p class="title" style="line-height: 30px;"><?php echo $bayan; ?></p>
        </div>
        <div class="col-xs-1 col-md-1">
          <i class="fa fa-play-circle-o fa-3x play"></i>
        </div>
      </div>
      <div class="clearfix"></div>
      <div class="row">
        <div class="col-xs-1 col-md-1">
        </div>
        <div class="col-md-10 col-sm-10 col-xs-11" style="margin-top: 20px;">
          <span class="title black_font"><?php echo $words; ?></span>
        </div>
        <div class="clearfix">&nbsp;</div>
      </div>
      <div class="row">
        <div style="margin:13px !important;" class="multitextbuttton blue-box1-button col-md-10 col-sm-10 col-xs-11">
          <p class="title text-center"><?php echo $question; ?></p>
        </div>
        <div class="col-xs-1 col-md-1">
          <i class="fa fa-play-circle-o fa-3x play"></i>
        </div>
      </div>
    </div>
    <div class="clearfix"></div>
  </form>
</div>

<script src="<?php echo SERVER_PATH; ?>templates/js/jquery.prettyPhoto.js" type="text/javascript" charset="utf-8"></script>
<script src="<?php echo SERVER_PATH; ?>bootstrap/js/bootstrap.min.js"></script>
<script src="<?php echo SERVER_PATH; ?>templates/js/jquery.slicknav.js"></script>
<script src="<?php echo SERVER_PATH; ?>templates/js/jquery.confirm.min.js"></script>
<script src="<?php echo SERVER_PATH; ?>templates/js/jquery.nivo.slider.pack.js"></script>
<script src="<?php echo SERVER_PATH; ?>templates/js/select2/select2.full.min.js"></script>
<script src="<?php echo SERVER_PATH; ?>templates/js/scripts.js"></script>

<script>
  $(document).ready(function () {
    $('[data-toggle="popover"]').popover({
      placement: 'bottom'
    });
    $('body').on('click', function (e) {
      $('[data-toggle="popover"]').each(function () {
        //the 'is' for buttons that trigger popups
        //the 'has' for icons within a button that triggers a popup
        if (!$(this).is(e.target) && $(this).has(e.target).length === 0 && $('.popover').has(e.target).length === 0) {
          $(this).popover('hide');
        }
      });
    });
  });
</script>
<script src="js/jquery.sparkline.min.js" type="text/javascript"></script>

<style>
  .multitextbuttton a{
    color: #FFF;
    text-decoration: none;
  }

  .circle
  {
    background-color: #549BC7;
    color: #FFF;
    line-height: 70px;
    padding: 12px;
    font-size: 35px;
    border: 1px solid #437C9F;
    border-radius: 3px;
    -moz-border-radius: 3px;
    -webkit-border-radius: 3px;
    border-top-right-radius: 20px;
    -moz-border-top-right-radius: 20px;
    -webkit-border-top-right-radius: 20px;
  }

  .blue-box1 .title{
    text-align: center !important;
    font-size: 18px;
    width: 100%;
    line-height: 20px;
  }

  .play{
    color: #155485;
    cursor: pointer;
    margin-top: 20px;
  }

  .black_font{
    color:#000 !important;
  }
  
  .lables label {
    color:#000 !important;
    padding: 5px;
  }
</style>

<script type="text/javascript">
  var values = [<?php echo "$days_left , $sr" ?>];

  $('.sparkline').sparkline(values, {
    type: "pie",
    width: "70px",
    height: "70px",
    tooltipFormat: '',
    tooltipValueLookups: {
      'offset': {
        0: 'Days Left: ',
        1: 'Days Past: '
      }
    },
  });
</script>

<?php
require_once 'inc/inc.footer.php';
?>
