<?php
ob_start();
require 'smarty/libs/Smarty.class.php';
require_once 'session.php';
require_once 'requires_login.php';
require_once 'classes/class.survey.php';

$srv_survey = new Mtx_Survey();

$user_id = $_SESSION[USER_ID];
$user_its = $_SESSION[USER_ITS];

require_once 'inc/inc.header2.php';

if (isset($_REQUEST['survey_id'])) {
  $survey_id = $_REQUEST['survey_id'];
}

if (isset($_POST['submit_srv_ques'])) {
  $answers = serialize($_POST);
  
  $insert = $srv_survey->add_survey_ques_answers($_SESSION[USER_ITS], $survey_id, $answers);
  if ($insert) {
    $_SESSION[SUCCESS_MESSAGE] = "Your Survey Answers Saved Successfully";
    header('location: srv_thankyou.php');
    exit();
  }else {
    $_SESSION[ERROR_MESSAGE] = "Error In Saving Survey Answers";
  }
}

// Survey List
$survey_list = $srv_survey->get_open_surveys($_SESSION[USER_ITS], $srv_jamaat, $srv_jamiat, $srv_marhala, $srv_school);

// Single Survey
$survey = $srv_survey->get_survey($survey_id);

// Survey Questions
$survey_questions = $srv_survey->get_survey_questions($survey_id);


$smarty = new Smarty;

// Header / Session variables
$smarty->assign("server_path", SERVER_PATH);
$smarty->assign("srv_survey", $srv_survey);
$smarty->assign("survey_id", $survey_id);
$smarty->assign("surveys", $survey_list);
$smarty->assign("survey", $survey);
$smarty->assign("survey_questions", $survey_questions);

$smarty->display('srv_take_survey.tpl');
