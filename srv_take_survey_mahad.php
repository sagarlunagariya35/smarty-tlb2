<?php
require_once 'session.php';
require_once 'requires_login.php';
require_once 'classes/class.survey.php';

$srv_survey = new Mtx_Survey();

$user_id = $_SESSION[USER_ID];
$user_its = $_SESSION[USER_ITS];

if (isset($_POST['srvsubmit'])) {
  $survey_id = 1;
  $answers = serialize($_POST);

  $insert = $srv_survey->add_survey_ques_answers($_SESSION[USER_ITS], $survey_id, $answers);
  //$insert = TRUE;
  if ($insert) {
    $_SESSION[SUCCESS_MESSAGE] = "Your Survey Answers Saved Successfully";
    header('location: srv_thankyou.php');
    exit();
  } else {
    $_SESSION[ERROR_MESSAGE] = "Error In Saving Survey Answers";
  }
}

require_once 'inc/inc.header2.php';

?>
<style>
  .forms1 h3{
    font-size: 14px;
    color: black;
    padding: 0;
  }
  .blue-box1{
    background-color: #e5e5e5;
    padding: 8px;
    border-radius: 8px;
    margin-bottom: 8px;
  }
  .quiz-point-ques{
    border: 0px;
    padding-bottom: 10px;
    border-bottom: 1px solid #eee;
    margin-bottom: 50px;
    border-radius: 0px 0px 0px 10px;
    -webkit-border-radius: 0px 0px 0px 10px;
    -moz-border-radius: 0px 0px 0px 10px;
  }
  .forms1 select{
    border: 1px solid #d0d0d0;
  }
</style>
<div class="container white-bg">
  <div class="col-md-12 col-sm-12">
    <div class="page-title">
      <p style="margin-top:5px;">
        <a href="{SERVER_PATH}">Home</a> / 
        <a href="srv_take_survey_demo.php">Qism al-Tahfeez Survey</a>
      <h1>QISM AL-TAHFEEZ SURVEY<!--span class="alfatemi-text">معلومات ذاتية</span--></h1>
    </div>
  </div>
  <div class="clearfix"></div> <!-- do not delete -->
  <form class="forms1 white" id="ques_form" name="raza_form" method="post">

    <div class="col-md-12 col-sm-12 col-md-8 col-md-offset-2">
      <div class="row">
        <div class="col-xs-12 col-sm-12">
          <div class="static-show-button large-fonts col-xs-12 col-sm-12 text-center">Qism al-Tahfeez Survey</div>
        </div>
        <div class="clearfix"></div>
        <div class="clearfix"><br></div>

        <div class="col-xs-12 col-sm-12">
          <div class="quiz-point-ques">
            <div id="question_panel">
              <div class="blue-box1 border">
                <h3><span class="inline-block">1. ENTER THE ITS NO. OF YOUR CHILD WHO IS PARTICIPATING IN THE MAHAD AL-ZAHRA CAMP/PROGRAM:</span></h3>
              </div>
              <div class='clearfix'></div>
              <div class="col-md-6 col-sm-12">
                <input type="text" name="your_answer1" class="form-control" maxlength="8"  />
              </div>
            </div>
            <div class='clearfix'></div>
          </div>
        </div>
        
        <div class="col-xs-12 col-sm-12">
          <div class="quiz-point-ques">
            <div id="question_panel">
              <div class="blue-box1 border">
                <h3><span class="inline-block">2. SELECT THE MAHAD AL-ZAHRA BRANCH CITY WHERE YOUR CHILD IS ATTENDING:</span></h3>
              </div>
              <div class='clearfix'></div>
              <div class="col-md-6 col-sm-12">
                <select name="your_answer42" class="form-control select-input">
                  <option value="Houston">Houston</option>
                  <option value="Sharjah">Sharjah</option>
                  <option value="Kuwait">Kuwait</option>
                  <option value="Mumbai">Mumbai</option>
                  <option value="Colombo">Colombo</option>
                  <option value="Burhanpur">Burhanpur</option>
                  <option value="Galiakot">Galiakot</option>
                  <option value="Denmal">Denmal</option>
                  <option value="Yemen">Yemen</option>
                  <option value="Dubai">Dubai</option>
                  <option value="Nairobi">Nairobi</option>
                  <option value="Karachi">Karachi</option>
                  <option value="Surat">Surat</option>
                  <option value="Dongam">Dongam</option>
                  <option value="Banswara">Banswara</option>
                </select>
              </div>
              <div class='clearfix'></div>
            </div>
          </div>
        </div>
        
        <div class="col-xs-12 col-sm-12">
          <div class="quiz-point-ques">
            <div id="question_panel">
              <div class="blue-box1 border">
                <h3><span class="inline-block">3. HOW IS THEIR OVERALL EXPERIENCE IN MAHAD AL-ZAHRA?</span></h3>
              </div>
              <div class='clearfix'></div>
              <div class="col-md-12 col-sm-12">
                <label for="input-5" class="control-label">Rate This</label>
                <input type="number" name="your_answer2" value="0" id="input-5" class="rating rating-loading" data-min="0" data-max="10" data-step="1" data-show-clear="false" data-show-caption="true" data-size="xs" />
              </div>
              <div class="col-md-12 col-sm-12">
                <input type="text" name="your_answer2-2" class="form-control" placeholder="Let us know your experience" />
              </div>
            </div>
            <div class='clearfix'></div>
          </div>
        </div>
        
        <div class="col-xs-12 col-sm-12">
          <div class="static-show-button col-xs-12 col-sm-12 text-center">HIFZ RELATED</div>
        </div>

        <div class="col-xs-12 col-sm-12">
          <div class="quiz-point-ques">
            <div id="question_panel">
              <div class="blue-box1 border">
                <h3><span class="inline-block">1. HOW MANY HOURS A DAY DO THEY SPEND IN HIFZ AL-QURAN?</span></h3>
              </div>
              <div class='clearfix'></div>
              <div class="col-md-6 col-sm-6">
                <select name="your_answer15" class="form-control select-input">
                  <option value="1">1</option>
                  <option value="2">2</option>
                  <option value="3">3</option>
                  <option value="4">4</option>
                  <option value="5">5</option>
                  <option value="6">6</option>
                  <option value="7">7</option>
                  <option value="8">8</option>
                  <option value="9">9</option>
                  <option value="10">10</option>
                  <option value="11">11</option>
                  <option value="12">12</option>
                  <option value="13">13</option>
                  <option value="14">14</option>
                  <option value="15">15</option>
                  <option value="Not Applicable">Not Applicable</option>
                </select>
              </div>
            </div>
            <div class='clearfix'></div>
          </div>
        </div>

        <div class="col-xs-12 col-sm-12">
          <div class="quiz-point-ques">
            <div id="question_panel">
              <div class="blue-box1 border">
                <h3><span class="inline-block">2. WHEN DO THEY MEMORISE JADEED HIFZ?</span></h3>
              </div>
              <div class='clearfix'></div>
              <div class="col-md-6 col-sm-6">
                <select name="your_answer16" class="form-control select-input">
                  <option value="5 am">5 am</option>
                  <option value="6 am">6 am</option>
                  <option value="7 am">7 am</option>
                  <option value="8 am">8 am</option>
                  <option value="9 am">9 am</option>
                  <option value="10 am">10 am</option>
                  <option value="11 am">11 am</option>
                  <option value="12 am">12 am</option>
                  <option value="1 pm">1 pm</option>
                  <option value="2 pm">2 pm</option>
                  <option value="3 pm">3 pm</option>
                  <option value="4 pm">4 pm</option>
                  <option value="5 pm">5 pm</option>
                  <option value="6 pm">6 pm</option>
                  <option value="7 pm">7 pm</option>
                  <option value="8 pm">8 pm</option>
                  <option value="9 pm">9 pm</option>
                  <option value="10 pm">10 pm</option>
                  <option value="11 pm">11 pm</option>
                  <option value="Not Applicable">Not Applicable</option>
                </select>
              </div>
            </div>
            <div class='clearfix'></div>
          </div>
        </div>
        
        <div class="col-xs-12 col-sm-12">
          <div class="quiz-point-ques">
            <div id="question_panel">
              <div class="blue-box1 border">
                <h3><span class="inline-block">3. ARE THEY PROGRESSING IN DAILY HIFZ, AS PER THE TAKHTEET GIVEN TO THEM?</span></h3>
              </div>
              <div class="col-md-12 col-sm-12">
                <div class="col-md-3 col-sm-6">
                  <div class="quiz-point-radio">
                    <input type="radio" name="your_answer43" value="yes" id="radio50"/>
                    <label for="radio50" style="color: #000;" class="radGroup3">Yes</label>
                  </div>
                </div>
                <div class="col-md-3 col-sm-6">
                  <div class="quiz-point-radio">
                    <input type="radio" name="your_answer43" value="no" id="radio51"/>
                    <label for="radio51" style="color: #000;" class="radGroup3">No</label>
                  </div>
                </div>
                <div class="col-md-6 col-sm-12 h3no" style="display:none;">
                  <input type="text" name="your_answer43-2" class="form-control" placeholder="Please Explain">
                </div>
              </div>
            </div>
            <div class="clearfix"></div>
          </div>
        </div>
        
        <div class="col-xs-12 col-sm-12">
          <div class="quiz-point-ques">
            <div id="question_panel">
              <div class="blue-box1 border">
                <h3><span class="inline-block">4. ARE THERE ANY DIFFICULTIES YOUR CHILD FACES IN HIFZ AL-QURAN?</span></h3>
              </div>
              <div class="col-md-12 col-sm-12">
                <div class="col-md-3 col-sm-6">
                  <div class="quiz-point-radio">
                    <input type="radio" name="your_answer44" value="yes" id="radio52" />
                    <label for="radio52" style="color: #000;" class="radGroup3">Yes</label>
                  </div>
                </div>
                <div class="col-md-3 col-sm-6">
                  <div class="quiz-point-radio">
                    <input type="radio" name="your_answer44" value="no" id="radio53" />
                    <label for="radio53" style="color: #000;" class="radGroup3">No</label>
                  </div>
                </div>
                <div class="col-md-6 col-sm-12 h4no" style="display:none;">
                  <input type="text" name="your_answer44-2" class="form-control" placeholder="Please Explain">
                </div>
              </div>
            </div>
            <div class="clearfix"></div>
          </div>
        </div>
        
        <div class="col-xs-12 col-sm-12">
          <div class="static-show-button col-xs-12 col-sm-12 text-center">EDUCATION RELATED</div>
        </div>

        <div class="col-xs-12 col-sm-12">
          <div class="quiz-point-ques">
            <div id="question_panel">
              <div class="blue-box1 border">
                <h3><span class="inline-block">1. ALONG WITH HIFZ AL-QURAN, ARE THEY CONTINUING SCHOOLING OR PURSUING ANY COURSE?</span></h3>
              </div>
              <div class="col-md-12 col-sm-12">
                <div class="col-md-3 col-sm-6">
                  <div class="quiz-point-radio">
                    <input type="radio" name="your_answer33" value="yes" id="radio38" class="" />
                    <label for="radio38" style="color: #000;" class="radGroup3">Yes</label>
                  </div>
                </div>
                <div class="col-md-3 col-sm-6">
                  <div class="quiz-point-radio">
                    <input type="radio" name="your_answer33" value="no" id="radio39" class="" />
                    <label for="radio39" style="color: #000;" class="radGroup3">No</label>
                  </div>
                </div>

                <div class="col-md-6 col-sm-12 e1yes" style="display:none;">
                  <input type="text" name="your_answer33-2" class="form-control" placeholder="Which Course?">
                </div>
              </div>
            </div>
            <div class='clearfix'></div>
          </div>
        </div>

        <div class="col-xs-12 col-sm-12">
          <div class="quiz-point-ques">
            <div id="question_panel">
              <div class="blue-box1 border">
                <h3><span class="inline-block">2. TILL WHICH STANDARD (GRADE) HAVE THEY STUDIED TO, BEFORE JOINING MAHAD AL-ZAHRA?</span></h3>
              </div>
              <div class="col-md-6 col-sm-6">
                <select name="your_answer34"  class="form-control select-input e2">
                  <option value=""> Select One </option>
                  <option value="4">4</option>
                  <option value="5">5</option>
                  <option value="6">6</option>
                  <option value="7">7</option>
                  <option value="8">8</option>
                  <option value="3">9</option>
                  <option value="10">10</option>
                  <option value="11">11</option>
                  <option value="12">12</option>
                  <option value="Other">Other</option>
                  <option value="Not Applicable">Not Applicable</option>
                </select>
              </div>

              <div class="col-md-6 col-sm-12 e2o" style="display:none;">
                <input type="text" name="your_answer34-2" class="form-control" placeholder="Which?">
              </div>
            </div>
            <div class='clearfix'></div>
          </div>
        </div>

        <div class="col-xs-12 col-sm-12">
          <div class="quiz-point-ques">
            <div id="question_panel">
              <div class="blue-box1 border">
                <h3><span class="inline-block">3. DO THEY HAVE AN APPROPRIATE AREA TO STUDY AND ACCESS TO ADDITIONAL LEARNING RESOURCES? (MAGAZINES, INTERNET, BOOKS)</span></h3>
              </div>
              <div class="col-md-12 col-sm-12">
                <div class="col-md-3 col-sm-6">
                  <div class="quiz-point-radio">
                    <input type="radio" name="your_answer35" value="yes" id="radio40" class="" />
                    <label for="radio40" style="color: #000;" class="radGroup3">Yes</label>
                  </div>
                </div>
                <div class="col-md-3 col-sm-6">
                  <div class="quiz-point-radio">
                    <input type="radio" name="your_answer35" value="no" id="radio41" class="" />
                    <label for="radio41" style="color: #000;" class="radGroup3">No</label>
                  </div>
                </div>

                <div class="col-md-6 col-sm-12 e3yes" style="display:none;">
                  <input type="text" name="your_answer35-2" class="form-control"placeholder="Where?">
                </div>
              </div>
            </div>
            <div class='clearfix'></div>
          </div>
        </div>

        <div class="col-xs-12 col-sm-12">
          <div class="quiz-point-ques">
            <div id="question_panel">
              <div class="blue-box1 border">
                <h3><span class="inline-block">4. DO THEY EVER FEEL THAT A PARTICULAR USTAAD/TEACHER IS PICKING ON THEM OR DISCRIMINATING AGAINST YOU?</span></h3>
              </div>
              <div class="col-md-12 col-sm-12">
                <div class="col-md-3 col-sm-6">
                  <div class="quiz-point-radio">
                    <input type="radio" name="your_answer36" value="yes" id="radio42" class="" />
                    <label for="radio42" style="color: #000;" class="radGroup3">Yes</label>
                  </div>
                </div>
                <div class="col-md-3 col-sm-6">
                  <div class="quiz-point-radio">
                    <input type="radio" name="your_answer36" value="no" id="radio43" class="" />
                    <label for="radio43" style="color: #000;" class="radGroup3">No</label>
                  </div>
                </div>

                <div class="col-md-6 col-sm-12 e4yes" style="display:none;">
                  <input type="text" name="your_answer36-2" class="form-control" placeholder="Who?">
                </div>
              </div>
            </div>
            <div class='clearfix'></div>
          </div>
        </div>

        <div class="col-xs-12 col-sm-12">
          <div class="quiz-point-ques">
            <div id="question_panel">
              <div class="blue-box1 border">
                <h3><span class="inline-block">5. IS THEIR MOHAFFIZ/MUSAAID FRIENDLY/HELPFUL/COMMITTED?</span></h3>
              </div>
              <div class="col-md-12 col-sm-12">
                <div class="col-md-3 col-sm-6">
                  <div class="quiz-point-radio">
                    <input type="radio" name="your_answer37" value="yes" id="radio44" class="" />
                    <label for="radio44" style="color: #000;" class="radGroup3">Yes</label>
                  </div>
                </div>
                <div class="col-md-3 col-sm-6">
                  <div class="quiz-point-radio">
                    <input type="radio" name="your_answer37" value="no" id="radio45" class="" />
                    <label for="radio45" style="color: #000;" class="radGroup3">No</label>
                  </div>
                </div>

                <div class="col-md-6 col-sm-12 e5no" style="display:none;">
                  <input type="text" name="your_answer37-2" class="form-control" placeholder="Explain">
                </div>
              </div>
            </div>
            <div class='clearfix'></div>
          </div>
        </div>

        <div class="col-xs-12 col-sm-12">
          <div class="quiz-point-ques">
            <div id="question_panel">
              <div class="blue-box1 border">
                <h3><span class="inline-block">6. DO THEY CLEARLY UNDERSTAND THEIR INSTRUCTIONS?</span></h3>
              </div>
              <div class="col-md-12 col-sm-12">
                <div class="col-md-4 col-sm-6">
                  <div class="quiz-point-radio">
                    <input type="radio" name="your_answer38" value="yes" id="radio46" class="" />
                    <label for="radio46" style="color: #000;" class="radGroup3">Yes</label>
                  </div>
                </div>
                <div class="col-md-4 col-sm-6">
                  <div class="quiz-point-radio">
                    <input type="radio" name="your_answer38" value="no" id="radio47" class="" />
                    <label for="radio47" style="color: #000;" class="radGroup3">No</label>
                  </div>
                </div>
              </div>
            </div>
            <div class='clearfix'></div>
          </div>
        </div>

        <div class="col-xs-12 col-sm-12">
          <div class="quiz-point-ques">
            <div id="question_panel">
              <div class="blue-box1 border"> 
                <h3><span class="inline-block">7. AFTER COMPLETING HIFZ, DO THEY PLAN TO STUDY FURTHER, IF SO, WHICH SUBJECT/FIELD?</span></h3>
              </div>
              <div class="col-md-12 col-sm-12">
                <div class="col-md-3 col-sm-6">
                  <div class="quiz-point-radio">
                    <input type="radio" name="your_answer39" value="yes" id="radio48" class="" />
                    <label for="radio48" style="color: #000;" class="radGroup3">Yes</label>
                  </div>
                </div>
                <div class="col-md-3 col-sm-6">
                  <div class="quiz-point-radio">
                    <input type="radio" name="your_answer39" value="no" id="radio49" class="" />
                    <label for="radio49" style="color: #000;" class="radGroup3">No</label>
                  </div>
                </div>

                <div class="col-md-6 col-sm-12 e7yes" style="display:none;">
                  <input type="text" name="your_answer39-2" class="form-control" placeholder="Which Subject / field?">
                </div>
              </div>
            </div>
            <div class='clearfix'></div>
          </div>
        </div>

        <div class="col-xs-12 col-sm-12">
          <div class="quiz-point-ques">
            <div id="question_panel">
              <div class="blue-box1 border">
                <h3><span class="inline-block">8. HOW DO THEY PLAN TO PERFORM KHIDMAT ONCE THEY COMPLETE HIFZ?</span></h3>
              </div>
              <div class='clearfix'></div>
              <div class="col-md-12 col-sm-12">
                <input type="text" name="your_answer40" class="form-control">
              </div>
            </div>
            <div class='clearfix'></div>
          </div>
        </div>

        <div class="col-xs-12 col-sm-12">
          <div class="quiz-point-ques">
            <div id="question_panel">
              <div class="blue-box1 border">
                <h3><span class="inline-block">9. WHAT ELSE DO YOU FEEL THAT COULD BE DONE TO MAKE THEIR STAY MORE ENJOYABLE AND MEMORABLE?</span></h3>
              </div>
              <div class='clearfix'></div>
              <div class="col-md-12 col-sm-12">
                <input type="text" name="your_answer41" class="form-control">
              </div>
            </div>
            <div class='clearfix'></div>
          </div>
        </div>
        
        <div class="col-xs-12 col-sm-12">
          <div class="static-show-button col-xs-12 col-sm-12 text-center">General</div>
        </div>

        <div class="col-xs-12 col-sm-12">
          <div class="quiz-point-ques">
            <div id="question_panel">
              <div class="blue-box1 border">
                <h3><span class="inline-block">1. HOW OFTEN DO THEY COMMUNICATE WITH YOU?</span></h3>
              </div>
              <div class='clearfix'></div>
              <div class="col-md-6 col-sm-12">
                <select name="your_answer17"  class="form-control select-input">
                  <option value="Daily">Daily</option>
                  <option value="Every 2 days">Every 2 days</option>
                  <option value="Every 3 days">Every 3 days</option>
                  <option value="Every 4 days">Every 4 days</option>
                  <option value="Every 5 days">Every 5 days</option>
                  <option value="Every 6 days">Every 6 days</option>
                  <option value="Once a week">Once a week</option>
                  <option value="Once every 2 weeks">Once every 2 weeks</option>
                  <option value="Once a month">Once a month</option>
                  <option value="Not Applicable">Not Applicable</option>
                </select>
              </div>
            </div>
            <div class='clearfix'></div>
          </div>
        </div>

        <div class="col-xs-12 col-sm-12">
          <div class="quiz-point-ques">
            <div id="question_panel">
              <div class="blue-box1 border">
                <h3><span class="inline-block">2. HOW DO THEY COMMUNICATE WITH YOU?</span></h3>
              </div>
              <div class='clearfix'></div>
              <div class="col-md-6 col-sm-12">
                <select name="your_answer18" class="form-control select-input g2">
                  <option value=""> Select One </option>
                  <option value="Personal Mobile">Personal Mobile</option>
                  <option value="Mahad al-Zahra provided phone">Mahad al-Zahra provided phone</option>
                  <option value="Email">Email</option>
                  <option value="Phone of Khidmat guzaar">Phone of Khidmat guzaar</option>
                  <option value="Other">Other</option>
                  <option value="Not Applicable">Not Applicable</option>
                </select>
              </div>

              <div class="col-md-6 col-sm-12 g2o" style="display:none;">
                <input type="text" name="your_answer18-2" class="form-control" placeholder="Who?">
              </div>
            </div>
            <div class='clearfix'></div>
          </div>
        </div>

        <div class="col-xs-12 col-sm-12">
          <div class="quiz-point-ques">
            <div id="question_panel">
              <div class="blue-box1 border">
                <h3><span class="inline-block">3. AT WHAT TIME IN THE MORNING DO THEY WAKE UP?</span></h3>
              </div>
              <div class='clearfix'></div>
              <div class="col-md-6 col-sm-6">
                <select name="your_answer19" class="form-control select-input">
                  <option value="5 am">5 am</option>
                  <option value="6 am">6 am</option>
                  <option value="7 am">7 am</option>
                  <option value="8 am">8 am</option>
                  <option value="9 am">9 am</option>
                  <option value="10 am">10 am</option>
                  <option value="Not Applicable">Not Applicable</option>
                </select>
              </div>
            </div>
            <div class='clearfix'></div>
          </div>
        </div>

        <div class="col-xs-12 col-sm-12">
          <div class="quiz-point-ques">
            <div id="question_panel">
              <div class="blue-box1 border">
                <h3><span class="inline-block">4. AT WHAT TIME DO THEY GO TO SLEEP?</span></h3>
              </div>
              <div class='clearfix'></div>
              <div class="col-md-6 col-sm-6">
                <select name="your_answer20" class="form-control select-input">
                  <option value="9 pm">9 pm</option>
                  <option value="10 pm">10 pm</option>
                  <option value="11 pm">11 pm</option>
                  <option value="12 pm">12 am</option>
                  <option value="Not Applicable">Not Applicable</option>
                </select>
              </div>
            </div>
            <div class='clearfix'></div>
          </div>
        </div>

        <div class="col-xs-12 col-sm-12">
          <div class="quiz-point-ques">
            <div id="question_panel">
              <div class="blue-box1 border">
                <h3><span class="inline-block">5. WHERE DO THEY HAVE BREAKFAST, LUNCH AND DINNER?</span></h3>
              </div>
              <div class='clearfix'></div>
              <div class="col-md-6 col-sm-12">
                <select name="your_answer21" class="form-control select-input g5">
                  <option value=""> Select one </option>
                  <option value="Home">Home</option>
                  <option value="Mawaid">Mawaid</option>
                  <option value="Other">Other</option>
                  <option value="Not Applicable">Not Applicable</option>
                </select>
              </div>

              <div class="col-md-6 col-sm-12 g5o" style="display:none;">
                <input type="text" name="your_answer21-2" class="form-control" placeholder="Where?">
              </div>
            </div>
            <div class='clearfix'></div>
          </div>
        </div>

        <div class="col-xs-12 col-sm-12">
          <div class="quiz-point-ques">
            <div id="question_panel">
              <div class="blue-box1 border">
                <h3><span class="inline-block">6. DO THEY LIKE THE TASTE OF THE FOOD?</span></h3>
              </div>
              <div class="col-md-12 col-sm-12">
                <div class="col-md-3 col-sm-6">
                  <div class="quiz-point-radio">
                    <input type="radio" name="your_answer22" value="yes" id="radio22" class="" />
                    <label for="radio22" style="color: #000;" class="radGroup3">Yes</label>
                  </div>
                </div>
                <div class="col-md-3 col-sm-6">
                  <div class="quiz-point-radio">
                    <input type="radio" name="your_answer22" value="no" id="radio23" class="" />
                    <label for="radio23" style="color: #000;" class="radGroup3">No</label>
                  </div>
                </div>

                <div class="col-md-6 col-sm-12 g6no" style="display:none;">
                  <input type="text" name="your_answer22-2" class="form-control" placeholder="Why?">
                </div>
              </div>
            </div>
            <div class='clearfix'></div>
          </div>
        </div>

        <div class="col-xs-12 col-sm-12">
          <div class="quiz-point-ques">
            <div id="question_panel">
              <div class="blue-box1 border">
                <h3><span class="inline-block">7. IF THEY ARE HUNGRY BESIDES THOSE THREE MEAL TIMES, WHERE CAN THEY GO TO EAT?</span></h3>
              </div>
              <div class='clearfix'></div>
              <div class="col-md-6 col-sm-12">
                <select name="your_answer23" class="form-control select-input g7">
                  <option value=""> Select One </option>
                  <option value="Canteen">Canteen</option>
                  <option value="Other">Other</option>
                  <option value="Not Applicable">Not Applicable</option>
                </select>
              </div>

              <div class="col-md-6 col-sm-12 g7o" style="display:none;">
                <input type="text" name="your_answer23-2" class="form-control" placeholder="Where?">
              </div>
            </div>
            <div class='clearfix'></div>
          </div>
        </div>

        <div class="col-xs-12 col-sm-12">
          <div class="quiz-point-ques">
            <div id="question_panel">
              <div class="blue-box1 border">
                <h3><span class="inline-block">8. WHAT IS THEIR SCHEDULE ON SUNDAY?</span></h3>
              </div>
              <div class='clearfix'></div>
              <div class="col-md-12 col-sm-12">
                <input type="text" name="your_answer24" class="form-control">
              </div>
            </div>
            <div class='clearfix'></div>
          </div>
        </div>

        <div class="col-xs-12 col-sm-12">
          <div class="quiz-point-ques">
            <div id="question_panel">
              <div class="blue-box1 border">
                <h3><span class="inline-block">9. HAVE THEY ADJUSTED TO THE CLIMATE?</span></h3>
              </div>
              <div class="col-md-12 col-sm-12">
                <div class="col-md-3 col-sm-6">
                  <div class="quiz-point-radio">
                    <input type="radio" name="your_answer25" value="yes" id="radio24" class="" />
                    <label for="radio24" style="color: #000;" class="radGroup3">Yes</label>
                  </div>
                </div>
                <div class="col-md-3 col-sm-6">
                  <div class="quiz-point-radio">
                    <input type="radio" name="your_answer25" value="no" id="radio25" class="" />
                    <label for="radio25" style="color: #000;" class="radGroup3">No</label>
                  </div>
                </div>

                <div class="col-md-6 col-sm-12 g9no" style="display:none;">
                  <input type="text" name="your_answer25-2" class="form-control" placeholder="Describe">
                </div>
              </div>
            </div>
            <div class='clearfix'></div>
          </div>
        </div>

        <div class="col-xs-12 col-sm-12">
          <div class="quiz-point-ques">
            <div id="question_panel">
              <div class="blue-box1 border">
                <h3><span class="inline-block">10. IN THEIR ROOM, IS THERE A FAN OR AN AC?</span></h3>
              </div>
              <div class="col-md-12 col-sm-12">
                <div class="col-md-3 col-sm-6">
                  <div class="quiz-point-radio">
                    <input type="checkbox" name="your_answer26" value="AC" id="radio26" class=""/>
                    <label for="radio26" style="color: #000;" class="radGroup3">AC</label>
                  </div>
                </div>
                <div class="col-md-3 col-sm-6">
                  <div class="quiz-point-radio">
                    <input type="checkbox" name="your_answer26-2" value="Fan" id="radio27" class=""/>
                    <label for="radio27" style="color: #000;" class="radGroup3">Fan</label>
                  </div>
                </div>
              </div>
            </div>
            <div class='clearfix'></div>
          </div>
        </div>

        <div class="col-xs-12 col-sm-12">
          <div class="quiz-point-ques">
            <div id="question_panel">
              <div class="blue-box1 border">
                <h3><span class="inline-block">11. ARE THERE ANY WINDOWS IN THE ROOM?</span></h3>
              </div>
              <div class="col-md-12 col-sm-12">
                <div class="col-md-3 col-sm-6">
                  <div class="quiz-point-radio">
                    <input type="radio" name="your_answer27" value="yes" id="radio28" class="" />
                    <label for="radio28" style="color: #000;" class="radGroup3">Yes</label>
                  </div>
                </div>
                <div class="col-md-3 col-sm-6">
                  <div class="quiz-point-radio">
                    <input type="radio" name="your_answer27" value="no" id="radio29" class="" />
                    <label for="radio29" style="color: #000;" class="radGroup3">No</label>
                  </div>
                </div>
              </div>
            </div>
            <div class='clearfix'></div>
          </div>
        </div>

        <div class="col-xs-12 col-sm-12">
          <div class="quiz-point-ques">
            <div id="question_panel">
              <div class="blue-box1 border">
                <h3><span class="inline-block">12. DO THEY TAKE A SHOWER DAILY?</span></h3>
              </div>
              <div class="col-md-12 col-sm-12">
                <div class="col-md-3 col-sm-6">
                  <div class="quiz-point-radio">
                    <input type="radio" name="your_answer28" value="yes" id="radio30" class="" />
                    <label for="radio30" style="color: #000;" class="radGroup3">Yes</label>
                  </div>
                </div>
                <div class="col-md-3 col-sm-6">
                  <div class="quiz-point-radio">
                    <input type="radio" name="your_answer28" value="no" id="radio31" class="" />
                    <label for="radio31" style="color: #000;" class="radGroup3">No</label>
                  </div>
                </div>
              </div>
            </div>
            <div class='clearfix'></div>
          </div>
        </div>

        <div class="col-xs-12 col-sm-12">
          <div class="quiz-point-ques">
            <div id="question_panel">
              <div class="blue-box1 border">
                <h3><span class="inline-block">13. DO THEY PLAY SPORTS DAILY?</span></h3>
              </div>
              <div class='clearfix'></div>
              <div class="col-md-6 col-sm-12">
                <select name="your_answer29" class="form-control select-input">
                  <option value="Daily">Daily</option>
                  <option value="Every 2 days">Every 2 days</option>
                  <option value="Every 3 days">Every 3 days</option>
                  <option value="Every 4 days">Every 4 days</option>
                  <option value="Every 5 days">Every 5 days</option>
                  <option value="Every 6 days">Every 6 days</option>
                  <option value="Once a week">Once a week</option>
                  <option value="Not Applicable">Not Applicable</option>
                </select>
              </div>
            </div>
            <div class='clearfix'></div>
          </div>
        </div>

        <div class="col-xs-12 col-sm-12">
          <div class="quiz-point-ques">
            <div id="question_panel">
              <div class="blue-box1 border">
                <h3><span class="inline-block">14. ARE THERE ADDITIONAL ACTIVITIES SUCH AS TAFREEH PROGRAMS AND OR EDUCATIONAL FIELD TRIPS?</span></h3>
              </div>
              <div class="col-md-12 col-sm-12">
                <div class="col-md-3 col-sm-6">
                  <div class="quiz-point-radio">
                    <input type="radio" name="your_answer30" value="yes" id="radio32" class=""/>
                    <label for="radio32" style="color: #000;" class="radGroup3">Yes</label>
                  </div>
                </div>
                <div class="col-md-3 col-sm-6">
                  <div class="quiz-point-radio">
                    <input type="radio" name="your_answer30" value="no" id="radio33" class="" />
                    <label for="radio33" style="color: #000;" class="radGroup3">No</label>
                  </div>
                </div>

                <div class="col-md-6 col-sm-12 g14yes" style="display:none;">
                  <input type="text" name="your_answer30-2" class="form-control" placeholder="Explain">
                </div>
              </div>
            </div>
            <div class='clearfix'></div>
          </div>
        </div>

        <div class="col-xs-12 col-sm-12">
          <div class="quiz-point-ques">
            <div id="question_panel">
              <div class="blue-box1 border">
                <h3><span class="inline-block">15. IS THERE A COUNSELLOR OR SOMEONE THEY CAN CONFIDE IN REGARDING THEIR PERSONAL ISSUES?</span></h3>
              </div>
              <div class="col-md-12 col-sm-12">
                <div class="col-md-3 col-sm-6">
                  <div class="quiz-point-radio">
                    <input type="radio" name="your_answer31" value="yes" id="radio34" class="" />
                    <label for="radio34" style="color: #000;" class="radGroup3">Yes</label>
                  </div>
                </div>
                <div class="col-md-3 col-sm-6">
                  <div class="quiz-point-radio">
                    <input type="radio" name="your_answer31" value="no" id="radio35" class="" />
                    <label for="radio35" style="color: #000;" class="radGroup3">No</label>
                  </div>
                </div>

                <div class="col-md-6 col-sm-12 g15yes" style="display:none;">
                  <input type="text" name="your_answer31-2" class="form-control" placeholder="Counsellor Name?">
                </div>
              </div>
            </div>
            <div class='clearfix'></div>
          </div>
        </div>

        <div class="col-xs-12 col-sm-12">
          <div class="quiz-point-ques">
            <div id="question_panel">
              <div class="blue-box1 border">
                <h3><span class="inline-block">16. IS THERE SOMEONE THEY CAN SPEAK TO WHEN THEY FEEL SAD OR NEED ASSISTANCE AND GUIDANCE IN THEIR PERSONAL MATTERS?</span></h3>
              </div>
              <div class="col-md-12 col-sm-12">
                <div class="col-md-3 col-sm-6">
                  <div class="quiz-point-radio">
                    <input type="radio" name="your_answer32" value="yes" id="radio36" class="" />
                    <label for="radio36" style="color: #000;" class="radGroup3">Yes</label>
                  </div>
                </div>
                <div class="col-md-3 col-sm-6">
                  <div class="quiz-point-radio">
                    <input type="radio" name="your_answer32" value="no" id="radio37" class="" />
                    <label for="radio37" style="color: #000;" class="radGroup3">No</label>
                  </div>
                </div>

                <div class="col-md-6 col-sm-12 g16yes" style="display:none;">
                  <input type="text" name="your_answer32-2" class="form-control" placeholder="Who?">
                </div>
              </div>
            </div>
            <div class='clearfix'></div>
          </div>
        </div>

        <div class="col-xs-12 col-sm-12">
          <div class="static-show-button col-xs-12 col-sm-12 text-center">ACCOMMODATION RELATED</div>
        </div>
        <div class="clearfix"></div>

        <div class="col-xs-12 col-sm-12">
          <div class="quiz-point-ques">
            <div id="question_panel">
              <div class="blue-box1 border">
                <h3><span class="inline-block">1. INCLUDING YOU, HOW MANY OTHERS ARE IN YOUR ROOM?</span></h3>
              </div>
              <div class='clearfix'></div>
              <div class="col-md-6 col-sm-12">
                <select name="your_answer3" class="form-control select-input">
                  <option value="1">1</option>
                  <option value="2">2</option>
                  <option value="3">3</option>
                  <option value="4">4</option>
                  <option value="5">5</option>
                  <option value="6">6</option>
                  <option value="7">7</option>
                  <option value="8">8</option>
                  <option value="9">9</option>
                  <option value="10">10</option>
                  <option value="Not Applicable">Not Applicable</option>
                </select>
              </div>
              <div class='clearfix'></div>
            </div>
          </div>
        </div>

        <div class="col-xs-12 col-sm-12">
          <div class="quiz-point-ques">
            <div id="question_panel">
              <div class="blue-box1 border">
                <h3><span class="inline-block">2. ARE THEY COMFORTABLE WITH THEIR ROOMMATES?</span></h3>
              </div>
              <div class="col-md-12 col-sm-12">
                <div class="col-md-3 col-sm-6">
                  <div class="quiz-point-radio">
                    <input type="radio" name="your_answer4" value="yes" id="radio12" />
                    <label for="radio12" style="color: #000;" class="radGroup3">Yes</label>
                  </div>
                </div>
                <div class="col-md-3 col-sm-6">
                  <div class="quiz-point-radio">
                    <input type="radio" name="your_answer4" value="no" id="radio13" />
                    <label for="radio13" style="color: #000;" class="radGroup3">No</label>
                  </div>
                </div>
                <div class="col-md-6 col-sm-12 q2no" style="display:none;">
                  <input type="text" name="your_answer4-2" class="form-control" placeholder="Please Explain">
                </div>
              </div>
            </div>
            <div class="clearfix"></div>
          </div>
        </div>

        <div class="col-xs-12 col-sm-12">
          <div class="quiz-point-ques">
            <div id="question_panel">
              <div class="blue-box1 border">
                <h3><span class="inline-block">3. ON AVERAGE, HOW MANY HOURS OF SLEEP DO THEY GET EVERY NIGHT?</span></h3>
              </div>
              <div class='clearfix'></div>
              <div class="col-md-4 col-sm-6">
                <select name="your_answer5" class="form-control select-input">
                  <option value="3">3</option>
                  <option value="4">4</option>
                  <option value="5">5</option>
                  <option value="6">6</option>
                  <option value="7">7</option>
                  <option value="8">8</option>
                  <option value="9">9</option>
                  <option value="10">10</option>
                  <option value="Not Applicable">Not Applicable</option>
                </select>
              </div>
            </div>
            <div class="clearfix"></div>
          </div>
        </div>
        <div class='clearfix'></div>

        <div class="col-xs-12 col-sm-12">
          <div class="quiz-point-ques">
            <div id="question_panel">
              <div class="blue-box1 border">
                <h3><span class="inline-block">4. HOW OFTEN IS THEIR ROOM CLEANED?</span></h3>
              </div>
              <div class='clearfix'></div>
              <div class="col-md-4 col-sm-6">
                <select name="your_answer6" class="form-control select-input">
                  <option value="Daily">Daily</option>
                  <option value="Every 2 days">Every 2 days</option>
                  <option value="Every 3 days">Every 3 days</option>
                  <option value="Every 4 days">Every 4 days</option>
                  <option value="Every 5 days">Every 5 days</option>
                  <option value="Every 6 days">Every 6 days</option>
                  <option value="Once a week">Once a week</option>
                  <option value="Not Applicable">Not Applicable</option>
                </select>
              </div>
            </div>
            <div class="clearfix"></div>
          </div>
        </div>
        <div class='clearfix'></div>

        <div class="col-xs-12 col-sm-12">
          <div class="quiz-point-ques">
            <div id="question_panel">
              <div class="blue-box1 border">
                <h3><span class="inline-block">5. IS THE BED THEY SLEEP IN COMFORTABLE?</span></h3>
              </div>
              <div class="col-md-12 col-sm-12">
                <div class="col-md-3 col-sm-6">
                  <div class="quiz-point-radio">
                    <input type="radio" name="your_answer7" value="yes" id="radio14" class="" />
                    <label for="radio14" style="color: #000;" class="radGroup3">Yes</label>
                  </div>
                </div>
                <div class="col-md-3 col-sm-6">
                  <div class="quiz-point-radio">
                    <input type="radio" name="your_answer7" value="no" id="radio15" class="" />
                    <label for="radio15" style="color: #000;" class="radGroup3">No</label>
                  </div>
                </div>
                <div class="col-md-6 col-sm-12 q5no" style="display:none;">
                  <input type="text" name="your_answer7-2" class="form-control" placeholder="Please Describe">
                </div>
              </div>
            </div>
            <div class="clearfix"></div>
          </div>
        </div>

        <div class="col-xs-12 col-sm-12">
          <div class="quiz-point-ques">
            <div id="question_panel">
              <div class="blue-box1 border">
                <h3><span class="inline-block">6. WHO WASHES THEIR CLOTHES? (INCLUDING UNDERGARMENTS)</span></h3>
              </div>
              <div class='clearfix'></div>
              <div class="col-md-6 col-sm-12">
                <select name="your_answer8" class="form-control select-input q6">
                  <option value=""> Select One </option>
                  <option value="Self">Self</option>
                  <option value="Administration">Administration</option>
                  <option value="Other">Other</option>
                  <option value="Not Applicable">Not Applicable</option>
                </select>
              </div>
              <div class="col-md-6 col-sm-12 q6o" style="display:none;">
                <input type="text" name="your_answer8-2" class="form-control" placeholder="Who?">
              </div>
            </div>
            <div class="clearfix"></div>
          </div>
        </div>

        <div class="col-xs-12 col-sm-12">
          <div class="quiz-point-ques">
            <div id="question_panel">
              <div class="blue-box1 border">
                <h3><span class="inline-block">7. WHO IRONS THEIR CLOTHES?</span></h3>
              </div>
              <div class='clearfix'></div>
              <div class="col-md-6 col-sm-12">
                <select name="your_answer9" class="form-control select-input q7">
                  <option value=""> Select One </option>
                  <option value="Self">Self</option>
                  <option value="Administration">Administration</option>
                  <option value="Other">Other</option>
                  <option value="Not Applicable">Not Applicable</option>
                </select>
              </div>

              <div class="col-md-6 col-sm-12 q7o" style="display:none;">
                <input type="text" name="your_answer9-2" class="form-control" placeholder="Who?">
              </div>
            </div>
            <div class="clearfix"></div>
          </div>
        </div>

        <div class="col-xs-12 col-sm-12">
          <div class="quiz-point-ques">
            <div id="question_panel">
              <div class="blue-box1 border">
                <h3><span class="inline-block">8. WHERE DO THEY KEEP THEIR SPENDING MONEY?</span></h3>
              </div>
              <div class='clearfix'></div>
              <div class="col-md-4 col-sm-6">
                <select name="your_answer10" class="form-control select-input q8">
                  <option value=""> Select One </option>
                  <option value="In their cupboard">In their cupboard</option>
                  <option value="Bag">Bag</option>
                  <option value="Amanat">Amanat</option>
                  <option value="With Khidmat Guzaar">With Khidmat Guzaar</option>
                  <option value="Other">Other</option>
                  <option value="Not Applicable">Not Applicable</option>
                </select>
              </div>
              <div class="col-md-4 col-sm-6 q8o" style="display:none;">
                <input type="text" name="your_answer10-2" class="form-control" placeholder="Who?">
              </div>
            </div>
            <div class='clearfix'></div>
          </div>
        </div>

        <div class="col-xs-12 col-sm-12">
          <div class="quiz-point-ques">
            <div id="question_panel">
              <div class="blue-box1 border">
                <h3><span class="inline-block">9. ARE THEIR BELONGINGS SAFE IN THEIR CUPBOARD/BAGS?</span></h3>
              </div>
              <div class="col-md-12 col-sm-12">
                <div class="col-md-3 col-sm-6">
                  <div class="quiz-point-radio">
                    <input type="radio" name="your_answer11" value="yes" id="radio16" class="" />
                    <label for="radio16" style="color: #000;" class="radGroup3">Yes</label>
                  </div>
                </div>
                <div class="col-md-3 col-sm-6">
                  <div class="quiz-point-radio">
                    <input type="radio" name="your_answer11" value="no" id="radio17" class="" />
                    <label for="radio17" style="color: #000;" class="radGroup3">No</label>
                  </div>
                </div>
              </div>
            </div>
            <div class='clearfix'></div>
          </div>
        </div>

        <div class="col-xs-12 col-sm-12">
          <div class="quiz-point-ques">
            <div id="question_panel">
              <div class="blue-box1 border">
                <h3><span class="inline-block">10. IS THERE ANYONE WHO BULLIES OR DISTURBS THEM?</span></h3>
              </div>
              <div class="col-md-12 col-sm-12">
                <div class="col-md-3 col-sm-6">
                  <div class="quiz-point-radio">
                    <input type="radio" name="your_answer12" value="yes" id="radio18" class="" />
                    <label for="radio18" style="color: #000;" class="radGroup3">Yes</label>
                  </div>
                </div>
                <div class="col-md-3 col-sm-6">
                  <div class="quiz-point-radio">
                    <input type="radio" name="your_answer12" value="no" id="radio19" class="" />
                    <label for="radio19" style="color: #000;" class="radGroup3">No</label>
                  </div>
                </div>
                <div class="col-md-6 col-sm-12 q10yes" style="display:none;">
                  <input type="text" name="your_answer12-2" class="form-control" placeholder="Who?">
                </div>
              </div>
            </div>
            <div class='clearfix'></div>
          </div>
        </div>

        <div class="col-xs-12 col-sm-12">
          <div class="quiz-point-ques">
            <div id="question_panel">
              <div class="blue-box1 border">
                <h3><span class="inline-block">11. ARE THEIR HEALTH ISSUES TAKEN CARE OF IN A TIMELY MANNER?</span></h3>
              </div>
              <div class="col-md-12 col-sm-12">
                <div class="col-md-3 col-sm-6">
                  <div class="quiz-point-radio">
                    <input type="radio" name="your_answer13" value="yes" id="radio20" class="" />
                    <label for="radio20" style="color: #000;" class="radGroup3">Yes</label>
                  </div>
                </div>
                <div class="col-md-3 col-sm-6">
                  <div class="quiz-point-radio">
                    <input type="radio" name="your_answer13" value="no" id="radio21" class="" />
                    <label for="radio21" style="color: #000;" class="radGroup3">No</label>
                  </div>
                </div>
              </div>
            </div>
            <div class='clearfix'></div>
          </div>
        </div>

        <div class="col-xs-12 col-sm-12">
          <div class="quiz-point-ques">
            <div id="question_panel">
              <div class="blue-box1 border">
                <h3><span class="inline-block">12. HOW IS THE QUALITY OF THE MEDICAL TREATMENT?</span></h3>
              </div>
              <div class='clearfix'></div>
              <div class="col-md-6 col-sm-6">
                <div class="col-md-12 col-sm-12">
                <select name="your_answer14" class="form-control select-input">
                  <option value=""> Select One </option>
                  <option value="Excellent">Excellent</option>
                  <option value="Good">Good</option>
                  <option value="Very good">Very good</option>
                  <option value="Poor">Poor</option>
                  <option value="Very poor">Very poor</option>
                  <option value="Not Applicable">Not Applicable</option>
                </select>
                </div>
              </div>
            </div>
            <div class='clearfix'></div>
          </div>
        </div>  
        
        <div class="col-xs-12 col-sm-12">
          <div class="quiz-point-ques">
            <div id="question_panel">
              <div class="col-md-12 col-sm-12">
                <textarea name="your_answer45" class="form-control" placeholder="Enter Comments"></textarea>
              </div>
            </div>
            <div class='clearfix'></div>
          </div>
        </div>        

        

        <div class="col-xs-12 col-sm-12">
          <input type="submit" name="srvsubmit" class="btn btn-primary btn-block" value="Submit">
        </div>

      </div>
    </div>
    <div class="clearfix"></div>
  </form>
</div>

<script src="<?php echo SERVER_PATH ?>templates/js/jquery.prettyPhoto.js" type="text/javascript" charset="utf-8"></script>
<script src="<?php echo SERVER_PATH ?>bootstrap/js/bootstrap.min.js"></script>
<script src="<?php echo SERVER_PATH ?>templates/js/jquery.slicknav.js"></script>
<script src="<?php echo SERVER_PATH ?>templates/js/jquery.confirm.min.js"></script>
<script src="<?php echo SERVER_PATH ?>templates/js/jquery.nivo.slider.pack.js"></script>
<script src="<?php echo SERVER_PATH ?>templates/js/scripts.js"></script>

<link href="templates/css/star-rating.css" media="all" rel="stylesheet" type="text/css" />
<script src="templates/js/star-rating.js" type="text/javascript"></script>
<script src="templates/js/star-rating_locale_LANG.js"></script>

<script>
  $(document).ready(function () {
    $('[data-toggle="popover"]').popover({
      placement: 'bottom'
    });
    $('body').on('click', function (e) {
      $('[data-toggle="popover"]').each(function () {
        //the 'is' for buttons that trigger popups
        //the 'has' for icons within a button that triggers a popup
        if (!$(this).is(e.target) && $(this).has(e.target).length === 0 && $('.popover').has(e.target).length === 0) {
          $(this).popover('hide');
        }
      });
    });
  });

  $('#radio13').click(function () {
    $('.q2no').show();
    $('.q2no input').prop('required', false);
  });
  $('#radio12').click(function () {
    $('.q2no').hide();
    $('.q2no input').prop('required', false);
  });

  $('#radio15').click(function () {
    $('.q5no').show();
    $('.q5no input').prop('required',false);
  });
  $('#radio14').click(function () {
    $('.q5no').hide();
    $('.q5no input').prop('required', false);
  });

  $('#radio18').click(function () {
    $('.q10yes').show();
    $('.q10yes input').prop('required',false);
  });
  $('#radio19').click(function () {
    $('.q10yes').hide();
    $('.q10yes input').prop('required', false);
  });

  $('#radio22').click(function () {
    $('.g6no').hide();
    $('.g6no input').prop('required', false);
  });
  $('#radio23').click(function () {
    $('.g6no').show();
    $('.g6no input').prop('required',false);
  });

  $('#radio24').click(function () {
    $('.g9no').hide();
    $('.g9no input').prop('required', false);
  });
  $('#radio25').click(function () {
    $('.g9no').show();
    $('.g9no input').prop('required',false);
  });

  $('#radio32').click(function () {
    $('.g14yes').show();
    $('.g14yes input').prop('required',false);
  });
  $('#radio33').click(function () {
    $('.g14yes').hide();
    $('.g14yes input').prop('required', false);
  });

  $('#radio34').click(function () {
    $('.g15yes').show();
    $('.g15yes input').prop('required',false);
  });
  $('#radio35').click(function () {
    $('.g15yes').hide();
    $('.g15yes input').prop('required', false);
  });

  $('#radio36').click(function () {
    $('.g16yes').show();
    $('.g16yes input').prop('required',false);
  });
  $('#radio37').click(function () {
    $('.g16yes').hide();
    $('.g16yes input').prop('required', false);
  });

  $('#radio38').click(function () {
    $('.e1yes').show();
    $('.e1yes input').prop('required',false);
  });
  $('#radio39').click(function () {
    $('.e1yes').hide();
    $('.e1yes input').prop('required', false);
  });

  $('#radio40').click(function () {
    $('.e3yes').show();
    $('.e3yes input').prop('required',false);
  });
  $('#radio41').click(function () {
    $('.e3yes').hide();
    $('.e3yes input').prop('required', false);
  });

  $('#radio42').click(function () {
    $('.e4yes').show();
    $('.e4yes input').prop('required',false);
  });
  $('#radio43').click(function () {
    $('.e4yes').hide();
    $('.e4yes input').prop('required', false);
  });

  $('#radio44').click(function () {
    $('.e5no').hide();
    $('.e5no input').prop('required', false);
  });
  $('#radio45').click(function () {
    $('.e5no').show();
    $('.e5no input').prop('required',false);
  });

  $('#radio48').click(function () {
    $('.e7yes').show();
    $('.e7yes input').prop('required',false);
  });
  $('#radio49').click(function () {
    $('.e7yes').hide();
    $('.e7yes input').prop('required', false);
  });
  
  $('#radio50').click(function () {
    $('.h3no').show();
    $('.h3no input').prop('required', false);
  });
  $('#radio51').click(function () {
    $('.h3no').show();
    $('.h3no input').prop('required', false);
  });
 
  $('#radio52').click(function () {
    $('.h4no').show();
    $('.h4no input').prop('required', false);
  });
  $('#radio53').click(function () {
    $('.h4no').show();
    $('.h4no input').prop('required', false);
  });

  $(".q6").change(function () {
    if ($(this).val() === "Other") {
      $(".q6o").show();
      $('.q6o input').prop('required',false);
    } else {
      $(".q6o").hide();
      $('.q6o input').prop('required', false);
    }
  });

  $(".q7").change(function () {
    if ($(this).val() === "Other") {
      $(".q7o").show();
      $('.q7o input').prop('required',false);
    } else {
      $(".q7o").hide();
      $('.q7o input').prop('required', false);
    }
  });

  $(".q8").change(function () {
    if ($(this).val() === "Other") {
      $(".q8o").show();
      $('.q8o input').prop('required',false);
    } else {
      $(".q8o").hide();
      $('.q8o input').prop('required', false);
    }
  });

  $(".g2").change(function () {
    if ($(this).val() === "Other") {
      $(".g2o").show();
      $('.g2o input').prop('required',false);
    } else {
      $(".g2o").hide();
      $('.g2o input').prop('required', false);
    }
  });

  $(".g5").change(function () {
    if ($(this).val() === "Other") {
      $(".g5o").show();
      $('.g5o input').prop('required',false);
    } else {
      $(".g5o").hide();
      $('.g5o input').prop('required', false);
    }
  });

  $(".g7").change(function () {
    if ($(this).val() === "Other") {
      $(".g7o").show();
      $('.g7o input').prop('required',false);
    } else {
      $(".g7o").hide();
      $('.g7o input').prop('required', false);
    }
  });

  $(".e2").change(function () {
    if ($(this).val() === "Other") {
      $(".e2o").show();
      $('.e2o input').prop('required',false);
    } else {
      $(".e2o").hide();
      $('.e2o input').prop('required', false);
    }
  });

</script>

<?php include 'inc/inc.footer.php'; ?>