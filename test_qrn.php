<?php
require_once 'classes/class.qrn_tasmee.php';

$qrn_tasmee = new Qrn_Tasmee();

if (isset($_POST['submit'])) {
    $surah_no = $_POST['surah_no'];
    $total_ayat = $_POST['total_ayat'];
    $surah_name = $qrn_tasmee->get_surat_name($surah_no);

    $i = 1;
    while($i <= $total_ayat) {

      $query = "INSERT INTO `qrn_data`(`surah_no`, `aayat_no`, `surah_name`) VALUES ('$surah_no','$i','$surah_name')";

      $result = $db->query($query);
      $i++;
    }
}
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content=""><meta name="author" content="">
    <title>Talabul-ilm</title> 
    <link href="<?php echo SERVER_PATH; ?>css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo SERVER_PATH; ?>assets/css/login.css" rel="stylesheet">
    <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
  </head>
  <body>
    <div class="container">
      <form method="post">
        <div class="row">
          <div class="col-md-12">&nbsp;</div>
          <div class="col-md-3">&nbsp;</div>
          <div class="col-md-6">
            <div class="form-group">
              <label>Surah No: </label>
              <input class="form-control" type="text" name="surah_no" placeholder="Enter Surah No.">
            </div>
            <div class="form-group">
              <label>Total Ayat: </label>
              <input class="form-control" type="text" name="total_ayat" placeholder="Enter Total Ayat">
            </div>
            <div class="form-group">
              <button type="submit" class="btn btn-block btn-orange" name="submit">Submit</button>
            </div>
          </div>
          <div class="col-md-3">&nbsp;</div>
        </div>
      </form>
    </div>
    <script src="<?php echo SERVER_PATH; ?>assets/js/jquery-2.1.0.min.js"></script>
    <script src="<?php echo SERVER_PATH; ?>assets/bootstrap/js/bootstrap.min.js"></script>
  </body>
</html>