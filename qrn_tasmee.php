<?php
require 'smarty/libs/Smarty.class.php';
require_once 'session.php';
require_once 'requires_login.php';
require_once 'classes/class.qrn_mumin.php';
require_once 'classes/class.qrn_muhafiz.php';
require_once 'classes/class.user.php';

$qrn_mumin = new Qrn_Mumin();
$qrn_muhafiz = new Qrn_Muhafiz();
$user_logedin = new mtx_user();
$user_logedin->loaduser($_SESSION[USER_ITS]);
$user_full_name = $user_logedin->get_full_name();
$user_email = $user_logedin->get_email();
$user_contact = $user_logedin->get_mobile();

$count_pending_request = FALSE;
$last_tasmee_surat = $last_tasmee_ayat = $prefered_time = $required_sanad = FALSE;

if (isset($_POST['delete_id'])) {
  $delete_id = $_POST['delete_id'];
  $delete_result = $qrn_muhafiz->get_tasmee_request($delete_id);
  if ($delete_result) {
    $result = $qrn_muhafiz->delete_tasmee_request($delete_id);
    $mumin_its = $delete_result['mumin_its'];

    $mumin_data = $user_logedin->loaduser($mumin_its);
    $email = $mumin_data->get_email();
    $subject = 'Tasmee Request';
    $message = $user_full_name . ' has rejected your Tasmee request';

    $sent_mail = $qrn_muhafiz->insert_cron_emails('akhequrani@talabulilm.com', $email, $subject, $message);

    if ($result) {
      $_SESSION[SUCCESS_MESSAGE] = "Tasmee Request Successfully Deleted";
    } else {
      $_SESSION[ERROR_MESSAGE] = "Error while Delete";
    }
  }
}

if (isset($_POST['accept_id'])) {
  $accept_id = $_POST['accept_id'];
  
  //Get Tasmee Request
  $accept_result = $qrn_muhafiz->get_tasmee_request($accept_id);
  if ($accept_result) {
    
    //Accept Tasmee Request
    $result = $qrn_muhafiz->accept_tasmee_request($accept_id);
    $mumin_its = $accept_result['mumin_its'];

    //Delete Other Mumin Tasmee Request
    $get_mumin_other_req = $qrn_muhafiz->get_all_tasmee_request_of_mumin($mumin_its);
    if ($get_mumin_other_req) {
      $request_mumin_full_name = $qrn_mumin->get_mumin_name($mumin_its);
      
      foreach ($get_mumin_other_req as $other_data) {
        $other_muhaffiz_its = $other_data['muhafiz_its'];
        
        if ($other_muhaffiz_its != 0) {
          $other_muhafiz_user_data = $user_logedin->loaduser($other_muhaffiz_its);
          $other_muhaffiz_email = $other_muhafiz_user_data->get_email();

          if ($other_muhaffiz_email) {
            $other_muhaffiz_subject = 'Sadeeq Removed You Successfully';
            $other_muhaffiz_message = 'Tasmee request from '. $request_mumin_full_name .' has been deleted as an other muhaffiz has already accepted his/her request';

            $sent_other_muhaffiz_mail = $qrn_muhafiz->insert_cron_emails('akhequrani@talabulilm.com', $other_muhaffiz_email, $other_muhaffiz_subject, $other_muhaffiz_message);
          }
        }
      }
      
      $delete_other_rec = $qrn_muhafiz->delete_all_tasmee_request($mumin_its);
    }
    
    //Mumin ITS and Loggedin User are not same
    if ($mumin_its != $_SESSION[USER_ITS]) {
      $muhafiz_user_data = $user_logedin->loaduser($_SESSION[USER_ITS]);
      
      $exist_mumin_result = $qrn_mumin->get_all_mumin($mumin_its);
      if ($exist_mumin_result) {
        
        $exist_mumin_name = $exist_mumin_result[0]['mumin_full_name'];
        
        if ($exist_mumin_result[0]['muhafiz_id'] != 0) {
          $exist_muhafiz_data = $user_logedin->loaduser($exist_mumin_result[0]['muhafiz_id']);
          $exist_muhafiz_email = $exist_muhafiz_data->get_email();

          if ($exist_muhafiz_email) {
            $exist_muhafiz_subject = 'Sadeeq Removed You Successfully';
            $exist_muhafiz_message = 'Your sadeeq '. $exist_mumin_name .' is not linked with you anymore. And has chosen a new muhaffiz.';

            $sent_exist_muhafiz_mail = $qrn_muhafiz->insert_cron_emails('akhequrani@talabulilm.com', $exist_muhafiz_email, $exist_muhafiz_subject, $exist_muhafiz_message);
          }
        }
        
        $update_mumin = $qrn_mumin->update_muhafiz_data_of_mumin($mumin_its, $_SESSION[USER_ITS], $muhafiz_user_data->get_full_name(), $muhafiz_user_data->get_jamaat(), $muhafiz_user_data->get_jamiat(), $muhafiz_user_data->get_quran_sanad(), $muhafiz_user_data->get_gender());
      }
    }

    $sent_mail_mumin_data = $user_logedin->loaduser($mumin_its);
    $mumin_full_name = $sent_mail_mumin_data->get_full_name();
    $mumin_email = $sent_mail_mumin_data->get_email();
    $mumin_contact = $sent_mail_mumin_data->get_mobile();
    $mumin_whatsapp = $sent_mail_mumin_data->get_whatsapp();
    $mumin_skype = $sent_mail_mumin_data->get_viber();
    
    $sent_mail_muhaffiz_data = $user_logedin->loaduser($_SESSION[USER_ITS]);
    $muhaffiz_email = $sent_mail_muhaffiz_data->get_email();
    $muhaffiz_contact = $sent_mail_muhaffiz_data->get_mobile();
    $muhaffiz_whatsapp = $sent_mail_muhaffiz_data->get_whatsapp();
    $muhaffiz_skype = $sent_mail_muhaffiz_data->get_viber();
    
    $muhaffiz_subject = 'Tasmee Request';
    $muhaffiz_message = 'You have accepted Akh e-qurani request of '. $mumin_full_name .'.<br><br>Email: ' . $mumin_email . '<br>Contact: ' . $mumin_contact . '<br>Whatsapp: ' . $mumin_whatsapp . '<br>Skype: ' . $mumin_skype . '<p>These details will be available in your Ikhwaan report page. You will also be able to view each of your Ikhwaan(sadeeq) progress reports on this page.</p><p>Make sure you enter his/her tasmee details everytime.</p><p>We would like you to go to the <em>Info</em> page and understand the detialed hifz structure.</p><p>Mahad-al-Zahra has a Quran ikhtebaar structure as follows.</p><ol><li>Juzz Amma</li><li>Sanaa One</li><li>Sanaa Two</li><li>Sanaa Three</li><li>Sanaa Four</li></ol><p>Your goal should be to make your sadeeq ready for each of these ikhtebaar when they reach that level</p><p>Go to <a href="http://www.mahadalquran.com/maq/">mahad al quran</a> for further details.</p><p>Maula tus ni dua mubarak na tufail ma Allah taala Maula ni ummeed ne apna ma tamaam kare. Aameen.</p>';

    $sent_mail = $qrn_muhafiz->insert_cron_emails('akhequrani@talabulilm.com', $muhaffiz_email, $muhaffiz_subject, $muhaffiz_message);

    $mumin_subject = 'Tasmee Request';
    $mumin_message = $user_full_name . ' has accepted your Akh e-qurani request.<br><br>Email: ' . $muhaffiz_email . '<br>Contact: ' . $muhaffiz_contact . '<br>Whatsapp: ' . $muhaffiz_whatsapp . '<br>Skype: ' . $muhaffiz_skype . '<br><br>Now you may setup yours and your sadeeqs ideal tasmee time.<p>Remember to request and remind your sadeeq to update your hifz(tasmee) on his Listen(tehfeez) page, everytime you do tasmee.</p><p>You will be able to see your progress report on your tasmee report page.</p>';

    $sent_mail = $qrn_muhafiz->insert_cron_emails('akhequrani@talabulilm.com', $mumin_email, $mumin_subject, $mumin_message);

    if ($result) {
      $akh_email = $mumin_email;
      $akh_contact = $mumin_contact;

      $_SESSION[SUCCESS_MESSAGE] = "Akh e-qurani request Successfully Accepted.<br><br>Email: $akh_email<br>Contact No: $akh_contact<br><br>We have sent an email with these details.";
    } else {
      $_SESSION[ERROR_MESSAGE] = "Error while Accept";
    }
  }
}

$pending_request = $qrn_muhafiz->get_pending_request_for_tasmee($_SESSION[USER_ITS]);
if ($pending_request) {
  $count_pending_request = count($pending_request);
}

$select_mumin = $qrn_mumin->get_all_mumin_by_muhafiz($_SESSION[USER_ITS]);

require_once 'inc/inc.header2.php';
require_once 'inc/inc.footer.php';

$smarty = new Smarty;

// Header / Session variables
$smarty->assign("server_path", SERVER_PATH);
$smarty->assign("mumin_data_header", $mumin_data_header);
$smarty->assign("muhafiz_data_header", $muhafiz_data_header);
$smarty->assign("select_mumin", $select_mumin);
$smarty->assign("pending_request", $pending_request);
$smarty->assign("count_pending_request", $count_pending_request);
$smarty->assign("user_logedin", $user_logedin);
$smarty->assign("success_message", $_SESSION['SUCCESS_MESSAGE']);
$smarty->assign("error_message", $_SESSION['ERROR_MESSAGE']);

$smarty->display('qrn_tasmee.tpl');