<?php
  require 'smarty/libs/Smarty.class.php';
  require_once 'session.php';
  require_once 'requires_login.php';
  require_once 'classes/class.qrn_mumin.php';
  require_once 'classes/class.qrn_muhafiz.php';
  require_once 'classes/class.qrn_tasmee.php';
  require_once 'classes/class.user.php';
  
  $qrn_mumin = new Qrn_Mumin();
  $qrn_muhafiz = new Qrn_Muhafiz();
  $qrn_tasmee = new Qrn_Tasmee();
  $user_logedin = new mtx_user();
  
  $mumin_user_data = $mumin_its = FALSE;
  $muhafiz_its = $last_tasmee_surat = $last_tasmee_ayat = $prefered_time = $required_sanad = FALSE;
  $today_dt = date('Y-m-d');
  
  if (isset($_POST['submit'])) {
    $mumin_its = $_POST['mumin_its'];
    $last_tasmee_surat = $_POST['surah'];
    $last_tasmee_ayat = $_POST['ayat_to'];
    
    $mumin_data = $user_logedin->loaduser($mumin_its);
    
    $insert_mumin = $qrn_mumin->insert_mumin($mumin_its, $muhafiz_its, $today_dt, $last_tasmee_surat, $last_tasmee_ayat, $prefered_time, $required_sanad, $_SESSION[USER_ITS], $mumin_data->get_full_name(), $mumin_data->get_jamaat(), $mumin_data->get_jamiat(), $mumin_data->get_quran_sanad(), $mumin_data->get_gender());
    
    if ($insert_mumin) {
      $_SESSION[SUCCESS_MESSAGE] = 'Mumin data Successfully Inserted. Please click <a href="'.SERVER_PATH.'qrn_tasmee.php">Here</a> to go back to Tasmee';
      header('Location: '.SERVER_PATH.'qrn_tasmee.php');
    }
  }
  
  if (isset($_POST['search'])) {
    $mumin_its = $_POST['mumin_its'];
    $mumin_user_data = $user_logedin->check_user_exist($mumin_its);
    if (!$mumin_user_data) {
      $_SESSION[ERROR_MESSAGE] = 'Please Enter Correct ITS ID';
    }
  }
  
  $list_of_surahs = $qrn_tasmee->get_surat_list();
  
  require_once 'inc/inc.header2.php';
  require_once 'inc/inc.footer.php';
  
  $smarty = new Smarty;

  // Header / Session variables
  $smarty->assign("server_path", SERVER_PATH);
  $smarty->assign("mumin_data_header", $mumin_data_header);
  $smarty->assign("muhafiz_data_header", $muhafiz_data_header);
  $smarty->assign("mumin_its", $mumin_its);
  $smarty->assign("user_logedin", $user_logedin);
  $smarty->assign("mumin_user_data", $mumin_user_data);
  $smarty->assign("list_of_surahs", $list_of_surahs);
  $smarty->assign("success_message", $_SESSION['SUCCESS_MESSAGE']);
  $smarty->assign("error_message", $_SESSION['ERROR_MESSAGE']);

  $smarty->display('qrn_mumin_enroll.tpl');