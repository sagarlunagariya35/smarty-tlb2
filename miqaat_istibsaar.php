<?php
require 'smarty/libs/Smarty.class.php';
require_once 'session.php';
require_once 'requires_login.php';

$body_class = 'page-sub-page';

$year = $_GET['tag'];
$miqaat_istibsaars =  get_all_miqaat_istibsaar($year);

require_once 'inc/inc.header2.php';
require_once 'inc/inc.footer.php';

$smarty = new Smarty;

// Header / Session variables
$smarty->assign("server_path", SERVER_PATH);
$smarty->assign("year", $year);
$smarty->assign("miqaat_istibsaars", $miqaat_istibsaars);

$smarty->display('miqaat_istibsaar.tpl');