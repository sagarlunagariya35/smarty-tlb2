<?php
require_once 'session.php';
require_once 'requires_login.php';

$title = 'Event Details';

$event_id = ($_GET['event_id']) ? $_GET['event_id'] : FALSE;
$event_register = get_event_register($event_id,$_SESSION[USER_ITS]);
$event = ($event_id > 0) ? get_article_by_id('events', $event_id) : FALSE;


include ('admin/print_header.php');
?>
<body style="padding: 10px;">
  <div>
    <p style="display: block; text-align: right"><?php echo date('d-m-Y H:i:s'); ?></p>
  </div>
  
  <div class="row">
    <div class="col-lg-12">
      <h3 class="page-header"><?php echo $title; ?></h3>
    </div>
    <!-- /.col-lg-12 -->
  </div>
  <div class="row">&nbsp;</div>
  
    <div class="row">
      <div class="col-md-12">
        <table class="table table-responsive table-condensed">
          <thead>
            <tr>
              <th>Event Title</th>
              <th>Start Date</th>
              <th>End Date</th>
              <th>Capacity</th>
              <th>Venue</th>
              <th>Contact Person</th>
              <th>Contact E-mail</th>
              <th>Contact Mobile</th>
              <th>Register Id</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td><?php echo $event['event_title']; ?></td>
              <td><?php echo date('d, F Y',strtotime($event['start_date'])); ?></a></td>
              <td><?php echo date('d, F Y',strtotime($event['end_date'])); ?></td>
              <td><?php echo $event['capacity']; ?></td>
              <td><?php echo $event['venue']; ?></td>
              <td><?php echo $event['contact_person']; ?></td>
              <td><?php echo $event['email']; ?></td>
              <td><?php echo $event['mobile']; ?></td>
              <td><?php echo $event_register[0]['rand_id']; ?></td>
            </tr>
          </tbody>
        </table>
      </div>
    </div>
  </div>
</body>
<style>
  body{
    background-color: #FFF !important;
    font-size: 13px;
  }
  h3{
    font-size: 24px;
  }
</style>
</html>
