<?php
require 'smarty/libs/Smarty.class.php';
require_once 'session.php';
require_once 'classes/class.user.php';

$body_class = 'page-homepage-carousel';


$user = new mtx_user();
if (isset($_SESSION[USER_ITS])) {
  $user->loaduser($_SESSION[USER_ITS]);
}

$user_type = $user->get_user_type();

switch ($user_type) {
  case 'Student':
    $tag_line = '<span class="tag-author">المجلس السادس-1413</span><br>
تمام علوم نو جملة قراْن، لوگو جھ نے  دنيا نا علوم كهے  چھے يھ بھي سگلا قراْن ما چھے}، تارے  كوئي ايك نے  رواں نهيں  كھ امام الزمان ني رزا بغير، داعي ني رزا بغير ايك حرف بھي پڑهاوي سكے،';
    break;
  case 'Teacher':
    $tag_line = '<span class="tag-author">المجلس السابع-1428</span><br>
اهوو كرو كھ دنيا بندگي كرے، مدارس اسكولو ما بھي يه}مثل جانفشاني كرو، يه}مثل معلم معلمين ٹيچرو جوئيے، teachers اهنا پر جتنو كروو هوئي تھ كرو';
    break;
  case 'Professional':
    $tag_line = '<span class="tag-author">المجلس الثاني-1436</span><br>
رسول الله يھ فرمايوچھے كه"ان الله يحب الصانع المتقن في صنعه"، كھ خدا تعالى وه كاريگر نے  پسند كرے  چھے كھ جھ اهني كاريگري نے  اتقان سي كرے، مضبوطي سي كرے،
تواے  داودي بهره قوم ني جماعة! تميں جھ بھي كام كرتا هوئي اهنے  اتقان سي كرجو، خدا ني طاعة كري نے  حلال سي كرجو، حرام سي دور رهجو، انے  دنيا ماهر هنر ما تميں ذروة پر هوئي، top پر هوئي.
';
    break;
  case 'Counselor':
    $tag_line = '<span class="tag-author">المجلس الثاني-1436</span><br>
موعظة حسنة سوں؟ كھ كهواني ڈهب، اهوي شاكلة سي اداء كرے  كھ سامنے  والا قبول كري لے';
    break;
  case 'Mentor':
    $tag_line = '<span class="tag-author">المجلس الرابع-1421</span><br>
فرزند نے   اْج مھوٹا تھيا پچھي بھي ناهنا نے   ناهنا} سمجهے، يھ برابر نهيں، مھوٹا تھيا تو هوے   اهنے   مھوٹا سمجهوو لازم چھے، تو اهنے   يھ طريقة سي سمجهاوو جوئيے   كھ جھ نا سي اهنا گلے   يھ وات اترے، انے   اولياء الله ني سكهامنو بتاوي چھے يھ كهوي جوئيے';
    break;
  case 'Sponsor':
    $tag_line = '<span class="tag-author">المجلس السابع-1432</span><br>
اْ دنو ما نية كر ي لو كھ  اْ مثل نا مؤمنين نے  هميں  مدد كريسوں، انے  تميں  يھ سگلا نے  اْپو  تو عزة انے  كرامة سي، جمنا هاتھــ سي اْپو تو دابا هاتھــ نے  خبر نھ پڑے، انے  هرگز گناؤجو  مت، تميں  اْ مثل نا عمل كرسو تو يھ مؤمن گهنا خوش تھاسے، مگر مھوٹي وات يھ چھے كھ ميں  گهنو خوش تھئيس.';
    break;
  case 'Foster':
    $tag_line = '<span class="tag-author">المجلس السابع-1426</span><br>
ضرورة مندو مؤمنين نا رهائشي امور ساتھے، تمام ديني، دنيوي، تعليمي انے  اقتصادي امور اُستوار  تھائے، بهتر تھائے، انے  يھ خدمة جھ مستطيع  چھے يھ لئي اُٹھے، كوئي ايك گهر، كوئي بے  گهر، يا زيادة، يھ مثل ضرورة مند نے  Fostership اْپے، كھ پورا سال لگ يھ نو خيال راكهے';
    break;
}

require_once 'inc/inc.header.php';

$smarty = new Smarty;
// Header / Session variables
$smarty->assign("server_path", SERVER_PATH);
$smarty->assign("page_title", "");
$smarty->assign("loggedin_user", "");
$smarty->assign("active_menu", 'index');
$smarty->assign("active_tree", '');

$smarty->display('index.tpl');

require_once 'inc/inc.footer.php';