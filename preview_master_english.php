<?php
require 'smarty/libs/Smarty.class.php';
require_once 'session.php';
require_once 'requires_login.php';
require_once 'classes/class.user.php';
require_once 'classes/class.araz.php';

$page = 'preview_english';
$body_class = 'page-sub-page';

$marhala_id = $_SESSION[MARHALA_ID];
$user_its = $_SESSION[USER_ITS];

$marhala_school_array = array('1','2','3','4');

if($_SESSION['step'] != '6'){
  if($_SESSION['step'] != '7'){
    if($_SESSION['step'] <= '7'){
      if(in_array($marhala_id,$marhala_school_array)){
        header('location: ' . SERVER_PATH . 'marhala-' . $marhala_id . '/' . get_marhala_slug($marhala_id) . '/ques/');
      }else {
        header('location: ' . SERVER_PATH . 'marhala-' . $marhala_id . '/' . get_marhala_slug($marhala_id) . '/ques_english/');
      }
    }
  }
}

switch ($marhala_id) {
  case 1:
    $panel_heading = 'Pre-Primary Araz';
    $panel_heading_ar = 'الحضانة';
    break;
  case 2:
    $panel_heading = 'Primary Araz';
    $panel_heading_ar = 'الابتدائية الأولى';
    break;
  case 3:
    $panel_heading = 'Std 5th to Std 7th Araz';
    $panel_heading_ar = 'الابتدائية الثانية';
    break;
  case 4:
    $panel_heading = 'Std 8th to 10th Araz';
    $panel_heading_ar = 'الثانوية الأولى';
    break;
  case 5:
    $panel_heading = 'Std 11th - 12th';
    $panel_heading_ar = 'الثانوية الثانية';
    break;
  case 6:
    $panel_heading = 'Graduation';
    $panel_heading_ar = 'البكالوريا';
    break;
  case 7:
    $panel_heading = 'Post Graduation';
    $panel_heading_ar = 'الماجيستار';
    break;
  case 8:
    $panel_heading = 'Diploma';
    $panel_heading_ar = 'الدبلوم';
    break;
}

$user = new mtx_user();
$user->loaduser($_SESSION[USER_ITS]);

if($_SESSION['already_araz_done'] == '1'){
  
  $araz_pending = check_pending_araz($_SESSION[USER_ITS]);
  if($araz_pending)
  {
    $araz_id = get_pending_araz_data($_SESSION[USER_ITS]);
    header('location: ' . SERVER_PATH . 'araz-submit/' . $araz_id . '/');
    exit();
  }else {
    
    $araz = new mtx_araz();
    $araz->set_marhala($marhala_id);
    $araz->set_login_its($_SESSION[USER_ITS]);
    if($_SESSION['madrasah_name']){ $araz->set_madrasah_name($_SESSION['madrasah_name']); }
    if($_SESSION['madrasah_name_ar']){ $araz->set_madrasah_name_ar($_SESSION['madrasah_name_ar']); }
    if($_SESSION['madrasah_darajah']){ $araz->set_madrasah_darajah($_SESSION['madrasah_darajah']); }
    if($_SESSION['madrasah_country']){ $araz->set_madrasah_country($_SESSION['madrasah_country']); }
    if($_SESSION['madrasah_city']){ $araz->set_madrasah_city($_SESSION['madrasah_city']); }
    if($_SESSION['school_city']){ $araz->set_school_city($_SESSION['school_city']); }
    if($_SESSION['school_standard']){ $araz->set_school_standard($_SESSION['school_standard']); }
    if($_SESSION['school_name']){ $araz->set_school_name($_SESSION['school_name']); }
    if($_SESSION['school_filter']){ $araz->set_school_filter($_SESSION['school_filter']); }
    if($_SESSION['hifz_sanad']){ $araz->set_hifz_sanad($_SESSION['hifz_sanad']); }
    if($_SESSION['already_araz_done']){ $araz->set_already_araz_done($_SESSION['already_araz_done']); }
    if($_SESSION['already_araz_from_name']){ $araz->set_already_araz_from_name($_SESSION['already_araz_from_name']); }
    if($_SESSION['school_pincode']){ $araz->set_school_pincode($_SESSION['school_pincode']); }
    if($_SESSION['already_araz_done'] == '1'){ 
      $araz->set_jawab_given($_SESSION['already_araz_done']); 
      $araz->set_jawab_given_ts(date('Y-m-d')); 
    }

    if ($araz->add_to_db()) {
      $araz_id = $araz->get_id();
      $profile_questionaire = $araz->add_araz_profile_questionaire($araz_id, $_SESSION['taken_psychometric_aptitude_test'], $_SESSION['text_psychometric_aptitude'], $_SESSION['prefer_more_clarity'], $_SESSION['want_further_guidence'], $_SESSION['seek_funding_edu'], $_SESSION['want_assistance'], $_SESSION['received_sponsorship'], $_SESSION['follow_shariaa'], $_SESSION['disclaimer']);
      if(!in_array($marhala_id,$marhala_school_array)){
        $courses_ret = save_course_and_institution_to_db($araz_id, $_SESSION['choose_stream'], $_SESSION['comp_subject'], $marhala_id, $user_its, $_SESSION['save_institute_data'], $_SESSION['already_araz_done']);
      }
      $ret = save_ques_ans_to_db($araz_id, $_SESSION['ques']);
      if ($ret) {
        $save_araz_data = check_save_araz_data($_SESSION[USER_ITS]);
        if($save_araz_data){
          delete_save_araz_data($marhala_id, $_SESSION[USER_ITS]);
        }
        
        if($_SESSION['madrasah_darajah']){ unset($_SESSION['madrasah_darajah']); }
        if($_SESSION['madrasah_country']){ unset($_SESSION['madrasah_country']); }
        if($_SESSION['madrasah_city']){ unset($_SESSION['madrasah_city']); }
        if($_SESSION['madrasah_name']){ unset($_SESSION['madrasah_name']); }
        if($_SESSION['madrasah_name_ar']){ unset($_SESSION['madrasah_name_ar']); }
        if($_SESSION['hifz_sanad']){ unset($_SESSION['hifz_sanad']); }
        if($_SESSION['school_city']){ unset($_SESSION['school_city']); }
        if($_SESSION['school_standard']){ unset($_SESSION['school_standard']); }
        if($_SESSION['school_name']){ unset($_SESSION['school_name']); }
        if($_SESSION['school_filter']){ unset($_SESSION['school_filter']); }
        if($_SESSION['school_pincode']){ unset($_SESSION['school_pincode']); }
        if($_SESSION['ques']){ unset($_SESSION['ques']); }
        if($_SESSION['is_institute_selected']){ unset($_SESSION['is_institute_selected']); }
        if($_SESSION['taken_psychometric_aptitude_test']){ unset($_SESSION['taken_psychometric_aptitude_test']); }
        if($_SESSION['text_psychometric_aptitude']){ unset($_SESSION['text_psychometric_aptitude']); }
        if($_SESSION['prefer_more_clarity']){ unset($_SESSION['prefer_more_clarity']); }
        if($_SESSION['want_further_guidence']){ unset($_SESSION['want_further_guidence']); }
        if($_SESSION['seek_funding_edu']){ unset($_SESSION['seek_funding_edu']); }
        if($_SESSION['want_assistance']){ unset($_SESSION['want_assistance']); }
        if($_SESSION['received_sponsorship']){ unset($_SESSION['received_sponsorship']); }
        if($_SESSION['follow_shariaa']){ unset($_SESSION['follow_shariaa']); }
        if($_SESSION['ques']){ unset($_SESSION['ques']); }
        if($_SESSION['disclaimer']){ unset($_SESSION['disclaimer']); }
        header('location: ' . SERVER_PATH . 'araz-submit/' . $araz_id . '/');
      } else {
        //log_text(__FILE__ . "error adding questions to db");
      }
    } else {
      //log_text(__FILE__ . "error adding araz to db");
    }
  }
}else {
  if (isset($_POST['submit'])) {
    
    $araz_pending = check_pending_araz($_SESSION[USER_ITS]);
    if($araz_pending)
    {
      $araz_id = get_pending_araz_data($_SESSION[USER_ITS]);
      header('location: ' . SERVER_PATH . 'araz-submit/' . $araz_id . '/');
      exit();
    }else {
      
      $araz = new mtx_araz();
      $araz->set_marhala($marhala_id);
      $araz->set_login_its($_SESSION[USER_ITS]);

      $result = get_id_from_form_processes($_SESSION['form_id']);
      if(!$result){

        if($marhala_id == '6' || $marhala_id == '7' || $marhala_id == '8'){
          $c_inst_name = $_POST['institute_name'];
          $c_inst_city = $_POST['madrasah_city'];

          $araz->set_current_course($_POST['course_name']);
          $araz->set_current_institute_name($c_inst_name);
          $araz->set_current_institute_city($c_inst_city);
          $araz->set_current_institute_country($_POST['institute_country']);
          //$araz->set_current_inst_start_month($_POST['course_start_month']);
          //$araz->set_current_inst_start_year($_POST['course_start_year']);
          //$araz->set_current_inst_end_month(substr($_POST['course_end_month'],3));
          //$araz->set_current_inst_end_year(substr($_POST['course_end_year'],3));

          $find_lp_institute = get_lp_institute($c_inst_name, $marhala_id);
          if(!$find_lp_institute){
            $insert_lp_institute = insert_lp_institute($c_inst_name, $marhala_id, $c_inst_city);
          }
        }

        if($_SESSION['madrasah_name']){ $araz->set_madrasah_name($_SESSION['madrasah_name']); }
        if($_SESSION['madrasah_name_ar']){ $araz->set_madrasah_name_ar($_SESSION['madrasah_name_ar']); }
        if($_SESSION['madrasah_darajah']){ $araz->set_madrasah_darajah($_SESSION['madrasah_darajah']); }
        if($_SESSION['madrasah_country']){ $araz->set_madrasah_country($_SESSION['madrasah_country']); }
        if($_SESSION['madrasah_city']){ $araz->set_madrasah_city($_SESSION['madrasah_city']); }
        if($_SESSION['school_city']){ $araz->set_school_city($_SESSION['school_city']); }
        if($_SESSION['school_standard']){ $araz->set_school_standard($_SESSION['school_standard']); }
        if($_SESSION['school_name']){ $araz->set_school_name($_SESSION['school_name']); }
        if($_SESSION['school_filter']){ $araz->set_school_filter($_SESSION['school_filter']); }
        if($_SESSION['hifz_sanad']){ $araz->set_hifz_sanad($_SESSION['hifz_sanad']); }
        if($_SESSION['already_araz_done']){ $araz->set_already_araz_done($_SESSION['already_araz_done']); }
        if($_SESSION['already_araz_from_name']){ $araz->set_already_araz_from_name($_SESSION['already_araz_from_name']); }
        if($_SESSION['school_pincode']){ $araz->set_school_pincode($_SESSION['school_pincode']); }

        if ($araz->add_to_db($_SESSION['form_id'])) {
          $araz_id = $araz->get_id();
          $profile_questionaire = $araz->add_araz_profile_questionaire($araz_id, $_SESSION['taken_psychometric_aptitude_test'], $_SESSION['text_psychometric_aptitude'], $_SESSION['prefer_more_clarity'], $_SESSION['want_further_guidence'], $_SESSION['seek_funding_edu'], $_SESSION['want_assistance'], $_SESSION['received_sponsorship'], $_SESSION['follow_shariaa'], $_SESSION['disclaimer']);
          if(!in_array($marhala_id,$marhala_school_array)){
            $courses_ret = save_course_and_institution_to_db($araz_id, $_SESSION['choose_stream'], $_SESSION['comp_subject'], $marhala_id, $user_its, $_SESSION['save_institute_data'], $_SESSION['already_araz_done']);
          }
          $ret = save_ques_ans_to_db($araz_id, $_SESSION['ques']);
          if ($ret) {
            $save_araz_data = check_save_araz_data($_SESSION[USER_ITS]);
            if($save_araz_data){
              delete_save_araz_data($marhala_id, $_SESSION[USER_ITS]);
            }
        
            if($_SESSION['madrasah_darajah']){ unset($_SESSION['madrasah_darajah']); }
            if($_SESSION['madrasah_country']){ unset($_SESSION['madrasah_country']); }
            if($_SESSION['madrasah_city']){ unset($_SESSION['madrasah_city']); }
            if($_SESSION['madrasah_name']){ unset($_SESSION['madrasah_name']); }
            if($_SESSION['madrasah_name_ar']){ unset($_SESSION['madrasah_name_ar']); }
            if($_SESSION['hifz_sanad']){ unset($_SESSION['hifz_sanad']); }
            if($_SESSION['school_city']){ unset($_SESSION['school_city']); }
            if($_SESSION['school_standard']){ unset($_SESSION['school_standard']); }
            if($_SESSION['school_name']){ unset($_SESSION['school_name']); }
            if($_SESSION['school_filter']){ unset($_SESSION['school_filter']); }
            if($_SESSION['school_pincode']){ unset($_SESSION['school_pincode']); }
            if($_SESSION['ques']){ unset($_SESSION['ques']); }
            if($_SESSION['is_institute_selected']){ unset($_SESSION['is_institute_selected']); }
            if($_SESSION['taken_psychometric_aptitude_test']){ unset($_SESSION['taken_psychometric_aptitude_test']); }
            if($_SESSION['text_psychometric_aptitude']){ unset($_SESSION['text_psychometric_aptitude']); }
            if($_SESSION['prefer_more_clarity']){ unset($_SESSION['prefer_more_clarity']); }
            if($_SESSION['want_further_guidence']){ unset($_SESSION['want_further_guidence']); }
            if($_SESSION['seek_funding_edu']){ unset($_SESSION['seek_funding_edu']); }
            if($_SESSION['want_assistance']){ unset($_SESSION['want_assistance']); }
            if($_SESSION['received_sponsorship']){ unset($_SESSION['received_sponsorship']); }
            if($_SESSION['follow_shariaa']){ unset($_SESSION['follow_shariaa']); }
            if($_SESSION['ques']){ unset($_SESSION['ques']); }
            if($_SESSION['disclaimer']){ unset($_SESSION['disclaimer']); }
            header('location: ' . SERVER_PATH . 'araz-submit/' . $araz_id . '/');
          } else {
            //log_text(__FILE__ . "error adding questions to db");
          }
        } else {
          //log_text(__FILE__ . "error adding araz to db");
        }
      }
    }
  }
}

$institute_data = get_temp_institute_data($marhala_id, $user_its);
if(!$institute_data){
  $institute_data = @$_SESSION['save_institute_data'];
}

if ($marhala_id == 1) {
  $hifz_table = '';
  $madrasah_table = '';
  $institute_table = get_school_table();
} elseif (in_array($marhala_id, $marhala_school_array)) {
  $hifz_table = get_hifz_table();
  $madrasah_table = get_madrasah_table();
  $institute_table = get_school_table();
} else {
  $hifz_table = get_hifz_table();
  $madrasah_table = get_deeni_taalim_table();
  $institute_table = get_institute_table($institute_data);
}
$countries = get_all_countries();
$course_month_array = array('January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December');

require_once 'inc/inc.header2.php';
require_once 'inc/inc.footer.php';

$smarty = new Smarty;

// Header / Session variables
$smarty->assign("server_path", SERVER_PATH);
$smarty->assign("user", $user);
$smarty->assign("marhala_id", $marhala_id);
$smarty->assign("panel_heading", $panel_heading);
$smarty->assign("panel_heading_ar", $panel_heading_ar);
$smarty->assign("marhala_school_array", $marhala_school_array);
$smarty->assign("institute_data", $institute_data);
$smarty->assign("hifz_table", $hifz_table);
$smarty->assign("madrasah_table", $madrasah_table);
$smarty->assign("institute_table", $institute_table);
$smarty->assign("countries", $countries);
$smarty->assign("course_month_array", $course_month_array);

$smarty->display('preview_master_english.tpl');