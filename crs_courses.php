<?php
require 'smarty/libs/Smarty.class.php';
require_once 'session.php';
require_once 'requires_login.php';
require_once 'classes/class.user.php';
require_once 'classes/class.crs_topic.php';
require_once 'classes/class.crs_course.php';
require_once 'classes/class.crs_chapter.php';

$user_logedin = new mtx_user();
$crs_topic = new Crs_Topic();
$crs_course = new Crs_Course();
$crs_chapter = new Crs_Chapter();

if (isset($_REQUEST['topic_id'])) {
  $topic_id = $_REQUEST['topic_id'];
}

$topics_list = $crs_topic->get_all_topics();
$topic_details = $crs_topic->get_topic($topic_id);
$courses_list = $crs_course->get_all_courses_of_topic($topic_id);

require_once 'inc/inc.header2.php';
require_once 'inc/inc.footer.php';

$smarty = new Smarty;

// Header / Session variables
$smarty->assign("server_path", SERVER_PATH);
$smarty->assign("crs_topic", $crs_topic);
$smarty->assign("crs_chapter", $crs_chapter);
$smarty->assign("topics", $topics_list);
$smarty->assign("topic_details", $topic_details);
$smarty->assign("courses", $courses_list);

$smarty->display('crs_courses.tpl');