<?php
require_once 'session.php';
require_once 'requires_login.php';

$body_class = 'page-sub-page';
$miqat_id = $_GET['miqaat_id'];

$miqaat_stationaries = get_all_miqaat_files('miqaat_stationary', $miqat_id);

require_once 'inc/inc.header2.php';
?>
<style>
  .skip{
    display:block;
    max-width:500px;
    -webkit-transition: 0.4s;
    -moz-transition: 0.4s;
    -o-transition: 0.4s;
    transition: 0.4s;
    border: none;
    background-color: #549BC7;
    height: auto;
    line-height:20px;
    margin:20px auto;
    padding:10px;
    outline: none;
    color:#FFF;
    text-transform:uppercase;
    text-align:center;
    font-weight:bold;
    font-size:20px;
    float:right;
    text-decoration: underline none;
  }
</style>
<!-- Contents -->
<!-- ====================================================================================================== -->
<div class="container white-bg">
  <div class="col-md-12 col-sm-12">

    <div class="page-title">
      <p style="margin-top:5px;">
        <a href="#" class="active">Istibsaar 1436 H</a></p>
      <h1>Istibsaar 1436 H<span class="alfatemi-text">معلومات ذاتية</span></h1>
    </div>
  </div>
  <div class="clearfix"></div> <!-- do not delete -->
  <form class="forms1 white" name="raza_form" method="post">
    <div class="col-md-3 col-sm-12 hidden-xs">
      <div class="profile-box-static">
        <h3 class="uppercase text-center">Tareekhi Event</h3>
      </div>
      <div class="profile-box-static-bottom">
        <a href="#">» 1435 H<hr></a>
        <a href="#">» 1436 H<hr></a>
        <a href="#">» 1437 H<hr></a>
      </div>
    </div>

    <div class="col-md-8 col-sm-12"> 
      <div class="static-show-button lsd large-fonts col-xs-12 col-sm-5" dir="rtl">معلومات ذاتية</div>
      <div class="clearfix"></div>
      <div class="col-md-12">
        <?php
          if($miqaat_stationaries){
            $i = 1;
            foreach ($miqaat_stationaries as $mi){
              
              $filearray = explode('.',$mi['stationary_url']);
              $ext = explode('-',$filearray[0]);
              $file_name = $ext[1];
              
        ?>
            <div class="col-md-4">
              <div class="istibsaar-boxes text-center">
                <i class="fa fa-file fa-5x color1"></i>
                <a href="file_downloader.php?file=<?php echo $mi['stationary_url']; ?>" download="newfilename">
                <div class="istibsaar-boxes-bottom">
                  <p class="text-center"><?php echo $file_name; ?></p>
                </div>
                </a>
              </div>
            </div>
        <?php
            if($i++ % 4 == 0) echo '<div class="clearfix"><br></div><div class="clearfix"><br></div>';
            }
          }
        ?>
        
      </div>
    
      <div class="clearfix"></div>
    </div>
    <div class="clearfix"></div>
  </form>
</div>

<style>
  .istibsaar-boxes-bottom:hover{
    background-color: #549BC7;
  }
</style>

<?php
require_once 'inc/inc.footer.php';
?>
