<div class="container white-bg" style="padding:0px;">

  <footer>

    <p>&copy; Mahad al-Hasanaat al-Burhaniyah, All Rights Reserved.<span>Designed by: <a href="http://bohradevelopers.com" target="_blank">Bohradevelopers</a><br>Developed By: <a href="http://www.matrixsoftwares.com/" target="_blank">Matrix Software Solutions</a></span></p>
    <div class="clearfix"></div>
  </footer>
</div>


<script>
  $('.select-input').select2({width: '100%'});

  setTimeout(function () {
    $(".alert-message").fadeTo(500, 0).slideUp(500, function () {
      $(this).remove();
    });
  }, 60000);

  $('.carousel').carousel({
    interval: 5000
  });

</script>
</body>
</html>
