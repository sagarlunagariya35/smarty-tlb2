<?php
require 'smarty/libs/Smarty.class.php';
require_once 'session.php';
require_once 'requires_login.php';
require_once 'classes/class.qrn_mumin.php';
require_once 'classes/class.qrn_muhafiz.php';
require_once 'classes/class.qrn_tasmee.php';
require_once 'classes/class.user.php';

$qrn_mumin = new Qrn_Mumin();
$qrn_muhafiz = new Qrn_Muhafiz();
$qrn_tasmee = new Qrn_Tasmee();
$user_logedin = new mtx_user();
$user_logedin->loaduser($_SESSION[USER_ITS]);

$user_email = $user_logedin->get_email();
$user_contact = $user_logedin->get_mobile();
$user_whatsapp = $user_logedin->get_whatsapp();
$user_skype = $user_logedin->get_viber();

$last_tasmee_surat = $last_tasmee_ayat = $prefered_time = $required_sanad = FALSE;
$show_email = $show_mobile = $prefered_person_count = $current_sanad_id = FALSE;
$today_dt = date('Y-m-d');

if (isset($_POST['delete_mumin'])) {
  $mumin_its = $_POST['delete_mumin'];

  $get_mumin_data = $qrn_mumin->get_all_mumin($mumin_its);
  if ($get_mumin_data) {

    $muhafiz_its_data = $get_mumin_data[0]['muhafiz_id'];
    $mumin_name = $get_mumin_data[0]['mumin_full_name'];
    if ($muhafiz_its_data) {

      $muhafiz_user_data = $user_logedin->loaduser($muhafiz_its_data);
      $email = $muhafiz_user_data->get_email();
      $subject = 'Unregistered successfully from Akh al-Qurani (Tehfeez)';
      
      $message = 'Your sadeeq '. $mumin_name .' has unregistered himself. hence his records have been deleted from your report pages. you may want to contact him at '.'.<br><br>Email: ' . $user_email . '<br>Contact: ' . $user_contact . '<br>Whatsapp: ' . $user_whatsapp . '<br>Skype: ' . $user_skype . ' and inquire about him. you may want to try make him start again. if u would like him to become your sadeeq again guide him to go to select muhafiz page and click on the enter its tab and enter your its id there.';

      $sent_mail = $qrn_muhafiz->insert_cron_emails('akhequrani@talabulilm.com', $email, $subject, $message);
    }

    $delete_mumin = $qrn_mumin->delete_mumin($mumin_its);
    $delete_mumin_request = $qrn_muhafiz->delete_tasmee_request_by_its($mumin_its, 'mumin_its');
    $delete_mumin_schedule = $qrn_tasmee->delete_mumin_time_schedule($mumin_its);
  }
}

if (isset($_POST['delete_muhafiz'])) {
  $muhafiz_its = $_POST['delete_muhafiz'];

  $get_mumin_data = $qrn_mumin->get_all_mumin_by_muhafiz($muhafiz_its);
  if ($get_mumin_data) {
    foreach ($get_mumin_data as $mumin_value) {

      $mumin_its_data = $mumin_value['its_id'];
      $muhafiz_name = $mumin_value['muhafiz_full_name'];
      if ($mumin_its_data) {

        $mumin_user_data = $user_logedin->loaduser($mumin_its_data);
        $email = $mumin_user_data->get_email();
        $subject = 'Unregistered successfully from Akh al-Qurani (Tasmee)';
        $message = $muhafiz_name . ' has unregisterd himself from Akh al-Qurani (Tehfeez). You can select new muhaffiz from the select muhaffiz page.';

        $sent_mail = $qrn_muhafiz->insert_cron_emails('akhequrani@talabulilm.com', $email, $subject, $message);
      }
      $update_muhafiz_from_tlb_mumin = $qrn_muhafiz->update_data_of_mumin_by_muhafiz($muhafiz_its);
    }
  }

  $delete_muhafiz = $qrn_muhafiz->delete_muhafiz($muhafiz_its);
  $delete_muhafiz_request = $qrn_muhafiz->delete_tasmee_request_by_its($muhafiz_its, 'muhafiz_its');
  $delete_muhafiz_schedule = $qrn_tasmee->delete_muhafiz_time_schedule($muhafiz_its);
}

if (isset($_POST['submit'])) {
  $data = $_POST;
  $mumin = $_POST['mumin'];
  $muhafiz = $_POST['muhafiz'];
  
  $user_logedin->update_data($data);
  
  if ($mumin) {
    $last_tasmee_surat = $_POST['surah'];
    $last_tasmee_ayat = $_POST['ayat_to'];

    $mumin_data = $user_logedin->loaduser($_SESSION[USER_ITS]);

    $insert_mumin = $qrn_mumin->insert_mumin($_SESSION[USER_ITS], FALSE, $today_dt, $last_tasmee_surat, $last_tasmee_ayat, $prefered_time, $required_sanad, FALSE, $mumin_data->get_full_name(), $mumin_data->get_jamaat(), $mumin_data->get_jamiat(), $mumin_data->get_quran_sanad(), $mumin_data->get_gender());
    header('Location: ' . SERVER_PATH . 'qrn_select_muhaffiz.php');
  }

  if ($muhafiz) {
    $insert_muhafiz = $qrn_muhafiz->insert_muhafiz($_SESSION[USER_ITS], $today_dt, $show_email, $show_mobile, $prefered_time, $prefered_person_count, $current_sanad_id);
    header('Location: ' . SERVER_PATH . 'qrn_tasmee.php');
  }
}

$select_mumin = $qrn_mumin->get_all_mumin($_SESSION[USER_ITS]);
$select_muhafiz = $qrn_muhafiz->get_all_muhafiz($_SESSION[USER_ITS]);
$list_of_surahs = $qrn_tasmee->get_surat_list();

require_once 'inc/inc.header2.php';

if ($select_mumin) {
  $mumin_text = 'You are enrolled for Al-Akh al-Qurani program. - <a href="#" class="delete_mumin" id="' . $_SESSION[USER_ITS] . '">To unregister click here</a>';
  $mumin_selected = 'Checked disabled';
  $show_surat_ayat_options = FALSE;
} else {
  $mumin_text = 'I want to do Tasmee of My Hifz to my sadeeq (my friend of hifz). I want to start from following Surat and Aayat.';
  $mumin_selected = '';
  $show_surat_ayat_options = TRUE;
}

if ($select_muhafiz) {
  $muhafiz_text = 'You can now listen to Quran Hifz of your fellow brothers and do Tasmee of your Hifz to them. - <a href="#" class="delete_muhafiz" id="' . $_SESSION[USER_ITS] . '">To unregister click here</a>';
  $muhafiz_selected = 'Checked disabled';
} else {
  $muhafiz_text = 'I am also willing to listen to the Hifz of my sadeeq (my friend of hifz).';
  $muhafiz_selected = '';
}

require_once 'inc/inc.footer.php';

$smarty = new Smarty;

// Header / Session variables
$smarty->assign("server_path", SERVER_PATH);
$smarty->assign("mumin_data_header", $mumin_data_header);
$smarty->assign("muhafiz_data_header", $muhafiz_data_header);
$smarty->assign("select_mumin", $select_mumin);
$smarty->assign("select_muhafiz", $select_muhafiz);
$smarty->assign("user_email", $user_email);
$smarty->assign("user_contact", $user_contact);
$smarty->assign("user_whatsapp", $user_whatsapp);
$smarty->assign("user_skype", $user_skype);
$smarty->assign("user_its", $_SESSION[USER_ITS]);
$smarty->assign("list_of_surahs", $list_of_surahs);
$smarty->assign("success_message", @$_SESSION['SUCCESS_MESSAGE']);
$smarty->assign("error_message", @$_SESSION['ERROR_MESSAGE']);
$smarty->assign("mumin_text", $mumin_text);
$smarty->assign("mumin_selected", $mumin_selected);
$smarty->assign("show_surat_ayat_options", $show_surat_ayat_options);
$smarty->assign("muhafiz_text", $muhafiz_text);
$smarty->assign("muhafiz_selected", $muhafiz_selected);


$smarty->display('qrn_enroll.tpl');
