<?php
session_start();

require_once 'classes/constants.php';

// verify if the source is its else redirect to its login
if (!in_array($_SERVER['HTTP_REFERER'], array('http://www.its52.com/ServiceLogin.aspx?domain=www.talabulilm.com&continue=http://www.talabulilm.com/ITS/Authentication/', 'http://www.its52.com/'))) {
  header('location:' . SERVER_PATH . 'index.html');
  exit();
}

require_once 'classes/class.user.php';
require_once 'classes/class.survey.php';
require_once('classes/gen_functions.php');

$srv_survey = new Mtx_Survey();

if (isset($_GET['ITSID'])) {
  $user_its = $_GET['ITSID'];
  $cls_user = new mtx_user();
  $is_valid = $cls_user->check_user_exist($user_its);
  if ($is_valid) {
    $_SESSION[USER_LOGGED_IN] = TRUE;
    $_SESSION[USER_ID] = $cls_user->get_user_id();
    $_SESSION[USER_ITS] = $user_its;
    
    $redirect_link = $cls_user->get_general_setting_data('login');
    
    $hashcode = get_my_jawab_sent_araz_by_its($_SESSION[USER_ITS], 'code');
    $srv_jamaat = $cls_user->get_jamaat();
    $srv_jamiat = $cls_user->get_jamiat();
    $srv_marhala = $hashcode['marhala'];
    if ($srv_marhala < 5) {
      $srv_school = $hashcode['school_name'];
    } else {
      $aprv_inst = get_my_araz_approved_institutions_data($hashcode['id']);
      $srv_school = $aprv_inst['institute_name'];
    }

    $survey_result = $srv_survey->get_open_surveys($_SESSION[USER_ITS], $srv_jamaat, $srv_jamiat, $srv_marhala, $srv_school);

    if ($survey_result) {
      $survey_id = $survey_result[0]['survey_id'];
      $already_attend_survey = $srv_survey->get_attend_survey($survey_id, $_SESSION[USER_ITS]);
      if (!$already_attend_survey) {
        $redirect_link = SERVER_PATH . 'srv_take_survey_mahad.php';
      }
    }

    header('Location: ' . $redirect_link);
  } else {
    header('location:' . SERVER_PATH . 'index.html');
  }
}
