<?php
require 'smarty/libs/Smarty.class.php';
require_once 'session.php';
require_once 'requires_login.php';

// Page bady class
$page = 'ques_ar';
$body_class = 'page-sub-page';

$araz_id = $_REQUEST['araz_id'];
$check1 = $check2 = $check3 = $check4 = FALSE;
$marhala_id = get_marhala_from_araz($araz_id);

if (isset($_POST['submit'])) {
  $delete_ques = delete_marhala_question_answers($araz_id);
  
  if($delete_ques){
    $all_ques = serialize($_POST);
    $ret = save_ques_ans_to_db($araz_id, $all_ques);
    
    if($ret){
      header('location: '. SERVER_PATH . 'review.php');
      exit();
    }
  }
}

$page_heading = 'Azaaim';
$page_heading_ar = 'عزائم';
$araz_data = get_marhala_question_answers($araz_id);

$araz_notes = get_notes_by_marhala($marhala_id);
$ques_preffered = get_marhala_ques_preffered($marhala_id);
$ques = get_marhala_ques($marhala_id);

require_once 'inc/inc.header2.php';
require_once 'inc/inc.footer.php';

$smarty = new Smarty;

// Header / Session variables
$smarty->assign("server_path", SERVER_PATH);
$smarty->assign("araz_id", $araz_id);
$smarty->assign("marhala_id", $marhala_id);
$smarty->assign("page_heading", $page_heading);
$smarty->assign("page_heading_ar", $page_heading_ar);
$smarty->assign("araz_data", $araz_data);
$smarty->assign("araz_notes", $araz_notes);
$smarty->assign("ques_preffered", $ques_preffered);
$smarty->assign("ques", $ques);
$smarty->assign("page", $page);
$smarty->assign("check1", $check1);
$smarty->assign("check2", $check2);
$smarty->assign("check3", $check3);
$smarty->assign("check4", $check4);
$smarty->assign("success_message", $_SESSION['SUCCESS_MESSAGE']);
$smarty->assign("error_message", $_SESSION['ERROR_MESSAGE']);

$smarty->display('complete_ques_details.tpl');
