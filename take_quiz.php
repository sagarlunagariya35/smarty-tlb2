<?php
require_once 'session.php';
require_once 'requires_login.php';
require_once 'classes/class.quiz.php';

$user_id = $_SESSION[USER_ID];
$user_its = $_SESSION[USER_ITS];

if (isset($_SESSION[GROUP_ID]) && $_SESSION[GROUP_ID] != '') {
  $group_id = (int) $_SESSION[GROUP_ID];
} else {
  $group_id = '';
}
// get groups
$groups = Quiz::get_groups();
if ($group_id)
  $min_group_id = $group_id;
else
  $min_group_id = $groups['MIN_GROUP_ID'];
$max_group_id = $groups['MAX_GROUP_ID'];

// check wheather user has cleared any test or not.
$cleared_test = Quiz::get_cleared_test($user_id, $group_id);
//print_r($cleared_test);
if ($group_id) {
  $cleared_last_group_id = $group_id;
  $query = "SELECT * FROM `questions` WHERE `group_id` = '$cleared_last_group_id' ORDER BY `que_id` ASC";
  $result = $db->query_fetch_full_result($query);
  $cleared_last_que_id = $result[0]['que_id'] - 1;
} else if ($cleared_test) {
  $cleared_last_group_id = $cleared_test['group_id'];
  $cleared_last_que_id = $cleared_test['que_id'];
  $next_que = $cleared_last_que_id + 1;
  // check wheather there are any question remaining to give answer
  $query = "SELECT * FROM `questions` WHERE `group_id` = '$cleared_last_group_id' AND `que_id` = '$next_que'";
  $result = $db->query_fetch_full_result($query);
  if ($result) {
    $min_group_id = $cleared_last_group_id;
    $cleared_last_que_id = $result[0]['que_id'] - 1;
    $next_que_id = $result[0]['que_id'];
  } else {
    $min_group_id = $cleared_last_group_id + 1;
  }
} else {
  $cleared_last_group_id = $min_group_id;
  $cleared_last_que_id = 0;
}

// group contains how many questions
$total_group_que = Quiz::total_group_questions($cleared_last_group_id);
$min_que_id = $total_group_que['MIN_QUE_ID'];
$max_que_id = $total_group_que['MAX_QUE_ID'];

// get current question to show here
$questions = Quiz::get_questions($cleared_last_group_id, $cleared_last_que_id);
//print_r($questions);
if ($cleared_last_group_id != $questions['group_id'])
  $cleared_last_que_id = 0;

$user_acquired_points = Quiz::get_group_acquired_points($cleared_last_group_id, $user_id);
require_once 'inc/inc.header2.php';
?>

<!-- Contents -->
<!-- ====================================================================================================== -->
<div class="container white-bg">
  <div class="col-md-12 col-sm-12">

    <div class="page-title">
      <p style="margin-top:5px;">
        <a href="#" class="active">Asharah Mubarakah 1436 H</a></p>
      <h1>Asharah Mubarakah 1436 H<!--span class="alfatemi-text">معلومات ذاتية</span--></h1>
    </div>
  </div>
  <div class="clearfix"></div> <!-- do not delete -->
  <form class="forms1 white" name="raza_form" method="post">
    <div class="col-md-4 col-sm-12">
      <div class="profile-box-static">
        <h3 class="uppercase text-center">Tareekhi Event</h3>
      </div>
      <div class="profile-box-static-bottom">
        <a href="https://www.talabulilm.com/test2/istibsaar/1435/">» 1435 H<hr></a>
        <a href="https://www.talabulilm.com/test2/istibsaar/1436/">» 1436 H<hr></a>
        <a href="https://www.talabulilm.com/test2/istibsaar/1437/">» 1437 H<hr></a>
      </div>
    </div>

    <div class="col-md-8 col-sm-12">
      <div class="static-show-button lsd large-fonts col-xs-12 col-sm-5" dir="rtl">Asharah Mubarakah</div>
      <div class="clearfix"></div>
      <div>
        <div class="multitextbuttton lsd large-fonts col-xs-12 col-sm-5" id="group_list" dir="rtl">Level <?php echo $min_group_id . ' / ' . $max_group_id; ?></div>
        <div class="quiz-point col-xs-12 col-sm-4 color1" dir="rtl">Point&nbsp;<span id="points"><?php echo $user_acquired_points; ?></span></div>
        <div class="quiz-point col-xs-12 col-sm-4 color1">
          <h2><a href="#"><img src="images/prev.png" height="25"></a> &nbsp; <b id="question_flow"> <?php echo (isset($next_que_id) ? $next_que_id : ($cleared_last_que_id + 1)) . ' / ' . $max_que_id; ?> </b> &nbsp; <a href="#"><img src="images/next.png" height="25"></a></h2>
        </div>
        <div class="clearfix"></div>
        
        <div class="quiz-point-ques">
          <div id="question_panel">
            <div class="blue-box1 rtl border"> <!-- Add class rtl -->
              <h3><span class="inline-block lh3 arb_ques"><?php echo $questions['question']; ?></span></h3>
            </div>
          
            <input type="hidden" id="current_que_id" value="<?php echo $questions['que_id']; ?>">
            <input type="hidden" id="current_group_id" value="<?php echo $questions['group_id']; ?>">

            <div class="col-md-12 col-sm-12">
              <div class="quiz-point-radio rtl"> <!-- Add Class .rtl -->
                <input type="radio" name="optionsRadios" value="1" id="radio11" class="" />
                <label for="radio11" style="color: #000;" class="radGroup3 lsd"><?php echo $questions['opt1']; ?></label>
              </div>
            </div>
            <div class="col-md-12 col-sm-12">
              <div class="quiz-point-radio rtl"> <!-- Add Class .rtl -->
                <input type="radio" name="optionsRadios" value="2" id="radio21" class="" />
                <label for="radio21" style="color: #000;" class="radGroup3 lsd"><?php echo $questions['opt2']; ?></label>
              </div>
            </div>
            <div class="col-md-12 col-sm-12">
              <div class="quiz-point-radio rtl"> <!-- Add Class .rtl -->
                <input type="radio" name="optionsRadios" value="3" id="radio31" class="" />
                <label for="radio31" style="color: #000;" class="radGroup3 lsd"><?php echo $questions['opt3']; ?></label>
              </div>
            </div>
            <div class="col-md-12 col-sm-12">
              <div class="quiz-point-radio rtl"> <!-- Add Class .rtl -->
                <input type="radio" name="optionsRadios" value="4" id="radio41" class="" />
                <label for="radio41" style="color: #000;" class="radGroup3 lsd"><?php echo $questions['opt4']; ?></label>
              </div>
            </div>
          </div>
          <div class="col-md-12 col-sm-12"><br></div>
          <div class="col-md-12 col-sm-12">
            <button type="button" id="next" class="btn btn-success pull-right col-md-2">Next</button>
          </div>
          <div class="col-md-12 col-sm-12"><br></div>
          <div class="clearfix"></div>
        </div>
      </div>
    </div>
    <div class="clearfix"></div>
  </form>
</div>

<script type="text/javascript">
  $('#next').on('click', function(e) {
    e.preventDefault();
    var panel = $('#question_panel');
    var cur_qid = $('#current_que_id').val();
    var cur_grp_id = $('#current_group_id').val();
    var answer = $('input:radio[name=optionsRadios]:checked').val();
    if (typeof answer === 'undefined') {
      alert('Please select the answer');
      return false;
    }
    myApp.showPleaseWait();
    $.ajax({
      url: 'ajax.php',
      method: 'POST',
      data: 'que_id=' + cur_qid + '&group_id=' + cur_grp_id + '&query=get_next_questions&answer=' + answer,
      success: function(response) {
        if (response != '') {
          var data = $.parseJSON(response);
          var group_id = data.group_id;

          // update current group if current != reponse.group_id
          if (group_id != cur_grp_id) {
            $('#group_list').text('Level ' + data.group_id + ' / ' + data.max_group_id);
          }

          var hidden_field = '<input type="hidden" id="current_que_id" value="' + data.que_id + '"><input type="hidden" id="current_group_id" value="' + data.group_id + '">';
          // update current question = response.questions
          $('#question_flow').text(data.que_id + ' / ' + data.max_que_id);
          $('#points').text(data.acquired_points);
          var question = '<div class="blue-box1 rtl border"><h3><span class="inline-block lh3 arb_ques">' + data.question + '</span></h3></div>';
          var opt = '';
          opt += create_dom(data.opt1, 1);
          opt += create_dom(data.opt2, 2);
          opt += create_dom(data.opt3, 3);
          opt += create_dom(data.opt4, 4);
          panel.empty().append(question + hidden_field + opt);
          myApp.hidePleaseWait();
        } else {
          window.location.href = "result.php";
        }
      },
      error: function(response) {

      }
    });
  });

  function create_dom(output, val) {

    var element = '<div class="col-md-12 col-sm-12"><div class="quiz-point-radio rtl"><input type="radio" name="optionsRadios" value="1" id="radio11" class="" /><label for="radio11" style="color: #000;" class="radGroup3 lsd">' + output + '</label></div></div>';
    return element;
  }

  var myApp;
  myApp = myApp || (function() {
    var pleaseWaitDiv = $('<div class="modal fade" id="pleaseWaitDialog" data-backdrop="static" data-keyboard="false"><div class="modal-dialog"><div class="modal-content"><div class="modal-header"><h4 class="modal-title">Processing . . .</h4></div><div class="modal-body"><img src="images/loader.gif"></div></div></div></div>');
    return {
      showPleaseWait: function() {
        pleaseWaitDiv.modal();
      },
      hidePleaseWait: function() {
        pleaseWaitDiv.modal('hide');
      },
    };
  })();

</script>

<?php
require_once 'inc/inc.footer.php';
?>