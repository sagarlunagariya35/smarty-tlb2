<?php
  require 'smarty/libs/Smarty.class.php';
  require_once 'session.php';
  require_once 'requires_login.php';
  require_once 'classes/class.qrn_mumin.php';
  require_once 'classes/class.qrn_muhafiz.php';
  require_once 'classes/class.qrn_tasmee.php';
  require_once 'classes/class.user.php';
  
  $qrn_mumin = new Qrn_Mumin();
  $qrn_muhafiz = new Qrn_Muhafiz();
  $qrn_tasmee = new Qrn_Tasmee();
  
  $user_logedin = new mtx_user();
  $user_logedin->loaduser($_SESSION[USER_ITS]);
  
  $timestamp = date('Y-m-d');
  $tasmee_records = $qrn_tasmee->get_tasmee_log_of_muhafiz_date_wise_for_graph($_SESSION[USER_ITS], $timestamp);
  $muhafiz_data = $qrn_muhafiz->get_all_muhafiz($_SESSION[USER_ITS]);
  $yesterday_ayat_count = $qrn_muhafiz->get_yesterday_ayat_count($_SESSION[USER_ITS]);
  
  require_once 'inc/inc.header2.php';
  require_once 'inc/inc.footer.php';
  
  $smarty = new Smarty;

  // Header / Session variables
  $smarty->assign("server_path", SERVER_PATH);
  $smarty->assign("mumin_data_header", $mumin_data_header);
  $smarty->assign("muhafiz_data_header", $muhafiz_data_header);
  $smarty->assign("tasmee_records", $tasmee_records);
  $smarty->assign("muhafiz_data", $muhafiz_data);
  $smarty->assign("yesterday_ayat_count", $yesterday_ayat_count);
  $smarty->assign("success_message", $_SESSION['SUCCESS_MESSAGE']);
  $smarty->assign("error_message", $_SESSION['ERROR_MESSAGE']);

  $smarty->display('qrn_report_muhafiz.tpl');