<?php
require 'smarty/libs/Smarty.class.php';
require_once 'session.php';
require_once 'requires_login.php';

$body_class = 'page-sub-page';

$year = $_GET['year'];
$art_id = trim($_GET['page'], "/");

$audios = FALSE;

$menu_id = get_value_by_name('tlb_menu', $year,' AND `level` = (SELECT id FROM `tlb_menu` WHERE `name` = \'Activities\')');
$all_articles = get_article_by_submenu('article', $menu_id[0]['id']);
$all_categories = get_category_of_article_by_submenu('article', $menu_id[0]['id']);
$article = ($art_id > 0) ? get_article_by_id('article', $art_id) : FALSE;
$videos = get_media_by_id('video', $article['id']);
$audios = get_media_by_id('audio', $article['id']);
$images = get_media_by_id('image', $article['id']);

require_once 'inc/inc.header2.php';
require_once 'inc/inc.footer.php';

$smarty = new Smarty;

// Header / Session variables
$smarty->assign("server_path", SERVER_PATH);
$smarty->assign("year", $year);
$smarty->assign("art_id", $art_id);
$smarty->assign("menu_id", $menu_id);
$smarty->assign("all_articles", $all_articles);
$smarty->assign("all_categories", $all_categories);
$smarty->assign("article", $article);
$smarty->assign("videos", $videos);
$smarty->assign("audios", $audios);
$smarty->assign("images", $images);

$smarty->display('activities.tpl');
