<?php

require_once 'class.database.php';

class Qrn_Tasmee {

  function insert_tasmee_log($muhafiz_its, $mumin_its, $timestamp, $from_surat, $from_ayat, $to_surat, $to_ayat, $total_ayat, $line_count) {
    global $db;

    $query = "INSERT INTO `qrn_tasmee_log`(`muhafiz_its`, `mumin_its`, `timestamp`, `from_surat`, `from_ayat`, `to_surat`, `to_ayat`, `total_ayat`, `line_count`) VALUES ('$muhafiz_its','$mumin_its','$timestamp','$from_surat', '$from_ayat', '$to_surat', '$to_ayat', '$total_ayat', '$line_count')";

    $result = $db->query($query);
    return $result;
  }

  function update_tasmee_log($muhafiz_its, $mumin_its, $timestamp, $from_surat, $from_ayat, $to_surat, $to_ayat, $total_ayat, $line_count, $id) {
    global $db;
    $update_query = "UPDATE `qrn_tasmee_log` SET `muhafiz_its`='$muhafiz_its', `mumin_its`='$mumin_its', `timestamp`='$timestamp', `from_surat`='$from_surat', `from_ayat`='$from_ayat', `to_surat`='$to_surat', `to_ayat`='$to_ayat', `total_ayat`='$total_ayat', `line_count`='$line_count' WHERE id = '$id' ";

    $result = $db->query($update_query);
    return $result;
  }
  
  function update_last_tasmee_log($to_surat, $to_ayat, $total_ayat, $id) {
    global $db;
    $update_query = "UPDATE `qrn_tasmee_log` SET `to_surat`='$to_surat', `to_ayat`='$to_ayat', `total_ayat`='$total_ayat' WHERE id = '$id' ";

    $result = $db->query($update_query);
    return $result;
  }
  
  function get_all_tasmee_log($id = FALSE) {
    global $db;
    $query = "SELECT * FROM `qrn_tasmee_log`";
    
    ($id) ? $query .= " WHERE `id` LIKE '$id'" : "";
    
    $result = $db->query_fetch_full_result($query);
    return $result;
  }
  
  function get_tasmee_log_of_mumin($mumin_its, $muhafiz_its) {
    global $db;
    $cur_date = date('Y-m-d');
    
    $query = "SELECT * FROM `qrn_tasmee_log` WHERE `muhafiz_its` LIKE '$muhafiz_its' AND `mumin_its` LIKE '$mumin_its'";
    $result = $db->query_fetch_full_result($query);
    
    $query = "SELECT `accept_ts` FROM `qrn_tasmee_request` WHERE `muhafiz_its` LIKE '$muhafiz_its' AND `mumin_its` LIKE '$mumin_its'";
    $ts_result = $db->query_fetch_full_result($query);
    
    $res['Days_Past'] = $res['Total_Record'] = $res['Total_Ayat'] = FALSE;
    
    if ($result) {
      $from = new DateTime($ts_result[0]['accept_ts']);
      $to = new DateTime('today');
      $days_past = $from->diff($to)->days;
      
      $res['Days_Past'] = $days_past;
      $count_record = count($result);
      $res['Total_Record'] = $count_record;
      foreach ($result as $value) {
        $res['Total_Ayat'] += $value['total_ayat'];
      }
      
      return $res;
    }else {
      return FALSE;
    }
  }
  
  function get_mumin_total_ayat_count($its) {
    global $db;
    $query = "SELECT `last_tasmee_surat`, `last_tasmee_ayat` FROM `qrn_mumin` WHERE `its_id` LIKE '$its'";
    $result = $db->query_fetch_full_result($query);
    
    if ($result) {
      $surah_no = $result[0]['last_tasmee_surat'];
      $ayat_no = $result[0]['last_tasmee_ayat'];
      
      $query = "SELECT `id` FROM `qrn_data` WHERE `surah_no` LIKE '$surah_no' AND `aayat_no` LIKE '$ayat_no'";
      $result = $db->query_fetch_full_result($query);
      
      return $result[0]['id'];
    }
  }
  
  function get_tasmee_log_of_mumin_date_wise_for_graph($its, $to_date) {
    global $db;
    $from_date = date('Y-m-d', strtotime('-7 days'));
    
    $query = "SELECT *, SUM(`total_ayat`) as sum_total_ayat FROM `qrn_tasmee_log` WHERE `mumin_its` LIKE '$its' AND `timestamp` BETWEEN '$from_date' AND '$to_date' GROUP BY `timestamp` ORDER BY `timestamp` ASC";
    $result = $db->query_fetch_full_result($query);
    return $result;
  }
  
  function get_tasmee_log_of_mumin_date_wise_for_table($its, $to_date) {
    global $db;
    $from_date = date('Y-m-d', strtotime('-7 days'));
    
    $query = "SELECT * FROM `qrn_tasmee_log` WHERE `mumin_its` LIKE '$its' AND `timestamp` BETWEEN '$from_date' AND '$to_date' ORDER BY `timestamp` DESC";
    $result = $db->query_fetch_full_result($query);
    return $result;
  }
  
  function get_tasmee_log_of_muhafiz_date_wise_for_graph($its, $to_date) {
    global $db;
    $from_date = date('Y-m-d', strtotime('-7 days'));
    
    $query = "SELECT *, SUM(`total_ayat`) as sum_total_ayat FROM `qrn_tasmee_log` WHERE `muhafiz_its` LIKE '$its' AND `timestamp` BETWEEN '$from_date' AND '$to_date' GROUP BY `timestamp` ORDER BY `timestamp` ASC";
    $result = $db->query_fetch_full_result($query);
    return $result;
  }
  
  function get_tasmee_log_of_muhafiz_date_wise_for_table($its, $to_date) {
    global $db;
    $from_date = date('Y-m-d', strtotime('-7 days'));
    
    $query = "SELECT * FROM `qrn_tasmee_log` WHERE `muhafiz_its` LIKE '$its' AND `timestamp` BETWEEN '$from_date' AND '$to_date' ORDER BY `timestamp` DESC";
    $result = $db->query_fetch_full_result($query);
    return $result;
  }
  
  function delete_tasmee_log($id) {
    global $db;
    $query = "DELETE FROM `qrn_tasmee_log` WHERE `id` LIKE '$id'";
    $result = $db->query($query);
    return $result;
  }
  
  function get_last_tasmee_entry($its) {
    global $db;
    $query = "SELECT * FROM `qrn_tasmee_log` WHERE `mumin_its` LIKE '$its' ORDER BY `id` DESC LIMIT 0,1";
    $result = $db->query_fetch_full_result($query);
    return $result[0];
  }
  
  function get_quran_data_of_surah($surah, $ayat_no) {
    global $db;
    $query = "SELECT * FROM `qrn_data` WHERE `surah_no` LIKE '$surah' AND `aayat_no` LIKE '$ayat_no'";
    $result = $db->query_fetch_full_result($query);
    return $result[0];
  }
  
  function insert_mumin_time_schedule($muhafiz_its, $mumin_its, $days, $hour, $time, $timestamp) {
    global $db;

    $query = "INSERT INTO `qrn_time_schedule`(`muhafiz_its`, `mumin_its`, `timestamp`, `days`, `hour`, `time`) VALUES ('$muhafiz_its','$mumin_its','$timestamp','$days', '$hour', '$time')  ON DUPLICATE KEY UPDATE `timestamp` = '$timestamp', `days` = '$days', `hour` = '$hour', `time` = '$time'";

    $result = $db->query($query);
    return $result;
  }
  
  function get_my_time_schedule($muhafiz_its) {
    global $db;
    $query = "SELECT * FROM `qrn_time_schedule` WHERE `muhafiz_its` = '$muhafiz_its'";
    $result = $db->query_fetch_full_result($query);
    return $result;
  }
  
  function get_mumin_time_schedule($muhafiz_its, $mumin_its) {
    global $db;
    $query = "SELECT * FROM `qrn_time_schedule` WHERE `muhafiz_its` = '$muhafiz_its' AND `mumin_its` = '$mumin_its'";
    $result = $db->query_fetch_full_result($query);
    return $result[0];
  }
  
  function delete_time_schedule($id) {
    global $db;
    $query = "DELETE FROM `qrn_time_schedule` WHERE `id` LIKE '$id'";
    $result = $db->query($query);
    return $result;
  }
  
  function delete_mumin_time_schedule($mumin_its) {
    global $db;
    $query = "DELETE FROM `qrn_time_schedule` WHERE `mumin_its` LIKE '$mumin_its'";
    $result = $db->query($query);
    return $result;
  }
  
  function delete_muhafiz_time_schedule($muhafiz_its) {
    global $db;
    $query = "DELETE FROM `qrn_time_schedule` WHERE `muhafiz_its` LIKE '$muhafiz_its'";
    $result = $db->query($query);
    return $result;
  }
  
  function get_surat_list() {
    global $db;
    $query = "SELECT * FROM `qrn_surah_names`";
    $result = $db->query_fetch_full_result($query);
    return $result;
  }
  
  function get_surat_name($key) {
    global $db;
    $query = "SELECT * FROM `qrn_surah_names`";
    $result = $db->query_fetch_full_result($query);
    if ($result) {
      foreach ($result as $data) {
        $surah[$data['id']] = $data['name'];
      }
    }
    return $surah[$key];
  }
  
}