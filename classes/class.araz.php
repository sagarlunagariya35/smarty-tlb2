<?php

require_once 'class.database.php';

class mtx_araz {

  private $id = '';
  private $marhala;
  private $login_its;
  private $madrasah_darajah;
  private $madrasah_country;
  private $madrasah_state;
  private $madrasah_city;
  private $madrasah_name;
  private $madrasah_name_ar;
  private $school_city;
  private $school_standard;
  private $school_name;
  private $araz_ts;
  private $school_state;
  private $nisaab;
  private $school_filter;
  private $hifz_sanad;
  private $already_araz_done;
  private $already_araz_from_name;
  private $school_pincode;
  //add this field to db
  private $approved;
  private $approved_by_ts;
  private $approved_by_its;
  private $jawab_given;
  private $jawab_given_by;
  private $jawab_given_ts;
  private $current_course;
  private $current_institute_name;
  private $current_institute_city;
  private $current_institute_country;
  private $current_inst_start_month;
  private $current_inst_start_year;
  private $current_inst_end_month;
  private $current_inst_end_year;



  /**
   * 
   * @global type $db
   * @param type $id
   * @return boolean
   * @assert ('') == false
   * @assert ('50476733') == true
   */
  public function mtx_araz($id = '') {
    global $db;
    if ($id == '') {
      Return FALSE;
      exit;
    }
    $sql = "SELECT * from tbl_araz WHERE id = '$id'";
    $ret = $db->query_fetch_full_result($sql);
    $ret = $ret[0];
    if ($ret) {
      $this->id = $id;
      $this->set_marhala($ret['marhala']);
      $this->set_login_its($ret['login_its']);
      $this->set_madrasah_darajah($ret['madrasah_darajah']);
      $this->set_madrasah_country($ret['madrasah_country']);
      $this->set_madrasah_state($ret['madrasah_state']);
      $this->set_madrasah_city($ret['madrasah_city']);
      $this->set_madrasah_name($ret['madrasah_name']);
      $this->set_madrasah_name_ar($ret['madrasah_name_ar']);
      $this->set_school_city($ret['school_city']);
      $this->set_school_standard($ret['school_standard']);
      $this->set_school_name($ret['school_name']);
      $this->set_araz_ts($ret['araz_ts']);
      $this->set_school_state($ret['school_state']);
      $this->set_nisaab($ret['nisaab']);
      $this->set_school_filter($ret['school_filter']);
      $this->set_hifz_sanad($ret['hifz_sanad']);
      $this->set_already_araz_done($ret['already_araz_done']);
      $this->set_already_araz_from_name($ret['already_araz_from_name']);
      $this->set_school_pincode($ret['school_pincode']);
      $this->set_jawab_given($ret['jawab_given']);
      $this->set_jawab_given_ts($ret['jawab_ts']);
      $this->set_current_course($ret['current_course']);
      $this->set_current_institute_name($ret['current_inst_name']);
      $this->set_current_institute_city($ret['current_inst_city']);
      
    } else {
      //log_text(__FILE__ . "mtx_araz" . $db->get_last_err());
      return FALSE;
    }
  }
  
  
  public function get_madrasah_details_from_attalim_its($its) {

    $ip = "162.208.53.164";
    //Data, connection, auth
    $soapUrl = "http://www.attalimims.com/AttalimWebService.asmx"; // asmx URL of WSDL

    // xml post structure
    $xml_post_string = '<?xml version="1.0" encoding="utf-8"?>
    <soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
      <soap:Body>
        <GetStudentITSForTalabulilm xmlns="http://www.attalimims.com/">
          <IPAddress>'.$ip.'</IPAddress>
          <StrKey>TASub052</StrKey>
          <ITSNo>'.$its.'</ITSNo>
        </GetStudentITSForTalabulilm>
      </soap:Body>
    </soap:Envelope>';

    $headers = array(
        "Content-type: text/xml;charset=\"utf-8\"",
        "Accept: text/xml",
        "Cache-Control: no-cache",
        "Pragma: no-cache",
        "SOAPAction: http://www.attalimims.com/GetStudentITSForTalabulilm",
        "Content-length: " . strlen($xml_post_string)
    );

    // PHP cURL  for https connection with auth
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $soapUrl);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    //curl_setopt($ch, CURLOPT_USERPWD, $soapUser . ":" . $soapPassword); // username and password - declared at the top of the doc
    curl_setopt($ch, CURLOPT_POST, true);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $xml_post_string); // the SOAP request
    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

    // converting
    $response = curl_exec($ch);
    curl_close($ch);

    //log_text(__FILE__ . '-response text-' . $response);

    if ($response == FALSE) {
      return FALSE;
    }
    $xml = simplexml_load_string($response);

    $madrasah_data = $xml->xpath('//Table');
    if($madrasah_data)
    return $madrasah_data[0];    
  }
  
  public function get_madrasah_details_from_ejamaat_its($its) {

    //Data, connection, auth
    $soapUrl = "http://www.its52.com/eJas/EjamaatServices.asmx"; // asmx URL of WSDL

    // xml post structure
    $xml_post_string = '<?xml version="1.0" encoding="utf-8"?>
    <soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
      <soap:Body>
        <MSB_StudentInfo xmlns="http://localhost/eJAS/EjamaatServices">
          <EjamaatId>'.$its.'</EjamaatId>
          <strKey>Talabulilm562138</strKey>
        </MSB_StudentInfo>
      </soap:Body>
    </soap:Envelope>';

    $headers = array(
        "Content-type: text/xml;charset=\"utf-8\"",
        "Accept: text/xml",
        "Cache-Control: no-cache",
        "Pragma: no-cache",
        "SOAPAction: http://localhost/eJAS/EjamaatServices/MSB_StudentInfo",
        "Content-length: " . strlen($xml_post_string)
    );

    // PHP cURL  for https connection with auth
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $soapUrl);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    //curl_setopt($ch, CURLOPT_USERPWD, $soapUser . ":" . $soapPassword); // username and password - declared at the top of the doc
    curl_setopt($ch, CURLOPT_POST, true);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $xml_post_string); // the SOAP request
    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

    // converting
    $response = curl_exec($ch);
    curl_close($ch);

    //log_text(__FILE__ . '-response text-' . $response);

    if ($response == FALSE) {
      return FALSE;
    }
    $xml = simplexml_load_string($response);

    $madrasah_data = $xml->xpath('//Table');
    if($madrasah_data)
     return $madrasah_data[0];    
  }

  /**
   * 
   * @global type $db
   * @return boolean
   * @assert () == true
   */
  public function add_to_db($form_id=FALSE) {
    global $db;
    $sql = "INSERT INTO tlb_araz (marhala, login_its, madrasah_darajah, madrasah_country, madrasah_state, madrasah_city, madrasah_name, madrasah_name_ar, school_city, school_standard, school_name, araz_ts, school_state, nisaab, school_filter, hifz_sanad, already_araz_done, already_araz_from_name, school_pincode, jawab_given, jawab_ts, current_course, current_inst_name, current_inst_city, current_inst_country, inst_start_month, inst_start_year, inst_end_month, inst_end_year) VALUES ('$this->marhala', '$this->login_its', '$this->madrasah_darajah', '$this->madrasah_country', '$this->madrasah_state', '$this->madrasah_city', '$this->madrasah_name', '$this->madrasah_name_ar', '$this->school_city', '$this->school_standard', '$this->school_name', NOW(), '$this->school_state', '$this->nisaab', '$this->school_filter', '$this->hifz_sanad', '$this->already_araz_done', '$this->already_araz_from_name', '$this->school_pincode', '$this->jawab_given', '$this->jawab_given_ts', '$this->current_course', '$this->current_institute_name', '$this->current_institute_city', '$this->current_institute_country', '$this->current_inst_start_month', '$this->current_inst_start_year', '$this->current_inst_end_month', '$this->current_inst_end_year')";

    $ret = $db->query($sql);
    if ($ret) {
      $this->id = $db->get_last_insert_id();
      
      if($form_id){
        $time = time();
        $query = "INSERT INTO forms_processed VALUES ('$form_id','$this->id','$time')";
        $db->query($query);
      }
      
      return TRUE;
    } else {

      //log_text(__FILE__ . "add_to_db" . $db->get_last_err());
      return FALSE;
    }
  }
  
  function add_araz_profile_questionaire($araz_id, $taken_psychometric_aptitude_test, $text_psychometric_aptitude, $prefer_more_clarity, $want_further_guidence, $seek_funding_edu, $want_assistance, $received_sponsorship, $follow_shariaa, $disclaimer) {
    
    global $db;
    $query = "UPDATE `tlb_araz` SET `taken_psychometric_aptitude_test` = '$taken_psychometric_aptitude_test', `text_psychometric_aptitude` = '$text_psychometric_aptitude', `prefer_more_clarity` = '$prefer_more_clarity', `want_further_guidence` = '$want_further_guidence', `seek_funding_edu` = '$seek_funding_edu', `want_assistance` = '$want_assistance', `received_sponsorship` = '$received_sponsorship', `follow_shariaa` = '$follow_shariaa', `disclaimer` = '$disclaimer' WHERE `id` = '$araz_id'";
    
    $ret = $db->query($query);
    return $ret;
  }

  /**
   * 
   * GETTERS-----------------------------------
   */
  public function get_id() {
    return $this->id;
  }

  public function get_marhala() {
    return $this->marhala;
  }

  public function get_login_its() {
    return $this->login_its;
  }

  public function get_madrasah_darajah() {
    return $this->madrasah_darajah;
  }

  public function get_madrasah_country() {
    return $this->madrasah_country;
  }

  public function get_madrasah_state() {
    return $this->madrasah_state;
  }

  public function get_madrasah_city() {
    return $this->madrasah_city;
  }

  public function get_madrasah_name() {
    return $this->madrasah_name;
  }
  
  public function get_madrasah_name_ar() {
    return $this->madrasah_name_ar;
  }

  public function get_school_city() {
    return $this->school_city;
  }

  public function get_school_standard() {
    return $this->school_standard;
  }

  public function get_school_name() {
    return $this->school_name;
  }

  public function get_araz_ts() {
    return $this->araz_ts;
  }

  public function get_school_state() {
    return $this->school_state;
  }

  public function get_nisaab() {
    return $this->nisaab;
  }

  public function get_school_filter() {
    return $this->school_filter;
  }

  public function get_hifz_sanad() {
    return $this->hifz_sanad;
  }

  public function get_already_araz_done() {
    return $this->already_araz_done;
  }

  public function get_already_araz_from_name() {
    return $this->already_araz_from_name;
  }
  
  public function get_school_pincode() {
    return $this->school_pincode;
  }

  public function get_approved() {
    return $this->approved;
  }

  public function get_approved_by_ts() {
    return $this->approved_by_ts;
  }

  public function get_approved_by_its() {
    return $this->approved_by_its;
  }

  public function get_jawab_given() {
    return $this->jawab_given;
  }

  public function get_jawab_given_by() {
    return $this->jawab_given_by;
  }

  public function get_jawab_given_ts() {
    return $this->jawab_given_ts;
  }
  
  public function get_current_course() {
    return $this->current_course;
  }
  public function get_current_institute_name() {
    return $this->current_institute_name;
  }
  public function get_current_institute_city() {
    return $this->current_institute_city;
  }
  public function get_current_institute_country() {
    return $this->current_institute_country;
  }
  public function get_current_inst_start_month() {
    return $this->current_inst_start_month;
  }
  public function get_current_inst_start_year() {
    return $this->current_inst_start_year;
  }
  public function get_current_inst_end_month() {
    return $this->current_inst_end_month;
  }
  public function get_current_inst_end_year() {
    return $this->current_inst_end_year;
  }

  /**
   * 
   * SETTERS---------------------
   * 
   */
  public function set_marhala($marhala) {
    $this->marhala = $marhala;
  }

  public function set_login_its($login_its) {
    $this->login_its = $login_its;
  }

  public function set_madrasah_darajah($madrasah_darajah) {
    $this->madrasah_darajah = $madrasah_darajah;
  }

  public function set_madrasah_country($madrasah_country) {
    $this->madrasah_country = $madrasah_country;
  }

  public function set_madrasah_state($madrasah_state) {
    $this->madrasah_state = $madrasah_state;
  }

  public function set_madrasah_city($madrasah_city) {
    $this->madrasah_city = $madrasah_city;
  }

  public function set_madrasah_name($madrasah_name) {
    $this->madrasah_name = $madrasah_name;
  }
  
  public function set_madrasah_name_ar($madrasah_name_ar) {
    $this->madrasah_name_ar = $madrasah_name_ar;
  }

  public function set_school_city($school_city) {
    $this->school_city = $school_city;
  }

  public function set_school_standard($school_standard) {
    $this->school_standard = $school_standard;
  }

  public function set_school_name($school_name) {
    $this->school_name = $school_name;
  }

  public function set_school_state($school_state) {
    $this->school_state = $school_state;
  }

  public function set_nisaab($nisaab) {
    $this->nisaab = $nisaab;
  }

  public function set_school_filter($school_filter) {
    $this->school_filter = $school_filter;
  }

  public function set_hifz_sanad($hifz_sanad) {
    $this->hifz_sanad = $hifz_sanad;
  }

  public function set_already_araz_done($already_araz_done) {
    $this->already_araz_done = $already_araz_done;
  }

  public function set_already_araz_from_name($already_araz_from_name) {
    $this->already_araz_from_name = $already_araz_from_name;
  }

  public function set_school_pincode($school_pincode) {
    $this->school_pincode = $school_pincode;
  }

  public function set_approved($approved) {
    $this->approved = $approved;
  }

  public function set_approved_by_ts($approved_by_ts) {
    $this->approved_by_ts = $approved_by_ts;
  }

  public function set_approved_by_its($approved_by_its) {
    $this->approved_by_its = $approved_by_its;
  }

  public function set_jawab_given($jawab_given) {
    $this->jawab_given = $jawab_given;
  }

  public function set_jawab_given_by($jawab_given_by) {
    $this->jawab_given_by = $jawab_given_by;
  }

  public function set_jawab_given_ts($jawab_given_ts) {
    $this->jawab_given_ts = $jawab_given_ts;
  }
  
  public function set_current_course($current_course) {
    $this->current_course = $current_course;
  }
  
  public function set_current_institute_name($current_institute_name) {
    $this->current_institute_name = $current_institute_name;
  }
  
  public function set_current_institute_city($current_institute_city) {
    $this->current_institute_city = $current_institute_city;
  }
  
  public function set_current_institute_country($current_institute_country) {
    $this->current_institute_country = $current_institute_country;
  }
  
  public function set_current_inst_start_month($current_inst_start_month) {
    $this->current_inst_start_month = $current_inst_start_month;
  }
  
  public function set_current_inst_start_year($current_inst_start_year) {
    $this->current_inst_start_year = $current_inst_start_year;
  }
  
  public function set_current_inst_end_month($current_inst_end_month) {
    $this->current_inst_end_month = $current_inst_end_month;
  }
  
  public function set_current_inst_end_year($current_inst_end_year) {
    $this->current_inst_end_year = $current_inst_end_year;
  }

    public function check_if_araz_pending() {
    global $db;
    //Todo 
    $query = "SELECT COUNT(*) count from tlb_araz WHERE login_its LIKE '" . $this->login_its . "' AND jawab_given <> 1 ";

    $result = $db->query_fetch_full_result($query);

    return $result[0]['count'];
  }

  public function approve($approved_by_its) {
    global $db;
    $sql = "UPDATE `tlb_araz` SET `approved`= 1, `approved_by_its` = $approved_by_its, `approved_ts` = NOW() WHERE id = '$this->id'";
    $ret = $db->query($sql);
    if ($ret) {
      return TRUE;
    } else {
      //log_text(__FILE__ . approved . ' error in approved update query');
      return FALSE;
    }
  }

}
