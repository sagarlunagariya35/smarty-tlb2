<?php
// Database Constants
define('DB_API_SERVER','localhost');
define('DB_API_USER','root');
//define('DB_API_USER','talabuli_data');
define('DB_API_PASS', 'matrix2');
//define('DB_API_PASS', 'tlbilm@5253');
//define('DB_API_NAME','talabuli_data');
define('DB_API_NAME','talabuli_dev');
//define('DB_API_NAME','talabulilm_new');

//define('SERVER_PATH', 'https://www.talabulilm.com/');
//define('SERVER_PATH', 'http://localhost/tlb2/dev2/');
define('SERVER_PATH', 'http://localhost/smarty_tlb2/');
//define('SERVER_PATH', 'https://www.talabulilm.com/dev2/'); // this is the dev server address

define('ITS_KEY', '1qaz2w24sx');

define('USER_LOGGED_IN', 'user_logged_in');
define('USER_ID', 'user_id');
define('USER_ITS', 'user_its');
define('GROUP_ID', 'group_id');
define('ENABLE_WRITE_LOG', FALSE);
define('MARHALA_ID', 'marhala_id');
define('IS_ADMIN', 'is_admin');
define('USER_ROLE', 'user_role');
define('TANZEEM_ID', 'tanzeem_id');
define('USER_TYPE','user_type');
define('JAMAT_NAME','jamat_name');
define('EDU_HUB','edu_hub');
define('IS_EDU_HUB','is_edu_hub');
define('SUCCESS_MESSAGE','success_message');
define('ERROR_MESSAGE','error_message');

define('ROLE_HEAD_OFFICE', 1);
define('ROLE_AAMIL_SAHEB', 2);
define('ROLE_EDU_SEC', 4);
define('ROLE_TEACHER', 5);
define('ROLE_COMMITTEE_MEMBER', 8);
define('ROLE_COUNSELOR', 9);
define('ROLE_SAHEB', 10);
define('ROLE_REVIEWER', 11);
define('ROLE_PROJECT_COORDINATOR', 12);
define('ROLE_ADMIN_MARHALA_1', 13);
define('ROLE_ADMIN_MARHALA_2', 14);
define('ROLE_ADMIN_MARHALA_3', 15);
define('ROLE_ADMIN_MARHALA_4', 16);
define('ROLE_ADMIN_MARHALA_5', 17);
define('ROLE_ADMIN_MARHALA_6', 18);
define('ROLE_ADMIN_MARHALA_7', 19);
define('ROLE_ADMIN_MARHALA_8', 20);
define('ROLE_ASHARAH_ADMIN', 21);
define('ROLE_DATA_ENTRY', 22);
define('ROLE_JAMIAT_COORDINATOR', 23);

define('ACTIVITIES', 1);
define('ISTIBSAAR', 2);
define('INSTITUTES', 3);
define('MARAHIL', 4);
define('EVENTS', 16);
define('ISTIFAADAH', 19);

define('COUNSELOR_AND_SAHEB_CONTENT', 1);
define('JAWAB_SENT_CONTENT', 1);

define('LOGIN_REDIRECT', 'login');
define('SUBMIT_REDIRECT', 'submit_link');
define('HOME_CAROUSAL', 'home');
define('SUBMIT_CAROUSAL', 'submit');

define('CRS_JAMAAT', 1);
define('CRS_JAMIAT', 2);
define('CRS_SCHOOL', 3);
define('CRS_ITS_ID', 4);
define('CRS_MARHALA', 5);

define('CRS_ELEMENT_VIDEO', 1);
define('CRS_ELEMENT_AUDIO', 2);
define('CRS_ELEMENT_IMAGE', 3);
define('CRS_ELEMENT_FILE', 4);
define('CRS_ELEMENT_OPEN_HTML', 5);
define('CRS_ELEMENT_HTML_TEXT', 6);

define('QUIZ_MULTIPLE_CHOICE', 1);
define('QUIZ_TRUE_AND_FALSE', 2);
define('QUIZ_FILL_IN_THE_BLANK', 3);
define('QUIZ_MATCH_THE_FOLLOWING', 4);
define('QUIZ_OPEN_ANSWERS', 5);
define('QUIZ_SEQUENCING', 6);
define('QUIZ_GENERAL_HTML', 7);

define('COURSE_NOT_IN_LIST', 1);
define('INSTITUTE_NOT_IN_LIST', 2);
define('COUNTRY_NOT_IN_LIST', 3);
define('CITY_NOT_IN_LIST', 4);
?>
