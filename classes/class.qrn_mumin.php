<?php

require_once 'class.database.php';

class Qrn_Mumin {

  function insert_mumin($its, $muhafiz_its, $doj, $last_tasmee_surat, $last_tasmee_ayat, $prefered_time, $required_sanad, $registered_by, $mumin_full_name, $mumin_jamaat, $mumin_jamiat, $mumin_quran_sanad, $mumin_gender) {
    global $db;

    $query = "INSERT INTO `qrn_mumin`(`its_id`, `muhafiz_id`, `doj`, `last_tasmee_surat`, `last_tasmee_ayat`, `prefered_time`, `required_sanad`, `registered_by`, `mumin_full_name`, `mumin_jamaat`, `mumin_jamiat`, `mumin_quran_sanad`, `mumin_gender`) VALUES ('$its','$muhafiz_its','$doj','$last_tasmee_surat','$last_tasmee_ayat', '$prefered_time', '$required_sanad', '$registered_by', '$mumin_full_name', '$mumin_jamaat', '$mumin_jamiat', '$mumin_quran_sanad', '$mumin_gender')";

    $result = $db->query($query);
    return $result;
  }

  function update_mumin($its, $muhafiz_its, $doj, $last_tasmee_surat, $last_tasmee_ayat, $prefered_time, $required_sanad, $muhafiz_full_name, $muhafiz_jamaat, $muhafiz_jamiat, $muhafiz_quran_sanad, $muhafiz_gender) {
    global $db;
    $update_query = "UPDATE `qrn_mumin` SET `muhafiz_id`='$muhafiz_its', `doj`='$doj', `last_tasmee_surat`='$last_tasmee_surat', `last_tasmee_ayat`='$last_tasmee_ayat', `prefered_time`='$prefered_time', `required_sanad`='$required_sanad', `muhafiz_full_name`='$muhafiz_full_name', `muhafiz_jamaat`='$muhafiz_jamaat', `muhafiz_jamiat`='$muhafiz_jamiat', `muhafiz_quran_sanad`='$muhafiz_quran_sanad', `muhafiz_gender`='$muhafiz_gender' WHERE its_id = '$its' ";

    $result = $db->query($update_query);
    return $result;
  }
  
  function update_surah_and_ayat($its, $last_tasmee_surat, $last_tasmee_ayat) {
    global $db;
    $update_query = "UPDATE `qrn_mumin` SET `last_tasmee_surat`='$last_tasmee_surat', `last_tasmee_ayat`='$last_tasmee_ayat' WHERE its_id = '$its' ";

    $result = $db->query($update_query);
    return $result;
  }
  
  function update_muhafiz_data_of_mumin($its, $muhafiz_its, $muhafiz_full_name, $muhafiz_jamaat, $muhafiz_jamiat, $muhafiz_quran_sanad, $muhafiz_gender) {
    global $db;
    $update_query = "UPDATE `qrn_mumin` SET `muhafiz_id`='$muhafiz_its', `muhafiz_full_name`='$muhafiz_full_name', `muhafiz_jamaat`='$muhafiz_jamaat', `muhafiz_jamiat`='$muhafiz_jamiat', `muhafiz_quran_sanad`='$muhafiz_quran_sanad', `muhafiz_gender`='$muhafiz_gender' WHERE its_id = '$its' ";

    $result = $db->query($update_query);
    return $result;
  }
  
  function get_all_mumin($its = FALSE) {
    global $db;
    $query = "SELECT * FROM `qrn_mumin`";
    
    ($its) ? $query .= " WHERE `its_id` LIKE '$its'" : "";
    
    $result = $db->query_fetch_full_result($query);
    return $result;
  }
  
  function delete_mumin($its) {
    global $db;
    $query = "DELETE FROM `qrn_mumin` WHERE `its_id` LIKE '$its'";
    $result = $db->query($query);
    return $result;
  }
  
  function get_all_mumin_by_muhafiz($its) {
    global $db;
    $query = "SELECT * FROM `qrn_mumin` WHERE `muhafiz_id` LIKE '$its'";
    $result = $db->query_fetch_full_result($query);
    return $result;
  }

  function get_mumin_name($its) {
    global $db;
    $query = "SELECT mumin_full_name FROM `qrn_mumin` WHERE `its_id` LIKE '$its'";
    $result = $db->query_fetch_full_result($query);
    return $result[0]['mumin_full_name'];
  }

}