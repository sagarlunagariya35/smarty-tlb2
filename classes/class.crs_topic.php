<?php
require_once 'class.database.php';

class Crs_Topic {

  function get_all_topics($parent_topic = FALSE, $sort = 'ASC') {
    global $db;
    $query = "SELECT * FROM `crs_topic` WHERE `is_active` LIKE '1' AND ";
    $query .= ($parent_topic) ? "`parent_id` LIKE '$parent_topic'" : "`parent_id` LIKE '0'";
    
    $query .= " ORDER BY `sort_id` $sort";
    $result = $db->query_fetch_full_result($query);
    return $result;
  }
  
  function get_topic($id) {
    global $db;
    $query = "SELECT * FROM `crs_topic` WHERE `id` LIKE '$id'";
    $result = $db->query_fetch_full_result($query);
    return $result[0];
  }

  
}
