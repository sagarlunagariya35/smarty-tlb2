<?php
require_once 'class.database.php';

class Crs_Chapter {  
  
  function get_all_chapters($course = FALSE, $sort = 'ASC') {
    global $db;
    $query = "SELECT * FROM `crs_chapter` WHERE `is_active` LIKE '1'";
    ($course) ? $query .= "AND `course_id` LIKE '$course' " : '';
    
    $query .= "ORDER BY `sort_id` $sort";
    $result = $db->query_fetch_full_result($query);
    return $result;
  }
  
  function get_all_chapter_elements($chapter) {
    global $db;
    $query = "SELECT * FROM `crs_element` WHERE `chapter_id` LIKE '$chapter' AND `is_active` LIKE '1' ORDER BY `sort_id` ASC";
    $result = $db->query_fetch_full_result($query);
    return $result;
  }
  
  function get_chapter($id) {
    global $db;
    $query = "SELECT * FROM `crs_chapter` WHERE `id` LIKE '$id'";
    $result = $db->query_fetch_full_result($query);
    return $result[0];
  }

  
}
