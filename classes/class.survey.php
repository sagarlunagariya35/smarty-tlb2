<?php
require_once 'class.database.php';

class Mtx_Survey {
  
  function get_open_surveys($its_id, $jamat, $jamiat, $marhala, $school){
    global $db;
    
    $query = "SELECT * FROM txn_open_surveys WHERE "
            . "(`its` = '$its_id'"
            . " OR `jamaat` = '$jamat'"
            . " OR `jamiat` = '$jamiat'"
            . " OR `marhala` = '$marhala'"
            . " OR `school` = '$school')"
            . " AND `is_active` = '1'";
    
    $result = $db->query_fetch_full_result($query);
    return $result;
  }
  
  function get_attend_survey($survey_id, $its_id){
    global $db;
    $query = "SELECT * FROM srv_survey_ques_answers WHERE `survey_id` = '$survey_id' AND `user_id` = '$its_id'";
    $result = $db->query_fetch_full_result($query);
    return $result;
  }
  
  function get_survey($survey_id){
    global $db;
    $query = "SELECT * FROM txn_open_surveys WHERE `survey_id` = '$survey_id'";
    $result = $db->query_fetch_full_result($query);
    return $result[0];
  }
  
  function get_survey_questions($survey_id){
    global $db;
    $query = "SELECT * FROM srv_survey_questions WHERE `survey_id` = '$survey_id'";
    $result = $db->query_fetch_full_result($query);
    return $result;
  }
  
  function get_survey_ques_correct_answer($survey_id, $que_id) {
    global $db;
    $query = "SELECT `question_type`,`correct_answer` FROM `srv_survey_questions` WHERE `survey_id` = '$survey_id' AND `id` = '$que_id'";
    
    $result = $db->query_fetch_full_result($query);
    return $result[0];
  }
  
  function add_survey_ques_answers($user_id, $survey_id, $ques_answers) {
    global $db;
    
    
    $result_ques_answers = unserialize($ques_answers);
    $date = date('Y-m-d');
    
    if ($result_ques_answers) {
      foreach ($result_ques_answers as $ques => $answer) {
        if ($ques != "srvsubmit") {
          $answer2 = FALSE;
          $que_id = ltrim($ques, "your_answer");
          
          if (preg_match('/-/', $que_id)) {
            $ques_detail = explode('-', $que_id);
            $que_id = $ques_detail[0];
            $answer2 = $answer;
          } else {
            $que_id = (int) $que_id;
          }
          
          if ($que_id == '26' AND $answer2) {
            $query = "UPDATE `srv_survey_ques_answers` SET `answer` = CONCAT(answer,',$answer2') WHERE `que_id` = '$que_id' AND `user_id` = '$user_id'";
            
          } else if ($que_id != '26' AND $answer2) {
            $query = "UPDATE `srv_survey_ques_answers` SET `answer2` = '$answer2' WHERE `que_id` = '$que_id' AND `user_id` = '$user_id'";
            
          } else {
            $query = "INSERT INTO `srv_survey_ques_answers`(`user_id`, `survey_id`, `que_id`, `answer`, `date`) VALUES('$user_id', '$survey_id', '$que_id', '$answer', '$date') ON DUPLICATE KEY UPDATE `answer` = '$answer', `date` = '$date'";
          }
          
          $result = $db->query($query);
        }
      }
      return $result;
    }
  }
  
  public function get_survey_ques_answers($survey_id, $user_id) {
    global $db;
    $query = "SELECT * FROM `srv_survey_ques_answers` WHERE `survey_id` = '$survey_id' AND `user_id` = '$user_id' ORDER BY id ASC";
    $result = $db->query_fetch_full_result($query);
    return $result;
  }
  
 
}
