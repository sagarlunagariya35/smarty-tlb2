<?php

require_once 'class.database.php';

class Quiz {

  public static function get_sentences($sr_id) {
    global $db;
    $query = "SELECT * FROM `sentences` WHERE `sr` = '$sr_id'";
    $result = $db->query_fetch_full_result($query);
    return $result[0];
  }

  public static function get_meanings($sr_id) {
    global $db;
    $query = "SELECT * FROM `meanings` WHERE `qid` = '$sr_id'";
    $result = $db->query_fetch_full_result($query);
    return $result;
  }

  public static function get_list_questions($group_id) {
    global $db;
    $query = "SELECT * FROM `questions` WHERE `group_id` = '$group_id' ORDER BY que_id ASC";
    $result = $db->query_fetch_full_result($query);
    return $result;
  }

  public static function get_questions($last_group_id, $last_ques_id) {
    global $db;
    $last_ques_id += 1;
    $query = "SELECT * FROM `questions` WHERE `que_id` = '$last_ques_id' AND `group_id` = '$last_group_id' LIMIT 0, 1";
    //echo $query;
    $result = $db->query_fetch_full_result($query);
    //print_r($result);
    // if results are found than there are questions else
    // we will go to get questions from next group id by $last_group_id + 1;
    if (!$result) {
      $last_group_id += 1;
      $query = "SELECT * FROM `questions` WHERE `group_id` = '$last_group_id' ORDER BY `id` ASC LIMIT 0, 1";
      //echo '<br>'.$query;
      $result = $db->query_fetch_full_result($query);
      //print_r($result);
    }
    return $result[0];
  }

  public static function get_group_acquired_points($group_id, $user_id) {
    global $db;
    $query = "SELECT IFNULL(SUM(`points`), 0) point FROM `user_answers` WHERE `group_id` = '$group_id' AND `user_id` = '$user_id'";
    $result = $db->query_fetch_full_result($query);
    return $result[0]['point'];
  }

  public static function get_total_points($user_id) {
    global $db;
    $query = "SELECT IFNULL(SUM(`points`), 0) point FROM `user_answers` WHERE `group_id` = '$group_id' AND `user_id` = '$user_id'";
    $result = $db->query_fetch_full_result($query);
    return $result[0]['point'];
  }

  public static function get_user_answer($group_id, $que_id, $user_id) {
    global $db;
    $query = "SELECT * FROM `user_answers` WHERE `group_id` = '$group_id' AND `que_id` = '$que_id' AND `user_id` = '$user_id'";
    $result = $db->query_fetch_full_result($query);
    return $result[0];
  }

  public function get_user_answers_for_course($course_id, $user_id) {
    global $db;
    $query = "SELECT * FROM `user_take_courses` WHERE `course_id` = '$course_id' AND `user_id` = '$user_id' ORDER BY id ASC";
    $result = $db->query_fetch_full_result($query);
    return $result;
  }
  
  public function pre_fill_user_answers_for_course($course_id, $user_id, $que_id) {
    global $db;
    $query = "SELECT * FROM `user_take_courses` WHERE `user_id` = '$user_id' AND `course_id` = '$course_id' AND `que_id` = '$que_id'";
    $result = $db->query_fetch_full_result($query);
    return $result[0];
  }

  public static function get_correct_answer($group_id, $que_id) {
    global $db;
    $query = "SELECT `correct_answer` FROM `questions` WHERE `group_id` = '$group_id' AND `que_id` = '$que_id'";
    $result = $db->query_fetch_full_result($query);
    return $result[0]['correct_answer'];
  }

  public static function insert_user_answer($user_id, $group_id, $que_id, $answer) {
    global $db;
    $date = date('Y-m-d H:i:s');
    $correct_answer = self::get_correct_answer($group_id, $que_id);
    if ($correct_answer == $answer)
      $points = 5;
    else
      $points = 0;

    // check wheather user already completed stage...
    $query = "SELECT * FROM `user_answers` WHERE `user_id` = '$user_id' AND `group_id` = '$group_id' AND `que_id` = '$que_id'";
    $result = $db->query_fetch_full_result($query);
    if ($result) {
      $update_id = $result[0]['id'];
      // if the answer has changed then update the score else exit
      if ($result[0]['submitted_answer'] != $answer) {
        $attempt_no = $result[0]['attempts'] + 1;
        $points = ($attempt_no < 5) ? (5 - $attempt_no) : 0;

        $query = "UPDATE `user_answers` SET `submitted_answer` = '$answer', `points` = '$points', `date` = '$date', attempts = '$attempt_no' WHERE `id` = '$update_id'";
      }
    } else {
      $query = "INSERT INTO `user_answers`(`user_id`, `group_id`, `que_id`, `submitted_answer`, `points`, `date`) VALUES('$user_id', '$group_id', '$que_id', '$answer', '$points', '$date')";
    }
    $result = $db->query($query);
    return $result;
  }

  public static function total_group_questions($group_id) {
    global $db;
    $query = "SELECT MIN(`que_id`) MIN_QUE_ID, MAX(`que_id`) MAX_QUE_ID FROM `questions` WHERE `group_id` = '$group_id'";
    $result = $db->query_fetch_full_result($query);
    return $result[0];
  }

  public static function check_user_test() {
    global $db;
    $query = "";
  }

  public static function get_cleared_test($user_id, $group_id = FALSE) {
    global $db;
    if ($group_id)
      $group = " AND `group_id` = '$group_id'";
    else
      $group = '';
    $query = "SELECT group_id, que_id FROM `user_answers` WHERE `user_id` = '$user_id' $group ORDER BY `id` DESC LIMIT 0, 1";
    //echo $query;
    $result = $db->query_fetch_full_result($query);
    if ($result) {
      return $result[0];
    } else {
      return FALSE;
    }
  }

  public static function get_completed_stages($user_id) {
    global $db;
    $query = "SELECT DISTINCT(`group_id`) FROM `user_answers` WHERE user_id = '$user_id'";
    $result = $db->query_fetch_full_result($query);
    $stages = array();
    foreach ($result as $s)
      $stages[] = $s['group_id'];
    return $stages;
  }

  public static function get_groups_list() {
    global $db;
    $query = "SELECT DISTINCT(`group_id`) group_id FROM `questions`";
    $result = $db->query_fetch_full_result($query);
    return $result;
  }

  public static function get_groups() {
    global $db;
    $query = "SELECT MIN(`group_id`) MIN_GROUP_ID, MAX(`group_id`) MAX_GROUP_ID FROM `questions`";
    $result = $db->query_fetch_full_result($query);
    return $result[0];
  }

  public static function get_top_10_users() {
    global $db;
    $query = "SELECT SUM(`points`) total_points, (SELECT `user_name` FROM `quiz_users` u WHERE `u`.`id` = `ua`.`user_id`) user_name, user_id FROM `user_answers` ua GROUP BY `user_id` ORDER BY total_points DESC LIMIT 0, 10";
    $result = $db->query_fetch_full_result($query);
    return $result;
  }

  function get_all_courses() {
    global $db;
    $query = "SELECT * FROM `courses` WHERE `active` = '1' ORDER BY `course_id` ASC";
    $result = $db->query_fetch_full_result($query);
    return $result;
  }

  function get_single_course() {
    global $db;
    $query = "SELECT `course_id` FROM `courses` LIMIT 0,1";
    $result = $db->query_fetch_full_result($query);
    return $result[0]['course_id'];
  }

  function get_course_by_id($id) {
    global $db;
    $query = "SELECT * FROM `courses` WHERE `course_id` LIKE '$id'";
    $result = $db->query_fetch_full_result($query);
    return $result[0];
  }

  function get_courses_question_by_course_id($id) {
    global $db;
    $query = "SELECT * FROM `courses_question` WHERE `course_id` LIKE '$id'";
    $result = $db->query_fetch_full_result($query);
    return $result;
  }

  function get_cleared_course($user_id, $course_id = FALSE) {
    global $db;
    if ($course_id)
      $course = " AND `course_id` = '$course_id'";
    else
      $course = '';

    $query = "SELECT course_id, COUNT(que_id) que_id FROM `user_take_courses` WHERE `user_id` = '$user_id' $course ORDER BY `id` DESC LIMIT 0, 1";
    //echo $query;
    $result = $db->query_fetch_full_result($query);
    if ($result) {
      return $result[0];
    } else {
      return FALSE;
    }
  }

  function total_course_questions($course_id) {
    global $db;
    $query = "SELECT COUNT(`id`) MAX_QUE_ID FROM `courses_question` WHERE `course_id` = '$course_id'";
    $result = $db->query_fetch_full_result($query);
    return $result[0];
  }
  
  function total_course_questions_old($course_id) {
    global $db;
    $query = "SELECT MIN(`id`) MIN_QUE_ID, MAX(`id`) MAX_QUE_ID FROM `courses_question` WHERE `course_id` = '$course_id'";
    $result = $db->query_fetch_full_result($query);
    return $result[0];
  }

  function get_course_questions($last_course_id, $last_ques_id) {
    global $db;
    
    $query = "SELECT * FROM `courses_question` WHERE `course_id` = '$last_course_id' AND `id` > '$last_ques_id' ORDER BY `id` ASC LIMIT 0,1";
    //echo $query;
    $result = $db->query_fetch_full_result($query);

    if ($result) {
      return $result[0];
    } else {
      return FALSE;
    }
  }

  function insert_user_courses_ques_answer($user_id, $course_id, $que_id, $answer) {
    global $db;
    $date = date('Y-m-d');
    $correct_answer = self::get_questions_correct_answer($course_id, $que_id);
    $right_answers = unserialize($correct_answer['correct_answer']);
    $correct = 0;
    $answer = htmlentities($answer);
    
    $question_type = $correct_answer['question_type'];
    if($question_type == QUIZ_MATCH_THE_FOLLOWING){
      ($answer == '1') ? $correct = 1 : '';
    }else {
      foreach ($right_answers as $a){
        (strtolower(trim($answer)) == strtolower(trim($a))) ? $correct = 1 : '';
      }
    }
    
    $rslt = Quiz::pre_fill_user_answers_for_course($course_id, $user_id, $que_id);
    if($rslt){
      $attempt_no = $rslt['attempts'] + 1;
    }else {
      $attempt_no = 1;
    }
    
    $query = "INSERT INTO `user_take_courses`(`user_id`, `course_id`, `que_id`, `submitted_answer`, `correct`, `date`, `attempts`) VALUES('$user_id', '$course_id', '$que_id', '$answer', '$correct', '$date', '$attempt_no') ON DUPLICATE KEY UPDATE `submitted_answer` = '$answer', `correct` = '$correct', `date` = '$date', `attempts` = '$attempt_no'";
    $result = $db->query($query);
    return $result;
  }

  function get_questions_correct_answer($course_id, $que_id) {
    global $db;
    $query = "SELECT `question_type`,`correct_answer` FROM `courses_question` WHERE `course_id` = '$course_id' AND `id` = '$que_id'";
    
    $result = $db->query_fetch_full_result($query);
    return $result[0];
  }
  
  function insert_log_courses($user_its, $course_id, $score) {
    global $db;
    $timestamp = date('Y-m-d');
    $query = "INSERT INTO `tlb_log_courses`(`user_its`, `course_id`, `score`, `timestamp`) VALUES('$user_its', '$course_id', '$score', '$timestamp')";
    $result = $db->query($query);
    return $result;
  }
  
  function get_log_courses($user_its, $course_id) {
    global $db;
    $query = "SELECT * FROM `tlb_log_courses` WHERE `user_its` = '$user_its' AND `course_id` = '$course_id' ORDER BY `id` ASC";
    $result = $db->query_fetch_full_result($query);
    return $result;
  }

}

?>
