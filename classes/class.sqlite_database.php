<?php

require_once("constants.php");

class SQLiteDB {

  private $db;
  private $result = array();
  private $enum = 1;
  private $err = array();

  /* Class constructor */

  function SQLiteDB() {
    /* Make connection to database */
    $this->db = new SQLite3;
    $this->db->open(DB_API_NAME);
  }

  function __destruct() {
    $this->db->close();
  }

  /**
   * query - Performs the given query on the database and
   * returns the result, which may be false, true or a
   * resource identifier.
   */
  function query($query) {

    $cmd = strtoupper(substr($query, 0, strpos($query, ' ')));

    switch ($cmd) {
      case 'INSERT':
      case 'UPDATE':
      case 'DELETE':
      case 'CREATE':
      case 'ALTER':
        $ret = $this->db->exec($query);
        if (!$ret) {
          $this->err[$this->enum] = $this->db->lastErrorMsg();
        }
        return $ret;
        break;
      case 'SELECT':
        $this->result[$this->enum] = $this->db->query($query);
        if ($this->result[$this->enum]) {
          $this->enum++;
          return $this->enum - 1;
        } else {
          //echo $query;
          $this->err[$this->enum] = $this->db->lastErrorMsg();
          unset($this->result[$this->enum]);
          return FALSE;
        }
        break;
    }
  }

  function query_fetch_full_result($query) {

    $res = $this->db->query($query) or die($this->db->lastErrorMsg());
    $rows = array();

    while ($row = $res->fetchArray(SQLITE3_ASSOC)) {
      $rows[] = $row;
    }

    return $rows;
  }

  function fetch_array($enum) {
    if($this->result[$enum]){
      $row = $this->result[$enum]->fetchArray(SQLITE3_ASSOC);
    } else {
      $row = FALSE;
    }
  }

  function row_count($enum) {
    if($this->result[$enum]){
      return $this->result[$enum]->sqlite_num_rows();
    } else {
      return FALSE;
    }
  }

  function sanitize($data) {
    $data = stripcslashes(strip_tags($data));
    $result = $this->db->escapeString($data);
    return $result;
  }

  function free_result($enum) {
    if(isset($this->result[$enum]))unset($this->result[$enum]);
    if(isset($this->result[$enum]))unset($this->err[$enum]);
    return TRUE;
  }

  function clean_data($input) {
    if (is_array($input)) {
      foreach ($input as &$value) {
        $value = $this->clean_data($value);
      }
    } else {
      $input = $this->sanitize($input);
    }
    return $input;
  }

  function get_last_err() {
    return $this->err[$this->enum];
  }

  function get_last_insert_id() {
    return $this->db->sqlite_last_insert_rowid($this->db);
  }

};

/* Create database connection */
$db = new SQLiteDB;
?>