<?php

require_once 'class.database.php';

function get_ary_countries() {
  global $db;

  $query = "SELECT nicename, top_list FROM country ORDER BY top_list DESC, nicename";

  $result = $db->query_fetch_full_result($query);

  return $result;
}

function get_states($country) {
  global $db;

  $query = "SELECT `name`, `top_list` FROM `states` WHERE country_iso3 LIKE (SELECT iso3 FROM country WHERE nicename LIKE '$country') ORDER BY top_list DESC, `name`";

  $states = $db->query_fetch_full_result($query);

  if (is_array($states)) {
    foreach ($states as $state) {
      if ($state['top_list'] == 1) {
        $preferred[] = array('i' => $state['name'], 'v' => $state['name']);
      } else {
        $rest[] = array('i' => $state['name'], 'v' => $state['name']);
      }
    }
    $data = array_merge($preferred, $rest);
    echo json_encode($data);
  } else {
    echo FALSE;
  }
}

function log_text($txt) {
  if (!ENABLE_WRITE_LOG)
    return TRUE;
  $fh = fopen('my_log.txt', 'a');

  fwrite($fh, $txt . "\n\n");

  fclose($fh);

  unset($fh);
}

$query = '';
switch ($query) {
  case 'getCities':
    $state = (int) $data['state'];
    $cities = $global->select_data('city', $state, 'state_id');
    if (is_array($cities)) {
      foreach ($cities as $city) {
        if ($city['top_list'] == 1) {
          $preferred[] = array('i' => $city['id'], 'v' => $city['name']);
        } else {
          $rest[] = array('i' => $city['id'], 'v' => $city['name']);
        }
      }
      $preferred[] = array('i' => '', 'v' => '');
      $data = array_merge($preferred, $rest);
      echo json_encode($data);
    } else {
      echo FALSE;
    }
    break;
}

function get_marhala_slug($marhala_id) {
  switch ($marhala_id) {
    case 1:
      return "pre-primary";

      break;
    case 2:
      return "primary";

      break;
    case 3:
      return "std5to7";

      break;
    case 4:
      return "std8to10";

      break;
    case 5:
      return "std11to12";

      break;
    case 6:
      return "graduate";

      break;
    case 7:
      return "post-graduation";

      break;
    case 8:
      return "diploma";

      break;
  }
}

function get_marhala_ques_preffered($marhala_id) {
  global $db;
  $query = "SELECT * FROM `tlb_araz_questionaire` WHERE `grpID` LIKE '$marhala_id' AND `preferred` = '1' ORDER BY `id`";
  $ques = $db->query_fetch_full_result($query);
  return $ques;
}

function get_marhala_ques($marhala_id) {
  global $db;
  $query = "SELECT * FROM `tlb_araz_questionaire` WHERE `grpID` LIKE '$marhala_id' AND `preferred` = '0' ORDER BY `id`";
  $ques = $db->query_fetch_full_result($query);
  return $ques;
}

function get_marhala_questions($marhala_id) {
  global $db;
  $query = "SELECT * FROM `tlb_araz_questionaire` WHERE `grpID` LIKE '$marhala_id'";
  $ques = $db->query_fetch_full_result($query);
  return $ques;
}

function get_marhala_question_answers($araz_id) {
  global $db;
  $query = "SELECT * FROM `tlb_araz_questionaire_answers` WHERE `araz_id` LIKE '$araz_id'";
  $result = $db->query_fetch_full_result($query);
  return $result;
}

function delete_marhala_question_answers($araz_id) {
  global $db;
  $query = "DELETE FROM `tlb_araz_questionaire_answers` WHERE `araz_id` LIKE '$araz_id'";
  $result = $db->query($query);
  return $result;
}

function get_marhala_from_araz($araz_id) {
  global $db;
  $query = "SELECT `marhala` FROM `tlb_araz` WHERE `id` LIKE '$araz_id'";
  $result = $db->query_fetch_full_result($query);
  return $result[0]['marhala'];
}

function save_ques_ans_to_db($araz_id, $ques) {
  global $db;
  $ans = unserialize($ques);

  $sql = "INSERT INTO `tlb_araz_questionaire_answers` (`araz_id`,`ques_id`,`answer`) VALUES ";

  if ($ans) {
    foreach ($ans as $q => $a) {
      if ($q != "submit") {
        $my_q = (int) ltrim($q, "q_");
        $sql .= "($araz_id, $my_q, $a),";
      }
    }
  }

  $sql = rtrim($sql, ",");

  $ret = $db->query($sql);
  if ($ret) {
    return TRUE;
  } else {
    return FALSE;
  }
}

function save_course_and_institution_to_db($araz_id, $choose_stream, $count_comp_subject, $group_id, $user_its, $save_institutes, $jawab_given = FALSE) {
  global $db;

  $stream = implode(",", $choose_stream);

  $sql = "INSERT INTO `tlb_araz_courses` (`araz_id`,`stream`,`count_comp_subjects`,`degree_name`,`course_name`,`institute_name`,`institute_country`,`institute_city`,`accomodation`,`course_duration`,`course_started`,`comp_subjects`,`jawab_given`) VALUES ";

  $date = date('Y-m-d');

  $query = "SELECT * FROM `tlb_temp_institute_data` WHERE `marhala` LIKE '$group_id' && `login_its` LIKE '$user_its' && `timestamp` = '$date'";

  $institutes = $db->query_fetch_full_result($query);
  if(!$institutes){
    $institutes = $save_institutes;
  }

  foreach ($institutes as $inst) {
    $degree_name = $inst['degree_name'];
    $course_name = $inst['course_name'];
    $institute_name = $inst['inst_name'];
    $institute_country = $inst['inst_country'];
    $institute_city = $inst['inst_city'];
    $accomodation = $inst['accommodation'];
    $course_duration = $inst['duration'];
    $course_started = $inst['course_started'];
    $comp_subject = $inst['comp_subjects'];

    $sql .= "('$araz_id','$stream','$count_comp_subject','$degree_name','$course_name','$institute_name','$institute_country','$institute_city','$accomodation','$course_duration','$course_started','$comp_subject','$jawab_given'),";

    $find_lp_institute = get_lp_institute($institute_name, $group_id);
    if (!$find_lp_institute) {
      $insert_lp_institute = insert_lp_institute($institute_name, $group_id, $institute_city);
    }
  }

  $sql = rtrim($sql, ",");

  $ret = $db->query($sql);

  if ($ret) {
    foreach ($institutes as $inst) {
      $inst_id = $inst['id'];
      $q = "DELETE FROM `tlb_temp_institute_data` WHERE `id` = '$inst_id'";
      $res = $db->query($q);
    }
  } else {
    return FALSE;
  }
}

function get_courses_by_stream($table, $stream, $category) {
  global $db;

  if (is_array($stream)) {
    foreach ($stream as $val) {
      if ($val != '') {
        $search_query[] = " `stream` = '$val' ";
      }
    }
    $search_query = implode(' OR ', $search_query);
  } else {
    $search_query = " `stream` = '$stream' ";
  }

  $query = "SELECT * FROM `" . $table . "` WHERE `category` = '$category' && $search_query ";

  $result = $db->query_fetch_full_result($query);
  return $result;
}

function get_courses_by_degree($degree, $marhala) {
  global $db;
  $query = "SELECT * FROM `tlb_courses` WHERE `degree` = '$degree' && `marhala_group` = '$marhala'";
  $result = $db->query_fetch_full_result($query);
  return $result;
}

function get_degrees_by_marhala($marhala) {
  global $db;
  $query = "SELECT * FROM `tlb_courses` WHERE `marhala_group` = '$marhala'";
  $result = $db->query_fetch_full_result($query);
  return $result;
}

function get_madrasah_countries() {
  global $db;

  $query = 'SELECT DISTINCT(country) FROM `dawat_madrasah` ORDER BY `country`';

  $countries = $db->query_fetch_full_result($query);

  return $countries;
}

function get_all_countries() {
  global $db;
  $query = 'SELECT DISTINCT(name) FROM `tlb_country` ORDER BY `name` ASC';
  $countries = $db->query_fetch_full_result($query);
  return $countries;
}

function get_country_iso_by_name($name) {
  global $db;
  $query = "SELECT `ISO2` FROM `tlb_country` WHERE `name` = '$name'";
  $country = $db->query_fetch_full_result($query);
  return $country;
}

function get_all_states_by_country($country_id) {
  global $db;
  $query = "SELECT `location_id`,`name` FROM `tlb_country` WHERE `parent_id` = '$country_id' AND `location_type` = '1'";
  $states = $db->query_fetch_full_result($query);
  return $states;
}

function get_all_city_by_country_iso($country_iso) {
  global $db;
  $query = "SELECT `city` FROM `tlb_city` WHERE `country_code` = '$country_iso'";
  $cities = $db->query_fetch_full_result($query);
  return $cities;
}

function get_madrasah_city_by_country($cntry) {
  global $db;

  //$query = "SELECT DISTINCT(city) FROM `dawat_madrasah` WHERE country LIKE '$cntry' ORDER BY `city`";
  $query = "SELECT DISTINCT(city) FROM `dawat_madrasah` ORDER BY `city`";

  $cities = $db->query_fetch_full_result($query);

  return $cities;
}

function get_madrasah_city_by_country_all($country) {
  global $db;

  //$query = "SELECT DISTINCT(city) FROM `dawat_madrasah` WHERE country LIKE '$cntry' ORDER BY `city`";
  $query = "SELECT DISTINCT(city) FROM `dawat_madrasah` WHERE `country` LIKE '$country' ORDER BY `city`";

  $cities = $db->query_fetch_full_result($query);

  return $cities;
}

function get_madrasah_list_by_country($city) {
  global $db;

  //$query = "SELECT madrasah_name_eng FROM `dawat_madrasah` WHERE city LIKE '$city' ORDER BY `madrasah_name_eng`";
  $query = "SELECT madrasah_name_eng FROM `dawat_madrasah` WHERE `city` LIKE '$city' ORDER BY `madrasah_name_eng`";

  $cities = $db->query_fetch_full_result($query);

  return $cities;
}

function get_madrasah_list_by_city($city) {
  global $db;

  //$query = "SELECT madrasah_name_eng FROM `dawat_madrasah` WHERE city LIKE '$city' ORDER BY `madrasah_name_eng`";
  $query = "SELECT madrasah_name_eng FROM `dawat_madrasah` ORDER BY `madrasah_name_eng`";

  $cities = $db->query_fetch_full_result($query);

  return $cities;
}

function get_all_menu() {
  global $db;
  $query = "SELECT * FROM `tlb_menu` WHERE `level` = '0' AND `active` = '1' ORDER BY `index` ASC";
  $result = $db->query_fetch_full_result($query);
  return $result;
}

function get_all_submenu($parent_menu) {
  global $db;
  $query = "SELECT * FROM `tlb_menu` WHERE `level` = '$parent_menu' AND `active` = '1' ORDER BY `index` ASC";
  $result = $db->query_fetch_full_result($query);
  return $result;
}

function get_all_third_step_menu($parent_menu) {
  global $db;
  $query = "SELECT * FROM `tlb_menu` WHERE level = '$parent_menu' AND `active` = '1' ORDER BY `index` ASC";
  $result = $db->query_fetch_full_result($query);
  return $result;
}

function get_all_article() {
  global $db;
  $query = "SELECT * FROM `article`";

  $result = $db->query_fetch_full_result($query);
  return $result;
}

function get_value_by_name($table, $name, $clause = FALSE) {
  global $db;
  $query = "SELECT * FROM `$table` WHERE name = '$name'";

  $query .= ($clause) ? $clause : '';
  $result = $db->query_fetch_full_result($query);
  return $result;
}

function get_carousal_images_by_category($category) {
  global $db;
  $query = "SELECT * FROM `general_setting_for_carousal` WHERE page = '$category'";
  $result = $db->query_fetch_full_result($query);
  return $result;
}

function get_article_by_submenu($table, $submenu, $category = FALSE) {
  global $db;
  $query = "SELECT * FROM `$table` WHERE submenu = '$submenu'";

  if ($category) {
    $query .= " && `category` = '$category'";
  }
  $result = $db->query_fetch_full_result($query);
  return $result;
}

function get_article_by_mainmenu($table, $menu, $category = FALSE) {
  global $db;
  $query = "SELECT * FROM `$table` WHERE menu = '$menu'";

  if ($category) {
    $query .= " && `category` = '$category'";
  }
  echo $query;
  $result = $db->query_fetch_full_result($query);
  return $result;
}

function get_category_of_article_by_submenu($table, $submenu) {
  global $db;
  $query = "SELECT * FROM `$table` WHERE submenu = '$submenu' GROUP BY `category`";
  $result = $db->query_fetch_full_result($query);
  return $result;
}

function get_first_image_from_article($art_id) {
  global $db;
  $query = "SELECT * FROM image WHERE arti_id LIKE '$art_id' ORDER BY id DESC LIMIT 0,1";

  $result = $db->query_fetch_full_result($query);
  return $result[0];
}

function get_first_image_from_event($event_id) {
  global $db;
  $query = "SELECT * FROM event_image WHERE event_id LIKE '$event_id' ORDER BY id DESC LIMIT 0,1";

  $result = $db->query_fetch_full_result($query);
  return $result[0];
}

function get_article_by_id($table, $id) {
  global $db;
  $query = "SELECT * FROM " . $table . " WHERE id = '" . $id . "'";

  $result = $db->query_fetch_full_result($query);
  return $result[0];
}

function get_media_by_id($table, $id) {
  global $db;
  $query = "SELECT * FROM " . $table . " where arti_id='" . $id . "' ";

  $result = $db->query_fetch_full_result($query);
  return $result;
}

function get_event_media_by_id($table, $id) {
  global $db;
  $query = "SELECT * FROM " . $table . " where event_id='" . $id . "' ";

  $result = $db->query_fetch_full_result($query);
  return $result;
}

function get_event_register($id,$its) {
  global $db;
  $query = "SELECT * FROM `event_register` where event_id = '$id' && `its` = '$its' && `cancel` = '0'";
  $result = $db->query_fetch_full_result($query);
  return $result;
}

function count_all_event_register($id) {
  global $db;
  $query = "SELECT COUNT(DISTINCT id) as count FROM `event_register` where event_id = '$id'";
  $result = $db->query_fetch_full_result($query);
  return $result[0]['count'];
}

function insert_temp_session_data($user_its, $data) {
  global $db;

  $sql = "INSERT INTO `tlb_temp_session_data` (`its`, `session_data`, `coun_reply`) VALUES ( '$user_its', '$data', '0')";

  $result = $db->query($sql);

  if ($result) {
    return TRUE;
  } else {
    return FALSE;
  }
}

function insert_temp_counselor_data($user_its, $data) {
  global $db;

  $sql = "INSERT INTO `tlb_temp_counselor_data` (`its`, `counselor_data`, `coun_reply`) VALUES ( '$user_its', '$data', '0')";

  $result = $db->query($sql);

  if ($result) {
    return TRUE;
  } else {
    return FALSE;
  }
}

function save_tlb_araz($user_its, $marhala, $araz_data) {
  global $db;
  
  $date = date('Y-m-d');
  $sql = "INSERT INTO `tlb_temp_araz_stored` (`its`, `marhala`, `araz_data`, `timestamp`) VALUES ( '$user_its', '$marhala', '$araz_data', '$date')";
  
  $result = $db->query($sql);

  if ($result) {
    return TRUE;
  } else {
    return FALSE;
  }
}

function insert_temp_institute_data($group_id, $user_its, $degree_name, $course_name, $institute_name, $institute_country, $institute_city, $accomodation, $course_duration, $course_started, $com_subjects) {
  global $db;
  $date = date('Y-m-d');
  $comp_subject = FALSE;
  if ($com_subjects) {
    $comp_subject = implode(",", $com_subjects);
  }

  $sql = "INSERT INTO `tlb_temp_institute_data` (`marhala`, `login_its`, `degree_name`, `course_name`, `inst_name`, `inst_country`, `inst_city`, `accommodation`, `duration`, `course_started`, `comp_subjects`, `timestamp`) VALUES ( '$group_id', '$user_its', '$degree_name', '$course_name', '$institute_name', '$institute_country', '$institute_city', '$accomodation', '$course_duration', '$course_started', '$comp_subject', '$date')";

  $result = $db->query($sql);

  if ($result) {
    return TRUE;
  } else {
    return FALSE;
  }
}

function check_pending_araz($its) {
  global $db;
  $query = "SELECT COUNT(*) count from tlb_araz WHERE login_its LIKE '$its' AND jawab_given <> 1";
  $result = $db->query_fetch_full_result($query);
  return $result[0]['count'];
}

function get_pending_araz_data($its) {
  global $db;
  $query = "SELECT `id` from tlb_araz WHERE login_its LIKE '$its' AND jawab_given <> 1 ORDER BY `id` DESC";
  $result = $db->query_fetch_full_result($query);
  return $result[0]['id'];
}

function get_all_jawab_sent_araz_by_its($its, $marhala) {
  global $db;
  $query = "SELECT * FROM `tlb_araz` WHERE `login_its` = '$its' AND `jawab_given` = '1' AND `code` != '' AND `email_sent_student` = '1'";
  
  ($marhala) ? $query .= " AND `marhala` < '$marhala'" : '';
  
  $result = $db->query_fetch_full_result($query);
  return $result;
}

function check_save_araz_data($user_its) {
  global $db;
  $query = "SELECT * FROM `tlb_temp_araz_stored` WHERE its LIKE '$user_its'";
  $result = $db->query_fetch_full_result($query);
  return $result[0];
}

function check_member_already_in_temp($user_its) {
  global $db;
  $query = "SELECT * FROM `tlb_temp_session_data` WHERE its LIKE '$user_its'";
  $result = $db->query_fetch_full_result($query);

  if ($result) {
    return TRUE;
  } else {
    return FALSE;
  }
}

function check_member_already_in_coun_data($user_its) {
  global $db;
  $query = "SELECT * FROM `tlb_temp_counselor_data` WHERE its LIKE '$user_its' AND coun_reply LIKE '0'";
  $result = $db->query_fetch_full_result($query);

  if ($result) {
    return TRUE;
  } else {
    return FALSE;
  }
}

function delete_member_by_user_its($user_its) {
  global $db;
  $query = "DELETE FROM `tlb_temp_session_data` WHERE its LIKE '$user_its'";
  $result = $db->query($query);

  if ($result) {
    return TRUE;
  } else {
    return FALSE;
  }
}

function delete_single_temp_institute_data($group_id, $user_its, $inst_id) {
  global $db;
  $date = date('Y-m-d');

  $query = "DELETE FROM `tlb_temp_institute_data` WHERE `id` = '$inst_id' && `marhala` LIKE '$group_id' && `login_its` LIKE '$user_its' && `timestamp` = '$date'";

  $result = $db->query($query);

  if ($result) {
    return TRUE;
  } else {
    return FALSE;
  }
}

function delete_save_araz_data($group_id, $user_its) {
  global $db;
  $query = "DELETE FROM `tlb_temp_araz_stored` WHERE `marhala` LIKE '$group_id' && `its` LIKE '$user_its'";
  $result = $db->query($query);

  if ($result) {
    return TRUE;
  } else {
    return FALSE;
  }
}

function get_temp_session_data($user_its) {
  global $db;
  $query = "SELECT * FROM `tlb_temp_session_data` WHERE `its` LIKE '$user_its'";
  $result = $db->query_fetch_full_result($query);
  return $result;
}

function get_temp_institute_data($group_id, $user_its) {
  global $db;
  $date = date('Y-m-d');
  
  $query = "SELECT * FROM `tlb_temp_institute_data` WHERE `marhala` LIKE '$group_id' && `login_its` LIKE '$user_its' && `timestamp` = '$date'";
  $result = $db->query_fetch_full_result($query);
  return $result;
}

function delete_temp_institute_data($user_its) {
  global $db;
  $query = "DELETE FROM `tlb_temp_institute_data` WHERE `login_its` LIKE '$user_its'";
  $result = $db->query($query);

  if ($result) {
    return TRUE;
  } else {
    return FALSE;
  }
}

function rand_string( $length ) {
  $chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
  return substr(str_shuffle($chars),0,$length);
}

function insert_event_register($event_id, $its_id) {
  global $db;
  $date = date('Y-m-d');
  $rand_id = rand_string(8);
  
  $query = "INSERT INTO `event_register`(`event_id`,`its`,`rand_id`,`create_ts`) VALUES ('$event_id','$its_id','$rand_id','$date') ON DUPLICATE KEY UPDATE `cancel`='0', `reason`='', `cancel_ts`=''";
  
  $result = $db->query($query);
  return $result;
}

function cancel_event_register($event_id, $its_id, $reason) {
  global $db;
  $date = date('Y-m-d');
  $query = "UPDATE `event_register` SET `cancel` = '1', `reason` = '$reason', `cancel_ts` = '$date' WHERE `event_id` = '$event_id' && `its` = '$its_id'";

  $result = $db->query($query);
  return $result;
}

function array_sort($array, $on, $order = SORT_ASC) {
  $new_array = array();
  $sortable_array = array();

  if ($array != '') {
    foreach ($array as $k => $v) {
      if (is_array($v)) {
        foreach ($v as $k2 => $v2) {
          if ($k2 == $on) {
            $sortable_array[$k] = $v2;
          }
        }
      } else {
        $sortable_array[$k] = $v;
      }
    }

    switch ($order) {
      case SORT_ASC:
        asort($sortable_array);
        break;
      case SORT_DESC:
        arsort($sortable_array);
        break;
    }

    foreach ($sortable_array as $k => $v) {
      $new_array[$k] = $array[$k];
    }
  }

  return $new_array;
}

function get_araz_data_by_join($user_its) {
  global $db;
  $query = "SELECT `ques_id`,`answer` FROM `tlb_araz_questionaire_answers` WHERE `araz_id` = (SELECT `id` FROM `tlb_araz` WHERE `login_its` LIKE '$user_its' ORDER BY `id` DESC LIMIT 0,1) ORDER BY ques_id";
  $result = $db->query_fetch_full_result($query);
  return $result;
}

function get_notes_by_marhala($marhala_id) {
  global $db;
  $query = "SELECT * FROM `tlb_notes` where `id` = '$marhala_id'";
  $result = $db->query_fetch_full_result($query);
  return $result;
}

function get_my_jawab_sent_araz_by_its($its, $code = FALSE) {
  global $db;
  $query = "SELECT * FROM `tlb_araz` WHERE `login_its` = '$its'";

  if ($code) {
    $query .= "AND `jawab_given` = '1' AND `code` != '' AND `email_sent_student` = '1'";
  }

  $query .= " ORDER BY `id` DESC";

  $result = $db->query_fetch_full_result($query);
  return $result[0];
}

function get_my_araz_data_by_its_and_id($its, $araz_id) {
  global $db;
  $query = "SELECT * FROM `tlb_araz` WHERE `login_its` = '$its' && `id` = '$araz_id'";

  $result = $db->query_fetch_full_result($query);
  return $result[0];
}

function get_my_araz_institutions_data($araz_id) {
  global $db;
  $query = "SELECT * FROM `tlb_araz_courses` WHERE `araz_id` = '$araz_id'";

  $result = $db->query_fetch_full_result($query);
  return $result;
}

function get_my_araz_approved_institutions_data($araz_id) {
  global $db;
  $query = "SELECT * FROM `tlb_araz_courses` WHERE `araz_id` = '$araz_id' AND `jawab_given` = '1'";
  $result = $db->query_fetch_full_result($query);
  return $result[0];
}

function search_institutes($keywords, $marhala) {
  global $db;
  $query = "SELECT * FROM `tlb_lp_institute` WHERE `institute` LIKE '%" . $keywords . "%' && `approved` = '1'";

  $result = $db->query_fetch_full_result($query);
  return $result;
}

function search_schools($keywords) {
  global $db;
  $query = "SELECT * FROM `tlb_araz_schools_list` WHERE `school_name` LIKE '%$keywords%'";
  $result = $db->query_fetch_full_result($query);
  return $result;
}

function search_finalize_courses($keywords) {
  global $db;
  $query = "SELECT * FROM `tlb_finalize_courses` WHERE `course` LIKE '%" . $keywords . "%'";
  $result = $db->query_fetch_full_result($query);
  return $result;
}

function check_finalize_courses_exist($course) {
  global $db;
  $query = "SELECT * FROM `tlb_finalize_courses` WHERE `course` LIKE '$course'";
  $result = $db->query_fetch_full_result($query);
  return $result;
}

function search_courses($keywords) {
  global $db;
  $query = "SELECT * FROM `tlb_courses` WHERE `course_name` LIKE '%" . $keywords . "%'";
  $result = $db->query_fetch_full_result($query);
  return $result;
}

function get_lp_institute($institute, $marhala) {
  global $db;
  $query = "SELECT * FROM `tlb_lp_institute` WHERE `institute` = '$institute' && `marhala` = '$marhala'";

  $result = $db->query_fetch_full_result($query);
  return $result;
}

function get_all_institutes_db() {
  global $db;
  $query = "SELECT * FROM `tlb_institute_db`";
  $result = $db->query_fetch_full_result($query);
  return $result;
}

function get_all_sentences() {
  global $db;
  $query = "SELECT * FROM `tlb_sentences`";
  $result = $db->query_fetch_full_result($query);
  return $result;
}

function get_all_qasaid($alam = FALSE) {
  global $db;
  $query = "SELECT * FROM `tlb_qasaid`";
  if ($alam) {
    $query .= " WHERE `alam`='$alam'";
  }
  $result = $db->query_fetch_full_result($query);
  return $result;
}

function get_all_alam_of_qasaid() {
  global $db;
  $query = "SELECT * FROM `tlb_qasaid` GROUP BY `alam`";
  $result = $db->query_fetch_full_result($query);
  return $result;
}

function get_qasida_by_id($id) {
  global $db;
  $query = "SELECT * FROM `tlb_qasaid` WHERE `id` LIKE '$id'";
  $result = $db->query_fetch_full_result($query);
  return $result[0];
}

function get_qasaid_is_done($qasaid_id) {
  global $db;
  $query = "SELECT * FROM `tlb_menu_log` WHERE `qasaid_id` = '$qasaid_id'";
  $result = $db->query_fetch_full_result($query);
  return $result;
}

function check_user_its_exist_in_log($user_its) {
  global $db;
  $query = "SELECT IFNULL(COUNT(*), 0) TOTAL_USER  FROM `tlb_menu_log` WHERE `its_id` LIKE '$user_its'";
  $result = $db->query_fetch_full_result($query);
  $result = $result[0];
  if ($result['TOTAL_USER'] > 0) {
    return TRUE;
  }
}

function insert_user_its_log($menu_id, $its_id, $qasaid_id) {
  global $db;
  $date = date('Y-m-d');
  $query = "INSERT INTO `tlb_menu_log`(`menu_id`,`its_id`,`qasaid_id`,`timestamp`) VALUES ('$menu_id','$its_id','$qasaid_id','$date')";

  $result = $db->query($query);
  return $result;
}

function insert_qasaid_log($qasaid_id, $its_id, $slider_id, $slider_value) {
  global $db;
  $date = date('Y-m-d');
  $query = "INSERT INTO `tlb_log_qasaid`(`its_id`,`qasaid_id`,`slider_id`,`slider_value`,`timestamp`) VALUES ('$its_id','$qasaid_id','$slider_id','$slider_value','$date') ON DUPLICATE KEY UPDATE `slider_value` = '$slider_value', `timestamp` = '$date'";

  $result = $db->query($query);
  return $result;
}

function get_log_qasaid_slider_value($its_id, $qasaid_id, $slider_id) {
  global $db;
  $query = "SELECT `slider_value` FROM `tlb_log_qasaid` WHERE `its_id` = '$its_id' && `qasaid_id` = '$qasaid_id' && `slider_id` = '$slider_id'";
  $result = $db->query_fetch_full_result($query);
  return $result[0]['slider_value'];
}

function get_sub_menu_by_menu($menu_id, $menu_slug) {
  global $db;
  $query = "SELECT `id` FROM `tlb_menu` WHERE `index` = '$menu_id' && `slug` = '$menu_slug'";
  $result = $db->query_fetch_full_result($query);
  return $result[0];
}

function count_all_sentences() {
  global $db;
  $query = "SELECT COUNT(*) as count FROM `tlb_sentences`";
  $result = $db->query_fetch_full_result($query);
  return $result[0]['count'];
}

function get_single_sentences($id) {
  global $db;
  $query = "SELECT * FROM `tlb_sentences` WHERE `id` = '$id'";
  $result = $db->query_fetch_full_result($query);
  return $result[0];
}

function get_id_from_form_processes($form_id) {
  global $db;
  $query = "SELECT `return_id` FROM `forms_processed` where `form_id` LIKE '$form_id'";
  $result = $db->query_fetch_full_result($query);
  return $result;
}

function insert_lp_institute($institute, $marhala, $city) {
  global $db;
  $query = "INSERT INTO `tlb_lp_institute`(`institute`,`marhala`,`city`,`approved`) VALUES ('$institute','$marhala','$city','0')";

  $result = $db->query($query);
  return $result;
}

function insert_queries($its, $f_ques, $f_ans) {
  global $db;
  $timestamp = date('Y-m-d');
  $query = "INSERT INTO `tlb_queries`(`its`,`ques_id`,`ans_text`,`timestamp`) VALUES ('$its','$f_ques','$f_ans','$timestamp')";

  $result = $db->query($query);
  return $result;
}

function get_all_miqaat_istibsaar($year) {
  global $db;
  $query = "SELECT * FROM `miqaat_istibsaar` WHERE year = '$year'";
  $result = $db->query_fetch_full_result($query);
  return $result;
}

function get_single_miqaat_istibsaar($miqaat_id) {
  global $db;
  $query = "SELECT * FROM `miqaat_istibsaar` WHERE `id` = '$miqaat_id'";
  $result = $db->query_fetch_full_result($query);
  return $result[0];
}

function get_all_miqaat_ohbat_sentences($miqaat_id) {
  global $db;
  $query = "SELECT * FROM `tlb_ohbat_daily_sentences` WHERE `miqaat_id` = '$miqaat_id'";
  $result = $db->query_fetch_full_result($query);
  return $result;
}

function get_all_miqaat_data($table, $miqaat_id) {
  global $db;
  $query = "SELECT * FROM `$table` WHERE `miqaat_id` = '$miqaat_id'";
  $result = $db->query_fetch_full_result($query);
  return $result;
}

function get_all_mawaqeet_by_year($year) {
  global $db;
  $query = "SELECT * FROM `miqaat_istibsaar` WHERE `year` = '$year'";
  $result = $db->query_fetch_full_result($query);
  return $result;
}

function get_all_miqaat_files($table, $miqaat_id) {
  global $db;
  $query = "SELECT * FROM `$table` WHERE `miqaat_istibsaar_id` = '$miqaat_id' ORDER BY sr_no ASC";
  $result = $db->query_fetch_full_result($query);
  return $result;
}

function get_event_time_in_miqaat_files($miqaat_id) {
  global $db;
  $query = "SELECT * FROM `miqaat_video` mv JOIN `miqaat_audio` mu ON mv.`miqaat_istibsaar_id` = mu.`miqaat_istibsaar_id` JOIN `miqaat_stationary` ms ON mv.`miqaat_istibsaar_id` = ms.`miqaat_istibsaar_id` WHERE mv.`miqaat_istibsaar_id` = '$miqaat_id'";
  $result = $db->query_fetch_full_result($query);
  return $result;
}

function insert_tasavvuraat_araz($its,$file_name,$text) {
  global $db;
  $timestamp = date('Y-m-d');
  $query = "INSERT INTO `tlb_tasavvuraat_araz`(`its_id`,`audio_file`,`text`,`create_ts`) VALUES ('$its','$file_name','$text','$timestamp')";

  $result = $db->query($query);
  return $result;
}

function get_araz_done_tasavvuraat_araz() {
  global $db;
  $query = "SELECT * FROM `tlb_tasavvuraat_araz` WHERE `araz_done` = '1'";
  $result = $db->query_fetch_full_result($query);
  return $result;
}

function ageCalculator($dob) {
    if (!empty($dob)) {
      $birthdate = new DateTime($dob);
      $today = new DateTime('today');
      $age = $birthdate->diff($today)->y;
      return $age;
    } else {
      return 0;
    }
  }

function get_hifz_table() {
  $hifz_sanad = (isset($_SESSION['hifz_sanad']) && $_SESSION['hifz_sanad'] != '') ? $_SESSION['hifz_sanad'] : 'NA';

  return '<table class="table1 table table-responsive">
    <tr class="first-child">
      <td class="first-child">Hifz al Quran</td>
    </tr>
    <tr>
      <td>' . $hifz_sanad . '</td>
    </tr>
  </table>';
}

function get_madrasah_table() {
  $madrasah_name = (isset($_SESSION['madrasah_name']) && $_SESSION['madrasah_name'] != '') ? $_SESSION['madrasah_name'] : 'NA';
  $madrasah_darajah = (isset($_SESSION['madrasah_darajah']) && $_SESSION['madrasah_darajah'] != '') ? $_SESSION['madrasah_darajah'] : 'NA';

  return '<table class="table1 table table-responsive">
    <tr class="first-child">
      <td class="first-child">Madrasah name</td>
      <td>Darajah</td>
    </tr>
    <tr>
      <td>' . $madrasah_name . '</td>
      <td>' . $madrasah_darajah . '</td>
    </tr>
  </table>';
}

function get_school_table() {
  $school_name = (isset($_SESSION['school_name']) && $_SESSION['school_name'] != '') ? $_SESSION['school_name'] : 'NA';
  $school_city = (isset($_SESSION['school_city']) && $_SESSION['school_city'] != '') ? $_SESSION['school_city'] : 'NA';
  $school_standard = (isset($_SESSION['school_standard']) && $_SESSION['school_standard'] != '') ? $_SESSION['school_standard'] : 'NA';

  return '<table class="table1 table table-responsive">
    <tr class="first-child">
      <td class="first-child">Institution name</td>
      <td>Place</td>
      <td>Standard</td>
    </tr>
    <tr>

      <td>' . $school_name . '</td>
      <td>' . $school_city . '</td>
      <td>' . $school_standard . '</td>
    </tr>
  </table>';
}

function get_deeni_taalim_table() {
  $madrasah_darajah = (isset($_SESSION['madrasah_darajah']) && $_SESSION['madrasah_darajah'] != '') ? $_SESSION['madrasah_darajah'] : 'NA';

  return '<table class="table1 table table-responsive">
    <tr class="first-child">
      <td class="first-child">Deeni Taalim</td>
    </tr>
    <tr>
      <td>' . $madrasah_darajah . '</td>
    </tr>
  </table>';
}

function get_institute_table($institute_data) {

  $ret = '<table class="table1 table table-responsive">
      <tr class="first-child">
        <td class="first-child">Institute name</td>
        <td>Place</td>
        <td>Degree / Course</td>
        <td>Accomodation</td>
        <td>Duration</td>
        <td>Course Start Date</td>
      </tr>';

  if($institute_data){
    foreach ($institute_data as $inst) {
      $ret .= '<tr>
        <td>' . $inst['inst_name'] . '</td>
        <td>' . $inst['inst_city'] . '</td>
        <td>' . $inst['course_name'] . '</td>
        <td>' . $inst['accommodation'] . '</td>
        <td>' . $inst['duration'] . '</td>
        <td>' . $inst['course_started'] . '</td>
      </tr>';
    }
  }

  return $ret . '</table>';
}

function get_standards_by_marhala($marhala_id) {
  switch ($marhala_id) {
    case 1:
      return array('Pre-school', 'Nursery', 'Kindergarten', 'Junior Kindergarten', 'Senior Kindergarten');
    case 2:
      return array('1st', '2nd', '3rd', '4th');
    case 3:
      return array('5th', '6th', '7th');
    case 4:
      return array('8th', '9th', '10th');
    case 5:
      return array('11th', '12th');
    default:
      return array();
  }
}

?>
