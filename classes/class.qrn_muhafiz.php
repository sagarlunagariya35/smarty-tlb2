<?php

require_once 'class.database.php';

class Qrn_Muhafiz {

  function insert_muhafiz($its, $doj, $show_email, $show_mobile, $prefered_time, $prefered_person_count, $current_sanad_id) {
    global $db;

    $query = "INSERT INTO `qrn_muhafiz`(`its_id`, `doj`, `show_email`, `show_mobile`, `prefered_time`, `prefered_person_count`, `current_sanad_id`) VALUES ('$its','$doj','$show_email','$show_mobile', '$prefered_time', '$prefered_person_count', '$current_sanad_id')";

    $result = $db->query($query);
    return $result;
  }

  function update_muhafiz($its, $doj, $show_email, $show_mobile, $prefered_time, $prefered_person_count, $current_sanad_id) {
    global $db;
    $update_query = "UPDATE `qrn_muhafiz` SET `doj`='$doj', `show_email`='$show_email', `show_mobile`='$show_mobile', `prefered_time`='$prefered_time', `prefered_person_count`='$prefered_person_count', `current_sanad_id`='$current_sanad_id' WHERE its_id = '$its' ";

    $result = $db->query($update_query);
    return $result;
  }
  
  function get_all_muhafiz($its = FALSE) {
    global $db;
    $query = "SELECT * FROM `qrn_muhafiz`";
    
    ($its) ? $query .= " WHERE `its_id` LIKE '$its'" : "";
    
    $result = $db->query_fetch_full_result($query);
    return $result;
  }
  
  function get_all_muhafiz_view($gender) {
    global $db;
    $query = "SELECT * FROM `view_qrn_muhafiz` WHERE `muhafiz_gender` LIKE '$gender' LIMIT 0,8";
    $result = $db->query_fetch_full_result($query);
    return $result;
  }
  
  function delete_muhafiz($its) {
    global $db;
    $query = "DELETE FROM `qrn_muhafiz` WHERE `its_id` LIKE '$its'";
    $result = $db->query($query);
    return $result;
  }
  
  function sent_request_for_tasmee($its, $muhafiz) {
    global $db;
    $date = date('Y-m-d');

    $query = "INSERT INTO `qrn_tasmee_request`(`mumin_its`, `muhafiz_its`, `request_ts`) VALUES ('$its','$muhafiz','$date')";

    $result = $db->query($query);
    return $result;
  }
  
  function get_pending_request_for_tasmee($its) {
    global $db;
    $query = "SELECT * FROM `qrn_tasmee_request` WHERE `muhafiz_its` LIKE '$its' AND `status` = '0'";
    $result = $db->query_fetch_full_result($query);
    return $result;
  }
  
  function get_all_tasmee_request_of_mumin($its) {
    global $db;
    $query = "SELECT * FROM `qrn_tasmee_request` WHERE `mumin_its` LIKE '$its' AND `status` = '0'";
    $result = $db->query_fetch_full_result($query);
    return $result;
  }
  
  function count_all_tasmee_request_of_mumin($its) {
    global $db;
    $query = "SELECT COUNT(*) as count FROM `qrn_tasmee_request` WHERE `mumin_its` LIKE '$its' AND `status` = '0'";
    $result = $db->query_fetch_full_result($query);
    return $result[0]['count'];
  }
  
  function get_tasmee_request($id) {
    global $db;
    $query = "SELECT * FROM `qrn_tasmee_request` WHERE `id` LIKE '$id'";
    $result = $db->query_fetch_full_result($query);
    return $result[0];
  }
  
  function delete_tasmee_request($id) {
    global $db;
    $query = "DELETE FROM `qrn_tasmee_request` WHERE `id` LIKE '$id'";
    $result = $db->query($query);
    return $result;
  }
  
  function delete_tasmee_request_by_its($its, $field) {
    global $db;
    $query = "DELETE FROM `qrn_tasmee_request` WHERE `$field` LIKE '$its'";
    $result = $db->query($query);
    return $result;
  }
  
  function update_data_of_mumin_by_muhafiz($muhafiz_its) {
    global $db;
    $update_query = "UPDATE `qrn_mumin` SET `muhafiz_id`='', `muhafiz_full_name`='', `muhafiz_jamaat`='', `muhafiz_jamiat`='', `muhafiz_quran_sanad`='', `muhafiz_gender`='' WHERE muhafiz_id = '$muhafiz_its' ";

    $result = $db->query($update_query);
    return $result;
  }
  
  function delete_all_tasmee_request($mumin_its) {
    global $db;
    $query = "DELETE FROM `qrn_tasmee_request` WHERE `mumin_its` LIKE '$mumin_its' AND `status` = '0'";
    $result = $db->query($query);
    return $result;
  }
  
  function accept_tasmee_request($id) {
    global $db;
    $date = date('Y-m-d');
    
    $query = "UPDATE `qrn_tasmee_request` SET `status`='1', `accept_ts`='$date' WHERE `id` LIKE '$id'";
    $result = $db->query($query);
    return $result;
  }
  
  function insert_cron_emails($from, $to, $subject, $message) {
    global $db;
    $query = "INSERT INTO `tlb_cron_emails`(`from_user`, `to_user`, `subject`, `message`) VALUES ('$from','$to','$subject','$message')";
    $result = $db->query($query);
    return $result;
  }

  function get_yesterday_ayat_count($muhafiz_its){
    global $db;
    
    $dt = date('Y-m-d');
    $query = "SELECT COUNT(*) cnt FROM qrn_tasmee_log WHERE muhafiz_its LIKE '$muhafiz_its' AND `timestamp` LIKE '$dt'";
    $result = $db->query_fetch_full_result($query);
    return $result[0]['cnt'];
  }
  
}
