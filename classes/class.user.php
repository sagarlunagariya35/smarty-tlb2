<?php

require_once 'class.database.php';

/**
 * 
 * this will be used in raza araz page. will provide data 
 * and will update data from that page
 * 
 * Description of class
 *
 * @author mustafa
 */
class mtx_user {

  private $user_id;
  private $its_id;
  private $password;
  private $first_prefix;
  private $first_name;
  private $middle_prefix;
  private $middle_name;
  private $last_name;
  private $full_name_ar;
  private $gender;
  private $dob;
  private $nationality;
  private $address;
  private $pincode;
  private $country;
  private $state;
  private $city;
  private $mobile;
  private $telephone;
  private $email;
  private $whatsapp;
  private $viber;
  private $tanzeem_id;
  private $occupation;
  private $qualification;
  private $jamaat;
  private $jamiat;
  private $last_update_ts;
  private $user_type;
  private $prefer_email;
  private $prefer_whatsapp;
  private $prefer_call;
  private $prefer_viber;
  private $mumin_photo = '';
  //TODO add getter setter and update query for insert and update
  private $subscribe;
  private $quran_sanad;

  /**
   * Loads values from database
   * @param type $user_its
   * @return class or false is not found
   */
  public function loaduser($user_its) {
    global $db;
    $query = "SELECT * FROM `tlb_user` WHERE `its_id` LIKE '$user_its'";
    $result = $db->query_fetch_full_result($query);
    $result = $db->clean_data($result[0]);
    if ($result) {
      $this->set_user_id($result['user_id']);
      $this->set_its_id($result['its_id']);
      $this->set_first_prefix($result['first_prefix']);
      $this->set_first_name($result['first_name']);
      $this->set_middle_prefix($result['middle_prefix']);
      $this->set_middle_name($result['middle_name']);
      $this->set_last_name($result['last_name']);
      $this->set_full_name_ar($result['full_name_ar']);
      $this->set_gender($result['gender']);
      $this->set_dob($result['dob']);
      $this->set_nationality($result['nationality']);
      $this->set_address($result['address']);
      $this->set_country($result['country']);
      $this->set_state($result['state']);
      $this->set_city($result['city']);
      $this->set_mobile($result['mobile']);
      $this->set_telephone($result['telephone']);
      $this->set_email($result['email']);
      $this->set_whatsapp($result['whatsapp']);
      $this->set_viber($result['viber']);
      $this->set_user_type($result['user_type']);
      $this->set_prefer_call($result['prefer_call']);
      $this->set_prefer_email($result['prefer_email']);
      $this->set_prefer_whatsapp($result['prefer_whatsapp']);
      $this->set_prefer_viber($result['prefer_viber']);
      $this->set_tanzeem_id($result['tanzeem_id']);
      $this->set_qualification($result['qualification']);
      $this->set_occupation($result['occupation']);
      $this->set_jamaat($result['jamaat']);
      $this->set_jamiat($result['jamiat']);
      $this->set_last_update_ts($result['last_update_ts']);
      @$this->set_subscribe($result['subscribe']);
      $this->set_quran_sanad($result['quran_sanad']);

      return $this;
    } else {
      return FALSE;
    }
  }

  /*
   * 
   */

  public function get_mumeen_detail_from_its($its) {
    global $db;
    $key = 'Talabulilm562138';

    $soapUrl = "http://www.its52.com/eJas/EjamaatServices.asmx";

    $xml_post_string = '<?xml version="1.0" encoding="utf-8"?>
  <soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
  <soap:Body>
  <EduDept_MuminDetail xmlns="http://localhost/eJAS/EjamaatServices">
  <EjamaatId>' . $its . '</EjamaatId>
  <strKey>' . $key . '</strKey>
  </EduDept_MuminDetail>
  </soap:Body>
  </soap:Envelope>';

    $headers = array(
        "Content-type: text/xml;charset=\"utf-8\"",
        "Accept: text/xml",
        "Cache-Control: no-cache",
        "Pragma: no-cache",
        "SOAPAction: http://localhost/eJAS/EjamaatServices/EduDept_MuminDetail",
        "Content-length: " . strlen($xml_post_string)
    );

    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $soapUrl);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_POST, true);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $xml_post_string); // the SOAP request
    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

    $response = curl_exec($ch);
    curl_close($ch);

    //log_text(__FILE__ . '-response text-' . $response);

    if ($response == FALSE) {
      return FALSE;
    }
    $xml = simplexml_load_string($response);

    $mumin_data = $xml->xpath('//Table');
    $mumin_data = $mumin_data[0];

    $this->set_its_id((int) $mumin_data->Mumin_id);
    $this->set_first_prefix((string) $mumin_data->prefix);
    $this->set_first_name((string) $mumin_data->First_name);
    $this->set_middle_prefix((string) $mumin_data->Middle_Prefix);
    $this->set_middle_name((string) $mumin_data->Middle_Name);
    $this->set_last_name((string) $mumin_data->Surname);
    $this->set_full_name_ar((string) $mumin_data->arabic_fullname);
    $this->set_gender((string) $mumin_data->gender);
    $this->set_address((string) $db->clean_data($mumin_data->Address));
// TODO mmm: correct the date format to (Y-m-d)
    $this->set_dob((string) $mumin_data->DOB);
    $this->set_mobile((string) $mumin_data->MOBILE_NO);
    $this->set_email((string) $mumin_data->email);
// TODO mmm: create following four vars and their respective getter setter
    $this->set_qualification((string) $db->clean_data($mumin_data->Qualification));
    $this->set_occupation((string) $db->clean_data($mumin_data->Occupation));
    $this->set_jamaat((string) $db->clean_data($mumin_data->Jamaat));
    $this->set_jamiat((string) $mumin_data->Jamiaat);
    $this->set_tanzeem_id((int) $mumin_data->Tanzeem_ID);
    $this->set_nationality((string) $db->clean_data($mumin_data->Nationality));
    $this->set_city((string) $db->clean_data($mumin_data->city));
    $this->set_country((string) $db->clean_data($mumin_data->country));
    $this->set_quran_sanad((string) $mumin_data->QuranSanad);

    return TRUE;
  }

  private function get_mumeen_photo_from_its() {

    if (file_exists($_SERVER['DOCUMENT_ROOT'] . '/mumin_images/' . $this->its_id . '.png')) {
      $this->mumin_photo = 'https://www.talabulilm.com/mumin_images/' . $this->its_id . '.png';
      return TRUE;
    }

    $key = 'Talabulilm562138';

    $soapUrl = "http://www.its52.com/eJas/EjamaatServices.asmx";

    $xml_post_string = '<?xml version="1.0" encoding="utf-8"?>
<soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
  <soap:Body>
    <GetMuminNewPhoto xmlns="http://localhost/eJAS/EjamaatServices">
      <EjamaatId>' . $this->its_id . '</EjamaatId>
      <strKey>' . $key . '</strKey>
    </GetMuminNewPhoto>
  </soap:Body>
</soap:Envelope>';

    $headers = array(
        "Content-type: text/xml;charset=\"utf-8\"",
        "Accept: text/xml",
        "Cache-Control: no-cache",
        "Pragma: no-cache",
        "SOAPAction: http://localhost/eJAS/EjamaatServices/GetMuminNewPhoto",
        "Content-length: " . strlen($xml_post_string)
    );

    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $soapUrl);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_POST, true);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $xml_post_string); // the SOAP request
    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

    $response = curl_exec($ch);
    curl_close($ch);

    //log_text(__FILE__ . '-Get Mumin Photo response text-' . $response);

    if ($response == FALSE) {
      return FALSE;
    }

    $result = preg_match_all('/<GetMuminNewPhotoResult>(.*)<\/GetMuminNewPhotoResult>/imUs', $response, $imgs);

    if (is_array($imgs) && isset($imgs[1][0])) {
      $this->mumin_photo = 'data:image/png;base64,' . $imgs[1][0];

      // save the mumin photo to the local server
      $file_data = str_replace(' ', '+', $imgs[1][0]);
      $file_data = base64_decode($file_data);
      file_put_contents($_SERVER['DOCUMENT_ROOT'] . '/mumin_images/' . $this->its_id . '.png', $file_data);
    }
    return TRUE;
  }

  public function get_ip_details_from_its($its) {

    $key = ITS_KEY;

    $soapUrl = "http://husami.tmjamat.com/txt_service.php";

    $xml_post_string = '<?xml version="1.0" encoding="utf-8"?>
<soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
  <soap:Body>
    <MSB_StudentInfo xmlns="http://husami.tmjamat.com">
      <EjamaatId>' . $its . '</EjamaatId>
      <strKey>' . $key . '</strKey>
    </MSB_StudentInfo>
  </soap:Body>
</soap:Envelope>';

    $headers = array(
        "Content-type: text/xml;charset=\"utf-8\"",
        "Accept: text/xml",
        "Cache-Control: no-cache",
        "Pragma: no-cache",
        "SOAPAction: http://husami.tmjamat.com",
        "Content-length: " . strlen($xml_post_string)
    );

    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $soapUrl);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_POST, true);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $xml_post_string); // the SOAP request
    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

    $response = curl_exec($ch);
    curl_close($ch);

    //log_text(__FILE__ . '-response text-' . $response);

    if ($response == FALSE) {
      return FALSE;
    }

    //$xml = simplexml_load_string($response);
    //$mumin_data = $xml->xpath('//Table');
    //$mumin_data = $mumin_data[0];

    return $response;
  }

  /**
   * This function checks if user exists: calls load
   *  if not then it gets data from ITS52 and adds record to database
   * @param type $user_its
   * @return type true/false - will always return true.
   */
  public function check_user_exist($user_its) {
    global $db;

    $query = "SELECT IFNULL(COUNT(*), 0) TOTAL_USER  FROM `tlb_user` WHERE `its_id` LIKE '$user_its'";
    $result = $db->query_fetch_full_result($query);
    $result = $result[0];
    if ($result['TOTAL_USER'] == 0) {
// Get details from ITS
      if ($this->get_mumeen_detail_from_its($user_its)) {
// Insert new record
        $add = $this->add_to_db();
        if (!$add) {
          return FALSE;
        }
      } else {
        return FALSE;
      }
    } else {
      if ($this->get_mumeen_detail_from_its($user_its)) {
// Update record
        $update = $this->update_user();
        if (!$update) {
          return FALSE;
        }
      } else {
        return FALSE;
      }
    }
    $this->loaduser($user_its);
    return TRUE;
  }

  public function add_to_db() {
    global $db;

//TODO mmm user_type has to be calculated from occupation? 
//only the prefer fields have been left out here.

    $sql = "INSERT INTO `tlb_user` (`its_id`, `first_prefix`, `first_name`, `middle_prefix`, `middle_name`, `last_name`, `full_name_ar`, `gender`, `dob`, `nationality`, `address`, `country`, `state`, `city`, `mobile`, `telephone`, `email` , `tanzeem_id`, `qualification`, `occupation`, `jamaat`, `jamiat`, `last_update_ts`, `password`,`user_type`,`quran_sanad`) VALUES ( '$this->its_id', '$this->first_prefix', '$this->first_name', '$this->middle_prefix', '$this->middle_name', '$this->last_name', '$this->full_name_ar', '$this->gender', '$this->dob', '$this->nationality', '$this->address', '$this->country', '$this->state', '$this->city', '$this->mobile', '$this->telephone', '$this->email', '$this->tanzeem_id', '$this->qualification', '$this->occupation', '$this->jamaat', '$this->jamiat', NOW(), '','$this->user_type','$this->quran_sanad')";

    //log_text(__FILE__ . '-add_to_db-' . $sql);

    $ret = $db->query($sql);
    if ($ret)
      return TRUE;
  }

  public function update_data($data) {
    global $db;
    $data = $db->clean_data($data);
    $updated = FALSE;

    //$sql = "UPDATE tlb_user SET ";
    foreach ($data as $key => $value) {
      switch ($key) {
        case 'prefix':
          $prefix = str_replace('none', '', $value);
          if ($prefix != $this->first_prefix) {
            //$sql = $sql . "$key = '$value' ,";
            $this->set_first_prefix($prefix);
            $updated = TRUE;
          }
          break;
        case 'first_name':
          if ($value != $this->first_name) {
            //$sql = $sql . "$key = '$value' ,";
            $this->set_first_name($value);
            $updated = TRUE;
          }
          break;
        case 'father_prefix':
          $fp_prefix = str_replace('fp_', '', $value);
          $fp_prefix = str_replace('none', '', $fp_prefix);
          if ($fp_prefix != $this->middle_prefix) {
            //$sql = $sql . "$key = '$value' ,";
            $this->set_middle_prefix($fp_prefix);
            $updated = TRUE;
          }
          break;
        case 'father_name':
          if ($value != $this->middle_name) {
            //$sql = $sql . "$key = '$value' ,";
            $this->set_middle_name($value);
            $updated = TRUE;
          }
          break;
        case 'surname':
          if ($value != $this->last_name) {
            //$sql = $sql . "$key = '$value' ,";
            $this->set_last_name($value);
            $updated = TRUE;
          }
          break;
        case 'email':
          if ($value != $this->email) {
            //$sql = $sql . "$key = '$value' ,";
            $this->set_email($value);
            $updated = TRUE;
          }
          break;
        case 'mobile':
          if ($value != $this->mobile) {
            //$sql = $sql . "$key = '$value' ,";
            $this->set_mobile($value);
            $updated = TRUE;
          }
          break;
        case 'whatsapp':
          if ($value != $this->whatsapp) {
            //$sql = $sql . "$key = '$value' ,";
            $this->set_whatsapp($value);
            $updated = TRUE;
          }
          break;
        case 'viber':
          if ($value != $this->viber) {
            //$sql = $sql . "$key = '$value' ,";
            $this->set_viber($value);
            $updated = TRUE;
          }
          break;
        case 'prefer_email':
          if ($value != $this->prefer_email) {
            //$sql = $sql . "$key = '$value' ,";
            $this->set_prefer_email($value);
            $updated = TRUE;
          }
          break;
        case 'prefer_whatsapp':
          if ($value != $this->prefer_whatsapp) {
            $this->set_prefer_whatsapp($value);
            //$sql = $sql . "$key = '$value' ,";
            $updated = TRUE;
          }
          break;
        case 'prefer_call':
          if ($value != $this->prefer_call) {
            //$sql = $sql . "$key = '$value' ,";
            $this->set_prefer_call($value);
            $updated = TRUE;
          }
          break;
        case 'prefer_viber':
          if ($value != $this->prefer_viber) {
            //$sql = $sql . "$key = '$value' ,";
            $this->set_prefer_viber($value);
            $updated = TRUE;
          }
          break;
        case 'subscribe':
          if ($value != $this->subscribe) {
            $sql = $sql . "$key = '$value' ,";
            //$this->set_prefer_call($value);
            $updated = TRUE;
          }
          break;
      }
    }
//checkboxes will not return off 
    if (!isset($data['prefer_email'])) {
      $this->set_prefer_email(0);
      //$sql .= " prefer_email = '0',";
      $updated = TRUE;
    }
    if (!isset($data['prefer_whatsapp'])) {
      $this->set_prefer_whatsapp(0);
      //$sql .= " prefer_whatsapp = '0',";
      $updated = TRUE;
    }
    if (!isset($data['prefer_call'])) {
      $this->set_prefer_call(0);
      //$sql .= " prefer_call = '0',";
      $updated = TRUE;
    }
    if (!isset($data['prefer_viber'])) {
      $this->set_prefer_viber(0);
      //$sql .= " prefer_call = '0',";
      $updated = TRUE;
    }

//remove last coma
    //$sql = rtrim($sql, ",");
    //$sql = "$sql WHERE its_id = '$data[its_id]'";


    if ($updated) {
      //$ret = $db->query($sql);
      $ret = $this->update_user();
      if ($ret) {
        return array('status' => 1);
      } else {
        $error = $db->get_last_err();
        return array('status' => 0, 'error' => $error);
      }
    } else {
      return array('status' => 1);
    }
  }

  public function update_user() {
    global $db;
    //$data = $db->clean_data($data);

    $sql = "UPDATE tlb_user SET `first_prefix`='$this->first_prefix', `first_name`='$this->first_name', `middle_prefix`='$this->middle_prefix', `middle_name`='$this->middle_name', `last_name`='$this->last_name', `full_name_ar`='$this->full_name_ar', `gender`='$this->gender', `dob`='$this->dob', `nationality`='$this->nationality', `address`='$this->address', `pincode`='$this->pincode', `country`='$this->country', `state`='$this->state', `city`='$this->city', `mobile`='$this->mobile', `telephone`='$this->telephone', `email`='$this->email',";

    if ($this->whatsapp != '') {
      $sql .= " `whatsapp`='$this->whatsapp',";
    }

    if ($this->viber != '') {
      $sql .= " `viber`='$this->viber',";
    }

    $sql .= " `user_type`='$this->user_type', `prefer_email`='$this->prefer_email', `prefer_whatsapp`='$this->prefer_whatsapp', `prefer_call`='$this->prefer_call', `prefer_viber`='$this->prefer_viber', `tanzeem_id`='$this->tanzeem_id', `last_update_ts`='$this->last_update_ts', `qualification`='$this->qualification', `occupation`='$this->occupation', `jamaat`='$this->jamaat', `jamiat`='$this->jamiat', `quran_sanad`='$this->quran_sanad' WHERE its_id = '$this->its_id'";


    $ret = $db->query($sql);
    if ($ret) {
      return TRUE;
    } else {
      //log_text(__FILE__ . " update_user " . $db->get_last_err());
      return FALSE;
    }
  }

  public function get_counselor_data() {
    global $db;
    $query = "SELECT *  FROM `tlb_user` WHERE `user_type` = '4'";
    $result = $db->query_fetch_full_result($query);
    return $result;
  }

  public function get_general_setting_data($category) {
    global $db;
    $query = "SELECT `redirect_link` FROM `general_setting` WHERE `category` LIKE '$category'";
    $result = $db->query_fetch_full_result($query);
    return $result[0]['redirect_link'];
  }

  private function get_user_type_by_string($user_type_string) {
    switch (strtolower($user_type_string)) {
      case 'student':
        return 1;
      case 'teacher':
        return 2;
      case 'professional':
        return 3;
      case 'counselor':
        return 4;
      case 'mentor':
        return 5;
      case 'sponsor':
        return 6;
      case 'foster':
        return 7;
    }
  }

  /*
   * GETTERs and SETTERs 
   * --------------------------
   */

  public function get_user_id() {
    return $this->user_id;
  }

  public function get_its_id() {
    return $this->its_id;
  }

  public function get_password() {
    return $this->password;
  }

  public function get_full_name() {
    $name = $this->first_prefix . ' ' . $this->first_name . ' ' . $this->middle_prefix . ' ' . $this->middle_name . ' ' . $this->last_name;
    $name = str_replace('  ', ' ', $name);
    return trim($name);
  }

  public function get_first_prefix() {
    return $this->first_prefix;
  }

  public function get_first_name() {
    return $this->first_name;
  }

  public function get_middle_prefix() {
    return $this->middle_prefix;
  }

  public function get_middle_name() {
    return $this->middle_name;
  }

  public function get_last_name() {
    return $this->last_name;
  }

  public function get_full_name_ar() {
    return $this->full_name_ar;
  }

  public function get_gender() {
    return $this->gender;
  }

  public function get_dob() {
    return $this->dob;
  }

  public function get_nationality() {
    return $this->nationality;
  }

  public function get_address() {
    return $this->address;
  }

  public function get_country() {
    return $this->country;
  }

  public function get_state() {
    return $this->state;
  }

  public function get_city() {
    return $this->city;
  }

  public function get_mobile() {
    return $this->mobile;
  }

  public function get_telephone() {
    return $this->telephone;
  }

  public function get_email() {
    return $this->email;
  }

  public function get_whatsapp() {
    return $this->whatsapp;
  }

  public function get_viber() {
    return $this->viber;
  }

  public function get_tanzeem_id() {
    return $this->tanzeem_id;
  }

  public function get_last_update_ts() {
    return $this->last_update_ts;
  }

  public function get_user_type() {
    switch ($this->user_type) {
      case 1:
        return 'Student';
      case 2:
        return 'Teacher';
      case 3:
        return 'Professional';
      case 4:
        return 'Counselor';
      case 5:
        return 'Mentor';
      case 6:
        return 'Sponsor';
      case 7:
        return 'Foster';
      default :
        return '---';
    }
  }

  public function get_prefer_email() {
    return $this->prefer_email;
  }

  public function get_prefer_whatsapp() {
    return $this->prefer_whatsapp;
  }

  public function get_prefer_call() {
    return $this->prefer_call;
  }

  public function get_prefer_viber() {
    return $this->prefer_viber;
  }

  public function get_occupation() {
    return $this->occupation;
  }

  public function get_qualification() {
    return $this->qualification;
  }

  public function get_jamaat() {
    return $this->jamaat;
  }

  public function get_jamiat() {
    return $this->jamiat;
  }

  public function get_mumin_photo() {
    if ($this->mumin_photo == '')
      $this->get_mumeen_photo_from_its();

    return $this->mumin_photo;
  }

  public function get_pincode() {
    return $this->pincode;
  }

  /*
   * -Setters
   */

  public function set_user_id($user_id) {
    $this->user_id = $user_id;
  }

  public function set_its_id($its_id) {
    $this->its_id = $its_id;
  }

  public function set_password($password) {
    $this->password = $password;
  }

  public function set_first_prefix($first_prefix) {
    $this->first_prefix = $first_prefix;
  }

  public function set_first_name($first_name) {
    $this->first_name = $first_name;
  }

  public function set_middle_prefix($middle_prefix) {
    $this->middle_prefix = $middle_prefix;
  }

  public function set_middle_name($middle_name) {
    $this->middle_name = $middle_name;
  }

  public function set_last_name($last_name) {
    $this->last_name = $last_name;
  }

  public function set_full_name_ar($full_name_ar) {
    $this->full_name_ar = $full_name_ar;
  }

  public function set_gender($gender) {
    $this->gender = $gender;
  }

  public function set_dob($dob) {
    $my_dob = date('Y-m-d', strtotime($dob));
    $this->dob = $my_dob;
  }

  public function set_nationality($nationality) {
    $this->nationality = $nationality;
  }

  public function set_address($address) {
    $this->address = $address;
  }

  public function set_country($country) {
    $this->country = $country;
  }

  public function set_state($state) {
    $this->state = $state;
  }

  public function set_city($city) {
    $this->city = $city;
  }

  public function set_mobile($mobile) {
    $this->mobile = $mobile;
  }

  public function set_telephone($telephone) {
    $this->telephone = $telephone;
  }

  public function set_email($email) {
    $this->email = $email;
  }

  public function set_whatsapp($whatsapp) {
    $this->whatsapp = $whatsapp;
  }

  public function set_viber($viber) {
    $this->viber = $viber;
  }

  public function set_tanzeem_id($tanzeem_id) {
    $this->tanzeem_id = $tanzeem_id;
  }

  public function set_last_update_ts($last_update_ts) {
    $this->last_update_ts = $last_update_ts;
  }

  /*
   * two values : student , professional
   */

  public function set_user_type($user_type) {
    // fix to solve if the user type is passed as text
    $user_type = (int) $user_type;
    if (is_int($user_type))
      $this->user_type = (int) $user_type;
    else
      $this->user_type = $this->get_user_type_by_string($user_type);
  }

  public function set_prefer_email($prefer_email) {
    $this->prefer_email = $prefer_email;
  }

  public function set_prefer_whatsapp($prefer_whatsapp) {
    $this->prefer_whatsapp = $prefer_whatsapp;
  }

  public function set_prefer_call($prefer_call) {
    $this->prefer_call = $prefer_call;
  }

  public function set_prefer_viber($prefer_viber) {
    $this->prefer_viber = $prefer_viber;
  }

  public function set_occupation($occupation) {
//TODO mmm set user_type here based on occupation value

    $this->occupation = $occupation;
//    if ($occupation == 'student') {
//      $this->set_user_type('student');
//    } else {
//      $this->set_user_type('professional');
//    }
  }

  public function set_qualification($qualification) {
    $this->qualification = $qualification;
  }

  public function set_jamaat($jamaat) {
    $this->jamaat = $jamaat;
  }

  public function set_jamiat($jamiat) {
    $this->jamiat = $jamiat;
  }

  public function set_pincode($pincode) {
    $this->pincode = $pincode;
  }

  public function get_subscribe() {
    return $this->subscribe;
  }

  public function set_subscribe($subscribe) {
    $this->subscribe = $subscribe;
  }

  public function get_quran_sanad() {
    return $this->quran_sanad;
  }

  public function set_quran_sanad($quran_sanad) {
    $this->quran_sanad = $quran_sanad;
  }

  /*
   * GETTERs and SETTERs 
   * --------------------------
   */
}

?>
