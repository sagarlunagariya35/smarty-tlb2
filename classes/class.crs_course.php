<?php
require_once 'class.database.php';

class Crs_Course {
  
  function get_all_courses($sort = 'ASC') {
    global $db;
    $query = "SELECT * FROM `crs_course` WHERE `is_active` LIKE '1' ORDER BY `sort_id` $sort";
    $result = $db->query_fetch_full_result($query);
    return $result;
  }

  function get_all_courses_of_topic($topic = FALSE) {
    global $db;
    $query = "SELECT * FROM `crs_course`";
    ($topic) ? $query .= " WHERE `topic_id` LIKE '$topic'" : '';
    
    $query .= " ORDER BY `id` DESC";
    $result = $db->query_fetch_full_result($query);
    return $result;
  }
  
  function get_course($id) {
    global $db;
    $query = "SELECT * FROM `crs_course` WHERE `id` LIKE '$id'";
    $result = $db->query_fetch_full_result($query);
    return $result[0];
  }
  
}
