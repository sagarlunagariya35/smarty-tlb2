<?php
require_once 'session.php';
require_once 'requires_login.php';
require_once 'classes/class.user.php';
require_once 'classes/class.araz.php';

$user = new mtx_user();
$user->loaduser($_SESSION[USER_ITS]);

// Page body class
$body_class = 'page-sub-page';
$heading_msg = 'Website is under maintenance mode';

require_once 'inc/inc.header2.php';
?>

<!-- Contents -->
<!-- ======================================================================================= -->
<div class="container white-bg">
  <div class="col-md-12 col-sm-12">

    <div class="page-title">
      <p style="margin-top:5px;">
        <a href="#" class="active"><?php echo $heading_msg; ?></a></p>
        <h1><?php echo $heading_msg; ?></h1>
    </div>
  </div>
  <div class="clearfix"></div> <!-- do not delete -->
  <div class="col-md-12 col-sm-12">
    <div class="page">
      <form class="forms1 white" action="#" method="post">
        <div class="block top-mgn10"></div>
        <div class="blue-box1">
          <div class="block bot-mgn20 hmgn20">
            <p class="large-fonts text-center">This Module is currently under maintenance. Kindly please send your araz for education through <a href="http://www.hawaij.org/">www.hawaij.org</a></p>
          </div>
          <div class="clearfix"></div> <!-- do not delete -->
        </div>
        <div class="clearfix"></div> <!-- do not delete -->
      </form>
      <div class="clearfix"></div> <!-- do not delete -->
    </div>
  </div>
</div>


<?php
require_once 'inc/inc.footer.php';
?>