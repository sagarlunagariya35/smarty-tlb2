<?php
  require 'smarty/libs/Smarty.class.php';
  require_once 'session.php';
  require_once 'requires_login.php';
  require_once 'classes/class.qrn_mumin.php';
  require_once 'classes/class.qrn_muhafiz.php';
  require_once 'classes/class.qrn_tasmee.php';
  require_once 'classes/class.user.php';
  
  $qrn_mumin = new Qrn_Mumin();
  $qrn_muhafiz = new Qrn_Muhafiz();
  $qrn_tasmee = new Qrn_Tasmee();
  $user_logedin = new mtx_user();
  
  $timestamp = date('Y-m-d');
  $tasmee_records = $qrn_tasmee->get_tasmee_log_of_mumin_date_wise_for_graph($_SESSION[USER_ITS], $timestamp);
  $mumin_data = $qrn_mumin->get_all_mumin($_SESSION[USER_ITS]);
  if ($mumin_data[0]['muhafiz_id'] != 0) {
    $user_logedin->loaduser($mumin_data[0]['muhafiz_id']);
  }
  $schedule_record = $qrn_tasmee->get_mumin_time_schedule($_SESSION[USER_ITS], $mumin_data[0]['muhafiz_id']);
  $total_ayat_count = $qrn_tasmee->get_mumin_total_ayat_count($_SESSION[USER_ITS]);
  
  require_once 'inc/inc.header2.php';
  require_once 'inc/inc.footer.php';
  
  $smarty = new Smarty;

  // Header / Session variables
  $smarty->assign("server_path", SERVER_PATH);
  $smarty->assign("mumin_data_header", $mumin_data_header);
  $smarty->assign("muhafiz_data_header", $muhafiz_data_header);
  $smarty->assign("tasmee_records", $tasmee_records);
  $smarty->assign("mumin_data", $mumin_data);
  $smarty->assign("qrn_tasmee", $qrn_tasmee);
  $smarty->assign("user_logedin", $user_logedin);
  $smarty->assign("schedule_record", $schedule_record);
  $smarty->assign("total_ayat_count", $total_ayat_count);
  $smarty->assign("success_message", $_SESSION['SUCCESS_MESSAGE']);
  $smarty->assign("error_message", $_SESSION['ERROR_MESSAGE']);

  $smarty->display('qrn_report_mumin.tpl');