<div id="question_panel">
  <?php if($questions['question_type'] != QUIZ_GENERAL_HTML){ ?>
  <div class="blue-box1 rtl border"> <!-- Add class rtl -->
    <h3><span class="inline-block lh3 arb_ques"><?php echo $questions['question']; ?></span></h3>
  </div>

  <input type="hidden" id="current_que_id" value="<?php echo $questions['id']; ?>">
  <input type="hidden" id="current_course_id" value="<?php echo $questions['course_id']; ?>">

  <?php
  $options = unserialize($questions['options']);
  if ($questions['question_type'] == QUIZ_MULTIPLE_CHOICE || $questions['question_type'] == QUIZ_TRUE_AND_FALSE) {
    $m = 1;
    foreach ($options as $opt) {
      $selected = ($pre_fill_user_answers['submitted_answer'] == $opt) ? 'checked' : '';
      ?>
      <div class="col-md-12 col-sm-12">
        <div class="quiz-point-radio rtl"> <!-- Add Class .rtl -->
          <input type="radio" name="your_answer" value="<?php echo $opt; ?>" id="radio1<?php echo $m++; ?>" class="" <?php echo $selected; ?> />
          <label for="radio1<?php echo $m++; ?>" style="color: #000;" class="radGroup3 lsd"><?php echo $opt; ?></label>
        </div>
      </div>
      <?php
    }
  }

  if ($questions['question_type'] == QUIZ_FILL_IN_THE_BLANK) {
    if (count($options) == 1 && $options[0] == '') {
      ?>
      <div class="col-md-12 col-sm-12">&nbsp;</div>
      <div class="col-md-12 col-sm-12">
        <input type="text" name="your_answer" id="your_answer" class="form-control" value="<?php echo $pre_fill_user_answers['submitted_answer']; ?>">
      </div>

      <?php
    } else {
      ?>
      <div class="col-md-12 col-sm-12">&nbsp;</div>
      <div class="col-md-12 col-sm-12">
        <select name="your_answer" id="your_answer" class="form-control">
          <option value="">Select Answer</option>
          <?php 
            foreach ($options as $opt) {
              $selected = ($pre_fill_user_answers['submitted_answer'] == $opt) ? 'selected' : '';
          ?>
            <option value="<?php echo $opt; ?>" <?php echo $selected; ?>><?php echo $opt; ?></option>
          <?php 
            } 
          ?>
        </select>
      </div>
      <?php
    }
  }

  if ($questions['question_type'] == QUIZ_MATCH_THE_FOLLOWING) {
    ?>
    <div class="col-md-12 col-sm-12">
      <div id="dragScriptContainer">
        <div id="questionDiv">
          <?php
          $m = 1;
          foreach ($options as $opt) {
            ?>
            <div class="dragDropSmallBox" id="q<?php echo $m++; ?>"><?php echo $opt; ?></div>
            <div class="destinationBox"></div>
            <?php
          }
          ?>
        </div>
        <?php
        $ans_options = unserialize($questions['correct_answer']);
        //shuffle($ans_options);
        ?>
        <div id="answerDiv">
          <?php
          $a = 1;
          foreach ($ans_options as $opt) {
            ?>
            <div class="dragDropSmallBox" id="a<?php echo $a++; ?>"><?php echo $opt; ?></div>
            <?php
          }
          ?>
        </div>
        <div id="dragContent"></div>
      </div>
      <input type="hidden" value="<?php echo $pre_fill_user_answers['submitted_answer']; ?>" name="your_answer" id="your_answer" class="form-control">
    </div>

    <?php
  }
  if ($questions['question_type'] == QUIZ_OPEN_ANSWERS) {
      ?>
      <div class="col-md-12 col-sm-12">&nbsp;</div>
      <div class="col-md-12 col-sm-12">
        <input type="text" name="your_answer" id="your_answer" class="form-control" value="<?php echo $pre_fill_user_answers['submitted_answer']; ?>">
      </div>

  <?php
    }
  }else {
    echo $questions['question'];
  }
  ?>
</div>

<div class="col-md-12 col-sm-12"><br></div>
<div class="col-md-12 col-sm-12">
  <button type="button" id="next" class="btn btn-success pull-right col-md-2">Next</button>
</div>
<div class="col-md-12 col-sm-12"><br></div>
<div class="clearfix"></div>

<?php if($questions['timer'] != 0){ ?>
  <script type="text/javascript">
    secs = <?php echo $questions['timer']; ?>;
    timer = setInterval(function () {
        if(secs < 1){
            clearInterval(timer);
            document.getElementById('next').click();
        }
        secs--;
    }, 1000);
  </script>
<?php } ?>
