<?php
include('session.php');
require_once 'classes/gen_functions.php';

$search_institutes = $inst_keywords = FALSE;
$search_courses = $course_keywords = FALSE;

$marhala = (isset($_GET['group'])) ? $_GET['group'] : FALSE;
$inst_keywords = (isset($_GET['s'])) ? $_GET['s'] : FALSE;
$course_keywords = (isset($_GET['c'])) ? $_GET['c'] : FALSE;

($inst_keywords) ? $search_institutes = search_institutes($inst_keywords,$marhala) : FALSE;
($course_keywords) ? $search_courses = search_finalize_courses($course_keywords) : FALSE;

if ($search_institutes) {
  foreach ($search_institutes as $r) {
    $institute_name = $r['institute'];
    ?>
    <div class="show" onclick="get_value_institute('<?php echo str_replace("'", "\'", $institute_name) ; ?>');">
      <span class="name"><?php echo $institute_name; ?></span>
    </div>
    <div class="clearfix"></div>
    <?php
  }
}

if ($search_courses) {
  foreach ($search_courses as $r) {
    $course_name = $r['course'];
    ?>
    <div class="show" onclick="get_value_course('<?php echo str_replace("'", "\'", $course_name) ; ?>');">
      <span class="name"><?php echo $course_name; ?></span>
    </div>
    <div class="clearfix"></div>
    <?php
  }
}

$cmd = (isset($_REQUEST['cmd'])) ? $_REQUEST['cmd'] : FALSE;

if($cmd){
  
  switch ($cmd){
    
    case 'get_school_list':
      $school = $_GET['q'];
      $result = search_schools($school);
      if ($result) {
        foreach ($result as $data) {
          $ret['items'][] = array("id" => $data['school_name'], "school_name" => $data['school_name']);
        }
      }
      echo json_encode($ret);
      break;
  }
}
?>