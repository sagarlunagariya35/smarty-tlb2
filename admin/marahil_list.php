<?php
require_once 'session.php';
require_once 'requires_login.php';

if (isset($_REQUEST['submit'])) {
  if (isset($_POST['already_araz_done'])) {
    $_SESSION['already_araz_done'] = $_POST['already_araz_done'];
  } else {
    $_SESSION['already_araz_done'] = 0;
  }

  // redirect here
  $marhala_id = $_POST['submit'];
  switch ($marhala_id) {
    case 1:
      header('location: ' . SERVER_PATH . 'marhala-' . $marhala_id . '/' . get_marhala_slug($marhala_id) . '/school');
      break;
    case 2:
    case 3:
    case 4:
    case 5:
    case 6:
    case 7:
    case 8:  
      header('location: ' . SERVER_PATH . 'marhala-' . $marhala_id . '/' . get_marhala_slug($marhala_id) . '/madrasah');
      break;
  }
}
// Page body class
$body_class = 'page-sub-page';

require_once 'inc/inc.header2.php';
?>

<style>
  .arz-list{
  list-style:none;
  display:block;
  margin:30px 0;
}

.arz-list li{
  float:left;
}

.arz-list li button{
  display:block;
  background-color:#f2f0f1;
  background-image:url(../images/arrow-right.png);
  background-position: 292px center;
  background-repeat:no-repeat;
  width:400px;
  height:40px;
  overflow:hidden;
  line-height:40px;
  border-radius: 12px;
  -moz-border-radius: 12px;
  -webkit-border-radius: 12px;
  border:1px solid #ccc;
  margin:0px auto 30px;
  padding:0 0 0 30px;
  color:#cf4914;
  text-transform:uppercase;
  font-size:14px;
  -webkit-transition: all 0.5s ease;
  -moz-transition: all 0.5s ease;
  -o-transition: all 0.5s ease;
  -ms-transition: all 0.5s ease;
  transition: all 0.5s ease;
}

.arz-list li button span{
  display:block;
  float:right;
  background-color:#cd4008;
  background-position:center;
  background-repeat:no-repeat;
  width:100px;
  text-align:center;
  height:40px;
  line-height:40px;
  margin:0px 0 0 30px;
  color:#fff;

}

.arz-list li button.others{
  background-color:#2c2c2c;
  color:#fff;
}

.arz-list li button sup{
  font-size:11px;
  text-transform:lowercase;
}

.arz-list li button:hover{
  border-color:#fff;
  background-image:url(../images/arrow-right-hover.png);
  color:#fff;
  background-color:#cf4914;
  -webkit-transition: all 0.5s ease;
  -moz-transition: all 0.5s ease;
  -o-transition: all 0.5s ease;
  -ms-transition: all 0.5s ease;
  transition: all 0.5s ease;
  text-decoration:none;
}

.arz-list li button:hover span{
  color:#fff;
  background-color:#07294d;
  -webkit-transition: all 0.5s ease;
  -moz-transition: all 0.5s ease;
  -o-transition: all 0.5s ease;
  -ms-transition: all 0.5s ease;
  transition: all 0.5s ease;
}
</style>


<!-- Contents -->
<!-- ====================================================================================================== -->
<div class="container white-bg">
  <div class="col-md-12 col-sm-12">

    <div class="page-title">
      <p style="margin-top:5px;"><a href="<?php echo SERVER_PATH; ?>">Home</a> / <a href="<?php echo SERVER_PATH; ?>raza-araz/">Send Araz To Aqa Maula TUS</a> / <a href="#" class="active">Raza Araz for</a></p>
      <h1>Raza Araz For<span class="alfatemi-text">في الحضرة العالية النورانية اْ مرحلة واسطے رزا ني عرض كروں چھوں</span></h1>
    </div>
  </div>
  <div class="clearfix"></div> <!-- do not delete -->
  <div class="col-md-12 col-sm-12">
    <form class="forms" action="<?php echo SERVER_PATH; ?>marahil/" method="post">
      <p><i class="fa fa-info-circle"></i>
        <span class="block floatleft right-mgn30 lh2 width80p small-text1">
          If you have already obtained raza from Aqa Moula TUS through other sources, kindly fill out your raza details for each respective group/marhala</span>
        <span class="block floatleft lh2">
          <div class="radiobuttons-holder">
            <input type="radio" name="already_araz_done" id="already_araz_done" class="css-radiobutton1" value="1" />
            <label for="already_araz_done" class="css-label1 radGroup1">Yes</label>
            <input type="radio" name="already_araz_done" id="already_araz_not_done" class="css-radiobutton1" />
            <label for="already_araz_not_done" class="css-label1 radGroup1">No</label>
          </div>
          <div class="clearfix"></div> <!-- do not delete -->
        </span>
      </p>

      <ul class="arz-list">
        <li class="col-md-6 col-sm-12">
          <button type="submit" name="submit" value="1">Pre Primary<span>3 to 6 yrs</span></button> 
        </li>
        <li class="col-md-6 col-sm-12">
          <button type="submit" name="submit" value="2">Primary<span>6 to 11 yrs</span></button>
        </li>
        <li class="col-md-6 col-sm-12">
          <button type="submit" name="submit" value="3">Std 5<sup>th</sup>  to Std 7<sup>th</sup><span>11 to 13 yrs</span></button>
        </li>
        <li class="col-md-6 col-sm-12">
          <button type="submit" name="submit" value="4">Std 8<sup>th</sup>  to Std 10<sup>th</sup><span>13 to 15 yrs</span></button>
        </li>
        <li class="col-md-6 col-sm-12">
          <button type="submit" name="submit" value="5">Std 10<sup>th</sup>  to Std 12<sup>th</sup><span>15 to 18 yrs</span></button>
        </li>
        <li class="col-md-6 col-sm-12">
          <button type="submit" name="submit" value="6">Graduation<span>18 to 21 yrs</span></button>
        </li>
        <li class="col-md-12 col-sm-12">
          <button type="submit" name="submit" value="7">Post Graduation<span>21 to 24 yrs</span></button>
        </li>
        <li class="col-md-12 col-sm-12">
          <button type="submit" name="submit" value="8">Diploma / other courses<span>Proceed</span></button>
        </li>
      </ul>
    </form>
  </div>
</div>

<script>
  $('input').on('ifChecked', function() {
    /*if (this.name == 'already_araz_done') {
      $('#from_name').show(800);
    };*/
  });
  $('input').on('ifUnchecked', function() {
    /*if (this.name == 'already_araz_done') {
      $('#from_name').hide(800);
    };*/
  });
  
  $('#from_name').hide();
  
  $('#araz_done_from_name').tooltip({'trigger': 'focus', 'title': 'Write full name with correct spellings'});
</script>

<?php
require_once 'inc/inc.footer.php';
?>