<?php
require_once 'classes/class.user.admin.php';
require_once '../classes/class.user.php';
include 'classes/class.araiz.php';
$allowed_roles = array(ROLE_HEAD_OFFICE,ROLE_AAMIL_SAHEB,ROLE_JAMIAT_COORDINATOR);
require_once 'session.php';

$user = new mtx_user_admin;
$araiz = new Araiz();

$title = 'Search Araiz By Multiple Data';
$description = '';
$keywords = '';
$active_page = "search_araiz_by_multiple_data";

$select = FALSE;
$from_date = $to_date = $pagination = FALSE;
$araz_saheb_id = $saheb_id = $araz_coun_id = $coun_id = FALSE;
$counselor_days_past = $saheb_days_past = $jamiyet_data = FALSE;

$jamiyet = unserialize($_SESSION['jamiat']);
if (count($jamiyet) == 1 && $jamiyet[0] != '') {
  $jamiyet_data = implode("','", $jamiyet[0]);
}

// Fix for ROLE_JAMIAT_COORDINATOR is no jamiat assign
if ($_SESSION[USER_ROLE] == ROLE_JAMIAT_COORDINATOR && $jamiyet_data === FALSE) {
  $jamiyet_data = ' ';
}

if(isset($_POST['print_list_raza']))
{
  $_SESSION['track_id'] = (array) $_POST['jawab'];
  header('location: print_raza_araiz_list_date_wise.php');
}

if(isset($_POST['print_list_istirshad']))
{
  $_SESSION['track_id'] = (array) $_POST['jawab'];
  header('location: print_istirshad_araiz_date_wise.php');
}

if(isset($_POST['search'])) {
  
  $track_no = $_POST['track_no'];
  $its_id = $_POST['its_id'];
  $marhala = $_POST['marhala'];
  $darajah = $_POST['darajah'];
  $madrasah_name = $_POST['madrasah_name'];
  $user_full_name = $_POST['user_full_name'];
  $school_city = $_POST['school_city'];
  $school_standard = $_POST['school_standard'];
  $school_name = $_POST['school_name'];
  $filter_school = $_POST['filter_school'];
  $from_date = $_POST['from_date'];
  $to_date = $_POST['to_date'];
  $stream = $_POST['stream'];
  $course_name = $_POST['course_name'];
  $institute_name = $_POST['institute_name'];
  $institute_city = $_POST['institute_city'];
  $jamaat = $_POST['jamaat'];
  $araz_type = $_POST['araz_type'];
  $araz_status = $_POST['araz_status'];
  
  $select = $araiz->get_multiple_data_search_araz($track_no,$its_id,$marhala,$darajah,$madrasah_name,$user_full_name,$school_city,$school_standard,$school_name,$filter_school,$from_date,$to_date,$stream,$course_name,$institute_name,$institute_city,$jamaat,$araz_type,$araz_status,$jamiyet_data);
}

$counselors = $user->get_all_counselors();
$sahebs = $user->get_all_sahebs();

$araiz_parsed_data = $araiz->get_araiz_data_organised($select);

include ('header.php');
$counselor_and_saheb_content = COUNSELOR_AND_SAHEB_CONTENT;
$jawab_sent_content = JAWAB_SENT_CONTENT;
?>
<link rel="stylesheet" href="js/tablesorter/css/theme.bootstrap.css">
<script src="js/tablesorter/js/jquery.tablesorter.js"></script>
<script src="js/tablesorter/js/jquery.tablesorter.widgets.js"></script>
<style>
  .alfatemi-text{
    font-size: 18px;
  }
</style>
<div class="row">
  <div class="row">
    <div class="col-xs-12 col-sm-12">&nbsp;</div>
  </div>
  <div class="row">
    <div class="col-xs-12 col-sm-12">
      <h2 class="text-center">Total Pending Raza Araiz Count: <?php echo count($araiz_parsed_data); ?></h2>
    </div>
  </div>
  <div class="row">
    <div class="col-xs-12 col-sm-12">&nbsp;</div>
  </div>
  <form name="raza_form" class="form" method="post" action="">
    <div class="row">
      <div class="col-xs-12 col-sm-3">
        <select name="counselor_id" class="form-control" id="counselor_id">
          <option value="">Select Counselor</option>
          <?php
          if ($counselors) {
            foreach ($counselors as $coun) {
              ?>
              <option value="<?php echo $coun['user_id'] ?>"><?php echo $coun['first_name'] . ' ' . $coun['last_name']; ?></option>
              <?php
            }
          }
          ?>
        </select>
      </div>
      <div class="col-xs-12 col-sm-3">
        <input type="submit" name="sent_counselor" id="sent_counselor" value="Assign Counselor" class="btn btn-success btn-block assign-counselor">
      </div>
      <div class="col-xs-12 col-sm-3">
        <select name="saheb_id" class="form-control" id="saheb_id">
          <option value="">Select Saheb</option>
          <?php
          if ($sahebs) {
            foreach ($sahebs as $s) {
              ?>
              <option value="<?php echo $s['user_id'] ?>"><?php echo $s['first_name'] . ' ' . $s['last_name']; ?></option>
              <?php
            }
          }
          ?>
        </select>
      </div>
      <div class="col-xs-12 col-sm-3">
        <input type="submit" name="sent_saheb" id="sent_saheb" value="Assign Saheb" class="btn btn-success btn-block assign-saheb">
      </div>
    </div>
    <div class="row">
      <div class="col-xs-12">&nbsp;</div>
    </div>
    <div class="row">
      <div class="col-xs-12 col-sm-3" style="padding-top:6px">
      </div>
      <div class="col-xs-12 col-sm-3">
      </div>
      <div class="col-xs-12 col-sm-3">
        <select name="jawab_city" class="form-control" id="jawab_city">
          <option value="">Select City</option>
          <?php
          if ($jamaats) {
            foreach ($jamaats as $data) {
              ?>
              <option value="<?php echo $data['jamaat']; ?>"><?php echo $data['jamaat']; ?></option>
              <?php
            }
          }
          ?>
        </select>
      </div>
      <div class="col-xs-12 col-sm-3">
        <input type="submit" name="sent_jawab" id="sent_jawab" value="Send Jawab" class="btn btn-success btn-block assign-jawab">
      </div>
    </div>
  
  <div class="col-lg-12 col-xs-12 col-sm-12 pull-left">
    <div class="col-md-12">&nbsp;</div>
    <div class="clearfix"></div>
    <div class="row">
      <ul class="nav nav-tabs nav-justified" role="tablist">
        <li role="presentation" class="active"><a href="#raza_araiz" aria-controls="pre" role="tab" data-toggle="tab">Raza Araiz</a></li>
        <li role="presentation"><a href="#istirshad_araiz" aria-controls="in" role="tab" data-toggle="tab">Isttirshad Araiz</a></li>
      </ul>
      <div class="row">
        <div class="col-xs-12 col-sm-12">&nbsp;</div>
      </div>
      <div class="tab-content">
        <div role="tabpanel" class="tab-pane active" id="raza_araiz">
          <div class="col-xs-12 col-sm-3" style="padding-top:6px">
            <input type="checkbox" name="print" value="Print" id="check_all_raza" onchange="checkAllRaza(this);" class="css-checkbox1"><label for="check_all_raza" class="css-label1">Check All</label>
          </div>
          <div class="col-xs-12 col-sm-3 pull-right">
            <input type="submit" name="print_list_raza" value="Print" class="btn btn-success btn-block assign-print">
          </div>
          <div class="row">
            <div class="col-xs-12 col-sm-12">&nbsp;</div>
          </div>
          <?php
          foreach ($araiz_parsed_data as $araz_id => $araz) {
            // Create bins for User Data, Araz Data, Previous Studies, Current Studeis, Remarks
            $user_details = $araz['user_data'];
            $araz_general_detail = $araz['araz_data'];
            $previous_study_details = $araz['previous_study_details'];
            $course_details = $araz['course_details'];
            $counselor_details = $araz['counselor_details'];

            if($user_details['araz_type'] == 'raza'){
              include 'inc/inc.show_parsed_araiz_details.php';
            }
          }
          ?>
        </div>
        
        <div role="tabpanel" class="tab-pane" id="istirshad_araiz">
          <div class="col-xs-12 col-sm-3" style="padding-top:6px">
            <input type="checkbox" name="print" value="Print" id="check_all_istirshad" onchange="checkAllIstirshad(this);" class="css-checkbox1"><label for="check_all_istirshad" class="css-label1">Check All</label>
          </div>
          <div class="col-xs-12 col-sm-3 pull-right">
            <input type="submit" name="print_list_istirshad" value="Print" class="btn btn-success btn-block assign-print">
          </div>
          <div class="row">
            <div class="col-xs-12 col-sm-12">&nbsp;</div>
          </div>
          <?php
          foreach ($araiz_parsed_data as $araz_id => $araz) {
            // Create bins for User Data, Araz Data, Previous Studies, Current Studeis, Remarks
            $user_details = $araz['user_data'];
            $araz_general_detail = $araz['araz_data'];
            $previous_study_details = $araz['previous_study_details'];
            $course_details = $araz['course_details'];
            $counselor_details = $araz['counselor_details'];

            if($user_details['araz_type'] == 'istirshad'){
              include 'inc/inc.show_parsed_araiz_details.php';
            }
          }
          ?>
        </div>
      </div>
    </div>
  </div>
</form>
</div>
<style>
  .popover{
    color:#000;
  }
</style>

<?php
include 'footer.php';
?>