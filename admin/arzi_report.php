<?php
$title = 'Arazi city Wise';
$description = '';
$keywords = '';
$active_page = "Arazi city Wise";

include 'classLoader.php';
$global = new Glob_func();

//get
$getCity = FALSE;
if (isset($_GET['city']) && $_GET['city']) {
  $getCity = $db->clean_data(urldecode($_GET['city']));
  $records = $global->select_data('araiz', $getCity, 'city');
}
//read data
$cities = $global->getDistinct('city', 'araiz');
$title = 'Arazi city Wise';

include_once("header.php");
?>

  
            <div class="row">
                <div class="col-lg-12">
                    <h3 class="page-header">City Wise Records</h3>
                </div>
                <!-- /.col-lg-12 -->
            </div>
      <div class="col-lg-12">&nbsp;</div>
    <div class="col-lg-12 col-xs-12"><a href="print_city.php?city=<?php echo $getCity; ?>" class="btn btn-success btn-xs pull-right <?php echo (!$getCity) ? 'disabled' : ''; ?>" target="_blank">Print</a></div>
            <!-- /.row -->
             <div class="col-lg-12">&nbsp;</div>
            <form class="form-inline text-center" role="form" id="frmCity">
      <div class="form-group">
        <label class="control-label">City</label>
        <select class="form-control" name="city">
          <option value="">--Select City--</option>
          <?php foreach ($cities as $c) {?>
          <option value="<?php echo urlencode($c['city']); ?>" <?php echo $getCity == $c['city'] ? 'selected' : ''; ?>><?php $cityName = $global->getNameById($c['city'], 'city'); echo is_null($cityName) ? $c['city'] : $cityName['name']; ?></option>
          <?php } ?>
        </select>
      </div>
      <div class="form-group">
        
        <button type="submit" class="btn btn-success">Search</button>
      </div>
    </form>
    <div class="col-lg-12">&nbsp;</div>
         <?php if($getCity) require './inc/school_wise.php'; ?>  
 


<?php include ('footer.php');?>