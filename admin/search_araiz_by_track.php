<?php
require_once 'classes/class.user.admin.php';
include 'classes/class.araiz.php';
$allowed_roles = array(ROLE_AAMIL_SAHEB);
require_once 'session.php';

$user = new mtx_user_admin;
$araiz = new Araiz();

$title = 'Search Araiz By Track No.';
$description = '';
$keywords = '';
$active_page = "search_araiz_by_track";

include ('header.php');

$select = $pagination = FALSE;
$track_no = $res1 = $res2 = FALSE;

if(isset($_POST['search'])) {
  $track_no = $_POST['track_no'];
  $select = $araiz->get_araz_data_by_araz_id($track_no);
}

?>

  <div class="row">
    <form method="post" action="" name="raza_form" class="form-horizontal" onsubmit="return(validate());">
     <div class="col-lg-12">
        <h3 class="page-header"><?php echo $title; ?></h3>
      </div>
      <div class="col-md-12">
        <label class="col-md-2 control-label">Track No.</label>
        <div class="col-md-3">
          <input type="text" name="track_no" class="form-control" value="<?php echo $track_no; ?>">
        </div>
        <div class="col-md-2">
          <input type="submit" name="search" value="Search" class="btn btn-block btn-success">
        </div>
        <?php if(isset($_POST['search'])) { ?>
          <!--<div class="col-md-2 pull-right">
            <a href="print_araiz_list.php?track_id=<?php echo $track_no; ?>" target="_blank" class="btn btn-success btn-block pull-right">Print</a>
          </div>-->
        <?php } ?>
      </div>
    </form>
    
    <?php if(isset($_POST['search'])) { ?>
    <form method="post" action="">
    <div class="col-lg-12 col-xs-12 col-sm-12 pull-left">
      <div class="col-md-12">&nbsp;</div>
      <div class="clearfix"></div>
      <div class="panel panel-primary">
        <div class="panel-heading">
          <i class="fa fa-group fa-fw"></i> Araz Details of <?php echo $select[0]['user_full_name']; ?>
        </div>
        <!-- /.panel-heading -->
        <div class="panel-body">
          <div class="table-responsive">
            <table class="table table-striped table-hover table-condensed">
              <thead>
                <th>Track No</th>
                <th>ITS</th>
                <th>Name</th>
                <th>Araz Type</th>
                <th>Status</th>
              </thead>
              <tbody>
                <?php
                $current_araz = null;
                  if($select){
                    if($select[0]['araz_id'] != $current_araz) {
                    $current_araz = $select[0]['araz_id'];
                  ?>

                <tr>
                  <td><?php echo $select[0]['araz_id']; ?></td>
                  <td><?php echo $select[0]['login_its']; ?></td>
                  <td><?php echo $select[0]['user_full_name']; ?></td>
                  <td><?php echo ucfirst($select[0]['araz_type']); ?></td>
                  <td style="color: green;"><?php if($select[0]['counselor_id'] != 0){ echo 'Pending at Counselor'; } else if($select[0]['saheb_id'] != 0){ echo 'Pending at Saheb'; } ?></td>
                </tr>
                
                <?php 
                if($select[0]['school_name'] == ''){
                  foreach ($select as $ac) {
                    if($ac['araz_id'] == $current_araz) {
                ?>
                <tr class="<?php if($ac['jawab_given'] == '1'){ echo 'success'; } ?>">
                  <td></td>
                  <td colspan='2'><strong>Degree / Course Name : </strong><?php echo $ac['course_name']; ?></td>
                  <td colspan='2'><strong>Institute Name : </strong><?php echo $ac['institute_name']; ?></td>
                </tr>
                
                <?php 
                      }
                    }
                  }else {
                ?>
                
                <tr class="<?php if($select[0]['jawab_given'] == '1'){ echo 'success'; } ?>">
                  <td></td>
                  <td colspan='2'><strong>School Name : </strong><?php echo $select[0]['school_name']; ?></td>
                  <td colspan='2'><strong>School Standard : </strong><?php echo $select[0]['school_standard']; ?></td>
                </tr>
                <?php
                    }
                  }
                }
                else
                {
                  echo '<tr><td colspan="7" class="text-center">No Records</td></tr>';
                }
                ?>
              </tbody>
            </table>
          </div>
        </div>
      </form>
    <?php } ?>
  </div>
<!-- /#page-wrapper -->
<!-- /#wrapper -->
<?php
include 'footer.php';
?>         