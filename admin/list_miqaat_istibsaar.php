<?php
require_once '../classes/constants.php';
require_once '../classes/class.database.php';
require_once 'classes/class.user.admin.php';
require_once 'classes/class.article.php';

$allowed_roles = array(ROLE_HEAD_OFFICE);
require_once 'session.php';

// Get list of articles
$art = new Article();
$list_miqaat_istibsaar = $art->get_all_miqaat_istibsaar();

$title = 'Miqaat Istibsaar';
$description = '';
$keywords = '';
$active_page = "list_miqaat_istibsaar";

if (isset($_POST['delete_id'])) {
  $delete_id = $_POST['delete_id'];
  $result = $art->delete_miqaat_istibsaar($delete_id);

  if ($result) {
    $_SESSION[SUCCESS_MESSAGE] = "Miqaat Istibsaar Successfully Delete";
    header('Location: list_miqaat_istibsaar.php');
    exit();
  } else {
    $_SESSION[ERROR_MESSAGE] = "Error In Delete";
  }
}

include_once("header.php");

?>
  <div class="row">
    <div class="col-lg-12 col-xs-12 col-sm-12  pull-left">
      <div class="col-md-12">&nbsp;</div>
      <div class="clearfix"></div>
      <div class="panel panel-green">
        <div class="panel-heading">
          <i class="fa fa-group fa-fw"></i> Manage Miqaat Istibsaar
          <span class="pull-right"><a href="add_miqaat_istibsaar.php" style="color: white;">Add New Miqaat Istibsaar</a></span>
        </div>
        <!-- /.panel-heading -->
        <div class="panel-body">
          <div class="table-responsive">
            <table class="table table-striped table-hover table-condensed">
              <thead>
                <tr>
                  <th>No</th>
                  <th>Miqaat Title</th>
                  <th>Miqaat Slug</th>
                  <th>Year</th>
                  <th>Action</th>
                </tr>
              </thead>
              <tbody>
                <?php
                $i = 1;
                if($list_miqaat_istibsaar){
                  foreach ($list_miqaat_istibsaar as $lmi) {
                ?>
                    <tr>
                      <td><?php echo $i++; ?></td>
                      <td><?php echo $lmi['miqaat_title']; ?></td>
                      <td><?php echo $lmi['slug']; ?></td>
                      <td><?php echo $lmi['year']; ?></td>
                      <td>
                        <a href="edit_miqaat_istibsaar.php?id=<?php echo $lmi['id']; ?>"><i class="fa fa-edit fa-fw"></i></a>
                      <a href="#" class="confirm" id="<?php echo $lmi['id']; ?>"><i class="fa fa-remove fa-fw text-danger"></i></a>
                      </td>
                    </tr>
                    <?php
                  }
                }else {
                  echo '<tr><td class="text-center" colspan="4">No Records..</td></tr>';
                }
                ?>
              </tbody>
            </table>
          </div>
          <!-- /.table-responsive -->
        </div>
        <!-- /.panel-body -->
      </div>
    </div>
    <form method="post" id="frm-del-grp">
      <input type="hidden" name="delete_id" id="del-miqaat-val" value="">
      <input type="hidden" name="cmd" value="del-miqaat">
    </form>
  </div><!-- ./ row -->
  <script>
    $(".confirm").confirm({
      text: "Are you sure you want to delete?",
      title: "Confirmation required",
      confirm: function(button) {
        $('#del-miqaat-val').val($(button).attr('id'));
        $('#frm-del-grp').submit();
        alert('Are you Sure You want to delete: ' + id);
      },
      cancel: function(button) {
        // nothing to do
      },
      confirmButton: "Yes",
      cancelButton: "No",
      post: true,
      confirmButtonClass: "btn-danger"
    });
  </script>
  
<?php include "./footer.php"; ?>
