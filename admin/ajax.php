<?php
session_start();
require_once '../classes/class.database.php';
require_once 'classes/class.user.admin.php';
include 'classes/class.araiz.php';
include 'classes/class.crs_topic.php';
include 'classes/class.crs_course.php';
include 'classes/class.crs_chapter.php';
include 'classes/class.menus.php';
require_once ('classes/admin_gen_functions.php');

$user = new mtx_user_admin;
$araiz = new Araiz();
$crs_topic = new Mtx_Crs_Topic();
$crs_course = new Mtx_Crs_Course();
$crs_chapter = new Mtx_Crs_Chapter();
$crs_menu = new Mtx_Menu();

 if(isset($_GET['id']))
 {
   global $db;
$ary_id = "1, 2, 3";

$pass_id = split(', ' ,$ary_id);

$remove_question  = "delete from questionary where id IN ($pass_id)";

$result = $db->query($remove_question);
return $result;
 }

 
 if(isset($_GET['question']))
 {
   global $db;
   $group_id = $_GET['Group_id'];
   $question = $_GET['que'];
   
   $add_question = ("INSERT INTO `questionary`(`grpID`, `question`) VALUES ('$group_id','$question')");
   
   $result = $db->query($add_question);
return $result;
 }

function display_question($group_id,$page,$count)
{
  global $db;
  
   $limit = ($page) * $count;
 
  
  $get_result = "SELECT * FROM `questionary` ";
    if ($group_id) {
      $get_result .= " where id = $group_id ";
    }
  
   $get_result .= " limit $limit,$count";
   
    $row = $db->query_fetch_full_result($get_result);
    return $row;
 
}

$cmd = (isset($_REQUEST['cmd'])) ? $_REQUEST['cmd'] : FALSE;

if($cmd){
  switch ($cmd){
    case 'get_miqat_list':
      $year = $_POST['year'];
      $query = "SELECT id, miqaat_title FROM miqaat_istibsaar WHERE year LIKE '$year'";
      $result = $db->query_fetch_full_result($query);
      echo json_encode($result);
      break;
    
    case 'make_entry_of_counselor':
      $araz_id = $_POST['araz_id'];
      $counselor_id = $_POST['counselor_id'];
      $counselor_remarks = $db->clean_data($_POST['coun_remarks']);
      $date = date('Y-m-d');
      
      if($counselor_id != ''){
        $res = $araiz->araz_assign_to_counselor($araz_id,$date,$counselor_id,$counselor_remarks);
      }
      break;
    
    case 'make_entry_of_saheb':
      $araz_id = $_POST['araz_id'];
      $saheb_id = $_POST['saheb_id'];
      $saheb_remarks = $db->clean_data($_POST['saheb_remarks']);
      $date = date('Y-m-d');
      
      if($saheb_id != ''){
        $res = $araiz->araz_assign_to_saheb($araz_id,$date,$saheb_id,$saheb_remarks);
      }
      break;
      
    case 'make_entry_of_jawab':
      $araz_id = $_POST['araz_id'];
      $jawab_city = $_POST['city'];
      $course_id = $_POST['course_id'];
      $ho_remarks_araz = $db->clean_data($_POST['ho_remarks']);
      $date = date('Y-m-d');
      
      if($jawab_city != ''){
        $res = $araiz->araz_send_jawab($araz_id, $jawab_city, $date, $course_id, $ho_remarks_araz);
        
        if ($res) {
          $_SESSION[SUCCESS_MESSAGE] = "Jawab Sent successfully";
        } else {
          $_SESSION[ERROR_MESSAGE] = "Error In Jawab Sending..";
        }
      }
      break;
      
    case 'make_entry_of_bazid_jawab':
      $araz_id = $_POST['araz_id'];
      $bazid_jawab = $_POST['bazid_jawab'];
      $jawab_city = $_POST['city'];
      $course_id = $_POST['course_id'];
      $ho_remarks_araz = $db->clean_data($_POST['ho_remarks']);
      $date = date('Y-m-d');
      
      if($jawab_city != ''){
        $res = $araiz->araz_send_bazid_jawab($araz_id, $bazid_jawab, $jawab_city, $date, $course_id, $ho_remarks_araz);
        
        if ($res) {
          $_SESSION[SUCCESS_MESSAGE] = "Jawab Sent successfully";
        } else {
          $_SESSION[ERROR_MESSAGE] = "Error In Jawab Sending..";
        }
      }
      break;
      
    case 'make_entry_of_print':
      $araz_id = $_POST['araz_id'];
      $saheb_remarks = $db->clean_data($_POST['saheb_remarks']);
      
      $res = $araiz->araz_assign_to_print($araz_id,$saheb_remarks);
      break;
    
    case 'make_entry_of_attend_later':
      $araz_id = $_POST['araz_id'];
      $ho_remarks_araz = $db->clean_data($_POST['ho_remarks']);
      
      $res = $araiz->araz_assign_to_attend_later($araz_id,$ho_remarks_araz);
      break;
      
    case 'make_entry_of_counselor_suggestion':
      $araz_id = $_POST['araz_id'];
      $counselor_id = $_POST['counselor_id'];
      $course = $_POST['course'];
      $country = $_POST['country'];
      $city = $_POST['city'];
      $remarks = $db->clean_data($_POST['remarks']);
      
      $res = $araiz->insert_counselor_suggestions($araz_id,$counselor_id,$course,$country,$city,$remarks);
      break;
    
    case 'make_entry_of_ho_remarks':
      $araz_id = $_POST['araz_id'];
      $remarks = $db->clean_data($_POST['remarks']);
      
      $res = $araiz->insert_araz_ho_remarks($araz_id,$remarks);
      break;
    
    case 'make_entry_of_recommend_counselor':
      $araz_id = $_POST['araz_id'];
      $counselor_id = $_POST['counselor_id'];
      $saheb_id = $_SESSION[USER_ID];
      $date = date('Y-m-d');
      
      if($counselor_id != ''){
        $res = $araiz->saheb_recommend_to_counselor_araz($araz_id,$saheb_id,$counselor_id,$date);
      }
      break;
      
    case 'make_entry_of_accept_recommendation':
      $araz_id = $_POST['araz_id'];
      $counselor_id = $_POST['counselor_id'];
      $remarks = $db->clean_data($_POST['remarks']);
      $date = date('Y-m-d');
      
      if($araz_id != ''){
        $res = $araiz->accept_saheb_recommendation($araz_id,$counselor_id,$remarks,$date);
      }
      break;
    
    case 'make_entry_of_review':
      $araz_id = $_POST['araz_id'];
      $recommend_test = $_POST['recommend_test'];
      $recommend_coun = $_POST['recommend_coun'];
      $recommend_qardan = $_POST['recommend_qardan'];
      $recommend_sponsorship = $_POST['recommend_sponsorship'];
      
      $res = $araiz->insert_araz_review($araz_id,$recommend_test,$recommend_coun,$recommend_qardan,$recommend_sponsorship);
      break;
    
    case 'make_entry_of_bchet_status':
      $araz_id = $_POST['araz_id'];
      $set_status = $_POST['set_status'];
      
      $res = $araiz->insert_araz_bchet_status($araz_id,$set_status);
      break;
    
    case 'make_entry_of_queries_course':
      $marhala = $_POST['marhala'];
      $degree = $_POST['degree'];
      $course = $_POST['course'];
      
      $res = insert_course_by_marhala_and_degree($marhala, $degree, $course);
      break;
      
    case 'make_entry_of_queries_institute':
      $marhala = $_POST['marhala'];
      $institute = $_POST['institute'];
      
      $res = insert_institute_by_marhala($marhala, $institute);
      break;
    
    case 'make_entry_of_queries_country':
      $country = $_POST['country'];
      $iso = $_POST['iso'];
      
      $res = insert_country_by_queries($country, $iso);
      break;
    
    case 'make_entry_of_queries_city':
      $country = $_POST['country'];
      $city = $_POST['city'];
      
      $res = insert_city_by_country($country, $city);
      break;
    
    case 'make_entry_of_chat':
      $araz_id = $_POST['araz_id'];
      $message = $db->clean_data($_POST['message']);
      $user_id = $_SESSION[USER_ID];
      $user_type = $_SESSION[USER_TYPE];
      
      if($message != ''){
        $res = $araiz->insert_araz_chat_message($araz_id,$message,$user_id,$user_type);
      }
      break;
      
    case 'make_entry_of_whitelist_course':
      $course = $_POST['course'];
      
      $res = $araiz->insert_whitelist_data('tlb_its_course','course',$course);
      break;
    
    case 'make_entry_of_whitelist_institute':
      $institute = $_POST['institute'];
      
      $res = $araiz->insert_whitelist_data('tlb_its_institute','institute',$institute);
      break;
    
    case 'make_entry_of_whitelist_city':
      $city = $_POST['city'];
      
      $res = $araiz->insert_whitelist_data('tlb_its_city','city',$city);
      break;
    
    case 'check_iso_exist':
      $iso = $_GET['iso'];
      $result = check_iso_already_exist($iso);
      if ($result) {
        echo $result;
      }
      break;
      
    case 'check_user_exist':
      $userid = $_GET['userid'];
      $result = $user->check_user_already_exist($userid);
      if ($result) {
        echo $result;
      }
      break;
    
    case 'check_old_password':
      $old_pwd = $_GET['old_pwd'];
      $userid = $_GET['id'];
      $result = $user->check_old_password($old_pwd, $userid);
      echo $result;
      break;
    
    case 'get_topics_title':
      $topic = $_GET['q'];
      $result = $crs_topic->get_title_of_topics($topic);
      if ($result) {
        foreach ($result as $data) {
          $ret['items'][] = array("id" => $data['id'], "title" => $data['title']);
        }
      }
      echo json_encode($ret);
      break;
      
    case 'get_courses_title':
      $course = $_GET['q'];
      $result = $crs_course->get_title_of_courses($course);
      if ($result) {
        foreach ($result as $data) {
          $ret['items'][] = array("id" => $data['id'], "title" => $data['title']);
        }
      }
      echo json_encode($ret);
      break;
      
    case 'get_chapters_title':
      $chapter = $_GET['q'];
      $result = $crs_chapter->get_title_of_chapters($chapter);
      if ($result) {
        foreach ($result as $data) {
          $ret['items'][] = array("id" => $data['id'], "title" => $data['title']);
        }
      }
      echo json_encode($ret);
      break;
      
    case 'get_menus_title':
      $menu = $_GET['q'];
      $result = $crs_menu->get_title_of_menus($menu);
      if ($result) {
        foreach ($result as $data) {
          $ret['items'][] = array("id" => $data['id'], "title" => $data['name']);
        }
      }
      echo json_encode($ret);
      break;
      
    case 'get_its_id_records':
      $its_id = $_POST['its_id'];
      $result = $user->get_its_id_records($its_id);
      echo json_encode($result);
      break;
  }
}

?>