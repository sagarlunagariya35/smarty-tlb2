<?php
require_once '../classes/constants.php';
require_once '../classes/class.database.php';
require_once 'classes/admin_gen_functions.php';
require_once 'classes/class.user.admin.php';

$allowed_roles = array(ROLE_HEAD_OFFICE);
require_once 'session.php';

// Get list of tanzeems
$list_araz_cities = get_all_tanzeem_from_araz_edu_comm();

// Split the data into 3 sections, arz received, downloaded and misal uploaded
$ary_received = array();
$ary_downloaded = array();
$ary_misal_uploaded = array();
$ary_uniques = array();
if($list_araz_cities)
{
foreach ($list_araz_cities as $key => $value) {
  // Filter the duplicates from list due to mix of downloaded and not downloaded records
  if (in_array($value['tanzeem_id'], $ary_uniques)) {
    unset($list_araz_cities[$key]);
  } else {
    $ary_uniques[] = $value['tanzeem_id'];
    if ($value['misaal']) {
      $ary_misal_uploaded[] = $value;
    } elseif ($value['downloaded']) {
      $ary_downloaded[] = $value;
    } else {
      $ary_received[] = $value;
    }
  }
}
}
$title = 'Head Office Back end Module';
$description = '';
$keywords = '';
$active_page = "ho_list_cities";

include_once("header.php");
?>

<link rel="stylesheet" href="js/tablesorter/css/theme.bootstrap.css">
<script src="js/tablesorter/js/jquery.tablesorter.js"></script>
<script src="js/tablesorter/js/jquery.tablesorter.widgets.js"></script>
<!-- Tablesorter: optional -->



  <div class="row">
    <div class="col-lg-12">
      <h1 class="page-header">Hub and Local Mahad al Hasanaat al Burhaniyyah (Education) committee</h1>
    </div>
    <!-- /.col-lg-12 -->
  </div>
  <!-- /.row -->

  <div class="row">
    <div class="col-lg-3 col-md-6">
      <div class="panel panel-primary">
        <div class="panel-heading">
          <div class="row">
            <div class="col-xs-3">
              <i class="fa fa-briefcase fa-5x"></i>
            </div>
            <div class="col-xs-9 text-right">
              <div class="huge"><?php echo count($ary_received); ?></div>
              <div></div>
            </div>
          </div>
        </div>
        <a href="#received">
          <div class="panel-footer">
            <span class="pull-left">Total araz pending</span>
            <span class="pull-right"><i class="fa fa-briefcase"></i></span>
            <div class="clearfix"></div>
          </div>
        </a>
      </div>
    </div>
    <div class="col-lg-3 col-md-6">
      <div class="panel panel-green">
        <div class="panel-heading">
          <div class="row">
            <div class="col-xs-3">
              <i class="fa fa-download fa-5x"></i>
            </div>
            <div class="col-xs-9 text-right">
              <div class="huge"><?php echo count($ary_downloaded); ?></div>
              <div></div>
            </div>
          </div>
        </div>
        <a href="#downloaded">
          <div class="panel-footer">
            <span class="pull-left">Total araz Downloaded</span>
            <span class="pull-right"><i class="fa fa-download"></i></span>
            <div class="clearfix"></div>
          </div>
        </a>
      </div>
    </div>
    <div class="col-lg-3 col-md-6">
      <div class="panel panel-yellow">
        <div class="panel-heading">
          <div class="row">
            <div class="col-xs-3">
              <i class="fa fa-check-square-o fa-5x"></i>
            </div>
            <div class="col-xs-9 text-right">
              <div class="huge"><?php echo count($ary_misal_uploaded); ?></div>
              <div></div>
            </div>
          </div>
        </div>
        <a href="#misal_uploaded">
          <div class="panel-footer">
            <span class="pull-left">Total Misal Uploaded</span>
            <span class="pull-right"><i class="fa fa-check-square-o"></i></span>
            <div class="clearfix"></div>
          </div>
        </a>
      </div>
    </div>
    <div class="col-lg-3 col-md-6">
      <div class="panel panel-info">
        <div class="panel-heading">
          <div class="row">
            <div class="col-xs-3">
              <i class="fa fa-newspaper-o fa-5x"></i>
            </div>
            <div class="col-xs-9 text-right">
              <div class="huge"><?php echo count($ary_received) + count($ary_downloaded) + count($ary_misal_uploaded); ?></div>
              <div></div>
            </div>
          </div>
        </div>
        <a href="#">
          <div class="panel-footer">
            <span class="pull-left">Total Araiz</span>
            <span class="pull-right"><i class="fa fa-newspaper-o"></i></span>
            <div class="clearfix"></div>
          </div>
        </a>
      </div>
    </div>
  </div>

  <!-- List of Edu Committee -->
  <div class="row">
    <div class="col-md-12">
      <table class="table table-hover table-condensed tablesorter">
        <thead>
          <tr>
            <th>Sr</th>
            <th>Tanzeem</th>
            <th>Is Hub</th>
            <!--th>Downloaded</th>
            <th>Misaal Uploaded</th-->
          </tr>
        </thead>
        <tbody>
          <tr class="<?php echo @$tr_class ?>">
            <td></td><td colspan="4"><a name="received"><h3>List of Araiz Received and pending for Download</h3></a></td>
          </tr>
          <?php
          if ($list_araz_cities) {
            $i = 0;
            $total_downloaded = 0;
            $total_uploaded = 0;
            $total_araz = count($list_araz_cities);

            foreach ($ary_received as $city) {
              $i++;
              $tr_class = 'alert-warning';
              if ($city['downloaded'] == 1) {
                $downloaded = 'Yes';
                $tr_class = 'alert-info';
                $total_downloaded++;
              } else {
                $downloaded = 'No';
              }
              if ($city['misaal'] != '') {
                $misaal_uploaded = 'Yes';
                $tr_class = 'alert-success';
                $total_uploaded++;
              } else {
                $misaal_uploaded = 'No';
              }
              $is_hub = ($city['hub_name'] == $city['jamat_name']) ? 'Yes' : 'No';
              ?>
              <tr class="<?php echo $tr_class ?>">
                <td><?php echo $i; ?></td>
                <td><a href="<?php echo SERVER_PATH ?>admin/ho_list_edu_comm_members.php?city=<?php echo $city['tanzeem_id']; ?>" target="_blank"><?php echo $city['jamat_name']; ?></a></td>
                <td><?php echo $is_hub ?></td>
                <!--td><?php echo $downloaded ?></td>
                <td><?php echo $misaal_uploaded; ?></td-->
              </tr>
              <?php
            }
            ?>
          <tfoot>
          <tr>
            <th>Sr</th>
            <th>Tanzeem</th>
            <th>Is Hub</th>
            <!--th>Downloaded</th>
            <th>Misaal Uploaded</th-->
          </tr>
        </tfoot>
        </tbody>
      </table>
      
      <table class="table table-hover table-condensed tablesorter">
        <thead>
          <tr>
            <th>Sr</th>
            <th>Tanzeem</th>
            <th>Is Hub</th>
            <!--th>Downloaded</th>
            <th>Misaal Uploaded</th-->
          </tr>
        </thead>
        <tbody>
          <tr class="<?php echo $tr_class ?>">
            <td></td><td colspan="4"><a name="downloaded"><h3>List of Araiz Downloaded for misal</h3></a></td>
          </tr>
            <?php
            foreach ($ary_downloaded as $city) {
              $i++;
              $tr_class = 'alert-warning';
              if ($city['downloaded'] == 1) {
                $downloaded = 'Yes';
                $tr_class = 'alert-info';
                $total_downloaded++;
              } else {
                $downloaded = 'No';
              }
              if ($city['misaal'] != '') {
                $misaal_uploaded = 'Yes';
                $tr_class = 'alert-success';
                $total_uploaded++;
              } else {
                $misaal_uploaded = 'No';
              }
              $is_hub = ($city['hub_name'] == $city['jamat_name']) ? 'Yes' : 'No';
              ?>
              <tr class="<?php echo $tr_class ?>">
                <td><?php echo $i; ?></td>
                <td><a href="<?php echo SERVER_PATH ?>admin/ho_list_edu_comm_members.php?city=<?php echo $city['tanzeem_id']; ?>" target="_blank"><?php echo $city['jamat_name']; ?></a></td>
                <td><?php echo $is_hub ?></td>
                <!--td><?php echo $downloaded ?></td>
                <td><?php echo $misaal_uploaded; ?></td-->
              </tr>
              <?php
            }
            ?>
        <tfoot>
          <tr>
            <th>Sr</th>
            <th>Tanzeem</th>
            <th>Is Hub</th>
            <!--th>Downloaded</th>
            <th>Misaal Uploaded</th-->
          </tr>
        </tfoot>
        </tbody>
      </table>
  
      
      <table class="table table-hover table-condensed tablesorter">
        <thead>
          <tr>
            <th>Sr</th>
            <th>Tanzeem</th>
            <th>Is Hub</th>
            <!--th>Downloaded</th>
            <th>Misaal Uploaded</th-->
          </tr>
        </thead>
        <tbody>
          <tr class="<?php echo $tr_class ?>">
            <td></td><td colspan="4"><a name="misal_uploaded"><h3>List of Araiz Misaal Uploaded</h3></a></td>
          </tr>
            <?php
            foreach ($ary_misal_uploaded as $city) {
              $i++;
              $tr_class = 'alert-warning';
              if ($city['downloaded'] == 1) {
                $downloaded = 'Yes';
                $tr_class = 'alert-info';
                $total_downloaded++;
              } else {
                $downloaded = 'No';
              }
              if ($city['misaal'] != '') {
                $misaal_uploaded = 'Yes';
                $tr_class = 'alert-success';
                $total_uploaded++;
              } else {
                $misaal_uploaded = 'No';
              }
              $is_hub = ($city['hub_name'] == $city['jamat_name']) ? 'Yes' : 'No';
              ?>
              <tr class="<?php echo $tr_class ?>">
                <td><?php echo $i; ?></td>
                <td><a href="<?php echo SERVER_PATH ?>admin/ho_list_edu_comm_members.php?city=<?php echo $city['tanzeem_id']; ?>" target="_blank"><?php echo $city['jamat_name']; ?></a></td>
                <td><?php echo $is_hub ?></td>
                <!--td><?php echo $downloaded ?></td>
                <td><?php echo $misaal_uploaded; ?></td-->
              </tr>
              <?php
            }
          } else {
            ?>
            <tr>
              <td colspan="4">No Records Found.</td>
            </tr>
            <?php
          }
          ?>
        <tfoot>
          <tr>
            <th>Sr</th>
            <th>Tanzeem</th>
            <th>Is Hub</th>
            <!--th>Downloaded</th>
            <th>Misaal Uploaded</th-->
          </tr>
        </tfoot>
        </tbody>
      </table>
    </div><!-- ./ md-12 -->
  </div><!-- ./ row -->
  <!-- ./List of Edu Committee -->
<!-- /#page-wrapper -->

<script id="js">$(function() {

        // NOTE: $.tablesorter.theme.bootstrap is ALREADY INCLUDED in the jquery.tablesorter.widgets.js
        // file; it is included here to show how you can modify the default classes
        $.tablesorter.themes.bootstrap = {
            // these classes are added to the table. To see other table classes available,
            // look here: http://getbootstrap.com/css/#tables
            table      : 'table table-striped table-condensed',
        };

        // call the tablesorter plugin and apply the uitheme widget
        $("table").tablesorter({
            // this will apply the bootstrap theme if "uitheme" widget is included
            // the widgetOptions.uitheme is no longer required to be set
            theme : "bootstrap",

            widthFixed: true,

            headerTemplate : '{content} {icon}', // new in v2.7. Needed to add the bootstrap icon!

            // widget code contained in the jquery.tablesorter.widgets.js file
            // use the zebra stripe widget if you plan on hiding any rows (filter widget)
            widgets : [ "uitheme", "filter", "zebra" ],

            widgetOptions : {
                // using the default zebra striping class name, so it actually isn't included in the theme variable above
                // this is ONLY needed for bootstrap theming if you are using the filter widget, because rows are hidden
                zebra : ["even", "odd"],

                // reset filters button
                filter_reset : ".reset"

                // set the uitheme widget to use the bootstrap theme class names
                // this is no longer required, if theme is set
                // ,uitheme : "bootstrap"

            }
        })
    });
 </script>

<?php include "./footer.php"; ?>
