<?php
require_once 'classes/class.user.admin.php';
include 'classes/class.araiz.php';
$allowed_roles = array(ROLE_HEAD_OFFICE,ROLE_AAMIL_SAHEB);
require_once 'session.php';

$araiz = new Araiz();

$id = FALSE;
$select = FALSE;

$from_date = $_GET['from_date'];
$to_date = $_GET['to_date'];

$title = 'Araiz List';
$description = '';
$keywords = '';
$active_page = "manage_araiz";

include ('print_header.php');
?>
<body style="padding: 10px;">
  <div>
    <p style="display: block; text-align: right"><?php echo date('d-m-Y H:i:s'); ?></p>
  </div>
  <div class="row text-center">
    <img src="images/Logo-tlb-araz.jpg" height="100" >
  </div>
  
  <div class="row">
    <div class="col-lg-12">
      <h3 class="page-header font18">List of Araiz from Date : <?php echo $from_date; ?> to <?php echo $to_date; ?></h3>
    </div>
    <!-- /.col-lg-12 -->
  </div>
  
  <div class="row">
    <div class="col-md-12">
      <table class="table table-bordered table-condensed table-responsive dontSplit" dir="rtl">
        <tr>
          <th>Track No.</th>
          <th>&nbsp;</th>
          <th>ملاحظة</th>
        </tr>
        <tbody>
          <?php
          $i=0;
          $select = $araiz->print_all_araz_and_user_data_by_date_wise($from_date,$to_date);
          foreach ($select as $data) {
            
            $araz_courses = $araiz->get_all_araz_course_data($data['id']);
            if(count($araz_courses) > 1)
            { $araz_type = 'Istirshad';
              $i++;
              $student_name = $data['full_name_ar'];
              $ITS_city = $data['city'];
              $track_no = $data['id'];
              $age = $araiz->ageCalculator($data['dob']);
              
          ?>
          <tr>
            <td><?php echo $track_no; ?></td>
            <td>
              <div class="row">
                <div class="col-md-12">
                  <div class="col-xs-3 border">
                    <span class="alfatemi-text font18"><strong>موضع:</strong><?php echo $ITS_city; ?></span>
                  </div>
                  <div class="col-xs-9" dir="rtl">
                    <span class="alfatemi-text font18"><strong>الاسم:</strong><?php echo $student_name; ?></span>
                  </div>
                </div>
                <div class="col-md-12 text-center">
                  <span class="alfatemi-text font18"> مزيد تعليم واسطسس استرشادا عرض؛</span>
                </div>
              </div>
              <table class="table table-bordered table-condensed" dir="ltr">
                <thead>
                  <tr>
                    <th>Institution name</th>
                    <th>Place</th>
                    <th>Type of course</th>
                    <th>Duration</th>
                    <th>Accommodation</th>
                    <th>Age</th>
                  </tr>
                </thead>
                <tbody>
                  <?php foreach ($araz_courses as $ac) { ?>
                  <tr>
                    <td><?php echo $ac['institute_name']; ?></td>
                    <td><?php echo $ac['institute_city']; ?></td>
                    <td><?php echo $ac['course_name']; ?></td>
                    <td><?php echo $ac['course_duration']; ?></td>
                    <td><?php echo $ac['accomodation']; ?></td>
                    <td><?php echo $age ?></td>
                  </tr>
                  <?php } ?>
                </tbody>
              </table>
              <div class="col-md-12 text-center" dir="rtl">
                <span class="alfatemi-text font18">رزا انسس دعاء مبارك فضل فرما وا ادبًا عرض ؛،</span>
              </div>
            </td>
            <td></td>
          </tr>
          <?php 
              } 
            } 
          ?>
        </tbody>
      </table>
    </div>
  </div>
</body>
</html>