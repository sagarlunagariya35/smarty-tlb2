<?php
require_once '../classes/class.database.php';
include 'classes/class.ohbat_sentences.php';
$allowed_roles = array(ROLE_HEAD_OFFICE);
require_once 'session.php';

$title = 'Miqaat Daily Sentences';
$description = '';
$keywords = '';
$active_page = "ohbat_sentences_edit";
$art = new Mtx_Ohbat_Sentences;

$id = (isset($_GET['id'])) ? $_GET['id'] : 0;

//Arabic
if (isset($_REQUEST['delete_ar_bayan_image']) || (@$_REQUEST['delete_ar_ques_image']) || (@$_REQUEST['delete_ar_bayan_title_image']) || (@$_REQUEST['delete_ar_word_image'])) {

  if ($_REQUEST['delete_ar_bayan_image']) {
    $id = $_REQUEST['delete_ar_bayan_image'];
    $field = 'ar_bayan_image';
    $Msg = 'Arabic Bayan Image';
  }

  if ($_REQUEST['delete_ar_ques_image']) {
    $id = $_REQUEST['delete_ar_ques_image'];
    $field = 'ar_ques_image';
    $Msg = 'Arabic Question Image';
  }

  if ($_REQUEST['delete_ar_bayan_title_image']) {
    $id = $_REQUEST['delete_ar_bayan_title_image'];
    $field = 'ar_bayan_title_image';
    $Msg = 'Arabic Bayan Title Image';
  }

  if ($_REQUEST['delete_ar_word_image']) {
    $id = $_REQUEST['delete_ar_word_image'];
    $field = 'ar_word_image';
    $Msg = 'Arabic Word Image';
  }

  $i_rslt = $art->delete_ohbat_image($id, $field);
  if ($i_rslt) {
    $_SESSION[SUCCESS_MESSAGE] = $Msg . ' is deleted Successfully';
  } else {
    $_SESSION[ERROR_MESSAGE] = 'Erors encountered while processing the request';
  }

  header('Location: edit_ohbat_sentence.php?id=' . $id);
  exit();
}

//English
if (isset($_REQUEST['delete_en_bayan_image']) || (@$_REQUEST['delete_en_ques_image']) || (@$_REQUEST['delete_en_bayan_title_image']) || (@$_REQUEST['delete_en_word_image'])) {

  if ($_REQUEST['delete_en_bayan_image']) {
    $id = $_REQUEST['delete_en_bayan_image'];
    $field = 'en_bayan_image';
    $Msg = 'English Bayan Image';
  }

  if ($_REQUEST['delete_en_ques_image']) {
    $id = $_REQUEST['delete_en_ques_image'];
    $field = 'en_ques_image';
    $Msg = 'English Question Image';
  }

  if ($_REQUEST['delete_en_bayan_title_image']) {
    $id = $_REQUEST['delete_en_bayan_title_image'];
    $field = 'en_bayan_title_image';
    $Msg = 'English Bayan Title Image';
  }

  if ($_REQUEST['delete_en_word_image']) {
    $id = $_REQUEST['delete_en_word_image'];
    $field = 'en_word_image';
    $Msg = 'English Word Image';
  }

  $i_rslt = $art->delete_ohbat_image($id, $field);
  if ($i_rslt) {
    $_SESSION[SUCCESS_MESSAGE] = $Msg . ' is deleted Successfully';
  } else {
    $_SESSION[ERROR_MESSAGE] = 'Erors encountered while processing the request';
  }

  header('Location: edit_ohbat_sentence.php?id=' . $id);
  exit();
}

if (isset($_POST['btn_submit'])) {
  $data = $db->clean_data($_POST);
  // get variables
  //Arabic
  $title_ar = $data['title'];
  $question = addslashes($_POST['question']);
  $bayan_title = $data['bayan_title'];
  $bayan = addslashes($_POST['bayan']);
  $words = addslashes($_POST['words']);
  
  //English
  $title_eng = $data['title_eng'];
  $slug = $data['slug'];
  $question_eng = addslashes($_POST['question_eng']);
  $en_bayan_title = $data['en_bayan_title'];
  $bayan_eng = addslashes($_POST['bayan_eng']);
  $words_eng = addslashes($_POST['words_eng']);
  
  //Other
  $miqat_id = $data['miqat_id'];
  $event_time = $data['event_time'];
  $sr = $data['sr'];
  $active = $data['active'];
  $ohbat_id = $data['ohbat_id'];
  
  // Arabic Images
  $ar_question_image = $_FILES['ar_question_image']['name'];
  
  if($ar_question_image == ''){
    $ar_ques_file_name = $data['last_ar_ques_image'];
  }else {
    $ar_ques_file_name = $ohbat_id.'-'.$ar_question_image;
  }
  
  $ar_question_image_temp = $_FILES['ar_question_image']['tmp_name'];
  $arquespathANDname = "../upload/ohbat_upload/" . $ar_ques_file_name;
  move_uploaded_file($ar_question_image_temp, $arquespathANDname);
  
  $ar_bayan_title_image = $_FILES['ar_bayan_title_image']['name'];
  
  if($ar_bayan_title_image == ''){
    $ar_bayan_title_file_name = $data['last_ar_bayan_title_image'];
  }else {
    $ar_bayan_title_file_name = $ohbat_id.'-'.$ar_bayan_title_image;
  }
  
  $ar_bayan_title_image_temp = $_FILES['ar_bayan_title_image']['tmp_name'];
  $arbayantitlepathANDname = "../upload/ohbat_upload/" . $ar_bayan_title_file_name;
  move_uploaded_file($ar_bayan_title_image_temp, $arbayantitlepathANDname);
  
  $ar_bayan_image = $_FILES['ar_bayan_image']['name'];
  
  if($ar_bayan_image == ''){
    $ar_file_name = $data['last_ar_bayan_image'];
  }else {
    $ar_file_name = $ohbat_id.'-'.$ar_bayan_image;
  }
    
  $ar_bayan_image_temp = $_FILES['ar_bayan_image']['tmp_name'];
  $arbayanpathANDname = "../upload/ohbat_upload/" . $ar_file_name;
  move_uploaded_file($ar_bayan_image_temp, $arbayanpathANDname);
  
  $ar_word_image = $_FILES['ar_word_image']['name'];
  
  if($ar_word_image == ''){
    $ar_word_file_name = $data['last_ar_word_image'];
  }else {
    $ar_word_file_name = $ohbat_id.'-'.$ar_word_image;
  }
    
  $ar_word_image_temp = $_FILES['ar_word_image']['tmp_name'];
  $arwordpathANDname = "../upload/ohbat_upload/" . $ar_word_file_name;
  move_uploaded_file($ar_word_image_temp, $arwordpathANDname);
  
  
  // English Images
  $en_question_image = $_FILES['en_question_image']['name'];
  
  if($en_question_image == ''){
    $en_ques_file_name = $data['last_en_ques_image'];
  }else {
    $en_ques_file_name = $ohbat_id.'-'.$en_question_image;
  }
  
  $en_question_image_temp = $_FILES['en_question_image']['tmp_name'];
  $enquespathANDname = "../upload/ohbat_upload/" . $en_ques_file_name;
  move_uploaded_file($en_question_image_temp, $enquespathANDname);
  
  $en_bayan_title_image = $_FILES['en_bayan_title_image']['name'];
  
  if($en_bayan_title_image == ''){
    $en_bayan_title_file_name = $data['last_en_bayan_title_image'];
  }else {
    $en_bayan_title_file_name = $ohbat_id.'-'.$en_bayan_title_image;
  }
    
  $en_bayan_title_image_temp = $_FILES['en_bayan_title_image']['tmp_name'];
  $enbayantitlepathANDname = "../upload/ohbat_upload/" . $en_bayan_title_file_name;
  move_uploaded_file($en_bayan_title_image_temp, $enbayantitlepathANDname);
  
  $en_bayan_image = $_FILES['en_bayan_image']['name'];
  
  if($en_bayan_image == ''){
    $en_file_name = $data['last_en_bayan_image'];
  }else {
    $en_file_name = $ohbat_id.'-'.$en_bayan_image;
  }
  
  $en_bayan_image_temp = $_FILES['en_bayan_image']['tmp_name'];
  $enbayanpathANDname = "../upload/ohbat_upload/" . $en_file_name;
  move_uploaded_file($en_bayan_image_temp, $enbayanpathANDname);
  
  $en_word_image = $_FILES['en_word_image']['name'];
  
  if($en_word_image == ''){
    $en_word_file_name = $data['last_en_word_image'];
  }else {
    $en_word_file_name = $ohbat_id.'-'.$en_word_image;
  }
  
  $en_word_image_temp = $_FILES['en_word_image']['tmp_name'];
  $enwordpathANDname = "../upload/ohbat_upload/" . $en_word_file_name;
  move_uploaded_file($en_word_image_temp, $enwordpathANDname);
  

  $art_insert = $art->edit_article($ohbat_id, $miqat_id, $event_time, $sr, $slug, $title_ar, $title_eng, $bayan_title, $ar_bayan_title_file_name, $bayan, $ar_file_name, $question, $ar_ques_file_name, $active, $words, $ar_word_file_name, $question_eng, $en_ques_file_name, $en_bayan_title, $en_bayan_title_file_name, $bayan_eng, $en_file_name, $words_eng, $en_word_file_name);

  if ($art_insert) {
    $_SESSION[SUCCESS_MESSAGE] = 'Sentence Edited Successfully.';
  } else {
    $_SESSION[ERROR_MESSAGE] = 'Error encountered while Editing Data';
  }
}

$ohbat_sentence = $art->get_article_by_id($id);
$miqaat_title = $art->get_miqaat_title_by_miqaat_id($ohbat_sentence['miqaat_id']);

include_once("header.php");
?>
<script type="text/javascript" src="ckeditor/ckeditor.js"></script>

<div class="row">
  <div class="col-lg-12">
    <h1 class="page-header"><?php echo $_SESSION[USER_ID]; ?>Edit Ohbat Sentence <?php //echo $_SESSION['user_its'];        ?></h1>
  </div>
  <!-- /.col-lg-12 -->
</div>

<div class="row">
  <div class="col-md-12">
    <form method="post" enctype="multipart/form-data">

      <div class="form-group col-md-4 col-xs-12">
        <label for="title">Sentence Arabic Title :</label>
        <input type="text" name="title" id="title" required="required" class="form-control" value="<?php echo $ohbat_sentence['title']; ?>">
      </div>

      <div class="form-group col-md-4 col-xs-12">
        <label for="title">Sentence English Title :</label>
        <input type="text" name="title_eng" id="title" required="required" class="form-control" value="<?php echo $ohbat_sentence['title_eng']; ?>">
      </div>

      <div class="form-group col-md-4 col-xs-12">
        <label for="slug">Sentence slug :</label>
        <input type="text" name="slug" id="slug" required="required" class="form-control" value="<?php echo $ohbat_sentence['slug']; ?>">
      </div>

      <div class="form-group col-md-4 col-xs-12">
        <label for="slug">Event Time :</label>
        <select name="event_time" id="event_time" class="form-control" required >
          <option value="">Select one</option>
          <option value="pre" <?php if ($ohbat_sentence['event_time'] == 'pre') echo 'selected'; ?>>Pre</option>
          <option value="in" <?php if ($ohbat_sentence['event_time'] == 'in') echo 'selected'; ?>>In</option>
          <option value="post" <?php if ($ohbat_sentence['event_time'] == 'post') echo 'selected'; ?>>Post</option>
        </select>
      </div>

      <div class="form-group col-md-4 col-xs-12">
        <label for="year">Select Year :</label>
        <select name="year" id="year" class="form-control" required >
          <option value="">Select Year</option>
          <option value="1435" <?php if ($miqaat_title['year'] == '1435') echo 'selected'; ?>>1435</option>
          <option value="1436" <?php if ($miqaat_title['year'] == '1436') echo 'selected'; ?>>1436</option>
          <option value="1437" <?php if ($miqaat_title['year'] == '1437') echo 'selected'; ?>>1437</option>
        </select>
      </div>

      <div class="form-group col-md-4 col-xs-12">
        <label for="miqat">Select Miqat :</label>
        <select name="miqat_id" id="miqat" class="form-control" required >
          <?php if ($ohbat_sentence['miqaat_id'] != '') { ?>
            <option value="<?php echo $miqaat_title['id']; ?>"><?php echo $miqaat_title['miqaat_title']; ?></option>
          <?php } else { ?>
            <option value="">Select Miqaat</option>
          <?php } ?>
        </select>
      </div>

      <div class="form-group col-md-3 col-xs-12">
        <label for="sr">Serial :</label>
        <input type="text" name="sr" id="sr" class="form-control" value="<?php echo $ohbat_sentence['sr']; ?>">
      </div>

      <div class="form-group col-md-3 col-xs-12">
        <label for="active">Is Active? :</label>
        <select name="active" id="active" class="form-control" required >
          <option value="1" <?php if ($ohbat_sentence['active'] == '1') echo 'selected'; ?>>Yes</option>
          <option value="0" <?php if ($ohbat_sentence['active'] == '0') echo 'selected'; ?>>No</option>
        </select>
      </div>
      <div class="clearfix"></div>

      <div class="form-group col-md-6 col-xs-12">
        <label for="miqaat_logo">Arabic Question Image :</label>
        <input type="file" name="ar_question_image" class="file">

        <?php if ($ohbat_sentence['ar_ques_image'] != '') { ?>
          <br>
          <img src="<?php echo SERVER_PATH . 'upload/ohbat_upload/' . $ohbat_sentence['ar_ques_image']; ?>" height="100" width="100">
          <a href="edit_ohbat_sentence.php?id=<?php echo $id; ?>&delete_ar_ques_image=<?php echo $id; ?>"><img src="images/remove.png" alt="" style="height: 25px;vertical-align: auto;margin-right: 10px;"></a>
        <?php } ?>
        <input type="hidden" name="last_ar_ques_image" value="<?php echo $ohbat_sentence['ar_ques_image']; ?>">
      </div>

      <div class="form-group col-md-12" id="bayan">
        <label>Arabic Question : </label>
        <textarea name="question" class="ckeditor" class="form-control"><?php echo $ohbat_sentence['question']; ?></textarea>
      </div><!----CONTENT_BOX-->

      <div class="form-group col-md-6 col-xs-12">
        <label for="miqaat_logo">Arabic Bayan Title Image :</label>
        <input type="file" name="ar_bayan_title_image" class="file">

        <?php if ($ohbat_sentence['ar_bayan_title_image'] != '') { ?>
          <br>
          <img src="<?php echo SERVER_PATH . 'upload/ohbat_upload/' . $ohbat_sentence['ar_bayan_title_image']; ?>" height="100" width="100">
          <a href="edit_ohbat_sentence.php?id=<?php echo $id; ?>&delete_ar_bayan_title_image=<?php echo $id; ?>"><img src="images/remove.png" alt="" style="height: 25px;vertical-align: auto;margin-right: 10px;"></a>
        <?php } ?>
        <input type="hidden" name="last_ar_bayan_title_image" value="<?php echo $ohbat_sentence['ar_bayan_title_image']; ?>">
      </div>

      <div class="form-group col-md-6 col-xs-12">
        <label for="title">Arabic Bayan Title :</label>
        <input type="text" name="bayan_title" id="bayan_title" class="form-control" value="<?php echo $ohbat_sentence['bayan_title'] ?>">
      </div>
      <div class="clearfix"></div>

      <div class="form-group col-md-6 col-xs-12">
        <label for="miqaat_logo">Arabic Bayan Image :</label>
        <input type="file" name="ar_bayan_image" class="file">

        <?php if ($ohbat_sentence['ar_bayan_image'] != '') { ?>
          <br>
          <img src="<?php echo SERVER_PATH . 'upload/ohbat_upload/' . $ohbat_sentence['ar_bayan_image']; ?>" height="100" width="100">
          <a href="edit_ohbat_sentence.php?id=<?php echo $id; ?>&delete_ar_bayan_image=<?php echo $id; ?>"><img src="images/remove.png" alt="" style="height: 25px;vertical-align: auto;margin-right: 10px;"></a>
        <?php } ?>
        <input type="hidden" name="last_ar_bayan_image" value="<?php echo $ohbat_sentence['ar_bayan_image']; ?>">
      </div>

      <div class="form-group col-md-12" id="bayan">
        <label>Arabic Bayan : </label>
        <textarea name="bayan" class="ckeditor" class="form-control"><?php echo $ohbat_sentence['bayan']; ?></textarea>
      </div><!----CONTENT_BOX-->

      <div class="form-group col-md-6 col-xs-12">
        <label for="miqaat_logo">Arabic Word Image :</label>
        <input type="file" name="ar_word_image" class="file">

        <?php if ($ohbat_sentence['ar_word_image'] != '') { ?>
          <br>
          <img src="<?php echo SERVER_PATH . 'upload/ohbat_upload/' . $ohbat_sentence['ar_word_image']; ?>" height="100" width="100">
          <a href="edit_ohbat_sentence.php?id=<?php echo $id; ?>&delete_ar_word_image=<?php echo $id; ?>"><img src="images/remove.png" alt="" style="height: 25px;vertical-align: auto;margin-right: 10px;"></a>
        <?php } ?>
        <input type="hidden" name="last_ar_word_image" value="<?php echo $ohbat_sentence['ar_word_image']; ?>">
      </div>

      <div class="form-group col-md-12" id="bayan">
        <label>Arabic Words : </label>
        <textarea name="words" class="ckeditor" class="form-control"><?php echo $ohbat_sentence['word_meaning']; ?></textarea>
      </div><!----CONTENT_BOX-->

      <div class="form-group col-md-6 col-xs-12">
        <label for="miqaat_logo">English Question Image :</label>
        <input type="file" name="en_question_image" class="file">

        <?php if ($ohbat_sentence['en_ques_image'] != '') { ?>
          <br>
          <img src="<?php echo SERVER_PATH . 'upload/ohbat_upload/' . $ohbat_sentence['en_ques_image']; ?>" height="100" width="100">
          <a href="edit_ohbat_sentence.php?id=<?php echo $id; ?>&delete_en_ques_image=<?php echo $id; ?>"><img src="images/remove.png" alt="" style="height: 25px;vertical-align: auto;margin-right: 10px;"></a>
        <?php } ?>
        <input type="hidden" name="last_en_ques_image" value="<?php echo $ohbat_sentence['en_ques_image']; ?>">
      </div>

      <div class="form-group col-md-12" id="bayan">
        <label>English Question : </label>
        <textarea name="question_eng" class="ckeditor" class="form-control"><?php echo $ohbat_sentence['question_eng']; ?></textarea>
      </div><!----CONTENT_BOX-->

      <div class="form-group col-md-6 col-xs-12">
        <label for="miqaat_logo">English Bayan Title Image :</label>
        <input type="file" name="en_bayan_title_image" class="file">

        <?php if ($ohbat_sentence['en_bayan_title_image'] != '') { ?>
          <br>
          <img src="<?php echo SERVER_PATH . 'upload/ohbat_upload/' . $ohbat_sentence['en_bayan_title_image']; ?>" height="100" width="100">
          <a href="edit_ohbat_sentence.php?id=<?php echo $id; ?>&delete_en_bayan_title_image=<?php echo $id; ?>"><img src="images/remove.png" alt="" style="height: 25px;vertical-align: auto;margin-right: 10px;"></a>
        <?php } ?>
        <input type="hidden" name="last_en_bayan_title_image" value="<?php echo $ohbat_sentence['en_bayan_title_image']; ?>">
      </div>

      <div class="form-group col-md-4 col-xs-12">
        <label for="title">English Bayan Title :</label>
        <input type="text" name="en_bayan_title" id="bayan_title" class="form-control" value="<?php echo $ohbat_sentence['en_bayan_title']; ?>">
      </div>
      <div class="clearfix"></div>

      <div class="form-group col-md-6 col-xs-12">
        <label for="miqaat_logo">English Bayan Image :</label>
        <input type="file" name="en_bayan_image" class="file">

        <?php if ($ohbat_sentence['en_bayan_image'] != '') { ?>
          <br>
          <img src="<?php echo SERVER_PATH . 'upload/ohbat_upload/' . $ohbat_sentence['en_bayan_image']; ?>" height="100" width="100">
          <a href="edit_ohbat_sentence.php?id=<?php echo $id; ?>&delete_en_bayan_image=<?php echo $id; ?>"><img src="images/remove.png" alt="" style="height: 25px;vertical-align: auto;margin-right: 10px;"></a>
        <?php } ?>
        <input type="hidden" name="last_en_bayan_image" value="<?php echo $ohbat_sentence['en_bayan_image']; ?>">
      </div>

      <div class="form-group col-md-12" id="bayan">
        <label>English Bayan : </label>
        <textarea name="bayan_eng" class="ckeditor" class="form-control"><?php echo $ohbat_sentence['bayan_eng']; ?></textarea>
      </div><!----CONTENT_BOX-->

      <div class="form-group col-md-6 col-xs-12">
        <label for="miqaat_logo">English Word Image :</label>
        <input type="file" name="en_word_image" class="file">

        <?php if ($ohbat_sentence['en_word_image'] != '') { ?>
          <br>
          <img src="<?php echo SERVER_PATH . 'upload/ohbat_upload/' . $ohbat_sentence['en_word_image']; ?>" height="100" width="100">
          <a href="edit_ohbat_sentence.php?id=<?php echo $id; ?>&delete_en_word_image=<?php echo $id; ?>"><img src="images/remove.png" alt="" style="height: 25px;vertical-align: auto;margin-right: 10px;"></a>
        <?php } ?>
        <input type="hidden" name="last_en_word_image" value="<?php echo $ohbat_sentence['en_word_image']; ?>">
      </div>

      <div class="form-group col-md-12" id="bayan">
        <label>English Words : </label>
        <textarea name="words_eng" class="ckeditor" class="form-control"><?php echo $ohbat_sentence['word_meaning_eng']; ?></textarea>
      </div><!----CONTENT_BOX-->

      <div class="clearfix"></div>
      <p>&nbsp;</p>
      <div class="form-group col-md-6 col-xs-12">
        <input type="hidden" name="ohbat_id" value="<?php echo $id; ?>">
        <input type="submit" name="btn_submit" id="btn_submit" value="Submit" class="btn btn-primary btn-block form-control">
      </div>
    </form>

  </div>
</div>

<script type="text/javascript">

  $('#year').on('change', function(e) {
    $('#miqat').empty();
    var dropDown = document.getElementById("year");
    var year_val = dropDown.options[dropDown.selectedIndex].value;
    $.ajax({
      type: "POST",
      url: "ajax.php",
      data: {'cmd': 'get_miqat_list', 'year': year_val},
      success: function(data) {
        // Parse the returned json data
        var opts = $.parseJSON(data);
        // Use jQuery's each to iterate over the opts value
        $.each(opts, function(i, d) {
          // You will need to alter the below to get the right values from your json object.  Guessing that d.id / d.modelName are columns in your carModels data
          $('#miqat').append('<option value="' + d.id + '">' + d.miqaat_title + '</option>');
        });
      }
    });
  });

  function show_miqaat()
  {
    $('#miqat').empty()
    var dropDown = document.getElementById("year");
    var year_val = dropDown.options[dropDown.selectedIndex].value;
    $.ajax({
      type: "POST",
      url: "/ajax.php",
      data: {'cmd': 'get_miqat_list', 'year': year_val},
      success: function(data) {
        // Parse the returned json data
        var opts = $.parseJSON(data);
        alert(data);
        // Use jQuery's each to iterate over the opts value
        $.each(opts, function(i, d) {
          // You will need to alter the below to get the right values from your json object.  Guessing that d.id / d.modelName are columns in your carModels data
          $('#miqat').append('<option value="' + d.miqat_id + '">' + d.miqat_name + '</option>');
        });
      }
    });




    if (str == "")
    {
      document.getElementById("sub_menu").innerHTML = "";
      document.getElementById("mname").innerHTML = "";
      return;
    }

    if (window.XMLHttpRequest)
    {// code for IE7+, Firefox, Chrome, Opera, Safari
      xmlhttp = new XMLHttpRequest();
    }
    else
    {// code for IE6, IE5
      xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
    }



    xmlhttp.onreadystatechange = function()
    {
      if (xmlhttp.readyState == 4 && xmlhttp.status == 200)
      {
        document.getElementById("sub_menu").innerHTML = xmlhttp.responseText;
      }
    }
    xmlhttp.open("GET", "get_data_by_ajax.php?menu=" + str, true);
    xmlhttp.send();
  }
</script>

<script type="text/javascript" src="ckeditor/ckeditor.js"></script>

<!-- /#page-wrapper -->
<?php include "./footer.php"; ?>
