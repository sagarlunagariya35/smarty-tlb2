<?php
require_once 'classes/class.user.admin.php';
include 'classes/class.addstate.php';
include 'classes/class.addcountry.php';
$allowed_roles = array(ROLE_HEAD_OFFICE);
require_once 'session.php';

$title = 'state';
$description = '';
$keywords = '';
$active_page = "manage_states";


$country = new Country();
$state = new State();
$id = FALSE;
$select = FALSE;

if (isset($_GET['delete_id'])) {
  global $db;

  $delete_id = $_GET['delete_id'];
  $delete_user = "delete from states where id = '$delete_id'";
  $result = $db->query($delete_user);

  if ($result) {
    $_SESSION[SUCCESS_MESSAGE] = "state Successfully Delete";
  } else {
    $_SESSION[ERROR_MESSAGE] = "Error In Delete";
  }
}

if (isset($_GET['id'])) {
  $select = $state->state_result(false, false,$_GET['id']);
//print_r($select);
  $id = $_GET['id'];
}

if (isset($_POST['Update'])) {
  $id = $_GET['id'];
  $country = $_POST['country'];
  $state = $_POST['statename'];

  $state_update = $state->update_state($country, $state, $id);

  if ($state_update)
    $_SESSION[SUCCESS_MESSAGE] = "state Successfully Update";
  if (!$state_update)
    $_SESSION[ERROR_MESSAGE] = "Error In Update Data";
}

if (isset($_POST['Save'])) {

  $country = $_POST['country'];
  $state = $_POST['statename'];

  $state_insert = $state->insert_state($country, $state);
  if ($state_insert)
    $_SESSION[SUCCESS_MESSAGE] = "state Successfully Insert";
  if (!$state_insert)
    $_SESSION[ERROR_MESSAGE] = "Error In Inserting Data";
}

$heading = $id ? 'Update State' : $heading = 'Add New State';
$btn_name = $id ? 'Update' : 'Add New';

include ('header.php');
?>
  <div class="row">

    <div class="col-lg-4 col-xs-12 col-sm-12 pull-right">
      <div class="col-lg-12">
        <h3 class="page-header"><?php echo $heading; ?></h3>
      </div>
      <!-- /.col-lg-12 -->

      <form method="post"  action="addstate.php" role="form" class="form-horizontal" >
        <div class="col-md-12">&nbsp;</div>

        <div class="form-group">
          <label class="control-label">Country:</label>
          <select name="country" class="form-control">
            <?php
            $select_place = $country->country_result1();
            foreach ($select_place as $data) {
              if ($data['id'] == $select[0]['country_id'])
                $check = 'selected';else {
                $check = '';
              }
              ?>
              <option value="<?php echo $data['id']; ?>" <?php echo $check; ?> > <?php echo $data['nicename']; ?> </option>
            <?php } ?>
          </select>
        </div>

        <div class="form-group">
          <label class="control-label">State:</label>
          <input type="text" class="form-control" name="statename"  placeholder="Add State" value="<?php echo $select[0]['name']; ?>" required>
        </div>

        <div class="form-group">
          <?php
          $btn_name = $id ? 'Update' : 'Save';
          ?>
          <button name="<?php echo $btn_name; ?>" class="btn btn-success btn-block"><?php echo $btn_name ?></button>
        </div>
      </form>
    </div>

    <div class="col-lg-8 col-xs-12 col-sm-12 pull-left">
      <div class="col-md-12">&nbsp;</div>
      <div class="clearfix"></div>
      <div class="panel panel-info">
        <div class="panel-heading">
          <i class="fa fa-group fa-fw"></i> States
        </div>
        <!-- /.panel-heading -->
        <div class="panel-body">
          <div class="table-responsive">
            <table class="table table-striped table-hover table-condensed">
              <thead>
              <th>No</th>
              <th>Country</th>
              <th>State</th>
              <th>Action</th>
              </thead>
              <tbody>
                <?php
                $select = $state->state_result($page, 10);
                $count_question = $state->count_state_record1();
                require_once 'pagination.php';
                $pagination = pagination(10, $page, 'addstate.php?page=', $count_question);

                $i = 1;
                foreach ($select as $data) {
                  ?>
                  <tr>
                    <td>
                      <?php echo $i++; ?>
                    </td>
                    <td>
                      <?php echo $data['country_iso3']; ?>
                    </td>
                    <td>
                      <?php echo $data['name']; ?>
                    </td>
                    <td>
                      <a href="addstate.php?id=<?php echo $data['id']; ?>"><i class="fa fa-edit fa-fw"></i></a>
                      <a href="#" class="confirm"><i class="fa fa-remove fa-fw text-danger"></i></a>
                    </td>
                  </tr>
                  <?php
                }
                ?>
              </tbody>
            </table>
          </div>
          <!-- /.table-responsive -->
        </div>
        <div class="panel-footer">
          <?php echo $pagination; ?>
          <div class="clearfix"></div>
        </div>
        <!-- /.panel-body -->
      </div>
    </div>

    <form method="post" id="frm-del-grp">
      <input type="hidden" name="id" id="del-state-val" value="">
      <input type="hidden" name="cmd" value="del-state">
    </form>
  </div>
  <script>
    $(".confirm").confirm({
      text: "Are you sure you want to delete?",
      title: "Confirmation required",
      confirm: function(button) {
        $('#del-state-val').val($(button).attr('id'));
        $('#frm-del-state').submit();
        alert('Are you Sure You want to delete: ' + id);
      },
      cancel: function(button) {
        // nothing to do
      },
      confirmButton: "Yes",
      cancelButton: "No",
      post: true,
      confirmButtonClass: "btn-danger"
    });
  </script>
<!-- /#page-wrapper -->

<!-- /#wrapper -->
<?php
include 'footer.php';
?>
