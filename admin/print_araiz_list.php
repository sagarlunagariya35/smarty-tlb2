<?php
require_once 'classes/class.user.admin.php';
include 'classes/class.araiz.php';
$allowed_roles = array(ROLE_HEAD_OFFICE,ROLE_AAMIL_SAHEB);
require_once 'session.php';

$araiz = new Araiz();

$id = FALSE;
$select = FALSE;
$track_id = FALSE;

if(isset($_POST['print']))
{
  $track_id = (array) $_POST['print'];
}
else if(isset($_GET['track_id'])){
  $track_id = (array) $_GET['track_id'];
}
else
{
  $track_id = $_SESSION['track_id'];
}

$title = 'Araiz List';
$description = '';
$keywords = '';
$active_page = "manage_araiz";

include ('print_header.php');
?>
<body style="padding: 10px;">
  <div>
    <p style="display: block; text-align: right"><?php echo date('d-m-Y H:i:s'); ?></p>
  </div>
  
  <div class="row">
    <div class="col-lg-12">
      <h3 class="page-header">List of Araiz</h3>
    </div>
    <!-- /.col-lg-12 -->
  </div>
  
  <div class="row">
    <div class="col-md-12">
      <table class="table table-responsive table-condensed">
        <thead>
          <th class="text-center">Track No</th>
          <th>ITS</th>
          <th>Name</th>
          <th>Jamaat</th>
          <th>Araz Type</th>
          <th>City</th>
        </thead>
        <tbody>
          <?php
          $select = $araiz->print_all_araz_and_user_data($track_id);
          foreach ($select as $data) {
            $araz_courses = $araiz->get_all_araz_course_data($data['id']);
            if(count($araz_courses) > 1)
            { $araz_type = 'Istirshad'; } else { $araz_type = 'Raza'; }
          ?>
          <tr><td colspan="6"></td></tr>
          <tr>
            <td class="text-center"><?php echo $data['id']; ?></td>
            <td><?php echo $data['login_its']; ?></td>
            <td><?php echo $data['first_name'].' '.$data['last_name']; ?></td>
            <td><?php echo $data['jamaat']; ?></td>
            <td><?php echo $araz_type; ?></td>
            <td><?php echo $data['city']; ?></td>
          </tr>
          <?php 
            if($araz_courses){
              foreach ($araz_courses as $ac) { 
          ?>
            <tr><td colspan="6"></td></tr>
            <tr>
              <td></td>
              <td colspan='2'><strong>Degree / Course Name : </strong><?php echo $ac['course_name']; ?></td>
              <td colspan='4'><strong>Institute Name : </strong><?php echo $ac['institute_name']; ?></td>
            </tr>
            <tr>
              <td></td>
              <td colspan='2'><strong>Course Duration : </strong><?php echo $ac['course_duration']; ?></td>
              <td colspan='2'><strong>Institute City : </strong><?php echo $ac['institute_city']; ?></td>
              <td colspan='2'><strong>Accomodation : </strong><?php echo $ac['accomodation']; ?></td>
            </tr>
                  
          <?php 
              } 
            }else { 
          ?>
            <tr><td colspan="6"></td></tr>
            <tr>
              <td></td>
              <td colspan='2'><strong>School Name : </strong><?php echo $data['school_name']; ?></td>
              <td colspan='2'><strong>School Standard : </strong><?php echo $data['school_standard']; ?></td>
              <td colspan='2'><strong>School City : </strong><?php echo $data['school_city']; ?></td>
            </tr>
          <?php 
              } 
            } 
          ?>                                                              
                        
        </tbody>
      </table>
    </div>
  </div>
</body>
</html>
