<?php
require_once 'classes/class.user.admin.php';
include 'classes/class.groupname.php';
$allowed_roles = array(ROLE_HEAD_OFFICE);
require_once 'session.php';

$title = 'Group';
$description = '';
$keywords = '';
$active_page = "manage_group";


$group = new Group();
$id = FALSE;
$select = FALSE;

// Perform delete operation if requested
if (isset($_POST['cmd']) && $_POST['cmd'] == 'del-grp') {
  global $db;

  $delete_id = (int) $_POST['id'];
  $query = "DELETE FROM groups where id = '$delete_id'";
  $result = $db->query($query);

  if ($result) {
    $_SESSION[SUCCESS_MESSAGE] = "Group Deleted Successfully";
  } else {
    $_SESSION[ERROR_MESSAGE] = "An error occoured while delete operation.";
  }
}

if (isset($_GET['id'])) {
  $select = $group->grpname_result($_GET['id']);
  $id = $_GET['id'];
}

if (isset($_POST['Update'])) {
  $id = $_POST['id'];
  $grpname = $_POST['grpname'];

  $update = $group->update_grpname($grpname, $id);

  $update ? $_SESSION[SUCCESS_MESSAGE] = "Groupname Successfully Update" : $_SESSION[ERROR_MESSAGE] = "Error In Update Data";
}

if (isset($_POST['Save'])) {
  $grpname = $_POST['grpname'];

  $insert = $group->insert_grp($grpname);
  $insert ? $_SESSION[SUCCESS_MESSAGE] = "Groupname Successfully Insert" : $_SESSION[ERROR_MESSAGE] = "Error In Inserting Data";
}

$heading = $id ? 'Update Group Title' : $heading = 'Add New Group';
$btn_name = $id ? 'Update' : 'Add New';

include ('header.php');
?>


  <div class="row">

    <div class="col-lg-4 col-xs-12 col-sm-12 pull-right">
      <div class="col-lg-12">
        <h3 class="page-header"><?php echo $heading; ?></h3>
      </div>
      <!-- /.col-lg-12 -->

      <form method="post"  action="groupname.php" role="form" class="form-horizontal" >

        <div class="form-group">
          <label class="control-label">Group Title:</label>
          <input type="text" class="form-control" name="grpname"  placeholder="Add Group" value="<?php echo $select[0]['grpName']; ?>" required>
        </div>

        <div class="form-group">
          <button name="<?php echo $btn_name; ?>" class="btn btn-success btn-block"><?php echo $btn_name ?></button>
        </div>
        <?php if ($id) echo '<input type="hidden" name="id" value="' . $id . '">'; ?>
      </form>
    </div>

    <div class="col-lg-8 col-xs-12 col-sm-12  pull-left">
      <div class="col-md-12">&nbsp;</div>
      <div class="clearfix"></div>
      <div class="panel panel-green">
        <div class="panel-heading">
          <i class="fa fa-group fa-fw"></i> Groups
        </div>
        <!-- /.panel-heading -->
        <div class="panel-body">
          <div class="table-responsive">
            <table class="table table-striped table-hover table-condensed">
              <thead>
                <tr>
                  <th>No</th>
                  <th>Group Name</th>
                  <th>Action</th>
                </tr>
              </thead>
              <tbody>
                <?php
                $select = $group->grpname_result();

                $i = 1;
                foreach ($select as $data) {
                  ?>

                  <tr>
                    <td>
                      <?php echo $i++; ?>
                    </td>
                    <td class="text-left">
                      <?php echo $data['grpName']; ?>
                    </td>

                    <td>
                      <a href="groupname.php?id=<?php echo $data['id']; ?>"><i class="fa fa-edit"></i></a>
                      <button class="btn btn-link confirm" id="<?php echo $data['id']; ?>"><i class="fa fa-remove text-danger"></i></button>
                    </td>
                  </tr>
                  <?php
                }
                ?>
              </tbody>
            </table>
          </div>
          <!-- /.table-responsive -->
        </div>
        <!-- /.panel-body -->
      </div>
    </div>
  </div>

  <form method="post" id="frm-del-grp">
    <input type="hidden" name="id" id="del-grp-val" value="">
    <input type="hidden" name="cmd" value="del-grp">
  </form>
  <script>
    $(".confirm").confirm({
      text: "Are you sure you want to delete?",
      title: "Confirmation required",
      confirm: function(button) {
        $('#del-grp-val').val($(button).attr('id'));
        $('#frm-del-grp').submit();
        alert('Are you Sure You want to delete: ' + id);
      },
      cancel: function(button) {
        // nothing to do
      },
      confirmButton: "Yes",
      cancelButton: "No",
      post: true,
      confirmButtonClass: "btn-danger"
    });
  </script>

<!-- /#page-wrapper -->

<!-- /#wrapper -->
<?php
include 'footer.php';
?>



