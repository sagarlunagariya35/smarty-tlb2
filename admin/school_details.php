<?php
require_once 'session.php';
require_once 'requires_login.php';

// Page bady class
$body_class = 'page-sub-page';

$group_id = $_GET['group'];
if (isset($_GET['s'])) {
  $skip = $_GET['s'];
} else {
  $skip = 'f';
}

if($_SESSION['step'] != '2') {
  if($_SESSION['step'] <= '2'){
    if($_SESSION['step'] == '1' && $group_id != '1'){
      header('location: ' . SERVER_PATH . 'marhala-' . $group_id . '/' . get_marhala_slug($group_id) . '/madrasah');
    }
  }
}

$filter_school_array = array('School run by Dawat e Hadiyah - Boys','School run by Dawat e Hadiyah - Girls','School run by Dawat e Hadiyah - Co-Ed','Private School - Boys','Private School - Girls','Private School - Co-Ed','Public School - Boys','Public School - Girls','Public School - Co-Ed');

switch ($group_id) {
  case 1:
    $panel_heading = 'Pre-Primary Araz';
    $std_heading = 'Pre-Primary';
    $panel_heading_ar = 'الحضانة';
    $kalemaat_nooraniyah = 'kalemaat nooraniyah marhala 1';
    $ary_standard = array('Nursery','Junior K.G.', 'Senior K.G.');
    break;
  case 2:
    $panel_heading = 'Primary Araz';
    $std_heading = 'Primary';
    $panel_heading_ar = 'الابتدائية الأولى';
    $ary_standard = array('1st', '2nd', '3rd', '4th');
    $kalemaat_nooraniyah = 'kalemaat nooraniyah marhala 2';
    break;
  case 3:
    $panel_heading = 'Std 5th to Std 7th Araz';
    $std_heading = 'Std 5th to Std 7th';
    $panel_heading_ar = 'الابتدائية الثانية';
    $ary_standard = array('5th', '6th', '7th');
    $kalemaat_nooraniyah = 'kalemaat nooraniyah marhala 3';
    break;
  case 4:
    $panel_heading = 'Std 8th to 10th Araz';
    $std_heading = 'Std 8th to Std 10th';
    $panel_heading_ar = 'الثانوية الأولى';
    $ary_standard = array('8th', '9th', '10th');
    $kalemaat_nooraniyah = 'kalemaat nooraniyah marhala 4';
    break;
  case 5:
    $panel_heading = 'Std 11th - 12th Araz';
    $std_heading = 'Std 11th - 12th';
    $panel_heading_ar = 'الثانوية الثانية';
    $ary_standard = array('11th', '12th');
    $kalemaat_nooraniyah = 'kalemaat nooraniyah marhala 5';
    break;
  case 6:
    $panel_heading = 'Graduation Araz';
    $std_heading = 'Graduation';
    $panel_heading_ar = 'البكالوريا';
    $ary_standard = array();
    $kalemaat_nooraniyah = 'kalemaat nooraniyah marhala 6';
    break;
  case 7:
    $panel_heading = 'Post Graduation Araz';
    $std_heading = 'Post Graduation';
    $panel_heading_ar = 'الماجيستار';
    $ary_standard = array();
    $kalemaat_nooraniyah = 'kalemaat nooraniyah marhala 7';
    break;
  case 8:
    $panel_heading = 'Diploma Araz';
    $std_heading = 'Diploma';
    $panel_heading_ar = 'الدبلوم';
    $ary_standard = array();
    $kalemaat_nooraniyah = 'kalemaat nooraniyah marhala 7';
    break;
}
require_once './classes/class.araz.php';

if (isset($_POST['submit'])) {

  //store in session only
//  $araz = new mtx_araz();

  $_SESSION['marhala_id'] = $group_id;
  $_SESSION['madrasah_country'] = $_POST['madrasah_country'];
  $_SESSION['school_city'] = $_POST['school_city'];
  $_SESSION['school_standard'] = $_POST['school_standard'];
  $_SESSION['school_name'] = $_POST['school_name'];
  $_SESSION['school_state'] = $_POST['school_state'];
  $_SESSION['school_filter'] = $_POST['filter_school'];
  $_SESSION['school_pincode'] = $_POST['school_pincode'];
  
  $_SESSION['step'] = '3';

  header('location: ' . SERVER_PATH . 'marhala-' . $group_id . '/' . get_marhala_slug($group_id) . '/ques/');
}

$countries = get_all_countries();

require_once 'inc/inc.header2.php';
?>


<!-- Contents -->
<!-- ====================================================================================================== -->

<script type="text/javascript">
// Form validation code will come here.
function validate()
{
 
   if( document.school_form.filter_school.value == "" )
   {
     alert( "Please Select Your School!" );
     document.school_form.filter_school.focus() ;
     return false;
   }
   if( document.school_form.school_standard.value == "" )
   {
     alert( "Please Select Your Standard!" );
     document.school_form.school_standard.focus() ;
     return false;
   }
   if( document.school_form.madrasah_country.value == "" )
   {
     alert( "Please Select Country!" );
     document.school_form.madrasah_country.focus() ;
     return false;
   }
   if( document.school_form.school_city.value == "")
   {
     alert( "Please Enter City where your School is Located!" );
     document.school_form.school_city.focus() ;
     return false;
   }
   if( document.school_form.school_name.value == "" )
   {
     alert( "Please Enter School Name!" );
     document.school_form.school_name.focus() ;
     return false;
   }
   return( true );
}
//-->
</script>


<div class="container white-bg">
  <div class="col-md-12 col-sm-12">

    <div class="page-title">
      <p style="margin-top:5px;"><a href="<?php echo SERVER_PATH; ?>">Home</a> / <a href="<?php echo SERVER_PATH; ?>raza-araz/">Send Araz To Aqa Maula TUS</a> / <a href="<?php echo SERVER_PATH; ?>marahil/">Raza Araz for</a> <?php if($group_id != '1') { ?> / <a href="<?php echo SERVER_PATH . 'marhala-' . $group_id . '/' . get_marhala_slug($group_id) . '/madrasah'; ?>"><?php echo $panel_heading; ?> ( Madrasah )</a> <?php } ?> / <a href="#" class="active"><?php echo $panel_heading; ?></a></p>
      <h1><?php echo $panel_heading; ?><span class="alfatemi-text"><?php echo $panel_heading_ar; ?></span></h1>
    </div>
  </div>
  <div class="clearfix"></div> <!-- do not delete -->
  <div class="col-md-12 col-sm-12">
    <div class="page">
      
      <h6><span class="alfatemi-text">تمارا بچه ؤ مارا بچه ؤ  چھے، اهني قدر جانو خدا ني نعمة  چھے</span></h6>
      
      <h4>Fill up this form to submit your araz for <?php echo $std_heading; ?> Education. <span class="alfatemi-text">تماري ابتدائي تعليم  ني عرض واسطے اْ فارم بھري عرض كرو</span></h4>
      
      <form name="school_form" class="forms1" action="<?php echo SERVER_PATH . 'marhala-' . $group_id . '/' . get_marhala_slug($group_id) . '/school/' ?>" method="post" onsubmit="return(validate());">
        
        <h3>School / Grade<span class="alfatemi-text">اسكول</span></h3>

        <div class="clearfix"></div> <!-- do not delete -->
        <div class="col-md-6 col-sm-12 shift">
          <div  class="shift10">
            <select name="filter_school">
              <option value="">Is your school</option>
              <?php
                foreach ($filter_school_array as $scl) {
                  ?>
                  <option value="<?php echo $scl; ?>" <?php if(@$_SESSION['school_filter'] == $scl) echo 'selected'; ?>><?php echo $scl; ?></option>
                  <?php
                }
              ?>
            </select>
          </div>
        </div>
        <div class="col-md-6 col-sm-12 shift">
          <div  class="shift10">
            <select name="school_standard">
              <option value="">Select Standard</option>
                <?php
                  foreach ($ary_standard as $std) {
                    ?>
                    <option value="<?php echo $std; ?>" <?php if(@$_SESSION['school_standard'] == $std) echo 'selected'; ?>><?php echo $std; ?></option>
                    <?php
                  }
                ?>
            </select>
          </div>
        </div>
        
        <div class = "col-md-6 col-sm-12 shift">
          <div class="">
            <select name="madrasah_country" onChange="showcity(this.value)">
              <option value="">Select Country</option>
              <?php
              if($countries != '') {
              foreach ($countries as $cntry) {
                ?>
              <option value="<?php echo $cntry['name']; ?>" <?php if(@$_SESSION['madrasah_country'] == $cntry['name']) echo 'selected'; ?>  ><?php echo $cntry['name']; ?></option>
                <?php
                }
              }
              ?>
            </select>
          </div>
        </div>
        
        <div class="col-md-6 col-sm-12 shift">
          <div  class="shift10" id="your_city">
            <select name="school_city" id="select-city">
                  <option value="">Select City where your school is located</option>
                  <?php if($_SESSION['madrasah_city']) { ?>
                  <option value="<?php echo $_SESSION['madrasah_city']; ?>" selected><?php echo $_SESSION['madrasah_city']; ?></option>
                  <?php } ?>
              </select>
          </div>
        </div>

        <div class="col-md-6 col-sm-12 shift">
          <div  class="shift10">
            <input type="text"  name="school_name" id="school" placeholder="Please write the School name with correct spelling" value="<?php echo @$_SESSION['school_name']; ?>">
          </div>
        </div>
        <div class="col-md-6 col-sm-12 shift">
          <div  class="shift10">
            <input type="text" name="school_pincode" placeholder="PINCODE/ZIPCODE" value="<?php echo @$_SESSION['school_pincode']; ?>">
          </div>
        </div>
        <div class="clearfix"></div> <!-- do not delete -->
        <input type="submit" class="submit1" name="submit" value="next"/>
        <div class="clearfix"></div> <!-- do not delete -->
        <div class="block vmgn20 allcaps">
          <p>For any updates in the above dropdown fields kindly email to <a href="mailto:education@alvazarat.org" target="_blank">education@alvazarat.org</a> . Updates will be effective within 24 hours. </p>
        </div>
        <div class="clearfix"></div> <!-- do not delete -->
      </form>
      <div class="clearfix"></div> <!-- do not delete -->
    </div>
  </div>
</div>

<script>
  function showcity(str)
  {
    if (str == "")
    {
      document.getElementById("your_city").innerHTML = "";
      document.getElementById("mname").innerHTML = "";
      return;
    }

    if (window.XMLHttpRequest)
    {// code for IE7+, Firefox, Chrome, Opera, Safari
      xmlhttp = new XMLHttpRequest();
    }
    else
    {// code for IE6, IE5
      xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
    }



    xmlhttp.onreadystatechange = function()
    {
      if (xmlhttp.readyState == 4 && xmlhttp.status == 200)
      {
        document.getElementById("your_city").innerHTML = xmlhttp.responseText;
      }
    }
    xmlhttp.open("GET", "get_city_by_country/" + str, true);
    xmlhttp.send();
  }
</script>

<?php
require_once 'inc/inc.footer.php';
?>