<?php
require_once '../classes/class.database.php';
include 'classes/class.courses.php';
$allowed_roles = array(ROLE_HEAD_OFFICE);
require_once 'session.php';

$course = new Mtx_Courses();

$title = 'List of Taken Course';
$description = '';
$keywords = '';
$active_page = 'list_taken_course';

$select = FALSE;
$select = $course->get_all_user_taken_courses();
$total_taken_course = $course->count_total_user_taken_courses();

include ('print_header.php');
?>
<body style="padding: 10px;">
  <div>
    <p style="display: block; text-align: right"><?php echo date('d-m-Y H:i:s'); ?></p>
  </div>
  
  <div class="row">
    <div class="col-xs-12 col-sm-12">
      <h4>Total Taken Courses: <?php echo $total_taken_course; ?></h4>
    </div>
  </div>
  
  <div class="row">
    <div class="col-md-12">
      <table class="table table-responsive table-condensed">
        <thead>
          <tr>
            <th>Sr No.</th>
            <th>ITS</th>
            <th>Full Name</th>
            <th>Email</th>
            <th>Mobile</th>
          </tr>
        </thead>
        <tbody>
          <?php 
          if($select){
            $i = 1; 
            foreach($select as $data){
          ?>
          <tr>
            <td><?php echo $i++;?></td>
            <td><?php echo $data['its_id']; ?></td>
            <td><?php echo $data['first_name']. ' '. $data['last_name']; ?></a></td>
            <td><?php echo $data['email']; ?></td>
            <td><?php echo $data['mobile']; ?></td>
          </tr>
          <?php 
              } 
            } else { ?>
          <tr>
            <td class="text-center" colspan="5">No Records..</td>
          </tr>
          <?php } ?>
        </tbody>
      </table>
    </div>
  </div>
</body>
</html>
