<?php
include 'classes/class.crs_topic.php';
include 'classes/class.crs_chapter.php';
include 'classes/class.crs_course.php';
include 'classes/class.crs_announcements.php';
$allowed_roles = array(ROLE_HEAD_OFFICE);
require_once 'session.php';

$crs_announce = new Mtx_Crs_Announcements();
$crs_topic = new Mtx_Crs_Topic();
$crs_course = new Mtx_Crs_Course();
$crs_chapter = new Mtx_Crs_Chapter();

$title = 'Announcements List';
$description = '';
$keywords = '';
$active_page = "list_announcements";

$result = $id = FALSE;

if (isset($_REQUEST['id'])) {
  $id = $_REQUEST['id'];
  $result = $crs_announce->get_announcement($id);
}

if(isset($_GET['cmd']) && $_GET['cmd'] == 'change_status') {
  
  $change_status = $crs_announce->change_announcement_status($_GET['status'], $_GET['id']);
  
  if ($change_status) {
    $_SESSION[SUCCESS_MESSAGE] = 'Announcement Status changed successfully.';
    header('Location: crs_announcements.php');
    exit();
  } else {
    $_SESSION[ERROR_MESSAGE] = 'Error encountered while Changing Announcement Status';
  }
}

if (isset($_POST['btn_update'])) {
  $data = $db->clean_data($_POST);
  // get variables
  $topic_id = $data['topic_id'];
  $course_id = $data['course_id'];
  $chapter_id = $data['chapter_id'];
  $active = $data['active'];
  $description = $data['description'];

  $update = $crs_announce->update_announcement($topic_id, $course_id, $chapter_id, $description, $active, $id);

  if ($update) {
    $_SESSION[SUCCESS_MESSAGE] = 'Announcement Updated Successfully.';
    header('Location: crs_announcements.php?id='.$id);
    exit();
  } else {
    $_SESSION[ERROR_MESSAGE] = 'Error encountered while Updating Announcement';
  }
}

if (isset($_POST['btn_submit'])) {
  $data = $db->clean_data($_POST);
  // get variables
  
  $topic_id = $data['topic_id'];
  $course_id = $data['course_id'];
  $chapter_id = $data['chapter_id'];
  $active = $data['active'];
  $description = $data['description'];
  $user_id = $_SESSION[USER_ID];

  $insert = $crs_announce->insert_announcement($topic_id, $course_id, $chapter_id, $description, $user_id, $active);

  if ($insert) {
    $_SESSION[SUCCESS_MESSAGE] = 'Announcement Inserted Successfully.';
  } else {
    $_SESSION[ERROR_MESSAGE] = 'Error encountered while Inserting Announcement';
  }
}

$btn = ($id != FALSE) ? 'btn_update' : 'btn_submit';
$list_announcements = $crs_announce->get_all_announcements();
$list_topics = $crs_topic->get_all_topics();
$list_courses = $crs_course->get_all_courses();
$list_chapters = $crs_chapter->get_all_chapters();

include_once("header.php");
?>
<link rel="stylesheet" href="js/select2/select2.min.css">
<script src="js/select2/select2.full.min.js"></script>
<script type="text/javascript" src="ckeditor/ckeditor.js"></script>

<div class="row">
  <div class="col-lg-12">
    <h3 class="page-header">Add New Announcement</h3>
  </div>
  <!-- /.col-lg-12 -->
</div>

<div class="row">
  <div class="col-md-12">
    <form method="post" enctype="multipart/form-data">
      
      <div class="form-group col-md-6 col-xs-12">
        <label for="title">Topic :</label>
        <select name="topic_id" id="topic_id" class="form-control topic_id">
          <option value="">Select</option>
          <?php
          if ($list_topics) {
            foreach ($list_topics as $lt) {
              $selected_topic = ($lt['id'] == $result['topic_id']) ? 'selected' : '';
          ?>
            <option value="<?php echo $lt['id']; ?>" <?php echo $selected_topic; ?>><?php echo $lt['title']; ?></option>
          <?php
            }
          }
          ?>
        </select>
      </div>
      
      <div class="form-group col-md-6 col-xs-12">
        <label for="title">Course :</label>
        <select name="course_id" id="course_id" class="form-control course_id">
          <option value="">Select</option>
          <?php
          if ($list_courses) {
            foreach ($list_courses as $lc) {
              $selected_course = ($lc['id'] == $result['course_id']) ? 'selected' : '';
          ?>
            <option value="<?php echo $lc['id']; ?>" <?php echo $selected_course; ?>><?php echo $lc['title']; ?></option>
          <?php
            }
          }
          ?>
        </select>
      </div>
      
      <div class="form-group col-md-6 col-xs-12">
        <label for="title">Chapter :</label>
        <select name="chapter_id" id="chapter_id" class="form-control chapter_id">
          <option value="">Select</option>
          <?php
          if ($list_chapters) {
            foreach ($list_chapters as $lcp) {
              $selected_chapter = ($lcp['id'] == $result['chapter_id']) ? 'selected' : '';
          ?>
            <option value="<?php echo $lcp['id']; ?>" <?php echo $selected_chapter; ?>><?php echo $lcp['title']; ?></option>
          <?php
            }
          }
          ?>
        </select>
      </div>

      <div class="form-group col-md-6 col-xs-12">
        <label for="active">Is Active? :</label>
        <select name="active" id="active" class="form-control" required >
          <option value="">Select</option>
          <option value="1" <?php echo ($result['is_active'] == '1') ? 'selected' : ''; ?>>Yes</option>
          <option value="0" <?php echo ($result['is_active'] == '0') ? 'selected' : ''; ?>>No</option>
        </select>
      </div>
      
      <div class="form-group col-md-12 col-xs-12">
        <label for="miqat">Description :</label>
        <textarea name="description" class="form-control"><?php echo $result['description']; ?></textarea>
      </div>
      
      <div class="clearfix"></div>
      <div class="form-group col-md-2 col-xs-12">
        <input type="submit" name="<?php echo $btn; ?>" id="<?php echo $btn; ?>" value="Submit" class="btn btn-primary btn-block form-control">
      </div>
    </form>

  </div>
</div>

<br>
<div class="panel panel-primary">
  <div class="panel-heading">
    <i class="fa fa-group fa-fw"></i> <?php echo $title; ?>
  </div>
  <!-- /.panel-heading -->
  <div class="panel-body">
    <div class="table-responsive">
      <table class="table table-striped table-hover table-condensed">
        <thead>
          <tr>
            <th>Sr</th>
            <th>Topic</th>
            <th>Course</th>
            <th>Chapter</th>
            <th>Date</th>
            <th>Status</th>
            <th>Action</th>
          </tr>
        </thead>
        <tbody>
          <?php
          if($list_announcements){
            $sr = 0;
            foreach ($list_announcements as $data) {
              $sr++;
          ?>
              <tr class="<?php if($data['is_active'] == '1') { echo 'success'; } else { echo 'danger'; } ?>">
                <td><?php echo $sr; ?></td>
                <td><?php echo $crs_announce->get_table_title_by_id('crs_topic', $data['topic_id']); ?></td>
                <td><?php echo $crs_announce->get_table_title_by_id('crs_course', $data['course_id']); ?></td>
                <td><?php echo $crs_announce->get_table_title_by_id('crs_chapter', $data['chapter_id']); ?></td>
                <td><?php echo date('d, F Y',  strtotime($data['created_ts'])); ?></td>
                <td><input type="checkbox" name="is_active[]" id="action<?php echo $data['id']; ?>" onclick="check(<?php echo $data['id']; ?>)" <?php echo ($data['is_active'] == 1) ? 'checked' : ''; ?>></td>
                <td><a href="crs_announcements.php?id=<?php echo $data['id']; ?>"><i class="fa fa-pencil-square-o"></i></a></td>
              </tr>
              <?php
            }
          }else {
            echo '<tr><td class="text-center" colspan="7">No Records..</td></tr>';
          }
          ?>
        </tbody>
      </table>
    </div>
    <!-- /.table-responsive -->
  </div>
  <!-- /.panel-body -->
</div>

<script type="text/javascript">
  function check(val){
    var doc = document.getElementById('action'+val).checked;

    if(doc == true) doc = 1;
    else doc = 0;
    window.location.href='crs_announcements.php?id='+val+'&status='+doc+'&cmd=change_status';
  }
</script>
<script type="text/javascript" src="js/course_details.js?v=1"></script>

<!-- /#page-wrapper -->
<?php include "./footer.php"; ?>
