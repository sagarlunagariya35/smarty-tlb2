<div class="panel panel-primary" id="araz_div">
  <div class="panel-heading" style="font-size: 19px;">
    <div class="row">
      <div class="col-xs-12 col-sm-4"><strong>Track No: </strong><?php echo $araz_id; ?></div>
      <div class="col-xs-12 col-sm-4 text-center"><strong>Marhala: </strong><?php echo $araz_general_detail['marhala']; ?></div>
      <?php
        $from = new DateTime($araz_general_detail['araz_ts']);
        $to = new DateTime('today');
        $araz_days_past = $from->diff($to)->days;
        
        if($counselor_details['coun_assign_ts'] > 0) {
          $coun_from = new DateTime($counselor_details['coun_assign_ts']);
          $coun_to = new DateTime('today');
          $counselor_days_past = $coun_from->diff($coun_to)->days;
        }
        
        if($araz_general_detail['saheb_assign_ts'] > 0) {
          $saheb_from = new DateTime($araz_general_detail['saheb_assign_ts']);
          $saheb_to = new DateTime('today');
          $saheb_days_past = $saheb_from->diff($saheb_to)->days;
        }
        ?>
      <div class="col-xs-12 col-sm-4 text-right example black_font" rel="popover" data-html="true" data-content="<?php 
        echo 'New Araz Days Past : '.$araz_days_past;
      if($counselor_days_past){
        echo '<br />';
        echo 'Counselor Assign Days Past : '.$counselor_days_past;
      }
      if($saheb_days_past){
        echo '<br />';
        echo 'Saheb Assign Days Past : '.$saheb_days_past;
      }
?>" data-placement="top"><strong>Days past: </strong> <?php echo $araz_days_past; ?></div>
      <div class="clearfix"></div>
    </div>
  </div>
  <div class="row">
  </div>
  <div class="panel-body">
    <div class="panel panel-default">
      <div class="panel-heading">
        <strong>Personal Details: </strong><?php echo $user_details['user_full_name']; ?>
        <span class="pull-right"><a role="button" data-toggle="collapse" href="#collapse-personal-info<?php echo $araz_id; ?>" aria-expanded="false" aria-controls="collapseExample">Show / Hide</a></span>
        <div class="clearfix"></div>
      </div>
      <div class="panel-body collapse" id="collapse-personal-info<?php echo $araz_id; ?>">
        <div class="row">
          <div class="col-xs-12 col-sm-10">
            <div class="col-xs-12 col-sm-4 bottom10px">
              <strong>ITS: </strong><?php echo $user_details['login_its']; ?>
            </div>
            <div class="col-xs-12 col-sm-4 bottom10px">
              <strong>Hifz Sanad: </strong><?php echo $user_details['hifz_sanad']; ?>
            </div>
            <div class="col-xs-12 col-sm-4 bottom10px">
              <strong>Age: </strong>
              <?php
              $from = new DateTime($user_details['dob']);
              $to = new DateTime('today');
              echo $from->diff($to)->y;
              ?>
            </div>
            <div class="col-xs-12 col-sm-4 bottom10px">
              <strong>Gender: </strong>
<?php echo ($user_details['gender'] == 'M') ? 'Male' : 'Female'; ?>
            </div>
            <div class="col-xs-12 col-sm-4 bottom10px">
              <strong>Nationality: </strong><?php echo $user_details['nationality']; ?>
            </div>
            <div class="col-xs-12 col-sm-4 bottom10px">
              <strong>Country: </strong><?php echo $user_details['country']; ?>
            </div>
            <div class="col-xs-12 col-sm-4 bottom10px">
              <strong>State: </strong><?php echo $user_details['state']; ?>
            </div>
            <div class="col-xs-12 col-sm-4 bottom10px">
              <strong>City: </strong><?php echo $user_details['city']; ?>
            </div>
            <div class="col-xs-12 col-sm-4 bottom10px">
              <strong>Jamaat: </strong><?php echo $user_details['jamaat']; ?>
            </div>
            <div class="col-xs-12 col-sm-4 bottom10px">
              <strong>Jamiyat: </strong><?php echo $user_details['jamiat']; ?>
            </div>
            <div class="col-xs-12 col-sm-4 bg-success bottom10px">
              <strong>Araz Type: </strong><?php echo ucfirst($user_details['araz_type']); ?>
            </div>
            <div class="col-xs-12 col-sm-4 bottom10px">
              <strong>Mobile: </strong><?php echo ucfirst($user_details['mobile']); ?>
            </div>
            <div class="col-xs-12 col-sm-4 bottom10px">
              <strong>Whatsapp: </strong><?php echo ucfirst($user_details['whatsapp']); ?>
            </div>
            <div class="col-xs-12 col-sm-8 bottom10px">
              <strong>email: </strong><?php echo ucfirst($user_details['email']); ?>
            </div>
            <div class="clearfix"></div>
          </div>
          <div class="col-xs-12 col-sm-2">
<?php
$user_araz = new mtx_user;
$user_araz->loaduser($user_details['login_its']);
?>
            <img class="img-responsive" src="<?php echo $user_araz->get_mumin_photo(); ?>">
          </div>
        </div>
      </div>
    </div>

    <!-- Araz General Details -->
    <?php
    unset($user_araz);
    if ($araz_general_detail['marhala'] < 5) {
      /*
       */
      ?>
      <div class="panel panel-default">
        <div class="panel-heading">
          <strong>Deeni Ta'aleem</strong>
          <span class="pull-right"><a role="button" data-toggle="collapse" href="#collapse-deeni-taaleem<?php echo $araz_id; ?>" aria-expanded="false" aria-controls="collapseExample">Show / Hide</a></span>
        <div class="clearfix"></div>
        </div>
        <div class="panel-body collapse" id="collapse-deeni-taaleem<?php echo $araz_id; ?>">
          <div class="row">
            <div class="col-xs-12 col-sm-6 bottom10px">
              <strong>Madrasah Name: </strong><?php echo $araz_general_detail['madrasah_name']; ?>
            </div>
            <div class="col-xs-12 col-sm-6 bottom10px">
              <strong>Darajah: </strong><?php echo $araz_general_detail['madrasah_darajah']; ?>
            </div>
            <div class="col-xs-12 col-sm-6 bottom10px">
              <strong>Country: </strong><?php echo $araz_general_detail['madrasah_country']; ?>
            </div>
            <div class="col-xs-12 col-sm-6 bottom10px">
              <strong>City: </strong><?php echo $araz_general_detail['madrasah_city']; ?>
            </div>
            <div class="clearfix"></div>
          </div>
        </div>
      </div>

      <div class="panel panel-default">
        <div class="panel-heading">
          <strong>School Information</strong>
          <span class="pull-right"><a role="button" data-toggle="collapse" href="#collapse-school<?php echo $araz_id; ?>" aria-expanded="false" aria-controls="collapseExample">Show / Hide</a></span>
          <div class="clearfix"></div>
        </div>
        <div class="panel-body collapse" id="collapse-school<?php echo $araz_id; ?>">
          <div class="row">
            <div class="col-xs-12 col-sm-6 bottom10px">
              <strong>School Name: </strong><?php echo $araz_general_detail['school_name']; ?>
            </div>
            <div class="col-xs-12 col-sm-6 bottom10px">
              <strong>School is: </strong><?php echo $araz_general_detail['school_filter']; ?>
            </div>
            <div class="col-xs-12 col-sm-6 bottom10px">
              <strong>Standard / Grade: </strong><?php echo $araz_general_detail['school_standard']; ?>
            </div>
            <div class="col-xs-12 col-sm-6 bottom10px">
              <strong>City: </strong><?php echo $araz_general_detail['school_city']; ?>
            </div>
            <div class="clearfix"></div>
          </div>
        </div>
      </div>
      <?php
    }

    // Previous Study Details
    if ($araz_general_detail['marhala'] > 4) {
      if($araz_general_detail['marhala'] > 5){
        if($previous_study_details['current_course'] == '' AND $previous_study_details['current_inst_name'] == ''){
          $previous_studies = $araiz->get_previous_study_data($course_details, $user_details['araz_type']);
          $curr_course_name = $previous_studies['course_name'];
          $curr_institute_name = $previous_studies['institute_name'];
          $curr_country_name = $previous_studies['institute_country'];
          $curr_city_name = $previous_studies['institute_city'];
        }else {
          $curr_course_name = $previous_study_details['current_course'];
          $curr_institute_name = $previous_study_details['current_inst_name'];
          $curr_country_name = $previous_study_details['current_inst_country'];
          $curr_city_name = $previous_study_details['current_inst_city'];
        }
        ?>
      <div class="row">
        <div class="col-xs-12 col-sm-12 text-center alfatemi-text" dir="rtl">
          ادبًا عرض كه<br>
          في الحال حسب الذيل تعليم حاصل كروطط ححهوطط
        </div>
      </div>
      <?php } ?>
      <div class="panel panel-default">
        <div class="panel-heading">
          <strong>Previous / Current Study Information</strong>
          <span class="pull-right"><a role="button" data-toggle="collapse" href="#collapse-previous-study<?php echo $araz_id; ?>" aria-expanded="false" aria-controls="collapseExample">Show / Hide</a></span>
        <div class="clearfix"></div>
        </div>
        <div class="panel-body collapse" id="collapse-previous-study<?php echo $araz_id; ?>">
          <div class="row">
            <div class="col-xs-12 col-sm-6 bottom10px">
              <strong>Course: </strong><?php echo $curr_course_name; ?>
            </div>
            <div class="col-xs-12 col-sm-6 bottom10px">
              <strong>Institute: </strong><?php echo $curr_institute_name; ?>
            </div>
            <div class="col-xs-12 col-sm-6 bottom10px">
              <strong>Country: </strong><?php echo $curr_country_name; ?>
            </div>
            <div class="col-xs-12 col-sm-6 bottom10px">
              <strong>City: </strong><?php echo $curr_city_name; ?>
            </div>
            <div class="clearfix"></div>
          </div>
        </div>
      </div>
      <div class="panel panel-success">
        <div class="panel-heading">
          <strong>Further Education Information</strong>
          <span class="pull-right"><a role="button" data-toggle="collapse" href="#collapse-further-study<?php echo $araz_id; ?>" aria-expanded="false" aria-controls="collapseExample">Show / Hide</a></span>
          <div class="clearfix"></div>
        </div>
        <div class="panel-body collapse" id="collapse-further-study<?php echo $araz_id; ?>">
  <?php
  if (in_array($_SESSION[USER_ROLE], array(ROLE_HEAD_OFFICE,ROLE_SAHEB,ROLE_ADMIN_MARHALA_1,ROLE_ADMIN_MARHALA_2,ROLE_ADMIN_MARHALA_3,ROLE_ADMIN_MARHALA_4,ROLE_ADMIN_MARHALA_5,ROLE_ADMIN_MARHALA_6,ROLE_ADMIN_MARHALA_7,ROLE_ADMIN_MARHALA_8))) {
    ?>
            <div class="row">
              <div class="col-xs-12 col-sm-12 well well-sm text-info alfatemi-text" dir="rtl">
                <strong>Head Office Remarks: </strong><?php echo $araz_general_detail['ho_remarks_saheb']; ?>
              </div>
            </div>
            <?php
          }
          $sr = 0;
          foreach ($course_details as $course) {
            $sr++;
            if ($user_details['araz_type'] == 'istirshad') {
              ?>
              <div class="row">
                <div class="col-xs-12 col-sm-6">
                  <label>
                    <input type="radio" name="istirshad-course-<?php echo $araz_id; ?>" id="optionsRadios<?php echo $araz_id; ?>" value="<?php echo $course['araz_course_id']; ?>">
                    Option <?php echo $sr; ?>
                  </label>
                </div>
              </div>
      <?php
    }
      $its_course = $its_city = $its_institute = FALSE;
      if($course['course_name'] != ''){
        $its_course = $araiz->get_data_of_table('tlb_its_course','course',$course['course_name']);
      }
      
      if($course['institute_city'] != ''){
        $its_city = $araiz->get_data_of_table('tlb_its_city','city',$course['institute_city']);
      }
      
      if($course['institute_name'] != ''){
        $its_institute = $araiz->get_data_of_table('tlb_its_institute','institute',$course['institute_name']);
      }
      
      if ($araz_general_detail['marhala'] == 5) {
        $course_name = $course['course_name'];
        $lable = 'Standard';
      } else {
        $course_name = $course['course_name'];
        $lable = 'Duration';
      }
    ?>
            <div class="row well well-sm">
              <div class="col-xs-12 col-sm-6 bottom10px <?php if(!$its_course){ echo 'red'; } ?>">
                <strong>Course: </strong><?php echo $course_name; ?>
              </div>
              <div class="col-xs-12 col-sm-6 bottom10px">
                <strong><?php echo $lable; ?>: </strong><?php echo $course['course_duration']; ?>
              </div>
              <div class="col-xs-12 col-sm-6 bottom10px">
                <strong>Start Month-Year: </strong><?php echo $course['course_started']; ?>
              </div>
              <div class="col-xs-12 col-sm-6 bottom10px <?php if(!$its_institute){ echo 'red'; } ?>">
                <strong>Institute: </strong><?php echo $course['institute_name']; ?>
              </div>
              <div class="col-xs-12 col-sm-6 bottom10px">
                <strong>Country: </strong><?php echo $course['institute_country']; ?>
              </div>
              <div class="col-xs-12 col-sm-6 bottom10px <?php if(!$its_city){ echo 'red'; } ?>">
                <strong>City: </strong><?php echo $course['institute_city']; ?>
              </div>
              <div class="col-xs-12 col-sm-6 bottom10px">
                <strong>Accomodation: </strong><?php echo $course['accomodation']; ?>
              </div>
              <div class="clearfix"></div>
            </div>
        <?php
      }
      ?>
        </div>
      </div>
      <?php
    }
    if ($counselor_details['counselor_id'] != '0') {
      $counselor_array = $araiz->get_counselor_details_of_user_admin($counselor_details['counselor_id']);
      ?>
      <div class="panel panel-default">
        <div class="panel-heading">
          <strong>Counseling Information</strong>
          <span class="pull-right"><a role="button" data-toggle="collapse" href="#collapse-counseling<?php echo $araz_id; ?>" aria-expanded="false" aria-controls="collapseExample">Show / Hide</a></span>
          <div class="clearfix"></div>
        </div>
        <div class="panel-body collapse" id="collapse-counseling<?php echo $araz_id; ?>">
          <div class="row">
            <div class="col-xs-12 col-sm-6 bottom10px">
              <strong>Counselor Name: </strong><?php echo $counselor_array['full_name']; ?>
            </div>
            <div class="col-xs-12 col-sm-6 bottom10px">
              <strong>Counselor Email: </strong><?php echo $counselor_array['email']; ?>
            </div>
            <div class="col-xs-12 col-sm-6 bottom10px">
              <strong>Counselor Mobile: </strong><?php echo $counselor_array['mobile']; ?>
            </div>
            <div class="col-xs-12 col-sm-6 bottom10px">
              <strong>Suggested Course: </strong><?php echo $counselor_details['counselor_sugest_course']; ?>
            </div>
            <div class="col-xs-12 col-sm-6 bottom10px">
              <strong>Suggested Country: </strong><?php echo $counselor_details['counselor_sugest_country']; ?>
            </div>
            <div class="col-xs-12 col-sm-6 bottom10px">
              <strong>Suggested City: </strong><?php echo $counselor_details['counselor_sugest_city']; ?>
            </div>
            <div class="col-xs-12 col-sm-12 bottom10px alfatemi-text" dir="rtl">
              <strong>Counselor Remarks: </strong><?php echo $counselor_details['counselor_sugest_remarks']; ?>
            </div>
            <div class="clearfix"></div>
          </div>
        </div>
      </div>
      <?php
    }
      $ary_answer[0] = 'Not Specified';
      $ary_answer[1] = 'Yes';
      $ary_answer[2] = 'No';
      ?>
      <div class="panel panel-default">
        <div class="panel-heading">
          <strong>Questionnaire Information</strong>
          <span class="pull-right"><a role="button" data-toggle="collapse" href="#collapse-azaaim<?php echo $araz_id; ?>" aria-expanded="true" aria-controls="collapseExample">Show / Hide</a></span>
          <div class="clearfix"></div>
        </div>
        <div class="panel-body" id="collapse-azaaim<?php echo $araz_id; ?>">
          <?php
          $cls_text_class = ($araz_general_detail['want_assistance'] == 1) ? 'text-success' : 'text-danger';
          ?>
          <div class="row well well-sm">
              <div class="row">
                <div class="col-xs-12 col-sm-12 bottom10px <?php echo $cls_text_class ?>">
                  <strong>1: </strong>Are you interested in <strong>counseling</strong> through <strong>Talabulilm</strong>?
                  <div class="clearfix"></div>
                  <strong>Answer: </strong><?php echo $ary_answer[$araz_general_detail['want_assistance']]; ?>
                </div>
              <div class="clearfix"></div>
            </div>
          </div>
          <?php
          $cls_text_class = ($araz_general_detail['seek_funding_edu'] == 1) ? 'text-success' : 'text-danger';
          ?>
          <div class="row well well-sm">
              <div class="row">
                <div class="col-xs-12 col-sm-12 bottom10px <?php echo $cls_text_class ?>">
                  <strong>2: </strong>Do you require <strong>financial assistance</strong>?
                  <div class="clearfix"></div>
                  <strong>Answer: </strong><?php echo $ary_answer[$araz_general_detail['seek_funding_edu']]; ?>
                </div>
              <div class="clearfix"></div>
            </div>
          </div>
          <?php
          $cls_text_class = ($araz_general_detail['disclaimer'] == 1) ? 'text-success' : 'text-danger';
          ?>
          <div class="row well well-sm">
              <div class="row">
                <div class="col-xs-12 col-sm-12 bottom10px <?php echo $cls_text_class ?>">
                  <strong>3: </strong>I agree to share my information with Talabulilm
                  <div class="clearfix"></div>
                  <strong>Answer: </strong><?php echo $ary_answer[$araz_general_detail['disclaimer']]; ?>
                </div>
              <div class="clearfix"></div>
            </div>
          </div>
        </div>
      </div>
  
    <div class="row">
      <div class="col-xs-12 col-sm-12 text-right">
        <strong>Araz Date: </strong><?php echo $araz_general_detail['araz_ts']; ?>
      </div>
    </div>
  </div>
  <div class="panel-footer">
<?php
if (in_array($_SESSION[USER_ROLE], array(ROLE_HEAD_OFFICE,ROLE_SAHEB,ROLE_ADMIN_MARHALA_1,ROLE_ADMIN_MARHALA_2,ROLE_ADMIN_MARHALA_3,ROLE_ADMIN_MARHALA_4,ROLE_ADMIN_MARHALA_5,ROLE_ADMIN_MARHALA_6,ROLE_ADMIN_MARHALA_7,ROLE_ADMIN_MARHALA_8))) {
  ?>
      <div class="row">
        <div class="col-xs-12">
          <textarea class="form-control" name="ho_remarks[]" id="ho-remarks-<?php echo $araz_id; ?>" placeholder="Enter HO Remarks.."></textarea>
        </div>
      </div>
      <div class="col-xs-12 col-md-12">&nbsp;</div>
  <?php
}
if (in_array($_SESSION[USER_ROLE], array(ROLE_COUNSELOR))) {
  ?>
      <div class="row">
        <div class="col-xs-12 col-md-12">
          <strong>Head Office Remarks: </strong><?php echo $araz_general_detail['ho_remarks_counselor']; ?>
        </div>
        <div class="col-xs-12 col-md-12">&nbsp;</div>
        <div class="col-md-12 clslocation">
          <div class="row">
            <div class="col-md-6">
              <select name="course" class="form-control" id="course-<?php echo $araz_id; ?>">
                <option value="">Select Course</option>
  <?php foreach ($courses as $crs) { ?>
                  <option value="<?php echo $crs['course_name']; ?>"><?php echo $crs['course_name']; ?></option>
  <?php } ?>
              </select>
            </div>
            <div class="col-md-3">
              <select name="country" id="country-<?php echo $araz_id; ?>" class="form-control clscountry">
                <option value="">Select Country</option>
  <?php foreach ($countries as $cntry) { ?>
                  <option value="<?php echo $cntry['name']; ?>"><?php echo $cntry['name']; ?></option>
  <?php } ?>
              </select>
            </div>
            <div class="col-md-3" id="your_city">
              <select name="city" class="form-control clscity" id="city-<?php echo $araz_id; ?>">
                <option value="">Select City</option>
              </select>
            </div>
            <div class="col-xs-12">&nbsp;</div>
            <div class="col-md-12">
              <textarea name="remarks" class="form-control" id="remarks-<?php echo $araz_id; ?>" placeholder="Enter Remarks" class="alfatemi-text" dir="rtl"></textarea>
            </div>
          </div>
        </div>
      </div>
<?php 
  }
  if (in_array($_SESSION[USER_ROLE], array(ROLE_REVIEWER))) {
  ?>
      <div class="row clssubmitbchet">
        <?php
          if($araz_general_detail['reviewer_processed'] == '1'){
            $review_answer[0] = 'No';
            $review_answer[1] = 'Yes';
        ?>
        <div class="col-xs-12 col-sm-4 bottom10px">
          <strong>Recommend Psychometric Test : </strong> <?php echo $review_answer[$araz_general_detail['recommend_psychometric_test']] ?>
        </div>
        <div class="col-xs-12 col-sm-4 bottom10px">
          <strong>Recommend Counseling : </strong> <?php echo $review_answer[$araz_general_detail['recommend_counseling']] ?>
        </div>
        <div class="col-xs-12 col-sm-4 bottom10px">
          <strong>Recommend Qardan Hasana : </strong> <?php echo $review_answer[$araz_general_detail['recommend_qardan']] ?>
        </div>
        <div class="col-xs-12 col-sm-4 bottom10px">
          <strong>Recommend Sponsorship : </strong> <?php echo $review_answer[$araz_general_detail['recommend_sponsorship']] ?>
        </div>
        <?php
          } else {
        ?>
        <div class="col-md-8">
          <div class="col-md-6 col-xs-12">
            <div class="checkbox">
              <label>
                <input type="checkbox" name="recommend_test" class="recommend_test" value="1">Recommend Psychometric Test
              </label>
            </div>
          </div>

          <div class="col-md-6 col-xs-12">
            <div class="checkbox">
              <label>
                <input type="checkbox" name="recommend_coun" class="recommend_coun" value="1">Recommend Counseling
              </label>
            </div>
          </div>
          <div class="clearfix"></div>
          
          <div class="col-md-6 col-xs-12">
            <div class="checkbox">
              <label>
                <input type="checkbox" name="recommend_qardan" class="recommend_qardan" value="1">Recommend Qardan Hasana
              </label>
            </div>
          </div>

          <div class="col-md-6 col-xs-12">
            <div class="checkbox">
              <label>
                <input type="checkbox" name="recommend_sponsorship" class="recommend_sponsorship" value="1">Recommend Sponsorship
              </label>
            </div>
          </div>
        </div>
        <div class="col-md-4">
          <input type="hidden" name="araz_id" class="araz_id" value="<?php echo $araz_id; ?>" >
        
          <div class="col-md-6 col-xs-12" style="padding: 20px;">
            <button type="submit" name="submit" class="btn btn-success btn-block submitaraz">Submit</button>
          </div>
        </div>
        <?php
          }
        ?>
      </div>
      <div class="col-xs-12 col-md-12">&nbsp;</div>
  <?php
}
?>
  </div>
</div>
<hr>
