<div class="panel panel-primary main-araz-<?php echo $araz_id; ?>">
  <div class="panel-heading" style="font-size: 19px;">
    <div class="row">
      <div class="col-xs-12 col-sm-4">
        <input type="checkbox" id="jawab-<?php echo $araz_id; ?>" name="jawab[]" class="jawab css-checkbox1 <?php echo $user_details['araz_type']; ?>" value="<?php echo $araz_id; ?>">
        <label for="jawab-<?php echo $araz_id; ?>" class="css-label1">Track No: </strong><?php echo $araz_id; ?></label>
      </div>
      <div class="col-xs-12 col-sm-4 text-center"><strong>Marhala: </strong><?php echo $araz_general_detail['marhala']; ?></div>
      <?php
        $from = new DateTime($araz_general_detail['araz_ts']);
        $to = new DateTime('today');
        $araz_days_past = $from->diff($to)->days;
        
        if($counselor_details['coun_assign_ts'] > 0) {
          $coun_from = new DateTime($counselor_details['coun_assign_ts']);
          $coun_to = new DateTime('today');
          $counselor_days_past = $coun_from->diff($coun_to)->days;
        }
        
        if($araz_general_detail['saheb_assign_ts'] > 0) {
          $saheb_from = new DateTime($araz_general_detail['saheb_assign_ts']);
          $saheb_to = new DateTime('today');
          $saheb_days_past = $saheb_from->diff($saheb_to)->days;
        }
        ?>
      <div class="col-xs-12 col-sm-4 text-right example black_font" rel="popover" data-html="true" data-content="<?php 
        echo 'New Araz Days Past : '.$araz_days_past;
      if($counselor_days_past){
        echo '<br />';
        echo 'Counselor Assign Days Past : '.$counselor_days_past;
      }
      if($saheb_days_past){
        echo '<br />';
        echo 'Saheb Assign Days Past : '.$saheb_days_past;
      }
?>" data-placement="top"><strong>Days past: </strong> <?php echo $araz_days_past; if(in_array($_SESSION[USER_ROLE], array(ROLE_HEAD_OFFICE, ROLE_JAMIAT_COORDINATOR))){ ?> | <a href="#" class="confirm" id="<?php echo $araz_id; ?>"><i class="fa fa-remove fa-fw text-danger"></i></a><?php } ?></div>
      <div class="clearfix"></div>
    </div>
  </div>
  <?php if (!in_array($_SESSION[USER_ROLE], array(ROLE_COUNSELOR))) { ?>
  <div class="row">
    <div class="col-xs-12 col-sm-12 text-center alfatemi-text" dir="rtl">
      غب السجدات العبودية <br>
      في الحضرة الامامية النورانية اشرق الله انوارها
    </div>
  </div>
  <?php } ?>
  <div class="panel-body">
    <div class="panel panel-default">
      <div class="panel-heading">
        <strong>Personal Details: </strong><?php echo $user_details['user_full_name']; ?>
        <span class="pull-right"><a role="button" data-toggle="collapse" href="#collapse-personal-info<?php echo $araz_id; ?>" aria-expanded="false" aria-controls="collapseExample">Show / Hide</a></span>
        <div class="clearfix"></div>
      </div>
      <div class="panel-body" id="collapse-personal-info<?php echo $araz_id; ?>">
        <div class="row">
          <div class="col-xs-12 col-sm-10">
            <div class="col-xs-12 col-sm-4 bottom10px">
              <strong>ITS: </strong><?php echo $user_details['login_its']; ?>
            </div>
            <div class="col-xs-12 col-sm-4 bottom10px">
              <strong>Hifz Sanad: </strong><?php echo $user_details['hifz_sanad']; ?>
            </div>
            <div class="col-xs-12 col-sm-4 bottom10px">
              <strong>Age: </strong>
              <?php
              $from = new DateTime($user_details['dob']);
              $to = new DateTime('today');
              echo $from->diff($to)->y;
              ?>
            </div>
            <div class="col-xs-12 col-sm-4 bottom10px">
              <strong>Gender: </strong>
<?php echo ($user_details['gender'] == 'M') ? 'Male' : 'Female'; ?>
            </div>
            <div class="col-xs-12 col-sm-4 bottom10px">
              <strong>Nationality: </strong><?php echo $user_details['nationality']; ?>
            </div>
            <div class="col-xs-12 col-sm-4 bottom10px">
              <strong>Country: </strong><?php echo $user_details['country']; ?>
            </div>
            <div class="col-xs-12 col-sm-4 bottom10px">
              <strong>State: </strong><?php echo $user_details['state']; ?>
            </div>
            <div class="col-xs-12 col-sm-4 bottom10px">
              <strong>City: </strong><?php echo $user_details['city']; ?>
            </div>
            <div class="col-xs-12 col-sm-4 bottom10px">
              <strong>Jamaat: </strong><?php echo $user_details['jamaat']; ?>
            </div>
            <div class="col-xs-12 col-sm-4 bottom10px">
              <strong>Jamiyat: </strong><?php echo $user_details['jamiat']; ?>
            </div>
            <div class="col-xs-12 col-sm-4 bg-success bottom10px">
              <strong>Araz Type: </strong><?php echo ucfirst($user_details['araz_type']); ?>
            </div>
            <div class="col-xs-12 col-sm-4 bottom10px">
              <strong>Mobile: </strong><?php echo ucfirst($user_details['mobile']); ?>
            </div>
            <div class="col-xs-12 col-sm-4 bottom10px">
              <strong>Whatsapp: </strong><?php echo ucfirst($user_details['whatsapp']); ?>
            </div>
            <div class="col-xs-12 col-sm-8 bottom10px">
              <strong>email: </strong><?php echo ucfirst($user_details['email']); ?>
            </div>
            <div class="clearfix"></div>
          </div>
          <div class="col-xs-12 col-sm-2">
<?php
$user_araz = new mtx_user;
$user_araz->loaduser($user_details['login_its']);
?>
            <img class="img-responsive" src="<?php echo $user_araz->get_mumin_photo(); ?>">
          </div>
        </div>
      </div>
    </div>
    <?php
      if($araz_general_detail['saheb_id'] != 0 OR ($counselor_details['counselor_id'] != 0 && $counselor_details['counselor_sugest_timestamp'] == NULL)) {
    ?>
    <div class="panel panel-default">
      <div class="panel-body">
        <div class="row">
          <?php
          if($araz_general_detail['saheb_id'] != 0){
            $saheb_details = $user->get_all_sahebs($araz_general_detail['saheb_id']);
            if($saheb_details){
          ?>
          <div class="col-xs-12 col-sm-4 bottom10px">
            <strong>Saheb: </strong><?php echo $saheb_details[0]['full_name']; ?>
          </div>
          <div class="col-xs-12 col-sm-4 bottom10px">
            <strong>Email: </strong><?php echo $saheb_details[0]['email']; ?>
          </div>
          <div class="col-xs-12 col-sm-4 bottom10px">
            <strong>Mobile: </strong><?php echo $saheb_details[0]['mobile']; ?>
          </div>
          <?php
            }
          }
          if($counselor_details['counselor_id'] != 0 && $counselor_details['counselor_sugest_timestamp'] == NULL){
            $counselor_assign_details = $user->get_all_counselors($counselor_details['counselor_id']);
            if($counselor_assign_details){
          ?>
          <div class="col-xs-12 col-sm-4 bottom10px">
            <strong>Counselor: </strong><?php echo $counselor_assign_details[0]['full_name']; ?>
          </div>
          <div class="col-xs-12 col-sm-4 bottom10px">
            <strong>Email: </strong><?php echo $counselor_assign_details[0]['email']; ?>
          </div>
          <div class="col-xs-12 col-sm-4 bottom10px">
            <strong>Mobile: </strong><?php echo $counselor_assign_details[0]['mobile']; ?>
          </div>
          <?php
            }
          }
          ?>
          <div class="clearfix"></div>
        </div>
      </div>
    </div>
    <?php
      }
    ?>

    <!-- Araz General Details -->
    <?php
    unset($user_araz);
    if ($araz_general_detail['marhala'] < 5) {
      /*
       */
      ?>
      <div class="panel panel-default">
        <div class="panel-heading">
          <strong>Deeni Ta'aleem</strong>
          <span class="pull-right"><a role="button" data-toggle="collapse" href="#collapse-deeni-taaleem<?php echo $araz_id; ?>" aria-expanded="false" aria-controls="collapseExample">Show / Hide</a></span>
        <div class="clearfix"></div>
        </div>
        <div class="panel-body" id="collapse-deeni-taaleem<?php echo $araz_id; ?>">
          <div class="row">
            <div class="col-xs-12 col-sm-6 bottom10px">
              <strong>Madrasah Name: </strong><?php echo $araz_general_detail['madrasah_name']; ?>
            </div>
            <div class="col-xs-12 col-sm-6 bottom10px">
              <strong>Darajah: </strong><?php echo $araz_general_detail['madrasah_darajah']; ?>
            </div>
            <div class="col-xs-12 col-sm-6 bottom10px">
              <strong>Country: </strong><?php echo $araz_general_detail['madrasah_country']; ?>
            </div>
            <div class="col-xs-12 col-sm-6 bottom10px">
              <strong>City: </strong><?php echo $araz_general_detail['madrasah_city']; ?>
            </div>
            <div class="clearfix"></div>
          </div>
        </div>
      </div>

      <div class="panel panel-default">
        <div class="panel-heading">
          <strong>School Information</strong>
          <span class="pull-right"><a role="button" data-toggle="collapse" href="#collapse-school<?php echo $araz_id; ?>" aria-expanded="false" aria-controls="collapseExample">Show / Hide</a></span>
        <div class="clearfix"></div>
        </div>
        <div class="panel-body" id="collapse-school<?php echo $araz_id; ?>">
          <div class="row">
            <div class="col-xs-12 col-sm-6 bottom10px">
              <strong>School Name: </strong><?php echo $araz_general_detail['school_name']; ?>
            </div>
            <div class="col-xs-12 col-sm-6 bottom10px">
              <strong>School is: </strong><?php echo $araz_general_detail['school_filter']; ?>
            </div>
            <div class="col-xs-12 col-sm-6 bottom10px">
              <strong>Standard / Grade: </strong><?php echo $araz_general_detail['school_standard']; ?>
            </div>
            <div class="col-xs-12 col-sm-6 bottom10px">
              <strong>City: </strong><?php echo $araz_general_detail['school_city']; ?>
            </div>
            <div class="clearfix"></div>
          </div>
        </div>
      </div>
      <?php
    }

    // Previous Study Details
    if ($araz_general_detail['marhala'] > 4) {
      if($araz_general_detail['marhala'] > 5){
        if($previous_study_details['current_course'] == '' AND $previous_study_details['current_inst_name'] == ''){
          $previous_studies = $araiz->get_previous_study_data($course_details, $user_details['araz_type']);
          $curr_course_name = $previous_studies['course_name'];
          $curr_institute_name = $previous_studies['institute_name'];
          $curr_country_name = $previous_studies['institute_country'];
          $curr_city_name = $previous_studies['institute_city'];
        }else {
          $curr_course_name = $previous_study_details['current_course'];
          $curr_institute_name = $previous_study_details['current_inst_name'];
          $curr_country_name = $previous_study_details['current_inst_country'];
          $curr_city_name = $previous_study_details['current_inst_city'];
        }
        if (!in_array($_SESSION[USER_ROLE], array(ROLE_COUNSELOR))) {
      ?>
      <div class="row">
        <div class="col-xs-12 col-sm-12 text-center alfatemi-text" dir="rtl">
          ادبًا عرض كه<br>
          في الحال حسب الذيل تعليم حاصل كروطط ححهوطط
        </div>
      </div>
      <?php 
        }
          if($previous_study_details['current_course'] != '' OR $araz_general_detail['marhala'] < 6){
      ?>
      <div class="panel panel-default">
        <div class="panel-heading">
          <strong><?php echo ($previous_study_details['current_course'] == '') ? "Current" : "Previous " ?> Study Information</strong>
          <span class="pull-right"><a role="button" data-toggle="collapse" href="#collapse-previous-study<?php echo $araz_id; ?>" aria-expanded="false" aria-controls="collapseExample">Show / Hide</a></span>
        <div class="clearfix"></div>
        </div>
        <div class="panel-body" id="collapse-previous-study<?php echo $araz_id; ?>">
          <div class="row">
            <div class="col-xs-12 col-sm-6 bottom10px">
              <strong>Course: </strong><?php echo $curr_course_name; ?>
            </div>
            <div class="col-xs-12 col-sm-6 bottom10px">
              <strong>Institute: </strong><?php echo $curr_institute_name; ?>
            </div>
            <div class="col-xs-12 col-sm-6 bottom10px">
              <strong>Country: </strong><?php echo $curr_country_name; ?>
            </div>
            <div class="col-xs-12 col-sm-6 bottom10px">
              <strong>City: </strong><?php echo $curr_city_name; ?>
            </div>
            <div class="clearfix"></div>
          </div>
        </div>
      </div>
      <?php 
          }
        }
         if (!in_array($_SESSION[USER_ROLE], array(ROLE_COUNSELOR))) {
      ?>
      <div class="row">
        <div class="col-xs-12 col-sm-12 text-center alfatemi-text" dir="rtl">
  <?php
  if(isset($raza_already_obtained) && $raza_already_obtained == TRUE) {
    //echo 'المذكور تعليم حاصل كروانو شروع كري ححكا؛';
  } else {
    echo ($user_details['araz_type'] == 'raza') ? ' مزيد تعليم واسطسس عرض؛' : ' مزيد تعليم واسطسس استرشادًا عرض';
  }
  ?>
        </div>
      </div>
      <?php } ?>
      <div class="panel panel-success whitelist-data">
        <div class="panel-heading">
          <strong>Further Education Information</strong> 
          <span class="pull-right"><a role="button" data-toggle="collapse" href="#collapse-further-study<?php echo $araz_id; ?>" aria-expanded="false" aria-controls="collapseExample">Show / Hide</a> | <a href="edit_araz.php?araz_id=<?php echo $araz_id; ?>">Edit</a></span>
          <div class="clearfix"></div>
        </div>
        <div class="panel-body" id="collapse-further-study<?php echo $araz_id; ?>">
  <?php
  if (in_array($_SESSION[USER_ROLE], array(ROLE_HEAD_OFFICE,ROLE_SAHEB,ROLE_ADMIN_MARHALA_1,ROLE_ADMIN_MARHALA_2,ROLE_ADMIN_MARHALA_3,ROLE_ADMIN_MARHALA_4,ROLE_ADMIN_MARHALA_5,ROLE_ADMIN_MARHALA_6,ROLE_ADMIN_MARHALA_7,ROLE_ADMIN_MARHALA_8,ROLE_JAMIAT_COORDINATOR))) {
    ?>
            <div class="row">
              <div class="col-xs-12 col-sm-12 well well-sm text-info alfatemi-text" dir="rtl">
                <strong>Head Office Remarks: </strong><?php echo $araz_general_detail['ho_remarks_saheb']; ?>
              </div>
            </div>
            <?php
          }
          $sr = 0;
          foreach ($course_details as $course) {
            $sr++;
            if ($user_details['araz_type'] == 'istirshad') {
              ?>
              <div class="row">
                <div class="col-xs-12 col-sm-6">
                  <label>
                    <input type="radio" name="istirshad-course-<?php echo $araz_id; ?>" id="optionsRadios<?php echo $araz_id; ?>" value="<?php echo $course['araz_course_id']; ?>">
                    Option <?php echo $sr; ?>
                  </label>
                </div>
              </div>
    <?php
      }
        if ($araz_general_detail['marhala'] == 5) {
          $course_name = $course['course_name'];
          $lable = 'Standard';
        } else {
          $course_name = $course['course_name'];
          $lable = 'Duration';
        }

        $its_course = $its_city = $its_institute = FALSE;
        if($course_name != ''){
          $its_course = $araiz->get_data_of_table('tlb_its_course','course',$course_name);
        }

        if($course['institute_city'] != ''){
          $its_city = $araiz->get_data_of_table('tlb_its_city','city',$course['institute_city']);
        }

        if($course['institute_name'] != ''){
          $its_institute = $araiz->get_data_of_table('tlb_its_institute','institute',$course['institute_name']);
        }
      
      $course_link = (!$its_course) ? '<a href="#" id="whitelist-course">Whitelist</a>' : '';
      $city_link = (!$its_city) ? '<a href="#" id="whitelist-city">Whitelist</a>' : '';
      $institute_link = (!$its_institute) ? '<a href="#" id="whitelist-institute">Whitelist</a>' : '';
    ?>
            <div class="row well well-sm">
              <div class="col-xs-12 col-sm-6 bottom10px <?php if(!$its_course){ echo 'red'; } ?>">
                <strong>Course: </strong><?php echo ($course_name) ? $course_name . ' ' . $course_link : 'Not yet Decided'; ?>
              </div>
              <div class="col-xs-12 col-sm-6 bottom10px">
                <strong><?php echo $lable; ?>: </strong><?php echo $course['course_duration']; ?>
              </div>
              <div class="col-xs-12 col-sm-6 bottom10px">
                <strong>Start Month-Year: </strong><?php echo $course['course_started']; ?>
              </div>
              <div class="col-xs-12 col-sm-6 bottom10px <?php if(!$its_institute){ echo 'red'; } ?>">
                <strong>Institute: </strong><?php echo ($course['institute_name']) ? $course['institute_name'] . ' ' . $institute_link : 'Not yet Decided'; ?>
              </div>
              <div class="col-xs-12 col-sm-6 bottom10px">
                <strong>Country: </strong><?php echo ($course['institute_country']) ? $course['institute_country'] : 'Not yet Decided'; ?>
              </div>
              <div class="col-xs-12 col-sm-6 bottom10px <?php if(!$its_city){ echo 'red'; } ?>">
                <strong>City: </strong><?php echo ($course['institute_city']) ? $course['institute_city'] . ' ' . $city_link : 'Not yet Decided'; ?>
              </div>
              <div class="col-xs-12 col-sm-6 bottom10px">
                <strong>Accomodation: </strong><?php echo ($course['accomodation']) ? $course['accomodation'] : 'Not yet Decided'; ?>
              </div>
              <div class="clearfix"></div>
              <input type="hidden" value="<?php echo $course_name; ?>" class="whitelist-course">
              <input type="hidden" value="<?php echo $course['institute_name']; ?>" class="whitelist-institute">
              <input type="hidden" value="<?php echo $course['institute_city']; ?>" class="whitelist-city">
            </div>
        <?php
      }
      ?>
        </div>
      </div>
      <?php
    }
    //if (!in_array($_SESSION[USER_ROLE], array(ROLE_COUNSELOR))) {
      if ($counselor_details['counselor_id'] != 0 && $counselor_details['counselor_sugest_timestamp'] > 0) {
        $counselor_array = $araiz->get_counselor_details_of_user_admin($counselor_details['counselor_id']);
        ?>
        <div class="panel panel-default">
          <div class="panel-heading">
            <strong>Counseling Information</strong>
            <span class="pull-right"><a role="button" data-toggle="collapse" href="#collapse-counseling<?php echo $araz_id; ?>" aria-expanded="false" aria-controls="collapseExample">Show / Hide</a></span>
          <div class="clearfix"></div>
          </div>
          <div class="panel-body" id="collapse-counseling<?php echo $araz_id; ?>">
            <div class="row">
              <div class="col-xs-12 col-sm-6 bottom10px">
                <strong>Counselor Name: </strong><?php echo $counselor_array['full_name']; ?>
              </div>
              <div class="col-xs-12 col-sm-6 bottom10px">
                <strong>Counselor Email: </strong><?php echo $counselor_array['email']; ?>
              </div>
              <div class="col-xs-12 col-sm-6 bottom10px">
                <strong>Counselor Mobile: </strong><?php echo $counselor_array['mobile']; ?>
              </div>
              <div class="col-xs-12 col-sm-6 bottom10px">
                <strong>Suggested Course: </strong><?php echo $counselor_details['counselor_sugest_course']; ?>
              </div>
              <div class="col-xs-12 col-sm-6 bottom10px">
                <strong>Suggested Country: </strong><?php echo $counselor_details['counselor_sugest_country']; ?>
              </div>
              <div class="col-xs-12 col-sm-6 bottom10px">
                <strong>Suggested City: </strong><?php echo $counselor_details['counselor_sugest_city']; ?>
              </div>
              <div class="col-xs-12 col-sm-12 bottom10px alfatemi-text" dir="rtl">
                <strong>Counselor Remarks: </strong><?php echo $counselor_details['counselor_sugest_remarks']; ?>
              </div>
              <div class="clearfix"></div>
            </div>
          </div>
        </div>
        <?php
      }
    //}
    $questionaire_details = $araiz->get_question_answers_of_araz($araz_id);
    if ($questionaire_details) {
      $ary_answer[1] = 'ميں عزم كروطط ححهوطط';
      $ary_answer[2] = 'ميں هر ممكن كوشش كريس ';
      $ary_answer[3] = 'ميں كوشش كريس';
      $ary_answer[4] = 'ميطط اْئنده جواب اْثثيس';
      $answer_sr = 1;
      ?>
      <div class="panel panel-default">
        <div class="panel-heading">
          <strong>Questionnaire Information</strong>
          <span class="pull-right"><a role="button" data-toggle="collapse" href="#collapse-azaaim<?php echo $araz_id; ?>" aria-expanded="false" aria-controls="collapseExample">Show / Hide</a></span>
          <div class="clearfix"></div>
        </div>
        <div class="panel-body collapse" id="collapse-azaaim<?php echo $araz_id; ?>">
  <?php
  foreach ($questionaire_details as $ques_ans) {
    $cls_text_class = ($ques_ans['answer'] == 1) ? 'text-success' : 'text-danger';
    ?>
            <div class="row well well-sm">
              <div class="row">
                <div class="col-xs-12 col-sm-12 bottom10px alfatemi-text <?php echo $cls_text_class ?>" dir="rtl">
                  <strong><?php echo $answer_sr++; ?>: </strong><?php echo $ques_ans['question_preview']; ?>
                  <div class="clearfix"></div>
                  <strong>Answer: </strong><?php echo $ary_answer[$ques_ans['answer']]; ?>
                </div>
                <div class="clearfix"></div>
              </div>
            </div>
        <?php
      }
      ?>
        </div>
      </div>
  <?php
}
    $araz_chat_data = $araiz->get_chat_data_of_araz($araz_id);
?>
    <div class="panel panel-yellow">
      <div class="panel-heading">
        <strong>Chat Box</strong>
        <span class="pull-right"><a role="button" data-toggle="collapse" href="#collapse-chat-box<?php echo $araz_id; ?>" aria-expanded="false" aria-controls="collapseExample" style="color:#FFF;">Show / Hide</a></span>
        <div class="clearfix"></div>
      </div>
      <div class="panel-body" id="collapse-chat-box<?php echo $araz_id; ?>">
        <?php
        if($araz_chat_data) {
          foreach ($araz_chat_data as $chat) {
        ?>
          <div class="row well well-sm">
            <div class="row">
              <div class="col-xs-12 col-sm-12 bottom10px">
                <strong><?php echo $chat['full_name']; ?>: </strong>
                <div class="clearfix"></div>
                <?php echo $chat['message']; ?>
              </div>
              <div class="clearfix"></div>
            </div>
          </div>
        <?php
            }
          }
        ?>
      </div>
      <div class="box-footer">
        <div class="input-group clschat">
          <input class="form-control" id="chat-msg-<?php echo $araz_id; ?>" class="alfatemi-text" placeholder="Type message..." />
          <input type="hidden" class="chat_araz_id" value="<?php echo $araz_id; ?>">
          <div class="input-group-btn">
            <button class="btn btn-success araz_chat"><i class="fa fa-plus"></i></button>
          </div>
        </div>
        <div class="clearfix"></div>
      </div>
    </div>
    <?php
     if (in_array($_SESSION[USER_ROLE], array(ROLE_HEAD_OFFICE,ROLE_COUNSELOR,ROLE_JAMIAT_COORDINATOR))) {
       $preferred_contact_details = $araiz->get_user_data_by_its($user_details['login_its']);
    ?>
       <div class="panel panel-default">
        <div class="panel-heading">
          <strong>Preferred Mode of contact</strong>
          <span class="pull-right"><a role="button" data-toggle="collapse" href="#collapse-mode_of_contact<?php echo $araz_id; ?>" aria-expanded="false" aria-controls="collapseExample">Show / Hide</a></span>
          <div class="clearfix"></div>
        </div>
        <div class="panel-body" id="collapse-mode_of_contact<?php echo $araz_id; ?>">
          <div class="row">
            <?php 
              if ($preferred_contact_details['prefer_email'] == 1) { 
            ?>
            <div class="col-xs-12 col-sm-6 bottom10px">
              <strong>E-mail: </strong><?php echo $preferred_contact_details['email']; ?>
            </div>
            <?php 
              }
              if ($preferred_contact_details['prefer_call'] == 1) { 
            ?>
            <div class="col-xs-12 col-sm-6 bottom10px">
              <strong>Mobile: </strong><?php echo $preferred_contact_details['mobile']; ?>
            </div>
            <?php 
              }
              if ($preferred_contact_details['prefer_whatsapp'] == 1) { 
            ?>
            <div class="col-xs-12 col-sm-6 bottom10px">
              <strong>Whatsapp: </strong><?php echo $preferred_contact_details['whatsapp']; ?>
            </div>
            <?php 
              }
              if ($preferred_contact_details['prefer_viber'] == 1) { 
            ?>
            <div class="col-xs-12 col-sm-6 bottom10px">
              <strong>Skype: </strong><?php echo $preferred_contact_details['viber']; ?>
            </div>
            <?php 
              }
            ?>
            <div class="clearfix"></div>
          </div>
        </div>
      </div>
    <?php
     }
     
    $old_araiz_details = $araiz->get_old_araiz_details($user_details['login_its'], $araz_id);
      if ($old_araiz_details) {
    ?>
        <div class="panel panel-default">
        <div class="panel-heading">
          <strong>Old Araiz Details</strong>
          <span class="pull-right"><a role="button" data-toggle="collapse" href="#collapse-old_araiz<?php echo $araz_id; ?>" aria-expanded="false" aria-controls="collapseExample">Show / Hide</a></span>
          <div class="clearfix"></div>
        </div>
        <div class="panel-body" id="collapse-old_araiz<?php echo $araz_id; ?>">
          <?php
            foreach ($old_araiz_details as $old_araz) {
              if ($old_araz['araz_type'] == 'raza' OR ($old_araz['araz_type'] == 'istirshad' AND $old_araz['course_jawab_given'] == '1')) {
          ?>
            <div class="row">
              <div class="col-xs-12 col-sm-12 well well-sm text-info alfatemi-text" dir="rtl">
                <strong>Head Office Remarks: </strong><?php echo $old_araz['ho_remarks_saheb']; ?>
              </div>
            </div>
            <div class="row well well-sm">
              <div class="col-xs-12 col-sm-6 bottom10px">
                <strong>Track No: </strong><?php echo $old_araz['araz_id']; ?>
              </div>
              <div class="col-xs-12 col-sm-6 bottom10px">
                <strong>Marhala: </strong><?php echo $old_araz['marhala']; ?>
              </div>
              <?php
                echo ($old_araz['marhala'] > 4) ? $araiz->show_institute_data($old_araz) : $araiz->show_school_data($old_araz);
              ?>
            </div>
          <?php
              }
            }
          ?>
          <div class="clearfix"></div>
        </div>
      </div>
    <?php
      }
     
    if (!in_array($_SESSION[USER_ROLE], array(ROLE_COUNSELOR))) {
    ?>
    <div class="row">
      <div class="col-xs-12 col-sm-12 text-center alfatemi-text" dir="rtl">
        رزا انسس دعاء مبارك فضل فرماوا ادباً عرض ؛<br>
        <br>
        والسجدات
      </div>
      <div class="col-xs-12 col-sm-12 text-right">
        <strong>Araz Date: </strong><?php echo $araz_general_detail['araz_ts']; ?>
      </div>
    </div>
    <?php } ?>
  </div>
  <div class="panel-footer">
<?php
if (in_array($_SESSION[USER_ROLE], array(ROLE_HEAD_OFFICE,ROLE_SAHEB,ROLE_ADMIN_MARHALA_1,ROLE_ADMIN_MARHALA_2,ROLE_ADMIN_MARHALA_3,ROLE_ADMIN_MARHALA_4,ROLE_ADMIN_MARHALA_5,ROLE_ADMIN_MARHALA_6,ROLE_ADMIN_MARHALA_7,ROLE_ADMIN_MARHALA_8, ROLE_JAMIAT_COORDINATOR))) {
  ?>
    <div class="row for-assign-araz">
      <?php
        if ($counselor_and_saheb_content == COUNSELOR_AND_SAHEB_CONTENT) {
      ?>
      <div class="col-xs-12 col-md-12">
        <div class="row">
          <div class="col-xs-12 col-sm-3">
            <select name="counselor_id" class="form-control" id="counselor_id<?php echo $araz_id; ?>">
              <option value="">Select Counselor</option>
              <?php
              if ($counselors) {
                foreach ($counselors as $coun) {
                  $pending_araz_count = $araiz->count_total_assign_araz_to_counselor($coun['user_id']);
                  ?>
                  <option value="<?php echo $coun['user_id'] ?>"><?php echo $coun['full_name'].' - '.$pending_araz_count; ?></option>
                  <?php
                }
              }
              ?>
            </select>
          </div>
          <div class="col-xs-12 col-sm-3">
            <input type="submit" name="sent_counselor" id="sent_counselor" value="Assign Counselor" class="btn btn-success btn-block assign-counselor-araz">
          </div>
          <div class="col-xs-12 col-sm-3">
            <select name="saheb_id" class="form-control" id="saheb_id<?php echo $araz_id; ?>">
              <option value="">Select Saheb</option>
              <?php
              if ($sahebs) {
                foreach ($sahebs as $s) {
                  $pending_araz_count = $araiz->count_total_assign_araz_to_saheb(FALSE, FALSE, FALSE, $s['user_id']);
                  ?>
                  <option value="<?php echo $s['user_id'] ?>"><?php echo $s['full_name'].' - '.$pending_araz_count; ?></option>
                  <?php
                }
              }
              ?>
            </select>
          </div>
          <div class="col-xs-12 col-sm-3">
            <input type="submit" name="sent_saheb" id="sent_saheb" value="Assign Saheb" class="btn btn-success btn-block assign-saheb-araz">
          </div>
        </div>
      </div>
      <div class="col-xs-12 col-md-12">&nbsp;</div>
      <?php
        }
        if ($jawab_sent_content == JAWAB_SENT_CONTENT) {
      ?>
      <input type="hidden" class="assign_araz_id" value="<?php echo $araz_id; ?>">
      <div class="col-xs-12 col-md-12">
        <div class="row">
          <div class="col-xs-12 col-sm-9">
            <textarea class="form-control alfatemi-text" name="ho_remarks[]" id="ho-remarks-<?php echo $araz_id; ?>" placeholder="Enter HO Remarks.." rows="3" dir="rtl"></textarea>
          </div>
          <div class="col-xs-12 col-sm-3">
            <select name="jawab_city" class="form-control jawab_city" id="jawab_city<?php echo $araz_id; ?>">
              <option value="">Select City</option>
              <?php
              if ($jamaats) {
                foreach ($jamaats as $data) {
                  ?>
                  <option value="<?php echo $data['jamaat']; ?>"><?php echo $data['jamaat']; ?></option>
                  <?php
                }
              }
              ?>
            </select>
            <div class="clearfix"><br></div>
            <div class="btn-group">
              <button type="submit"  name="sent_jawab" id="sent_jawab" class="btn btn-success btn-block dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> Action <span class="caret"></span>
              </button>
              <ul class="dropdown-menu">
                <li><a href="#" class="assign-jawab-araz"><span>Send Jawab</span></a></li>
                <li><a href="#" class="assign-bazid-jawab-araz"><span>Send Bazid Jawab</span></a></li>
                <li><a href="#" class="assign-attend-later-araz"><span>Attend Later</span></a></li>
              </ul>
            </div>
          </div>
        </div>
      </div>
      <?php
        }
      ?>
    </div>
    
  <?php
}
if (in_array($_SESSION[USER_ROLE], array(ROLE_COUNSELOR))) {
  ?>
      <div class="row">
        <?php if($araz_general_detail['ho_remarks_counselor']){ ?>
        <div class="col-xs-12 col-md-12">
          <strong>Head Office Remarks: </strong><?php echo $araz_general_detail['ho_remarks_counselor']; ?>
        </div>
        <?php } ?>
        <div class="col-xs-12 col-md-12">&nbsp;</div>
        <div class="col-md-12 clslocation">
          <input type="hidden" class="assign_coun_sugg_araz_id" value="<?php echo $araz_id; ?>">
          <div class="row">
            <div class="col-md-6">
              <select name="course" class="form-control" id="course-<?php echo $araz_id; ?>">
                <option value="">Select Course</option>
  <?php foreach ($courses as $crs) { ?>
                  <option value="<?php echo $crs['course_name']; ?>"><?php echo $crs['course_name']; ?></option>
  <?php } ?>
              </select>
            </div>
            <div class="col-md-3">
              <select name="country" id="country-<?php echo $araz_id; ?>" class="form-control clscountry">
                <option value="">Select Country</option>
  <?php foreach ($countries as $cntry) { ?>
                  <option value="<?php echo $cntry['name']; ?>"><?php echo $cntry['name']; ?></option>
  <?php } ?>
              </select>
            </div>
            <div class="col-md-3" id="your_city">
              <select name="city" class="form-control clscity" id="city-<?php echo $araz_id; ?>">
                <option value="">Select City</option>
              </select>
            </div>
            <div class="col-xs-12">&nbsp;</div>
            <div class="col-xs-12 col-md-10">
              <textarea name="remarks" class="form-control alfatemi-text" id="remarks-<?php echo $araz_id; ?>" placeholder="Enter Remarks" dir="rtl"></textarea>
            </div>
            <div class="col-xs-12 col-md-2">
              <input type="submit" name="sent_araz_suggestion" id="sent_araz_suggestion" value="Send Comment" class="btn btn-success btn-block sent-araz-suggestion">
            </div>
          </div>
        </div>
      </div>
<?php 
  }
?>
  </div>
</div>
<hr>
