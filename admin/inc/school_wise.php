<table class="table table-bordered table-hover">
      <thead>
        <tr>
          <th>No.</th>
          <th>Student Name</th>
          <th>ITS ID</th>
          <th>Moze (City)</th>
          <th>Gender</th>
          <th>Contact</th>
        </tr>
      </thead>
      <tbody>
        <?php $no = 1; foreach($records as $s) {?>
        <tr>
          <td><?php echo $no++; ?></td>
          <td><?php $dataUser = $global->getNameById($s['login_its'], 'users', 'user_its'); echo $dataUser['childName']; ?></td>
          <td><?php echo $s['login_its']; ?></td>
          <td><?php $cityName = $global->getNameById($s['city'], 'city'); echo is_null($cityName) ? $s['city'] : $cityName['name']; ?></td>
          <td><?php echo 'No field for gender'; ?></td>
          <td><?php echo $dataUser['contact']; ?></td>
        </tr>
        <?php } ?>
      </tbody>
    </table>