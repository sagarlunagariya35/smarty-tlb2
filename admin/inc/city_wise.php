<table class="table table-bordered table-hover table-responsive">
      <thead>
        <tr>
          <th>No.</th>
          <th>Group Name</th>
          <th>City</th>
          <th>School</th>
          <th>Count</th>
        </tr>
      </thead>
      <tbody>
        <?php $no = 1; foreach($records as $c) {?>
        <tr>
          <td><?php echo $no++; ?></td>
          <td><?php $grpName = $global->getNameById($c['group_id'], 'groups'); echo $grpName['grpName']; ?></td>
          <td><?php echo $c['cityName']; ?></td>
          <td><?php echo $c['school']; ?></td>
          <td><?php echo $c['count']; ?></td>
        </tr>
        <?php } ?>
      </tbody>
    </table>