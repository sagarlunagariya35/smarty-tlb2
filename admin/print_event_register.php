<?php
require_once '../classes/class.database.php';
include 'classes/class.event.php';
$allowed_roles = array(ROLE_HEAD_OFFICE);
require_once 'session.php';

$evnt = new Event();

$title = 'List of Event Registers';
$description = '';
$keywords = '';
$active_page = 'list_event_register';

$select = $event_id = FALSE;

$event_id = (isset($_REQUEST['event_id'])) ? $_REQUEST['event_id'] : FALSE;
$select = $evnt->get_event_registers_by_event_id($event_id);

include ('print_header.php');
?>

<body style="padding: 10px;">
  <div>
    <p style="display: block; text-align: right"><?php echo date('d-m-Y H:i:s'); ?></p>
  </div>
  
  <div class="row">
    <div class="col-lg-12">
      <h3 class="page-header"><?php echo $title; ?></h3>
    </div>
    <!-- /.col-lg-12 -->
  </div>
  
  <div class="row">
    <div class="col-md-12">
      <table class="table table-responsive table-condensed">
        <thead>
          <tr>
            <th>Sr No.</th>
            <th>ITS</th>
            <th>Full Name</th>
            <th>Email</th>
            <th>Mobile</th>
          </tr>
        </thead>
        <tbody>
          <?php 
          if($select){
            $i = 1; 
            foreach($select as $data){

          ?>
          <tr>
            <td><?php echo $i++;?></td>
            <td><?php echo $data['its_id']; ?></td>
            <td><?php echo $data['first_name']. ' '. $data['last_name']; ?></a></td>
            <td><?php echo $data['email']; ?></td>
            <td><?php echo $data['mobile']; ?></td>
          </tr>
          <?php 
              } 
            } else { ?>
          <tr>
            <td class="text-center" colspan="5">No users to show.</td>
          </tr>
          <?php } ?>
        </tbody>
      </table>
    </div>
  </div>
</body>
</html>