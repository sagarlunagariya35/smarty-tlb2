<?php
require_once 'session.php';
require_once 'requires_login.php';

// Page bady class
$body_class = 'page-sub-page';

$marhala_id = $_SESSION[MARHALA_ID];

$marhala_school_array = array('1','2','3','4');

if($_SESSION['step'] != '3')
{
  if($_SESSION['step'] != '5')
  {
    if($_SESSION['step'] <= '5'){
      if(in_array($marhala_id,$marhala_school_array))
      {
        header('location: ' . SERVER_PATH . 'marhala-' . $marhala_id . '/' . get_marhala_slug($marhala_id) . '/school/');
      }else {
        header('location: ' . SERVER_PATH . 'marhala-' . $marhala_id . '/' . get_marhala_slug($marhala_id) . '/select-institute/');
      }
    }
  }  
}

switch ($marhala_id) {
  case 1:
    $panel_heading = 'Pre-Primary Araz';
    $std_heading = 'Pre-Primary';
    $panel_heading_ar = 'الحضانة';
    break;
  case 2:
    $panel_heading = 'Primary Araz';
    $std_heading = 'Primary';
    $panel_heading_ar = 'الابتدائية الأولى';
    break;
  case 3:
    $panel_heading = 'Std 5th to Std 7th Araz';
    $std_heading = 'Std 5th to Std 7th';
    $panel_heading_ar = 'الابتدائية الثانية';
    break;
  case 4:
    $panel_heading = 'Std 8th to 10th Araz';
    $std_heading = 'Std 8th to Std 10th';
    $panel_heading_ar = 'الثانوية الأولى';
    break;
  case 5:
    $panel_heading = 'Std 11th - 12th';
    $std_heading = 'Std 11th - 12th';
    $panel_heading_ar = 'الثانوية الثانية';
    break;
  case 6:
    $panel_heading = 'Graduation';
    $std_heading = 'Graduation';
    $panel_heading_ar = 'البكالوريا';
    break;
  case 7:
    $panel_heading = 'Post Graduation';
    $std_heading = 'Post Graduation';
    $panel_heading_ar = 'الماجيستار';
    break;
  case 8:
    $panel_heading = 'Diploma';
    $std_heading = 'Diploma';
    $panel_heading_ar = 'الدبلوم';
    break;
}

if (isset($_POST['submit'])) {
  // store all data to the session
  $_SESSION['ques'] = serialize($_POST);
  $_SESSION['step'] = '6';
  
  switch ($marhala_id) {
    case 1:
    case 2:
    case 3:
    case 4:
      header('location: ' . SERVER_PATH . "marhala-$marhala_id/" . get_marhala_slug($marhala_id) . '/preview/');
      break;
    case 5:
    case 6:
    case 7:
    case 8:
      header('location: ' . SERVER_PATH . "marhala-$marhala_id/" . get_marhala_slug($marhala_id) . '/preview-istirshad/');
      break;
  }
}

$araz_data = get_araz_data_by_join($_SESSION[USER_ITS]);

$ques = get_marhala_ques($marhala_id);
require_once 'inc/inc.header2.php';
?>

<style>
  .skip{
  display:block;
  max-width:300px;
  -webkit-transition: 0.4s;
  -moz-transition: 0.4s;
  -o-transition: 0.4s;
  transition: 0.4s;
  border: none;
  background-color: #155485;
  height: auto;
  line-height:20px;
  margin:0px auto 20px;
  padding:10px;
  outline: none;
  color:#FFF;
  text-transform:uppercase;
  text-align:center;
  font-weight:bold;
  font-size:12px;
  float:right;
  text-decoration: underline none;
}
</style>

<div class="container white-bg">
  <div class="col-md-12 col-sm-12">

    <div class="page-title">
      <p style="margin-top:5px;"><a href="<?php echo SERVER_PATH; ?>">Home</a> / <a href="<?php echo SERVER_PATH; ?>raza-araz/">Send Araz To Aqa Maula TUS</a> / <a href="<?php echo SERVER_PATH; ?>marahil/">Raza Araz for</a> <?php if($marhala_id != '1') { ?> / <a href="<?php echo SERVER_PATH . 'marhala-' . $marhala_id . '/' . get_marhala_slug($marhala_id) . '/madrasah'; ?>"><?php echo $panel_heading; ?> ( Madrasah )</a> <?php } ?>
        
      <?php if(in_array($marhala_id,$marhala_school_array)) { ?>
       / <a href="<?php echo SERVER_PATH . 'marhala-' . $marhala_id . '/' . get_marhala_slug($marhala_id) . '/school'; ?>"><?php echo $panel_heading; ?> ( School )</a> 
       <?php }else { ?>
      <?php if($marhala_id != '5'){ ?> / <a href="<?php echo SERVER_PATH . 'marhala-' . $marhala_id . '/' . get_marhala_slug($marhala_id) . '/choose-stream/'; ?>"><?php echo $panel_heading; ?> (Choose Stream)</a> <?php } ?> / <a href="<?php echo SERVER_PATH . 'marhala-' . $marhala_id . '/' . get_marhala_slug($marhala_id) . '/select-institute/'; ?>"><?php echo $panel_heading; ?> (Select Institute)</a> 
       <?php } ?> 
       / <a href="#" class="active">Istibsaar</a></p>
      
      <h1>Istibsaar<span class="alfatemi-text">&#1575;&#1604;&#1571;&#1606;&#1588;&#1591;&#1577;</span></h1>
    </div>
  </div>
  <div class="clearfix"></div> <!-- do not delete -->
  
  <?php if(isset($_SESSION[SUCCESS_MESSAGE]) OR isset($_SESSION[ERROR_MESSAGE])) { ?>
    <div class="row">
        <?php include_once 'inc/message.php'; ?>
    </div>
  <?php } ?>
  
  <div class="col-md-12 col-sm-12">
    <div class="page">
      <span class="block vmgn20"></span>
      <div class="clearfix"></div> <!-- do not delete -->
      
      <h4>Fill up this form to submit your araz for <?php echo $std_heading; ?> Education.<span class="alfatemi-text">&#1575;&#1587;&#1578;&#1576;&#1589;&#1575;&#1585;&#1575;&#1587;&#1578;&#1576;&#1589;&#1575;&#1585;&#1575;&#1587;&#1578;&#1576;&#1589;&#1575;&#1585;</span></h4>
      
      <form name="ques_form" class="forms1 white" action="<?php echo SERVER_PATH."marhala-$marhala_id/".  get_marhala_slug($marhala_id) . '/ques/'?>" method="post" onsubmit="return(validate());">
        
        <a class="skip" href="<?php echo SERVER_PATH."marhala-$marhala_id/".  get_marhala_slug($marhala_id) . '/ques_english/'?>" class="btn btn-color-grey-light">For English Click Here</a><br>
      
        <div class="clearfix"></div> <!-- do not delete -->
        
        <?php
            if($ques) {
              $i = 0;
              foreach($ques as $q) { 
                $i++;
                $check1 = $check2 = $check3 = FALSE;
                
                foreach ($araz_data as $ad){
                  if($q['id'] == $ad['ques_id']){
                    if($ad['answer'] == '1'){
                      $check1 = 'checked';
                    }else if($ad['answer'] == '2'){
                      $check2 = 'checked';
                    }else {
                      $check3 = 'checked';
                    }
                  }else if($i == $ad['ques_id']) {
                    if($ad['answer'] == '1'){
                      $check1 = 'checked';
                    }else if($ad['answer'] == '2'){
                      $check2 = 'checked';
                    }else {
                      $check3 = 'checked';
                    }
                  }
                }
        ?>
        
        <div class="blue-box1 rtl"> <!-- Add class rtl -->
          <h3><span class="inline-block bigger1 lh1 w500 left-mgn20"><?php echo $i; ?></span>
            <span class="inline-block lh3"><?php echo $q['question_preview']; ?></span>


          </h3>
        </div>
            <div class="clearfix"></div> <!-- do not delete -->
            <div class="col-md-4 col-sm-12">
              <div class="radiobuttons-holder1 rtl"> <!-- Add Class .rtl -->
                <input type="radio" name="q_<?php echo $q['id']; ?>" value="1" id="radio1<?php echo $q['id']; ?>" class="css-radiobutton1" <?php echo $check1; ?> />
                <label for="radio1<?php echo $q['id']; ?>" style="color: #000;" class="css-label1 radGroup3 lsd">&#1575;&#1604;&#1571;&#1606;&#1588;&#1591;&#1577;</label>
              </div>
            </div>
            <div class="col-md-4 col-sm-12">
              <div class="radiobuttons-holder1 rtl"> <!-- Add Class .rtl -->
                <input type="radio" name="q_<?php echo $q['id']; ?>" value="2" id="radio2<?php echo $q['id']; ?>" class="css-radiobutton1" <?php echo $check2; ?> />
                <label for="radio2<?php echo $q['id']; ?>" style="color: #000;" class="css-label1 radGroup3 lsd">&#1575;&#1604;&#1571;&#1606;&#1588;&#1591;&#1577;</label>
              </div>
            </div>
            <div class="col-md-4 col-sm-12">
              <div class="radiobuttons-holder1 rtl"> <!-- Add Class .rtl -->
                <input type="radio" name="q_<?php echo $q['id']; ?>" value="3" id="radio3<?php echo $q['id']; ?>" class="css-radiobutton1" <?php echo $check3; ?> />
                <label for="radio3<?php echo $q['id']; ?>" style="color: #000;" class="css-label1 radGroup3 lsd">&#1575;&#1604;&#1571;&#1606;&#1588;&#1591;&#1577;</label>
              </div>
            </div>
            <div class="clearfix"></div>
        <?php 
              }
            } else {
        ?>
            <div class="blue-box1 rtl"> <!-- Add class rtl -->
          <h3><span class="inline-block bigger1 lh1 w500 left-mgn20">1</span>
            <span class="inline-block lh3">&#1587;&#1608;&#1601; &#1578;&#1603;&#1608;&#1606; &#1602;&#1575;&#1583;&#1585;&#1577; &#1593;&#1604;&#1609; &#1581;&#1590;&#1608;&#1585; waaz &#1575;&#1604;&#1603;&#1575;&#1605;&#1604; &#1605;&#1606; &#1603;&#1604; &#1578;&#1587;&#1593;&#1577; &#1571;&#1610;&#1575;&#1605; &#1605;&#1606; &#1575;&#1588;&#1575;&#1585;&#1575; &#1605;&#1576;&#1575;&#1585;&#1603;&#1577; &#1591;&#1601;&#1604;&#1603;&#1567;</span>


          </h3>
        </div>
            <div class="clearfix"></div> <!-- do not delete -->
            <div class="col-md-4 col-sm-12">
              <div class="radiobuttons-holder1 rtl"> <!-- Add Class .rtl -->
                <input type="radio" name="q_1" value="1" id="radio11" class="css-radiobutton1" />
                <label for="radio11" style="color: #000;" class="css-label1 radGroup3 lsd">&#1575;&#1604;&#1571;&#1606;&#1588;&#1591;&#1577;</label>
              </div>
            </div>
            <div class="col-md-4 col-sm-12">
              <div class="radiobuttons-holder1 rtl"> <!-- Add Class .rtl -->
                <input type="radio" name="q_1" value="2" id="radio21" class="css-radiobutton1" />
                <label for="radio21" style="color: #000;" class="css-label1 radGroup3 lsd">&#1575;&#1604;&#1571;&#1606;&#1588;&#1591;&#1577;</label>
              </div>
            </div>
            <div class="col-md-4 col-sm-12">
              <div class="radiobuttons-holder1 rtl"> <!-- Add Class .rtl -->
                <input type="radio" name="q_1" value="3" id="radio31" class="css-radiobutton1" />
                <label for="radio31" style="color: #000;" class="css-label1 radGroup3 lsd">&#1575;&#1604;&#1571;&#1606;&#1588;&#1591;&#1577;</label>
              </div>
            </div>
            <div class="clearfix"></div>
        <?php
              }
        ?>
        
         
        
        <div class="clearfix"></div> <!-- do not delete -->

        <!-a target="_blank" href="#" class="multitextbuttton">Istibsaar<span>&#1575;&#1604;&#1571;&#1606;&#1588;&#1591;&#1577;</span></a-->
        <input type="submit" class="submit1" name="submit" value="Proceed with Araz"/>
        <div class="clearfix"></div> <!-- do not delete -->
      </form>
      <div class="clearfix"></div> <!-- do not delete -->
    </div>
  </div>
</div>

<script type="text/javascript">
// Form validation code will come here.
function validate()
{
  <?php foreach ($ques as $q) { ?>
      
   if($('input[name=q_<?php echo $q['id']; ?>]:checked').length<=0)
    {
     alert( "Please Select any Answer!" );
     return false;
    }
   
  <?php } ?>
}
//-->
</script>

<?php
require_once 'inc/inc.footer.php';
?>