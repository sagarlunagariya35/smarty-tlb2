<?php
require_once 'classes/class.user.admin.php';
require_once '../classes/class.user.php';
include 'classes/class.araiz.php';
$allowed_roles = array(ROLE_HEAD_OFFICE, ROLE_JAMIAT_COORDINATOR);
require_once 'session.php';

$user = new mtx_user_admin;
$araiz = new Araiz();

$title = 'Saheb Recommend To Counselor';
$description = '';
$keywords = '';
$active_page = "saheb_recommend_to_counselor";

$select = FALSE;
$from_date = $to_date = $pagination = FALSE;
$araz_saheb_id = $saheb_id = $araz_coun_id = $coun_id = FALSE;
$counselor_days_past = $saheb_days_past = $jamiyet_data = FALSE;

if(isset($_POST['print_list_raza']))
{
  $_SESSION['track_id'] = (array) $_POST['jawab'];
  header('location: print_raza_araiz_list_date_wise.php');
}

if(isset($_POST['print_list_istirshad']))
{
  $_SESSION['track_id'] = (array) $_POST['jawab'];
  header('location: print_istirshad_araiz_date_wise.php');
}

$jamiyet = unserialize($_SESSION['jamiat']);
if (count($jamiyet) == 1 && $jamiyet[0] != '') {
  $jamiyet_data = implode("','", $jamiyet[0]);
}

// Fix for ROLE_JAMIAT_COORDINATOR is no jamiat assign
if ($_SESSION[USER_ROLE] == ROLE_JAMIAT_COORDINATOR && $jamiyet_data === FALSE) {
  $jamiyet_data = ' ';
}

$select = $araiz->get_all_recommend_to_counselor_araz(FALSE, FALSE, $jamiyet_data);
$total_araz = $araiz->count_total_recommend_to_counselor_araz($jamiyet_data);

$araiz_parsed_data = $araiz->get_araiz_data_organised($select);
$counselors = $user->get_all_counselors();

include ('header.php');
$counselor_and_saheb_content = COUNSELOR_AND_SAHEB_CONTENT;
$jawab_sent_content = JAWAB_SENT_CONTENT;
?>

<link rel="stylesheet" href="js/tablesorter/css/theme.bootstrap.css">
<script src="js/tablesorter/js/jquery.tablesorter.js"></script>
<script src="js/tablesorter/js/jquery.tablesorter.widgets.js"></script>
<style>
  .alfatemi-text{
    font-size: 18px;
  }
</style>
<div class="row">

  <div class="row">
    <div class="col-xs-12 col-sm-12">&nbsp;</div>
  </div>
  <div class="row">
    <div class="col-xs-12 col-sm-12">
      <h2 class="text-center">Total Recommendation to Counselor Araiz Count: <?php echo $total_araz; ?></h2>
    </div>
  </div>
  <div class="row">
    <div class="col-xs-12 col-sm-12">&nbsp;</div>
  </div>
  <form name="raza_form" class="form" method="post" action="">
    <div class="row">
      <div class="col-xs-12 col-sm-3">
      </div>
      <div class="col-xs-12 col-sm-3">
      </div>
      <div class="col-xs-12 col-sm-3">
        <select name="counselor_id" class="form-control" id="counselor_id">
          <option value="">Select Counselor</option>
          <?php
          if ($counselors) {
            foreach ($counselors as $coun) {
              $pending_araz_count = $araiz->count_total_recommend_araz_to_counselor($coun['user_id']);
              ?>
              <option value="<?php echo $coun['user_id'] ?>"><?php echo $coun['full_name'].' - '.$pending_araz_count; ?></option>
              <?php
            }
          }
          ?>
        </select>
      </div>
      <div class="col-xs-12 col-sm-3">
        <input type="submit" name="sent_jawab" id="sent_jawab" value="Accept Recommendation" class="btn btn-success btn-block accept-recommend">
      </div>
    </div>
  
  <div class="col-lg-12 col-xs-12 col-sm-12 pull-left">
    <div class="col-md-12">&nbsp;</div>
    <div class="clearfix"></div>
    <div class="row">
      <ul class="nav nav-tabs nav-justified" role="tablist">
        <li role="presentation" class="active"><a href="#raza_araiz" aria-controls="pre" role="tab" data-toggle="tab">Raza Araiz</a></li>
        <li role="presentation"><a href="#istirshad_araiz" aria-controls="in" role="tab" data-toggle="tab">Isttirshad Araiz</a></li>
      </ul>
      <div class="row">
        <div class="col-xs-12 col-sm-12">&nbsp;</div>
      </div>
      <div class="tab-content">
        <div role="tabpanel" class="tab-pane active" id="raza_araiz">
          <div class="col-xs-12 col-sm-3" style="padding-top:6px">
            <input type="checkbox" name="print" value="Print" id="check_all_raza" onchange="checkAllRaza(this);" class="css-checkbox1"><label for="check_all_raza" class="css-label1">Check All</label>
          </div>
          <div class="col-xs-12 col-sm-3 pull-right">
            <input type="submit" name="print_list_raza" value="Print" class="btn btn-success btn-block assign-print">
          </div>
          <div class="row">
            <div class="col-xs-12 col-sm-12">&nbsp;</div>
          </div>
          <?php
          foreach ($araiz_parsed_data as $araz_id => $araz) {
            // Create bins for User Data, Araz Data, Previous Studies, Current Studeis, Remarks
            $user_details = $araz['user_data'];
            $araz_general_detail = $araz['araz_data'];
            $previous_study_details = $araz['previous_study_details'];
            $course_details = $araz['course_details'];
            $counselor_details = $araz['counselor_details'];

            if($user_details['araz_type'] == 'raza'){
              include 'inc/inc.show_parsed_araiz_details.php';
            }
          }
          ?>
        </div>
        
        <div role="tabpanel" class="tab-pane" id="istirshad_araiz">
          <div class="col-xs-12 col-sm-3" style="padding-top:6px">
            <input type="checkbox" name="print" value="Print" id="check_all_istirshad" onchange="checkAllIstirshad(this);" class="css-checkbox1"><label for="check_all_istirshad" class="css-label1">Check All</label>
          </div>
          <div class="col-xs-12 col-sm-3 pull-right">
            <input type="submit" name="print_list_istirshad" value="Print" class="btn btn-success btn-block assign-print">
          </div>
          <div class="row">
            <div class="col-xs-12 col-sm-12">&nbsp;</div>
          </div>
          <?php
          foreach ($araiz_parsed_data as $araz_id => $araz) {
            // Create bins for User Data, Araz Data, Previous Studies, Current Studeis, Remarks
            $user_details = $araz['user_data'];
            $araz_general_detail = $araz['araz_data'];
            $previous_study_details = $araz['previous_study_details'];
            $course_details = $araz['course_details'];
            $counselor_details = $araz['counselor_details'];

            if($user_details['araz_type'] == 'istirshad'){
              include 'inc/inc.show_parsed_araiz_details.php';
            }
          }
          ?>
        </div>
      </div>
    </div>
  </div>
</form>
</div>
<style>
  .popover{
    color:#000;
  }
</style>

<?php
include 'footer.php';
?>

