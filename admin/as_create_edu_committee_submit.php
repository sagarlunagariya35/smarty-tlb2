<?php

require_once '../classes/constants.php';
require_once '../classes/class.database.php';
require_once 'classes/admin_gen_functions.php';
require_once 'classes/class.user.admin.php';
require_once 'classes/class.edu_commitee.php';

$allowed_roles = array(ROLE_AAMIL_SAHEB);

require_once 'session.php';

// Check if Confirmed data. And if yes insert into the database
if (isset($_POST['cmd']) && $_POST['cmd'] == 'confirm') {

  $data = $_SESSION['tmp_comm_mem'];

  $edu_comm = new Edu_committee($_SESSION[TANZEEM_ID]);
  $edu_comm->save_members_araz($data);

  $msg = "Members added successfully.";
  //clear session
  unset($_SESSION['tmp_comm_mem']);
}

//check in database and fetchdata. 
$is_downloaded = FALSE;

$edu_comm = new Edu_committee($_SESSION[TANZEEM_ID]);
$edu_mem_from_db = $edu_comm->members_get_list($_SESSION[TANZEEM_ID]);
if (!$edu_mem_from_db) {
  $edu_mem_from_db = array();
}
foreach ($edu_mem_from_db as $mem) {
  if ($mem['downloaded']) {
    $is_downloaded = TRUE;
  }
}
if (!$_SESSION['tmp_comm_mem']) {
  $_SESSION['tmp_comm_mem'] = array();
}
// if the database and session both has data then merge them both
$edu_members = array_merge($edu_mem_from_db, $_SESSION['tmp_comm_mem']);

$title = 'Aamil Saheb back end Module';
$description = '';
$keywords = '';
$active_page = "create_edu_comm";

include_once("header.php");
?>


  <div class="row">
    <div class="col-lg-12">
      <p>Text for submit preview page</p>
      <p>Text for submit preview page</p>
      <p>Text for submit preview page</p>
      <p>Text for submit preview page</p>
      <p>Text for submit preview page</p>
      <p>Text for submit preview page</p>
      <p>Text for submit preview page</p>
    </div>
    <!-- /.col-lg-12 -->
  </div>
  <!-- /.row -->


  <!-- Show the ITS detail -->
  <?php
  //end if of isdownloaded
  // Show the committed committee members list
  if (isset($edu_members)) {
    ?>
    <div class="row">
      <div class="col-md-12">
        &nbsp;
      </div>
    </div>

    <div class="row">
      <div class="col-md-12">
        <table class="table table-responsive table-condensed">
          <thead>
            <tr>
              <th>Sr</th>
              <th>Photo</th>
              <th>Name</th>
              <th>Designation</th>
              <th>City</th>
              <th>Mobile</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td>1</td>
              <td></td>
              <td class="lsd">العامل الحاضر</td>
              <td>President</td>
              <td><?php echo $edu_comm->get_jamat_name() ?></td>
              <td></td>
            </tr>
            <tr>
              <td>2</td>
              <td></td>
              <td class="lsd" colspan="2">لجنة العمال والمعلمين والمراقبين</td>
              <td><?php echo $edu_comm->get_jamat_name() ?></td>
              <td></td>
            </tr>
            <?php
            $i = 2;
            $approved = 0;
            $rejected = 0;
            $downloaded = 0;
            $misal_uploaded = 0;
            foreach ($edu_members as $mem) {
              $i++;
              $tr_class = '';
              $status = 'status 1';
              
              if ($mem['ho_approved']) {
                $approved++;
                $status = 'Approved';
                $tr_class = 'bg-success';
              }
              if ($mem['ho_rejected']) {
                $rejected++;
                $status = 'Rejected';
                $tr_class = 'bg-danger';
              }
              if ($mem['downloaded']) {
                $downloaded++;
                $status = 'Submitted';
                $tr_class = 'bg-info';
              }
              if ($mem['misaal']) {
                $misal_uploaded++;
                $status = 'Misaal Uploaded';
                $tr_class = 'bg-primary';
              }

              $user = new mtx_user_admin();
              $user->get_mumeen_detail_from_its($mem['its']);
              $mem_img = $user->get_mumin_photo($mem['its']);
              if($mem['is_co_ordinator'] == 1){
                $co_ord = ', Hub Co ordinator';
              } else {
                $co_ord = '';
              }
              
              if($mem['is_mamur_co_ordinator'] == 1){
                $mamur_co_ord = ', Mamur Co ordinator';
              }else{
                $mamur_co_ord = '';
              }
              ?>
              <tr class="<?php echo $tr_class; ?>">
                <td rowspan="2"><?php echo $i; ?></td>
                <td rowspan="2"><img src="<?php echo $mem_img ?>" height="80px"></td>
                <td class="lsd"><?php echo $mem['name']; ?></td>
                <td><?php
                  echo $edu_comm->designation_by_id_to_string($mem['designation']);
                  echo $co_ord;
                  echo $mamur_co_ord;
                  ?></td>
                <td><?php echo $mem['jamaat']; ?></td>
                <td><?php echo $mem['mobile']; ?></td>
              </tr>
              <tr class="<?php echo $tr_class; ?>">
                <td><strong>Qualification: </strong><?php echo $mem['qualification']; ?></td>
                <td colspan="2"><strong>Occupation: </strong><?php echo $mem['occupation']; ?></td>
                <td><?php echo $mem['email']; ?></td>
              </tr>
              <?php
            }
            ?>
          </tbody>
        </table>
        <?php
        if (!$is_downloaded) {
          ?>

          <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
              <div class="modal-content">
                <div class="modal-body">
                  <p>Your Araz has been submitted successfully.</p>
                </div>
                <div class="modal-footer">
                  <a href="<?php echo SERVER_PATH; ?>admin/as_create_edu_committee.php" class="btn btn-info">OK</a>
                </div>
              </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
          </div><!-- /.modal -->
          
          <div class="col-md-3 col-md-offset-9">
          <button type="button" class="btn btn-success btn-lg btn-block" data-toggle="modal" data-target="#myModal">Done</button>
          </div>
        <?php } ?> 
      </div>
    </div>
    <?php
  }
  ?>
<script>
  $('.tooltip').tooltip({
    selector: "[data-toggle=tooltip]",
    container: "body"
  })
</script>
<!-- /#page-wrapper -->
<?php include "./footer.php"; ?>
