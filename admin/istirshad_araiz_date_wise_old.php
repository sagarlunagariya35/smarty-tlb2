<?php
require_once 'classes/class.user.admin.php';
include 'classes/class.araiz.php';
$allowed_roles = array(ROLE_HEAD_OFFICE,ROLE_AAMIL_SAHEB);
require_once 'session.php';

$user = new mtx_user_admin;
$araiz = new Araiz();

$title = 'Search Istirshad Araiz';
$description = '';
$keywords = '';
$active_page = "search_istirshad_araiz";

include ('header.php');

$select = FALSE;
$from_date = $to_date = $pagination = FALSE;
$date = date('Y-m-d');

if(isset($_POST['sent_jawab']))
{
  $id = (array) $_POST['jawab'];
  $jawab_city = $_POST['jawab_city'];
  
  if($id){
    foreach($id as $i)
    {
      $a_val = explode('-', $i);
      $i_araz_id = $a_val[0];
      $i_araz_c_id = $a_val[1];

      $res = $araiz->araz_send_jawab($i_araz_id,$jawab_city,$date,$i_araz_c_id);
      if($res)
      { $_SESSION[SUCCESS_MESSAGE] = "Jawab Sent successfully"; } else
      { $_SESSION[ERROR_MESSAGE] = "Error In Jawab Sending.."; }
    }
  }
}

if(isset($_POST['search']) OR $_POST){
  $from_date = $_POST['from_date'];
  $to_date = $_POST['to_date'];
  
  if(@$_SESSION[JAMAT_NAME]){
    $jamat = $_SESSION[JAMAT_NAME];
    $select = $araiz->get_istirshad_araz_datewise($from_date,$to_date,$jamat);
  }else {
    $select = $araiz->get_istirshad_araz_datewise($from_date,$to_date);
  }
}

$counselors = $user->get_all_counselors();
$sahebs = $user->get_all_sahebs();
?>

<link rel="stylesheet" href="js/tablesorter/css/theme.bootstrap.css">
<script src="js/tablesorter/js/jquery.tablesorter.js"></script>
<script src="js/tablesorter/js/jquery.tablesorter.widgets.js"></script>

  <div class="row">
   <form method="post" action="" name="raza_form" class="form-horizontal" onsubmit="return(validate());">
     <div class="col-lg-12">
        <h3 class="page-header"><?php echo $title; ?></h3>
      </div>
     <div class="col-md-9">
        <label class="col-md-1 control-label">From</label>
        <div class="col-md-3">
          <input type="date" name="from_date" class="form-control" value="<?php echo $from_date; ?>">
        </div>
        <label class="col-md-1 control-label">To</label>
        <div class="col-md-3">
          <input type="date" name="to_date" class="form-control" value="<?php echo $to_date; ?>">
        </div>
        <div class="col-md-3">
          <input type="submit" name="search" value="Search" class="btn btn-block btn-success">
        </div>
    </div>
    <?php if($_POST) { ?>
      <div class="col-md-9 pull-right">
        <div class="col-md-2 pull-right">
          <a href="print_istirshad_araiz_date_wise.php?from_date=<?php echo $from_date; ?>&to_date=<?php echo $to_date; ?>" target="_blank" class="btn btn-success btn-block">Print</a>
        </div>
      </div>
      <div class="col-lg-12 col-xs-12 col-sm-12 pull-left">
        <div class="col-md-12">&nbsp;</div>
        <div class="clearfix"></div>
        <div class="panel panel-primary">
          <div class="panel-heading">
            <i class="fa fa-group fa-fw"></i> <?php echo $title; ?>
          </div>
          <!-- /.panel-heading -->
          <div class="panel-body">
            <div class="table-responsive">
              <table class="table table-striped table-hover table-condensed tablesorter">
                <thead>
                  <th>No.</th>
                  <th class="text-center" style="width: 50px;">Track No</th>
                  <th>ITS</th>
                  <th>Name</th>
                  <th>Jamaat</th>
                  <th>Araz Type</th>
                  <th>City</th>
                  <th>Date</th>
                  <th>Days</th>
                </thead>
                <tbody>
                  <?php
                  $current_araz = null;
                  if($select){
                  $i = 1;
                  foreach ($select as $data) {
                    if($data['araz_id'] != $current_araz) {
                    $current_araz = $data['araz_id'];
          
                      $today = strtotime(date('Y-m-d'));
                      $start_date = strtotime($data['araz_ts']);
                      $datediff = $today - $start_date;
                      $sr = floor($datediff / (86400));
                      $count_jawab_send_araz = $araiz->count_jawab_send_araz_course_data($data['araz_id']);
                    ?>

                    <tr>
                      <td class="text-center"><?php echo $i++; ?></td>
                      <td class="text-center"><?php echo $data['araz_id']; ?></td>
                      <td><?php echo $data['login_its']; ?></td>
                      <td><?php echo $data['user_full_name']; ?></td>
                      <td><?php echo $data['jamaat']; ?></td>
                      <td><?php echo ucfirst($data['araz_type']); ?></td>
                      <td><?php echo $data['city']; ?></td>
                      <td><?php echo date('d, F Y',strtotime($data['araz_ts'])); ?></td>
                      <td><?php echo $sr; ?></td>
                    </tr>
                    
                    <tr>
                      <td></td>
                      <td colspan="9"><strong style="color:#CF4914;">Previous Study Details :</strong></td>
                    </tr>
                    <tr>
                      <td></td>
                      <td colspan='3'><strong>Degree / Course Name : </strong><?php echo $data['current_course']; ?></td>
                      <td colspan='3'><strong>Institute Name : </strong><?php echo $data['current_inst_name']; ?></td>
                      <td colspan='2'><strong>Institute City : </strong><?php echo $data['current_inst_city']; ?></td>
                    </tr>
                    
                    <tr>
                      <td></td>
                      <td colspan="9"><strong style="color:#09386C;">Current Study Details :</strong></td>
                    </tr>
                    <?php 
                      foreach ($select as $ac) {
                        if($ac['araz_id'] == $current_araz) {
                    ?>
                    <tr class="<?php if($ac['jawab_given'] == '1'){ echo 'success'; } ?>">
                      <td></td>
                      <td colspan='4'><strong>Degree / Course Name : </strong><?php echo $ac['course_name']; ?></td>
                      <td colspan='4'><strong>Institute Name : </strong><?php echo $ac['institute_name']; ?></td>
                    </tr>
                    <tr class="<?php if($ac['jawab_given'] == '1'){ echo 'success'; } ?>">
                      <td></td>
                      <td colspan='3'><strong>Course Duration : </strong><?php echo $ac['course_duration']; ?></td>
                      <td colspan='2'><strong>Institute City : </strong><?php echo $ac['institute_city']; ?></td>
                      <td colspan='2'><strong>Accomodation : </strong><?php echo $ac['accomodation']; ?></td>
                      <td class="text-center" colspan="1"><?php if($count_jawab_send_araz < '1'){ ?><input type="checkbox" id="jawab" name="jawab[]" class="jawab" value="<?php echo $data['araz_id'].'-'.$ac['araz_course_id']; ?>" ><?php }else if($ac['jawab_given'] == '1') { echo 'Yes'; } ?></td>
                    </tr>
                    <?php 
                        }
                      }
                    ?>
                    
                    <tr>
                      <td></td>
                      <td colspan='3'>
                        <select name="counselor[]" class="form-control" id="coun-<?php echo $data['araz_id']; ?>">
                          <option value="">Select Counselor</option>
                          <?php
                            if($counselors){
                              foreach ($counselors as $coun){
                          ?>
                          <option value="<?php echo $coun['user_id'] ?>" <?php if($coun['user_id'] == $data['counselor_id']) echo 'selected'; ?>><?php echo $coun['first_name'].' '.$coun['last_name']; ?></option>
                          <?php
                              }
                            }
                          ?>
                        </select>
                      </td>
                      <td colspan="1">
                        <input type="submit" name="sent_coun_<?php echo $data['araz_id']; ?>" id="sent_coun_<?php echo $data['araz_id']; ?>" value="Send Counselor" class="btn btn-success btn-block">
                      </td>
                      <td colspan='3'>
                        <select name="saheb[]" class="form-control" id="saheb-<?php echo $data['araz_id']; ?>">
                          <option value="">Select Saheb</option>
                          <?php
                            if($sahebs){
                              foreach ($sahebs as $s){
                          ?>
                          <option value="<?php echo $s['user_id'] ?>" <?php if($s['user_id'] == $data['saheb_id']) echo 'selected'; ?>><?php echo $s['first_name'].' '.$s['last_name']; ?></option>
                          <?php
                              }
                            }
                          ?>
                        </select>
                      </td>
                      <td colspan="1">
                        <input type="submit" name="sent_saheb_<?php echo $data['araz_id']; ?>" id="sent_saheb_<?php echo $data['araz_id']; ?>" value="Send Saheb" class="btn btn-success btn-block">
                      </td>
                    </tr>
                    <tr>
                      <td></td>
                      <td colspan="7">
                        <textarea class="form-control" name="ho_remarks" id="ho-remarks-<?php echo $data['araz_id']; ?>" placeholder="Enter HO Remarks.."><?php echo $data['ho_remarks']; ?></textarea>
                      </td>
                      <td colspan="1">
                        <input type="submit" name="sent_ho_remarks_<?php echo $data['araz_id']; ?>" id="sent_ho_remarks_<?php echo $data['araz_id']; ?>" value="Send Remarks" class="btn btn-success btn-block">
                      </td>
                    </tr>

                    <script>
                      $(document).ready(function(){
                        $('#sent_coun_' + <?php echo $data['araz_id']; ?>).on('click', function () {
                          var coun_id = $('#coun-' + <?php echo $data['araz_id']; ?>).val();
                          $.ajax({
                            type: "POST",
                            url: "ajax.php",
                            data: 'cmd=make_entry_of_counselor&araz_id=<?php echo $data['araz_id']; ?>&counselor_id=' + coun_id,
                            cache: false,
                            success: function () {
                            }
                          });
                        });

                        $('#sent_saheb_' + <?php echo $data['araz_id']; ?>).on('click', function () {
                          var saheb_id = $('#saheb-' + <?php echo $data['araz_id']; ?>).val();
                          $.ajax({
                            type: "POST",
                            url: "ajax.php",
                            data: 'cmd=make_entry_of_saheb&araz_id=<?php echo $data['araz_id']; ?>&saheb_id=' + saheb_id,
                            cache: false,
                            success: function () {
                            }
                          });
                        });
                        
                        $('#sent_ho_remarks_' + <?php echo $data['araz_id']; ?>).on('click', function () {
                          var ho_remarks = $('#ho-remarks-' + <?php echo $data['araz_id']; ?>).val();
                          $.ajax({
                            type: "POST",
                            url: "ajax.php",
                            data: 'cmd=make_entry_of_ho_remarks&araz_id=<?php echo $data['araz_id']; ?>&remarks=' + ho_remarks,
                            cache: false,
                            success: function () {
                            }
                          });
                        });
                      });
                    </script>
                    <tr><td colspan="9"></td></tr>
                    
                  <?php 
                      }
                    }
                  }
                  else
                  {
                    echo '<tr><td colspan="9" class="text-center">No Records</td></tr>';
                  }
                  ?>
                </tbody>
              </table>
              
              <div class="col-md-12">
                <div class="col-md-2 pull-right">
                  <input type="submit" name="sent_jawab" value="Send Jawab" class="btn btn-success btn-block">
                </div>
                <div class="col-md-3 pull-right">
                  <input type="text" name="jawab_city" placeholder="Enter Jawab City" class="form-control">
                </div>
              </div>
            </div>
            <!-- /.table-responsive -->
          </div>
          <!-- /.panel-body -->
        </div>
      </div>
    <?php } ?>
   </form>
  </div>
  <script>
    function checkAll(ele) {
     var checkboxes = document.getElementsByClassName('jawab');
     if (ele.checked) {
         for (var i = 0; i < checkboxes.length; i++) {
             if (checkboxes[i].type == 'checkbox') {
                 checkboxes[i].checked = true;
             }
         }
     } else {
         for (var i = 0; i < checkboxes.length; i++) {
             console.log(i)
             if (checkboxes[i].type == 'checkbox') {
                 checkboxes[i].checked = false;
             }
         }
     }
   }
   
   function validate()
  {
      var jawab = document.getElementsByClassName('jawab');
      var error = 'Following Error Encountered :\n\n';
      var validate = true;
      
      for (var i = 0; i < jawab.length; i++) {
        if (jawab[i].checked == true) {
          if(document.raza_form.jawab_city.value == ''){
            error = 'Please Enter Jawab City\n';
            validate = false;
          }
        }
      }
      
      if (validate == false)
      {
        alert(error);
        return validate;
      }
  }
  </script>
<?php
include 'footer.php';
?>
