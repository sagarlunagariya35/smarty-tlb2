<?php
include 'classes/class.crs_course.php';
include 'classes/class.crs_chapter.php';
$allowed_roles = array(ROLE_HEAD_OFFICE);
require_once 'session.php';

$crs_chapter = new Mtx_Crs_Chapter();
$crs_course = new Mtx_Crs_Course();

$title = 'Chapters List';
$description = '';
$keywords = '';
$active_page = "list_chapters";

if (isset($_POST['btn_submit'])) {
  $data = $db->clean_data($_POST);
  // get variables
  $course_id = $data['course_id'];
  $title = $data['title'];
  $slug = $db->clean_slug($_POST['slug']);
  $active = $data['active'];
  $sort = $data['sort'];
  $description = $data['description'];
  $start_date = $data['start_date'];
  $end_date = $data['end_date'];
  $user_id = $_SESSION[USER_ID];
  
  $last_insert_id = $crs_chapter->get_last_insert_chapter();
  $last_insert_id = $last_insert_id + 1;
  
  if($_FILES['img_icon']['name'] != ''){
    $icon_image = $_FILES['img_icon']['name'];
    $icon_image_temp = $_FILES['img_icon']['tmp_name'];
    
    $filearray = explode('.', $icon_image);
    $ext = $filearray[count($filearray)-1];
              
    $icon_image_name = $last_insert_id.'.'.$ext;
    $imagepathANDname = "../upload/courses/chapter/" . $icon_image_name;
    move_uploaded_file($icon_image_temp, $imagepathANDname);
  }
  
  $insert = $crs_chapter->insert_chapter($course_id, $title, $slug, $description, $icon_image_name, $start_date, $end_date, $sort, $user_id, $active);

  if ($insert) {
    $_SESSION[SUCCESS_MESSAGE] = 'Chapter Inserted Successfully.';
    header('Location: crs_chapter_update.php?id='.$last_insert_id);
    exit();
  } else {
    $_SESSION[ERROR_MESSAGE] = 'Error encountered while Inserting Chapter';
  }
}

$list_chapters = $crs_chapter->get_all_chapters();
$list_courses = $crs_course->get_all_courses();

include_once("header.php");
?>
<link rel="stylesheet" href="js/select2/select2.min.css">
<script src="js/select2/select2.full.min.js"></script>
<script type="text/javascript" src="ckeditor/ckeditor.js"></script>

<div class="row">
  <div class="col-lg-12">
    <h3 class="page-header">Add New Chapter</h3>
  </div>
  <!-- /.col-lg-12 -->
</div>

<div class="row">
  <div class="col-md-12">
    <form method="post" enctype="multipart/form-data">
      
      <div class="form-group col-md-4 col-xs-12">
        <label for="title">Course :</label>
        <select name="course_id" id="course_id" class="form-control course_id">
          <option value="">Select</option>
          <?php
          if ($list_courses) {
            foreach ($list_courses as $lc) {
          ?>
            <option value="<?php echo $lc['id']; ?>"><?php echo $lc['title']; ?></option>
          <?php
            }
          }
          ?>
        </select>
      </div>

      <div class="form-group col-md-4 col-xs-12">
        <label for="title">Title :</label>
        <input type="text" name="title" id="title" required class="form-control">
      </div>
      
      <div class="form-group col-md-4 col-xs-12">
        <label for="slug">Slug :</label>
        <input type="text" name="slug" id="slug" required class="form-control">
      </div>

      <div class="form-group col-md-4 col-xs-12">
        <label for="slug">Image icon :</label>
        <input type="file" name="img_icon" class="file">
      </div>

      <div class="form-group col-md-4 col-xs-12">
        <label for="active">Is Active? :</label>
        <select name="active" id="active" class="form-control" required >
          <option value="">Select</option>
          <option value="1">Yes</option>
          <option value="0">No</option>
        </select>
      </div>
      
      <div class="form-group col-md-4 col-xs-12">
        <label for="sort">Sort :</label>
        <input type="text" name="sort" id="sort" required class="form-control">
      </div>
      
      <div class="form-group col-md-4 col-xs-12">
        <label for="slug">Start Date :</label>
        <input type="date" name="start_date" class="form-control">
      </div>
      
      <div class="form-group col-md-4 col-xs-12">
        <label for="slug">End Date :</label>
        <input type="date" name="end_date" class="form-control">
      </div>
      
      <div class="form-group col-md-12 col-xs-12">
        <label for="miqat">Description :</label>
        <textarea name="description" class="form-control"></textarea>
      </div>
      
      <div class="clearfix"></div>
      <div class="form-group col-md-2 col-xs-12">
        <input type="submit" name="btn_submit" id="btn_submit" value="Submit" class="btn btn-primary btn-block form-control">
      </div>
    </form>

  </div>
</div>

<script type="text/javascript" src="js/course_details.js?v=1"></script>

<!-- /#page-wrapper -->
<?php include "./footer.php"; ?>
