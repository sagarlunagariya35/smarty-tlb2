<?php
include 'classes/class.courses.php';
$allowed_roles = array(ROLE_HEAD_OFFICE);
require_once 'session.php';

$course = new Mtx_Courses();

$title = 'Courses List';
$description = '';
$keywords = '';
$active_page = "list_courses";

if(isset($_GET['status']))
{
    $course_id = $_GET['course_id'];
    $status = $_GET['status'];
    if($status == '1') 
    {
        $From = 'Active';
        $To = 'Inactive';
        $Value = '0';
    }
    else
    {			
        $From = 'Inactive';
        $To = 'Active';
        $Value = '1';
    }
    
    $result = $course->change_course_status($Value, $course_id);
    
    if($result)
    {
        $_SESSION[SUCCESS_MESSAGE] = "Status is Changed from '$From' To '$To'";
        header('location: list_courses.php');
    }
    else
    {
       $_SESSION[ERROR_MESSAGE] = "Status does not Change";
    }
}

if (isset($_POST['btn_submit'])) {
  $data = $db->clean_data($_POST);
  // get variables
  $title = $data['title'];
  $slug = $data['slug'];
  $active = $data['active'];
  $miqat_id = $data['miqat_id'];
  $event_time = $data['event_time'];
  $description = $data['description'];
  $user_id = $_SESSION[USER_ID];

  $insert = $course->insert_course($title, $slug, $active, $miqat_id, $event_time, $description, $user_id);

  if ($insert) {
    $_SESSION[SUCCESS_MESSAGE] = 'Course Inserted Successfully.';
  } else {
    $_SESSION[ERROR_MESSAGE] = 'Error encountered while Inserting Course';
  }
}

$list_courses = $course->get_all_courses();

include_once("header.php");
?>
<script type="text/javascript" src="ckeditor/ckeditor.js"></script>

<div class="row">
  <div class="col-lg-12">
    <h3 class="page-header">Add New Course</h3>
  </div>
  <!-- /.col-lg-12 -->
</div>

<div class="row">
  <div class="col-md-12">
    <form method="post" enctype="multipart/form-data">

      <div class="form-group col-md-4 col-xs-12">
        <label for="title">Course Title :</label>
        <input type="text" name="title" id="title" required="required" class="form-control" >
      </div>

      <div class="form-group col-md-4 col-xs-12">
        <label for="slug">Course slug :</label>
        <input type="text" name="slug" id="slug" required="required" class="form-control" >
      </div>

      <div class="form-group col-md-4 col-xs-12">
        <label for="active">Is Active? :</label>
        <select name="active" id="active" class="form-control" required >
          <option value="1" selected >Yes</option>
          <option value="0">No</option>
        </select>
      </div>
      
      <div class="form-group col-md-4 col-xs-12">
        <label for="slug">Event Time :</label>
        <select name="event_time" id="event_time" class="form-control" required >
          <option value="">Select one</option>
          <option value="pre">Pre</option>
          <option value="in">In</option>
          <option value="post">Post</option>
        </select>
      </div>
      
      <div class="form-group col-md-4 col-xs-12">
        <label for="year">Select Year :</label>
        <select name="year" id="year" class="form-control" required >
          <option value="">Select Year</option>
          <option value="1435">1435</option>
          <option value="1436">1436</option>
          <option value="1437">1437</option>
        </select>
      </div>

      <div class="form-group col-md-4 col-xs-12">
        <label for="miqat">Select Miqat :</label>
        <select name="miqat_id" id="miqat" class="form-control" required >
          <option value="">Select Miqaat</option>
        </select>
      </div>
      
      <div class="form-group col-md-12 col-xs-12">
        <label for="miqat">Description :</label>
        <textarea name="description" class="form-control"></textarea>
      </div>
      
      <div class="clearfix"></div>
      <div class="form-group col-md-2 col-xs-12">
        <input type="submit" name="btn_submit" id="btn_submit" value="Submit" class="btn btn-primary btn-block form-control">
      </div>
    </form>

  </div>
</div>

<br>
<div class="panel panel-primary">
  <div class="panel-heading">
    <i class="fa fa-group fa-fw"></i> <?php echo $title; ?>
  </div>
  <!-- /.panel-heading -->
  <div class="panel-body">
    <div class="table-responsive">
      <table class="table table-striped table-hover table-condensed">
        <thead>
          <tr>
            <th>Sr</th>
            <th>Course Title</th>
            <th>Course Slug</th>
            <th>Date</th>
            <th class="text-center">Status</th>
          </tr>
        </thead>
        <tbody>
          <?php
          if($list_courses){
            $sr = 0;
            foreach ($list_courses as $c) {
              $sr++;
          ?>
              <tr class="<?php if($c['active'] == '1') { echo 'success'; } else { echo 'danger'; } ?>">
                <td><?php echo $sr; ?></td>
                <td><a href="courses_question.php?course_id=<?php echo $c['course_id'] ?>"><?php echo $c['course_title']; ?></a></td>
                <td><?php echo $c['course_slug']; ?></td>
                <td><?php echo date('d, F Y',  strtotime($c['created_ts'])); ?></td>
                <td class="text-center">
                    <?php if($c['active'] == '0') { ?>
                  <a href="list_courses.php?course_id=<?php echo $c['course_id']; ?>&status=<?php echo $c['active']; ?>"><i class="fa fa-check-circle fa-fw"></i></a>
                    <?php } else { ?>
                    <a href="list_courses.php?course_id=<?php echo $c['course_id']; ?>&status=<?php echo $c['active']; ?>" class="confirm"><i class="fa fa-ban fa-fw text-danger"></i></a>
                    <?php } ?>
                </td>
              </tr>
              <?php
            }
          }else {
            echo '<tr><td class="text-center" colspan="4">No Records..</td></tr>';
          }
          ?>
        </tbody>
      </table>
    </div>
    <!-- /.table-responsive -->
  </div>
  <!-- /.panel-body -->
</div>

<script type="text/javascript">
  $('#year').on('change', function (e) {
    $('#miqat').empty();
    var dropDown = document.getElementById("year");
    var year_val = dropDown.options[dropDown.selectedIndex].value;
    $.ajax({
      type: "POST",
      url: "ajax.php",
      data: {'cmd': 'get_miqat_list', 'year': year_val},
      success: function (data) {
        // Parse the returned json data
        var opts = $.parseJSON(data);
        // Use jQuery's each to iterate over the opts value
        $.each(opts, function (i, d) {
          // You will need to alter the below to get the right values from your json object.  Guessing that d.id / d.modelName are columns in your carModels data
          $('#miqat').append('<option value="' + d.id + '">' + d.miqaat_title + '</option>');
        });
      }
    });
  });
</script>

<!-- /#page-wrapper -->
<?php include "./footer.php"; ?>
