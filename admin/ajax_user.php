<?php
require_once 'classes/class.user.admin.php';
$allowed_roles = array(ROLE_HEAD_OFFICE);
require_once 'session.php';

$user = new mtx_user_admin;

$cmd = $_REQUEST['cmd'];

switch ($cmd) {

  case 'check_old_password':
    $result = $user->check_old_password($_GET['old_pwd'], $_GET['id']);
    echo $result;
    break;
}
?>
