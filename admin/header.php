<?php
//require_once 'session.php';

 // set the default active page
 require_once 'classes/class.araiz.php';
$page = 1;
$cls_araiz_header = new Araiz;

$counselor_and_saheb_content = 0;
$jawab_sent_content = 0;

if (isset($_GET['page']) && $_GET['page'] != '') {
  $page = $_GET['page'];
}

if (!@$jamiyet_data) {
  $jamiyet_data = FALSE;
}

$jamaats = $cls_araiz_header->get_all_lp_jamaat();
$jamiats = $cls_araiz_header->get_all_lp_jamiat();
?>
<?php
$getGroupID = FALSE;
if (isset($_GET['group']))
  $getGroupID = (int) $db->clean_data($_GET['group']);
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="<?php echo $description; ?>">
    <meta name="keywords" content="<?php echo $keywords; ?>">
    <meta name="author" content="">

    <title><?php echo $title; ?></title>

    
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/style.css?v=1" rel="stylesheet">
    <link href="css/fonts.css" rel="stylesheet">
    

    <!-- MetisMenu CSS -->
    <link href="css/plugins/metisMenu/metisMenu.min.css" rel="stylesheet">

    <!-- Timeline CSS -->
    <link href="css/plugins/timeline.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="css/sb-admin-2.css?v=1" rel="stylesheet">
    <link href="css/selectize.bootstrap3.css" rel="stylesheet">

    <!-- Morris Charts CSS -->
    <link href="css/plugins/morris.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

    <script src="js/jQuery-2.1.4.min.js"></script>
    <script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js" type="text/javascript"></script>
    <script src="js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="<?php echo SERVER_PATH; ?>templates/css/prettyPhoto.css" type="text/css" media="screen" title="prettyPhoto main stylesheet" />
    <script src="<?php echo SERVER_PATH; ?>templates/js/jquery.prettyPhoto.js" type="text/javascript"></script>
    
    <script src="js/jquery.confirm.min.js"></script>
    <script src="js/selectize.js"></script>
    <!--- Tinymce Plugin Javascript -->
    <script src="js/plugins/tinymce/js/tinymce/tinymce.min.js"></script>
  </head>

  <body>
    <div id="wrapper">

      <!-- Navigation -->
      <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <img src="images/logo.png" height="90" alt="Talabul ilm logo">
        </div>
        <!-- /.navbar-header -->
        <img class="img-responsive" src="../assets/img/talab_white.png" style="margin: 0 auto" alt="">
        <ul class="nav navbar-top-links navbar-right">
          <!-- /.dropdown -->
          <li class="dropdown">
            <a class="dropdown-toggle" data-toggle="dropdown" href="#">
              <i class="fa fa-user fa-fw"></i>  <i class="fa fa-caret-down"></i>
            </a>
            <ul class="dropdown-menu dropdown-user">
              <li class="divider"></li>
              <li><a href="logout.php"><i class="fa fa-sign-out fa-fw"></i> Logout</a>
              </li>
            </ul>
            <!-- /.dropdown-user -->
          </li>
          <!-- /.dropdown -->
        </ul>
        <!-- /.navbar-top-links -->

        <div class="navbar-default sidebar" role="navigation">
          <div class="sidebar-nav navbar-collapse">
            <ul class="nav" id="side-menu">
              
              <?php
              $already_started_araz = $cls_araiz_header->get_raza_araz_already_started_count(FALSE, FALSE, FALSE, FALSE, $jamiyet_data);
              $pen_raza_araz_count = $cls_araiz_header->get_pending_ho_raza_araz_count(FALSE, FALSE, FALSE, FALSE, $jamiyet_data);
              $pen_isti_araz_count = $cls_araiz_header->get_pending_ho_istirshad_araz_count(FALSE, FALSE, FALSE, FALSE, $jamiyet_data);
              $pen_printed_araz_count = $cls_araiz_header->get_printed_araz_count(FALSE, FALSE, FALSE, FALSE, $jamiyet_data);
              $pen_attend_later_araz_count = $cls_araiz_header->get_attend_later_araz_count(FALSE, FALSE, FALSE, FALSE, $jamiyet_data);
              $pen_counselor_araz_count = $cls_araiz_header->get_pending_araiz_of_counselor_count(FALSE, FALSE, FALSE, FALSE, FALSE, $jamiyet_data);
              $pen_saheb_araz_count = $cls_araiz_header->get_pending_araiz_of_saheb_count(FALSE, FALSE, FALSE, FALSE, FALSE, $jamiyet_data);
              $pen_rcmd_to_cou_count = $cls_araiz_header->count_total_recommend_to_counselor_araz($jamiyet_data);
              $alrdy_done_araz_count = $cls_araiz_header->get_already_done_araiz_count(FALSE, $jamiyet_data);
              $preview_araz_count = $cls_araiz_header->get_total_araz_preview($jamiyet_data);
              $jwb_sent_araz_count = $cls_araiz_header->get_jawab_sent_araiz_count(FALSE, $jamiyet_data);
              $get_all_araz_count = $cls_araiz_header->get_all_araz_count(FALSE, $jamiyet_data);
              // set the Entry page active if any sub page is active
              $link_active = ($active_page == 'home') ? 'class="active"' : '';
              if (in_array($_SESSION[USER_ROLE], array(ROLE_HEAD_OFFICE))) {
                
                ?>
                <li <?php echo $link_active; ?>>
                  <a href="home.php" <?php echo ($active_page == 'home') ? 'class="active"' : '' ?>><i class="fa fa-line-chart fa-fw"></i> Over View</a>
                </li>

                <?php
              }
                if (in_array($_SESSION[USER_ROLE], array(ROLE_HEAD_OFFICE,ROLE_JAMIAT_COORDINATOR))) {
                // set the Entry page active if any sub page is active
                $array_active = array('manage_araiz', 'manage_pending_araiz', 'manage_jawab_sent', 'pending_araiz_at_counselor', 'pending_araiz_at_saheb', 'raza_araiz_already_started', 'araz_already_done', 'search_raza_araiz','search_istirshad_araiz','search_araiz_by_multiple_data','search_printed_araiz','attend_later_araiz','araz_preview','saheb_recommend_to_counselor');
                $link_active = (in_array($active_page, $array_active)) ? 'class="active"' : '';
                ?>
                <li <?php echo $link_active; ?>>
                  <a href="#"><i class="fa fa-line-chart fa-fw"></i> Student Araiz<span class="fa arrow"></span></a>
                  <ul class="nav nav-second-level">
                    <li><a href="raza_araiz_already_started.php" <?php echo ($active_page == 'raza_araiz_already_started') ? 'class="active"' : '' ?>><i class="fa fa-file-text fa-fw"></i> Already Started <?php if($already_started_araz > 0){ ?><span class="badge pull-right"><?php echo $already_started_araz; ?></span><?php } ?></a></li>
                    <li><a href="raza_araiz_date_wise.php" <?php echo ($active_page == 'search_raza_araiz') ? 'class="active"' : '' ?>><i class="fa fa-file-text fa-fw"></i> Raza Araiz <?php if($pen_raza_araz_count > 0){ ?><span class="badge pull-right"><?php echo $pen_raza_araz_count; ?></span><?php } ?></a></li>
                    
                    <li><a href="istirshad_araiz_date_wise.php" <?php echo ($active_page == 'search_istirshad_araiz') ? 'class="active"' : '' ?>><i class="fa fa-file fa-fw"></i> Istirshad Araiz <?php if($pen_isti_araz_count > 0){ ?><span class="badge pull-right"><?php echo $pen_isti_araz_count; ?></span><?php } ?></a></li>
                    <li><a href="printed_araiz_date_wise.php" <?php echo ($active_page == 'search_printed_araiz') ? 'class="active"' : '' ?>><i class="fa fa-print fa-fw"></i> Printed Araiz <?php if($pen_printed_araz_count > 0){ ?><span class="badge pull-right"><?php echo $pen_printed_araz_count; ?></span><?php } ?></a></li>
                    
                    <li><a href="attend_later_araiz_date_wise.php" <?php echo ($active_page == 'attend_later_araiz') ? 'class="active"' : '' ?>><i class="fa fa-print fa-fw"></i> Attend Later Araiz <?php if($pen_attend_later_araz_count > 0){ ?><span class="badge pull-right"><?php echo $pen_attend_later_araz_count; ?></span><?php } ?></a></li>
                    
                    <li><a href="search_araiz_by_multiple.php" <?php echo ($active_page == 'search_araiz_by_multiple_data') ? 'class="active"' : '' ?>><i class="fa fa-search fa-fw"></i> Search By Multiple Fields</a></li>
                    
                    <li><a href="pending_araiz_counselor.php" <?php echo ($active_page == 'pending_araiz_at_counselor') ? 'class="active"' : '' ?>><i class="fa fa-question fa-fw"></i> Pending at Counselor <?php if($pen_counselor_araz_count > 0){ ?><span class="badge pull-right"><?php echo $pen_counselor_araz_count; ?></span><?php } ?></a></li>
                    
                    <li><a href="pending_araiz_saheb.php" <?php echo ($active_page == 'pending_araiz_at_saheb') ? 'class="active"' : '' ?>><i class="fa fa-question fa-fw"></i> Pending at Saheb  <?php if($pen_saheb_araz_count > 0){ ?><span class="badge pull-right"><?php echo $pen_saheb_araz_count; ?></span><?php } ?></a></li>
                    
                    <li><a href="saheb_recommend_to_counselor.php" <?php echo ($active_page == 'saheb_recommend_to_counselor') ? 'class="active"' : '' ?>><i class="fa fa-question fa-fw"></i> Saheb Recommend to Counselor  <?php if($pen_rcmd_to_cou_count > 0){ ?><span class="badge pull-right"><?php echo $pen_rcmd_to_cou_count; ?></span><?php } ?></a></li>
                    
                    <li><a href="araz_already_done.php" <?php echo ($active_page == 'araz_already_done') ? 'class="active"' : '' ?>><i class="fa fa-file-text fa-fw"></i> Alreay Done Araiz <?php if($alrdy_done_araz_count > 0){ ?><span class="badge pull-right"><?php echo $alrdy_done_araz_count; ?></span><?php } ?></a></li>
                    
                    <li><a href="araz_preview.php" <?php echo ($active_page == 'araz_preview') ? 'class="active"' : '' ?>><i class="fa fa-file-text fa-fw"></i> Araz Preview <?php if($preview_araz_count > 0){ ?><span class="badge pull-right"><?php echo $preview_araz_count; ?></span><?php } ?></a></li>
                    
                    <li><a href="list_jawab_sent_araiz.php" <?php echo ($active_page == 'manage_jawab_sent') ? 'class="active"' : '' ?>><i class="fa fa-envelope fa-fw"></i> Jawab Sent <?php if($jwb_sent_araz_count > 0){ ?><span class="badge pull-right"><?php echo $jwb_sent_araz_count; ?></span><?php } ?></a></li>
                    
                    <li><a href="araiz_list.php" <?php echo ($active_page == 'manage_araiz') ? 'class="active"' : '' ?>><i class="fa fa-list-ul fa-fw"></i> Total Araiz <?php if($get_all_araz_count > 0){ ?><span class="badge pull-right"><?php echo $get_all_araz_count; ?></span><?php } ?></a></li>
                  </ul>
                </li>

                <?php
              }
              if (in_array($_SESSION[USER_ROLE], array(ROLE_HEAD_OFFICE))) {
                /*
                // set the Entry page active if any sub page is active
                $array_active = array('Student_Record', 'School_Wise', 'City_group');
                $link_active = (in_array($active_page, $array_active)) ? 'class="active"' : '';
                ?>

                <li <?php echo $link_active; ?>>
                  <a href="#"><i class="fa fa-file-pdf-o fa-fw"></i> Reports<span class="fa arrow"></span></a>
                  <ul class="nav nav-second-level">
                    <li><a href="student_records.php" <?php echo ($active_page == 'Student_Record') ? 'class="active"' : '' ?>><i class="fa fa-group fa-fw"></i>City Wise Record</a></li>
                    <li><a href="school_wise.php" <?php echo ($active_page == 'School_Wise') ? 'class="active"' : '' ?>><i class="fa fa-question fa-fw"></i>school Wise Record </a></li>
                    <li><a href="city_wise_group.php" <?php echo ($active_page == 'City_group') ? 'class="active"' : '' ?>><i class="fa fa-plane fa-fw"></i>City Wise Record</a></li> 

                  </ul>
                  <!-- /.nav-second-level -->
                </li>

                <?php
                 * 
                 */
                // set the Entry page active if any sub page is active
                $array_active = array('ho_list_cities');
                $link_active = (in_array($active_page, $array_active)) ? 'class="active"' : '';
                ?>

                <li <?php echo $link_active; ?>>
                  <a href="#"><i class="fa fa-file-pdf-o fa-fw"></i> MHB Comm.<span class="fa arrow"></span></a>
                  <ul class="nav nav-second-level">
                    <li><a href="ho_list_gaam.php" <?php echo ($active_page == 'ho_list_cities') ? 'class="active"' : '' ?>><i class="fa fa-group fa-fw"></i>List of MHB Comm. Araz</a></li>

                  </ul>
                  <!-- /.nav-second-level -->
                </li>
                
                <?php
                // set the Entry page active if any sub page is active
                $array_active = array('manage_user', 'manage_group', 'manage_questions', 'manage_countries', 'manage_states', 'manage_cities', 'manage_lp_institutes', 'manage_queries', 'manage_menus', 'manage_arz_subjects', 'list_articles', 'manage_events', 'list_coun_req','list_miqaat_istibsaar', 'list_courses', 'list_ohbat_sentences', 'list_qasida');
                $link_active = (in_array($active_page, $array_active)) ? 'class="active"' : '';
                ?>
                <li <?php echo $link_active; ?>>
                  <a href="#"><i class="fa fa-edit fa-fw"></i> Entry Forms<span class="fa arrow"></span></a>
                  <ul class="nav nav-second-level">
                    <li><a href="user_manage.php" <?php echo ($active_page == 'manage_user') ? 'class="active"' : '' ?>><i class="fa fa-user fa-fw"></i>Manage Users</a></li>
                    <li><a href="groupname.php" <?php echo ($active_page == 'manage_group') ? 'class="active"' : '' ?>><i class="fa fa-group fa-fw"></i> Manage Groups</a></li>
                    <li><a href="questionary.php" <?php echo ($active_page == 'manage_questions') ? 'class="active"' : '' ?>><i class="fa fa-question fa-fw"></i> Manage Questionary</a></li>
                    <!--<li><a href="addcountry.php" <?php echo ($active_page == 'manage_countries') ? 'class="active"' : '' ?>><i class="fa fa-plane fa-fw"></i> Manage Countries</a></li> 
                    <li><a href="addstate.php" <?php echo ($active_page == 'manage_states') ? 'class="active"' : '' ?>><i class="fa fa-taxi fa-fw"></i> Manage States</a></li>
                    <li><a href="addcity.php" <?php echo ($active_page == 'manage_cities') ? 'class="active"' : '' ?>><i class="fa fa-bicycle fa-fw"></i> Manage Cities</a></li>-->
                    <li><a href="lp_institutes.php" <?php echo ($active_page == 'manage_lp_institutes') ? 'class="active"' : '' ?>><i class="fa fa-list-ul fa-fw"></i> Manage Institutes</a></li>
                    <li><a href="queries.php" <?php echo ($active_page == 'manage_queries') ? 'class="active"' : '' ?>><i class="fa fa-list-ul fa-fw"></i> Manage Queries</a></li>
                    <li><a href="list_menus.php" <?php echo ($active_page == 'manage_menus') ? 'class="active"' : '' ?>><i class="fa fa-list-ul fa-fw"></i> Manage Menus</a></li>
                    <li><a href="list_article.php" <?php echo ($active_page == 'list_articles') ? 'class="active"' : '' ?>><i class="fa fa-list-ul fa-fw"></i> Articles List</a></li>
                    <li><a href="events.php" <?php echo ($active_page == 'manage_events') ? 'class="active"' : '' ?>><i class="fa fa-list-ul fa-fw"></i> Events List</a></li>
                    <li><a href="list_miqaat_istibsaar.php" <?php echo ($active_page == 'list_miqaat_istibsaar') ? 'class="active"' : '' ?>><i class="fa fa-list-ul fa-fw"></i> Miqaat Istibsaar List</a></li>
                    <li><a href="list_ohbat_sentences.php" <?php echo ($active_page == 'list_ohbat_sentences') ? 'class="active"' : '' ?>><i class="fa fa-list-ul fa-fw"></i> Ohbat Sentence List</a></li>
                    <li><a href="list_qasida.php" <?php echo ($active_page == 'list_qasida') ? 'class="active"' : '' ?>><i class="fa fa-list-ul fa-fw"></i> Qasaid List</a></li>
                    <li><a href="list_courses.php" <?php echo ($active_page == 'list_courses') ? 'class="active"' : '' ?>><i class="fa fa-list-ul fa-fw"></i> Courses List</a></li>
                  </ul>
                  <!-- /.nav-second-level -->
                </li>
                
                <?php
                $array_active = array('list_topics','list_istifadah_master','list_crs_courses','list_chapters','list_announcements','list_crs_groups');
                $link_active = (in_array($active_page, $array_active)) ? 'class="active"' : '';
                ?>

                <li <?php echo $link_active; ?>>
                  <a href="#"><i class="fa fa-file-pdf-o fa-fw"></i> Courses<span class="fa arrow"></span></a>
                  <ul class="nav nav-second-level">
                    <!--li><a href="crs_istifadah_master.php" <?php echo ($active_page == 'list_istifadah_master') ? 'class="active"' : '' ?>><i class="fa fa-group fa-fw"></i> List of Istifadah Master</a></li-->
                    <li><a href="crs_topics.php" <?php echo ($active_page == 'list_topics') ? 'class="active"' : '' ?>><i class="fa fa-group fa-fw"></i> List of Subjects/Topics</a></li>
                    <li><a href="crs_courses.php" <?php echo ($active_page == 'list_crs_courses') ? 'class="active"' : '' ?>><i class="fa fa-group fa-fw"></i> List of Courses</a></li>
                    <li><a href="crs_chapter_list.php" <?php echo ($active_page == 'list_chapters') ? 'class="active"' : '' ?>><i class="fa fa-group fa-fw"></i> List of Chapters</a></li>
                    <li><a href="crs_announcements.php" <?php echo ($active_page == 'list_announcements') ? 'class="active"' : '' ?>><i class="fa fa-group fa-fw"></i> List of Announcements</a></li>
                    <li><a href="crs_groups.php" <?php echo ($active_page == 'list_crs_groups') ? 'class="active"' : '' ?>><i class="fa fa-group fa-fw"></i> List of Groups</a></li>
                  </ul>
                  <!-- /.nav-second-level -->
                </li>
                
                <?php
                $array_active = array('list_event_register','list_qasaid_log','list_tasavvuraat_araz','list_taken_course','list_araz_jamiat_wise','list_araz_mauze_wise','list_araz_marhala_wise','list_araz_stream_wise','list_report_survey');
                $link_active = (in_array($active_page, $array_active)) ? 'class="active"' : '';
                ?>

                <li <?php echo $link_active; ?>>
                  <a href="#"><i class="fa fa-file-pdf-o fa-fw"></i> Reports<span class="fa arrow"></span></a>
                  <ul class="nav nav-second-level">
                    <li><a href="list_event_register.php" <?php echo ($active_page == 'list_event_register') ? 'class="active"' : '' ?>><i class="fa fa-group fa-fw"></i> List of Event Register</a></li>
                    <li><a href="list_qasaid_log.php" <?php echo ($active_page == 'list_qasaid_log') ? 'class="active"' : '' ?>><i class="fa fa-group fa-fw"></i> List of Qasaid Log</a></li>
                    <li><a href="list_tasavvuraat_araz.php" <?php echo ($active_page == 'list_tasavvuraat_araz') ? 'class="active"' : '' ?>><i class="fa fa-list-ul fa-fw"></i> Tasavvuraat Araz List</a></li>
                    <li><a href="list_taken_course.php" <?php echo ($active_page == 'list_taken_course') ? 'class="active"' : '' ?>><i class="fa fa-list-ul fa-fw"></i> User Taken Course</a></li>
                    <li><a href="report_jamiat_wise.php" <?php echo ($active_page == 'list_araz_jamiat_wise') ? 'class="active"' : '' ?>><i class="fa fa-list-ul fa-fw"></i> Report Jamiat Wise</a></li>
                    <li><a href="report_mauze_wise.php" <?php echo ($active_page == 'list_araz_mauze_wise') ? 'class="active"' : '' ?>><i class="fa fa-list-ul fa-fw"></i> Report Mauze Wise</a></li>
                    <li><a href="report_marhala_wise.php" <?php echo ($active_page == 'list_araz_marhala_wise') ? 'class="active"' : '' ?>><i class="fa fa-list-ul fa-fw"></i> Report Marhala Wise</a></li>
                    <li><a href="report_stream_wise.php" <?php echo ($active_page == 'list_araz_stream_wise') ? 'class="active"' : '' ?>><i class="fa fa-list-ul fa-fw"></i> Report Stream/Course Wise</a></li>
                    <li><a href="report_survey.php" <?php echo ($active_page == 'list_report_survey') ? 'class="active"' : '' ?>><i class="fa fa-list-ul fa-fw"></i> Report Qism al-Tahfeez Survey</a></li>
                    <li><a href="araiz_csv_download.php"><i class="fa fa-list-ul fa-fw"></i> Download Araiz Data</a></li>
                  </ul>
                  <!-- /.nav-second-level -->
                </li>
                
                <?php
                $array_active = array('general_setting');
                $link_active = (in_array($active_page, $array_active)) ? 'class="active"' : '';
                ?>

                <li <?php echo $link_active; ?>>
                  <a href="#"><i class="fa fa-send-o fa-fw"></i> General Setting <span class="fa arrow"></span></a>
                  <ul class="nav nav-second-level">
                    <li><a href="general_setting.php" <?php echo ($active_page == 'general_setting') ? 'class="active"' : '' ?>><i class="fa fa-send-o fa-fw"></i>Set General Settings</a></li>

                  </ul>
                  <!-- /.nav-second-level -->
                </li>
                
                <?php
              }

              if (in_array($_SESSION[USER_ROLE], array(ROLE_AAMIL_SAHEB))) {
                // set the Entry page active if any sub page is active
                $array_active = array('create_edu_comm','manage_araiz','manage_pending_araiz','manage_jawab_sent','search_raza_araiz', 'search_istirshad_araiz', 'search_araiz_by_track');
                $link_active = (in_array($active_page, $array_active)) ? 'class="active"' : '';
                  ?>
                  <li <?php echo $link_active; ?>>
                    <a href="as_create_edu_committee.php" <?php echo ($active_page == 'create_edu_comm') ? 'class="active"' : '' ?>><i class="fa fa-line-chart fa-fw"></i> Create New Education Committee</a>
                  </li>
                  <li <?php echo $link_active; ?>>
                    <a href="#"><i class="fa fa-line-chart fa-fw"></i> Student Araiz<span class="fa arrow"></span></a>
                    <ul class="nav nav-second-level">
                        <li><a href="total_araiz_list.php" <?php echo ($active_page == 'manage_araiz') ? 'class="active"' : '' ?>><i class="fa fa-group fa-fw"></i> Total Araiz</a></li>
                        <li><a href="pending_araiz_list.php" <?php echo ($active_page == 'manage_pending_araiz') ? 'class="active"' : '' ?>><i class="fa fa-question fa-fw"></i> Pending Araiz</a></li>                        <li><a href="jawab_sent_araiz_list.php" <?php echo ($active_page == 'manage_jawab_sent') ? 'class="active"' : '' ?>><i class="fa fa-plane fa-fw"></i> Jawab Sent</a></li> 
                        <li><a href="search_araiz_by_track.php" <?php echo ($active_page == 'search_araiz_by_track') ? 'class="active"' : '' ?>><i class="fa fa-plane fa-fw"></i> Search Araiz By Track No</a></li>
                    </ul>
                    </li>
                  <?php
              }
              if (in_array($_SESSION[USER_ROLE], array(ROLE_COUNSELOR))) {
                // set the Entry page active if any sub page is active
                $array_active = array('manage_coun_araz','history_coun_araz');
                $link_active = (in_array($active_page, $array_active)) ? 'class="active"' : '';
                  ?>
                  <li <?php echo $link_active; ?>>
                    <a href="#"><i class="fa fa-line-chart fa-fw"></i> Entry Forms<span class="fa arrow"></span></a>
                    <ul class="nav nav-second-level">
                      <li><a href="counselor_araz_list.php" <?php echo ($active_page == 'manage_coun_araz') ? 'class="active"' : '' ?>><i class="fa fa-group fa-fw"></i> Araz List</a></li>
                      <li><a href="counselor_araz_history.php" <?php echo ($active_page == 'history_coun_araz') ? 'class="active"' : '' ?>><i class="fa fa-group fa-fw"></i> Araz History</a></li>
                    </ul>
                    </li>
                  <?php
              }
              if (in_array($_SESSION[USER_ROLE], array(ROLE_SAHEB))) {
                // set the Entry page active if any sub page is active
                $array_active = array('manage_saheb_araz','history_saheb_araz');
                $link_active = (in_array($active_page, $array_active)) ? 'class="active"' : '';
                  ?>
                  <li <?php echo $link_active; ?>>
                    <a href="#"><i class="fa fa-line-chart fa-fw"></i> Entry Forms<span class="fa arrow"></span></a>
                    <ul class="nav nav-second-level">
                      <li><a href="saheb_araz_list.php" <?php echo ($active_page == 'manage_saheb_araz') ? 'class="active"' : '' ?>><i class="fa fa-group fa-fw"></i> Araz List</a></li>
                      <li><a href="saheb_araz_history.php" <?php echo ($active_page == 'history_saheb_araz') ? 'class="active"' : '' ?>><i class="fa fa-group fa-fw"></i> Araz History</a></li>
                    </ul>
                    </li>
                  <?php
              }
              if (in_array($_SESSION[USER_ROLE], array(ROLE_REVIEWER))) {
                // set the Entry page active if any sub page is active
                $array_active = array('manage_reviewer_araz','manage_reviewer_history');
                $link_active = (in_array($active_page, $array_active)) ? 'class="active"' : '';
                  ?>
                  <li <?php echo $link_active; ?>>
                    <a href="#"><i class="fa fa-line-chart fa-fw"></i> Entry Forms<span class="fa arrow"></span></a>
                    <ul class="nav nav-second-level">
                      <li><a href="reviewer_araz_list.php" <?php echo ($active_page == 'manage_reviewer_araz') ? 'class="active"' : '' ?>><i class="fa fa-group fa-fw"></i> Pending Applications</a></li>
                      <li><a href="reviewer_araz_history.php" <?php echo ($active_page == 'manage_reviewer_history') ? 'class="active"' : '' ?>><i class="fa fa-group fa-fw"></i> Applications History</a></li>
                    </ul>
                    </li>
                  <?php
              }
              if (in_array($_SESSION[USER_ROLE], array(ROLE_PROJECT_COORDINATOR))) {
                // set the Entry page active if any sub page is active
                $array_active = array('manage_project_araz','project_araz_mark_complete');
                $link_active = (in_array($active_page, $array_active)) ? 'class="active"' : '';
                  ?>
                  <li <?php echo $link_active; ?>>
                    <a href="#"><i class="fa fa-line-chart fa-fw"></i> Entry Forms<span class="fa arrow"></span></a>
                    <ul class="nav nav-second-level">
                      <li><a href="coordinator_araz_list.php" <?php echo ($active_page == 'manage_project_araz') ? 'class="active"' : '' ?>><i class="fa fa-group fa-fw"></i> Project Applications</a></li>
                      <li><a href="mark_completed.php" <?php echo ($active_page == 'project_araz_mark_complete') ? 'class="active"' : '' ?>><i class="fa fa-group fa-fw"></i> Mark Completed</a></li>
                    </ul>
                  </li>
                  <?php
                    $array_active = array('report_psychometric_test','report_psychometric_test_taken','report_counseling','report_counseling_done','report_qardan_hasana','report_qardan_hasana_done','report_sponsorship','report_sponsorship_done');
                    $link_active = (in_array($active_page, $array_active)) ? 'class="active"' : '';
                  ?>
                  <li <?php echo $link_active; ?>>
                    <a href="#"><i class="fa fa-line-chart fa-fw"></i> Reports<span class="fa arrow"></span></a>
                    <ul class="nav nav-second-level">
                      <li><a href="report_psychometric_test.php" <?php echo ($active_page == 'report_psychometric_test') ? 'class="active"' : '' ?>><i class="fa fa-group fa-fw"></i> Psychometric Test</a></li>
                      <li><a href="report_psychometric_test_taken.php" <?php echo ($active_page == 'report_psychometric_test_taken') ? 'class="active"' : '' ?>><i class="fa fa-group fa-fw"></i> Psychometric Test Taken</a></li>
                      <li><a href="report_recommend_counseling.php" <?php echo ($active_page == 'report_counseling') ? 'class="active"' : '' ?>><i class="fa fa-group fa-fw"></i> Counseling</a></li>
                      <li><a href="report_recommend_counseling_done.php" <?php echo ($active_page == 'report_counseling_done') ? 'class="active"' : '' ?>><i class="fa fa-group fa-fw"></i> Counseling Done</a></li>
                      <li><a href="report_qardan_hasana.php" <?php echo ($active_page == 'report_qardan_hasana') ? 'class="active"' : '' ?>><i class="fa fa-group fa-fw"></i> Qardan Hasana</a></li>
                      <li><a href="report_qardan_hasana_done.php" <?php echo ($active_page == 'report_qardan_hasana_done') ? 'class="active"' : '' ?>><i class="fa fa-group fa-fw"></i> Qardan Hasana Done</a></li>
                      <li><a href="report_sponsorship.php" <?php echo ($active_page == 'report_sponsorship') ? 'class="active"' : '' ?>><i class="fa fa-group fa-fw"></i> Sponsorship</a></li>
                      <li><a href="report_sponsorship_done.php" <?php echo ($active_page == 'report_sponsorship_done') ? 'class="active"' : '' ?>><i class="fa fa-group fa-fw"></i> Sponsorship Done</a></li>
                    </ul>
                  </li>
                  <?php
              }
              if (in_array($_SESSION[USER_ROLE], array(ROLE_ADMIN_MARHALA_1,ROLE_ADMIN_MARHALA_2,ROLE_ADMIN_MARHALA_3,ROLE_ADMIN_MARHALA_4,ROLE_ADMIN_MARHALA_5,ROLE_ADMIN_MARHALA_6,ROLE_ADMIN_MARHALA_7,ROLE_ADMIN_MARHALA_8))) {
                // set the Entry page active if any sub page is active
                $array_active = array('manage_marhala_araz');
                $link_active = (in_array($active_page, $array_active)) ? 'class="active"' : '';
                  ?>
                  <li <?php echo $link_active; ?>>
                    <a href="#"><i class="fa fa-line-chart fa-fw"></i> Entry Forms<span class="fa arrow"></span></a>
                    <ul class="nav nav-second-level">
                      <li><a href="marhala_araz_list.php" <?php echo ($active_page == 'manage_marhala_araz') ? 'class="active"' : '' ?>><i class="fa fa-group fa-fw"></i> Araz List</a></li>
                    </ul>
                    </li>
                  <?php
              }
              if (in_array($_SESSION[USER_ROLE], array(ROLE_ASHARAH_ADMIN))) {
                // set the Entry page active if any sub page is active
                $array_active = array('manage_reviewer_araz','manage_reviewer_history','manage_project_araz','project_araz_mark_complete','manage_user');
                $link_active = (in_array($active_page, $array_active)) ? 'class="active"' : '';
                  ?>
                  <li <?php echo $link_active; ?>>
                    <a href="#"><i class="fa fa-line-chart fa-fw"></i> Entry Forms<span class="fa arrow"></span></a>
                    <ul class="nav nav-second-level">
                      <li><a href="reviewer_araz_list.php" <?php echo ($active_page == 'manage_reviewer_araz') ? 'class="active"' : '' ?>><i class="fa fa-group fa-fw"></i> Pending Applications</a></li>
                      <li><a href="reviewer_araz_history.php" <?php echo ($active_page == 'manage_reviewer_history') ? 'class="active"' : '' ?>><i class="fa fa-group fa-fw"></i> Applications History</a></li>
                      <li><a href="coordinator_araz_list.php" <?php echo ($active_page == 'manage_project_araz') ? 'class="active"' : '' ?>><i class="fa fa-group fa-fw"></i> Project Applications</a></li>
                      <li><a href="mark_completed.php" <?php echo ($active_page == 'project_araz_mark_complete') ? 'class="active"' : '' ?>><i class="fa fa-group fa-fw"></i> Mark Completed</a></li>
                      <li><a href="user_manage.php" <?php echo ($active_page == 'manage_user') ? 'class="active"' : '' ?>><i class="fa fa-user fa-fw"></i>Manage Users</a></li>
                    </ul>
                    </li>
                    <?php
                    $array_active = array('report_psychometric_test','report_psychometric_test_taken','report_counseling','report_counseling_done','report_qardan_hasana','report_qardan_hasana_done','report_sponsorship','report_sponsorship_done');
                    $link_active = (in_array($active_page, $array_active)) ? 'class="active"' : '';
                  ?>
                  <li <?php echo $link_active; ?>>
                    <a href="#"><i class="fa fa-line-chart fa-fw"></i> Reports<span class="fa arrow"></span></a>
                    <ul class="nav nav-second-level">
                      <li><a href="report_psychometric_test.php" <?php echo ($active_page == 'report_psychometric_test') ? 'class="active"' : '' ?>><i class="fa fa-group fa-fw"></i> Psychometric Test</a></li>
                      <li><a href="report_psychometric_test_taken.php" <?php echo ($active_page == 'report_psychometric_test_taken') ? 'class="active"' : '' ?>><i class="fa fa-group fa-fw"></i> Psychometric Test Taken</a></li>
                      <li><a href="report_recommend_counseling.php" <?php echo ($active_page == 'report_counseling') ? 'class="active"' : '' ?>><i class="fa fa-group fa-fw"></i> Counseling</a></li>
                      <li><a href="report_recommend_counseling_done.php" <?php echo ($active_page == 'report_counseling_done') ? 'class="active"' : '' ?>><i class="fa fa-group fa-fw"></i> Counseling Done</a></li>
                      <li><a href="report_qardan_hasana.php" <?php echo ($active_page == 'report_qardan_hasana') ? 'class="active"' : '' ?>><i class="fa fa-group fa-fw"></i> Qardan Hasana</a></li>
                      <li><a href="report_qardan_hasana_done.php" <?php echo ($active_page == 'report_qardan_hasana_done') ? 'class="active"' : '' ?>><i class="fa fa-group fa-fw"></i> Qardan Hasana Done</a></li>
                      <li><a href="report_sponsorship.php" <?php echo ($active_page == 'report_sponsorship') ? 'class="active"' : '' ?>><i class="fa fa-group fa-fw"></i> Sponsorship</a></li>
                      <li><a href="report_sponsorship_done.php" <?php echo ($active_page == 'report_sponsorship_done') ? 'class="active"' : '' ?>><i class="fa fa-group fa-fw"></i> Sponsorship Done</a></li>
                    </ul>
                  </li>
                  <?php
              }
              if (in_array($_SESSION[USER_ROLE], array(ROLE_DATA_ENTRY))) {
                // set the Entry page active if any sub page is active
                $array_active = array('manage_queries');
                $link_active = (in_array($active_page, $array_active)) ? 'class="active"' : '';
                ?>
                <li <?php echo $link_active; ?>>
                  <a href="#"><i class="fa fa-edit fa-fw"></i> Entry Forms<span class="fa arrow"></span></a>
                  <ul class="nav nav-second-level">
                    <li><a href="queries.php" <?php echo ($active_page == 'manage_queries') ? 'class="active"' : '' ?>><i class="fa fa-list-ul fa-fw"></i> Manage Queries</a></li>
                  </ul>
                  <!-- /.nav-second-level -->
                </li>
                  <?php
              }
              ?>

            </ul>
            <img class="img-responsive" src="../assets/img/mahad_hasnaat_logo.jpg" style="margin: 0 auto">

          </div>
          <!-- /.sidebar-collapse -->
        </div>
        <!-- /.navbar-static-side -->
      </nav>
      
<div id="page-wrapper">
  <div class="row">
    <?php include_once 'inc/message.php' ?>
  </div>
  <?php
  unset($cls_araiz_header);
  ?>