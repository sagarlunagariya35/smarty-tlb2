<?php
include 'classes/class.crs_group.php';
$allowed_roles = array(ROLE_HEAD_OFFICE);
require_once 'session.php';

$crs_group = new Mtx_Crs_Group();

$title = 'Groups List';
$description = '';
$keywords = '';
$active_page = "list_crs_groups";

$list_groups = $crs_group->get_all_groups();
$group_array = array(CRS_JAMAAT => 'Jamaat', CRS_JAMIAT => 'Jamiat', CRS_SCHOOL => 'School', CRS_ITS_ID => 'ITS ID', CRS_MARHALA => 'Marhala');

include_once("header.php");
?>
<script type="text/javascript" src="ckeditor/ckeditor.js"></script>

<div class="row">
  <div class="col-lg-12">&nbsp;</div>
</div>

<div class="panel panel-primary">
  <div class="panel-heading">
    <i class="fa fa-group fa-fw"></i> <?php echo $title; ?>
    <a href="crs_group_add.php" style="color: #FFF;" class="pull-right">Add New Group</a>
  </div>
  <!-- /.panel-heading -->
  <div class="panel-body">
    <div class="table-responsive">
      <table class="table table-striped table-hover table-condensed">
        <thead>
          <tr>
            <th>Sr</th>
            <th>Title</th>
            <th>Type</th>
            <th>Date</th>
            <th>Action</th>
          </tr>
        </thead>
        <tbody>
          <?php
          if($list_groups){
            $sr = 0;
            foreach ($list_groups as $data) {
              $sr++;
          ?>
              <tr>
                <td><?php echo $sr; ?></td>
                <td><?php echo $data['title']; ?></td>
                <td><?php echo $group_name = $group_array[$data['type']];; ?></td>
                <td><?php echo date('d, F Y',  strtotime($data['created_ts'])); ?></td>
                <td><a href="crs_group_update.php?id=<?php echo $data['id']; ?>"><i class="fa fa-pencil-square-o"></i></a></td>
              </tr>
              <?php
            }
          }else {
            echo '<tr><td class="text-center" colspan="6">No Records..</td></tr>';
          }
          ?>
        </tbody>
      </table>
    </div>
    <!-- /.table-responsive -->
  </div>
  <!-- /.panel-body -->
</div>

<!-- /#page-wrapper -->
<?php include "./footer.php"; ?>
