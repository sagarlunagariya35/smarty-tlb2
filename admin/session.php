<?php

session_start();
date_default_timezone_set('Asia/Kolkata');

require_once ('../classes/constants.php');
require_once ('classes/admin_gen_functions.php');
//echo $allowed_roles;
if(!isset($_SESSION[IS_ADMIN]) && $_SESSION[IS_ADMIN] != TRUE) {
  header('location:login.php');
  exit();
} 
else if(!in_array($_SESSION[USER_ROLE], $allowed_roles)) {
  $_SESSION['err'] = 'You are not Authorised to view this page.';
  header('Location: home.php');
  exit();
}

require_once 'classes/class.user.admin.php';

$logged_in_user = new mtx_user_admin();
$logged_in_user->get_mumeen_detail_from_its($_SESSION[USER_ITS]);
?>
