<?php
require_once '../classes/class.database.php';
include 'classes/class.article.php';
$allowed_roles = array(ROLE_HEAD_OFFICE);
require_once 'session.php';

$title = 'Miqaat Istibsaar';
$description = '';
$keywords = '';
$active_page = "add_miqaat_istibsaar";
$art = new Article();

(isset($_GET['id'])) ? $miqaat_istibsaar_id = $_GET['id'] : '';

if (isset($_REQUEST['video_id'])) {
  $video_id = $_REQUEST['video_id'];
  $video_file = $art->get_value_by_id('miqaat_video', $video_id);
  if ($video_file) {
    $video_file_name = SERVER_PATH . 'upload/miqaat_upload/video/' . $video_file[0]['video_url'];
    if (file_exists($video_file_name)) {
      unlink($video_file_name);
    }
    $v_rslt = $art->delete_media_by_id('miqaat_video', $video_id);
    if ($v_rslt) {
      $_SESSION[SUCCESS_MESSAGE] = 'Video File deleted Successfully';
    } else {
      $_SESSION[ERROR_MESSAGE] = 'Erors encountered while processing the request';
    }
  }
  header('Location: edit_miqaat_istibsaar.php?id=' . $miqaat_istibsaar_id);
  exit();
}

if (isset($_REQUEST['audio_id'])) {
  $audio_id = $_REQUEST['audio_id'];
  $audio_file = $art->get_value_by_id('miqaat_audio', $audio_id);
  if ($audio_file) {
    $audio_file_name = SERVER_PATH . 'upload/miqaat_upload/audio/' . $audio_file[0]['audio_url'];
    if (file_exists($audio_file_name)) {
      unlink($audio_file_name);
    }
    $a_rslt = $art->delete_media_by_id('miqaat_audio', $audio_id);
    if ($a_rslt) {
      $_SESSION[SUCCESS_MESSAGE] = 'Audio File deleted Successfully';
    } else {
      $_SESSION[ERROR_MESSAGE] = 'Erors encountered while processing the request';
    }
  }
  header('Location: edit_miqaat_istibsaar.php?id=' . $miqaat_istibsaar_id);
  exit();
}

if (isset($_REQUEST['stat_id'])) {
  $stat_id = $_REQUEST['stat_id'];
  $stat_file = $art->get_value_by_id('miqaat_stationary', $stat_id);
  if ($stat_file) {
    $stat_file_name = SERVER_PATH . 'upload/miqaat_upload/stationary/' . $stat_file[0]['stationary_url'];
    if (file_exists($stat_file_name)) {
      unlink($stat_file_name);
    }
    $i_rslt = $art->delete_media_by_id('miqaat_stationary', $stat_id);
    if ($i_rslt) {
      $_SESSION[SUCCESS_MESSAGE] = 'Stationary File deleted Successfully';
    } else {
      $_SESSION[ERROR_MESSAGE] = 'Erors encountered while processing the request';
    }
  }
  header('Location: edit_miqaat_istibsaar.php?id=' . $miqaat_istibsaar_id);
  exit();
}

if (isset($_REQUEST['delete_logo'])) {
  $logo = $_REQUEST['delete_logo'];
  
  $i_rslt = $art->delete_miqaat_istibsaar_logo($logo);
  if ($i_rslt) {
    $_SESSION[SUCCESS_MESSAGE] = 'Logo is deleted Successfully';
  } else {
    $_SESSION[ERROR_MESSAGE] = 'Erors encountered while processing the request';
  }
  
  header('Location: edit_miqaat_istibsaar.php?id=' . $miqaat_istibsaar_id);
  exit();
}

if(isset($_POST['btn_submit']))
{
  $data = $db->clean_data($_POST);
    $media = 0;
    foreach($_FILES as $files => $fileattributes)
    {
        if($_FILES[$files]['size'] != '0' || $media == 1)
        {
            $media = 1;
        }
        else
        {
            $media = 0;
        }
    }
    if($media == 0 && $_POST['miqaat_title'] == '')
    {
        echo ' does not need to insert';
    }
    else
    {
        $miqaat_title = $data['miqaat_title'];
        $slug = str_replace(' ', '', $data['slug']);
        if($slug == ''){ $slug = str_replace(' ', '', $miqaat_title); }
        $year = $data['year'];
        $miqaat_istibsaar_id = $data['miqaat_istibsaar_id'];
        
        $logo_file = $_FILES['logo']['name'];
        if($logo_file == ''){
          $logo_file = $data['last_logo'];
        }
       
        $logo_temp = $_FILES['logo']['tmp_name'];
        $youtube_video = $_POST['youtube_video'];
        $event_time = $data['event_time'];
        $show_pre = $data['show_pre'];
        $show_in = $data['show_in'];
        $show_post = $data['show_post'];
        
        $logopathANDname = "../upload/miqaat_upload/logo/" . $logo_file;
        move_uploaded_file($logo_temp, $logopathANDname);

        $miqaat_insert = $art->edit_miqaat_istibsaar($miqaat_title,$slug,$year,$logo_file,$show_pre,$show_in,$show_post,$youtube_video,$event_time,$miqaat_istibsaar_id);

        if ($miqaat_insert)
        {
          $max_id = $miqaat_istibsaar_id;
          foreach($_FILES as $files => $fileattributes)
          {
            if($_FILES[$files]['size'] != '0')
            {
              if($files != 'logo')
              {
                if(strlen($files) > 10){
                  $keyword = substr($files, 0, 11);
                }else {
                  $keyword = substr($files, 0, 5);
                }

                $number = substr($files,5,strlen($files));
                $originalfile = $_FILES[$files]['name'];
                $filearray = explode('.',$originalfile);
                $ext = $filearray[count($filearray)-1];
                $filename = $max_id.'-'.$originalfile;
                $filetemp = $_FILES[$files]['tmp_name'];

                if($keyword == 'video_thumb'){
                  $thumbpathANDname = "../upload/miqaat_upload/video/images/" . $filename;
                  $moveResult = move_uploaded_file($filetemp, $thumbpathANDname);

                  $update_image = $art->update_miqaat_media($max_id, $filename, $v_thumb_id, $event_time);

                }else {
                  if($keyword == 'stati'){
                    $keyword = 'stationary';
                  }
                  $pathANDname = "../upload/miqaat_upload/" . $keyword . "/" . $filename;
                  $moveResult = move_uploaded_file($filetemp, $pathANDname);

                  $insert_image = $art->insert_miqaat_media($keyword, $max_id, $filename, $event_time);
                }
                $v_thumb_id = $insert_image;
              }
            }
          }
          $_SESSION[SUCCESS_MESSAGE] = 'Miqaat Istibsaar Successfully Created.';
        }else {
          $_SESSION[ERROR_MESSAGE] = 'Error In Inserting Data';
        }
    }
}

if (isset($_GET['id'])) {
  $miqaat_istibsaar = $art->get_value_by_id('miqaat_istibsaar', $miqaat_istibsaar_id);
  $videos = $art->get_miqaat_media_by_id('miqaat_video', $miqaat_istibsaar_id);
  $youtube_video = $art->get_miqaat_media_by_id('miqaat_youtube', $miqaat_istibsaar_id);
  $stationaries = $art->get_miqaat_media_by_id('miqaat_stationary', $miqaat_istibsaar_id);
  $audios = $art->get_miqaat_media_by_id('miqaat_audio', $miqaat_istibsaar_id);
}
include_once("header.php");

?>
<script type="text/javascript" src="ckeditor/ckeditor.js"></script>

<style type="text/css">
.video_box,.audio_box,.image_box
{
    border: 1px solid #ccc;
    padding:10px 5px 50px 5px;
}
#content_box
{
    margin-top: 10px;
}
.video_link,.audio_link,.image_link
{
    padding: 5px 0px;
}
input[type="file"]
{
    width: 95px;
    display: inline-block;
}
#remove_box_link1,#remove_audio_box_link1,#remove_image_box_link1
{
    margin-left: 0px !important;
}
#btn_submit
{
    margin-top: 20px;
    padding: 5px;
    width: 80px;
    font-size: 16px;
    font-weight: bold;
    background-color: #5bc0de;
    border-radius: 5px;

}
</style>

   <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header"><?php echo $_SESSION[USER_ID]; ?> Edit Miqaat, Stationary, Videos, Audios <?php //echo $_SESSION['user_its']; ?></h1>
        </div>
    <!-- /.col-lg-12 -->
  </div>
   
    <div class="row">
        <div class="col-md-12">
            <form method="post" enctype="multipart/form-data">
              
              <div class="form-group col-md-6 col-xs-12">
                <label for="title">Miqaat Title :</label>
                <input type="text" name="miqaat_title" id="title" required="required" class="form-control" value="<?php echo $miqaat_istibsaar[0]['miqaat_title']; ?>" >
              </div>

              <div class="form-group col-md-6 col-xs-12">
                <label for="slug">Slug :</label>
                <input type="text" name="slug" id="slug" required="required" value="<?php echo $miqaat_istibsaar[0]['slug']; ?>" class="form-control" >
              </div>
              
              <div class="form-group col-md-4 col-xs-12">
                <label for="year">Select Year :</label>
                <select name="year" id="year" class="form-control" required >
                      <option value="">Select Year</option>
                      <option value="1435" <?php if($miqaat_istibsaar[0]['year'] == '1435'){ echo 'selected'; } ?>>1435</option>
                      <option value="1436" <?php if($miqaat_istibsaar[0]['year'] == '1436'){ echo 'selected'; } ?>>1436</option>
                      <option value="1437" <?php if($miqaat_istibsaar[0]['year'] == '1437'){ echo 'selected'; } ?>>1437</option>
                </select>
              </div>
              
              <div class="form-group col-md-4 col-xs-12">
                <label for="event">Select Event :</label>
                <select name="event_time_select" id="year" class="form-control" required onchange='$("#event_time").val($(this).val());' >
                      <option value="">Select Event</option>
                      <option value="pre">Pre</option>
                      <option value="in">In</option>
                      <option value="post">Post</option>
                </select>
              </div>
              
              <div class="clearfix"></div>
              
              <div class="form-group col-md-4 col-xs-12">
                <div class="checkbox">
                  <label>
                    <input type="checkbox" name="show_pre" id="show_pre" value="1" <?php if($miqaat_istibsaar[0]['show_pre_event'] == '1'){ echo 'checked'; } ?>>Show Pre event tab
                  </label>
                </div>
              </div>

              <div class="form-group col-md-4 col-xs-12">
                <div class="checkbox">
                  <label>
                    <input type="checkbox" name="show_in" id="show_in" value="1" <?php if($miqaat_istibsaar[0]['show_in_event'] == '1'){ echo 'checked'; } ?>>Show In event tab
                  </label>
                </div>
              </div>

              <div class="form-group col-md-4 col-xs-12">
                <div class="checkbox">
                  <label>
                    <input type="checkbox" name="show_post" id="show_post" value="1" <?php if($miqaat_istibsaar[0]['show_post_event'] == '1'){ echo 'checked'; } ?>>Show Post event tab
                  </label>
                </div>
              </div>
              
              <div class="clearfix"></div>
              <div class="master-menu" id="master-menu" style="float: left; margin-right: 10px;margin-left: 15px;">
                  <span style="font-weight:bold;">Miqaat Logo : </span>
                  <input type="text" class="miqaat_logo1" id="miqaat_logo1" value="" readonly="readonly">
                  <input type="file" name="logo" class="file" onchange='$("#miqaat_logo1").val($(this).val());'>
                  <?php if($miqaat_istibsaar[0]['logo'] != ''){ ?>
                  <img src="<?php echo SERVER_PATH . 'upload/miqaat_upload/logo/' . $miqaat_istibsaar[0]['logo']; ?>" height="100" width="100">
                  <a href="edit_miqaat_istibsaar.php?id=<?php echo $miqaat_istibsaar_id; ?>&delete_logo=<?php echo $miqaat_istibsaar_id; ?>"><img src="images/remove.png" alt="" style="height: 25px;vertical-align: auto;margin-right: 10px;"></a>
                  <?php } ?>
                  <input type="hidden" name="last_logo" value="<?php echo $miqaat_istibsaar[0]['logo'];  ?>">
              </div>
              
              <div class="clearfix"></div>
              <div class="content_box" id="content_box">
                <div class="content_link" id="content_link">
                  <label>Youtube Video Embed: </label>
                  <textarea name="youtube_video" class="form-control" placeholder="Enter Youtube Video Embed Code"><?php echo $youtube_video[0]['video_url']; ?></textarea>
                </div>
              </div>
              <div class="clearfix"></div>
                
              <div class="video_box" id="video_box" style="margin-top:20px;">
                  <div class="video_link" id="video_link">
                      <label>Upload Video : </label>
                      <input type="text" class="videofilename1" id="videofilename1" readonly="readonly">
                      <input type="file" name="video1" id="video1" class="file" onchange="changevideo(this);">
                      <label>Upload Thumbnail : </label>
                      <input type="text" class="video_thumbnail1" id="video_thumbnail1" readonly="readonly">
                      <input type="file" name="video_thumb1" id="video_thumb1" class="file" onchange="changevideo_thumbnail(this);">
                  </div>
                  <div class="add_box" id="add_box"><a href="javascript:void(0);" id="add_box_link"><img src="images/add.png" alt="" style="height: 25px;"></a></div>
                  <input type="hidden" name="videovalue" id="videovalue" value="1"> 
              </div><!----VIDEO_BOX-->

              <div class="audio_box" id="audio_box">
                  <div class="audio_link" id="audio_link">
                      <label>Upload Audio : </label>
                      <input type="text" class="audiofilename1" id="audiofilename1" readonly="readonly">
                      <input type="file" name="audio1" id="audio1" class="file" onchange="audiochange(this);">
                  </div>
                  <div class="add_audio_box" id="add_audio_box"><a href="javascript:void(0);" id="add_audio_box_link"><img src="images/add.png" alt="" style="height: 25px;"></a></div>
                  <input type="hidden" name="audiovalue" id="audiovalue" value="1"> 
              </div><!----AUDIO_BOX-->

              <div class="image_box" id="image_box">
                  <div class="image_link" id="image_link">
                      <label>Upload Stationary : </label>
                      <input type="text" class="stationaryname1" id="stationaryname1" readonly="readonly">
                      <input type="file" name="stati1" id="stati1" class="file" onchange="statchange(this);">
                  </div>
                  <div class="add_stat_box" id="add_stat_box"><a href="javascript:void(0);" id="add_image_box_link"><img src="images/add.png" alt="" style="height: 25px;"></a></div>
                  <input type="hidden" name="statvalue" id="statvalue" value="1"> 
              </div><!----IMAGE_BOX-->

              <input type="hidden" name="miqaat_istibsaar_id" value="<?php echo $miqaat_istibsaar_id; ?>">
              <input type="hidden" value="" name="event_time" id="event_time">
              <input type="submit" name="btn_submit" id="btn_submit" value="Submit" style="">

              <div class="clearfix"><br></div>

              <div class="col-md-12 col-xs-12">

                <ul class="nav nav-tabs nav-justified" role="tablist">
                  <li role="presentation" class="active"><a href="#pre" aria-controls="pre" role="tab" data-toggle="tab">Pre Event</a></li>
                  <li role="presentation"><a href="#in" aria-controls="in" role="tab" data-toggle="tab">In Event</a></li>
                  <li role="presentation"><a href="#post" aria-controls="post" role="tab" data-toggle="tab">Post Event</a></li>
                </ul>

                <?php
                  $video_pre = $video_in = $video_post = FALSE;
                  $audio_pre = $audio_in = $audio_post = FALSE;
                  $stat_pre = $stat_in = $stat_post = FALSE;

                  if ($videos) {
                    foreach ($videos as $video) {
                      if($video['event_time'] == 'pre'){
                        $video_pre .= '<div class="col-md-4 col-xs-12">
                        <video width="200" height="100" controls style="vertical-align: middle;">
                          <source src="'.SERVER_PATH.'upload/miqaat_upload/video/'.$video['video_url'].'">
                        </video>

                        <a href="edit_miqaat_istibsaar.php?id='.$miqaat_istibsaar_id.'&video_id='.$video['id'].'"><img src="images/remove.png" style="height: 25px;vertical-align: auto;margin-right: 10px;"></a>
                      </div>';
                      }else if($video['event_time'] == 'in'){
                        $video_in .= '<div class="col-md-4 col-xs-12">
                        <video width="200" height="100" controls style="vertical-align: middle;">
                          <source src="'.SERVER_PATH.'upload/miqaat_upload/video/'.$video['video_url'].'">
                        </video>

                        <a href="edit_miqaat_istibsaar.php?id='.$miqaat_istibsaar_id.'&video_id='.$video['id'].'"><img src="images/remove.png" style="height: 25px;vertical-align: auto;margin-right: 10px;"></a>
                      </div>';
                      }else if($video['event_time'] == 'post') {
                        $video_post .= '<div class="col-md-4 col-xs-12">
                        <video width="200" height="100" controls style="vertical-align: middle;">
                          <source src="'.SERVER_PATH.'upload/miqaat_upload/video/'.$video['video_url'].'">
                        </video>

                        <a href="edit_miqaat_istibsaar.php?id='.$miqaat_istibsaar_id.'&video_id='.$video['id'].'"><img src="images/remove.png" style="height: 25px;vertical-align: auto;margin-right: 10px;"></a>
                      </div>';
                      }
                    }
                  }

                  if ($audios) {
                  foreach ($audios as $audio) {
                    if($audio['event_time'] == 'pre'){
                        $audio_pre .= '<div class="col-md-4 col-xs-12">
                        <audio controls style="width:250px;vertical-align: middle;">
                          <source src="'.SERVER_PATH.'upload/miqaat_upload/audio/'.$audio['audio_url'].'">
                        </audio>
                        <a href="edit_miqaat_istibsaar.php?id='.$miqaat_istibsaar_id.'&audio_id='.$audio['id'].'"><img src="images/remove.png" style="height: 25px;vertical-align: auto;margin-right: 10px;"></a>
                      </div>';
                    }else if($audio['event_time'] == 'in'){
                        $audio_in .= '<div class="col-md-4 col-xs-12">
                        <audio controls style="width:250px;vertical-align: middle;">
                          <source src="'.SERVER_PATH.'upload/miqaat_upload/audio/'.$audio['audio_url'].'">
                        </audio>
                        <a href="edit_miqaat_istibsaar.php?id='.$miqaat_istibsaar_id.'&audio_id='.$audio['id'].'"><img src="images/remove.png" style="height: 25px;vertical-align: auto;margin-right: 10px;"></a>
                      </div>';
                    }else if($audio['event_time'] == 'post'){
                        $audio_post .= '<div class="col-md-4 col-xs-12">
                        <audio controls style="width:250px;vertical-align: middle;">
                          <source src="'.SERVER_PATH.'upload/miqaat_upload/audio/'.$audio['audio_url'].'">
                        </audio>
                        <a href="edit_miqaat_istibsaar.php?id='.$miqaat_istibsaar_id.'&audio_id='.$audio['id'].'"><img src="images/remove.png" style="height: 25px;vertical-align: auto;margin-right: 10px;"></a>
                      </div>';
                    }
                  }
                }

                if ($stationaries) {
                  foreach ($stationaries as $stat_files) {
                    if($stat_files['event_time'] == 'pre'){
                        $stat_pre .= '<div class="col-md-4 col-xs-12">
                        <i class="fa fa-file-pdf-o"></i> '.$stat_files['stationary_url'].'
                        <a href="edit_miqaat_istibsaar.php?id='.$miqaat_istibsaar_id.'&stat_id='.$stat_files['id'].'"><img src="images/remove.png" style="height: 25px;vertical-align: auto;margin-right: 10px;"></a>
                      </div>';
                    }else if($stat_files['event_time'] == 'in'){
                        $stat_in .= '<div class="col-md-4 col-xs-12">
                        <i class="fa fa-file-pdf-o"></i> '.$stat_files['stationary_url'].'
                        <a href="edit_miqaat_istibsaar.php?id='.$miqaat_istibsaar_id.'&stat_id='.$stat_files['id'].'"><img src="images/remove.png" style="height: 25px;vertical-align: auto;margin-right: 10px;"></a>
                      </div>';
                    }else if($stat_files['event_time'] == 'post'){
                        $stat_post .= '<div class="col-md-4 col-xs-12">
                        <i class="fa fa-file-pdf-o"></i> '.$stat_files['stationary_url'].'
                        <a href="edit_miqaat_istibsaar.php?id='.$miqaat_istibsaar_id.'&stat_id='.$stat_files['id'].'"><img src="images/remove.png" style="height: 25px;vertical-align: auto;margin-right: 10px;"></a>
                      </div>';
                    }
                  }
                }

                $no_videos = '<p style="margin-left:10px;">No Videos</p>';
                $no_audios = '<p style="margin-left:10px;">No Audios</p>';
                $no_stationaries = '<p style="margin-left:10px;">No Stationaries</p>';
                ?>

                <div class="tab-content">
                  <div role="tabpanel" class="tab-pane active" id="pre">
                      <div class="static-show-button lsd large-fonts col-xs-12 col-sm-3" dir="rtl">Videos</div><div class="clearfix"></div>
                      <?php if($video_pre) { ?>
                      <a target="_blank" href="<?php echo SERVER_PATH.'admin/change_miqaat_content_order.php?id='.$miqaat_istibsaar_id.'&keyword=video'; ?>" class="pull-right">Change Video Order</a>
                      <div class="clearfix"></div>
                      <?php } ?>

                      <?php if($video_pre) echo $video_pre; else echo $no_videos; ?>

                      <div class="clearfix"></div>
                      <div class="static-show-button lsd large-fonts col-xs-12 col-sm-3" dir="rtl">Audios</div><div class="clearfix"></div>
                      <?php if($audio_pre) { ?>
                      <a target="_blank" href="<?php echo SERVER_PATH.'admin/change_miqaat_content_order.php?id='.$miqaat_istibsaar_id.'&keyword=audio'; ?>" class="pull-right">Change Audio Order</a>
                      <div class="clearfix"></div>
                      <?php } ?>

                      <?php if($audio_pre) echo $audio_pre; else echo $no_audios; ?>

                      <div class="clearfix"></div>
                      <div class="static-show-button lsd large-fonts col-xs-12 col-sm-3" dir="rtl">Stationary</div><div class="clearfix"></div>
                      <?php if($stat_pre) { ?>
                      <a target="_blank" href="<?php echo SERVER_PATH.'admin/change_miqaat_content_order.php?id='.$miqaat_istibsaar_id.'&keyword=stationary'; ?>" class="pull-right">Change Stationary Order</a>
                      <div class="clearfix"></div>
                      <?php } ?>

                      <?php if($stat_pre) echo $stat_pre; else echo $no_stationaries; ?>
                  </div>

                  <div role="tabpanel" class="tab-pane" id="in">
                      <div class="static-show-button lsd large-fonts col-xs-12 col-sm-3" dir="rtl">Videos</div><div class="clearfix"></div>
                      <?php if($video_in) { ?>
                      <a target="_blank" href="<?php echo SERVER_PATH.'admin/change_miqaat_content_order.php?id='.$miqaat_istibsaar_id.'&keyword=video'; ?>" class="pull-right">Change Video Order</a>
                      <div class="clearfix"></div>
                      <?php } ?>

                      <?php if($video_in) echo $video_in; else echo $no_videos; ?>

                      <div class="clearfix"></div>
                      <div class="static-show-button lsd large-fonts col-xs-12 col-sm-3" dir="rtl">Audios</div><div class="clearfix"></div>
                      <?php if($audio_in) { ?>
                      <a target="_blank" href="<?php echo SERVER_PATH.'admin/change_miqaat_content_order.php?id='.$miqaat_istibsaar_id.'&keyword=audio'; ?>" class="pull-right">Change Audio Order</a>
                      <div class="clearfix"></div>
                      <?php } ?>

                      <?php if($audio_in) echo $audio_in; else echo $no_audios; ?>

                      <div class="clearfix"></div>
                      <div class="static-show-button lsd large-fonts col-xs-12 col-sm-3" dir="rtl">Stationary</div><div class="clearfix"></div>
                      <?php if($stat_in) { ?>
                      <a target="_blank" href="<?php echo SERVER_PATH.'admin/change_miqaat_content_order.php?id='.$miqaat_istibsaar_id.'&keyword=stationary'; ?>" class="pull-right">Change Stationary Order</a>
                      <div class="clearfix"></div>
                      <?php } ?>

                      <?php if($stat_in) echo $stat_in; else echo $no_stationaries; ?>
                  </div>

                  <div role="tabpanel" class="tab-pane" id="post">
                      <div class="static-show-button lsd large-fonts col-xs-12 col-sm-3" dir="rtl">Videos</div><div class="clearfix"></div>
                      <?php if($video_post) { ?>
                      <a target="_blank" href="<?php echo SERVER_PATH.'admin/change_miqaat_content_order.php?id='.$miqaat_istibsaar_id.'&keyword=video'; ?>" class="pull-right">Change Video Order</a>
                      <div class="clearfix"></div>
                      <?php } ?>

                      <?php if($video_post) echo $video_post; else echo $no_videos; ?>

                      <div class="clearfix"></div>
                      <div class="static-show-button lsd large-fonts col-xs-12 col-sm-3" dir="rtl">Audios</div><div class="clearfix"></div>
                      <?php if($audio_post) { ?>
                      <a target="_blank" href="<?php echo SERVER_PATH.'admin/change_miqaat_content_order.php?id='.$miqaat_istibsaar_id.'&keyword=audio'; ?>" class="pull-right">Change Audio Order</a>
                      <div class="clearfix"></div>
                      <?php } ?>

                      <?php if($audio_post) echo $audio_post; else echo $no_audios; ?>

                      <div class="clearfix"></div>
                      <div class="static-show-button lsd large-fonts col-xs-12 col-sm-3" dir="rtl">Stationary</div><div class="clearfix"></div>
                      <?php if($stat_post) { ?>
                      <a target="_blank" href="<?php echo SERVER_PATH.'admin/change_miqaat_content_order.php?id='.$miqaat_istibsaar_id.'&keyword=stationary'; ?>" class="pull-right">Change Stationary Order</a>
                      <div class="clearfix"></div>
                      <?php } ?>

                      <?php if($stat_post) echo $stat_post; else echo $no_stationaries; ?>
                  </div>
                </div>
              </div>
          </form>
        </div>
    </div>

<style type="text/css">
  .static-show-button{
  display:block;
  max-width:500px;
  -webkit-transition: 0.4s;
  -moz-transition: 0.4s;
  -o-transition: 0.4s;
  transition: 0.4s;
  border: none;
  background-color: #549BC7;
  height: auto;
  line-height:20px;
  margin:20px auto;
  padding:10px;
  outline: none;
  color:#FFF;
  text-transform:uppercase;
  text-align:center;
  font-weight:bold;
  font-size:20px;
  float:right;
  text-decoration: underline none;
}
</style>

<script type="text/javascript">
     
jQuery(document).ready(function() {
 
    jQuery('.switch_options').each(function() {
 
        //This object
        var obj = jQuery(this);
 
        var enb = obj.children('.switch_enable'); //cache first element, this is equal to ON
        var dsb = obj.children('.switch_disable'); //cache first element, this is equal to OFF
        var input = obj.children('input'); //cache the element where we must set the value
        var input_val = obj.children('input').val(); //cache the element where we must set the value
 
        /* Check selected */
        if( 0 == input_val ){
            dsb.addClass('selected');
        }
        else if( 1 == input_val ){
            enb.addClass('selected');
        }
 
        //Action on user's click(ON)
        enb.on('click', function(){
            $(dsb).removeClass('selected'); //remove "selected" from other elements in this object class(OFF)
            $(this).addClass('selected'); //add "selected" to the element which was just clicked in this object class(ON) 
            $(input).val(1).change(); //Finally change the value to 1
            //alert('1');
        });
 
        //Action on user's click(OFF)
        dsb.on('click', function(){
            $(enb).removeClass('selected'); //remove "selected" from other elements in this object class(ON)
            $(this).addClass('selected'); //add "selected" to the element which was just clicked in this object class(OFF) 
            $(input).val(0).change(); // //Finally change the value to 0
            //alert('0');
        });
 
    });
 
});
</script>
<script type="text/javascript">
    /////////////////////////////////////////FOR VIDEO START/////////////////////////////////////////////////////     
$(document).ready(function() {
    var videocount = $('.video_link').length;
    var audiocount = $('.audio_link').length;
    var imagecount = $('.image_link').length;
    if(videocount == 1)
    {
        $('.video_link').css('float','left');
        $('#add_box').css('float','left');
    }
    if(audiocount == 1)
    {
        $('.audio_link').css('float','left');
        $('#add_audio_box').css('float','left');
    }
    if(imagecount == 1)
    {
        $('.image_link').css('float','left');
        $('#add_image_box').css('float','left');
    }

});
      
$(document).ready(function() {
   
    $("#add_box").click(function(){
        /*REMOVE ALL THINGS*/
       
        var videovalue = parseInt($('#videovalue').val()) + 1;
        $('#videovalue').val(videovalue);

         $('#video_box div.video_link').removeClass('lastchild');

        //ADD NEW BOX AND SET CSS
        $('#add_box').before('<div class="video_link" id="video_link"><label> Upload Video : </label><input type="text" class="videofilename'+videovalue+'" id="videofilename'+videovalue+'" readonly="readonly" style="margin-left:4px;"><input type="file" name="video'+videovalue+'" id="video'+videovalue+'" class="file" onchange="changevideo(this);" style="margin-left:4px;"><label> Upload Thumbnai : </label><input type="text" class="video_thumbnail'+videovalue+'" id="video_thumbnail'+videovalue+'" readonly="readonly" style="margin-left:4px;"><input type="file" name="video_thumb'+videovalue+'" id="video_thumb'+videovalue+'" class="file" onchange="changevideo_thumbnail(this);" style="margin-left:4px;"></div>');
        var count = $('.video_link').length;
        if(count > 1)
        {
            $('.video_link').css('float','');
            $('#video_box div.video_link:last').css('float','left'); 
            $('#video_box div.video_link:last').addClass('lastchild');
           
           //APPEND ONLY IF NOT THERE
           $( ".video_link" ).each(function( index ) {
                if($(this).find("a").length)
                {
                }
                else
                {
                    var rvideovalue = videovalue - 1;
                    $(this).append('<a href="javascript:void(0);" id="remove_box_link'+rvideovalue+'" onclick="return removevideo(this);" style="margin-left:4px;"><img src="images/remove.png" alt="" style="height: 15px;"></a>');
                }    
           });
           //REMOVE REMOVE BTN FROM LAST CHILD
            if($('#video_box div.video_link').hasClass('lastchild'))
            {
                $("#video_box div.video_link:last").children("a:first").remove();
            }
        }
    });//END ADD CLICK
});

function changevideo(elem)
{
    var id = $(elem).attr("id");
    var file = $('#'+id).val();
    var filearray = file.split('.');
    var arraysize = filearray.length;
    var filetype = filearray[arraysize - 1].toLowerCase();
    if(filetype == '3gp'||filetype =='wmv'||filetype =='mp4'||filetype =='avi'||filetype =='mpeg')
    {
        var lastChar = id.substr(id.length - 1);
        //alert(lastChar);
        $('#videofilename'+lastChar).val(file);
    }
    else
    {
        //alert(id);
        $('#'+id).val('');
        alert('Invalid File Type, Please Select Video Only');
    }

}

function changevideo_thumbnail(elem)
{
    var id = $(elem).attr("id");
    //alert(id.val);
    var file = $('#'+id).val();
    
    
    var filearray = file.split('.');
    var arraysize = filearray.length;
    var filetype = filearray[arraysize - 1].toLowerCase();
    if(filetype == 'jpg'||filetype =='png'||filetype =='jpeg'||filetype =='gif'||filetype =='tif'||filetype =='raw'||filetype =='bmp')
    {
        var lastChar = id.substr(id.length - 1);
        //alert(lastChar);
        $('#video_thumbnail'+lastChar).val(file);
    }
    else
    {
        $('#'+id).val('');
        alert('Invalid File Type, Please Select Image File Only');
    }

}

function removevideo(elem)
{
    if($( ".video_link" ).length == 2)
    {
        //alert($( ".video_link" ).length);
        //SET HVALUE AS 1
    }
    var id = $(elem).attr("id");
    $('#'+id).parent().remove();    
}
/////////////////////////////////////////FOR VIDEO END/////////////////////////////////////////////////////     
$(document).ready(function(){
   $('#add_audio_box').click(function(){
      var audiovalue = parseInt($('#audiovalue').val()) + 1;
      $('#audiovalue').val(audiovalue);
      $('#audio_box div.audio_link').removeClass('lastchild');
      //APPEND THE CODE
      $('#add_audio_box').before('<div class="audio_link" id="audio_link"><label>Upload Audio : </label><input type="text" style="margin-left:4px;" class="audiofilename'+audiovalue+'" id="audiofilename'+audiovalue+'" readonly="readonly"><input type="file" name="audio'+audiovalue+'" id="audio'+audiovalue+'" class="file" onchange="audiochange(this);" style="margin-left:4px;"></div>');
 
        var count = $('.audio_link').length;
        if(count > 1)
        {
            $('.audio_link').css('float','');
            $('#audio_box div.audio_link:last').css('float','left'); 
            $('#audio_box div.audio_link:last').addClass('lastchild');
           
           //APPEND ONLY IF NOT THERE
           $( ".audio_link" ).each(function( index ) {
                if($(this).find("a").length)
                {
                }
                else
                {
                    var raudiovalue = audiovalue - 1;
                    $(this).append('<a href="javascript:void(0);" id="remove_audio_box_link'+raudiovalue+'" onclick="return removeaudio(this);" style="margin-left:4px;"><img src="images/remove.png" alt="" style="height: 15px;"></a>');
                }    
           });
           //REMOVE REMOVE ADD BTN FROM LAST CHILD
            if($('#audio_box div.audio_link').hasClass('lastchild'))
            {
                $("#audio_box div.audio_link:last").children("a:first").remove();
            }
        } 
   }); 
});
function audiochange(elem)
{
    var id = $(elem).attr("id");
    //alert(id.val);
    var file = $('#'+id).val();
    
    var filearray = file.split('.');
    var arraysize = filearray.length;
    var filetype = filearray[arraysize - 1].toLowerCase();
    if(filetype == 'mp3'||filetype =='3ga'||filetype =='wav')
    {
        var lastChar = id.substr(id.length - 1);
        //alert(lastChar);
        $('#audiofilename'+lastChar).val(file);
    
    }
    else
    {
        //alert(id);
        $('#'+id).val('');
        alert('Invalid File Type, Please Select Audio File Only');
    }

}
function removeaudio(elem)
{
    if($( ".audio_link" ).length == 2)
    {
        //alert($( ".audio_link" ).length);
        //SET HVALUE AS 1
    }
    var id = $(elem).attr("id");
    $('#'+id).parent().remove();    
}
////////////////////////////////////////////////////////////////////////AUDIO END/////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////IMAGE START////////////////////////////////////////////////////////////
$(document).ready(function(){
   $('#add_stat_box').click(function(){
      var statvalue = parseInt($('#statvalue').val()) + 1;
      $('#statvalue').val(statvalue);
      $('#image_box div.image_link').removeClass('lastchild');
      //APPEND THE CODE
      $('#add_stat_box').before('<div class="image_link" id="image_link"><label>Upload Stationary : </label><input type="text" class="stationaryname'+statvalue+'" id="stationaryname'+statvalue+'" readonly="readonly" style="margin-left:4px;"><input type="file" name="stati'+statvalue+'" id="stati'+statvalue+'" class="file" onchange="statchange(this);" style="margin-left:4px;"></div>');
 
        var count = $('.image_link').length;
        if(count > 1)
        {
            $('.image_link').css('float','');
            $('#image_box div.image_link:last').css('float','left'); 
            $('#image_box div.image_link:last').addClass('lastchild');
           
           //APPEND ONLY IF NOT THERE
           $( ".image_link" ).each(function( index ) {
                if($(this).find("a").length)
                {
                }
                else
                {
                    var rimagevalue = statvalue - 1;
                    $(this).append('<a href="javascript:void(0);" id="remove_image_box_link'+rimagevalue+'" onclick="return removestat(this);" style="margin-left:4px;"><img src="images/remove.png" alt="" style="height: 15px;"></a>');
                }    
           });
           //REMOVE REMOVE ADD BTN FROM LAST CHILD
            if($('#image_box div.image_link').hasClass('lastchild'))
            {
                $("#image_box div.image_link:last").children("a:first").remove();
            }
        } 
   }); 
});

function statchange(elem)
{
    var id = $(elem).attr("id");
    //alert(id.val);
    var file = $('#'+id).val();
    
    
    var filearray = file.split('.');
    var arraysize = filearray.length;
    var filetype = filearray[arraysize - 1].toLowerCase();
    if(filetype == 'txt'||filetype =='pdf'||filetype =='doc'||filetype =='docx')
    {
        var lastChar = id.substr(id.length - 1);
        //alert(lastChar);
        $('#stationaryname'+lastChar).val(file);
    }
    else
    {
        $('#'+id).val('');
        alert('Invalid File Type, Please Select Document Only');
    }
}

function removestat(elem)
{
    if($( ".image_link" ).length == 2)
    {
        //alert($( ".image_link" ).length);
        //SET HVALUE AS 1
    }
    var id = $(elem).attr("id");
    $('#'+id).parent().remove();    
}
////////IMAGE END///////////////
///////Tinymce Start///////////

///////Tinymce END///////////

//DATE VALIDATION
function datevalidation()
{
  
    var startDate = new Date($('#start_date').val());
    var endDate = new Date($('#end_date').val());

    if (startDate < endDate)
    {
    // Do something
    }
    else
    {
         alert('End Date Must be Greater');
         $('#end_date').val('');
    }
}

function show_sub_menu(str)
{
  if (str == "")
  {
    document.getElementById("sub_menu").innerHTML = "";
    document.getElementById("mname").innerHTML = "";
    return;
  }

  if (window.XMLHttpRequest)
  {// code for IE7+, Firefox, Chrome, Opera, Safari
    xmlhttp = new XMLHttpRequest();
  }
  else
  {// code for IE6, IE5
    xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
  }



  xmlhttp.onreadystatechange = function()
  {
    if (xmlhttp.readyState == 4 && xmlhttp.status == 200)
    {
      document.getElementById("sub_menu").innerHTML = xmlhttp.responseText;
    }
  }
  xmlhttp.open("GET", "get_data_by_ajax.php?menu=" + str, true);
  xmlhttp.send();
}
</script>
<!-- /#page-wrapper -->
<?php include "./footer.php"; ?>
