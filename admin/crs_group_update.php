<?php
include 'classes/class.crs_group.php';
include 'classes/class.araiz.php';
$allowed_roles = array(ROLE_HEAD_OFFICE);
require_once 'session.php';

$crs_group = new Mtx_Crs_Group();
$araiz = new Araiz();
$user_admin = new mtx_user_admin();

$title = 'Update Group';
$description = '';
$keywords = '';
$active_page = "list_crs_groups";

$group_data = FALSE;

if (isset($_REQUEST['id'])) {
  $id = $_REQUEST['id'];
}

if (isset($_POST['btn_submit'])) {
  $data = $db->clean_data($_POST);
  
  $title = $data['title'];
  $group_type = $data['group_type'];
  $user_id = $_SESSION[USER_ID];
  
  if ($group_type == CRS_JAMAAT) {
    if ($data['jamaat'] != '') {
      $group_data = array($data['jamaat']);
    } else {
      $group_data = array($data['last_jamaat']);
    }
    
  } else if ($group_type == CRS_JAMIAT) {
    if ($data['jamiat'] != '') {
      $group_data = array($data['jamiat']);
    } else {
      $group_data = array($data['last_jamiat']);
    }
    
  } else if ($group_type == CRS_SCHOOL) {
    if ($data['school'] != '') {
      $group_data = array($data['school']);
    } else {
      $group_data = array($data['last_school']);
    }
    
  } else if ($group_type == CRS_MARHALA) {
    if ($data['marhala'] != '') {
      $group_data = array($data['marhala']);
    } else {
      $group_data = array($data['last_marhala']);
    }
    
  } else if ($group_type == CRS_ITS_ID) {
    $group_data = array($data['final_its_id']);
  }
  
  $update = $crs_group->update_group($title, $group_type, $group_data, $id);

  if ($update) {
    $_SESSION[SUCCESS_MESSAGE] = 'Group Updated Successfully.';
  } else {
    $_SESSION[ERROR_MESSAGE] = 'Error encountered while Updating Group';
  }
}

$result = $crs_group->get_group($id);
$jamiats = $araiz->get_all_lp_jamiat();
$jamaats = $araiz->get_all_lp_jamaat();
$schools = $araiz->get_all_lp_schools();

$marhala_array = array('1'=>'Pre-Primary', '2'=>'Primary', '3'=>'Std 5 to 7', '4'=>'Std 8 to 10', '5'=>'Std 11 to 12', '6'=>'Graduation', '7'=>'Post Graduation', '8'=>'Diploma');

$group_array = array(CRS_JAMAAT => 'Jamaat', CRS_JAMIAT => 'Jamiat', CRS_SCHOOL => 'School', CRS_ITS_ID => 'ITS ID', CRS_MARHALA => 'Marhala');

include_once("header.php");
?>
<script type="text/javascript" src="ckeditor/ckeditor.js"></script>

<style>
  .alfatemi-text{
    font-size: 18px;
    display: table;
  }
</style>

<div class="row">
  <div class="col-lg-12">
    <h3 class="page-header"><?php echo $title; ?></h3>
  </div>
  <!-- /.col-lg-12 -->
</div>

<div class="row">
  <div class="col-md-12">
    <form method="post" class="forms1" action="" enctype="multipart/form-data">

      <div class="form-group">
        <label class="control-label col-md-2 text-right">Title :</label>
        <div class="col-md-5">
          <input type="text" name="title" id="title" class="form-control" value="<?php echo $result['title']; ?>">
        </div>
        <div class="clearfix"></div>
      </div>
      
      <div class="form-group">
        <label class="control-label col-md-2 text-right">Group Type :</label>
        <div class="col-md-5">
          <select name="group_type" class="form-control" id="group_type">
            <option value="">Select Group Type</option>
            <?php
              foreach ($group_array as $key => $value) {
                $selected = ($key == $result['type']) ? 'selected' : '';
            ?>
                <option value="<?php echo $key; ?>" <?php echo $selected; ?>><?php echo $value; ?></option>
            <?php
              }
            ?>
          </select>
        </div>
        <div class="clearfix"></div>
      </div>
      
      <div class="form-group jamaat">
        <label class="control-label col-md-2 text-right">Jamat :</label>
        <div class="col-md-5">
          <select name="jamaat[]" multiple class="form-control" id="jamaat">
            <option value="">Select Jamaat</option>
            <?php
              if ($jamaats) {
                foreach ($jamaats as $data) {
                  ?>
                  <option value="<?php echo $data['jamaat']; ?>"><?php echo $data['jamaat']; ?></option>
                  <?php
                }
              }
            ?>
          </select>
        </div>
        <div class="clearfix"></div>
        <?php
          if ($result['type'] == CRS_JAMAAT) {
            $jamaat_result = $crs_group->get_group_data('crs_group_jamaat', $id);
            if ($jamaat_result) {
              foreach ($jamaat_result as $group_jamaat){
                echo '<input type="hidden" name="last_jamaat[]" value="'.$group_jamaat.'">';
              }
              $jamaat_result_data = implode(', ', $jamaat_result);
              echo '<div class="col-md-2"></div>';
              echo '<div class="col-md-5">';
              echo '<p>'.$jamaat_result_data.'</p>';
              echo '</div>';
              echo '<div class="clearfix"></div>';
            }
          }
        ?>
      </div>
      
      <div class="form-group jamiat">
        <label class="control-label col-md-2 text-right">Jamiat :</label>
        <div class="col-md-5">
          <select name="jamiat[]" multiple class="form-control" id="jamiat">
            <option value="">Select Jamiat</option>
            <?php
              if ($jamiats) {
                foreach ($jamiats as $data) {
                  ?>
                  <option value="<?php echo $data['jamiat']; ?>"><?php echo $data['jamiat']; ?></option>
                  <?php
                }
              }
            ?>
          </select>
        </div>
        <div class="clearfix"></div>
        <?php
          if ($result['type'] == CRS_JAMIAT) {
            $jamiat_result = $crs_group->get_group_data('crs_group_jamiat', $id);
            if ($jamiat_result) {
              foreach ($jamiat_result as $group_jamiat){
                echo '<input type="hidden" name="last_jamiat[]" value="'.$group_jamiat.'">';
              }
              $jamiat_result_data = implode(', ', $jamiat_result);
              echo '<div class="col-md-2"></div>';
              echo '<div class="col-md-5">';
              echo '<p>'.$jamiat_result_data.'</p>';
              echo '</div>';
              echo '<div class="clearfix"></div>';
            }
          }
        ?>
      </div>
      
      <div class="form-group school">
        <label class="control-label col-md-2 text-right">School :</label>
        <div class="col-md-5">
          <select name="school[]" multiple class="form-control" id="school">
            <option value="">Select School</option>
            <?php
              if ($schools) {
                foreach ($schools as $data) {
                  ?>
                  <option value="<?php echo $data['school_name']; ?>"><?php echo $data['school_name']; ?></option>
                  <?php
                }
              }
            ?>
          </select>
        </div>
        <div class="clearfix"></div>
        <?php
          if ($result['type'] == CRS_SCHOOL) {
            $school_result = $crs_group->get_group_data('crs_group_school', $id);
            if ($school_result) {
              foreach ($school_result as $group_school){
                echo '<input type="hidden" name="last_school[]" value="'.$group_school.'">';
              }
              $school_result_data = implode(', ', $school_result);
              echo '<div class="col-md-2"></div>';
              echo '<div class="col-md-5">';
              echo '<p>'.$school_result_data.'</p>';
              echo '</div>';
              echo '<div class="clearfix"></div>';
            }
          }
        ?>
      </div>
      
      <div class="form-group its_id">
        <label class="control-label col-md-2 text-right">ITS ID :</label>
        <div class="col-md-5">
          <input type="text" class="form-control" name="its_id" id="its_id">
        </div>
        <a href="#" id="get_details" class="btn btn-success">Get Details</a>
        <div class="clearfix"></div>
      </div>
      
      <div class="form-group show_ajx_result">
        <label class="control-label col-md-2 text-right"></label>
        <div class="col-md-5">
          <p class="form-control-static its_name"></p>
          <input type="hidden" class="form-control" value="" name="fatch_its_id" id="fatch_its_id">
          <input type="hidden" class="form-control" value="" name="fatch_its_name" id="fatch_its_name">
        </div>
        <a href="#" id="add_details" class="btn btn-success">Add</a>
        <div class="clearfix"></div>
      </div>
      
      <div class="form-group add_ajx_result">
        <label class="control-label col-md-2 text-right"></label>
        <div class="col-md-5 entry_ajax_result" style="border: 1px solid #000;border-radius: 5px;margin-left: 10px;">
          <?php
          if ($result['type'] == CRS_ITS_ID) {
            $its_result = $crs_group->get_group_data('crs_group_its', $id);
            if ($its_result) {
              foreach ($its_result as $group_its){
                $user_full_name = $user_admin->get_full_name_of_user_its($group_its);
                echo '<div class="'.$group_its.'"><p class="form-control-static">'.$user_full_name.' <a href="function(this.remove());" class="fa fa-remove pull-right"></a></p><input type="hidden" name="final_its_id[]" value="'.$group_its.'"></div>';
              }
            }
          }
        ?>
        </div>
        <div class="clearfix"></div>
      </div>
      
      <div class="form-group marhala">
        <label class="control-label col-md-2 text-right">Marhala :</label>
        <div class="col-md-5">
          <select name="marhala[]" multiple class="form-control" id="marhala">
            <option value="">Select Marhala</option>
            <?php
              if ($marhala_array) {
                foreach ($marhala_array as $key=>$value) {
                  ?>
                  <option value="<?php echo $key; ?>"><?php echo $value; ?></option>
                  <?php
                }
              }
            ?>
          </select>
        </div>
        <div class="clearfix"></div>
        <?php
          if ($result['type'] == CRS_MARHALA) {
            $marhala_result = $crs_group->get_group_data('crs_group_marhala', $id);
            if ($marhala_result) {
              foreach ($marhala_result as $group_marhala){
                $marhala_result_data[] = $marhala_array[$group_marhala];
                echo '<input type="hidden" name="last_marhala[]" value="'.$group_marhala.'">';
              }
              
              $marhala_result_data = implode(', ', $marhala_result_data);
              echo '<div class="col-md-2"></div>';
              echo '<div class="col-md-5">';
              echo '<p>'.$marhala_result_data.'</p>';
              echo '</div>';
              echo '<div class="clearfix"></div>';
            }
          }
        ?>
      </div>

      <label class="control-label col-md-2 text-right"></label>
      <div class="form-group col-md-2 col-xs-12">
        <input type="submit" name="btn_submit" id="btn_submit" value="Submit" class="btn btn-primary btn-block form-control">
      </div>
    </form>
  </div>
</div>
  
<script>
  $('#get_details').on("click", function () {
    var its_id = $('#its_id').val();
    
    if(its_id == '') {
      alert("Please Enter ITS ID.");
      return false;
    } else {
      
      myApp.showPleaseWait();
      $.ajax({
        type: "POST",
        url: "ajax.php",
        data: 'cmd=get_its_id_records&its_id=' + its_id,
        cache: false,
        success: function (response) {
          myApp.hidePleaseWait();
          $('.show_ajx_result').show(600);

          var data = $.parseJSON(response);
          $('.its_name').html(data.user_full_name);
          $('#fatch_its_id').val(data.user_its);
          $('#fatch_its_name').val(data.user_full_name);
        }
      });
    }
  });
  
  $('#add_details').on("click", function () {
    var fatch_its_id = $('#fatch_its_id').val();
    var fatch_its_name = $('#fatch_its_name').val();
    $('.add_ajx_result').show(600);
    
    $text = $('<div class="'+ fatch_its_id +'"><p class="form-control-static">'+ fatch_its_name +' <a href="javascript:void(0);" class="fa fa-remove pull-right"></a></p><input type="hidden" name="final_its_id[]" value="'+ fatch_its_id +'"></div>');
    
    $text.on("click", function(){$(this).remove();});
    $(".entry_ajax_result").append($text);
  });
  
  $('#group_type').on('click', function() {
    var group_type = $('#group_type').val();
    if(group_type == '<?php echo CRS_JAMAAT; ?>'){
      $('.jamaat').show(600);
      $('.jamiat').hide(600);
      $('.school').hide(600);
      $('.its_id').hide(600);
      $('.marhala').hide(600);
      $('.show_ajx_result').hide(600);
      $('.add_ajx_result').hide(600);
    } else if(group_type == '<?php echo CRS_JAMIAT; ?>') {
      $('.jamaat').hide(600);
      $('.jamiat').show(600);
      $('.school').hide(600);
      $('.its_id').hide(600);
      $('.marhala').hide(600);
      $('.show_ajx_result').hide(600);
      $('.add_ajx_result').hide(600);
    } else if(group_type == '<?php echo CRS_ITS_ID; ?>') {
      $('.jamaat').hide(600);
      $('.jamiat').hide(600);
      $('.school').hide(600);
      $('.its_id').show(600);
      $('.marhala').hide(600);
      $('.show_ajx_result').hide(600);
      $('.add_ajx_result').hide(600);
    } else if(group_type == '<?php echo CRS_SCHOOL; ?>') {
      $('.jamaat').hide(600);
      $('.jamiat').hide(600);
      $('.school').show(600);
      $('.its_id').hide(600);
      $('.marhala').hide(600);
      $('.show_ajx_result').hide(600);
      $('.add_ajx_result').hide(600);
    } else if(group_type == '<?php echo CRS_MARHALA; ?>') {
      $('.jamaat').hide(600);
      $('.jamiat').hide(600);
      $('.school').hide(600);
      $('.its_id').hide(600);
      $('.marhala').show(600);
      $('.show_ajx_result').hide(600);
      $('.add_ajx_result').hide(600);
    }
  });
  
  
  <?php
    $group_type = array(CRS_JAMAAT => 'jamaat', CRS_JAMIAT => 'jamiat', CRS_SCHOOL => 'school', CRS_ITS_ID => 'its_id', CRS_MARHALA => 'marhala');
    
    foreach ($group_type as $group_key => $group_val) {
      if ($group_key == $result['type']) {
  ?>
      $('.<?php echo $group_val; ?>').show();
      
      <?php if ($result['type'] == CRS_ITS_ID) { ?>
          $('.add_ajx_result').show();
      <?php } else { ?>
          $('.add_ajx_result').hide();
      <?php } ?>
    <?php } else { ?>
      $('.<?php echo $group_val; ?>').hide();
  <?php
      }
    }
  ?>
  
  $('.show_ajx_result').hide(600);
  $('#jamaat').selectize();
  $('#jamiat').selectize();
  $('#school').selectize();
  $('#marhala').selectize();
  
  
  var myApp;
  myApp = myApp || (function () {
    var pleaseWaitDiv = $('<div class="modal fade" id="pleaseWaitDialog" data-backdrop="static" data-keyboard="false"><div class="modal-dialog"><div class="modal-content"><div class="modal-header"><h4 class="modal-title">Processing . . .</h4></div><div class="modal-body"><img src="images/loader.gif"></div></div></div></div>');
    return {
      showPleaseWait: function () {
        pleaseWaitDiv.modal();
      },
      hidePleaseWait: function () {
        pleaseWaitDiv.modal('hide');
      },
    };
  })();
</script>

<!-- /#page-wrapper -->
<?php include "./footer.php"; ?>
