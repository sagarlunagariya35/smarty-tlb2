<?php
require_once '../classes/constants.php';
require_once '../classes/class.database.php';
require_once 'classes/admin_gen_functions.php';
require_once 'classes/class.user.admin.php';
require_once 'classes/class.edu_commitee.php';

//$allowed_roles = array(ROLE_AAMIL_SAHEB,ROLE_COMMITTEE_MEMBER,ROLE_EDU_SEC,ROLE_HEAD_OFFICE);
$allowed_roles = array(ROLE_AAMIL_SAHEB);
require_once 'session.php';

$admin_user = new mtx_user_admin();
$admin_user->loaduser($its);
//$mumin_img = $cls_user->get_mumin_photo($its);

//$title = 'Home Page';
$title = 'List of Education Committee Members in ' . $tanzeem_id;
$description = '';
$keywords = '';
$active_page = "as_home";
include_once("header.php");
?>


  <div class="row">
    <div class="col-lg-12">
      <h1 class="page-header">Dashboard <?php echo $admin_user->get_full_name(); ?><?php// echo $_SESSION[TANZEEM_ID]; ?></h1>
    </div>
    <!-- /.col-lg-12 -->
  </div>
  <!-- /.row -->
  <div class="row">
    <div class="col-lg-3 col-md-6">
      <div class="panel panel-primary">
        <div class="panel-heading">
          <div class="row">
            <div class="col-xs-3">
              <i class="fa fa-comments fa-5x"></i>
            </div>
            <div class="col-xs-9 text-right">
              <div class="huge">26</div>
              <div></div>
            </div>
          </div>
        </div>
        <a href="#">
          <div class="panel-footer">
            <span class="pull-left">New Araz Today!</span>
            <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
            <div class="clearfix"></div>
          </div>
        </a>
      </div>
    </div>
    <div class="col-lg-3 col-md-6">
      <div class="panel panel-green">
        <div class="panel-heading">
          <div class="row">
            <div class="col-xs-3">
              <i class="fa fa-tasks fa-5x"></i>
            </div>
            <div class="col-xs-9 text-right">
              <div class="huge">12</div>
              <div></div>
            </div>
          </div>
        </div>
        <a href="#">
          <div class="panel-footer">
            <span class="pull-left">Istirshad Araz Today!</span>
            <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
            <div class="clearfix"></div>
          </div>
        </a>
      </div>
    </div>
    <div class="col-lg-3 col-md-6">
      <div class="panel panel-yellow">
        <div class="panel-heading">
          <div class="row">
            <div class="col-xs-3">
              <i class="fa fa-clock-o fa-5x"></i>
            </div>
            <div class="col-xs-9 text-right">
              <div class="huge">124</div>
              <div></div>
            </div>
          </div>
        </div>
        <a href="#">
          <div class="panel-footer">
            <span class="pull-left">Pending Araz</span>
            <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
            <div class="clearfix"></div>
          </div>
        </a>
      </div>
    </div>
    <div class="col-lg-3 col-md-6">
      <div class="panel panel-red">
        <div class="panel-heading">
          <div class="row">
            <div class="col-xs-3">
              <i class="fa fa-support fa-5x"></i>
            </div>
            <div class="col-xs-9 text-right">
              <div class="huge">13</div>
              <div></div>
            </div>
          </div>
        </div>
        <a href="#">
          <div class="panel-footer">
            <span class="pull-left">Requires Attention</span>
            <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
            <div class="clearfix"></div>
          </div>
        </a>
      </div>
    </div>
  </div>
  <!-- /.row -->
  
  <?php require_once 'as_list_edu_comm_members.php'; ?>
  </div>

<!-- /#page-wrapper -->
<script>

  $(function() {
    Morris.Donut({
      element: 'arz-chart',
      data: [{
          label: "Total Araz",
          value: 1200
        }, {
          label: "Answered",
          value: 1000
        }, {
          label: "Pending Araz",
          value: 200
        }],
      resize: true
    });

    Morris.Bar({
      element: 'arz-trend-chart',
      data: [{
          y: '2008',
          a: 1000,
          b: 800,
          c: 200
        }, {
          y: '2009',
          a: 1500,
          b: 1200,
          c: 600
        }, {
          y: '2010',
          a: 3000,
          b: 1800,
          c: 1000
        }, {
          y: '2011',
          a: 5000,
          b: 4000,
          c: 2500
        }, {
          y: '2012',
          a: 6000,
          b: 5500,
          c: 3000
        }, {
          y: '2013',
          a: 8000,
          b: 5000,
          c: 4000
        }, {
          y: '2014',
          a: 9000,
          b: 6000,
          c: 5000
        }],
      xkey: 'y',
      ykeys: ['a', 'b', 'c'],
      labels: ['Pre Primary', 'Primary', 'Higher Education'],
      hideHover: 'auto',
      resize: true
    });

  });

</script>
<?php include "./footer.php"; ?>