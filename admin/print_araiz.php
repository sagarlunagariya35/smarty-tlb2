<?php
require_once 'classes/class.araiz.php';
require_once 'classes/hijri_cal.php';
require_once '../classes/gen_functions.php';

$araiz = new Araiz();
$hijari = new HijriCalendar();

$code = trim($_GET['code'], '/');
$araiz_data = $araiz->get_jawab_sent_araz_and_user_data_by_hashcode($code);
$marhala_id = $araiz_data['marhala'];

$ary_std = get_standards_by_marhala($marhala_id);
// remove the stds till the selected standard
$araz_std = $araiz_data['school_standard'];

if ($ary_std) {
  $remove_std = TRUE;
  foreach ($ary_std as $key => $std) {
    ($araz_std == $std) ? $remove_std = FALSE : '';
    if ($remove_std) {
      unset($ary_std[$key]);
    }
  }
}

$std_text = join(', ', $ary_std);

switch ($marhala_id) {
  case 1:
    $marhala_text = 'پظظلا مرحلة Pre-Primary';
    break;
  case 2:
    $marhala_text = "بيجا مرحلة Primary ($std_text standard/grade)";
    break;
  case 3:
    $marhala_text = "تيجامرحلة ($std_text standard/grade)";
    break;
  case 4:
    $marhala_text = "ححوتهامرحلة ($std_text standard/grade)";
    break;
  case 5:
    $marhala_text = "ثثانححمامرحلة Under graduation ($std_text standard/grade)";
    break;
  case 6:
    $marhala_text = "ححهتا مرحلة Graduation";
    break;
  case 7:
    $marhala_text = " ساتمامرحلة Post-Graduation";
    break;
  case 8:
    $marhala_text = "Diploma";
    break;
}

$quote_saheb = 'انسس جيم الداعي الاجل المقدس سيدنا محمد برهان الدين ر ض يھ ارشاد فرمايو  ؛ كھ';
$quote = ' تعليم صحيح اسلامي منطلق سي هوئي، ايمان ني منزل  اهني منزل هوئي،  اْل محمد ني طاعة اهنو مقصد هوئي، بقاء انسس  نجاة اهنو ثمرة هوئي.';

$title = 'My Araz Jawab';
$description = '';
$keywords = '';
$active_page = "manage_araiz";
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title><?php echo $title; ?></title>

    <link href="<?php echo SERVER_PATH; ?>admin/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo SERVER_PATH; ?>admin/css/custom.css" rel="stylesheet">

    <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">

    <script src="<?php echo SERVER_PATH; ?>admin/js/jquery-1.11.1.js"></script>
    <script src="<?php echo SERVER_PATH; ?>admin/js/jquery.confirm.min.js"></script>
    <style>
      .avoidBreak {
        page-break-inside: avoid !important;
        margin: 4px 0 4px 0;  /* to keep the page break from cutting too close to the text in the div */
      }
      div{
        page-break-inside: avoid;
      }
      .alfatemi-text{
        min-height: 20px;
        margin: 0;
        text-transform: none;
      }
      @media print {
        .pgbr {page-break-before: always;}
      }
    </style>
    <script>
      $(document).ready(function () {
        var ctlTd = $('.dontSplit td');
        if (ctlTd.length > 0)
        {
          //console.log('Found ctlTd');
          ctlTd.wrapInner('<div class="avoidBreak" />');
        }
      });
    </script>
  </head>

  <body style="padding: 10px;">
        <div class="col-md-12 text-center">
          <img src="<?php echo SERVER_PATH; ?>admin/images/Logo-tlb-araz.jpg" height="100" >
        </div>

        <?php
        $araz_courses = $araiz->get_all_araz_course_data($araiz_data['id'], 'jawab_given');
        if ($araz_courses) {
          $araz_years = $araz_courses[0]['course_started'];
          $araz_city = $araz_courses[0]['institute_city'];
          $inst_name = $araz_courses[0]['institute_name'];
          $course = $araz_courses[0]['course_name'];
          $degree = $araz_courses[0]['degree_name'];
        } else {
          $araz_years = '';
          $araz_city = $araiz_data['school_city'];
          $inst_name = $araiz_data['school_name'];
          //$course = $araiz_data['school_standard'];
          $course = $std_text;
        }

        $jawab_city = $araiz_data['jawab_city'];
        $ITS_city = $araiz_data['city'];
        $student_name_ar = $araiz_data['full_name_ar'];
        $ITS = $araiz_data['its_id'];

        $hijari_date = $hijari->GregorianToHijri(strtotime($araiz_data['jawab_ts']));
        $mon = $hijari->monthName($hijari_date[0]);
        $yr = $hijari_date[2];
        $dt = $hijari_date[1];

        $arb_date = $dt . ', ' . $mon . ', ' . $yr . ' H';
        $eng_date = date('d, F Y', strtotime($araiz_data['jawab_ts']));

        $count_araz_courses = $araiz->get_all_araz_course_data($araiz_data['id']);
        if (count($count_araz_courses) > 1) {
          if ($araiz_data['gender'] == 'M') {
            $salutation = 'الاخ النجيب';
            $dua = 'حفظه الله تعالى وسلمه';
            include 'istirshad_araiz.php';
          } else {
            $salutation = 'الاخت النجيبة';
            $dua = 'حفظها الله تعالى وسلمها';
            include 'istirshad_araiz.php';
          }
        } else {
          if ($araiz_data['gender'] == 'M') {
            $salutation = 'الاخ النجيب';
            $dua = 'حفظه الله تعالى وسلمه';
            include 'raza_araiz.php';
          } else {
            $salutation = 'الاخت النجيبة';
            $dua = 'حفظها الله تعالى وسلمها';
            include 'raza_araiz.php';
          }
        }
        ?>
  </body>
</html>
