<?php
include 'session.php';
include 'classLoader.php';
$global = new Glob_func();

$getCity = FALSE;
if (isset($_GET['city']) && $_GET['city']) {
  $getCity = $db->clean_data(urldecode($_GET['city']));
  $records = $global->select_data('araiz', $getCity, 'city');
} else die();

$title = 'City Wise Report';
include './print_inc/print_header.php';
?>
        <div class="col-lg-8 col-lg-offset-2 text-center"><span class="text-center"><strong><?php echo $title; ?></strong></span><span class="pull-right"><?php echo date('d F l, Y'); ?></span></div>
        <div class="col-lg-offset-2 col-lg-8">
          <?php require '../inc/school_wise.php'; ?>
        </div>
<?php include './print_inc/print_footer.php';?>