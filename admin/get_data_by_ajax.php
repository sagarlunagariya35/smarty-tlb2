<?php
include 'classes/class.araiz.php';
include 'classes/class.article.php';
include 'classes/admin_gen_functions.php';

$araiz = new Araiz();
$art = new Article();

//$country = $_GET["country"];
$menu = @$_GET['menu'];

$cmd = (isset($_POST['cmd'])) ? $_POST['cmd'] : '';
$country = (isset($_POST['country'])) ? $_POST['country'] : '';

switch ($cmd) {
  case 'get_cities':
    $country_iso = $araiz->get_country_iso_by_name($country);
    $ary_cities = array_sort($araiz->get_all_city_by_country_iso($country_iso[0]['ISO2']), 'city', SORT_ASC);
    foreach($ary_cities as $city){
      $ret[]['city'] = $city['city'];
    }
    echo json_encode($ret);
    break;
  
  case 'get_degrees':
    $marhala = $_POST['marhala'];
    $degrees = get_degrees_by_marhala($marhala);
    foreach($degrees as $degree){
      $ret[]['degree'] = $degree['degree'];
    }
    echo json_encode($ret);
    break;
    
  case 'get_courses':
    $degree = $_POST['degree'];
    $marhala = $_POST['marhala'];
    $courses = array_sort($araiz->get_courses_by_degree($degree, $marhala),'course_name',SORT_ASC);
    foreach($courses as $course){
      $ret[]['course_name'] = $course['course_name'];
    }
    echo json_encode($ret);
    break;
}

if ($menu) {
  $sub_menu = get_submenu_by_menu($menu);

  echo '<option value="">Select Sub Menu</option>';
  if ($sub_menu) {
    foreach ($sub_menu as $s) {
      ?>
      <option value="<?php echo $s['id']; ?>"><?php echo $s['name']; ?></option>

      <?php
    }
  }
}
?>