<?php
require_once '../classes/constants.php';
require_once '../classes/class.database.php';
include 'classes/class.courses.php';
$allowed_roles = array(ROLE_HEAD_OFFICE);
require_once 'session.php';

$course = new Mtx_Courses();

$title = 'ADD Course Question';
$description = '';
$keywords = '';
$active_page = "list_courses";

$ques_img_file_name = FALSE;

$course_id = (isset($_GET['course_id'])) ? $_GET['course_id'] : 0;

if (isset($_POST['delete_id'])) {
  $delete_id = $_POST['delete_id'];
  $result = $course->delete_course_question($delete_id);

  if ($result) {
    $_SESSION[SUCCESS_MESSAGE] = "Question Successfully Delete";
    header('Location: courses_question.php?course_id='.$course_id);
    exit();
  } else {
    $_SESSION[ERROR_MESSAGE] = "Error while Deleting";
  }
}

$last_insert_id = $course->get_last_insert_course_question();
$last_insert_id = $last_insert_id + 1;

if (isset($_POST['btn_submit'])) {
  $data = $db->clean_data($_POST);
  // get variables
  $ques_type = $data['ques_type'];
  
  if($ques_type == QUIZ_GENERAL_HTML){
   $question = stripcslashes(addslashes($_POST['question_gen_html']));
  }else {
    $question = htmlentities($data['question']);
  }
  
  if($_FILES['ques_img']['name'] != ''){
    $ques_img = $_FILES['ques_img']['name'];
    $ques_img_temp = $_FILES['ques_img']['tmp_name'];
    $ques_img_file_name = $course_id.'-'.$last_insert_id.'-'.$ques_img;
    $pathANDname = "../upload/courses_quiz/" . $ques_img_file_name;
    move_uploaded_file($ques_img_temp, $pathANDname);
  }
  
  $timer = $data['timer'];
  foreach ($data['option'] as $k => $o){
    $data['option'][$k] = htmlentities($o);
  }
  $option = serialize($data['option']);
  foreach ($data['correct_answer'] as $k => $o){
    $data['correct_answer'][$k] = htmlentities($o);
  }
  $correct_answer = serialize($data['correct_answer']);
  $user_id = $_SESSION[USER_ID];
  
  $insert = $course->insert_course_question($ques_type, $question, $ques_img_file_name, $timer, $option, $correct_answer, $user_id, $course_id);

  if ($insert) {
    $_SESSION[SUCCESS_MESSAGE] = 'Question Inserted Successfully.';
  } else {
    $_SESSION[ERROR_MESSAGE] = 'Error encountered while Inserting Question';
  }
}

$course_data = $course->get_course_by_id($course_id);
$course_questions = $course->get_courses_question_by_course_id($course_id);

include_once("header.php");
?>
<script type="text/javascript" src="ckeditor/ckeditor.js"></script>

<style>
  .alfatemi-text{
    font-size: 18px;
    display: table;
  }
</style>

<div class="row">
  <div class="col-lg-12">
    <h3 class="page-header">Add Question in <?php echo $course_data['course_title']; ?></h3>
  </div>
  <!-- /.col-lg-12 -->
</div>

<div class="row">
  <div class="col-md-12">
    <form method="post" class="forms1" action="" enctype="multipart/form-data">

      <div class="form-group">
        <label class="control-label col-md-2 text-right">Question Type :</label>
        <div class="col-md-5">
          <select name="ques_type" class="form-control" id="ques_type">
            <option value="">Select Question Type</option>
            <option value="<?php echo QUIZ_MULTIPLE_CHOICE; ?>">Multiple Choice</option>
            <option value="<?php echo QUIZ_TRUE_AND_FALSE; ?>">True and False</option>
            <option value="<?php echo QUIZ_FILL_IN_THE_BLANK; ?>">Fill in the Blank</option>
            <option value="<?php echo QUIZ_MATCH_THE_FOLLOWING; ?>">Match the Following</option>
            <option value="<?php echo QUIZ_OPEN_ANSWERS; ?>">Open Answers</option>
            <option value="<?php echo QUIZ_SEQUENCING; ?>">Sequencing</option>
            <option value="<?php echo QUIZ_GENERAL_HTML; ?>">General HTML</option>
          </select>
        </div>
      </div>

      <div class="clearfix"></div>
      <div class="clearfix"><br></div>
      
      <div class="form-group">
        <label class="control-label col-md-2 text-right">Question :</label>
        <div class="col-md-8 html_div">
          <textarea name="question" class="form-control ckeditor"></textarea>
        </div>
        <div class="col-md-8 text_div">
          <textarea name="question_gen_html" class="form-control"></textarea>
        </div>
        <div class="clearfix"></div>
      </div>
      
      <div class="form-group html_div img_div">
        <label class="control-label col-md-2 text-right">Image :</label>
        <div class="col-md-5">
          <input type="file" name="ques_img" class="file">
        </div>
        <div class="clearfix"></div>
      </div>
      
      <div class="form-group">
        <label class="control-label col-md-2 text-right">Timer :</label>
        <div class="col-md-5">
          <input type="text" name="timer" class="form-control">
        </div>
        <div class="clearfix"></div>
      </div>
      
      <div class="form-group masterDiv html_div">
        <label class="control-label col-md-2 text-right">Options :</label>
        <div class="col-md-5">
          <input type="text" name="option[]" class="form-control col-md-5" >
        </div>
        <input type="submit" name="add" id="btnMagic" value="Add" class="btn btn-success btn-sm">
      </div>
      
      <div class="form-group c_masterDiv html_div">
        <label class="control-label col-md-2 text-right">Correct Answer :</label>
        <div class="col-md-5">
          <input type="text" name="correct_answer[]" class="form-control col-md-5" >
        </div>
        <input type="submit" name="add" id="c_btnMagic" value="Add" class="btn btn-success btn-sm">
      </div>

      <div class="clearfix"></div>
      <label class="control-label col-md-2 text-right"></label>
      <div class="form-group col-md-2 col-xs-12">
        <input type="submit" name="btn_submit" id="btn_submit" value="Submit" class="btn btn-primary btn-block form-control">
      </div>
    </form>
    
    <div style="display: none;">
      <div class="copyDiv">
        <div class="clearfix"><br></div>
        <label class="control-label col-md-2 text-right"></label>
        <div class="col-md-5">
          <input type="text" name="option[]" class="form-control col-md-5" >
        </div>
        <div class="clearfix"><br></div>
      </div>
    </div>
    <div style="display: none;">
      <div class="c_copyDiv">
        <div class="clearfix"><br></div>
        <label class="control-label col-md-2 text-right"></label>
        <div class="col-md-5">
          <input type="text" name="correct_answer[]" class="form-control col-md-5" >
        </div>
        <div class="clearfix"><br></div>
      </div>
    </div>
  </div>
</div>
  
  
<br>
  <div class="row">
    <div class="panel panel-primary">
      <div class="panel-heading">
        <i class="fa fa-group fa-fw"></i> Question List in <?php echo $course_data['course_title']; ?>
      </div>
      <!-- /.panel-heading -->
      <div class="panel-body">
        <div class="table-responsive">
          <table class="table table-striped table-hover table-condensed alfatemi-text">
            <thead>
              <tr>
                <th>Sr</th>
                <th>Question Type</th>
                <th>Question</th>
                <th>Options</th>
                <th>Correct Answer</th>
                <th>Date</th>
                <th>Action</th>
              </tr>
            </thead>
            <tbody>
              <?php
              if($course_questions){
                $sr = 0;
                foreach ($course_questions as $cq) {
                  $sr++;
                  $question_type[1] = 'Multiple Choice';
                  $question_type[2] = 'True and False';
                  $question_type[3] = 'Fill in the Blank';
                  $question_type[4] = 'Match the Following';
                  $question_type[5] = 'Open Answers';
                  $question_type[6] = 'Sequencing';
                  $question_type[7] = 'General Html';
              ?>
                  <tr>
                    <td><?php echo $sr; ?></td>
                    <td><?php echo $question_type[$cq['question_type']]; ?></td>
                    <td><?php echo $cq['question']; ?></td>
                    <td><?php 
                    $res_option = unserialize($cq['options']);
                    echo $res_option = implode(',', $res_option);
                    ?></td>
                    <td><?php 
                    $res_correct_answer = unserialize($cq['correct_answer']);
                    echo $res_correct_answer = implode(',', $res_correct_answer); ?></td>
                    <td><?php echo date('d, F Y',  strtotime($cq['created_ts'])); ?></td>
                    <td>
                      <a href="#" class="confirm" id="<?php echo $cq['id']; ?>"><i class="fa fa-remove fa-fw text-danger"></i></a>
                    </td>
                  </tr>
                  <?php
                  }
                }else {
                  echo '<tr><td class="text-center" colspan="6">No Records..</td></tr>';
                }
              ?>
            </tbody>
          </table>
        </div>
        <!-- /.table-responsive -->
      </div>
      <!-- /.panel-body -->
    </div>
    <form method="post" id="frm-del-grp">
      <input type="hidden" name="delete_id" id="del-ques-val" value="">
    </form>
  </div>
<script>
  $(".confirm").confirm({
    text: "Are you sure you want to delete?",
    title: "Confirmation required",
    confirm: function(button) {
      $('#del-ques-val').val($(button).attr('id'));
      $('#frm-del-grp').submit();
      alert('Are you Sure You want to delete: ' + id);
    },
    cancel: function(button) {
      // nothing to do
    },
    confirmButton: "Yes",
    cancelButton: "No",
    post: true,
    confirmButtonClass: "btn-danger"
  });
  
  $('#btnMagic').on('click', function(e) {
    e.preventDefault();
    $(".copyDiv").clone().appendTo(".masterDiv");
    $(".masterDiv>div.copyDiv").removeClass("copyDiv");
  });

  $('#c_btnMagic').on('click', function(e) {
    e.preventDefault();
    $(".c_copyDiv").clone().appendTo(".c_masterDiv");
    $(".c_masterDiv>div.c_copyDiv").removeClass("c_copyDiv");
  });
  
  $('#ques_type').on('click', function() {
    var ques_type = $('#ques_type').val();
    if(ques_type == '<?php echo QUIZ_OPEN_ANSWERS; ?>'){
      $('.masterDiv').hide(600);
      $('.text_div').hide(600);
      $('.c_masterDiv').show(600);
      $('.img_div').show(600);
    } else if(ques_type == '<?php echo QUIZ_GENERAL_HTML; ?>') {
      $('.html_div').hide(600);
      $('.text_div').show(600);
    } else {
      $('.masterDiv').show(600);
      $('.html_div').show(600);
      $('.text_div').hide(600);
    }
  });
  $('.text_div').hide();
</script>

<!-- /#page-wrapper -->
<?php include "./footer.php"; ?>
