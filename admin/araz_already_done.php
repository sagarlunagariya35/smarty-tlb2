<?php
require_once 'classes/class.user.admin.php';
require_once '../classes/class.user.php';
include 'classes/class.araiz.php';
$allowed_roles = array(ROLE_HEAD_OFFICE, ROLE_AAMIL_SAHEB, ROLE_JAMIAT_COORDINATOR);
require_once 'session.php';

$user = new mtx_user_admin;
$araiz = new Araiz();

$title = 'Alreay Done Araiz';
$description = '';
$keywords = '';
$active_page = "araz_already_done";

$select = FALSE;
$from_date = $to_date = $pagination = FALSE;
$araz_saheb_id = $saheb_id = $araz_coun_id = $coun_id = FALSE;
$counselor_days_past = $saheb_days_past = $jamiyet_data = FALSE;

$jamiyet = unserialize($_SESSION['jamiat']);
if (count($jamiyet) == 1 && $jamiyet[0] != '') {
  $jamiyet_data = implode("','", $jamiyet[0]);
}

// Fix for ROLE_JAMIAT_COORDINATOR is no jamiat assign
if ($_SESSION[USER_ROLE] == ROLE_JAMIAT_COORDINATOR && $jamiyet_data === FALSE) {
  $jamiyet_data = ' ';
}

include ('header.php');

$select = $araiz->get_all_already_done_araiz(FALSE, $page, 10, $jamiyet_data);
$total_araz = $araiz->get_already_done_araiz_count(FALSE, $jamiyet_data);

$araiz_parsed_data = $araiz->get_araiz_data_organised($select);
?>

<link rel="stylesheet" href="js/tablesorter/css/theme.bootstrap.css">
<script src="js/tablesorter/js/jquery.tablesorter.js"></script>
<script src="js/tablesorter/js/jquery.tablesorter.widgets.js"></script>
<style>
  .alfatemi-text{
    font-size: 18px;
  }
</style>
<div class="row">

  <div class="row">
    <div class="col-xs-12 col-sm-12">&nbsp;</div>
  </div>
  <div class="row">
    <div class="col-xs-12 col-sm-12">
      <h2 class="text-center">Total Already Done Araiz Count: <?php echo $total_araz; ?></h2>
    </div>
  </div>
  <div class="row">
    <div class="col-xs-12 col-sm-12">&nbsp;</div>
  </div>
  <form name="raza_form" class="form" method="post" action="">
  <div class="col-lg-12 col-xs-12 col-sm-12 pull-left">
    <div class="col-md-12">&nbsp;</div>
    <div class="clearfix"></div>
    <div class="row">
      <?php
      require_once 'pagination.php';
      $pagination = pagination(10, $page, 'araz_already_done.php?page=', $total_araz);
        
      foreach ($araiz_parsed_data as $araz_id => $araz) {
        // Create bins for User Data, Araz Data, Previous Studies, Current Studeis, Remarks
        $user_details = $araz['user_data'];
        $araz_general_detail = $araz['araz_data'];
        $previous_study_details = $araz['previous_study_details'];
        $course_details = $araz['course_details'];
        $counselor_details = $araz['counselor_details'];
        
        include 'inc/inc.show_parsed_araiz_details.php';
      }
      echo $pagination;
      ?>
    </div>
  </div>
</form>
</div>
<style>
  .popover{
    color:#000;
  }
</style>
<?php
include 'footer.php';
?>
