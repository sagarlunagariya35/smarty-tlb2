<?php
require_once 'classes/class.user.admin.php';
include 'classes/class.questionary.php';
$allowed_roles = array(ROLE_HEAD_OFFICE);
require_once 'session.php';

$title = 'Questionary';
$description = '';
$keywords = '';
$active_page = "manage_questions";

$ques = new questionary();
$id = $marhala_id = FALSE;
$select = $result = FALSE;

if (isset($_GET['id'])) {
  $result = $ques->que_result(false, false, $_GET['id']);
  $id = $_GET['id'];
}

if (isset($_POST['Update'])) {
  $id = $_GET['id'];
  $groupid = $_POST['groupid'];
  $question = $_POST['question'];
  $que_prev = $_POST['que_prev'];
  $default_ans = $_POST['pre_fill'];

  $question_update = $ques->update_question($groupid, $question, $que_prev, $default_ans, $id);

  ($question_update) ? $_SESSION[SUCCESS_MESSAGE] = "Question Successfully Update" : $_SESSION[ERROR_MESSAGE] = "Error In Update Data";
}

if (isset($_POST['Save'])) {

  $groupid = $_POST['groupid'];
  $question = $_POST['question'];
  $que_prev = $_POST['que_prev'];
  $default_ans = $_POST['pre_fill'];

  $question_insert = $ques->insert_Question($groupid, $question, $que_prev, $default_ans);

  ($question_insert) ? $_SESSION[SUCCESS_MESSAGE] = "question Successfully Insert" : $_SESSION[ERROR_MESSAGE] = "Error In Inserting Data";
}

if (isset($_POST['marhala_id'])) {
  $marhala_id = $_POST['marhala_id'];
  $select = $ques->que_result_of_marhala($marhala_id);
}else {
  $select = $ques->que_result(false, false);
}

$marhala_array = array('1'=>'Pre-Primary','2'=>'Primary','3'=>'Std 5 to 7','4'=>'Std 8 to 10','5'=>'Std 11 to 12','6'=>'Graduation','7'=>'Post Graduation', '8'=>'Diploma');

include ('header.php');
?>

<style>
  tfoot {
    display: table-header-group;
    width: 100%;
    padding: 3px;
    box-sizing: border-box;
  }
  /*.dataTables_info, .dataTables_paginate{
    display: inline-block;
  }*/
</style>

<div id="wrapper">

  <!-- Navigation -->

  
    <div class="row">

      <div class="col-md-12">
        <div class="col-lg-12">
          <h3 class="page-header">Add Questionary</h3>
        </div>
        <!-- /.col-lg-12 -->

        <form method="post" action="" role="form" class="form-horizontal">

          <div class="form-group col-md-4">
            <label class="control-label col-md-4">Group ID:</label>
            <div class="col-md-8">
              <select name="groupid" class="form-control" id="groupid">
                <option value="">Select Marhala</option>
                <?php
                  foreach ($marhala_array as $key=>$val){
                    if($result[0]['grpID'] == ''){
                      $grpID = $marhala_id;
                    }else {
                      $grpID = $result[0]['grpID'];
                    }
                    
                    $selected = ($grpID == $key) ? 'selected' : '';
                ?>
                  <option value="<?php echo $key; ?>" <?php echo $selected; ?>><?php echo $val; ?></option>
                <?php
                  }
                ?>
              </select>
            </div>
          </div>

          <div class="form-group col-md-8">
            <label class="control-label col-md-2">Question:</label>
            <div class="col-md-10"><input type="text" class="form-control col-md-3" name="question"  placeholder="Add Question" value="<?php echo $result[0]['question']; ?>" required></div>
          </div>

          <div class="form-group col-md-4">
            <label class="control-label col-md-4">Pre-fill :</label>
            <div class="col-md-4">
              <label>
                <input type="checkbox" name="pre_fill" id="pre_fill" style="position: absolute;" value="0" <?php echo ($result[0]['preferred'] == '1') ? 'checked' : ''; ?>>
              </label>
            </div>
          </div>

          <div class="form-group col-md-8">
            <label class="control-label col-md-2">Question Preview:</label>
            <div class="col-md-10"><input type="text" class="form-control" name="que_prev" placeholder="Add Question Preview" value="<?php echo $result[0]['question_preview']; ?>"></div>
          </div>


          <div class="form-group">
            <?php
            $btn_name = $id ? 'Update' : 'Save';
            ?>
            <button name="<?php echo $btn_name; ?>" class="btn btn-success"><?php echo $btn_name ?></button>
          </div>
        </form>
      </div>
      
      <?php
        $marhala[1] = "Pre-Primary";
        $marhala[2] = "Primary";
        $marhala[3] = "Std 5 to 7";
        $marhala[4] = "Std 8 to 10";
        $marhala[5] = "Std 11 to 12";
        $marhala[6] = "Graduation";
        $marhala[7] = "Post Graduation";
        $marhala[8] = "Diploma";
      ?>
      
      <div class="row">
        <div class="col-lg-12">
          <div class="panel panel-default">
            <div class="panel-heading">
              Questionary List
            </div>
            <!-- /.panel-heading -->
            <div class="panel-body">
              <div class="table-responsive">
                <table class="table table-striped table-condensed table-hover" id="example">
                  <thead>
                    <tr>
                      <th>Group Id</th>
                      <th>Questions</th>
                      <th>Action</th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php
                    $i = 1;
                    foreach ($select as $data) {
                      ?>

                      <tr>
                        <td><?php echo $marhala[$data['grpID']]; ?></td>
                        <td><?php echo substr($data['question'], 0, 100); ?>&nbsp;&nbsp;<?php if(strlen($data['question']) > 100) { ?><a href="#" data-toggle="modal" data-target="#myModal<?php echo $data['id']; ?>">view</a><?php } ?></td>
                        <td>
                          <a href="questionary.php?id=<?php echo $data['id']; ?>"><i class="fa fa-edit"></i></a>
                          <button class="btn btn-link confirm" id="<?php echo $data['id']; ?>"><i class="fa fa-remove text-danger"></i></button>
                        </td>
                        <div class="modal fade" id="myModal<?php echo $data['id']; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                          <div class="modal-dialog">
                            <div class="modal-content">
                              <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                <h4 class="modal-title" id="myModalLabel">Tasavvuraat Araz Text</h4>
                              </div>
                              <div class="modal-body">
                                <?php echo $data['question']; ?>
                              </div>
                            </div>
                          </div>
                        </div>
                      </tr>
                      <?php
                    }
                    ?>
                  </tbody>
                </table>
              </div>
              <!-- /.table-responsive -->
            </div>
            <!-- /.panel-body -->
          </div>
          <!-- /.panel -->
        </div>
        <!-- /.col-lg-12 -->
      </div>

      <!-- DataTables JavaScript -->
      <script src="js/plugins/dataTables/jquery.dataTables.min.js"></script>
      <script src="js/plugins/dataTables/dataTables.bootstrap.js"></script>

    </div>
    <!-- /.row -->

  <!-- /#page-wrapper -->
  <form method="post" id="frm-del-question">
    <input type="hidden" name="id" id="del-grp-val" value="">
    <input type="hidden" name="cmd" value="del-question">
  </form>
  
  <form method="post" id="frm-search-grp">
    <input type="hidden" name="marhala_id" id="marhala_id" value="">
  </form>

</div
<!-- /#wrapper -->

<script>
  // DataTable
  $(document).ready(function() {
    $('#pre_fill').on('change',function(){
      if(this.checked) {
        $('#pre_fill').val('1');
      } else {
        $('#pre_fill').val('0');
      }
    });
  });
  
  $('#groupid').on("change", function () {
    var marhala = $(this).val();
    $('#marhala_id').val(marhala);
    $('#frm-search-grp').submit();
  });

  $(".confirm").confirm({
    text: "Are you sure you want to delete?",
    title: "Confirmation required",
    confirm: function(button) {
      $('#del-grp-val').val($(button).attr('id'));
      $('#frm-del-grp').submit();
      alert('Are you Sure You want to delete: ' + id);
    },
    cancel: function(button) {
      // nothing to do
    },
    confirmButton: "Yes",
    cancelButton: "No",
    post: true,
    confirmButtonClass: "btn-danger"
  });
</script>

<?php
include 'footer.php';
?>

