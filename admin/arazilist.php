<?php
 include 'classes/class.arazilist.php'; 
 include 'classes/class.addstate.php';
 include 'classes/class.addcity.php';
 include 'classes/class.addcountry.php';
 $allowed_roles = array(ROLE_HEAD_OFFICE);
 require_once 'session.php';
 
 $country = new Country();
 $state = new State();
 $city = new City();
 
 $arazi = new Arazi();
 $id=FALSE;
 $select=FALSE;
 
 $title = 'Arazi List';
$description = '';
$keywords = '';
$active_page = "Arazilist";

if(isset($_GET['id']))
{
$select =  $arazi->arz_result(false, false,$_GET['id']);
//print_r($select);
$id = $_GET['id'];
}

if (isset($_POST['Update'])) {
    $id = $_GET['id'];
 
    
    $standrad = $_POST['standrad'];
    $country = $_POST['country'];
   $state = $_POST['statename'];
    $cityname = $_POST['cityname'];
    $mschname = $_POST['mschname'];
    $schcity = $_POST['schcity'];
    $schstd = $_POST['schstd'];
    $schname = $_POST['schname'];
    $fieldsele = $_POST['fieldsele'];
    
    $arazi_update =   $arazi->update_arazi($standrad,$country,$state,$cityname,$mschname,$schcity,$schstd,$schname,$fieldsele,$id);
  
    if ($arazi_update)
        $_SESSION[SUCCESS_MESSAGE] = "state Successfully Update";
    if (!$arazi_update)
        $_SESSION[ERROR_MESSAGE] = "Error In Update Data";
 
        
      ?>
<script>window.location.href="arazilist.php"; </script>
        
<?php }

if (isset($_POST['Save'])) {
   
  $country = $_POST['country'];
    $state = $_POST['statename'];
   
    $state_insert = $state->insert_state($country,$state);
    if ($state_insert)
        $_SESSION[SUCCESS_MESSAGE] = "state Successfully Insert";
    if (!$state_insert)
        $_SESSION[ERROR_MESSAGE] = "Error In Inserting Data";
    
}
?>

<?php include ('header.php');?>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>

    <div id="wrapper">

        <!-- Navigation -->

        
            <div class="row">
                <div class="col-lg-12">
                    <h3 class="page-header">List Arazi</h3>
                </div>
                <!-- /.col-lg-12 -->
		
	



<form method="post"  action="" role="form" class="form-horizontal" align="center" >

  <div class="col-md-12">&nbsp;</div>
  
  
    <div class="form-group col-md-12">
      
      
      <div class="form-group col-md-6">
        <label class="control-label col-md-4">Standard:</label>
        <div class="col-md-5"><input type="text" class="form-control" name="standrad"  placeholder="Add standard" value="<?php echo $select[0]['standard']; ?>" required></div>
    </div>
    
     <div class="form-group col-md-6">  
    <label class="control-label col-sm-4">Country:</label>
    <div class="col-md-5">
          <?php $select_place = $country->country_result1(); ?>
        <select name="country" class="form-control">
        <?php
        
        foreach ($select_place as $data) {
          if ($data['id']==$select[0]['country_id'])
          $check = 'selected';else {
          $check='';
        }
          ?>
          
          <option value="<?php echo $data['id']; ?>" <?php echo $check; ?> > <?php echo $data['nicename']; ?> </option>
        <?php } ?>
      </select>
    </div>
      </div>
  
  
    <div class="form-group col-md-6">
        <label class="control-label col-md-4">State:</label>
        <div class="col-md-5"><input type="text" class="form-control" name="statename"  placeholder="Add State" value="<?php echo $select[0]['state']; ?>" required></div>
    </div>
  
    <div class="form-group col-md-6">
        <label class="control-label col-md-4">City:</label>
        <div class="col-md-5"><input type="text" class="form-control" name="cityname"  placeholder="Add State" value="<?php echo $select[0]['city']; ?>" required></div>
    </div>
  
    <div class="form-group col-md-6">
        <label class="control-label col-md-4">Madrasha Name:</label>
        <div class="col-md-5"><input type="text" class="form-control" name="mschname"  placeholder="Add Madrasha School" value="<?php echo $select[0]['school']; ?>" required></div>
    </div>
    <div class="form-group col-md-6">
        <label class="control-label col-md-4"> School City:</label>
        <div class="col-md-5"><input type="text" class="form-control" name="schcity"  placeholder="Add School City" value="<?php echo $select[0]['school_city']; ?>" required></div>
    </div>
    <div class="form-group col-md-6">
        <label class="control-label col-md-4">School standard:</label>
        <div class="col-md-5"><input type="text" class="form-control" name="schstd"  placeholder="Add School Standrad" value="<?php echo $select[0]['school_std']; ?>" required></div>
    </div>
    <div class="form-group col-md-6">
        <label class="control-label col-md-4">School Name:</label>
        <div class="col-md-5"><input type="text" class="form-control" name="schname"  placeholder="Add School Name" value="<?php echo $select[0]['school_school']; ?>" required></div>
    </div>
    <div class="form-group col-md-6">
        <label class="control-label col-md-4">Field Selection:</label>
        <div class="col-md-5"><input type="text" class="form-control" name="fieldsele"  placeholder="Field Selection" value="<?php echo $select[0]['fieldSelection']; ?>" required></div>
    </div>

    <div class="form-group col-md-6">
        <div class="col-md-8 col-md-offset-4">
            <?php
            $btn_name = $id ? 'Update' : 'Save';
            ?>
                <button name="Update" class="btn btn-success col-md-7">Update</button>
        </div>
    </div>


</form>
                
                <div class="col-lg-10 col-xs-12 col-sm-12 ">
      <div class="col-md-12">&nbsp;</div>
      <div class="clearfix"></div>
      <div class="panel panel-primary">
        <div class="panel-heading">
          <i class="fa fa-group fa-fw"></i> Arazi list
        </div>
         <div class="panel-body">
                <div class="col-sm-10 text-center col-sm-offset-1"> 
    <table class="table table-hover table-condensed table-bordered">
      <thead>
<!--      <th>No</th>-->
<!--      <th>Group Id</th>-->
      <th>Id</th>
      <th>Name</th>
      <th>Student its</th>
<!--      <th>School</th>
      <th>School Standard</th>-->
      <th>Action</th>
      </thead>
      <tbody>
<?php
$select =  $arazi->arz_result($page, 10);
$count_question = $arazi->count_arz_record1();
      require_once 'pagination.php';
      $pagination = pagination(10, $page, 'arazilist.php?page=', $count_question);

     

foreach ($select as $data)
    {?>

    
    <tr>
        <td>
        <?php echo $data['id']; ?>
        </td>
        <td>
        <?php //echo $data['name']; ?>
        </td>
        <td>
        <?php echo $data['login_its']; ?>
        </td>
        
        
<!--        <td>
        <?php
//        $select_place = $country->country_result1();
//        foreach ($select_place as $data1) {
//          if ($data1['id']==$data['country'])
//          
//        echo $data1['nicename'];
        
        //}?>
        </td>-->
<!--        <td>
        <?php 
         //echo $data['stateName']; ?>
        </td>-->
<!--        <td>
        <?php //echo $data['city']; ?>
        </td>-->
<!--          <td>
          <?php //echo $data['school']; ?>
          </td>-->
        
<!--          <td>
          <?php //echo $data['school_std']; ?>
          </td>-->
        
        <td><a href="arazilist.php?id=<?php echo $data['id']; ?>">Update</a>
       
    </td>
      
        </tr>
    <?php
    }
?>
          </tbody>
            </table>
          </div>
          <!-- /.table-responsive -->
        </div>
        <div class="panel-footer">
          <?php echo $pagination; ?>
          <div class="clearfix"></div>
        </div>
        <!-- /.panel-body -->
      </div>
    </div>
            <!-- /.row -->
            <div class="row">
                <div class="col-lg-8">
                 
                    <!-- /.panel -->
                    
                      
                    </div>
                    <!-- /.panel .chat-panel -->
                </div>
                <!-- /.col-lg-4 -->
           
            <!-- /.row -->
    
        <!-- /#page-wrapper -->

    <!-- /#wrapper -->
 <?php 
             include 'footer.php';
 ?>

   

