<?php
//include 'session.php';
include 'classLoader.php';
$global = new Glob_func();

$records = $global->getGroupByData(array('city', 'school', 'group_id') ,'araiz');

$title = 'City Wise Report';
include './print_inc/print_header.php';
?>

        <div class="col-lg-8 col-lg-offset-2 text-center"><span class="text-center"><strong><?php echo $title; ?></strong></span><span class="pull-right"><?php echo date('d F l, Y'); ?></span></div>
        <div class="col-lg-offset-2 col-lg-8">
          <?php require 'inc/city_wise.php'; ?>
        </div>
      </div>
      <!-- /.col-lg-12 -->
    <?php include './print_inc/print_footer.php';?>