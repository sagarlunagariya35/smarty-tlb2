<?php
require_once '../classes/constants.php';
require_once '../classes/class.database.php';
require_once 'classes/admin_gen_functions.php';
require_once 'classes/class.user.admin.php';
require_once 'classes/class.edu_commitee.php';

$allowed_roles = array(ROLE_HEAD_OFFICE);
require_once 'session.php';

// Check if tanzeem_id set or else redirect to list page
if (isset($_GET['city']) && $_GET['city'] !== '') {
  $tanzeem_id = $_GET['city'];
  // Get list of members in the tanzeem
  $edu_comm = new Edu_committee($tanzeem_id);
  $edu_comm_members = $edu_comm->members_get_list();
  $is_downloaded = FALSE;

  foreach ($edu_comm_members as $key => $mem) {
    if ($mem['downloaded']) {
      $is_downloaded = TRUE;
    }
  }
} else {
  header('Location: ' . SERVER_PATH . 'admin/ho_list_gaam.php');
}

if (isset($_POST['cmd']) && $_POST['cmd'] == 'update') {
  if (isset($_POST['approve'])) {
    $edu_comm->save_approved($_POST['approve']);
    $edu_comm_members = $edu_comm->members_get_list();
  }
  if (isset($_POST['reject'])) {
    $edu_comm->save_rejected($_POST['reject']);
    $edu_comm_members = $edu_comm->members_get_list();
  }
}

if (isset($_GET['cmd']) && $_GET['cmd'] == 'reset') {
  $its = $_GET['its'];
  $edu_comm->member_reset($its);
  header('location: ho_list_edu_comm_members.php?city='.$tanzeem_id);
}

if (isset($_POST['cmd']) && $_POST['cmd'] == 'download') {
  $edu_comm->delete_all_rejected_members();
  
  $edu_comm_members_approved = $edu_comm->members_get_list_approved();
    
  // Update designation to string from ID
  foreach ($edu_comm_members_approved as $key => $mem) {
    $edu_comm_members_approved[$key]['designation'] = $edu_comm->designation_by_id_to_string($mem['designation']);
  }
  $csv_head[] = array('its', 'name', 'designation', 'city');
  $csv_head[] = array('', 'العامل الحاضر', 'President', $edu_comm->get_jamat_name());
  $csv_head[] = array('', 'لجنة العمال والمعلمين والمراقبين', '', $edu_comm->get_jamat_name());
  $edu_comm->save_downloaded();
  csv_download($edu_comm_members_approved, $edu_comm->get_jamat_name() . '_approved_members.txt', $csv_head);
  header('location: ho_list_edu_comm_members.php');
}

if (isset($_POST['cmd']) && $_POST['cmd'] == 'upload') {
  $target_dir = "misaal/";
  $target_file = $target_dir . $edu_comm->get_jamat_name() . '.pdf';

  $uploaded = move_uploaded_file($_FILES['misal_pdf']['tmp_name'], $target_file);

  if ($uploaded) {
    $edu_comm_members_approved = $edu_comm->committe_misal_uploaded($target_file);
    $_SESSION[SUCCESS_MESSAGE] = "Misaal uploaded successfully";
  } else {
    $_SESSION[ERROR_MESSAGE] = "Please choose the file before pressing upload.";
  }
}

$edu_comm_members = $edu_comm->members_get_list();

$title = 'Head office back end module';
$description = '';
$keywords = '';
$active_page = "ho_list_edu_comm_members";

include_once("header.php");
?>

  <div class="row">
    <div class="col-lg-12">
      <h1 class="page-header">List of Education Committee Members in <?php echo $edu_comm->get_jamat_name(); ?></h1>
    </div>
    <!-- /.col-lg-12 -->
  </div>
  <!-- /.row -->

  <!-- List of Edu Committee -->
  <form method="post">

    <div class="row">

      <div class="col-md-12">
        <table class="table table-responsive table-condensed">
          <thead>
            <tr>
              <th>Sr</th>
              <th>Photo</th>
              <th>Name</th>
              <th>Designation</th>
              <th>City</th>
              <th>Mobile</th>
              <th class="text-center">
                <?php if (!$is_downloaded) { ?>
            <div class="checkbox">
              <label>
                <input type="checkbox" name="approve_all" id="approve_all"> Approve
              </label>
            </div>
          <?php } ?>
          </th>
          <th class="text-center">
            <?php if (!$is_downloaded) { ?>
            <div class="checkbox">
              <label>
                <input type="checkbox" name="reject_all" id="reject_all"> Reject
              </label>
            </div>
          <?php } ?>
          </th>
          </tr>
          </thead>
          <tbody>
            <tr>
              <td>1</td>
              <td></td>
              <td class="lsd">العامل الحاضر</td>
              <td>President</td>
              <td><?php echo $edu_comm->get_jamat_name() ?></td>
              <td></td>
            </tr>
            <tr>
              <td>2</td>
              <td></td>
              <td class="lsd" colspan="2">لجنة العمال والمعلمين والمراقبين</td>
              <td><?php echo $edu_comm->get_jamat_name() ?></td>
              <td></td>
            </tr>
            <?php
            if ($edu_comm_members) {
              $i = 2;
              $not_verified = 0;
              $rejected = 0;
              $downloaded = 0;
              $misal_uploaded = 0;
              foreach ($edu_comm_members as $mem) {
                $i++;
                $co_ord = ($mem['is_co_ordinator'] == 1) ? ', Co-ordinator' : '';
                $mamur_co_ord = ($mem['is_mamur_co_ordinator'] == 1) ? ', Co-ordinator' : '';
                $tr_class = 'bg-warning';
                
                if($mem['ho_approved'] != 1 && $mem['ho_rejected'] != 1)
                {
                  $not_verified++;
                }
                if ($mem['ho_approved']) {
                  $status = 'Approved';
                  $tr_class = 'bg-success';
                }
                if ($mem['ho_rejected']) {
                  $rejected++;
                  $status = 'Rejected';
                  $tr_class = 'bg-danger';
                }
                if ($mem['downloaded']) {
                  $downloaded++;
                  $status = 'Downloaded';
                  $tr_class = 'bg-primary';
                }
                if ($mem['misaal']) {
                  $misal_uploaded++;
                  $status = 'Misaal Uploaded';
                  $tr_class = 'bg-info';
                }
                $tmp_user = new mtx_user_admin();
                $mem_img = $tmp_user->get_mumin_photo($mem['its']);
                ?>
                <tr class="<?php echo $tr_class; ?>">
                  <td rowspan="3"><?php echo $i; ?></td>
                  <td rowspan="3"><img src="<?php echo $mem_img ?>" height="80px"></td>
                  <td class="lsd"><?php echo $mem['name']; ?></td>
                  <td><?php echo $edu_comm->designation_by_id_to_string($mem['designation']) . $co_ord . $mamur_co_ord; ?></td>
                  <td><?php echo $mem['jamaat']; ?></td>
                  <td><?php echo $mem['mobile']; ?></td>
                  <?php
                  if ($mem['ho_approved'] || $mem['ho_rejected']) {
                    echo "<td class='text-center' colspan='2' rowspan='1'>$status</td>";
                  } else {
                    ?>
                    <td class="text-center" rowspan="3">
                      <div class="checkbox">
                        <label>
                          <input type="checkbox" class="approve" name="approve[]" value="<?php echo $mem['id']; ?>"> Approve
                        </label>
                      </div>
                    </td>
                    <td class="text-center" rowspan="3">
                      <div class="checkbox">
                        <label>
                          <input type="checkbox" class="reject" name="reject[]" value="<?php echo $mem['id']; ?>"> Reject</label>
                      </div>
                    <?php } ?>

                  </td>
                </tr>
                <tr class="<?php echo $tr_class; ?>">
                  <td><strong>Qualification: </strong><?php echo $mem['qualification']; ?></td>
                  <td colspan="2"><strong>Occupation: </strong><?php echo $mem['occupation']; ?></td>
                  <td><?php echo $mem['email']; ?></td>
                <?php if($mem['ho_approved'] != 1 && $mem['ho_rejected'] != 1) { ?>
                  <td rowspan="2" colspan='2' class="text-center">
                  </td>
                <?php } else {
                if(!$misal_uploaded) { ?>
                  <td rowspan="2" colspan='2' class="text-center">
                    <a class="btn btn-danger" href="?city=<?php echo $tanzeem_id; ?>&its=<?php echo $mem['its']; ?>&cmd=reset">Reset</a>
                  </td>
                <?php } } ?>
                </tr>
                <tr class="<?php echo $tr_class; ?>">
                  <td><strong>Last Update: </strong><?php echo $mem['as_timestamp']; ?></td>
                  <td colspan="3"><strong></strong><?php echo ''; ?></td>
                </tr>
                <?php
              }
            } else {
              ?>
              <tr>
                <td colspan="4">No Records Found.</td>
              </tr>
              <?php
            }
            ?>
          </tbody>
        </table>
      </div><!-- ./ md-12 -->

    </div><!-- ./ row -->

    <?php if (!$downloaded && $not_verified != 0 && !$rejected) { ?>
      <div class="row">
        <div class="col-md-3 pull-right">
          <input type="hidden" name="cmd" value="update">
          <input type="submit" name="submit" value="Update" class="btn btn-success btn-block">
        </div>
      </div>
      <input type="hidden" name="tanzeem_id" value="<?php echo $tanzeem_id; ?>">
    <?php } ?>
  </form>
  <div class="row">&nbsp;</div>
<?php
  if ($not_verified == 0) {
    ?>
  <div class="row">
    <div class="col-md-3 col-md-offset-9 text-right">
      <a href="<?php echo SERVER_PATH; ?>admin/ho_print_list_committe_members.php?city=<?php echo $_GET['city']; ?>" class="btn btn-success btn-block" target="_blank">Download with Photos</a>
    </div>
  </div>
  <div class="row">&nbsp;</div>

  
    <div class="row">

      <div class="col-md-3 col-md-offset-9">
        <form class="form-inline" method="post">
          <input type="hidden" name="cmd" value="download">
          <input type="submit" name="submit" value="Download Member List" class="btn btn-success btn-block">
        </form>
      </div>
    </div>
    <?php
  }
  ?>
  <div class="row">&nbsp;</div>
  <?php
  if ($downloaded > 0) {
    ?>
    <div class="row">
      <div class="col-md-3 col-md-offset-9">
        <form class="form-inline" method="post" enctype="multipart/form-data">
          <input type="hidden" name="cmd" value="upload">
          <input type="file" required="required" name="misal_pdf">
          <input type="submit" name="submit" value="Upload Misaal" class="btn btn-success btn-block">
        </form>
      </div>
    </div>
    <?php
  }
  ?>
  <div class="row">&nbsp;</div>
  <?php
  if ($misal_uploaded > 0) {
    ?>
    <div class="row">
      <div class="col-md-3 col-md-offset-9">
        <a class="btn btn-info btn-block" href="misaal/<?php echo $edu_comm->get_jamat_name() ?>.pdf">Download Misaal</a>
      </div>
    </div>
    <?php
  }
  ?>

<script>
  $('#approve_all').on('click', function () {
    if (this.checked) {
      $(".approve").prop('checked', true);
      $(".reject").prop('checked', false);
      $("#reject_all").prop('checked', false);
    } else {
      $(".approve").prop('checked', false);
    }
  });
  $('#reject_all').on('click', function () {
    if (this.checked) {
      $(".reject").prop('checked', true);
      $(".approve").prop('checked', false);
      $("#approve_all").prop('checked', false);
    } else {
      $(".reject").prop('checked', false);
    }
  });
</script>
<!-- /#page-wrapper -->
<?php include "./footer.php"; ?>
