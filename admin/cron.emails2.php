<?php

require_once 'classes/class.user.admin.php';
require_once 'classes/class.araiz.php';

$user = new mtx_user_admin;
$araiz = new Araiz();

$fh = fopen('log_mtx.txt', 'a');

// Fix the type of araz in the txn table -- fix for istirshad to raza araz type
$cron_raza_istirshad = "UPDATE `txn_view_araz` SET `araz_type` = 'raza' WHERE `araz_id` IN (SELECT araz_id FROM `tlb_araz_courses` GROUP BY `araz_id` HAVING COUNT(*) = 1)";
$cron_raza_istirshad_rslt = $db->query($cron_raza_istirshad);

// send 100 emails fromt he cron table
$cron_emails_query = "SELECT * FROM `tlb_cron_emails` WHERE `email_sent` = 0 LIMIT 0,100";
$cron_emails_result = $db->query_fetch_full_result($cron_emails_query);

if ($cron_emails_result) {
  foreach ($cron_emails_result as $emails) {
    if($emails['to_user']) {
        $subject = $emails['subject'];
        $message = $emails['message'];

      //shoot_mail('janah.mustafa@gmail.com', $subject, $message);
      shoot_mail($emails['to_user'], $subject, $message, $emails['from_user']);
      
      $email_sent_ids[] = $emails['id'];
    }
  }
  
  $query_upd = "UPDATE `tlb_cron_emails` SET `email_sent` = '1' WHERE `id` IN (" . join(', ', $email_sent_ids) . ")";
  $db->query($query_upd);
}

function shoot_mail($to, $subject, $message, $sender = FALSE) {
  $sender = ($sender) ? $sender : 'eduaraiz@talabulilm.com';
  $headers = 'From: ' . $sender . "\r\n" .
          'Bcc: janah.mustafa@gmail.com' . "\r\n" .
          "MIME-Version: 1.0\r\n" .
          "Content-Type: text/html; charset=UTF-8\r\n" .
          'X-Mailer: PHP/' . phpversion();

  mail($to, $subject, $message, $headers);
}

?>
