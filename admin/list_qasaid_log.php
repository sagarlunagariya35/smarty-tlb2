<?php
require_once '../classes/class.database.php';
include 'classes/class.qasida.php';
$allowed_roles = array(ROLE_HEAD_OFFICE);
require_once 'session.php';

$qasaid = new Mtx_Qasaid();

$title = 'List of Qasaid Log';
$description = '';
$keywords = '';
$active_page = 'list_qasaid_log';

$select = FALSE;

if(isset($_POST['submit'])){
  $remarks = $_POST['remarks'];
  $its_key = (array) $_POST['its_key'];
  $subject = $_POST['subject'];
  
  if($its_key){
    foreach ($its_key as $its){
      $result = sent_email_of_report('tlb_log_qasaid','its_id',$its,$remarks,$subject);
    }
    
    if($result){
      $_SESSION[SUCCESS_MESSAGE] = 'E-mail Sent Successfully..';
    }else {
      $_SESSION[ERROR_MESSAGE] = 'Error Encounter While Sending E-mail';
    }
  }else {
    $_SESSION[ERROR_MESSAGE] = 'Please Select any User for sent E-mail';
  }
}

$select = $qasaid->get_log_qasaid();
$total_qasaid = $qasaid->count_total_qasaid();
$total_qasaid = $total_qasaid * 3;

include ('header.php');
?>

<div class="content-wrapper">
  <form method="post" role="form" class="form-horizontal">
    <section class="content">
      <div class="row">
        <div class="col-xs-12">&nbsp;</div>
      </div>
      <div class="row">
        <div class="col-xs-12 col-sm-3" style="padding-top:6px">
          <input type="checkbox" name="print" value="Print" id="check_all" onchange="checkAll(this);" class="css-checkbox1"><label for="check_all" class="css-label1">Check All</label>
          <br><br>
          <input type="text" name="subject" placeholder="Enter Subject" class="form-control">
        </div>
        <div class="col-xs-12 col-sm-6">
          <textarea type="text" class="form-control" rows="4" name="remarks" placeholder="Enter Text"></textarea>
        </div>
        <div class="col-xs-12 col-sm-3">
          <input type="submit" name="submit" value="Send Email" class="btn btn-success btn-block assign-jawab">
          <br>
          <a href="print_qasaid_log.php" target="_blank" class="btn btn-success btn-block pull-right">Print</a>
        </div>
      </div>
      <div class="row">
        <div class="col-xs-12">&nbsp;</div>
      </div>
      <!-- Content -->
      <div class="row">
        <div class="col-md-12">
          <div class="panel panel-primary">
            <div class="panel-heading">
              <i class="fa fa-group fa-fw"></i> <?php echo $title; ?>
            </div>
            <!-- /.panel-heading -->
            <div class="panel-body">
              <div class="table-responsive">
                <table class="table table-striped table-hover table-condensed">
                  <thead>
                    <tr>
                      <th>Sr No.</th>
                      <th>ITS</th>
                      <th>Full Name</th>
                      <th>Email</th>
                      <th>Mobile</th>
                      <th>Average Slider Value</th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php 
                    if($select){
                      $i = 0; 
                      foreach($select as $data){
                        $i++;
                        $slider_value = $data['sum'] / $total_qasaid;
                    ?>
                    <tr>
                      <td><input type="checkbox" id="jawab-<?php echo $i; ?>" name="its_key[]" class="jawab css-checkbox1" value="<?php echo $data['its_id']; ?>"><label for="jawab-<?php echo $i; ?>" class="css-label1"><?php echo $i; ?></label></td>
                      <td><?php echo $data['its_id']; ?></td>
                      <td><?php echo $data['first_name']. ' '. $data['last_name']; ?></a></td>
                      <td><?php echo $data['email']; ?></td>
                      <td><?php echo $data['mobile']; ?></td>
                      <td><?php echo number_format($slider_value, 2).'%'; ?></td>
                    </tr>
                    <?php 
                        } 
                      } else { ?>
                    <tr>
                      <td class="text-center" colspan="5">No Records..</td>
                    </tr>
                    <?php } ?>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
        <!-- /Center Bar -->
      </div>
      <!-- /Content -->
    </section>
  </form>
</div>
<script>
  function checkAll(ele) {
    if (ele.checked) {
      $('.jawab').prop('checked', true);
    } else {
      $('.jawab').prop('checked', false);
    }
  } 
</script>  
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<?php
include('footer.php');
?>