<?php
require_once '../classes/class.database.php';
include 'classes/class.araiz.php';
$allowed_roles = array(ROLE_PROJECT_COORDINATOR,ROLE_ASHARAH_ADMIN);
require_once 'session.php';

$araiz = new Araiz();

$title = 'Psychometric Test Taken';
$description = '';
$keywords = '';
$active_page = "report_psychometric_test_taken";

$psychometric_test_taken = $araiz->get_all_psychometric_test_taken();
$total_psychometric_test_taken = $araiz->count_total_psychometric_test_taken();

$araiz_parsed_psychometric = $araiz->get_araiz_data_organised($psychometric_test_taken);

include ('print_header.php');
?>
<body style="padding: 10px;">
  <div>
    <p style="display: block; text-align: right"><?php echo date('d-m-Y H:i:s'); ?></p>
  </div>
  
  <div class="row">
    <div class="col-xs-12 col-sm-12">
      <h4>Total Psychometric Test Taken: <?php echo $total_psychometric_test_taken; ?></h4>
    </div>
  </div>
  <div class="row">&nbsp;</div>
  
  <div class="row">
    <div class="col-md-12">
      <table class="table table-responsive table-condensed">
        <thead>
          <tr>
            <th>Sr No.</th>
            <th>ITS</th>
            <th>Full Name</th>
            <th>Email</th>
            <th>Mobile</th>
          </tr>
        </thead>
        <tbody>
          <?php
          if($araiz_parsed_psychometric){
          $i = 1;
          foreach ($araiz_parsed_psychometric as $araz_id => $araz) {
            $user_details = $araz['user_data'];
            $araz_general_detail = $araz['araz_data'];
          ?>
            <tr>
              <td><?php echo $i++;?></td>
              <td><?php echo $user_details['login_its']; ?></td>
              <td><?php echo $user_details['user_full_name']; ?></a></td>
              <td><?php echo $user_details['email']; ?></td>
              <td><?php echo $user_details['mobile']; ?></td>
            </tr>
          <?php
            }
          }else {
            echo '<tr><td class="text-center" colspan="5">No Records..</td></tr>';
          }
          ?>
        </tbody>
      </table>
    </div>
  </div>
</body>
</html>
