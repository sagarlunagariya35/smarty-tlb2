<?php
require_once '../classes/constants.php';
require_once '../classes/class.database.php';
require_once 'classes/class.user.admin.php';
require_once 'classes/class.article.php';

$allowed_roles = array(ROLE_HEAD_OFFICE);
require_once 'session.php';

$counselor_requests = get_list_counselor_data();

$title = 'Counselor Requests';
$description = '';
$keywords = '';
$active_page = "list_coun_req";

include_once("header.php");

?>
<style type="text/css">
    .switch_options{
    display: block;
    font-family: "Helvetica", Arial, Sans-serif;
    margin-bottom: 10px;
}
/* Main block clearfix */
.switch_options:before,
.switch_options:after{
    content:'.';
    display:block;
    overflow:hidden;
    visibility:hidden;
    font-size:0;
    line-height:0;
    width:0;
    height:0;
}
.switch_options:after{clear:both;}
 
/*Options*/
.switch_options span{
    display: inline-block;
    float: left;
    padding: 4px 9px;
    margin: 0;
    cursor: pointer;
    font-size: 12px;
    font-weight: 700;
    color: #555;
    border: 1px solid #aaa;
    text-transform: uppercase;
    background: #ffffff; /* Old browsers */
    background: -moz-linear-gradient(top,  #ffffff 0%, #e5e5e5 100%); /* FF3.6+ */
    background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,#ffffff), color-stop(100%,#e5e5e5)); /* Chrome,Safari4+ */
    background: -webkit-linear-gradient(top,  #ffffff 0%,#e5e5e5 100%); /* Chrome10+,Safari5.1+ */
    background: -o-linear-gradient(top,  #ffffff 0%,#e5e5e5 100%); /* Opera 11.10+ */
    background: -ms-linear-gradient(top,  #ffffff 0%,#e5e5e5 100%); /* IE10+ */
    background: linear-gradient(to bottom,  #ffffff 0%,#e5e5e5 100%); /* W3C */
    filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#ffffff', endColorstr='#e5e5e5',GradientType=0 ); /* IE6-9 */
 
}
.switch_options span:first-of-type{
    border-radius: 2px 0 0 2px;
    border-right: 0;
}
.switch_options span:last-of-type{
    border-radius: 0 2px 2px 0;
    border-left: 0;
}
.switch_options span:hover{
    background: #fafafa;
}
 
/* Active option */
.switch_options span.selected{
    background: #00b7ea; /* Old browsers */
    background: -moz-linear-gradient(top,  #00b7ea 0%, #009ec3 100%); /* FF3.6+ */
    background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,#00b7ea), color-stop(100%,#009ec3)); /* Chrome,Safari4+ */
    background: -webkit-linear-gradient(top,  #00b7ea 0%,#009ec3 100%); /* Chrome10+,Safari5.1+ */
    background: -o-linear-gradient(top,  #00b7ea 0%,#009ec3 100%); /* Opera 11.10+ */
    background: -ms-linear-gradient(top,  #00b7ea 0%,#009ec3 100%); /* IE10+ */
    background: linear-gradient(to bottom,  #00b7ea 0%,#009ec3 100%); /* W3C */
    filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#00b7ea', endColorstr='#009ec3',GradientType=0 ); /* IE6-9 */
    border-color: #0082A3;
    color: #fff;
}

    
</style>
<script type="text/javascript">
     
jQuery(document).ready(function() {
 
    jQuery('.switch_options').each(function() {
 
        //This object
        var obj = jQuery(this);
 
        var enb = obj.children('.switch_enable'); //cache first element, this is equal to ON
        var dsb = obj.children('.switch_disable'); //cache first element, this is equal to OFF
        var input = obj.children('input'); //cache the element where we must set the value
        var input_val = obj.children('input').val(); //cache the element where we must set the value
 
        /* Check selected */
        if( 0 == input_val ){
            dsb.addClass('selected');
        }
        else if( 1 == input_val ){
            enb.addClass('selected');
        }
 
        //Action on user's click(ON)
        enb.on('click', function(){
            $(dsb).removeClass('selected'); //remove "selected" from other elements in this object class(OFF)
            $(this).addClass('selected'); //add "selected" to the element which was just clicked in this object class(ON) 
            $(input).val(1).change(); //Finally change the value to 1
            //alert('1');
            var id = $('#active').val();
            var arti_id = $('#arti_id').val();
            //alert(arti_id);
            //var data = 'id='+ id;
            //alert(data);
            $.ajax({
            type:"POST",
            cache:false,
            url:"list_article.php",
            data: {'action': 'follow', 'id': id, 'arti_id': arti_id},    // multiple data sent using ajax
            success: function (data) {
                    //alert('success');
                    //alert(data);
                    //$('#add').val('data sent sent');
                    //$('#msg').html(html);
                    //console.log(data);
                }
            });
                      
        });
 
        //Action on user's click(OFF)
        dsb.on('click', function(){
            $(enb).removeClass('selected'); //remove "selected" from other elements in this object class(ON)
            $(this).addClass('selected'); //add "selected" to the element which was just clicked in this object class(OFF) 
            $(input).val(0).change(); // //Finally change the value to 0
            //alert('0');
            var id = $('#active').val();
            var arti_id = $('#arti_id').val();
            //alert(arti_id);
            //var data = 'id='+ id;
            //alert(data);
            $.ajax({
            type:"POST",
            cache:false,
            url:"list_article.php",
            data: {'action': 'follow', 'id': id, 'arti_id': arti_id},    // multiple data sent using ajax
            success: function (data) {
                    //alert('success');
                    //alert(data);
                    //$('#add').val('data sent sent');
                    //$('#msg').html(html);
                    console.log(data);
                }
            });
        });
 
    });
 
});
</script>

  <div class="row">
    <div class="col-lg-12">
      <h1 class="page-header">List of Counselor Requests</h1>
    </div>
    <!-- /.col-lg-12 -->
  </div>
  <!-- /.row -->
<?php 
//echo 'hello';
    if (isset($_POST['action']))
    {
        
        if($_POST['action'] == 'follow')
        {
        //CODE TO UPDATE THE DATABASE.
            
           echo $value = $_POST['id'];
           echo '<h1>'.$arti_id = $_POST['arti_id'].'</h1>';
           
           $update = $art->article_active_inactive($arti_id, $value);
            if($update)
            {
                //Display Successful Message
            }
            else
            {
                //Display Unsuccessful Message
            }
        }
        else 
        {
            //echo "failure";
        }
 }
?>
  <!-- List of Edu Committee -->
  <div class="row">
    <div class="col-md-12">
      <table class="table table-responsive table-condensed">
        <thead>
          <tr>
            <th>Sr</th>
            <th>ITS</th>
            <th>Counselor Reply</th>
          </tr>
        </thead>
        <tbody>
          <?php
          if ($counselor_requests) {
             $i = 0;
            
            
             $total_article = count($counselor_requests);
            
            foreach ($counselor_requests as $coun_reqst){
              $i++;
              $tr_class = 'alert-warning';
              if($coun_reqst['coun_reply'] == '0'){
                $coun_reply = 'No';
              }else{
                $coun_reply = 'Yes';
              }
              ?>
          <tr class="<?php echo $tr_class ?>">
                <td><?php echo $i; ?></td>
                <td><?php echo $coun_reqst['its']; ?></td>
                <td><?php echo $coun_reply; ?></td>
              <?php
            }
              ?>
              <tr>
                <td colspan="5" class="bg-primary">
                  <strong>Total Records: </strong><?php echo $total_article; ?>
                </td>
              </tr>
              <?php
          } else {
              ?>
              <tr>
                <td colspan="4">No Records Found.</td>
              </tr>
              <?php
          }
          ?>
        </tbody>
      </table>
    </div><!-- ./ md-12 -->
  </div><!-- ./ row -->
  <!-- ./List of Edu Committee -->
<!-- /#page-wrapper -->
<?php include "./footer.php"; ?>
