<?php
require_once 'classes/class.user.admin.php';
include 'classes/class.araiz.php';
$allowed_roles = array(ROLE_HEAD_OFFICE,ROLE_DATA_ENTRY);
require_once 'session.php';

$user = new mtx_user_admin;
$araiz = new Araiz();

$title = 'Queries';
$description = '';
$keywords = '';
$active_page = "manage_queries";

if (isset($_POST['delete_id'])) {
  $delete_id = $_POST['delete_id'];
  $result = $araiz->delete_query($delete_id);

  if ($result) {
    $_SESSION[SUCCESS_MESSAGE] = "Query Successfully Delete";
  } else {
    $_SESSION[ERROR_MESSAGE] = "Error while Delete";
  }
}

if(isset($_POST['submit'])){
  $query_id = $_POST['query_id'];
  $ho_reply = $_POST['ho_reply'];
  $its_id = $_POST['its_id'];
  $user_id = $_SESSION[USER_ID];
  $date = date('Y-m-d');
  
  $insert = sent_queries_ho_reply($query_id, $user_id, $ho_reply, $its_id, $date);
  if($insert){
    $_SESSION[SUCCESS_MESSAGE] = 'Query HO Remarks Send Successfully..';
  }else {
    $_SESSION[ERROR_MESSAGE] = 'Error Encounter While Inserting Data';
  }
}

$pending_queries = get_all_queries();
$replied_queries = get_all_ho_sent_reply_queries();
$total_queries = count_total_queries();

$countries = $araiz->get_all_countries_list();
include ('header.php');
?>

<link rel="stylesheet" href="js/tablesorter/css/theme.bootstrap.css">
<script src="js/tablesorter/js/jquery.tablesorter.js"></script>
<script src="js/tablesorter/js/jquery.tablesorter.widgets.js"></script>
<style>
  .alfatemi-text{
    font-size: 18px;
  }
</style>
<div class="row">
  <div class="row">
    <div class="col-xs-12 col-sm-12">&nbsp;</div>
  </div>
  <div class="row">
    <div class="col-xs-12 col-sm-12">
      <h2 class="text-center">Total Pending Queries: <?php echo $total_queries; ?></h2>
    </div>
  </div>
  <div class="row">
    <div class="col-xs-12 col-sm-12">&nbsp;</div>
  </div>
  
  <div class="col-lg-12 col-xs-12 col-sm-12 pull-left">
    <div class="col-md-12">&nbsp;</div>
    <div class="clearfix"></div>
    <div class="row">
      <ul class="nav nav-tabs nav-justified" role="tablist">
        <li role="presentation" class="active"><a href="#pending_queries" aria-controls="pre" role="tab" data-toggle="tab">Pending Queries</a></li>
        <li role="presentation"><a href="#replied_queries" aria-controls="in" role="tab" data-toggle="tab">Replied Queries</a></li>
      </ul>
      <div class="row">
        <div class="col-xs-12 col-sm-12">&nbsp;</div>
      </div>
      <div class="tab-content">
        <div role="tabpanel" class="tab-pane active" id="pending_queries">
        <?php
          foreach ($pending_queries as $data) {
            $user_details = $user->get_user_data_by_its($data['its']);
            $ques[1] = 'Course not in the List';
            $ques[2] = 'Institute not in the List';
            $ques[3] = 'Country not in the List';
            $ques[4] = 'City not in the List';
        ?>
        <div class="panel panel-primary">
          <div class="panel-heading" style="font-size: 19px;">
            <div class="row">
              <div class="col-xs-12 col-sm-4"><strong>Sr No: </strong><?php echo $data['id']; ?></div>
              <div class="col-xs-12 col-sm-4"></div>
              <div class="col-xs-12 col-sm-4 text-right"><strong>Date: </strong><?php echo date('d, F Y',strtotime($data['timestamp'])); ?> | <a href="#" class="confirm" id="<?php echo $data['id']; ?>"><i class="fa fa-remove fa-fw text-danger"></i></a></div>
              <div class="clearfix"></div>
            </div>
          </div>
          <div class="row"></div>
          <!-- /.panel-heading -->
          <div class="panel-body">
            <div class="panel panel-default">
              <div class="panel-heading">
                <strong>Personal Details: </strong><?php echo $user_details[0]['first_name']. ' '. $user_details[0]['last_name']; ?>
              </div>
              <div class="panel-body">
                <div class="row">
                  <div class="col-xs-12 col-sm-10">
                    <div class="col-xs-12 col-sm-4 bottom10px">
                      <strong>ITS: </strong><?php echo $user_details[0]['its_id']; ?>
                    </div>
                    <div class="col-xs-12 col-sm-4 bottom10px">
                      <strong>Mobile: </strong><?php echo ucfirst($user_details[0]['mobile']); ?>
                    </div>
                    <div class="col-xs-12 col-sm-4 bottom10px">
                      <strong>Email: </strong><?php echo ucfirst($user_details[0]['email']); ?>
                    </div>
                    <div class="clearfix"></div>
                  </div>
                </div>
              </div>
            </div>
            <div class="panel panel-default">
              <div class="panel-heading">
                <strong>Query Details: </strong>
              </div>
              <div class="panel-body">
                <div class="row">
                  <div class="col-xs-12 col-sm-12">
                    <div class="col-xs-12 col-sm-4 bottom10px">
                      <strong>Query: </strong> <?php echo $ques[$data['ques_id']]; ?>
                    </div>
                    <div class="clearfix"></div>
                    <div class="col-xs-12 col-sm-12 bottom10px">
                      <strong>Query Text: </strong> <?php echo $data['ans_text']; ?>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="panel-footer">
            <?php
              if($data['ques_id'] == COURSE_NOT_IN_LIST){
            ?>
              <div class="row clscourse">
                <div class="col-xs-12 col-md-2">
                  <select name="marhala" class="form-control clsmarhala">
                    <option value="">Select Marhala</option>
                    <option value="6">6</option>
                    <option value="7">7</option>
                    <option value="8">8</option>
                  </select> 
                </div>
                <div class="col-xs-12 col-md-3">
                  <select name="degree" class="form-control clsdegree">
                    <option value="">Select Degree</option>
                  </select> 
                </div>
                <div class="col-xs-12 col-md-3">
                  <input type="text" name="course" class="form-control course" placeholder="Enter Course">
                </div>
                <div class="col-xs-12 col-md-2">
                  <input type="submit" name="send_course" class="btn btn-block btn-success send-course" value="Submit">
                </div>
                <div class="col-xs-12 col-md-2">
                  <input type="submit" name="send_remark" data-toggle="modal" data-target="#myModal<?php echo $data['id']; ?>" class="btn btn-block btn-primary send-remarks" value="Send Remarks">
                </div>
              </div>
            <?php
              }
              //if($data['ques_id'] == INSTITUTE_NOT_IN_LIST){
            ?>
              <!--div class="row clsinstitute">
                <div class="col-xs-12 col-md-3">
                  <select name="marhala" class="form-control clsmarhala">
                    <option value="">Select Marhala</option>
                    <option value="5">5</option>
                    <option value="6">6</option>
                    <option value="7">7</option>
                    <option value="8">8</option>
                  </select> 
                </div>
                <div class="col-xs-12 col-md-5">
                  <input type="text" name="institute" class="form-control institute" placeholder="Enter Institute Name">
                </div>
                <div class="col-xs-12 col-md-2">
                  <input type="submit" name="send_institute" class="btn btn-block btn-success send-institute" value="Submit">
                </div>
                <div class="col-xs-12 col-md-2">
                  <input type="submit" name="send_remark" data-toggle="modal" data-target="#myModal<?php echo $data['id']; ?>" class="btn btn-block btn-primary send-remarks" value="Send Remarks">
                </div>
              </div-->
            <?php
              //}
              if($data['ques_id'] == COUNTRY_NOT_IN_LIST){
            ?>
              <div class="row clscountry">
                <div class="col-xs-12 col-md-3">
                  <input type="text" name="country" class="form-control country" placeholder="Enter Country">
                </div>
                <div class="col-xs-12 col-md-3 clsiso">
                  <input type="text" name="ISO" maxlength="2" class="form-control iso" placeholder="Enter ISO Code">
                </div>
                <div class="col-xs-12 col-md-3">
                  <input type="submit" name="send_country" class="btn btn-block btn-success send-country" value="Submit">
                </div>
                <div class="col-xs-12 col-md-3">
                  <input type="submit" name="send_remark" data-toggle="modal" data-target="#myModal<?php echo $data['id']; ?>" class="btn btn-block btn-primary send-remarks" value="Send Remarks">
                </div>
              </div>
            <?php
              }
              if($data['ques_id'] == CITY_NOT_IN_LIST){
            ?>
              <div class="row clscity">
                <div class="col-xs-12 col-md-3">
                  <select name="country" class="form-control country">
                    <option value="">Select Country</option>
                      <?php foreach ($countries as $cntry) { ?>
                        <option value="<?php echo $cntry['name']; ?>"><?php echo $cntry['name']; ?></option>
                      <?php } ?>
                  </select>
                </div>
                <div class="col-xs-12 col-md-3">
                  <input type="text" name="city" class="form-control city" placeholder="Enter City">
                </div>
                <div class="col-xs-12 col-md-3">
                  <input type="submit" name="send_city" class="btn btn-block btn-success send-city" value="Submit">
                </div>
                <div class="col-xs-12 col-md-3">
                  <input type="submit" name="send_remark" data-toggle="modal" data-target="#myModal<?php echo $data['id']; ?>" class="btn btn-block btn-primary send-remarks" value="Send Remarks">
                </div>
              </div>
            <?php
              }
            ?>
          </div>
          <div class="modal fade" id="myModal<?php echo $data['id']; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
              <form name="raza_form" class="form" method="post" action="">
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Enter Reply:</h4>
                  </div>
                  <div class="modal-body">
                    <textarea name="ho_reply" class="form-control ho_reply" placeholder="Enter Reply"></textarea>
                    <input type="hidden" name="query_id" value="<?php echo $data['id']; ?>">
                    <input type="hidden" name="its_id" value="<?php echo $data['its']; ?>">
                  </div>
                  <div class="modal-footer">
                    <input type="submit" value="Send" name="submit" class="btn btn-primary ho_reply_submit">
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div>
        <?php
          }
        ?>
        </div>
        <div role="tabpanel" class="tab-pane" id="replied_queries">
          <?php
          foreach ($replied_queries as $data) {
            $user_details = $user->get_user_data_by_its($data['its']);
            $ques[1] = 'Course not in the List';
            $ques[2] = 'Institute not in the List';
            $ques[3] = 'Country not in the List';
            $ques[4] = 'City not in the List';
        ?>
        <div class="panel panel-primary">
          <div class="panel-heading" style="font-size: 19px;">
            <div class="row">
              <div class="col-xs-12 col-sm-4"><strong>Sr No: </strong><?php echo $data['id']; ?></div>
              <div class="col-xs-12 col-sm-4"></div>
              <div class="col-xs-12 col-sm-4 text-right"><strong>Date: </strong><?php echo date('d, F Y',strtotime($data['timestamp'])); ?></div>
              <div class="clearfix"></div>
            </div>
          </div>
          <div class="row"></div>
          <!-- /.panel-heading -->
          <div class="panel-body">
            <div class="panel panel-default">
              <div class="panel-heading">
                <strong>Personal Details: </strong><?php echo $user_details[0]['first_name']. ' '. $user_details[0]['last_name']; ?>
              </div>
              <div class="panel-body">
                <div class="row">
                  <div class="col-xs-12 col-sm-10">
                    <div class="col-xs-12 col-sm-4 bottom10px">
                      <strong>ITS: </strong><?php echo $user_details[0]['its_id']; ?>
                    </div>
                    <div class="col-xs-12 col-sm-4 bottom10px">
                      <strong>Mobile: </strong><?php echo ucfirst($user_details[0]['mobile']); ?>
                    </div>
                    <div class="col-xs-12 col-sm-4 bottom10px">
                      <strong>Email: </strong><?php echo ucfirst($user_details[0]['email']); ?>
                    </div>
                    <div class="clearfix"></div>
                  </div>
                </div>
              </div>
            </div>
            <div class="panel panel-default">
              <div class="panel-heading">
                <strong>Query Details: </strong>
              </div>
              <div class="panel-body">
                <div class="row">
                  <div class="col-xs-12 col-sm-12">
                    <div class="col-xs-12 col-sm-4 bottom10px">
                      <strong>Query: </strong> <?php echo $ques[$data['ques_id']]; ?>
                    </div>
                    <div class="clearfix"></div>
                    <div class="col-xs-12 col-sm-12 bottom10px">
                      <strong>Query Text: </strong> <?php echo $data['ans_text']; ?>
                    </div>
                    <div class="col-xs-12 col-sm-12 bottom10px">
                      <strong>Ho Replied Text: </strong> <?php echo $data['ho_remarks']; ?>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <?php
          }
        ?>
        </div>
    </div>
  </div>
</div>
<form method="post" id="frm-del-grp">
  <input type="hidden" name="delete_id" id="del-query-val" value="">
</form>
<style>
  .iso {
    text-transform: uppercase;
  }
</style>
<script>
  $(".confirm").confirm({
    text: "Are you sure you want to delete?",
    title: "Confirmation required",
    confirm: function(button) {
      $('#del-query-val').val($(button).attr('id'));
      $('#frm-del-grp').submit();
      alert('Are you Sure You want to delete: ' + id);
    },
    cancel: function(button) {
      // nothing to do
    },
    confirmButton: "Yes",
    cancelButton: "No",
    post: true,
    confirmButtonClass: "btn-danger"
  });
  
  $('.clsmarhala').on("change", function () {
    var strmarhala = $(this).val();
    var degree = $(this).closest(".clscourse").find(".clsdegree");
    degree.empty();
   
    $.ajax({
        url: 'get_data_by_ajax.php',
        method: 'POST',
        data: 'cmd=get_degrees&marhala=' + strmarhala,
        success: function (data) {
          degree.append('<option value="">Select Degree</option>');
          var objdegrees = jQuery.parseJSON(data);
          $.each(objdegrees, function() {
            degree.append('<option value="' + this.degree + '">' + this.degree + '</option>');
          });
          ;
        }
    });    
  });
  
  $('.clscountry').on("change", function () {
    var strcountry = $(this).val();
    var city = $(this).closest(".clslocation").find(".clscity");
    city.empty();
   
    $.ajax({
        url: 'get_data_by_ajax.php',
        method: 'POST',
        data: 'cmd=get_cities&country=' + strcountry,
        success: function (data) {
          city.append('<option value="">Select City</option>');
          var objcities = jQuery.parseJSON(data);
          $.each(objcities, function() {
            city.append('<option value="' + this.city + '">' + this.city + '</option>');
          });
          ;
        }
    });    
  });
  
  $('.iso').on("change", function () {
    var iso = $(this).closest(".clscountry").find(".iso").val();
    var iso_path = $(this).closest(".clscountry").find(".clsiso");
    $.ajax({
      url: "ajax.php",
      type: "GET",
      data: 'iso=' + iso + '&cmd=check_iso_exist',
      success: function(data)
      {
        if (data == '1') {
          alert('ISO Code is already exists');
          iso_path.focus();
          iso_path.attr('class', 'col-xs-12 col-md-3 has-error');
          return false;
        } else {
          iso_path.attr('class', 'col-xs-12 col-md-3 has-success');
          return true;
        }
      }
    });
  });
   
  $('.send-course').on("click", function(){
    var marhala = $(this).closest(".clscourse").find(".clsmarhala").val();
    var degree = $(this).closest(".clscourse").find(".clsdegree").val();
    var course = $(this).closest(".clscourse").find(".course").val();
    
    myApp.showPleaseWait();
    $.ajax({
      type: "POST",
      url: "ajax.php",
      data: 'cmd=make_entry_of_queries_course&marhala=' + marhala +'&degree=' + degree +'&course=' + course,
      cache: false,
      success: function () {
        myApp.hidePleaseWait();
      }
    });
  }); 
  
  $('.send-institute').on("click", function(){
    var marhala = $(this).closest(".clsinstitute").find(".clsmarhala").val();
    var institute = $(this).closest(".clsinstitute").find(".institute").val();
    
    myApp.showPleaseWait();
    $.ajax({
      type: "POST",
      url: "ajax.php",
      data: 'cmd=make_entry_of_queries_institute&marhala=' + marhala +'&institute=' + institute,
      cache: false,
      success: function () {
        myApp.hidePleaseWait();
      }
    });
  }); 
  
  $('.send-country').on("click", function(){
    var country = $(this).closest(".clscountry").find(".country").val();
    var iso = $(this).closest(".clscountry").find(".iso").val();
    
    myApp.showPleaseWait();
    $.ajax({
      type: "POST",
      url: "ajax.php",
      data: 'cmd=make_entry_of_queries_country&country=' + country +'&iso=' + iso,
      cache: false,
      success: function () {
        myApp.hidePleaseWait();
      }
    });
  }); 
  
  $('.send-city').on("click", function(){
    var country = $(this).closest(".clscity").find(".country").val();
    var city = $(this).closest(".clscity").find(".city").val();
    
    myApp.showPleaseWait();
    $.ajax({
      type: "POST",
      url: "ajax.php",
      data: 'cmd=make_entry_of_queries_city&country=' + country +'&city=' + city,
      cache: false,
      success: function () {
        myApp.hidePleaseWait();
      }
    });
  }); 
  
  var myApp;
  myApp = myApp || (function() {
    var pleaseWaitDiv = $('<div class="modal fade" id="pleaseWaitDialog" data-backdrop="static" data-keyboard="false"><div class="modal-dialog"><div class="modal-content"><div class="modal-header"><h4 class="modal-title">Processing . . .</h4></div><div class="modal-body"><img src="images/loader.gif"></div></div></div></div>');
    return {
      showPleaseWait: function() {
        pleaseWaitDiv.modal();
      },
      hidePleaseWait: function() {
        pleaseWaitDiv.modal('hide');
      },
    };
  })();

</script>
<?php
include 'footer.php';
?>
