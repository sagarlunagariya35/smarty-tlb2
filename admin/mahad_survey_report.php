<?php
require_once 'classes/class.user.admin.php';
include 'classes/class.survey.php';

$user = new mtx_user_admin;
$mtx_survey = new Mtx_Survey();

$title = 'Surveys List';
$description = '';
$keywords = '';
$active_page = 'list_report_survey';

$select = $course  = FALSE;

$survey = '1';
$select = $mtx_survey->get_survey_data_list($survey);
$survey_title = $mtx_survey->get_survey_title($survey);

if (isset($_POST['csv_download'])) {
  csv_download_survey($select, $survey_title.'.csv');
}

function csv_download_survey($ary_data, $filename) {
  // output headers so that the file is downloaded rather than displayed
  header('Content-Type: text/csv; charset=utf-8');
  header('Content-Disposition: attachment; filename=' . $filename);
  header("Pragma: no-cache");
  header("Expires: 0");
  // create a file pointer connected to the output stream
  $output = fopen('php://output', 'w');

  // output the column headings
  fputcsv($output, array('Sr No.', 'ITS', 'Q:1', 'Q:2', 'Q:3', 'Q:4', 'Q:5', 'Q:6', 'Q:7', 'Q:8', 'Q:9', 'Q:10', 'Q:11', 'Q:12', 'Q:13', 'Q:14', 'Q:15', 'Q:16', 'Q:17', 'Q:18', 'Q:19', 'Q:20', 'Q:21', 'Q:22', 'Q:23', 'Q:24', 'Q:25', 'Q:26', 'Q:27', 'Q:28', 'Q:29', 'Q:30', 'Q:31', 'Q:32', 'Q:33', 'Q:34', 'Q:35', 'Q:36', 'Q:37', 'Q:38', 'Q:39', 'Q:40', 'Q:41', 'Q:42', 'Q:43', 'Q:44', 'Q:45'));
  
  // fetch the data
  // loop over the rows, outputting them
  
  $cr = 1;
  foreach ($ary_data as $key => $row) {
    
    $survey_val = array();
    $survey_val[] = $cr++;
    $survey_val[] = $key;
    
    if ($row) {
      for ($ct = 1; $ct <= 45; $ct++) {
        $val = (isset($row[$ct])) ? $row[$ct] : '';
        $survey_val[] = $val;
        //fputcsv($output, $val);
      }
      fputcsv($output, $survey_val);
    }
  }
  

  fclose($output);
  exit();
}

?>

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="<?php echo $description; ?>">
    <meta name="keywords" content="<?php echo $keywords; ?>">
    <meta name="author" content="">

    <title><?php echo $survey_title; ?></title>

    
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/style.css?v=1" rel="stylesheet">
    <link href="css/fonts.css" rel="stylesheet">
    

    <!-- MetisMenu CSS -->
    <link href="css/plugins/metisMenu/metisMenu.min.css" rel="stylesheet">

    <!-- Timeline CSS -->
    <link href="css/plugins/timeline.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="css/sb-admin-2.css" rel="stylesheet">
    <link href="css/selectize.bootstrap3.css" rel="stylesheet">

    <!-- Morris Charts CSS -->
    <link href="css/plugins/morris.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">   

    <script src="js/jQuery-2.1.4.min.js"></script>
    <script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js" type="text/javascript"></script>
    <script src="js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="<?php echo SERVER_PATH; ?>templates/css/prettyPhoto.css" type="text/css" media="screen" title="prettyPhoto main stylesheet" />
    <script src="<?php echo SERVER_PATH; ?>templates/js/jquery.prettyPhoto.js" type="text/javascript"></script>
    
    <script src="js/jquery.confirm.min.js"></script>
    <script src="js/selectize.js"></script>
    <!--- Tinymce Plugin Javascript -->
    <script src="js/plugins/tinymce/js/tinymce/tinymce.min.js"></script>
  </head>

<body>
  <div id="wrapper">
    <div id="page-wrapper" style="margin:0px;">

  <link href="../assets/datatables/dataTables.bootstrap.css" rel="stylesheet" type="text/css" />
  <script src="../assets/datatables/jquery.dataTables.min.js" type="text/javascript"></script>
  <script src="../assets/datatables/dataTables.bootstrap.min.js" type="text/javascript"></script>
  <style>
    .dataTable tbody tr, .dataTable tbody td {
      height: 35px;
      overflow: hidden;
    }
  </style>
  <div class="content-wrapper">
    <form method="post" role="form" class="form-horizontal">
      <section class="content">
        <div class="row">
          <div class="col-lg-12">
            <h3 class="page-header"><?php echo $survey_title; ?></h3>
            <input type="submit" name="csv_download" value="CSV Download" class="btn btn-success pull-right">
          </div>
        </div>
        <div class="row hidden-print">
          <div class="col-xs-12">&nbsp;</div>
        </div>
          <!-- Content -->
          <div class="row">
            <div class="col-md-12">
              <div class="panel panel-primary">
                <div class="panel-heading">
                  <i class="fa fa-group fa-fw"></i> <?php echo $survey_title; ?>
                </div>
                <!-- /.panel-heading -->
                <div class="panel-body">
                  <div class="table-responsive">
                    <table class="table table-striped table-hover table-condensed tlb_stream table-bordered">
                      <thead>
                        <tr>
                          <th>Sr No.</th>
                          <th>ITS</th>
                          <?php
                          for ($q = 1; $q <= 45; $q++) {
                            echo '<th>Q:' . $q . '</th>';
                          }
                          ?>
                        </tr>
                      </thead>
                      <tbody>
                        <?php
                        $i = 1;
                        foreach ($select as $key => $srv_data) {
                          ?>
                          <tr>
                            <td><?php echo $i++; ?></td>
                            <td><?php echo $key; ?></td>
                            <?php
                            if ($srv_data) {
                              for ($c = 1; $c <= 45; $c++) {
                                $val = (isset($srv_data[$c])) ? $srv_data[$c] : '';
                                $cell_content = substr($val, 0, 50);
                                echo "<td title=\"$val\">$cell_content</td>";
                              }
                            }
                            ?>
                          </tr>
                          <?php
                        }
                        ?>
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
            <!-- /Center Bar -->
          </div>
          <!-- /Content -->
      </section>
    </form>
  </div>
  <script type="text/javascript">
    $(function () {
      $('.tlb_stream').DataTable({
        'iDisplayLength': 4,
        "paging": true,
        "lengthChange": false,
        "searching": true,
        "ordering": false,
        "info": true,
        "autoWidth": true,
        language: {
          searchPlaceholder: "Search"
        }
      });
    });
  </script>
  </div>
    <script type="text/javascript" src="js/araz_details.js?v=2"></script>
    <!-- /#wrapper -->

    <!-- Metis Menu Plugin JavaScript -->
    <script src="js/plugins/metisMenu/metisMenu.min.js"></script>
    
    <!-- Morris Charts JavaScript -->
    <script src="js/plugins/morris/raphael.min.js"></script>
    <script src="js/plugins/morris/morris.min.js"></script>
    <!--script src="js/plugins/morris/morris-data.js"></script-->

    <!-- Custom Theme JavaScript -->
    <script src="js/sb-admin-2.js"></script>
    
    <script language="javascript">
//      document.onmousedown=disableclick;
//      status="Right Click Disabled";
//      function disableclick(event)
//      {
//        if(event.button==2)
//         {
//           alert(status);
//           return false;    
//         }
//      }
    </script>
</div>
</body>

</html>
