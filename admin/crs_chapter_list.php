<?php
include 'classes/class.crs_course.php';
include 'classes/class.crs_chapter.php';
$allowed_roles = array(ROLE_HEAD_OFFICE);
require_once 'session.php';

$crs_chapter = new Mtx_Crs_Chapter();
$crs_course = new Mtx_Crs_Course();

$title = 'Chapters List';
$description = '';
$keywords = '';
$active_page = "list_chapters";

if(isset($_GET['cmd']) && $_GET['cmd'] == 'change_status') {
  
  $change_status = $crs_chapter->change_chapter_status($_GET['status'], $_GET['id']);
  
  if ($change_status) {
    $_SESSION[SUCCESS_MESSAGE] = 'Chapter Status changed successfully.';
    header('Location: crs_chapter_list.php');
    exit();
  } else {
    $_SESSION[ERROR_MESSAGE] = 'Error encountered while Changing Chapter Status';
  }
}

$list_chapters = $crs_chapter->get_all_chapters();
$list_courses = $crs_course->get_all_courses();

include_once("header.php");
?>
<link rel="stylesheet" href="js/select2/select2.min.css">
<script src="js/select2/select2.full.min.js"></script>
<script type="text/javascript" src="ckeditor/ckeditor.js"></script>

<div class="row">
  <div class="col-lg-12">&nbsp;</div>
</div>

<div class="panel panel-primary">
  <div class="panel-heading">
    <i class="fa fa-group fa-fw"></i> <?php echo $title; ?>
    <a href="crs_chapter_add.php" style="color: #FFF;" class="pull-right">Add New Chapter</a>
  </div>
  <!-- /.panel-heading -->
  <div class="panel-body">
    <div class="table-responsive">
      <table class="table table-striped table-hover table-condensed">
        <thead>
          <tr>
            <th>Sr</th>
            <th>Title</th>
            <th>Description</th>
            <th>Date</th>
            <th>Status</th>
            <th>Action</th>
          </tr>
        </thead>
        <tbody>
          <?php
          if($list_chapters){
            $sr = 0;
            foreach ($list_chapters as $data) {
              $sr++;
          ?>
              <tr class="<?php if($data['is_active'] == '1') { echo 'success'; } else { echo 'danger'; } ?>">
                <td><?php echo $sr; ?></td>
                <td><?php echo $data['title']; ?></td>
                <td><?php echo $data['description']; ?></td>
                <td><?php echo date('d, F Y',  strtotime($data['created_ts'])); ?></td>
                <td><input type="checkbox" name="is_active[]" id="action<?php echo $data['id']; ?>" onclick="check(<?php echo $data['id']; ?>)" <?php echo ($data['is_active'] == 1) ? 'checked' : ''; ?>></td>
                <td><a href="crs_chapter_update.php?id=<?php echo $data['id']; ?>"><i class="fa fa-pencil-square-o"></i></a></td>
              </tr>
              <?php
            }
          }else {
            echo '<tr><td class="text-center" colspan="6">No Records..</td></tr>';
          }
          ?>
        </tbody>
      </table>
    </div>
    <!-- /.table-responsive -->
  </div>
  <!-- /.panel-body -->
</div>

<script type="text/javascript">
  function check(val){
    var doc = document.getElementById('action'+val).checked;

    if(doc == true) doc = 1;
    else doc = 0;
    window.location.href='crs_chapter_list.php?id='+val+'&status='+doc+'&cmd=change_status';
  }
</script>
<script type="text/javascript" src="js/course_details.js?v=1"></script>

<!-- /#page-wrapper -->
<?php include "./footer.php"; ?>
