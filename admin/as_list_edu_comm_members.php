
<div class="row">

  <div class="col-lg-12">
    <h1 class="page-header">List of Education Committee Members in <?php //echo $tanzeem_id;  ?></h1>
  </div>
  <!-- /.col-lg-12 -->
</div>

<form method="post">

  <div class="row">

    <div class="col-md-12">
      <table class="table table-responsive table-condensed">
        <thead>
          <tr>
            <th>Sr</th>
            <th>Name</th>
            <th>Designation</th>
            <th>Mobile</th>
          </tr>
        </thead>
        <tbody>
          <?php
          $edu_members = array();
          $edu_comm = new Edu_committee($_SESSION[TANZEEM_ID]);
          $edu_comm_members = $edu_comm->members_get_list();
          
          if ($edu_comm_members) {
            $i = 0;
            $approved = 0;
            $rejected = 0;
            $downloaded = 0;
            foreach ($edu_comm_members as $mem) {
            $i++;
            $co_ord = ($mem['is_co_ordinator'] == 1) ? ', Co-ordinator' : '';
            $tr_class = 'bg-warning';

            if ($mem['ho_approved']) {
            $approved++;
            $status = 'Approved';
            $tr_class = 'bg-success';
            }
            if ($mem['ho_rejected']) {
            $rejected++;
            $status = 'Rejected';
            $tr_class = 'bg-danger';
            }
            if ($mem['downloaded']) {
            $downloaded++;
            $status = 'Rejected';
            $tr_class = 'bg-primary';
            }
            ?>
            <tr class="<?php echo $tr_class; ?>">
            <td><?php echo $i; ?></td>
            <td><?php echo $mem['name']; ?></td>
            <td><?php echo $mem['designation'] . $co_ord; ?></td>
            <td><?php echo $mem['mobile']; ?></td>
            <?php
            if ($mem['ho_approved'] || $mem['ho_rejected']) {
            echo "<td class='text-center' colspan='2'>$status</td>";
            } 
            ?>
            </tr>
            <?php
            }
            } else {
            ?>
            <tr>
            <td colspan="4">No Records Found.</td>
            </tr>
            <?php
            }
          ?>
        </tbody>
      </table>
    </div><!-- ./ md-12 -->

  </div><!-- ./ row -->

  <!--<div class="row">
    <div class="col-md-3 col-md-offset-9">
      <input type="hidden" name="cmd" value="update">
      <input type="submit" name="submit" value="Update" class="btn btn-success btn-block">
    </div>
  </div>--->
  <input type="hidden" name="tanzeem_id" value="<?php echo $tanzeem_id; ?>">
</form>
