<?php
include 'classes/class.crs_course.php';
include 'classes/class.crs_topic.php';
$allowed_roles = array(ROLE_HEAD_OFFICE);
require_once 'session.php';
require_once 'classes/class.user.admin.php';

$crs_course = new Mtx_Crs_Course();
$crs_topic = new Mtx_Crs_Topic();
$admin_user = new mtx_user_admin();

$title = 'Courses List';
$description = '';
$keywords = '';
$active_page = "list_crs_courses";

$result = $id = FALSE;

if (isset($_REQUEST['id'])) {
  $id = $_REQUEST['id'];
  $result = $crs_course->get_course($id);
}

if (isset($_POST['btn_submit'])) {
  $data = $db->clean_data($_POST);
  // get variables
  $title = $data['title'];
  $slug = $db->clean_slug($_POST['slug']);
  $topic_id = $data['topic_id'];
  $sort = $data['sort'];
  $start_date = $data['start_date'];
  $end_date = $data['end_date'];
  $registration_end_date = $data['registration_end_date'];
  $is_modarated = $data['is_modarated'];
  $teacher = $data['teacher'];
  $capacity = $data['capacity'];
  $req_enroll = $data['req_enroll'];
  $is_paid = $data['is_paid'];
  $fee = $data['fee'];
  $assign_to_group = $data['assign_to_group'];
  $promote = $data['promote'];
  $promote_end_date = $data['promote_end_date'];
  $course_lng = $data['course_lng'];
  $active = $data['active'];
  $description = $data['description'];
  $user_id = $_SESSION[USER_ID];
  
  $last_insert_id = $crs_course->get_last_insert_course();
  $last_insert_id = $last_insert_id + 1;
  
  if($_FILES['img_icon']['name'] != ''){
    $icon_image = $_FILES['img_icon']['name'];
    $icon_image_temp = $_FILES['img_icon']['tmp_name'];
    
    $filearray = explode('.', $icon_image);
    $ext = $filearray[count($filearray)-1];
              
    $icon_image_name = $last_insert_id.'.'.$ext;
    $imagepathANDname = "../upload/courses/courses/" . $icon_image_name;
    move_uploaded_file($icon_image_temp, $imagepathANDname);
  }else {
    $icon_image_name = $data['last_img_icon'];
  }

  $update = $crs_course->update_course($topic_id, $title, $slug, $description, $icon_image_name, $sort, $start_date, $end_date, $registration_end_date, $is_modarated, $teacher, $capacity, $req_enroll, $is_paid, $fee, $promote, $promote_end_date, $course_lng, $assign_to_group, $user_id, $active, $id);

  if ($update) {
    $_SESSION[SUCCESS_MESSAGE] = 'Course Updated Successfully.';
    header('Location: crs_course_update.php?id='.$id);
    exit();
  } else {
    $_SESSION[ERROR_MESSAGE] = 'Error encountered while Updating Course';
  }
}

$list_topics = $crs_topic->get_all_topics();
$teachers = $admin_user->get_all_teachers();
$iso_language_codes = get_iso_language_codes();

include_once("header.php");
?>
<link rel="stylesheet" href="js/select2/select2.min.css">
<script src="js/select2/select2.full.min.js"></script>
<script type="text/javascript" src="ckeditor/ckeditor.js"></script>

<div class="row">
  <div class="col-lg-12">
    <h3 class="page-header">Update Course</h3>
  </div>
  <!-- /.col-lg-12 -->
</div>

<div class="row">
  <div class="col-md-12">
    <form method="post" enctype="multipart/form-data">

      <div class="form-group col-md-4 col-xs-12">
        <label for="title">Title :</label>
        <input type="text" name="title" id="title" required class="form-control" value="<?php echo $result['title']; ?>">
      </div>
      
      <div class="form-group col-md-4 col-xs-12">
        <label for="slug">Slug :</label>
        <input type="text" name="slug" id="slug" required class="form-control" value="<?php echo $result['slug']; ?>">
      </div>
      
      <div class="form-group col-md-4 col-xs-12">
        <label for="title">Topic :</label>
        <select name="topic_id" id="topic_id" class="form-control topic_id" required >
          <option value="">Select</option>
          <?php
          if ($list_topics) {
            foreach ($list_topics as $lt) {
              $selected = ($lt['id'] == $result['topic_id']) ? 'selected' : '';
          ?>
            <option value="<?php echo $lt['id']; ?>" <?php echo $selected; ?>><?php echo $lt['title']; ?></option>
          <?php
            }
          }
          ?>
        </select>
      </div>
      <div class="clearfix"></div>

      <div class="form-group col-md-4 col-xs-12">
        <label for="slug">Image icon :</label>
        <input type="file" name="img_icon" class="file">
        <?php
          if ($result['img_icon'] != '') {
        ?>
          <div class="clearfix">&nbsp;</div>
          <p><img src="../upload/courses/courses/<?php echo $result['img_icon']; ?>" width="100" height="100" /></p>
          <input type="hidden" name="last_img_icon" value="<?php echo $result['img_icon']; ?>">
        <?php
          }
        ?>
        <div class="clearfix">&nbsp;</div>
      </div>
      
      <div class="form-group col-md-4 col-xs-12">
        <label for="sort">Sort :</label>
        <input type="text" name="sort" id="sort" required class="form-control" value="<?php echo $result['sort_id']; ?>">
      </div>
      <div class="clearfix"></div>
      
      <div class="form-group col-md-4 col-xs-12">
        <label for="slug">Start Date :</label>
        <input type="date" name="start_date" class="form-control" value="<?php echo date('Y-m-d', strtotime($result['start_date'])); ?>">
      </div>
      
      <div class="form-group col-md-4 col-xs-12">
        <label for="slug">End Date :</label>
        <input type="date" name="end_date" class="form-control" value="<?php echo date('Y-m-d', strtotime($result['end_date'])); ?>">
      </div>
      
      <div class="form-group col-md-4 col-xs-12">
        <label for="slug">Registration End Date :</label>
        <input type="date" name="registration_end_date" class="form-control" value="<?php echo date('Y-m-d', strtotime($result['registration_end_date'])); ?>">
      </div>
      <div class="clearfix"></div>
      
      <div class="form-group col-md-3 col-xs-12">
        <label for="slug">Is Modarated? :</label>
        <div class="checkbox">
          <label>
            <input type="checkbox" name="is_modarated" id="is_modarated" value="1" <?php echo ($result['is_modarated'] == '1') ? 'checked' : ''; ?>>
          </label>
        </div>
      </div>

      <div class="form-group col-md-3 col-xs-12">
        <label for="teacher">Teacher :</label>
        <select name="teacher" id="teacher" class="form-control">
          <option value="">Select</option>
          <?php
            if ($teachers) {
              foreach ($teachers as $val) {
                $selected_tcr = ($val['user_id'] == $result['teacher_id']) ? 'selected' : '';
          ?>
                <option value="<?php echo $data['user_id']; ?>" <?php echo $selected_tcr; ?>><?php echo $data['full_name']; ?></option>
          <?php
              }
            }
          ?>
        </select>
      </div>
      
      <div class="form-group col-md-3 col-xs-12">
        <label for="capacity">Capacity :</label>
        <input type="text" name="capacity" id="capacity" class="form-control" value="<?php echo $result['capacity']; ?>">
      </div>
      
      <div class="form-group col-md-3 col-xs-12">
        <label for="capacity">Fee :</label>
        <input type="text" name="fee" id="fee" class="form-control" value="<?php echo $result['fee']; ?>">
      </div>
      <div class="clearfix"></div>
      
      <div class="form-group col-md-3 col-xs-12">
        <label for="slug">Require Enroll? :</label>
        <div class="checkbox">
          <label>
            <input type="checkbox" name="req_enroll" id="req_enroll" value="1" <?php echo ($result['req_enroll'] == '1') ? 'checked' : ''; ?>>
          </label>
        </div>
      </div>      
      
      <div class="form-group col-md-3 col-xs-12">
        <label for="slug">Is Paid? :</label>
        <div class="checkbox">
          <label>
            <input type="checkbox" name="is_paid" id="is_paid" value="1" <?php echo ($result['is_paid'] == '1') ? 'checked' : ''; ?>>
          </label>
        </div>
      </div>
      
      <div class="form-group col-md-3 col-xs-12">
        <label for="slug">Assign to Group :</label>
        <div class="checkbox">
          <label>
            <input type="checkbox" name="assign_to_group" id="assign_to_group" value="1" <?php echo ($result['assign_to_group'] == '1') ? 'checked' : ''; ?>>
          </label>
        </div>
      </div>
      
      <div class="form-group col-md-3 col-xs-12">
        <label for="slug">Promote :</label>
        <div class="checkbox">
          <label>
            <input type="checkbox" name="promote" id="promote" value="1" <?php echo ($result['promote'] == '1') ? 'checked' : ''; ?>>
          </label>
        </div>
      </div>
      <div class="clearfix"></div>
      
      <div class="form-group col-md-4 col-xs-12">
        <label for="slug">Promote End Date :</label>
        <input type="date" name="promote_end_date" class="form-control" value="<?php echo date('Y-m-d', strtotime($result['promote_end_date'])); ?>">
      </div>
      
      <div class="form-group col-md-4 col-xs-12">
        <label for="teacher">Course Language :</label>
        <select name="course_lng" id="course_lng" class="form-control">
          <option value="">Select</option>
          <?php
            if ($iso_language_codes) {
              foreach ($iso_language_codes as $ilc) {
                $selected_crs = ($ilc['iso3'] == $result['course_lng']) ? 'selected' : '';
          ?>
                <option value="<?php echo $ilc['iso3']; ?>" <?php echo $selected_crs; ?>><?php echo $ilc['language']; ?></option>
          <?php
              }
            }
          ?>
        </select>
      </div>
      
      <div class="form-group col-md-4 col-xs-12">
        <label for="active">Is Active? :</label>
        <select name="active" id="active" class="form-control" required >
          <option value="">Select</option>
          <option value="1" <?php echo ($result['is_active'] == 1) ? 'selected' : ''; ?>>Yes</option>
          <option value="0" <?php echo ($result['is_active'] == 0) ? 'selected' : ''; ?>>No</option>
        </select>
      </div>
      <div class="clearfix"></div>
      
      <div class="form-group col-md-12 col-xs-12">
        <label for="miqat">Description :</label>
        <textarea name="description" class="form-control"><?php echo $result['description']; ?></textarea>
      </div>
      
      <div class="clearfix"></div>
      <div class="form-group col-md-2 col-xs-12">
        <input type="submit" name="btn_submit" id="btn_submit" value="Submit" class="btn btn-primary btn-block form-control">
      </div>
    </form>

  </div>
</div>
<script type="text/javascript" src="js/course_details.js"></script>
<!-- /#page-wrapper -->
<?php include "./footer.php"; ?>
