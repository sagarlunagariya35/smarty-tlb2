<?php
$title = 'School Wise';
$description = '';
$keywords = '';
$active_page = "School_Wise";

//include 'session.php';
include 'classLoader.php';
$global = new Glob_func();

//get
$getCool = FALSE;
if (isset($_GET['school']) && $_GET['school']) {
  $getCool = $db->clean_data(urldecode($_GET['school']));
  $records = $global->select_data('araiz', $getCool, 'school');
}
//read data
$schools = $global->getDistinct('school', 'araiz');
$title = 'School Wise Report';
include_once("header.php");
?>
<!-- Page Content -->

  <div class="row">
      <div class="col-lg-12">
                    <h3 class="page-header">School Wise Records</h3>
                </div>
    <div class="col-lg-12">&nbsp;</div>
    <div class="col-lg-12 col-xs-12"><a href="print_schoolwise.php?school=<?php echo $getCool; ?>" class="btn btn-success btn-xs pull-right <?php echo (!$getCool) ? 'disabled' : ''; ?>" target="_blank">Print</a></div>
    <div class="col-lg-12">&nbsp;</div>
    <form class="form-inline text-center" role="form">
      <div class="form-group">
        <label class="control-label">School</label>
        <select class="form-control" name="school">
          <option>--Select School--</option>
          <?php foreach ($schools as $s) { ?>
            <option value="<?php echo urlencode($s['school']); ?>" <?php $getCool == $s['school'] ? 'selected' : ''; ?>><?php echo $s['school']; ?></option>
          <?php } ?>
        </select>
      </div>
      <div class="form-group">
        
        <button type="submit" class="btn btn-success">Search</button>
      </div>
    </form>
    <div class="col-lg-12">&nbsp;</div>
    
    <?php if($getCool) require './inc/school_wise.php'; ?>

  </div>
  <!-- /.col-lg-12 -->
</div>
<!-- /.row -->
<!-- Page Content -->

<?php include ('footer.php');?>
