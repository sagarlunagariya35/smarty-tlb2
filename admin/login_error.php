<?php
session_start();

require_once '../classes/constants.php';
?>
<!DOCTYPE html>
<html lang="en">

  <head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Talabul-ilm</title>
    <style>

    </style>
    <!-- Bootstrap Core CSS -->
    <link href="<?php echo SERVER_PATH; ?>admin/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="<?php echo SERVER_PATH; ?>admin/css/sb-admin-2.css" rel="stylesheet">

    <link href="<?php echo SERVER_PATH; ?>admin/css/custom.css" rel="stylesheet">
    <!-- Custom Fonts -->
    <link href="<?php echo SERVER_PATH; ?>admin/font-awesome-4.1.0/css/font-awesome.min.css" rel="stylesheet" type="text/css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    <style>
      .error_msg {
        padding: 5px;
      }
    </style>
  </head>

  <body>
    <div class="container">

      <div class="col-md-4 col-sm-6 col-xs-10 col-md-offset-4 col-sm-offset-3 col-xs-offset-1 login_form">

        <div class="text-center">
          <image class="login_main" src="<?php echo SERVER_PATH; ?>assets/img/pen_logo.png" />
          <div class="text-center col-md-12">
            <h1 class="font-white">Talab ul ilm</h1>
          </div>
        </div>

        <?php
        if (isset($_SESSION['error_msg']) && $_SESSION['error_msg'] != '') {
          ?>
          <div class="col-md-12 alert alert-danger">
            <button type="button" class="close" data-dismiss="alert">&times;</button>
            <?php
            echo $_SESSION['error_msg'];
            unset($_SESSION['error_msg']);
            ?>
          </div>
          <?php
        }
        ?>

      </div>

    </div>

    <!-- jQuery Version 1.11.0 -->
    <script src="<?php echo SERVER_PATH; ?>admin/js/jquery-1.11.0.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="<?php echo SERVER_PATH; ?>admin/js/bootstrap.min.js"></script>

</html>
