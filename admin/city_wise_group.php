<?php
$title = 'City Group';
$description = '';
$keywords = '';
$active_page = "City_group";

//include 'session.php';
include './classLoader.php';
$global = new Glob_func();

$records = $global->getGroupByData(array('city', 'school', 'group_id') ,'araiz');

$title = 'City Wise Report';
include 'header.php';
?>
<!-- Page Content -->
  <div class="row">
    <div class="col-lg-12">&nbsp;</div>
    <div class="col-lg-12 col-xs-12"><a href="print_citywise.php" class="btn btn-success btn-xs pull-right" target="_blank">Print</a></div>
    <div class="col-lg-12">&nbsp;</div>
    <?php require './inc/city_wise.php';?>

  </div>
  <!-- /.col-lg-12 -->
</div>
<!-- /.row -->

<!-- Page Content -->
<?php
include 'footer.php';
?>