<?php
require_once '../classes/class.database.php';
include 'classes/class.article.php';
$allowed_roles = array(ROLE_HEAD_OFFICE);
require_once 'session.php';

$title = 'Miqaat Istibsaar';
$description = '';
$keywords = '';
$active_page = "add_miqaat_istibsaar";
$art = new Article();

(isset($_GET['id'])) ? $miqaat_istibsaar_id = $_GET['id'] : '';
(isset($_GET['keyword'])) ? $miqaat_keyword = $_GET['keyword'] : '';

if (isset($_POST['btn_submit'])) {
  
  $sr_no = (array) $_POST['sr_no'];
  $category = (array) $_POST['category'];
  $name = (array) $_POST['name'];
  $content_id = (array) $_POST['content_id'];
  
  foreach ($content_id as $key=>$val){
    $update = $art->update_miqaat_media_sr_no($sr_no[$key],$category[$key],$name[$key],$content_id[$key],$miqaat_keyword);
  }
  
  if ($update) {
    $_SESSION[SUCCESS_MESSAGE] = 'Miqaat Media Re-order Successfully..';
  } else {
    $_SESSION[ERROR_MESSAGE] = 'Error In Ordering Data';
  }
}

$contents = $art->get_miqaat_media_by_id('miqaat_'.$miqaat_keyword, $miqaat_istibsaar_id);

include_once("header.php");
?>
<script type="text/javascript" src="ckeditor/ckeditor.js"></script>

<div class="row">
  <div class="col-lg-12">
    <h1 class="page-header">Change Miqaat Istibsaar <?php echo ucfirst($miqaat_keyword); ?> Order</h1>
  </div>
  <!-- /.col-lg-12 -->
</div>

<div class="row">
  <div class="col-md-12">
    <form method="post" enctype="multipart/form-data">
      <?php
        if($contents){
          foreach ($contents as $data){
      ?>
      <div class="form-group">
        <div class="col-md-1 text-right">
          <input type="text" name="sr_no[]" class="form-control" value="<?php echo $data['sr_no']; ?>" placeholder="Sr No.">
        </div>
        <div class="col-md-4">
          <p class="form-control static"><?php echo $data[$miqaat_keyword.'_url']; ?></p>
        </div>
        <div class="col-md-4">
          <input type="text" name="category[]" class="form-control" value="<?php echo $data['category']; ?>" placeholder="Category">
        </div>
        <div class="col-md-3">
          <input type="text" name="name[]" class="form-control" value="<?php echo $data['name']; ?>" placeholder="Name">
        </div>
        <input type="hidden" name="content_id[]" value="<?php echo $data['id']; ?>">
      </div>
      <div class="clearfix"></div>
      <?php
          }
      ?>
      <div class="clearfix"></div>
      <div class="form-group col-md-2 col-xs-12">
      <input type="submit" name="btn_submit" id="btn_submit" value="Submit" class="btn btn-block btn-primary">
      </div>
      <?php
        }
      ?>
    </form>
  </div>
</div>
<!-- /#page-wrapper -->
<?php include "./footer.php"; ?>
