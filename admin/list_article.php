<?php
require_once '../classes/constants.php';
require_once '../classes/class.database.php';
require_once 'classes/class.user.admin.php';
require_once 'classes/class.article.php';

$allowed_roles = array(ROLE_HEAD_OFFICE);
require_once 'session.php';

// Get list of articles
$art = new Article();
$list_articles = $art->get_all_article();

$title = 'Articles';
$description = '';
$keywords = '';
$active_page = "list_articles";

if (isset($_POST['delete_id'])) {
  $delete_id = $_POST['delete_id'];
  $result = $art->delete_article($delete_id);

  if ($result) {
    $_SESSION[SUCCESS_MESSAGE] = "Article Successfully Delete";
    header('Location: list_article.php');
    exit();
  } else {
    $_SESSION[ERROR_MESSAGE] = "Error In Delete";
  }
}

include_once("header.php");

?>
  <div class="row">
    <div class="col-lg-12 col-xs-12 col-sm-12  pull-left">
      <div class="col-md-12">&nbsp;</div>
      <div class="clearfix"></div>
      <div class="panel panel-green">
        <div class="panel-heading">
          <i class="fa fa-group fa-fw"></i> Manage Articles
          <span class="pull-right"><a href="articles.php" style="color: white;">Add New Article</a></span>
        </div>
        <!-- /.panel-heading -->
        <div class="panel-body">
          <div class="table-responsive">
            <table class="table table-striped table-hover table-condensed">
              <thead>
                <tr>
                  <th>No</th>
                  <th>Article Title</th>
                  <th>Article Path</th>
                  <th>Action</th>
                </tr>
              </thead>
              <tbody>
                <?php
                $i = 1;
                if($list_articles){
                  foreach ($list_articles as $article) {
                    $menu = get_menu_by_id($article['menu']);
                    $submenu = get_menu_by_id($article['submenu']);
                ?>
                    <tr>
                      <td><?php echo $i++; ?></td>
                      <td><?php echo $article['article_title']; ?></td>
                      <td><?php echo $menu[0]['name'].'/'.$submenu[0]['name']; ?></td>
                      <td>
                        <a href="edit_article.php?id=<?php echo $article['id']; ?>"><i class="fa fa-edit fa-fw"></i></a>
                      <a href="#" class="confirm" id="<?php echo $article['id']; ?>"><i class="fa fa-remove fa-fw text-danger"></i></a>
                      </td>
                    </tr>
                    <?php
                  }
                }else {
                  echo '<tr><td class="text-center" colspan="4">No Records..</td></tr>';
                }
                ?>
              </tbody>
            </table>
          </div>
          <!-- /.table-responsive -->
        </div>
        <!-- /.panel-body -->
      </div>
    </div>
    <form method="post" id="frm-del-grp">
      <input type="hidden" name="delete_id" id="del-article-val" value="">
      <input type="hidden" name="cmd" value="del-article">
    </form>
  </div><!-- ./ row -->
  <script>
    $(".confirm").confirm({
      text: "Are you sure you want to delete?",
      title: "Confirmation required",
      confirm: function(button) {
        $('#del-article-val').val($(button).attr('id'));
        $('#frm-del-grp').submit();
        alert('Are you Sure You want to delete: ' + id);
      },
      cancel: function(button) {
        // nothing to do
      },
      confirmButton: "Yes",
      cancelButton: "No",
      post: true,
      confirmButtonClass: "btn-danger"
    });
  </script>
  
<?php include "./footer.php"; ?>
