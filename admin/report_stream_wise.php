<?php
require_once 'classes/class.user.admin.php';
include 'classes/class.araiz.php';
$allowed_roles = array(ROLE_HEAD_OFFICE);
require_once 'session.php';

$user = new mtx_user_admin;
$araiz = new Araiz();

$title = 'Total count of araiz jamiyat and mauze and Stream/Course wise';
$description = '';
$keywords = '';
$active_page = 'list_araz_stream_wise';

$select = $course = FALSE;

if(isset($_POST['search'])){
  $jamiat = $_POST['jamiat'];
  $jamaat = $_POST['jamaat'];
  $course = $_POST['course'];
  $select = $araiz->get_araz_count_course_wise($jamiat, $jamaat, $course);
}

$jamiats = $araiz->get_all_lp_jamiat();
$jamaats = $araiz->get_all_lp_jamaat();
$stream_array = $araiz->get_finalize_courses();

include ('header.php');
?>

<link href="../assets/datatables/dataTables.bootstrap.css" rel="stylesheet" type="text/css" />
<script src="../assets/datatables/jquery.dataTables.min.js" type="text/javascript"></script>
<script src="../assets/datatables/dataTables.bootstrap.min.js" type="text/javascript"></script>

<div class="content-wrapper">
  <form method="post" role="form" class="form-horizontal">
    <section class="content">
      <div class="row">
        <div class="col-lg-12">
          <h3 class="page-header"><?php echo $title; ?></h3>
        </div>
      </div>
      <div class="row hidden-print">
        <div class="col-xs-12">&nbsp;</div>
      </div>
      <div class="row hidden-print">
        <div class="col-xs-12 col-md-3">
          <select name="jamiat" class="form-control">
            <option value="">Select Jamiat</option>
            <?php
              if ($jamiats) {
                foreach ($jamiats as $data) {
                  $selected = ($jamiat == $data['jamiat']) ? 'selected' : '';
                  ?>
                  <option value="<?php echo $data['jamiat']; ?>" <?php echo $selected; ?>><?php echo $data['jamiat']; ?></option>
                  <?php
                }
              }
            ?>
          </select>
        </div>
        <div class="col-xs-12 col-md-3">
          <select name="jamaat" class="form-control">
            <option value="">Select Jamaat</option>
            <?php
              if ($jamaats) {
                foreach ($jamaats as $data) {
                  $selected = ($jamaat == $data['jamaat']) ? 'selected' : '';
                  ?>
                  <option value="<?php echo $data['jamaat']; ?>" <?php echo $selected; ?>><?php echo $data['jamaat']; ?></option>
                  <?php
                }
              }
            ?>
          </select>
        </div>
        <div class="col-xs-12 col-md-3">
          <select name="course" class="form-control">
            <option value="">Select Stream</option>
            <?php
              if ($stream_array) {
                foreach ($stream_array as $crs) {
                  $selected = ($course == $crs['course']) ? 'selected' : '';
                  ?>
                  <option value="<?php echo $crs['course']; ?>" <?php echo $selected; ?>><?php echo $crs['course']; ?></option>
                  <?php
                }
              }
            ?>
          </select>
        </div>
        <div class="col-xs-12 col-md-2">
          <input type="submit" name="search" value="Search" class="btn btn-success">
        </div>
      </div>
      <div class="row hidden-print">
        <div class="col-xs-12">&nbsp;</div>
      </div>
      <?php 
        if($select){
      ?>
      <!-- Content -->
      <div class="row">
        <div class="col-md-12">
          <div class="panel panel-primary">
            <div class="panel-heading">
              <i class="fa fa-group fa-fw"></i> <?php echo $title; ?>
            </div>
            <!-- /.panel-heading -->
            <div class="panel-body">
              <div class="table-responsive">
                <table class="table table-striped table-hover table-condensed tlb_stream">
                  <thead>
                    <tr>
                      <th>Sr No.</th>
                      <th>Jamiat Name</th>
                      <th>Jamaat Name</th>
                      <th>Stream/Course</th>
                      <th>Male Count</th>
                      <th>Female Count</th>
                      <th>Total</th>
                    </tr>
                  </thead>
                  <tbody>
                  <?php
                  //echo '<pre>';
                  //print_r($select);
                  //echo '</pre>';

                  $i = 1; 
                  foreach($select as $key=>$jamiat_data){
                    if ($jamiat_data) {
                      foreach ($jamiat_data as $key_jamaat=>$jamaat_data) {
                        if ($jamaat_data) {
                          foreach ($jamaat_data as $course_key=>$course_data) {
                              
                          $male_count = (isset($course_data['M'])) ? (int) $course_data['M'] : 0;
                          $female_count = (isset($course_data['F'])) ? (int) $course_data['F'] : 0;
                          $total_count = $male_count + $female_count;
                          ?>
                          <tr>
                            <td><?php echo $i++; ?></td>
                            <td><?php echo $key; ?></td>
                            <td><?php echo $key_jamaat; ?></td>
                            <td><?php echo $course_key; ?></td>
                            <td><?php echo $male_count; ?></td>
                            <td><?php echo $female_count; ?></td>
                            <td><?php echo $total_count; ?></td>
                          </tr>
                          <?php
                              }
                            }
                          }
                        }
                      } 
                    ?>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
        <!-- /Center Bar -->
      </div>
      <!-- /Content -->
      <?php
        }
      ?>
    </section>
  </form>
</div>
<script type="text/javascript">
  $(function () {
    $('.tlb_stream').DataTable({
      'iDisplayLength': 50,
      "paging": true,
      "lengthChange": false,
      "searching": true,
      "ordering": false,
      "info": true,
      "autoWidth": true,
      language: {
        searchPlaceholder: "Search"
      }
    });
  });
</script>
<?php
  include('footer.php');
?>