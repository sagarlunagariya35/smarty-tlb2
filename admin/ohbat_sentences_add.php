<?php
require_once '../classes/class.database.php';
include 'classes/class.ohbat_sentences.php';
$allowed_roles = array(ROLE_HEAD_OFFICE);
require_once 'session.php';

$title = 'Miqaat Daily Sentences';
$description = '';
$keywords = '';
$active_page = "list_ohbat_sentences";
$art = new Mtx_Ohbat_Sentences;

$ar_file_name = $en_file_name = FALSE;

if (isset($_POST['btn_submit'])) {
  $data = $db->clean_data($_POST);
  //Arabic
  $title_ar = $data['title'];
  $question = addslashes($_POST['question']);
  $bayan_title = $data['bayan_title'];
  $bayan = addslashes($_POST['bayan']);
  $words = addslashes($_POST['words']);
  
  //English
  $title_eng = $data['title_eng'];
  $slug = $data['slug'];
  $question_eng = addslashes($_POST['question_eng']);
  $en_bayan_title = $data['en_bayan_title'];
  $bayan_eng = addslashes($_POST['bayan_eng']);
  $words_eng = addslashes($_POST['words_eng']);
  
  //Other
  $miqat_id = $data['miqat_id'];
  $event_time = $data['event_time'];
  $sr = $data['sr'];
  $active = $data['active'];
  
  $last_insert_id = $art->get_last_insert_article();
  $last_insert_id = $last_insert_id + 1;
  
  // Arabic Images
  if($_FILES['ar_question_image']['name'] != ''){
    $ar_question_image = $_FILES['ar_question_image']['name'];
    $ar_question_image_temp = $_FILES['ar_question_image']['tmp_name'];
    $ar_ques_file_name = $last_insert_id.'-'.$ar_question_image;
    $arquespathANDname = "../upload/ohbat_upload/" . $ar_ques_file_name;
    move_uploaded_file($ar_question_image_temp, $arquespathANDname);
  }
  if($_FILES['ar_bayan_title_image']['name'] != ''){
    $ar_bayan_title_image = $_FILES['ar_bayan_title_image']['name'];
    $ar_bayan_title_image_temp = $_FILES['ar_bayan_title_image']['tmp_name'];
    $ar_bayan_title_file_name = $last_insert_id.'-'.$ar_bayan_title_image;
    $arbayantitlepathANDname = "../upload/ohbat_upload/" . $ar_bayan_title_file_name;
    move_uploaded_file($ar_bayan_title_image_temp, $arbayantitlepathANDname);
  }
  if($_FILES['ar_bayan_image']['name'] != ''){
    $ar_bayan_image = $_FILES['ar_bayan_image']['name'];
    $ar_bayan_image_temp = $_FILES['ar_bayan_image']['tmp_name'];
    $ar_file_name = $last_insert_id.'-'.$ar_bayan_image;
    $arbayanpathANDname = "../upload/ohbat_upload/" . $ar_file_name;
    move_uploaded_file($ar_bayan_image_temp, $arbayanpathANDname);
  }
  if($_FILES['ar_word_image']['name'] != ''){
    $ar_word_image = $_FILES['ar_word_image']['name'];
    $ar_word_image_temp = $_FILES['ar_word_image']['tmp_name'];
    $ar_word_file_name = $last_insert_id.'-'.$ar_word_image;
    $arwordpathANDname = "../upload/ohbat_upload/" . $ar_word_file_name;
    move_uploaded_file($ar_word_image_temp, $arwordpathANDname);
  }
  
  // English Images
  if($_FILES['en_question_image']['name'] != ''){
    $en_question_image = $_FILES['en_question_image']['name'];
    $en_question_image_temp = $_FILES['en_question_image']['tmp_name'];
    $en_ques_file_name = $last_insert_id.'-'.$en_question_image;
    $enquespathANDname = "../upload/ohbat_upload/" . $en_ques_file_name;
    move_uploaded_file($en_question_image_temp, $enquespathANDname);
  }
  if($_FILES['en_bayan_title_image']['name'] != ''){
    $en_bayan_title_image = $_FILES['en_bayan_title_image']['name'];
    $en_bayan_title_image_temp = $_FILES['en_bayan_title_image']['tmp_name'];
    $en_bayan_title_file_name = $last_insert_id.'-'.$en_bayan_title_image;
    $enbayantitlepathANDname = "../upload/ohbat_upload/" . $en_bayan_title_file_name;
    move_uploaded_file($en_bayan_title_image_temp, $enbayantitlepathANDname);
  }
  if($_FILES['en_bayan_image']['name'] != ''){
    $en_bayan_image = $_FILES['en_bayan_image']['name'];
    $en_bayan_image_temp = $_FILES['en_bayan_image']['tmp_name'];
    $en_file_name = $last_insert_id.'-'.$en_bayan_image;
    $enbayanpathANDname = "../upload/ohbat_upload/" . $en_file_name;
    move_uploaded_file($en_bayan_image_temp, $enbayanpathANDname);
  }
  if($_FILES['en_word_image']['name'] != ''){
    $en_word_image = $_FILES['en_word_image']['name'];
    $en_word_image_temp = $_FILES['en_word_image']['tmp_name'];
    $en_word_file_name = $last_insert_id.'-'.$en_word_image;
    $enwordpathANDname = "../upload/ohbat_upload/" . $en_word_file_name;
    move_uploaded_file($en_word_image_temp, $enwordpathANDname);
  }
  
  $art_insert = $art->insert_article($miqat_id, $event_time, $sr, $slug, $title_ar, $title_eng, $bayan_title, $ar_bayan_title_file_name, $bayan, $ar_file_name, $question, $ar_ques_file_name, $active, $words, $ar_word_file_name, $question_eng, $en_ques_file_name, $en_bayan_title, $en_bayan_title_file_name, $bayan_eng, $en_file_name, $words_eng, $en_word_file_name);

  if ($art_insert) {
    $_SESSION[SUCCESS_MESSAGE] = 'Sentence Inserted Successfully.';
  } else {
    $_SESSION[ERROR_MESSAGE] = 'Error encountered while Inserting Data';
  }
}

include_once("header.php");
?>
<script type="text/javascript" src="ckeditor/ckeditor.js"></script>

<div class="row">
  <div class="col-lg-12">
    <h1 class="page-header"><?php echo $_SESSION[USER_ID]; ?>Add Ohbat Sentence <?php //echo $_SESSION['user_its'];       ?></h1>
  </div>
  <!-- /.col-lg-12 -->
</div>

<div class="row">
  <div class="col-md-12">
    <form method="post" enctype="multipart/form-data">

      <div class="form-group col-md-4 col-xs-12">
        <label for="title">Sentence Arabic Title :</label>
        <input type="text" name="title" id="title" required="required" class="form-control" >
      </div>
      
      <div class="form-group col-md-4 col-xs-12">
        <label for="title">Sentence English Title :</label>
        <input type="text" name="title_eng" id="title" required="required" class="form-control" >
      </div>

      <div class="form-group col-md-4 col-xs-12">
        <label for="slug">Sentence slug :</label>
        <input type="text" name="slug" id="slug" required="required" class="form-control" >
      </div>

      <div class="form-group col-md-4 col-xs-12">
        <label for="slug">Event Time :</label>
        <select name="event_time" id="event_time" class="form-control" required >
          <option value="">Select one</option>
          <option value="pre">Pre</option>
          <option value="in">In</option>
          <option value="post">Post</option>
        </select>
      </div>

      <div class="form-group col-md-4 col-xs-12">
        <label for="year">Select Year :</label>
        <select name="year" id="year" class="form-control" required >
          <option value="">Select Year</option>
          <option value="1435">1435</option>
          <option value="1436">1436</option>
          <option value="1437">1437</option>
        </select>
      </div>

      <div class="form-group col-md-4 col-xs-12">
        <label for="miqat">Select Miqat :</label>
        <select name="miqat_id" id="miqat" class="form-control" required >
          <option value="">Select Miqaat</option>
        </select>
      </div>

      <div class="form-group col-md-3 col-xs-12">
        <label for="sr">Serial :</label>
        <input type="text" name="sr" id="sr" class="form-control" >
      </div>

      <div class="form-group col-md-3 col-xs-12">
        <label for="active">Is Active? :</label>
        <select name="active" id="active" class="form-control" required >
          <option value="1" selected >Yes</option>
          <option value="0">No</option>
        </select>
      </div>
      <div class="clearfix"></div>
      
      <div class="form-group col-md-6 col-xs-12">
        <label for="miqaat_logo">Arabic Question Image :</label>
        <input type="file" name="ar_question_image" class="file">
      </div>

      <div class="form-group col-md-12" id="bayan">
        <label>Arabic Question : </label>
        <textarea name="question" class="ckeditor" class="form-control"></textarea>
      </div><!----CONTENT_BOX-->
      
      <div class="form-group col-md-4 col-xs-12">
        <label for="miqaat_logo">Arabic Bayan Title Image :</label>
        <input type="file" name="ar_bayan_title_image" class="file">
      </div>
      
      <div class="form-group col-md-4 col-xs-12">
        <label for="title">Arabic Bayan Title :</label>
        <input type="text" name="bayan_title" id="bayan_title" required="required" class="form-control" >
      </div>
      
      <div class="form-group col-md-6 col-xs-12">
        <label for="miqaat_logo">Arabic Bayan Image :</label>
        <input type="file" name="ar_bayan_image" class="file">
      </div>

      <div class="form-group col-md-12" id="bayan">
        <label>Arabic Bayan : </label>
        <textarea name="bayan" class="ckeditor" class="form-control"></textarea>
      </div><!----CONTENT_BOX-->
      
      <div class="form-group col-md-6 col-xs-12">
        <label for="miqaat_logo">Arabic Words Image :</label>
        <input type="file" name="ar_word_image" class="file">
      </div>

      <div class="form-group col-md-12" id="bayan">
        <label>Arabic Words : </label>
        <textarea name="words" class="ckeditor" class="form-control"></textarea>
      </div><!----CONTENT_BOX-->
      
      <div class="form-group col-md-6 col-xs-12">
        <label for="miqaat_logo">English Question Image :</label>
        <input type="file" name="en_question_image" class="file">
      </div>
      
      <div class="form-group col-md-12" id="bayan">
        <label>English Question : </label>
        <textarea name="question_eng" class="ckeditor" class="form-control"></textarea>
      </div><!----CONTENT_BOX-->
      
      <div class="form-group col-md-4 col-xs-12">
        <label for="miqaat_logo">English Bayan Title Image :</label>
        <input type="file" name="en_bayan_title_image" class="file">
      </div>
      
      <div class="form-group col-md-4 col-xs-12">
        <label for="title">English Bayan Title :</label>
        <input type="text" name="en_bayan_title" id="bayan_title" required="required" class="form-control" >
      </div>
      
      <div class="form-group col-md-6 col-xs-12">
        <label for="miqaat_logo">English Bayan Image :</label>
        <input type="file" name="en_bayan_image" class="file">
      </div>

      <div class="form-group col-md-12" id="bayan">
        <label>English Bayan : </label>
        <textarea name="bayan_eng" class="ckeditor" class="form-control"></textarea>
      </div><!----CONTENT_BOX-->
      
      <div class="form-group col-md-6 col-xs-12">
        <label for="miqaat_logo">English Words Image :</label>
        <input type="file" name="en_word_image" class="file">
      </div>

      <div class="form-group col-md-12" id="bayan">
        <label>English Words : </label>
        <textarea name="words_eng" class="ckeditor" class="form-control"></textarea>
      </div><!----CONTENT_BOX-->

      <div class="clearfix"></div>
      <p>&nbsp;</p>
      <div class="form-group col-md-6 col-xs-12">
        <input type="submit" name="btn_submit" id="btn_submit" value="Submit" class="btn btn-primary btn-block form-control">
      </div>
    </form>

  </div>
</div>

<script type="text/javascript">

  $('#year').on('change', function (e) {
    $('#miqat').empty();
    var dropDown = document.getElementById("year");
    var year_val = dropDown.options[dropDown.selectedIndex].value;
    $.ajax({
      type: "POST",
      url: "ajax.php",
      data: {'cmd': 'get_miqat_list', 'year': year_val},
      success: function (data) {
        // Parse the returned json data
        var opts = $.parseJSON(data);
        // Use jQuery's each to iterate over the opts value
        $.each(opts, function (i, d) {
          // You will need to alter the below to get the right values from your json object.  Guessing that d.id / d.modelName are columns in your carModels data
          $('#miqat').append('<option value="' + d.id + '">' + d.miqaat_title + '</option>');
        });
      }
    });
  });

  function show_miqaat()
  {
    $('#miqat').empty()
    var dropDown = document.getElementById("year");
    var year_val = dropDown.options[dropDown.selectedIndex].value;
    $.ajax({
      type: "POST",
      url: "/ajax.php",
      data: {'cmd': 'get_miqat_list', 'year': year_val},
      success: function (data) {
        // Parse the returned json data
        var opts = $.parseJSON(data);
        alert(data);
        // Use jQuery's each to iterate over the opts value
        $.each(opts, function (i, d) {
          // You will need to alter the below to get the right values from your json object.  Guessing that d.id / d.modelName are columns in your carModels data
          $('#miqat').append('<option value="' + d.miqat_id + '">' + d.miqat_name + '</option>');
        });
      }
    });




    if (str == "")
    {
      document.getElementById("sub_menu").innerHTML = "";
      document.getElementById("mname").innerHTML = "";
      return;
    }

    if (window.XMLHttpRequest)
    {// code for IE7+, Firefox, Chrome, Opera, Safari
      xmlhttp = new XMLHttpRequest();
    }
    else
    {// code for IE6, IE5
      xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
    }



    xmlhttp.onreadystatechange = function ()
    {
      if (xmlhttp.readyState == 4 && xmlhttp.status == 200)
      {
        document.getElementById("sub_menu").innerHTML = xmlhttp.responseText;
      }
    }
    xmlhttp.open("GET", "get_data_by_ajax.php?menu=" + str, true);
    xmlhttp.send();
  }
</script>

<script type="text/javascript" src="ckeditor/ckeditor.js"></script>

<!-- /#page-wrapper -->
<?php include "./footer.php"; ?>
