<?php
require_once 'classes/class.user.admin.php';
$allowed_roles = array(ROLE_HEAD_OFFICE,ROLE_ASHARAH_ADMIN);
require_once 'session.php';

$user = new mtx_user_admin;

$title = 'Update user';
$description = '';
$keywords = '';
$active_page = 'manage_user';
$jamiat = FALSE;

if (isset($_POST['Update_user'])) {
  $jamiat = serialize(array($_POST['jamiat']));
  $result = $user->update_admin_user($_POST['name'], $_POST['email'], $_POST['mobile'], $_POST['Confirm_password'], $_GET['user_id'], $_POST['user_role'], $jamiat);
  if ($result) {
    $_SESSION[SUCCESS_MESSAGE] = 'User has been updated successfully.';
    header('Location: user_update.php?user_id=' . $_GET['user_id']);
    exit;
  } else {
    $_SESSION[ERROR_MESSAGE] = 'Erors Encountered while updating the user.';
    header('Location: user_update.php?user_id=' . $_GET['user_id']);
    exit;
  }
}

include('header.php');

$user_roles = array(ROLE_HEAD_OFFICE => 'Head Office', ROLE_COUNSELOR => 'Counselor', ROLE_SAHEB => 'Saheb', ROLE_REVIEWER => 'Reviewer', ROLE_PROJECT_COORDINATOR => 'Project Co-ordinator', ROLE_ADMIN_MARHALA_1 => 'Marhala 1', ROLE_ADMIN_MARHALA_2 => 'Marhala 2', ROLE_ADMIN_MARHALA_3 => 'Marhala 3', ROLE_ADMIN_MARHALA_4 => 'Marhala 4', ROLE_ADMIN_MARHALA_5 => 'Marhala 5', ROLE_ADMIN_MARHALA_6 => 'Marhala 6', ROLE_ADMIN_MARHALA_7 => 'Marhala 7', ROLE_ADMIN_MARHALA_8 => 'Marhala 8', ROLE_ASHARAH_ADMIN => 'Asharah Admin', ROLE_DATA_ENTRY => 'Data Entry', ROLE_JAMIAT_COORDINATOR => 'Jamiat Co-ordinator');

$select = $user->get_all_user($_GET['user_id']);
$jamiyet = unserialize($select['jamiat']);
?>
<div class="content-wrapper">

  <section class="content">
    <!-- Content -->
    <div class="row">
      <div class="col-md-12">&nbsp;</div>

      <!-- Center Bar -->
      <div class="col-md-12">
        <form method="post" name="myForm" role="form" class="form-horizontal">
          <div></div>

          <div class="form-group">
            <label class="col-md-4 control-label">Full Name</label>
            <div class="col-md-4">
              <input type="text" id="name" name="name" class="form-control" value="<?php echo $select['full_name']; ?>">
            </div>
          </div>

          <div class="form-group">
            <label class="col-md-4 control-label">ITS Key</label>
            <div class="col-md-4">
              <p class="form-control-static"><?php echo $select['its_id']; ?></p>
            </div>
          </div>

          <div class="form-group">
            <label class="col-md-4 control-label">User Name</label>
            <div class="col-md-4">
              <p class="form-control-static"><?php echo $select['username']; ?></p>
            </div>
          </div>

          <div class="form-group">
            <label class="col-md-4 control-label">Email ID</label>
            <div class="col-md-4">
              <input type="text" id="email" name="email" class="form-control" value="<?php echo $select['email']; ?>">
            </div>
          </div>

          <div class="form-group">
            <label class="col-md-4 control-label">Mobile</label>
            <div class="col-md-4">
              <input type="text" id="mobile" name="mobile" class="form-control" value="<?php echo $select['mobile']; ?>">
            </div>
          </div>

          <div class="form-group">
            <label class="col-md-4 control-label">User Type</label>
            <div class="col-md-4">
              <select name="user_role" id="user_role" class="form-control">
                <option value="">Select User Type</option>
                <?php 
                  if ($_SESSION[USER_ROLE] == ROLE_ASHARAH_ADMIN) {
                ?>
                  <option value="<?php echo ROLE_REVIEWER; ?>" <?php echo ($select['user_type'] == ROLE_REVIEWER) ? 'selected' : ''; ?>>Reviewer</option>
                  <option value="<?php echo ROLE_PROJECT_COORDINATOR; ?>" <?php echo ($select['user_type'] == ROLE_PROJECT_COORDINATOR) ? 'selected' : ''; ?>>Project Co-ordinator</option>
                <?php 
                  } else {
                    foreach ($user_roles as $key => $value) {
                      $selected = ($select['user_type'] == $key) ? 'selected' : ''; 
                ?>
                  <option value="<?php echo $key; ?>" <?php echo $selected; ?>><?php echo $value; ?></option>
                <?php 
                    }
                  }
                ?>
              </select>
            </div>
          </div>
          
          <div class="form-group jamiat">
            <label class="col-md-4 control-label">Jamiat</label>
            <div class="col-md-4">
              <select name="jamiat[]" class="form-control" multiple id="jamiat">
                <option value="">Select Jamiat</option>
                <?php
                if ($jamiats) {
                  foreach ($jamiats as $data) {
                ?>
                  <option value="<?php echo $data['jamiat']; ?>"><?php echo $data['jamiat']; ?></option>
                <?php
                  }
                }
                ?>
              </select>
            </div>
          </div>
          
          <?php if (count($jamiyet) == 1 && $jamiyet[0] != '') { ?>
          <div class="form-group jamiat">
            <label class="col-md-4 control-label">&nbsp;</label>
            <div class="col-md-4">
              <?php echo implode(', ', $jamiyet[0]); ?>
            </div>
          </div>
          <?php } ?>
          
          <div class="form-group">
            <label class="col-md-4 control-label">Password</label>
            <div class="col-md-4">
              <input type="password" name="Confirm_password" id="Confirm_password" class="form-control" value="">
            </div>
          </div>

          <div class="form-group">
            <label class="col-md-4 control-label">&nbsp;</label>
            <div class="col-md-4">
              <input type="submit" id="Update_user" name="Update_user" value="Update" class="btn btn-success">
            </div>
          </div>
        </form>
      </div>
      <!-- /Center Bar -->
      <script>
        var right = true; //if old password is right than right = true else right = false;
        $('#Update_user').click(function() {
          var name = $('#name').val();
          var email = $('#email').val();
          var mobile = $('#mobile').val();
          var user_role = $('#user_role').val();
          var error = 'Following error(s) are occurred\n\n';
          var validate = true;
          if (name == '')
          {
            error += 'Please enter name\n';
            validate = false;
          }
          if (email == '')
          {
            error += 'Please enter email\n';
            validate = false;
          }
          if (user_role == '')
          {
            error += 'Please select User role\n';
            validate = false;
          }
          if (validate == false)
          {
            alert(error);
            return validate;
          }

        });
        
        $('#user_role').on('click', function() {
          var user_role = $('#user_role').val();
          if(user_role == '<?php echo ROLE_JAMIAT_COORDINATOR; ?>'){
            $('.jamiat').show(600);
          } else {
            $('.jamiat').hide(600);
          }
        });
        
        <?php 
          if($select['user_type'] == ROLE_JAMIAT_COORDINATOR) {
            if (count($jamiyet) == 1 && $jamiyet[0] != '') {
        ?>
          $('.jamiat').show(600);
        <?php 
           } 
          } else {
        ?>
          $('.jamiat').hide(600);
        <?php
          }
        ?>
          
        $('#jamiat').selectize();
      </script>

      <style>
        .box{ display: none; }
      </style>

    </div>
    <!-- /Content -->
  </section>
</div>

<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<?php
include('footer.php');
?>
