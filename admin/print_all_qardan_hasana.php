<?php
require_once '../classes/class.database.php';
include 'classes/class.araiz.php';
$allowed_roles = array(ROLE_PROJECT_COORDINATOR,ROLE_ASHARAH_ADMIN);
require_once 'session.php';

$araiz = new Araiz();

$title = 'Recommended Qardan Hasana';
$description = '';
$keywords = '';
$active_page = "report_qardan_hasana";

$qardan_hasana = $araiz->get_all_assign_araz_to_qardan_hasana();
$total_qardan_hasana = $araiz->count_total_assign_araz_to_qardan_hasana();
$total_qardan_hasana_done = $araiz->count_total_qardan_hasana_done();
$total_qardan_hasana_pending = $total_qardan_hasana - $total_qardan_hasana_done;

$araiz_qardan_hasana = $araiz->get_araiz_data_organised($qardan_hasana);

include ('print_header.php');
?>
<body style="padding: 10px;">
  <div>
    <p style="display: block; text-align: right"><?php echo date('d-m-Y H:i:s'); ?></p>
  </div>
  
  <div class="row">
    <div class="col-xs-12 col-sm-12">
      <h4>Total Recommended Qardan Hasana: <?php echo $total_qardan_hasana; ?></h4>
      <h4>Total Qardan Hasana Done: <?php echo $total_qardan_hasana_done; ?></h4>
      <h4>Total Pending for Qardan Hasana: <?php echo $total_qardan_hasana_pending ?></h4>
    </div>
  </div>
  <div class="row">&nbsp;</div>
  
  <div class="row">
    <div class="col-md-12">
      <table class="table table-responsive table-condensed">
        <thead>
          <tr>
            <th>Sr No.</th>
            <th>ITS</th>
            <th>Full Name</th>
            <th>Email</th>
            <th>Mobile</th>
            <th>Qardan Hasana Done</th>
          </tr>
        </thead>
        <tbody>
        <?php
        if($araiz_qardan_hasana){
        $i = 1;
        foreach ($araiz_qardan_hasana as $araz_id => $araz) {
          $user_details = $araz['user_data'];
          $araz_general_detail = $araz['araz_data'];

          if($araz_general_detail['qardan_hasana_done'] == '1'){
            $mark = 'Yes';
          }else {
            $mark = 'No';
          }
        ?>
          <tr>
            <td><?php echo $i++;?></td>
            <td><?php echo $user_details['login_its']; ?></td>
            <td><?php echo $user_details['user_full_name']; ?></a></td>
            <td><?php echo $user_details['email']; ?></td>
            <td><?php echo $user_details['mobile']; ?></td>
            <td><?php echo $mark; ?></td>
          </tr>
        <?php
          }
        }else {
          echo '<tr><td class="text-center" colspan="6">No Records..</td></tr>';
        }
        ?>
        </tbody>
      </table>
    </div>
  </div>
</body>
</html>
