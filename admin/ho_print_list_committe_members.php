<?php
require_once '../classes/constants.php';
require_once '../classes/class.database.php';
require_once 'classes/admin_gen_functions.php';
require_once 'classes/class.user.admin.php';
require_once 'classes/class.edu_commitee.php';

$allowed_roles = array(ROLE_HEAD_OFFICE);
require_once 'session.php';

// Check if tanzeem_id set or else redirect to list page
if (isset($_GET['city']) && $_GET['city'] !== '') {
  $tanzeem_id = $_GET['city'];
  // Get list of members in the tanzeem
  $edu_comm = new Edu_committee($tanzeem_id);
  $edu_comm_members = $edu_comm->members_get_list();
} else {
  header('Location: ' . SERVER_PATH . 'admin/ho_list_gaam.php');
}

$title = 'List of Education Committee Members in ' . $edu_comm->get_jamat_name();
$description = '';
$keywords = '';
$active_page = "ho_list_edu_comm_members";

include_once("print_header.php");
?>
<body>
<div>
  <p style="display: block; text-align: right"><?php echo date('d-m-Y H:i:s'); ?></p>
</div>
 
<script type="text/javascript">
    function printpage() {
        var printButton = document.getElementById("printpagebutton");
        printButton.style.visibility = 'hidden';
        window.print()
        printButton.style.visibility = 'visible';
    }
</script>
  
<button id="printpagebutton" onclick="printpage()" class="btn btn-success btn-lg pull-right">Print</button>



<div class="row">
  <div class="col-lg-12">
    <h3 class="page-header">List of Education Committee Members in <?php echo $edu_comm->get_jamat_name(); ?></h3>
  </div>
  <!-- /.col-lg-12 -->
</div>
<!-- /.row -->

<!-- List of Edu Committee -->
<div class="row">

  <div class="col-md-12">
    <table class="table table-responsive table-condensed">
      <thead>
        <tr>
          <th>Sr</th>
          <th>Photo</th>
          <th>Name</th>
          <th>Designation</th>
          <th>Mobile</th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <td>1</td>
          <td></td>
          <td class="lsd">العامل الحاضر</td>
          <td>President</td>
          <td></td>
        </tr>
        <tr>
          <td>2</td>
          <td></td>
          <td class="lsd" colspan="2">لجنة العمال والمعلمين والمراقبين</td>
          <td></td>
        </tr>
        <?php
        if ($edu_comm_members) {
          $i = 2;
          $approved = 0;
          $rejected = 0;
          $downloaded = 0;
          $misal_uploaded = 0;
          foreach ($edu_comm_members as $mem) {
            $i++;
            $co_ord = ($mem['is_co_ordinator'] == 1) ? ', Co-ordinator' : '';
            $mamur_co_ord = ($mem['is_mamur_co_ordinator'] == 1) ? ', Co-ordinator' : '';

            $tmp_user = new mtx_user_admin();
            $mem_img = $tmp_user->get_mumin_photo($mem['its']);
            $designation = $edu_comm->designation_by_id_to_string($mem['designation']);
            ?>
            <tr>
              <td rowspan="2"><?php echo $i; ?></td>
              <td rowspan="2"><img src="<?php echo $mem_img ?>" height="80px"></td>
              <td class="lsd"><?php echo $mem['name']; ?></td>
              <td><?php echo $designation . $co_ord . $mamur_co_ord; ?></td>
              <td><?php echo $mem['mobile']; ?></td>
            </tr>
            <tr class="<?php echo $tr_class; ?>">
              <td><strong>Qualification: </strong><?php echo $mem['qualification']; ?></td>
              <td><strong>Occupation: </strong><?php echo $mem['occupation']; ?></td>
              <td><?php echo $mem['email']; ?></td>
            </tr>
            <?php
          }
        } else {
          ?>
          <tr>
            <td colspan="4">No Records Found.</td>
          </tr>
          <?php
        }
        ?>
      </tbody>
    </table>
  </div><!-- ./ md-12 -->

</div><!-- ./ row -->

<div class="row">&nbsp;</div>

</body>