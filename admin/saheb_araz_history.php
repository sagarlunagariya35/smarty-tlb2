<?php
require_once 'classes/class.user.admin.php';
require_once '../classes/class.user.php';
include 'classes/class.araiz.php';
$allowed_roles = array(ROLE_SAHEB);
require_once 'session.php';

$user = new mtx_user_admin;
$araiz = new Araiz();

$title = 'Araiz History';
$description = '';
$keywords = '';
$active_page = "history_saheb_araz";

$select = FALSE;
$from_date = $to_date = $pagination = FALSE;
$araz_saheb_id = $saheb_id = $araz_coun_id = $coun_id = FALSE;
$counselor_days_past = $saheb_days_past = FALSE;

if(isset($_POST['print_list_raza']))
{
  $_SESSION['track_id'] = (array) $_POST['jawab'];
  header('location: print_raza_araiz_list_date_wise.php');
}

if(isset($_POST['print_list_istirshad']))
{
  $_SESSION['track_id'] = (array) $_POST['jawab'];
  header('location: print_istirshad_araiz_date_wise.php');
}
include ('header.php');

$select = $araiz->get_all_jawab_sent_araz_of_saheb($_SESSION[USER_ID], $page, 10);
$total_araz = $araiz->count_total_jawab_sent_araz_of_saheb($_SESSION[USER_ID]);

$araiz_parsed_data = $araiz->get_araiz_data_organised($select);
$courses = $araiz->get_all_courses();
$countries = $araiz->get_all_countries_list();
?>

<link rel="stylesheet" href="js/tablesorter/css/theme.bootstrap.css">
<script src="js/tablesorter/js/jquery.tablesorter.js"></script>
<script src="js/tablesorter/js/jquery.tablesorter.widgets.js"></script>
<style>
  .alfatemi-text{
    font-size: 18px;
  }
</style>
<div class="row">

  <div class="row">
    <div class="col-xs-12 col-sm-12">&nbsp;</div>
  </div>
  <div class="row">
    <div class="col-xs-12 col-sm-12">
      <h2 class="text-center">Total Jawab Send Araiz Count: <?php echo $total_araz; ?></h2>
    </div>
  </div>
  <div class="row">
    <div class="col-xs-12 col-sm-12">&nbsp;</div>
  </div>
  <form name="raza_form" class="form" method="post" action="">
  
  <div class="col-lg-12 col-xs-12 col-sm-12 pull-left">
    <div class="col-md-12">&nbsp;</div>
    <div class="clearfix"></div>
    <div class="row">
      <ul class="nav nav-tabs nav-justified" role="tablist">
        <li role="presentation" class="active"><a href="#raza_araiz" aria-controls="pre" role="tab" data-toggle="tab">Raza Araiz</a></li>
        <li role="presentation"><a href="#istirshad_araiz" aria-controls="in" role="tab" data-toggle="tab">Isttirshad Araiz</a></li>
      </ul>
      <div class="row">
        <div class="col-xs-12 col-sm-12">&nbsp;</div>
      </div>
      <?php
        require_once 'pagination.php';
        $pagination = pagination(10, $page, 'saheb_araz_history.php?page=', $total_araz);
      ?>
      <div class="tab-content">
        <div role="tabpanel" class="tab-pane active" id="raza_araiz">
          <div class="col-xs-12 col-sm-3" style="padding-top:6px">
            <input type="checkbox" name="print" value="Print" id="check_all_raza" onchange="checkAllRaza(this);" class="css-checkbox1"><label for="check_all_raza" class="css-label1">Check All</label>
          </div>
          <div class="col-xs-12 col-sm-3 pull-right">
            <input type="submit" name="print_list_raza" value="Print" class="btn btn-success btn-block">
          </div>
          <div class="row">
            <div class="col-xs-12 col-sm-12">&nbsp;</div>
          </div>
          <?php
          foreach ($araiz_parsed_data as $araz_id => $araz) {
            // Create bins for User Data, Araz Data, Previous Studies, Current Studeis, Remarks
            $user_details = $araz['user_data'];
            $araz_general_detail = $araz['araz_data'];
            $previous_study_details = $araz['previous_study_details'];
            $course_details = $araz['course_details'];
            $counselor_details = $araz['counselor_details'];

            if($user_details['araz_type'] == 'raza'){
              include 'inc/inc.show_parsed_araiz_details.php';
            }
          }
          ?>
        </div>
        
        <div role="tabpanel" class="tab-pane" id="istirshad_araiz">
          <div class="col-xs-12 col-sm-3" style="padding-top:6px">
            <input type="checkbox" name="print" value="Print" id="check_all_istirshad" onchange="checkAllIstirshad(this);" class="css-checkbox1"><label for="check_all_istirshad" class="css-label1">Check All</label>
          </div>
          <div class="col-xs-12 col-sm-3 pull-right">
            <input type="submit" name="print_list_istirshad" value="Print" class="btn btn-success btn-block">
          </div>
          <div class="row">
            <div class="col-xs-12 col-sm-12">&nbsp;</div>
          </div>
          <?php
          foreach ($araiz_parsed_data as $araz_id => $araz) {
            // Create bins for User Data, Araz Data, Previous Studies, Current Studeis, Remarks
            $user_details = $araz['user_data'];
            $araz_general_detail = $araz['araz_data'];
            $previous_study_details = $araz['previous_study_details'];
            $course_details = $araz['course_details'];
            $counselor_details = $araz['counselor_details'];

            if($user_details['araz_type'] == 'istirshad'){
              include 'inc/inc.show_parsed_araiz_details.php';
            }
          }
          ?>
        </div>
      </div>
      <?php echo $pagination; ?>
    </div>
  </div>
</form>
</div>
<style>
  .popover{
    color:#000;
  }
</style>

<?php
include 'footer.php';
?>

