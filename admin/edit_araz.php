<?php
require_once 'classes/class.user.admin.php';
require_once '../classes/class.user.php';
include 'classes/class.araiz.php';
$allowed_roles = array(ROLE_HEAD_OFFICE, ROLE_AAMIL_SAHEB, ROLE_JAMIAT_COORDINATOR);
require_once 'session.php';

$user = new mtx_user_admin;
$araiz = new Araiz();

$title = 'Edit Araz';
$description = '';
$keywords = '';
$active_page = "edit_araz";

$select = FALSE;
$from_date = $to_date = $pagination = FALSE;
$araz_saheb_id = $saheb_id = $araz_coun_id = $coun_id = FALSE;
$counselor_days_past = $saheb_days_past = $jamiyet_data = FALSE;

$araz_id = (isset($_REQUEST['araz_id'])) ? $_REQUEST['araz_id'] : FALSE;

if(isset($_POST['print_list']))
{
  $_SESSION['track_id'] = (array) $_POST['jawab'];
  header('location: print_raza_araiz_list_date_wise.php');
}

if (isset($_POST['delete_id'])) {
  $delete_id = $_POST['delete_id'];
  $result = $araiz->delete_araz_course($delete_id);

  if ($result) {
    $_SESSION[SUCCESS_MESSAGE] = "Course Successfully Delete";
  } else {
    $_SESSION[ERROR_MESSAGE] = "Error while Delete";
  }
}

if(isset($_POST['add_course'])) {
  $course_duration = $_POST['course_duration'];
  $course = $_POST['course_name'];
  $institute_name = $_POST['institute_name'];
  $country = $_POST['institute_country'];
  $city = $_POST['institute_city'];
  $accomodation = $_POST['accomodation'];
  $course_start_month = $_POST['course_start_month'];
  $course_start_year = $_POST['course_start_year'];
  
  $result = $araiz->edit_courses_in_araz(FALSE,$course_duration,$course,$institute_name,$country,$city,$accomodation,$course_start_month,$course_start_year,$araz_id);
  if($result){
    $_SESSION[SUCCESS_MESSAGE] = 'New Course Added Successfully.';
  }else {
    $_SESSION[ERROR_MESSAGE] = 'Error Encountered While Processing..';
  }
}

$date = date('Y-m-d');

$select = $araiz->get_araz_data_by_araz_id($araz_id);

$araiz_parsed_data = $araiz->get_araiz_data_organised($select);
$courses = $araiz->get_all_courses();
$countries = $araiz->get_all_countries_list();

if($select[0]['marhala'] == '5') {
  $stream_array = array('Arts', 'Commerce', 'Science');
  $standard_array = array('11', '12');
}else {
  $stream_array = array_sort($araiz->get_finalize_courses(),'course',SORT_ASC);
  $standard_array = array('3 months', '6 months', '9 months', '1 year', '1 year 3 months', '1 year 6 months', '1 year 9 months', '2 years', '2 year 3 months', '2 year 6 months', '2 year 9 months', '3 years', '3 year 3 months', '3 year 6 months', '3 year 9 months', '4 years', '4 year 3 months', '4 year 6 months', '4 year 9 months', '5 years');
}

include ('header.php');
$counselor_and_saheb_content = COUNSELOR_AND_SAHEB_CONTENT;
$jawab_sent_content = JAWAB_SENT_CONTENT;
?>

<link rel="stylesheet" href="js/tablesorter/css/theme.bootstrap.css">
<script src="js/tablesorter/js/jquery.tablesorter.js"></script>
<script src="js/tablesorter/js/jquery.tablesorter.widgets.js"></script>
<style>
  .alfatemi-text{
    font-size: 18px;
  }
</style>
<div class="row">
  <div class="row">
    <div class="col-xs-12 col-sm-12">
      <h2 class="text-center">Add New Course in Araz</h2>
    </div>
  </div>
  <div class="row">
    <div class="col-xs-12 col-sm-12">&nbsp;</div>
  </div>
  <form name="raza_form" class="form" method="post" action="">
    <?php
      if($select[0]['marhala'] == '5') {
    ?>
    <div class="row">
      <div class="col-sm-4 col-xs-12">
        <select name="course_name" class="form-control">
          <option value="">Stream</option>
          <?php
          foreach ($stream_array as $crs) {
            ?>
            <option value="<?php echo $crs; ?>"><?php echo $crs; ?></option>
            <?php
          }
          ?>
        </select>
      </div>
      <div class="col-sm-4 col-xs-12">
        <select name="course_duration" class="form-control">
          <option value="">Standard</option>
          <?php
          foreach ($standard_array as $std) {
            ?>
            <option value="<?php echo $std; ?>"><?php echo $std; ?></option>
            <?php
          }
          ?>
        </select>
      </div>
    </div>
    <?php
      }else {
    ?>
    <div class="row clscourselocation">
      <div class="col-sm-4 col-xs-12">
        <select name="course_name" class="form-control clsdeegree">
          <option value="">Course</option>
          <?php
          foreach ($stream_array as $crs) {
            ?>
            <option value="<?php echo $crs['course']; ?>"><?php echo $crs['course']; ?></option>
            <?php
          }
          ?>
        </select>
      </div>
      <div class="col-sm-4 col-xs-12">
        <select name="course_duration" class="form-control">
          <option value="">Course Duration</option>
          <?php
          foreach ($standard_array as $std) {
            ?>
            <option value="<?php echo $std; ?>"><?php echo $std; ?></option>
            <?php
          }
          ?>
        </select>
      </div>
    </div>
    <?php
      }
    ?>
    <div class="row">
      <div class="col-xs-12 col-sm-12">&nbsp;</div>
    </div>
    <div class="row clslocation">
      <div class="col-md-4">
        <input type="text" name="institute_name" class="form-control" placeholder="Full name of the institute"/>
      </div>
      <div class="col-md-4">
        <select name="institute_country" class="form-control clscountry">
          <option value="">Select Country</option>
          <?php foreach ($countries as $cntry) { ?>
            <option value="<?php echo $cntry['name']; ?>"><?php echo $cntry['name']; ?></option>
          <?php } ?>
        </select>
      </div>
      <div class="col-md-4">
        <select name="institute_city" class="form-control clscity" id="city-<?php echo $araz_id; ?>">
          <option value="">Select City</option>
        </select>
      </div>
    </div>
    <div class="row">
      <div class="col-xs-12 col-sm-12">&nbsp;</div>
    </div>
    <div class="row">
      <div class="col-md-4 col-xs-12">
        <select name="accomodation" class="form-control">
          <option value="">Accommodation</option>
          <option value="Own House">Own House</option>
          <option value="Relatives House">Relative's House</option>
          <option value="Hostel">Hostel</option>
          <option value="Individual Rental">Individual Rental</option>
          <option value="Sharing Rental with Mumin">Sharing Rental with Mumin</option>
          <option value="Sharing Rental with Non Mumin">Sharing Rental with Non Mumin</option>
          <option value="Not Decided">Not Decided</option>
          <option value="Distance Learning">Distance Learning</option>
        </select>
      </div>
      <?php
      $course_month_array = array('January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December');
      ?>
      <div class="col-md-4 col-xs-12">
        <select name="course_start_month" class="form-control">
          <option value="">Course Start Month</option>
          <?php
          foreach ($course_month_array as $str_mnth) {
            ?>
            <option value="<?php echo $str_mnth; ?>" ><?php echo $str_mnth; ?></option>
            <?php
          }
          ?>
        </select>
      </div>
      <div class="col-md-4 col-xs-12">
        <select name="course_start_year" class="form-control">
          <option value="">Course Start Year</option>
          <?php
          $cur_year = date('Y');
          $rec_year = $cur_year - 4;
          $nxt_year = $cur_year + 4;
          for ($i = $rec_year; $i <= $nxt_year; $i++) {
            ?>
            <option value="<?php echo $i; ?>" ><?php echo $i; ?></option>
            <?php
          }
          ?>
        </select>
      </div>
    </div>
    <div class="row">
      <div class="col-xs-12 col-sm-12">&nbsp;</div>
    </div>
    <div class="row">
      <div class="col-xs-12 col-sm-3 pull-right">
        <input type="submit" name="add_course" value="Add Course" class="btn btn-success btn-block sent-suggestion">
      </div>
    </div>
  
  <div class="col-lg-12 col-xs-12 col-sm-12 pull-left">
    <div class="col-md-12">&nbsp;</div>
    <div class="clearfix"></div>
    <div class="row">
      <?php
      foreach ($araiz_parsed_data as $araz_id => $araz) {
        // Create bins for User Data, Araz Data, Previous Studies, Current Studeis, Remarks
        $user_details = $araz['user_data'];
        $araz_general_detail = $araz['araz_data'];
        $previous_study_details = $araz['previous_study_details'];
        $course_details = $araz['course_details'];
        $counselor_details = $araz['counselor_details'];
        
        include 'parse_araiz_details.php';
      }
      ?>

    </div>
  </div>
</form>
<form method="post" id="frm-del-grp">
  <input type="hidden" name="delete_id" id="del-araz-val" value="">
</form>
</div>

<script>
  $('.clsdeegree').on("change", function () {
   var strdegree = $(this).val();
   var course = $(this).closest(".clscourselocation").find(".clscourse");
   course.empty();
     $.ajax({
         url: 'get_data_by_ajax.php',
         method: 'POST',
         data: 'cmd=get_courses&degree=' + strdegree + '&marhala=<?php echo $select[0]['marhala'] ?>',
         success: function (data) {
           course.append('<option value="">Select Course</option>');
           var objcourses = jQuery.parseJSON(data);
           $.each(objcourses, function() {
             course.append('<option value="' + this.course_name + '">' + this.course_name + '</option>');
           });
           ;
         }
     });    
  });

  $('.sent-suggestion').on("click", function(){
    $('input:checkbox:checked.jawab').each(function(){
      var araz_id =  $(this).val();
      var course = $('#course-' + araz_id).val();
      var country = $('#country-' + araz_id).val();
      var city = $('#city-' + araz_id).val();
      var remarks = $('#remarks-' + araz_id).val();
      var coun_id = <?php echo ($_SESSION[USER_ID]) ? $_SESSION[USER_ID] : 0; ?>;
    
      $.ajax({
        type: "POST",
        url: "ajax.php",
        data: 'cmd=make_entry_of_counselor_suggestion&araz_id=' + araz_id +'&counselor_id=' + coun_id +'&course=' + course + '&country=' + country + '&city=' + city + '&remarks=' + remarks,
        cache: false,
        success: function () {
        }
      });
    });
  });
</script>
<?php
include 'footer.php';
?>
