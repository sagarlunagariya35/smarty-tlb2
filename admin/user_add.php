<?php
require_once 'classes/class.user.admin.php';
$allowed_roles = array(ROLE_HEAD_OFFICE,ROLE_ASHARAH_ADMIN);
require_once 'session.php';

$user = new mtx_user_admin;
$jamiyet = FALSE;

if (isset($_POST['user_add'])) {
  $jamiyet = serialize(array($_POST['jamiat']));
  $result = $user->add_user($_POST['its'], $_POST['name'], $_POST['email'], $_POST['mobile'], $_POST['user_id'], $_POST['password'], $_POST['user_role'], $jamiyet);
  if ($result)
    $_SESSION[SUCCESS_MESSAGE] = 'User has been added successfully.';
  else
    $_SESSION[ERROR_MESSAGE] = 'Sorry! try again.';
}

$title = 'Add user';
$description = '';
$keywords = '';
$active_page = 'add_user';

$user_roles = array(ROLE_HEAD_OFFICE => 'Head Office', ROLE_COUNSELOR => 'Counselor', ROLE_SAHEB => 'Saheb', ROLE_REVIEWER => 'Reviewer', ROLE_PROJECT_COORDINATOR => 'Project Co-ordinator', ROLE_ADMIN_MARHALA_1 => 'Marhala 1', ROLE_ADMIN_MARHALA_2 => 'Marhala 2', ROLE_ADMIN_MARHALA_3 => 'Marhala 3', ROLE_ADMIN_MARHALA_4 => 'Marhala 4', ROLE_ADMIN_MARHALA_5 => 'Marhala 5', ROLE_ADMIN_MARHALA_6 => 'Marhala 6', ROLE_ADMIN_MARHALA_7 => 'Marhala 7', ROLE_ADMIN_MARHALA_8 => 'Marhala 8', ROLE_ASHARAH_ADMIN => 'Asharah Admin', ROLE_DATA_ENTRY => 'Data Entry', ROLE_JAMIAT_COORDINATOR => 'Jamiat Co-ordinator');

include ('header.php');
?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">

  <section class="content">
    <!-- Content -->
    <div class="row">
      <div class="col-md-12">&nbsp;</div>

      <!-- Center Bar -->
      <div class="col-md-12">
        <form method="post" name="myForm" id="myForm" role="form" class="form-horizontal">
          <div></div>

          <div class="form-group">
            <label class="col-md-4 control-label">ITS Key</label>
            <div class="col-md-4">
              <input type="text" id="its" name="its" class="form-control">
            </div>
          </div>

          <div class="form-group">
            <label class="col-md-4 control-label">Full Name</label>
            <div class="col-md-4">
              <input type="text" id="name" name="name" class="form-control">
            </div>
          </div>

          <div class="form-group">
            <label class="col-md-4 control-label">Email ID</label>
            <div class="col-md-4">
              <input type="text" id="email" name="email" class="form-control">
            </div>
          </div>

          <div class="form-group">
            <label class="col-md-4 control-label">Mobile</label>
            <div class="col-md-4">
              <input type="text" id="mobile" name="mobile" class="form-control">
            </div>
          </div>

          <div class="form-group" id="u_id">
            <label class="col-md-4 control-label">User ID</label>
            <div class="col-md-4">
              <input type="text" id="user_id" name="user_id" class="form-control">
            </div>
          </div>

          <div class="form-group">
            <label class="col-md-4 control-label">Password</label>
            <div class="col-md-4">
              <input type="password" name="password" id="password" class="form-control">
            </div>
          </div>

          <div class="form-group">
            <label class="col-md-4 control-label">User Type</label>
            <div class="col-md-4">
              <select name="user_role" id="user_role" class="form-control">
                <option value="">Select User Type</option>
                <?php 
                  if ($_SESSION[USER_ROLE] == ROLE_ASHARAH_ADMIN) {
                ?>
                <option value="<?php echo ROLE_REVIEWER; ?>">Reviewer</option>
                <option value="<?php echo ROLE_PROJECT_COORDINATOR; ?>">Project Co-ordinator</option>
                <?php 
                  } else {
                    foreach ($user_roles as $key => $value) {
                ?>
                  <option value="<?php echo $key; ?>"><?php echo $value; ?></option>
                <?php 
                    }
                  }
                ?>
              </select>
            </div>
          </div>
          
          <div class="form-group jamiat">
            <label class="col-md-4 control-label">Jamiat</label>
            <div class="col-md-4">
              <select name="jamiat[]" class="form-control" multiple id="jamiat">
                <option value="">Select Jamiat</option>
                <?php
                if ($jamiats) {
                  foreach ($jamiats as $data) {
                    ?>
                    <option value="<?php echo $data['jamiat']; ?>"><?php echo $data['jamiat']; ?></option>
                    <?php
                  }
                }
                ?>
              </select>
            </div>
          </div>

          <div class="form-group">
            <label class="col-md-4 control-label">&nbsp;</label>
            <div class="col-md-4">
              <input type="submit" id="user_add" name="user_add" value="Add" class="btn btn-success">
            </div>
          </div>
        </form>
      </div>
      <!-- /Center Bar -->
      <script>
        var right = true;
        $('#user_add').click(function() {
          var its = $('#its').val();
          var name = $('#name').val();
          var email = $('#email').val();
          var mobile = $('#mobile').val();
          var user_id = $('#user_id').val();
          var pwd = $('#password').val();
          var user_role = $('#user_role').val();
          var error = 'Following error(s) are occurred\n\n';
          var validate = true;
          if (its == '')
          {
            error += 'Please enter ITS Key\n';
            validate = false;
          }
          if (name == '')
          {
            error += 'Please enter User name\n';
            validate = false;
          }
          if (email == '')
          {
            error += 'Please enter email\n';
            validate = false;
          }
          if (mobile == '')
          {
            error += 'Please enter mobile\n';
            validate = false;
          }
          if (user_id == '')
          {
            error += 'Please enter your desired user id\n';
            validate = false;
          }
          if (pwd == '')
          {
            error += 'Please enter password\n';
            validate = false;
          }
          if (user_role == '')
          {
            error += 'Please select User Role\n';
            validate = false;
          }
          if (right == false) {
            error += 'User Id already exists\n';
            validate = false;
          }
          if (validate == false)
          {
            alert(error);
            return validate;
          }
        });

        $('#user_id').change(function() {
          var userid = $('#user_id').val();
          $.ajax({
            url: "ajax.php",
            type: "GET",
            data: 'userid=' + userid + '&cmd=check_user_exist',
            success: function(data)
            {
              if (data == '1') {
                alert('User Id already exists');
                $('#user_id').focus();
                right = false;
                $('#u_id').attr('class', 'form-group has-error');
                return false;
              } else {
                right = true;
                $('#u_id').attr('class', 'form-group has-success');
                return true;
              }
            }
          });
        });
        
        $('#user_role').on('click', function() {
          var user_role = $('#user_role').val();
          if(user_role == '<?php echo ROLE_JAMIAT_COORDINATOR; ?>'){
            $('.jamiat').show(600);
          } else {
            $('.jamiat').hide(600);
          }
        });
        $('.jamiat').hide(600);
        $('#jamiat').selectize();
      </script>    

    </div>
    <!-- /Content -->
  </section>
</div>

<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<?php
include('footer.php');
?>
