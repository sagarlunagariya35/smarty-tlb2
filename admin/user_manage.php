<?php
require_once 'classes/class.user.admin.php';
$allowed_roles = array(ROLE_HEAD_OFFICE,ROLE_ASHARAH_ADMIN);
require_once 'session.php';

$user = new mtx_user_admin;

$title = 'Manage user';
$description = '';
$keywords = '';
$active_page = 'manage_user';

if (isset($_POST['delete_id'])) {
  $delete_id = $_POST['delete_id'];
  $result = $user->delete_user($delete_id);

  if ($result) {
    $_SESSION[SUCCESS_MESSAGE] = "User Successfully Delete";
  } else {
    $_SESSION[ERROR_MESSAGE] = "Error while Delete";
  }
}

if(isset($_GET['cmd']) && $_GET['cmd'] == 'update') {
  $query = "UPDATE `tlb_user` SET `active` = '$_GET[status]' WHERE `user_id` = '$_GET[id]'";
  $result = $db->query($query);
  if ($result) {
    $_SESSION[SUCCESS_MESSAGE] = 'Record updated successfully.';
    header('Location: user_manage.php');
    exit();
  } else {
    $_SESSION[ERROR_MESSAGE] = 'Sorry! try again.';
    header('Location: user_manage.php');
    exit();
  }
}

if ($_SESSION[USER_ROLE] == ROLE_ASHARAH_ADMIN) {
  $users = $user->get_all_user(FALSE, 'asharah_admin');
} else {
  $users = $user->get_all_user();
}
if(isset($_POST['download'])) {
  
  $download = download_csv($users, 'user_data.csv');
  
  if ($download) {
    $_SESSION[SUCCESS_MESSAGE] = "CSV Successfully Download";
  } else {
    $_SESSION[ERROR_MESSAGE] = "Error while download";
  }
}

function download_csv($ary_data, $filename, $csv_headers = FALSE) {
    // output headers so that the file is downloaded rather than displayed
    header('Content-Type: text/csv; charset=utf-8');
    header('Content-Disposition: attachment; filename=' . $filename);
    header("Pragma: no-cache");
    header("Expires: 0");
  // create a file pointer connected to the output stream
    $output = fopen('php://output', 'w');

  // output the column headings
    fputcsv($output, array('id', 'its_id', 'full_name', 'username', 'email', 'mobile'));

  // fetch the data
  // loop over the rows, outputting them
    foreach ($ary_data as $row) {
      fputcsv($output, array($row['user_id'], $row['its_id'], $row['full_name'], $row['username'], $row['email'], $row['mobile']));
    }
    fclose($output);
    exit();
  }

include ('header.php');
?>

  <div class="content-wrapper">

    <section class="content">
      <!-- Content -->
      <div class="row">
        <form method="post" role="form" class="form-horizontal">
          <div class="col-md-12">&nbsp;</div>
          <div class="col-md-12">
            <input type="submit" class="btn btn-primary pull-right" name="download" value="Download CSV">
          </div>
          <div class="col-md-12">&nbsp;</div>
          <!-- Center Bar -->
          <div class="col-md-12">
            <div class="panel panel-primary">
              <div class="panel-heading">
                <i class="fa fa-group fa-fw"></i> <?php echo $title; ?>
                <span class="pull-right"><a href="user_add.php" style="color: white;">Add New User</a></span>
              </div>
              <!-- /.panel-heading -->
              <div class="panel-body">
                <div class="table-responsive">
                  <table class="table table-striped table-hover table-condensed">
                    <thead>
                      <tr>
                        <th>Sr No.</th>
                        <th>ITS</th>
                        <th>Full Name</th>
                        <th>User Name</th>
                        <th>Email</th>
                        <th>Mobile</th>
                        <th>Role</th>
                        <th>Delete</th>
                      </tr>
                    </thead>
                    <tbody>
                      <?php 
                      if($users){
                        $i = 1; 
                        foreach($users as $usr){
                          $role[ROLE_HEAD_OFFICE] = 'Head Office';
                          $role[ROLE_COUNSELOR] = 'Counselor';
                          $role[ROLE_SAHEB] = 'Saheb';
                          $role[ROLE_REVIEWER] = 'Reviewer';
                          $role[ROLE_PROJECT_COORDINATOR] = 'Project Co-ordinator';
                          $role[ROLE_ADMIN_MARHALA_1] = 'Marhala 1';
                          $role[ROLE_ADMIN_MARHALA_2] = 'Marhala 2';
                          $role[ROLE_ADMIN_MARHALA_3] = 'Marhala 3';
                          $role[ROLE_ADMIN_MARHALA_4] = 'Marhala 4';
                          $role[ROLE_ADMIN_MARHALA_5] = 'Marhala 5';
                          $role[ROLE_ADMIN_MARHALA_6] = 'Marhala 6';
                          $role[ROLE_ADMIN_MARHALA_7] = 'Marhala 7';
                          $role[ROLE_ADMIN_MARHALA_8] = 'Marhala 8';
                          $role[ROLE_ASHARAH_ADMIN] = 'Asharah Admin';
                          $role[ROLE_DATA_ENTRY] = 'Data Entry';
                          $role[ROLE_JAMIAT_COORDINATOR] = 'Jamiat Co-ordinator';
                      ?>
                      <tr>
                        <td><?php echo $i++;?></td>
                        <td><?php echo $usr['its_id']; ?></td>
                        <td><?php echo $usr['full_name']; ?></a></td>
                        <td><a href="user_update.php?user_id=<?php echo $usr['user_id']; ?>"><?php echo $usr['username']; ?></a></td>
                        <td><?php echo $usr['email']; ?></td>
                        <td><?php echo $usr['mobile']; ?></td>
                        <td><?php echo $role[$usr['user_type']]; ?></td>
                        <td><a href="#" class="confirm" id="<?php echo $usr['user_id']; ?>"><i class="fa fa-remove fa-fw text-danger"></i></a></td>
                      </tr>
                      <?php 
                          } 
                        } else { ?>
                      <tr>
                        <td colspan="3">No users to show.</td>
                      </tr>
                      <?php } ?>
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
        </form>
        <form method="post" id="frm-del-grp">
          <input type="hidden" name="delete_id" id="del-user-val" value="">
        </form>
        <!-- /Center Bar -->
        <script>
          function check(val){
            var doc = document.getElementById('action'+val).checked;

            if(doc == true) doc = 1;
            else doc = 0;
            window.location.href='user_manage.php?id='+val+'&status='+doc+'&cmd=update';
          }
          
          $(".confirm").confirm({
            text: "Are you sure you want to delete?",
            title: "Confirmation required",
            confirm: function(button) {
              $('#del-user-val').val($(button).attr('id'));
              $('#frm-del-grp').submit();
              alert('Are you Sure You want to delete: ' + id);
            },
            cancel: function(button) {
              // nothing to do
            },
            confirmButton: "Yes",
            cancelButton: "No",
            post: true,
            confirmButtonClass: "btn-danger"
          });
        </script>

      </div>
      <!-- /Content -->
    </section>
  </div>
  
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<?php
include('footer.php');
?>