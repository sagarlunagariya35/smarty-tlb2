<?php
require_once '../classes/constants.php';
$allowed_roles = array(ROLE_AAMIL_SAHEB,ROLE_COMMITTEE_MEMBER,ROLE_EDU_SEC,ROLE_HEAD_OFFICE,ROLE_COUNSELOR,ROLE_ASHARAH_ADMIN);
include 'classes/class.araiz.php';
require_once 'session.php';

$araiz = new Araiz();
$jamiyet_data = FALSE;

$total_araz = $araiz->get_total_araz_count();
$pending_araz = $araiz->get_total_pending_count();
$jawab_Sent_araz = $araiz->get_total_jawab_sent_count();
$already_done_araz = $araiz->get_total_already_done_araz_count();
$pending_araz_date = $araiz->get_first_pending_araz();
$jawab_sent_araz_date = $araiz->get_first_jawab_sent_araz();

$title = 'Home Page';
$description = '';
$keywords = '';
$active_page = "home";

include_once("header.php");

if ($jamiats) {
  foreach ($jamiats as $data) {
    $jamiat_name = $data['jamiat'];
    $jamiat_result = $araiz->get_araz_count_of_jamiat($jamiat_name);
    if($jamiat_result['count'] > 0){
      $ret[$jamiat_name] = $jamiat_result['count'];
    }
  }
}
?>

  <div class="row">
    <div class="col-lg-12">
      <h1 class="page-header">Dashboard</h1>
    </div>
    <!-- /.col-lg-12 -->
  </div>
  <!-- /.row -->
  <div class="row">
    <div class="col-lg-3 col-md-6">
      <div class="panel panel-red">
        <a target="_blank" href="araz_already_done.php">
          <div class="panel-heading">
            <div class="row">
              <div class="col-xs-3">
                <i class="fa fa-check-square-o fa-5x"></i>
              </div>
              <div class="col-xs-9 text-right">
                <div class="huge"><?php echo $already_done_araz; ?></div>
                <div></div>
              </div>
            </div>
          </div>
          <div class="panel-footer">
            <span class="pull-left">Already Done Araz!</span>
            <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
            <div class="clearfix"></div>
          </div>
        </a>
      </div>
    </div>
    <div class="col-lg-3 col-md-6">
      <a target="_blank" href="araiz_list.php">
        <div class="panel panel-primary">
          <div class="panel-heading">
            <div class="row">
              <div class="col-xs-3">
                <i class="fa fa-comments fa-5x"></i>
              </div>
              <div class="col-xs-9 text-right">
                <div class="huge"><?php echo $total_araz; ?></div>
                <div></div>
              </div>
            </div>
          </div>
          <div class="panel-footer">
            <span class="pull-left">Total Araz!</span>
            <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
            <div class="clearfix"></div>
          </div>
        </div>
      </a>
    </div>
    <div class="col-lg-3 col-md-6">
      <div class="panel panel-yellow">
        <a target="_blank" href="raza_araiz_date_wise.php">
          <div class="panel-heading">
            <div class="row">
              <div class="col-xs-3">
                <i class="fa fa-clock-o fa-5x"></i>
              </div>
              <div class="col-xs-9 text-right">
                <div class="huge"><?php echo $pending_araz; ?></div>
                <div></div>
              </div>
            </div>
          </div>
          <div class="panel-footer">
            <span class="pull-left">Pending Araz</span>
            <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
            <div class="clearfix"></div>
          </div>
        </a>
      </div>
    </div>
    <div class="col-lg-3 col-md-6">
      <div class="panel panel-green">
        <a target="_blank" href="list_jawab_sent_araiz.php">
          <div class="panel-heading">
            <div class="row">
              <div class="col-xs-3">
                <i class="fa fa-tasks fa-5x"></i>
              </div>
              <div class="col-xs-9 text-right">
                <div class="huge"><?php echo $jawab_Sent_araz; ?></div>
                <div></div>
              </div>
            </div>
          </div>
          <div class="panel-footer">
            <span class="pull-left">Jawab Sent</span>
            <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
            <div class="clearfix"></div>
          </div>
        </a>
      </div>
    </div>
  </div>
  <!-- /.row -->
  <div class="row">
    <div class="col-lg-8">
      <div class="panel panel-default">
        <div class="panel-heading">
          <i class="fa fa-bar-chart-o fa-fw"></i> Araz Trend Chart
        </div>
        <div class="panel-body">
          <div id="arz-trend-chart"></div>
        </div>
      </div>
    
      <div class="panel panel-default">
        <div class="panel-heading">
          <i class="fa fa-bar-chart-o fa-fw"></i> Jamiat Araz Chart
        </div>
        <div class="panel-body">
          <div id="jamiat-arz-chart"></div>
        </div>
      </div>
    </div>

    <div class="col-lg-4">
      <div class="panel panel-default">
        <div class="panel-heading">
          <i class="fa fa-bar-chart-o fa-fw"></i> Araz Status
        </div>
        <div class="panel-body">
          <h4><b>Total Araz : </b><?php echo $total_araz; ?></h4>
          <h5>Pending Araz Date : <?php echo $pending_araz_date; ?></h5>
          <h5>Jawab Sent Araz Date : <?php echo $jawab_sent_araz_date; ?></h5>
          <div id="arz-chart"></div>
        </div>
        <!-- /.panel-body -->
      </div>
    </div>
  </div>

<!-- /#page-wrapper -->
<script>

  $(function() {
    Morris.Donut({
      element: 'arz-chart',
      data: [{
          label: "Jawab Sent",
          value: <?php echo $jawab_Sent_araz; ?>,
        }, {
          label: "Pending Araz",
          value: <?php echo $pending_araz; ?>
        }],
      resize: true
    });

    Morris.Bar({
      element: 'arz-trend-chart',
      data: [{
          y: '<?php echo date('Y'); ?>',
          b: <?php echo $pending_araz; ?>,
          c: <?php echo $jawab_Sent_araz; ?>
        }],
      xkey: 'y',
      ykeys: ['b', 'c'],
      labels: ['Pending Araz', 'Jawab Sent'],
      hideHover: 'auto',
      resize: true
    });
    
    Morris.Bar({
      element: 'jamiat-arz-chart',
      data: [{
          y: '<?php echo date('Y'); ?>',
          <?php 
            if($ret){
              $i = 0;  
              foreach ($ret as $key=>$val) {
                $i++;
                echo $i.': '."'$val'";
                if (count($ret) - 1) {
                  echo ',';
                }
              }
            }
          ?>
        }],
      xkey: 'y',
      ykeys: [<?php 
                if($ret){
                  $i = 0;  
                  foreach ($ret as $key=>$val) {
                    $i++;
                    echo "'$i'";
                    if (count($ret) - 1) {
                      echo ',';
                    }
                  }
                }
              ?>],
      labels: [<?php 
                if($ret){
                  foreach ($ret as $key=>$val) {
                    echo "'$key'";
                    if (count($ret) - 1) {
                      echo ',';
                    }
                  }
                }
              ?>],
      hideHover: 'auto',
      resize: true
    });

  });

</script>
<?php include "footer.php"; ?>