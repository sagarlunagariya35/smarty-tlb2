<?php
require_once '../classes/class.database.php';
$allowed_roles = array(ROLE_HEAD_OFFICE);
require_once 'session.php';

$title = 'List of Tasavvuraat Araz';
$description = '';
$keywords = '';
$active_page = 'list_tasavvuraat_araz';

if(isset($_GET['cmd']) && $_GET['cmd'] == 'update') {
  $date = date('Y-m-d');
  $query = "UPDATE `tlb_tasavvuraat_araz` SET `araz_done` = '$_GET[status]', `user_id` = '$_SESSION[USER_ID]', `araz_done_ts` = '$date' WHERE `id` = '$_GET[id]'";
  $result = $db->query($query);
  if ($result) {
    $_SESSION[SUCCESS_MESSAGE] = 'Record updated successfully.';
    header('Location: list_tasavvuraat_araz.php');
    exit();
  } else {
    $_SESSION[ERROR_MESSAGE] = 'Sorry! try again.';
    header('Location: list_tasavvuraat_araz.php');
    exit();
  }
}

if(isset($_POST['submit'])){
  $remarks = $_POST['remarks'];
  $its_key = (array) $_POST['its_key'];
  $subject = $_POST['subject'];
  
  if($its_key){
    foreach ($its_key as $its){
      $result = sent_email_of_report('tlb_tasavvuraat_araz','its_id',$its,$remarks,$subject);
    }
    
    if($result){
      $_SESSION[SUCCESS_MESSAGE] = 'E-mail Sent Successfully..';
    }else {
      $_SESSION[ERROR_MESSAGE] = 'Error Encounter While Sending E-mail';
    }
  }else {
    $_SESSION[ERROR_MESSAGE] = 'Please Select any User for sent E-mail';
  }
}

$select = FALSE;
$select = get_all_tasavvuraat_araz();

include ('header.php');
?>

<div class="content-wrapper">
  <form method="post" role="form" class="form-horizontal">
    <section class="content">
      <div class="row">
        <div class="col-xs-12">&nbsp;</div>
      </div>
      <div class="row">
        <div class="col-xs-12 col-sm-2" style="padding-top:6px">
          <input type="checkbox" name="print" value="Print" id="check_all" onchange="checkAll(this);" class="css-checkbox1"><label for="check_all" class="css-label1">Check All</label>
        </div>
        <div class="col-xs-12 col-sm-3">
          <input type="text" name="subject" placeholder="Enter Subject" class="form-control">
        </div>
        <div class="col-xs-12 col-sm-5">
          <textarea type="text" class="form-control" name="remarks" placeholder="Enter Text"></textarea>
        </div>
        <div class="col-xs-12 col-sm-2">
          <input type="submit" name="submit" value="Send Email" class="btn btn-success btn-block assign-jawab">
        </div>
      </div>
      <div class="row">
        <div class="col-xs-12">&nbsp;</div>
      </div>
      <!-- Content -->
      <div class="row">
        <div class="col-md-12">
          <div class="panel panel-primary">
            <div class="panel-heading">
              <i class="fa fa-group fa-fw"></i> <?php echo $title; ?>
            </div>
            <!-- /.panel-heading -->
            <div class="panel-body">
              <div class="table-responsive">
                <table class="table table-striped table-hover table-condensed">
                  <thead>
                    <tr>
                      <th>Sr No.</th>
                      <th>ITS</th>
                      <th>Full Name</th>
                      <th>Audio</th>
                      <th>Text</th>
                      <th>Action</th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php 
                    if($select){
                      $i = 0; 
                      foreach($select as $data){
                        $i++;
                    ?>
                    <tr>
                      <td><input type="checkbox" id="jawab-<?php echo $i; ?>" name="its_key[]" class="jawab css-checkbox1" value="<?php echo $data['its_id']; ?>"><label for="jawab-<?php echo $i; ?>" class="css-label1"><?php echo $i; ?></label></td>
                      <td><?php echo $data['its_id']; ?></td>
                      <td><?php echo $data['first_name']. ' '. $data['last_name']; ?></a></td>
                      <td>
                        <audio controls>
                          <source src="<?php echo SERVER_PATH . 'upload/tasavvuraat_araz/' . $data['audio_file']; ?>">
                        </audio>
                      </td>
                      <td><?php echo substr($data['text'], 0, 50); ?>&nbsp;&nbsp;<?php if(strlen($data['text']) > 50) { ?><a href="#" data-toggle="modal" data-target="#myModal<?php echo $i; ?>">view</a><?php } ?></td>
                      <td class="text-center"><input type="checkbox" name="araz_done[]" id="araz_done<?php echo $data['id']; ?>" onclick="check(<?php echo $data['id']; ?>)" <?php echo ($data['araz_done'] == 1) ? 'checked' : ''; ?>></td>

                      <div class="modal fade" id="myModal<?php echo $i; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                        <div class="modal-dialog">
                          <div class="modal-content">
                            <div class="modal-header">
                              <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                              <h4 class="modal-title" id="myModalLabel">Tasavvuraat Araz Text</h4>
                            </div>
                            <div class="modal-body">
                              <?php echo $data['text']; ?>
                            </div>
                          </div>
                        </div>
                      </div>
                    </tr>
                    <?php 
                        } 
                      } else { ?>
                    <tr>
                      <td class="text-center" colspan="6">No Records..</td>
                    </tr>
                    <?php } ?>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
        <!-- /Center Bar -->
      </div>
      <!-- /Content -->
    </section>
  </form>
</div>
<script>
  function checkAll(ele) {
    if (ele.checked) {
      $('.jawab').prop('checked', true);
    } else {
      $('.jawab').prop('checked', false);
    }
  }
  
  function check(val){
    var doc = document.getElementById('araz_done'+val).checked;

    if(doc == true) doc = 1;
    else doc = 0;
    window.location.href='list_tasavvuraat_araz.php?id='+val+'&status='+doc+'&cmd=update';
  }
</script>  
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<?php
include('footer.php');
?>