<?php

require_once '../classes/class.database.php';

class Araiz {

  function get_total_araz_count($count_already_done = FALSE) {
    global $db;

    $clause = ($count_already_done) ? '' : ' WHERE `already_araz_done` = 0 ';
    $query = "SELECT COUNT(DISTINCT araz_id) cnt FROM `txn_view_araz` $clause";

    $result = $db->query_fetch_full_result($query);
    return $result[0]['cnt'];
  }

  function get_total_pending_count($count_already_done = FALSE) {
    global $db;

    $clause = ($count_already_done) ? '' : ' AND `already_araz_done` = 0 ';
    $query = "SELECT COUNT(DISTINCT araz_id) cnt FROM `txn_view_araz` WHERE `jawab_given` = '0' $clause";

    $result = $db->query_fetch_full_result($query);
    return $result[0]['cnt'];
  }

  function get_total_jawab_sent_count($count_already_done = FALSE) {
    global $db;

    $clause = ($count_already_done) ? '' : ' AND `already_araz_done` = 0 ';
    $query = "SELECT COUNT(DISTINCT araz_id) cnt FROM `txn_view_araz` WHERE `jawab_given` = '1' $clause";

    $result = $db->query_fetch_full_result($query);
    return $result[0]['cnt'];
  }

  function get_total_already_done_araz_count() {
    global $db;
    $query = "SELECT COUNT(DISTINCT araz_id) cnt FROM `txn_view_araz` WHERE `already_araz_done` = '1'";
    $result = $db->query_fetch_full_result($query);
    return $result[0]['cnt'];
  }

  function get_all_araz($jamat = FALSE, $page = FALSE, $num_count = FALSE, $jamiat = FALSE) {
    global $db;

    $query = "SELECT DISTINCT(araz_id) FROM `txn_view_araz` WHERE `araz_id` != '' ";

    ($jamat) ? $query .= "AND `jamaat` = '$jamat' " : '';

    ($jamiat !== FALSE) ? $query .= "AND `jamiat` IN ('$jamiat') " : '';

    $query .= "ORDER BY `araz_id` DESC";
    if ($page) {
      $limit = ($page - 1) * $num_count;
      $query .= " limit $limit,$num_count";
    }

    $result = $db->query_fetch_full_result($query);
    $ary_ids = array();
    foreach ($result as $row) {
      $ary_ids[] = $row['araz_id'];
    }

    $query = "SELECT * FROM `txn_view_araz` WHERE `araz_id` IN (" . join(', ', $ary_ids) . ")";
    $result = $db->query_fetch_full_result($query);
    return $result;
  }

  function get_all_araz_count($jamat = FALSE, $jamiat = FALSE) {
    global $db;
    $query = "SELECT COUNT(DISTINCT araz_id) cnt FROM `txn_view_araz` WHERE `araz_id` != '' ";

    ($jamat) ? $query .= "AND `jamaat` = '$jamat' " : '';

    ($jamiat !== FALSE) ? $query .= "AND `jamiat` IN ('$jamiat') " : '';

    $result = $db->query_fetch_full_result($query);
    return $result[0]['cnt'];
  }

  function get_first_pending_araz() {
    global $db;
    $query = "SELECT `araz_ts` FROM `tlb_araz`  WHERE `jawab_given` = '0' ORDER BY `id` ASC LIMIT 0,1";
    $result = $db->query_fetch_full_result($query);
    return $result[0]['araz_ts'];
  }

  function get_first_jawab_sent_araz() {
    global $db;
    $query = "SELECT `araz_ts` FROM `tlb_araz`  WHERE `jawab_given` = '1' ORDER BY `id` ASC LIMIT 0,1";
    $result = $db->query_fetch_full_result($query);
    return $result[0]['araz_ts'];
  }

  function get_pending_ho_raza_araiz($from_date = FALSE, $to_date = FALSE, $marhala = FALSE, $jamat = FALSE, $page = FALSE, $num_count = FALSE, $jamiat = FALSE) {
    global $db;

    $query = "SELECT * FROM `txn_view_araz` WHERE `jawab_given` = '0' AND `attend_later` = '0' AND `printed` = '0' AND saheb_id = 0 AND (counselor_id = 0 OR counselor_sugest_timestamp > 0) AND araz_type = 'raza' AND (course_started IS NULL OR STR_TO_DATE(course_started, '%M-%Y') > CURDATE()) ";

    ($from_date AND $to_date) ? $query .= "AND `araz_ts` BETWEEN '$from_date' AND '$to_date' " : '';

    ($marhala) ? $query .= "AND `marhala` = '$marhala' " : '';

    ($jamat) ? $query .= "AND `jamaat` = '$jamat' " : '';

    ($jamiat !== FALSE) ? $query .= "AND `jamiat` IN ('$jamiat') " : '';

    $query .= "ORDER BY `counselor_sugest_timestamp` DESC, `araz_id` DESC";
    if ($page == FALSE)
      $page = 1;
    if ($num_count == FALSE)
      $num_count = 50;
    if ($page) {
      $limit = ($page - 1) * $num_count;
      $query .= " limit $limit,$num_count";
    }

    $result = $db->query_fetch_full_result($query);

    return $result;
  }

  function get_pending_ho_raza_araz_count($from_date = FALSE, $to_date = FALSE, $marhala = FALSE, $jamat = FALSE, $jamiat = FALSE) {
    global $db;

    $query = "SELECT COUNT(*) as `count` FROM `txn_view_araz` WHERE `jawab_given` = '0' AND `attend_later` = '0' AND `printed` = '0' AND saheb_id = 0 AND (counselor_id = 0 OR counselor_sugest_timestamp > 0) AND araz_type = 'raza' AND (course_started IS NULL OR STR_TO_DATE(course_started, '%M-%Y') > CURDATE()) ";

    ($from_date AND $to_date) ? $query .= "AND `araz_ts` BETWEEN '$from_date' AND '$to_date' " : '';

    ($marhala) ? $query .= "AND `marhala` = '$marhala' " : '';

    ($jamat) ? $query .= "AND `jamaat` = '$jamat' " : '';

    ($jamiat !== FALSE) ? $query .= "AND `jamiat` IN ('$jamiat') " : '';

    $result = $db->query_fetch_full_result($query);
    return $result[0]['count'];
  }

  function get_pending_ho_raza_araiz_allready_started($from_date = FALSE, $to_date = FALSE, $marhala = FALSE, $jamat = FALSE, $page = FALSE, $num_count = FALSE, $jamiat = FALSE) {
    global $db;

    $query = "SELECT * FROM `txn_view_araz` WHERE `jawab_given` = '0' AND `attend_later` = '0' AND `printed` = '0' AND saheb_id = 0 AND (counselor_id = 0 OR counselor_sugest_timestamp > 0) AND araz_type = 'raza' AND STR_TO_DATE(course_started,'%M-%Y') < CURDATE() AND marhala > 4 ";

    ($from_date AND $to_date) ? $query .= "AND `araz_ts` BETWEEN '$from_date' AND '$to_date' " : '';

    ($marhala) ? $query .= "AND `marhala` = '$marhala' " : '';

    ($jamat) ? $query .= "AND `jamaat` = '$jamat' " : '';

    ($jamiat !== FALSE) ? $query .= "AND `jamiat` IN ('$jamiat') " : '';

    $query .= "ORDER BY `counselor_sugest_timestamp` DESC, `araz_id` DESC";
    if ($page == FALSE)
      $page = 1;
    if ($num_count == FALSE)
      $num_count = 50;
    if ($page) {
      $limit = ($page - 1) * $num_count;
      $query .= " limit $limit,$num_count";
    }

    $result = $db->query_fetch_full_result($query);

    return $result;
  }

  function get_raza_araz_already_started_count($from_date = FALSE, $to_date = FALSE, $marhala = FALSE, $jamat = FALSE, $jamiat = FALSE) {
    global $db;

    $query = "SELECT COUNT(*) as `count` FROM `txn_view_araz` WHERE `jawab_given` = '0' AND `attend_later` = '0' AND `printed` = '0' AND saheb_id = 0 AND (counselor_id = 0 OR counselor_sugest_timestamp > 0) AND araz_type = 'raza' AND STR_TO_DATE(course_started,'%M-%Y') < CURDATE() AND marhala > 4 ";

    ($from_date AND $to_date) ? $query .= "AND `araz_ts` BETWEEN '$from_date' AND '$to_date' " : '';

    ($marhala) ? $query .= "AND `marhala` = '$marhala' " : '';

    ($jamat) ? $query .= "AND `jamaat` = '$jamat' " : '';

    ($jamiat !== FALSE) ? $query .= "AND `jamiat` IN ('$jamiat') " : '';

    $result = $db->query_fetch_full_result($query);
    return $result[0]['count'];
  }

  function get_pending_ho_istirshad_araiz($from_date = FALSE, $to_date = FALSE, $marhala = FALSE, $jamat = FALSE, $page = FALSE, $num_count = FALSE, $jamiat = FALSE) {
    global $db;

    $limit = ($page - 1) * $num_count;
    // get the 10 araz ids of the pending araiz
    $query = "SELECT DISTINCT(araz_id) FROM `txn_view_araz` WHERE `jawab_given` = '0' AND `attend_later` = '0' AND `printed` = '0' AND saheb_id = 0 AND (counselor_id = 0 OR counselor_sugest_timestamp > 0) AND araz_type = 'istirshad' ";

    ($from_date AND $to_date) ? $query .= "AND `araz_ts` BETWEEN '$from_date' AND '$to_date' " : '';

    ($marhala) ? $query .= "AND `marhala` = '$marhala' " : '';

    ($jamat) ? $query .= "AND `jamaat` = '$jamat' " : '';

    ($jamiat !== FALSE) ? $query .= "AND `jamiat` IN ('$jamiat') " : '';

    $query .= "ORDER BY `counselor_sugest_timestamp` DESC, `araz_id` DESC";

    if ($page == FALSE)
      $page = 1;
    if ($num_count == FALSE)
      $num_count = 50;
    if ($page) {
      $limit = ($page - 1) * $num_count;
      $query .= " limit $limit,$num_count";
    }

    $result = $db->query_fetch_full_result($query);

    if ($result) {
      $ary_ids = array();
      foreach ($result as $row) {
        $ary_ids[] = $row['araz_id'];
      }

      $query = "SELECT * FROM `txn_view_araz` WHERE `araz_id` IN (" . join(', ', $ary_ids) . ")";
      $result = $db->query_fetch_full_result($query);
    } else {
      $result = FALSE;
    }

    return $result;
  }

  function get_pending_ho_istirshad_araz_count($from_date = FALSE, $to_date = FALSE, $marhala = FALSE, $jamat = FALSE, $jamiat = FALSE) {
    global $db;

    $query = "SELECT COUNT(DISTINCT araz_id) count FROM `txn_view_araz` WHERE `jawab_given` = '0' AND `attend_later` = '0' AND `printed` = '0' AND saheb_id = 0 AND (counselor_id = 0 OR counselor_sugest_timestamp > 0) AND araz_type = 'istirshad' ";

    ($from_date AND $to_date) ? $query .= "AND `araz_ts` BETWEEN '$from_date' AND '$to_date' " : '';

    ($marhala) ? $query .= "AND `marhala` = '$marhala' " : '';

    ($jamat) ? $query .= "AND `jamaat` = '$jamat' " : '';

    ($jamiat !== FALSE) ? $query .= "AND `jamiat` IN ('$jamiat') " : '';

    $result = $db->query_fetch_full_result($query);
    return $result[0]['count'];
  }

  function get_printed_araiz($from_date = FALSE, $to_date = FALSE, $marhala = FALSE, $jamat = FALSE, $page = FALSE, $num_count = FALSE, $jamiat = FALSE) {
    global $db;

    $query = "SELECT DISTINCT(araz_id) FROM `txn_view_araz` WHERE `jawab_given` = '0' AND `printed` = '1' ";

    ($from_date AND $to_date) ? $query .= "AND `araz_ts` BETWEEN '$from_date' AND '$to_date' " : '';

    ($marhala) ? $query .= "AND `marhala` = '$marhala' " : '';

    ($jamat) ? $query .= "AND `jamaat` = '$jamat' " : '';

    ($jamiat !== FALSE) ? $query .= "AND `jamiat` IN ('$jamiat') " : '';

    $query .= "ORDER BY `counselor_sugest_timestamp` DESC, `araz_id` DESC";
    if ($page) {
      $limit = ($page - 1) * $num_count;
      $query .= " limit $limit,$num_count";
    }

    $result = $db->query_fetch_full_result($query);
    
    if ($result) {
      $ary_ids = array();
      foreach ($result as $row) {
        $ary_ids[] = $row['araz_id'];
      }

      $query = "SELECT * FROM `txn_view_araz` WHERE `araz_id` IN (" . join(', ', $ary_ids) . ")";
      $result = $db->query_fetch_full_result($query);
    } else {
      $result = FALSE;
    }
    
    return $result;
  }

  function get_printed_araz_count($from_date = FALSE, $to_date = FALSE, $marhala = FALSE, $jamat = FALSE, $jamiat = FALSE) {
    global $db;

    $query = "SELECT COUNT(DISTINCT araz_id) count FROM `txn_view_araz` WHERE `jawab_given` = '0' AND `printed` = '1'";

    ($from_date AND $to_date) ? $query .= "AND `araz_ts` BETWEEN '$from_date' AND '$to_date' " : '';

    ($marhala) ? $query .= "AND `marhala` = '$marhala' " : '';

    ($jamat) ? $query .= "AND `jamaat` = '$jamat' " : '';

    ($jamiat !== FALSE) ? $query .= "AND `jamiat` IN ('$jamiat') " : '';

    $result = $db->query_fetch_full_result($query);
    return $result[0]['count'];
  }

  function get_attend_later_araiz($from_date = FALSE, $to_date = FALSE, $marhala = FALSE, $jamat = FALSE, $page = FALSE, $num_count = FALSE, $jamiat = FALSE) {
    global $db;

    $query = "SELECT DISTINCT(araz_id) FROM `txn_view_araz` WHERE `jawab_given` = '0' AND `attend_later` = '1' AND `printed` = '0' AND saheb_id = 0 AND (counselor_id = 0 OR counselor_sugest_timestamp > 0) ";

    ($from_date AND $to_date) ? $query .= "AND `araz_ts` BETWEEN '$from_date' AND '$to_date' " : '';

    ($marhala) ? $query .= "AND `marhala` = '$marhala' " : '';

    ($jamat) ? $query .= "AND `jamaat` = '$jamat' " : '';

    ($jamiat !== FALSE) ? $query .= "AND `jamiat` IN ('$jamiat') " : '';

    $query .= "ORDER BY `counselor_sugest_timestamp` DESC, `araz_id` DESC";
    if ($page) {
      $limit = ($page - 1) * $num_count;
      $query .= " limit $limit,$num_count";
    }

    $result = $db->query_fetch_full_result($query);
    
    if ($result) {
      $ary_ids = array();
      foreach ($result as $row) {
        $ary_ids[] = $row['araz_id'];
      }

      $query = "SELECT * FROM `txn_view_araz` WHERE `araz_id` IN (" . join(', ', $ary_ids) . ")";
      $result = $db->query_fetch_full_result($query);
    } else {
      $result = FALSE;
    }
    
    return $result;
  }

  function get_attend_later_araz_count($from_date = FALSE, $to_date = FALSE, $marhala = FALSE, $jamat = FALSE, $jamiat = FALSE) {
    global $db;

    $query = "SELECT COUNT(DISTINCT araz_id) count FROM `txn_view_araz` WHERE `jawab_given` = '0' AND `attend_later` = '1' AND `printed` = '0' AND saheb_id = 0 AND (counselor_id = 0 OR counselor_sugest_timestamp > 0) ";

    ($from_date AND $to_date) ? $query .= "AND `araz_ts` BETWEEN '$from_date' AND '$to_date' " : '';

    ($marhala) ? $query .= "AND `marhala` = '$marhala' " : '';

    ($jamat) ? $query .= "AND `jamaat` = '$jamat' " : '';

    ($jamiat !== FALSE) ? $query .= "AND `jamiat` IN ('$jamiat') " : '';

    $result = $db->query_fetch_full_result($query);
    return $result[0]['count'];
  }

  function get_pending_araiz_of_counselor($from_date = FALSE, $to_date = FALSE, $marhala = FALSE, $counselor_id = FALSE, $jamat = FALSE, $page = FALSE, $num_count = FALSE, $jamiat = FALSE) {
    global $db;

    $query = "SELECT DISTINCT(araz_id) FROM `txn_view_araz` WHERE `jawab_given` = '0' AND counselor_id != 0 AND counselor_sugest_timestamp IS NULL ";

    ($from_date AND $to_date) ? $query .= "AND `araz_ts` BETWEEN '$from_date' AND '$to_date' " : '';

    ($marhala) ? $query .= "AND `marhala` = '$marhala' " : '';

    ($jamat) ? $query .= "AND `jamaat` = '$jamat' " : '';

    ($jamiat !== FALSE) ? $query .= "AND `jamiat` IN ('$jamiat') " : '';

    $query .= "ORDER BY `araz_id` DESC";
    if ($page) {
      $limit = ($page - 1) * $num_count;
      $query .= " limit $limit,$num_count";
    }

    $result = $db->query_fetch_full_result($query);
    
    if ($result) {
      $ary_ids = array();
      foreach ($result as $row) {
        $ary_ids[] = $row['araz_id'];
      }

      $query = "SELECT * FROM `txn_view_araz` WHERE `araz_id` IN (" . join(', ', $ary_ids) . ")";
      $result = $db->query_fetch_full_result($query);
    } else {
      $result = FALSE;
    }
    
    return $result;
  }

  function get_pending_araiz_of_counselor_count($from_date = FALSE, $to_date = FALSE, $marhala = FALSE, $counselor_id = FALSE, $jamat = FALSE, $jamiat = FALSE) {
    global $db;

    $query = "SELECT COUNT(DISTINCT araz_id) count FROM `txn_view_araz` WHERE `jawab_given` = '0' AND counselor_id != 0 AND counselor_sugest_timestamp IS NULL ";

    ($from_date AND $to_date) ? $query .= "AND `araz_ts` BETWEEN '$from_date' AND '$to_date' " : '';

    ($marhala) ? $query .= "AND `marhala` = '$marhala' " : '';

    ($jamat) ? $query .= "AND `jamaat` = '$jamat' " : '';

    ($jamiat !== FALSE) ? $query .= "AND `jamiat` IN ('$jamiat') " : '';

    $result = $db->query_fetch_full_result($query);
    return $result[0]['count'];
  }

  function get_pending_araiz_of_saheb($from_date = FALSE, $to_date = FALSE, $marhala = FALSE, $saheb_id = FALSE, $jamat = FALSE, $page = FALSE, $num_count = FALSE, $jamiat = FALSE) {
    global $db;

    $query = "SELECT DISTINCT(araz_id) FROM `txn_view_araz` WHERE `jawab_given` = '0' AND saheb_id != 0 ";

    ($from_date AND $to_date) ? $query .= "AND `araz_ts` BETWEEN '$from_date' AND '$to_date' " : '';

    ($marhala) ? $query .= "AND `marhala` = '$marhala' " : '';

    ($jamat) ? $query .= "AND `jamaat` = '$jamat' " : '';

    ($jamiat !== FALSE) ? $query .= "AND `jamiat` IN ('$jamiat') " : '';

    $query .= "ORDER BY `araz_id` DESC";
    if ($page) {
      $limit = ($page - 1) * $num_count;
      $query .= " limit $limit,$num_count";
    }

    $result = $db->query_fetch_full_result($query);
    
    if ($result) {
      $ary_ids = array();
      foreach ($result as $row) {
        $ary_ids[] = $row['araz_id'];
      }

      $query = "SELECT * FROM `txn_view_araz` WHERE `araz_id` IN (" . join(', ', $ary_ids) . ")";
      $result = $db->query_fetch_full_result($query);
    } else {
      $result = FALSE;
    }
    
    return $result;
  }

  function get_pending_araiz_of_saheb_count($from_date = FALSE, $to_date = FALSE, $marhala = FALSE, $saheb_id = FALSE, $jamat = FALSE, $jamiat = FALSE) {
    global $db;

    $query = "SELECT COUNT(DISTINCT araz_id) count FROM `txn_view_araz` WHERE `jawab_given` = '0' AND saheb_id != 0 ";

    ($from_date AND $to_date) ? $query .= "AND `araz_ts` BETWEEN '$from_date' AND '$to_date' " : '';

    ($marhala) ? $query .= "AND `marhala` = '$marhala' " : '';

    ($jamat) ? $query .= "AND `jamaat` = '$jamat' " : '';

    ($jamiat !== FALSE) ? $query .= "AND `jamiat` IN ('$jamiat') " : '';

    $result = $db->query_fetch_full_result($query);
    return $result[0]['count'];
  }

  function get_all_jawab_sent_araiz($jamat = FALSE, $page = FALSE, $num_count = FALSE, $jamiat = FALSE) {
    global $db;
    $limit = ($page - 1) * $num_count;
    $query = "SELECT DISTINCT(araz_id) FROM `txn_view_araz` WHERE `jawab_given` = '1' ";

    ($jamat) ? $query .= "AND `jamaat` = '$jamat' " : '';

    ($jamiat !== FALSE) ? $query .= "AND `jamiat` IN ('$jamiat') " : '';

    $query .= "ORDER BY `araz_id` DESC";
    if ($page) {
      $query .= " limit $limit,$num_count";
    }

    $result = $db->query_fetch_full_result($query);
    
    if ($result) {
      $ary_ids = array();
      foreach ($result as $row) {
        $ary_ids[] = $row['araz_id'];
      }

      $query = "SELECT * FROM `txn_view_araz` WHERE `araz_id` IN (" . join(', ', $ary_ids) . ")";
      $result = $db->query_fetch_full_result($query);
    } else {
      $result = FALSE;
    }
    
    return $result;
  }

  function get_jawab_sent_araiz_count($jamat = FALSE, $jamiat = FALSE) {
    global $db;

    $query = "SELECT COUNT(DISTINCT araz_id) count FROM `txn_view_araz` WHERE `jawab_given` = '1' ";

    ($jamat) ? $query .= "AND `jamaat` = '$jamat' " : '';

    ($jamiat !== FALSE) ? $query .= "AND `jamiat` IN ('$jamiat') " : '';

    $result = $db->query_fetch_full_result($query);
    return $result[0]['count'];
  }

  function get_all_already_done_araiz($jamat = FALSE, $page = FALSE, $num_count = FALSE, $jamiat = FALSE) {
    global $db;

    $limit = ($page - 1) * $num_count;
    $query = "SELECT DISTINCT(araz_id) FROM `txn_view_araz` WHERE `already_araz_done` = '1' ";

    ($jamat) ? $query .= "AND `jamaat` = '$jamat' " : '';

    ($jamiat !== FALSE) ? $query .= "AND `jamiat` IN ('$jamiat') " : '';

    $query .= "ORDER BY `araz_id` DESC";
    if ($page) {
      $query .= " limit $limit,$num_count";
    }

    $result = $db->query_fetch_full_result($query);
    
    if ($result) {
      $ary_ids = array();
      foreach ($result as $row) {
        $ary_ids[] = $row['araz_id'];
      }

      $query = "SELECT * FROM `txn_view_araz` WHERE `araz_id` IN (" . join(', ', $ary_ids) . ")";
      $result = $db->query_fetch_full_result($query);
    } else {
      $result = FALSE;
    }
    
    return $result;
  }

  function get_already_done_araiz_count($jamat = FALSE, $jamiat = FALSE) {
    global $db;

    $query = "SELECT COUNT(DISTINCT araz_id) count FROM `txn_view_araz` WHERE `already_araz_done` = '1' ";

    ($jamat) ? $query .= "AND `jamaat` = '$jamat' " : '';

    ($jamiat !== FALSE) ? $query .= "AND `jamiat` IN ('$jamiat') " : '';

    $result = $db->query_fetch_full_result($query);
    return $result[0]['count'];
  }

  function get_jawab_sent_araz($jamat = FALSE, $page = FALSE, $num_count = FALSE, $jamiat = FALSE) {
    global $db;
    $limit = ($page - 1) * $num_count;
    $query = "SELECT * FROM `txn_view_araz` WHERE `jawab_given` = '1'";

    ($jamat) ? $query .= "AND `jamaat` = '$jamat' " : '';

    ($jamiat) ? $query .= "AND `jamiat` IN ('$jamiat') " : '';

    $query .= "ORDER BY `araz_id` DESC";
    if ($page) {
      $query .= " limit $limit,$num_count";
    }

    $result = $db->query_fetch_full_result($query);
    return $result;
  }

  function count_total_araz($tag = FALSE, $jamat = FALSE) {
    global $db;
    $query = "SELECT COUNT(*) as count FROM `txn_view_araz`";

    if ($tag && $jamat) {
      $query .= " WHERE ";
    } else if ($tag || $jamat) {
      $query .= " WHERE ";
    }

    if ($jamat) {
      $query .= "`jamaat` = '$jamat'";
    }

    if ($tag && $jamat) {
      $query .= " &&";
    }

    if ($tag) {
      if ($tag == 'pending') {
        $query .= " `jawab_given` = '0'";
      } else if ($tag == 'jawab_sent') {
        $query .= " `jawab_given` = '1'";
      }
    }

    $result = $db->query_fetch_full_result($query);
    return $result[0]['count'];
  }

  function get_raza_araz_datewise($from_date, $to_date, $jamat = FALSE, $page = FALSE, $num_count = FALSE) {
    global $db;
    $limit = ($page - 1) * $num_count;
    $query = "SELECT * FROM `txn_view_araz` WHERE `araz_ts` BETWEEN '$from_date' AND '$to_date' && `araz_type` = 'raza'";

    if ($jamat) {
      $query .= " && `jamaat` = '$jamat' ";
    }

    $query .= "ORDER BY `araz_id` DESC";
    if ($page) {
      $query .= " limit $limit,$num_count";
    }

    $result = $db->query_fetch_full_result($query);
    return $result;
  }

  function get_araz_by_track_id($track_id) {
    global $db;

    foreach ($track_id as $val) {
      if ($val != '') {
        $search_query[] = " `araz_id` = '$val' ";
      }
    }
    $search_query = implode(' OR ', $search_query);

    $query = "SELECT * FROM `txn_view_araz` WHERE `jawab_given` = '0' AND $search_query ORDER BY `araz_id` DESC";

    $result = $db->query_fetch_full_result($query);
    return $result;
  }

  function print_all_selected_araz($track_id) {
    global $db;

    foreach ($track_id as $val) {
      if ($val != '') {
        $search_query[] = "$val";
      }
    }
    $search_query = implode(',', $search_query);

    $query = "UPDATE `tlb_araz` SET `printed`='1' WHERE `jawab_given` = '0' AND id IN ($search_query)";
    $result = $db->query($query);

    $query = "UPDATE `txn_view_araz` SET `printed`='1' WHERE `jawab_given` = '0' AND araz_id IN ($search_query)";
    $result = $db->query($query);
    return $result;
  }

  function print_all_selected_araz_by_date_wise($from_date, $to_date) {
    global $db;
    $query = "UPDATE `tlb_araz` SET `printed`='1' WHERE `araz_ts` BETWEEN '$from_date' AND '$to_date'";
    $result = $db->query($query);

    $query = "UPDATE `txn_view_araz` SET `printed`='1' WHERE `araz_ts` BETWEEN '$from_date' AND '$to_date'";
    $result = $db->query($query);
    return $result;
  }

  function get_istirshad_araz_datewise($from_date, $to_date, $jamat = FALSE, $page = FALSE, $num_count = FALSE) {
    global $db;
    $limit = ($page - 1) * $num_count;
    $query = "SELECT * FROM `txn_view_araz` WHERE `araz_ts` BETWEEN '$from_date' AND '$to_date' && `araz_type` = 'istirshad'";

    if ($jamat) {
      $query .= " && `jamaat` = '$jamat' ";
    }

    $query .= "ORDER BY `araz_id` DESC";
    if ($page) {
      $query .= " limit $limit,$num_count";
    }

    $result = $db->query_fetch_full_result($query);
    return $result;
  }

  function clear_araz_data($araz_id) {
    global $db;
    $query = "UPDATE `tlb_araz` SET `jawab_given` = '0', `jawab_city` = '', `code` = '', `jawab_ts` = '' WHERE `id` LIKE $araz_id";

    $result = $db->query($query);

    $query = "UPDATE `txn_view_araz` SET `jawab_given` = '0', `jawab_city` = '', `code` = '', `jawab_ts` = '' WHERE `araz_id` LIKE $araz_id";

    $result = $db->query($query);
    return $result;
  }

  function get_araz_preview($araz_id = FALSE, $jamiat = FALSE) {
    global $db;
    $query = "SELECT * FROM `txn_view_araz` WHERE `jawab_given` = '1' AND `code` != '' AND `email_sent_student` = '0'";

    ($araz_id) ? $query .= " AND `araz_id` > '$araz_id' " : '';

    ($jamiat !== FALSE) ? $query .= " AND `jamiat` IN ('$jamiat') " : '';

    $query .= " GROUP BY `araz_id` ORDER BY `araz_id` ASC limit 0,10";

    $result = $db->query_fetch_full_result($query);
    return $result;
  }

  function get_total_araz_preview($jamiat = FALSE) {
    global $db;
    $query = "SELECT COUNT(DISTINCT araz_id) as count FROM `txn_view_araz` WHERE `jawab_given` = '1' AND `code` != '' AND `email_sent_student` = '0'";

    ($jamiat !== FALSE) ? $query .= "AND `jamiat` IN ('$jamiat') " : '';

    $result = $db->query_fetch_full_result($query);
    return $result[0]['count'];
  }

  function get_multiple_data_search_araz($track_no, $its_id, $marhala, $darajah, $madrasah_name, $user_full_name, $school_city, $school_standard, $school_name, $filter_school, $from_date, $to_date, $stream, $course_name, $institute_name, $institute_city, $jamaat, $araz_type, $araz_status, $jamiat) {

    global $db;
    $query = "SELECT * FROM `txn_view_araz` WHERE `araz_id` != ''";

    ($track_no) ? $query .= " AND `araz_id`='$track_no'" : '';
    ($its_id) ? $query .= " AND `login_its`='$its_id'" : '';
    ($marhala) ? $query .= " AND `marhala`='$marhala'" : '';
    ($darajah) ? $query .= " AND `madrasah_darajah`='$darajah'" : '';
    ($madrasah_name) ? $query .= " AND `madrasah_name` LIKE '%$madrasah_name%'" : '';
    ($user_full_name) ? $query .= " AND `user_full_name` LIKE '%$user_full_name%'" : '';
    ($school_city) ? $query .= " AND `school_city` LIKE '%$school_city%'" : '';
    ($school_standard) ? $query .= " AND `school_standard`='$school_standard'" : '';
    ($school_name) ? $query .= " AND `school_name` LIKE '%$school_name%'" : '';
    ($filter_school) ? $query .= " AND `school_filter`='$filter_school'" : '';
    ($from_date || $to_date) ? $query .= " AND `araz_ts` BETWEEN '$from_date' AND '$to_date'" : '';
    ($stream) ? $query .= " AND `stream` LIKE '%$stream%'" : '';
    ($course_name) ? $query .= " AND `course_name` LIKE '%$course_name%'" : '';
    ($institute_name) ? $query .= " AND `institute_name` LIKE '%$institute_name%'" : '';
    ($institute_city) ? $query .= " AND `institute_city` LIKE '%$institute_city%'" : '';
    ($jamaat) ? $query .= " AND `jamaat` LIKE '%$jamaat%'" : '';
    ($araz_type) ? $query .= " AND `araz_type`='$araz_type'" : '';
    ($araz_status != '') ? $query .= " AND `jawab_given`='$araz_status'" : '';
    ($jamiat !== FALSE) ? $query .= "AND `jamiat` IN ('$jamiat') " : '';

    $query .= "ORDER BY `counselor_sugest_timestamp` DESC, `araz_id` DESC";

    $result = $db->query_fetch_full_result($query);
    return $result;
  }

  function get_araz_data_by_araz_id($araz_id) {
    global $db;
    $query = "SELECT * FROM `txn_view_araz` WHERE `araz_id` = '$araz_id'";
    $result = $db->query_fetch_full_result($query);
    return $result;
  }

  function get_all_assign_araz_to_counselor($user_id, $page = false, $num_count = false) {
    global $db;
    $limit = ($page - 1) * $num_count;
    $query = "SELECT * FROM `txn_view_araz` WHERE `counselor_id` = '$user_id' AND counselor_sugest_timestamp IS NULL ORDER BY `araz_id` DESC";

    if ($page) {
      $query .= " limit $limit,$num_count";
    }

    $result = $db->query_fetch_full_result($query);
    return $result;
  }

  function count_total_assign_araz_to_counselor($user_id) {
    global $db;
    $query = "SELECT COUNT(DISTINCT araz_id) as count FROM `txn_view_araz` WHERE `counselor_id` = '$user_id' AND counselor_sugest_timestamp IS NULL";
    $result = $db->query_fetch_full_result($query);
    return $result[0]['count'];
  }

  function count_total_recommend_araz_to_counselor($user_id) {
    global $db;
    $query = "SELECT COUNT(DISTINCT araz_id) as count FROM `tlb_saheb_recommend_counselor` WHERE `counselor_id` = '$user_id'";
    $result = $db->query_fetch_full_result($query);
    return $result[0]['count'];
  }

  function get_all_suggestion_sent_araz_of_counselor($user_id, $page = false, $num_count = false) {
    global $db;
    $limit = ($page - 1) * $num_count;
    $query = "SELECT * FROM `txn_view_araz` WHERE `counselor_id` = '$user_id' AND counselor_sugest_timestamp IS NOT NULL ORDER BY `araz_id` DESC";

    if ($page) {
      $query .= " limit $limit,$num_count";
    }

    $result = $db->query_fetch_full_result($query);
    return $result;
  }

  function count_total_suggestion_sent_araz_of_counselor($user_id) {
    global $db;
    $query = "SELECT COUNT(DISTINCT araz_id) as count FROM `txn_view_araz` WHERE `counselor_id` = '$user_id' AND counselor_sugest_timestamp IS NOT NULL";
    $result = $db->query_fetch_full_result($query);
    return $result[0]['count'];
  }

  function get_all_assign_araz_to_saheb($from_date = FALSE, $to_date = FALSE, $marhala = FALSE, $user_id, $page = false, $num_count = false) {
    global $db;
    $limit = ($page - 1) * $num_count;
    $query = "SELECT * FROM `txn_view_araz` WHERE `saheb_id` = '$user_id' AND `jawab_given` = '0' ";

    ($from_date AND $to_date) ? $query .= "AND `araz_ts` BETWEEN '$from_date' AND '$to_date' " : '';

    ($marhala) ? $query .= "AND `marhala` = '$marhala' " : '';

    $query .= "ORDER BY `araz_id` DESC ";
    if ($page) {
      $query .= "limit $limit,$num_count";
    }
    $result = $db->query_fetch_full_result($query);
    return $result;
  }

  function count_total_assign_araz_to_saheb($from_date = FALSE, $to_date = FALSE, $marhala = FALSE, $user_id) {
    global $db;
    $query = "SELECT COUNT(DISTINCT araz_id) as count FROM `txn_view_araz` WHERE `saheb_id` = '$user_id' AND `jawab_given` = '0' ";

    ($from_date AND $to_date) ? $query .= "AND `araz_ts` BETWEEN '$from_date' AND '$to_date' " : '';

    ($marhala) ? $query .= "AND `marhala` = '$marhala' " : '';

    $result = $db->query_fetch_full_result($query);
    return $result[0]['count'];
  }

  function get_all_jawab_sent_araz_of_saheb($user_id, $page = false, $num_count = false) {
    global $db;
    $limit = ($page - 1) * $num_count;
    $query = "SELECT * FROM `txn_view_araz` WHERE `saheb_id` = '$user_id' AND `jawab_given` = '1' ORDER BY `araz_id` DESC";

    if ($page) {
      $query .= " limit $limit,$num_count";
    }
    $result = $db->query_fetch_full_result($query);
    return $result;
  }

  function count_total_jawab_sent_araz_of_saheb($user_id) {
    global $db;
    $query = "SELECT COUNT(DISTINCT araz_id) as count FROM `txn_view_araz` WHERE `saheb_id` = '$user_id' AND `jawab_given` = '1'";
    $result = $db->query_fetch_full_result($query);
    return $result[0]['count'];
  }

  function get_all_assign_araz_to_reviewer($page = false, $num_count = false) {
    global $db;
    $limit = ($page - 1) * $num_count;
    $query = "SELECT * FROM `txn_view_araz` WHERE `disclaimer` = '1' AND reviewer_processed = '0' ORDER BY `araz_id` DESC";

    if ($page) {
      $query .= " limit $limit,$num_count";
    }

    $result = $db->query_fetch_full_result($query);
    return $result;
  }

  function count_total_assign_araz_to_reviewer() {
    global $db;
    $query = "SELECT COUNT(DISTINCT araz_id) as count FROM `txn_view_araz` WHERE `disclaimer` = '1' AND reviewer_processed = '0'";
    $result = $db->query_fetch_full_result($query);
    return $result[0]['count'];
  }

  function get_all_assign_araz_to_reviewer_history($page = false, $num_count = false) {
    global $db;
    $limit = ($page - 1) * $num_count;
    $query = "SELECT * FROM `txn_view_araz` WHERE `disclaimer` = '1' AND reviewer_processed = '1' ORDER BY `araz_id` DESC";

    if ($page) {
      $query .= " limit $limit,$num_count";
    }

    $result = $db->query_fetch_full_result($query);
    return $result;
  }

  function count_total_assign_araz_to_reviewer_history() {
    global $db;
    $query = "SELECT COUNT(DISTINCT araz_id) as count FROM `txn_view_araz` WHERE `disclaimer` = '1' AND reviewer_processed = '1'";
    $result = $db->query_fetch_full_result($query);
    return $result[0]['count'];
  }

  function get_all_assign_araz_to_project_coordinator($page = false, $num_count = false) {
    global $db;
    $limit = ($page - 1) * $num_count;
    $query = "SELECT * FROM `txn_view_araz` WHERE `reviewer_processed` = '1' AND `test_file_upload` = '' ORDER BY `araz_id` DESC";

    if ($page) {
      $query .= " limit $limit,$num_count";
    }

    $result = $db->query_fetch_full_result($query);
    return $result;
  }

  function count_total_assign_araz_to_project_coordinator() {
    global $db;
    $query = "SELECT COUNT(DISTINCT araz_id) as count FROM `txn_view_araz` WHERE `reviewer_processed` = '1' AND `test_file_upload` = ''";
    $result = $db->query_fetch_full_result($query);
    return $result[0]['count'];
  }

  function get_all_assign_araz_to_psychometric_test($page = false, $num_count = false) {
    global $db;
    $limit = ($page - 1) * $num_count;
    $query = "SELECT * FROM `txn_view_araz` WHERE reviewer_processed = '1' AND `recommend_psychometric_test` = '1' ORDER BY `araz_id` DESC";

    if ($page) {
      $query .= " limit $limit,$num_count";
    }

    $result = $db->query_fetch_full_result($query);
    return $result;
  }

  function count_total_assign_araz_to_psychometric_test() {
    global $db;
    $query = "SELECT COUNT(DISTINCT araz_id) as count FROM `txn_view_araz` WHERE reviewer_processed = '1' AND `recommend_psychometric_test` = '1'";
    $result = $db->query_fetch_full_result($query);
    return $result[0]['count'];
  }

  function get_all_psychometric_test_taken($page = false, $num_count = false) {
    global $db;
    $limit = ($page - 1) * $num_count;
    $query = "SELECT * FROM `txn_view_araz` WHERE reviewer_processed = '1' AND `recommend_psychometric_test` = '1' AND `test_file_upload` != '' ORDER BY `araz_id` DESC";

    if ($page) {
      $query .= " limit $limit,$num_count";
    }

    $result = $db->query_fetch_full_result($query);
    return $result;
  }

  function count_total_psychometric_test_taken() {
    global $db;
    $query = "SELECT COUNT(DISTINCT araz_id) as count FROM `txn_view_araz` WHERE reviewer_processed = '1' AND `recommend_psychometric_test` = '1' AND `test_file_upload` != ''";
    $result = $db->query_fetch_full_result($query);
    return $result[0]['count'];
  }

  function get_all_assign_araz_to_counseling_test($page = false, $num_count = false) {
    global $db;
    $limit = ($page - 1) * $num_count;
    $query = "SELECT * FROM `txn_view_araz` WHERE reviewer_processed = '1' AND `recommend_counseling` = '1' ORDER BY `araz_id` DESC";

    if ($page) {
      $query .= " limit $limit,$num_count";
    }

    $result = $db->query_fetch_full_result($query);
    return $result;
  }

  function count_total_assign_araz_to_counseling_test() {
    global $db;
    $query = "SELECT COUNT(DISTINCT araz_id) as count FROM `txn_view_araz` WHERE reviewer_processed = '1' AND `recommend_counseling` = '1'";
    $result = $db->query_fetch_full_result($query);
    return $result[0]['count'];
  }

  function get_all_counseling_test_done($page = false, $num_count = false) {
    global $db;
    $limit = ($page - 1) * $num_count;
    $query = "SELECT * FROM `txn_view_araz` WHERE reviewer_processed = '1' AND `recommend_counseling` = '1' AND `counselling_done` = '1' ORDER BY `araz_id` DESC";

    if ($page) {
      $query .= " limit $limit,$num_count";
    }

    $result = $db->query_fetch_full_result($query);
    return $result;
  }

  function count_total_counseling_done_test() {
    global $db;
    $query = "SELECT COUNT(DISTINCT araz_id) as count FROM `txn_view_araz` WHERE reviewer_processed = '1' AND `recommend_counseling` = '1' AND `counselling_done` = '1'";
    $result = $db->query_fetch_full_result($query);
    return $result[0]['count'];
  }

  function get_all_assign_araz_to_qardan_hasana($page = false, $num_count = false) {
    global $db;
    $limit = ($page - 1) * $num_count;
    $query = "SELECT * FROM `txn_view_araz` WHERE reviewer_processed = '1' AND `recommend_qardan` = '1' ORDER BY `araz_id` DESC";

    if ($page) {
      $query .= " limit $limit,$num_count";
    }

    $result = $db->query_fetch_full_result($query);
    return $result;
  }

  function count_total_assign_araz_to_qardan_hasana() {
    global $db;
    $query = "SELECT COUNT(DISTINCT araz_id) as count FROM `txn_view_araz` WHERE reviewer_processed = '1' AND `recommend_qardan` = '1'";
    $result = $db->query_fetch_full_result($query);
    return $result[0]['count'];
  }

  function get_all_qardan_hasana_done($page = false, $num_count = false) {
    global $db;
    $limit = ($page - 1) * $num_count;
    $query = "SELECT * FROM `txn_view_araz` WHERE reviewer_processed = '1' AND `recommend_qardan` = '1' AND `qardan_hasana_done` = '1' ORDER BY `araz_id` DESC";

    if ($page) {
      $query .= " limit $limit,$num_count";
    }

    $result = $db->query_fetch_full_result($query);
    return $result;
  }

  function count_total_qardan_hasana_done() {
    global $db;
    $query = "SELECT COUNT(DISTINCT araz_id) as count FROM `txn_view_araz` WHERE reviewer_processed = '1' AND `recommend_qardan` = '1' AND `qardan_hasana_done` = '1'";
    $result = $db->query_fetch_full_result($query);
    return $result[0]['count'];
  }

  function get_all_assign_araz_to_sponsorship($page = false, $num_count = false) {
    global $db;
    $limit = ($page - 1) * $num_count;
    $query = "SELECT * FROM `txn_view_araz` WHERE reviewer_processed = '1' AND `recommend_sponsorship` = '1' ORDER BY `araz_id` DESC";

    if ($page) {
      $query .= " limit $limit,$num_count";
    }

    $result = $db->query_fetch_full_result($query);
    return $result;
  }

  function count_total_assign_araz_to_sponsorship() {
    global $db;
    $query = "SELECT COUNT(DISTINCT araz_id) as count FROM `txn_view_araz` WHERE reviewer_processed = '1' AND `recommend_sponsorship` = '1'";
    $result = $db->query_fetch_full_result($query);
    return $result[0]['count'];
  }

  function get_all_sponsorship_done($page = false, $num_count = false) {
    global $db;
    $limit = ($page - 1) * $num_count;
    $query = "SELECT * FROM `txn_view_araz` WHERE reviewer_processed = '1' AND `recommend_sponsorship` = '1' AND `sponsorship_done` = '1' ORDER BY `araz_id` DESC";

    if ($page) {
      $query .= " limit $limit,$num_count";
    }

    $result = $db->query_fetch_full_result($query);
    return $result;
  }

  function count_total_sponsorship_done() {
    global $db;
    $query = "SELECT COUNT(DISTINCT araz_id) as count FROM `txn_view_araz` WHERE reviewer_processed = '1' AND `recommend_sponsorship` = '1' AND `sponsorship_done` = '1'";
    $result = $db->query_fetch_full_result($query);
    return $result[0]['count'];
  }

  function get_all_recommend_to_counselor_araz($page = false, $num_count = false, $jamiat = FALSE) {
    global $db;
    $limit = ($page - 1) * $num_count;
    $query = "SELECT DISTINCT(araz_id) FROM `txn_view_araz` va JOIN `tlb_saheb_recommend_counselor` tsrc ON va.`araz_id` = tsrc.`araz_id` WHERE tsrc.`ho_assign_ts` IS NULL ";

    ($jamiat !== FALSE) ? $query .= "AND `jamiat` IN ('$jamiat') " : '';

    $query .= "ORDER BY va.`araz_id` DESC";
    if ($page) {
      $query .= " limit $limit,$num_count";
    }

    $result = $db->query_fetch_full_result($query);
    $ary_ids = array();
    foreach ($result as $row) {
      $ary_ids[] = $row['araz_id'];
    }

    $query = "SELECT * FROM `txn_view_araz` WHERE `araz_id` IN (" . join(', ', $ary_ids) . ")";
    $result = $db->query_fetch_full_result($query);
    return $result;
  }

  function count_total_recommend_to_counselor_araz($jamiat = FALSE) {
    global $db;
    $query = "SELECT COUNT(DISTINCT va.`araz_id`) as count FROM `txn_view_araz` va JOIN `tlb_saheb_recommend_counselor` tsrc ON va.`araz_id` = tsrc.`araz_id` WHERE tsrc.`ho_assign_ts` IS NULL ";

    ($jamiat !== FALSE) ? $query .= "AND `jamiat` IN ('$jamiat') " : '';

    $result = $db->query_fetch_full_result($query);
    return $result[0]['count'];
  }

  function delete_araz($id) {
    global $db;
    $tables = array('tlb_araz', 'tlb_araz_courses', 'tlb_araz_questionaire_answers', 'tlb_counselor_suggestion', 'txn_view_araz');

    foreach ($tables as $table) {
      $araz_id = ($table == 'tlb_araz') ? 'id' : 'araz_id';

      $query = "SELECT * FROM `$table` WHERE `$araz_id` LIKE '$id'";
      $result = $db->query_fetch_full_result($query);

      if ($result) {
        $qry = "DELETE FROM `$table` WHERE `$araz_id` LIKE '$id'";
        $rslt = $db->query($qry);
      }
    }
    return $rslt;
  }

  function delete_araz_course($id) {
    global $db;

    // get the araz id for later use
    $qry_course_count = "SELECT COUNT(*) cnt, araz_id FROM `txn_view_araz` WHERE `araz_id` LIKE (SELECT araz_id FROM txn_view_araz WHERE araz_course_id LIKE '$id')";
    $rslt_cnt = $db->query_fetch_full_result($qry_course_count);
    $cnt = ($rslt_cnt) ? $rslt_cnt['cnt'] : FALSE;
    $araz_id = ($rslt_cnt) ? $rslt_cnt['araz_id'] : FALSE;

    $qry = "DELETE FROM `tlb_araz_courses` WHERE `id` LIKE '$id'";
    $rslt = $db->query($qry);

    if ($rslt) {
      $qry_txn = "DELETE FROM `txn_view_araz` WHERE `araz_course_id` LIKE '$id'";
      $rslt = $db->query($qry_txn);

      // if the count of courses in the araz now are less then 2 then convert the araz to raza
      if ($cnt < 3) {
        $qry_update = "UPDATE `txn_view_araz` SET `araz_type` = 'raza' WHERE `araz_id` = '$araz_id'";
        $rslt = $db->query($qry_update);
      }
    } else {
      return $rslt;
    }
    return $rslt;
  }

  function remove_print_araz($track_id) {
    global $db;

    foreach ($track_id as $val) {
      if ($val != '') {
        $search_query[] = "$val";
      }
    }
    $search_query = implode(',', $search_query);

    $query = "UPDATE `tlb_araz` SET `printed`='0' WHERE `jawab_given` = '0' AND id IN ($search_query)";
    $result = $db->query($query);

    $query = "UPDATE `txn_view_araz` SET `printed`='0' WHERE `jawab_given` = '0' AND araz_id IN ($search_query)";
    $result = $db->query($query);
    return $result;
  }

  function clear_assign_counselor_data($track_id) {
    global $db;

    foreach ($track_id as $val) {
      if ($val != '') {
        $search_query[] = "$val";
      }
    }
    $search_query = implode(',', $search_query);

    $query = "UPDATE `tlb_araz` SET `counselor_id` = 0, `coun_assign_ts` = '', `ho_remarks_counselor` = '' WHERE `id` IN ($search_query)";
    $result = $db->query($query);

    $query = "UPDATE `txn_view_araz` SET `counselor_id` = 0, `coun_assign_ts` = '', `ho_remarks_counselor` = '' WHERE `araz_id` IN ($search_query)";
    $result = $db->query($query);
    return $result;
  }

  function clear_assign_saheb_data($track_id) {
    global $db;

    foreach ($track_id as $val) {
      if ($val != '') {
        $search_query[] = "$val";
      }
    }
    $search_query = implode(',', $search_query);

    $query = "UPDATE `tlb_araz` SET `saheb_id` = 0, `saheb_assign_ts` = '', `ho_remarks_saheb` = '' WHERE `id` IN ($search_query)";
    $result = $db->query($query);

    $query = "UPDATE `txn_view_araz` SET `saheb_id` = 0, `saheb_assign_ts` = '', `ho_remarks_saheb` = '' WHERE `araz_id` IN ($search_query)";
    $result = $db->query($query);
    return $result;
  }

  function get_pending_marhala_araiz($marhala, $page = FALSE, $num_count = FALSE) {
    global $db;

    $query = "SELECT * FROM `txn_view_araz` WHERE `marhala` = '$marhala' AND `jawab_given` = '0' AND `printed` = '0' AND saheb_id = 0 AND (counselor_id = 0 OR counselor_sugest_timestamp > 0) ORDER BY `araz_id` DESC";
    if ($page) {
      $query .= " limit $limit,$num_count";
    }

    $result = $db->query_fetch_full_result($query);
    return $result;
  }

  function get_pending_marhala_araz_count($marhala) {
    global $db;

    $query = "SELECT COUNT(DISTINCT araz_id) as count FROM `txn_view_araz` WHERE `marhala` = '$marhala' AND `jawab_given` = '0' AND `printed` = '0' AND saheb_id = 0 AND (counselor_id = 0 OR counselor_sugest_timestamp > 0)";
    $result = $db->query_fetch_full_result($query);
    return $result[0]['count'];
  }

  function get_araz_data_by_its_id($its_id) {
    global $db;
    $query = "SELECT * FROM `txn_view_araz` WHERE `login_its` = '$its_id' ORDER BY `araz_id` DESC LIMIT 0,1";
    $result = $db->query_fetch_full_result($query);
    return $result;
  }

  function get_old_araiz_details($its, $araz_id) {
    global $db;
    $query = "SELECT * FROM `txn_view_araz` WHERE `login_its` = '$its' AND `araz_id` != '$araz_id' AND `jawab_given` = '1' ORDER BY `araz_id` DESC";
    $result = $db->query_fetch_full_result($query);
    return $result;
  }

  function show_institute_data($araz_data) {

    $course = ($araz_data['course_name'] != '') ? $araz_data['course_name'] : 'NA';
    $institute_name = ($araz_data['institute_name'] != '') ? $araz_data['institute_name'] : 'NA';
    $city = ($araz_data['institute_city'] != '') ? $araz_data['institute_city'] : 'NA';
    $duration = ($araz_data['course_duration'] != '') ? $araz_data['course_duration'] : 'NA';
    $accomodation = ($araz_data['accomodation'] != '') ? $araz_data['accomodation'] : 'NA';
    $course_started = ($araz_data['course_started'] != '') ? $araz_data['course_started'] : 'NA';

    $ret = '<div class="col-xs-12 col-sm-6 bottom10px">
              <strong>Course Name: </strong>' . $course . '
            </div>
            <div class="col-xs-12 col-sm-6 bottom10px">
              <strong>Duration: </strong>' . $duration . '
            </div>
            <div class="col-xs-12 col-sm-6 bottom10px">
              <strong>Institute Name: </strong>' . $institute_name . '
            </div>
            <div class="col-xs-12 col-sm-6 bottom10px">
              <strong>City: </strong>' . $city . '
            </div>
            <div class="col-xs-12 col-sm-6 bottom10px">
              <strong>Accomodation: </strong>' . $accomodation . '
            </div>
            <div class="col-xs-12 col-sm-6 bottom10px">
              <strong>Start Month-Year: </strong>' . $course_started . '
            </div>';

    return $ret;
  }

  function show_school_data($araz_data) {

    $school_name = ($araz_data['school_name'] != '') ? $araz_data['school_name'] : 'NA';
    $school_city = ($araz_data['school_city'] != '') ? $araz_data['school_city'] : 'NA';
    $school_standard = ($araz_data['school_standard'] != '') ? $araz_data['school_standard'] : 'NA';

    $ret = '<div class="col-xs-12 col-sm-6 bottom10px">
              <strong>School Name: </strong>' . $school_name . '
            </div>
            <div class="col-xs-12 col-sm-6 bottom10px">
              <strong>Standard: </strong>' . $school_standard . '
            </div>
            <div class="col-xs-12 col-sm-6 bottom10px">
              <strong>City: </strong>' . $school_city . '
            </div>';

    return $ret;
  }

  function get_all_pending_araz($jamat) {
    global $db;
    $query = "SELECT * FROM `txn_view_araz` WHERE `jawab_given` = '0' AND `jamaat` = '$jamat' ORDER BY `araz_id` DESC";

    $result = $db->query_fetch_full_result($query);
    return $result;
  }

  function get_count_all_pending_araz($jamat) {
    global $db;
    $query = "SELECT COUNT(DISTINCT araz_id) as count FROM `txn_view_araz` WHERE `jawab_given` = '0' AND `jamaat` = '$jamat'";

    $result = $db->query_fetch_full_result($query);
    return $result[0]['count'];
  }

  function get_single_araz_and_user_data($araz_id) {
    global $db;
    $query = "SELECT * FROM `tlb_araz` ta JOIN `tlb_user` tu ON ta.`login_its` = tu.`its_id` WHERE ta.`id` = '$araz_id'";

    $result = $db->query_fetch_full_result($query);
    return $result;
  }

  function get_user_data_by_its($its) {
    global $db;
    $query = "SELECT * FROM `tlb_user` WHERE `its_id` = '$its'";
    $result = $db->query_fetch_full_result($query);
    return $result[0];
  }

  function get_all_araz_course_data($araz_id, $jawab_given = FALSE) {
    global $db;
    $query = "SELECT * FROM `tlb_araz_courses` WHERE `araz_id` = '$araz_id'";

    if ($jawab_given) {
      $query .= " && `jawab_given` = '1'";
    }

    $result = $db->query_fetch_full_result($query);
    return $result;
  }

  function get_counselor_remark($araz_id) {
    global $db;
    $query = "SELECT * FROM `tlb_counselor_suggestion` WHERE `araz_id` = '$araz_id' LIMIT 0,1";

    $result = $db->query_fetch_full_result($query);

    if ($result) {
      return $result[0];
    } else {
      return FALSE;
    }
  }

  function print_all_araz_and_user_data($track_id) {
    global $db;

    foreach ($track_id as $val) {
      if ($val != '') {
        $search_query[] = " `id` = '$val' ";
      }
    }
    $search_query = implode(' OR ', $search_query);


    $query = "SELECT * FROM `tlb_araz` ta JOIN `tlb_user` tu ON ta.`login_its` = tu.`its_id` WHERE $search_query";

    $result = $db->query_fetch_full_result($query);
    return $result;
  }

  function print_all_araz_and_user_data_by_date_wise($from_date, $to_date) {
    global $db;
    $query = "SELECT * FROM `tlb_araz` ta JOIN `tlb_user` tu ON ta.`login_its` = tu.`its_id` WHERE ta.`araz_ts` BETWEEN '$from_date' AND '$to_date'";

    $result = $db->query_fetch_full_result($query);
    return $result;
  }

  function rand_string($length) {
    $chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
    return substr(str_shuffle($chars), 0, $length);
  }

  function araz_send_jawab($araz_id, $jawab_city, $date, $course_id = FALSE, $ho_remarks_araz = FALSE) {
    global $db;

    $string = $this->rand_string(8);
    $hash_code = md5($string);

    $query = "UPDATE `tlb_araz` SET `jawab_city` = '$jawab_city', `jawab_given` = 1, `jawab_ts` = '$date', `code` = '$hash_code', `ho_remarks_araz` = '$ho_remarks_araz' WHERE id LIKE '$araz_id'";
    $result = $db->query($query);

    $query = "UPDATE `tlb_araz_courses` SET `jawab_given` = 1 WHERE araz_id LIKE '$araz_id'";

    if ($course_id) {
      $query .= " && id LIKE '$course_id'";
    }
    $result = $db->query($query);

    $query = "UPDATE `txn_view_araz` SET `jawab_city` = '$jawab_city', `jawab_given` = 1, `jawab_ts` = '$date', `code` = '$hash_code', `ho_remarks_araz` = '$ho_remarks_araz', `course_jawab_given` = 1 WHERE araz_id LIKE '$araz_id'";
    if ($course_id) {
      $query .= " AND araz_course_id LIKE '$course_id'";
    }
    $result = $db->query($query);
    
    // fix for istirshad araz, mark jawab given to 1 for all courses
    $query = "UPDATE `txn_view_araz` SET `jawab_city` = '$jawab_city', `jawab_given` = 1, `jawab_ts` = '$date', `code` = '$hash_code', `ho_remarks_araz` = '$ho_remarks_araz' WHERE araz_id LIKE '$araz_id'";
    $result = $db->query($query);
    
    return $result;
  }

  function araz_send_bazid_jawab($araz_id, $bazid_jawab, $jawab_city, $date, $course_id = FALSE, $ho_remarks_araz = FALSE) {
    global $db;

    $string = $this->rand_string(8);
    $hash_code = md5($string);

    $qry = "UPDATE `tlb_araz` SET `bazid_jawab` = '$bazid_jawab', `jawab_city` = '$jawab_city', `jawab_given` = 1, `jawab_ts` = '$date', `code` = '$hash_code', `ho_remarks_araz` = '$ho_remarks_araz' WHERE id LIKE '$araz_id'";
    $ret = $db->query($qry);

    $qry2 = "UPDATE `tlb_araz_courses` SET `jawab_given` = 1 WHERE araz_id LIKE '$araz_id'";

    if ($course_id) {
      $qry2 .= " && id LIKE '$course_id'";
    }
    $ret2 = $db->query($qry2);

    $query = "UPDATE `txn_view_araz` SET `bazid_jawab` = '$bazid_jawab', `jawab_city` = '$jawab_city', `jawab_given` = 1, `jawab_ts` = '$date', `code` = '$hash_code', `ho_remarks_araz` = '$ho_remarks_araz', `course_jawab_given` = 1 WHERE araz_id LIKE '$araz_id'";
    if ($course_id) {
      $query .= " AND araz_course_id LIKE '$course_id'";
    }
    $result = $db->query($query);
    return $result;
  }

  function araz_send_email_to_student($araz_id) {
    global $db;
    $qry = "UPDATE `tlb_araz` SET `email_sent_student` = '1' WHERE id LIKE '$araz_id'";
    $ret = $db->query($qry);

    $query = "UPDATE `txn_view_araz` SET `email_sent_student` = '1' WHERE araz_id LIKE '$araz_id'";
    $result = $db->query($query);

    if ($ret) {
      $query = "SELECT * from `txn_view_araz` WHERE `araz_id` = '$araz_id'";
      $result = $db->query_fetch_full_result($query);

      $hash_code = $result[0]['code'];
      $to = $result[0]['email'];
      $subject = "Your araz jawab intimation";
      $link = SERVER_PATH . 'araz-jawab/' . $hash_code;
      $message = "<p>Ba’ad al-Salaam al-Jameel,</p>
        <p></p>
        <p>Huzurala TUS has graciously granted jawab mubarak for your education araz.</p>
<p></p>
<p>Please follow the link below to see the araz jawab from Hadrat Aaliyah.
<br>
<a href=\"$link\">Click here</a></p>
<p></p>
<p>Wa al-Salaam<br>
Mahad al-Hasanaat al-Burhaniyah<br>
Al-Shu`oon al-`Aammah <br>
Al Jamea tus Saifiyah<br>
Taj Building, B - 2, Fort Dr. D.N Road (Wallace St.), Mumbai 400 001<br>
Contact no: +9122 4921 6552</p>";

      //$message .= SERVER_PATH . 'admin/print_araiz.php?code=' . $hash_code;

      insert_cron_emails('eduaraiz@talabulilm.com', $to, $subject, $message);

      return TRUE;
    } else {
      return FALSE;
    }
  }

  function araz_send_email_to_student_from_jawab_sent($araz_id, $subject, $message) {
    global $db;
    $qry = "UPDATE `tlb_araz` SET `email_sent_student` = '1' WHERE id LIKE '$araz_id'";
    $ret = $db->query($qry);

    $query = "UPDATE `txn_view_araz` SET `email_sent_student` = '1' WHERE araz_id LIKE '$araz_id'";
    $result = $db->query($query);

    if ($ret) {
      $query = "SELECT * from `txn_view_araz` WHERE `araz_id` = '$araz_id'";
      $result = $db->query_fetch_full_result($query);
      $to = $result[0]['email'];
      if ($to != '') {
        insert_cron_emails('eduaraiz@talabulilm.com', $to, $subject, $message);
      }
      return TRUE;
    } else {
      return FALSE;
    }
  }

  function count_jawab_send_araz_course_data($araz_id) {
    global $db;
    $query = "SELECT COUNT(*) as count FROM `tlb_araz_courses` WHERE `araz_id` = '$araz_id' && `jawab_given` = '1'";

    $result = $db->query_fetch_full_result($query);
    return $result[0]['count'];
  }

  function ageCalculator($dob) {
    if (!empty($dob)) {
      $birthdate = new DateTime($dob);
      $today = new DateTime('today');
      $age = $birthdate->diff($today)->y;
      return $age;
    } else {
      return 0;
    }
  }

  function get_jawab_sent_araz_and_user_data_by_hashcode($hashcode) {
    global $db;
    $query = "SELECT * FROM `tlb_araz` ta JOIN `tlb_user` tu ON ta.`login_its` = tu.`its_id` WHERE ta.`code` = '$hashcode'";

    $result = $db->query_fetch_full_result($query);
    return $result[0];
  }

  function get_all_lp_institutes($page, $num_count, $id = false) {
    global $db;

    $limit = ($page - 1) * $num_count;
    if (!$num_count)
      $num_count = 1;

    $get_result = "SELECT * FROM `tlb_lp_institute` ";
    if ($id) {
      $get_result .= " where id = $id ";
    }
    //$get_result .= " limit $limit,$num_count";

    $row = $db->query_fetch_full_result($get_result);
    return $row;
  }

  function count_lp_institutes() {
    global $db;
    $get_count = "SELECT count(*) AS count FROM `tlb_lp_institute`";
    $row = $db->query_fetch_full_result($get_count);
    return $row[0]['count'];
  }

  function change_lp_institutes_status($value, $id) {
    global $db;
    $query = "UPDATE `tlb_lp_institute` SET `approved` = '$value' WHERE `id` = '$id'";
    $result = $db->query($query);
    return $result;
  }

  function delete_query($id) {
    global $db;
    $query = "DELETE FROM `tlb_queries` WHERE `id` LIKE '$id'";
    $result = $db->query($query);
    return $result;
  }

  function delete_lp_institutes($id) {
    global $db;
    $query = "DELETE FROM `tlb_lp_institute` WHERE `id` LIKE '$id'";
    $result = $db->query($query);
    return $result;
  }

  function get_lp_institute_city() {
    global $db;
    $query = "SELECT `city` FROM `tlb_lp_institute` WHERE `approved` = '0' AND `city` != '' GROUP BY `city`";
    $result = $db->query_fetch_full_result($query);
    return $result;
  }

  function get_lp_institute_data_of_city($city) {
    global $db;
    $query = "SELECT * FROM `tlb_lp_institute` WHERE `city` = '$city' && `approved` = '0'";
    $result = $db->query_fetch_full_result($query);
    return $result;
  }

  function get_data_of_table($table, $tag, $str) {
    global $db;
    $query = "SELECT * FROM `$table` WHERE `$tag` = '$str'";
    $result = $db->query_fetch_full_result($query);
    return $result;
  }

  function insert_whitelist_data($table, $tag, $str) {
    global $db;
    $query = "INSERT INTO `$table` (`$tag`) VALUES ('$str')";
    $result = $db->query($query);
    return $result;
  }

  function get_all_courses() {
    global $db;
    $query = "SELECT * FROM `tlb_courses` WHERE `course_name` != ''";
    $result = $db->query_fetch_full_result($query);
    return $result;
  }

  function get_all_countries_list() {
    global $db;
    $query = 'SELECT DISTINCT(name) FROM `tlb_country` ORDER BY `name` ASC';
    $countries = $db->query_fetch_full_result($query);
    return $countries;
  }

  function get_country_iso_by_name($name) {
    global $db;
    $query = "SELECT `ISO2` FROM `tlb_country` WHERE `name` = '$name'";
    $country = $db->query_fetch_full_result($query);
    return $country;
  }

  function get_all_states_id_by_country_id($country_id) {
    global $db;
    $query = "SELECT `location_id`,`name` FROM `tlb_country` WHERE `parent_id` = '$country_id' AND `location_type` = '1'";
    $states = $db->query_fetch_full_result($query);
    return $states;
  }

  function get_all_city_by_country_iso($country_iso) {
    global $db;
    $query = "SELECT `city` FROM `tlb_city` WHERE `country_code` = '$country_iso'";
    $cities = $db->query_fetch_full_result($query);
    return $cities;
  }

  function get_all_lp_jamaat() {
    global $db;
    $query = "SELECT * FROM `tlb_lp_jamaat`";
    $result = $db->query_fetch_full_result($query);
    return $result;
  }

  function get_all_lp_jamiat() {
    global $db;
    $query = "SELECT * FROM `tlb_lp_jamiat`";
    $result = $db->query_fetch_full_result($query);
    return $result;
  }
  
  function get_all_lp_schools() {
    global $db;
    $query = "SELECT * FROM `tlb_araz_schools_list`";
    $result = $db->query_fetch_full_result($query);
    return $result;
  }

  function get_araz_count_of_jamiat($jamiat) {
    global $db;
    $query = "SELECT COUNT(DISTINCT araz_id) count FROM `txn_view_araz` WHERE `jawab_given` = '0' AND `printed` = '0' AND saheb_id = 0 AND (counselor_id = 0 OR counselor_sugest_timestamp > 0) AND `jamiat` LIKE '$jamiat'";
    $result = $db->query_fetch_full_result($query);
    return $result[0];
  }

  function get_araz_count_jamiat_wise($jamiat = FALSE) {
    global $db;
    $query = "SELECT COUNT(DISTINCT araz_id) count, jamiat, gender FROM `txn_view_araz` ";

    ($jamiat) ? $query .= "WHERE `jamiat` LIKE '$jamiat' " : '';

    $query .= "GROUP BY jamiat, gender ORDER BY `jamiat`";
    $result = $db->query_fetch_full_result($query);

    $ary_data = array();

    if ($result) {
      foreach ($result as $row) {
        $ary_data[$row['jamiat']][$row['gender']] = $row['count'];
      }
    }
    
    return $ary_data;
  }
  
  function get_araz_count_mauze_wise($jamiat = FALSE, $jamaat = FALSE) {
    global $db;
    $query = "SELECT COUNT(DISTINCT araz_id) count, jamiat, jamaat, gender FROM `txn_view_araz` WHERE `araz_id` != '' ";

    ($jamiat) ? $query .= "AND `jamiat` LIKE '$jamiat' " : '';
    ($jamaat) ? $query .= "AND `jamaat` LIKE '$jamaat' " : '';

    $query .= "GROUP BY jamiat, jamaat, gender ORDER BY `jamiat`";
    $result = $db->query_fetch_full_result($query);

    $ary_data = array();

    if ($result) {
      foreach ($result as $row) {
        $ary_data[$row['jamiat']][$row['jamaat']][$row['gender']] = $row['count'];
      }
    }
    
    return $ary_data;
  }
  
  function get_araz_count_marhala_wise($jamiat = FALSE, $jamaat = FALSE, $marhala = FALSE) {
    global $db;
    $query = "SELECT COUNT(DISTINCT araz_id) count, jamiat, jamaat, marhala, gender FROM `txn_view_araz` WHERE `araz_id` != '' ";

    ($jamiat) ? $query .= "AND `jamiat` LIKE '$jamiat' " : '';
    ($jamaat) ? $query .= "AND `jamaat` LIKE '$jamaat' " : '';
    ($marhala) ? $query .= "AND `marhala` LIKE '$marhala' " : '';

    $query .= "GROUP BY jamiat, jamaat, marhala, gender ORDER BY `jamiat`";
    $result = $db->query_fetch_full_result($query);

    $ary_data = array();

    if ($result) {
      foreach ($result as $row) {
        $ary_data[$row['jamiat']][$row['jamaat']][$row['marhala']][$row['gender']] = $row['count'];
      }
    }
    
    return $ary_data;
  }
  
  function get_araz_count_course_wise($jamiat = FALSE, $jamaat = FALSE, $course = FALSE) {
    global $db;
    $query = "SELECT COUNT(DISTINCT araz_id) count, jamiat, jamaat, course_name, gender FROM `txn_view_araz` WHERE `course_name` != '' ";

    ($jamiat) ? $query .= "AND `jamiat` LIKE '$jamiat' " : '';
    ($jamaat) ? $query .= "AND `jamaat` LIKE '$jamaat' " : '';
    ($course) ? $query .= "AND `course_name` LIKE '$course' " : '';

    $query .= "GROUP BY jamiat, jamaat, course_name, gender ORDER BY `jamiat`";
    $result = $db->query_fetch_full_result($query);

    $ary_data = array();

    if ($result) {
      foreach ($result as $row) {
        $ary_data[$row['jamiat']][$row['jamaat']][$row['course_name']][$row['gender']] = $row['count'];
      }
    }
    
    return $ary_data;
  }

  function insert_counselor_suggestions($araz_id, $counselor_id, $course, $country, $city, $remarks) {
    global $db;
    $date = date('Y-m-d');

    $sql = "INSERT INTO `tlb_counselor_suggestion` (`araz_id`, `counselor_id`, `course`, `country`, `city`, `remarks`, `timestamp`) VALUES ( '$araz_id', '$counselor_id', '$course', '$country', '$city', '$remarks', '$date') ON DUPLICATE KEY UPDATE `counselor_id` = '$counselor_id', `course` = '$course', `country` = '$country', `city` = '$city', `remarks` = '$remarks', `timestamp` = '$date'";
    $result = $db->query($sql);

    $qry = "SELECT * from `tlb_user_admin` WHERE `user_id` = '$counselor_id'";
    $res = $db->query_fetch_full_result($qry);
    $full_name = $res[0]['full_name'];
    $gender = $res[0]['gender'];
    $email = $res[0]['email'];
    $mobile = $res[0]['mobile'];

    $query = "UPDATE `txn_view_araz` SET `counselor_sugest_course` = '$course',`counselor_sugest_country` = '$country',`counselor_sugest_city`='$city',`counselor_sugest_remarks` = '$remarks',`counselor_sugest_timestamp` = '$date',`counselor_full_name`='$full_name',`counselor_gender` = '$gender',`counselor_email` = '$email',`counselor_mobile`='$mobile' WHERE araz_id LIKE '$araz_id'";
    $result = $db->query($query);
    return $result;
  }

  function araz_assign_to_counselor($araz_id, $date, $coun_id, $coun_remark) {
    global $db;

    $qry = "UPDATE `tlb_araz` SET `counselor_id` = '$coun_id',`coun_assign_ts` = '$date',`ho_remarks_counselor`='$coun_remark' WHERE id = '$araz_id'";
    $ret = $db->query($qry);

    $query = "UPDATE `txn_view_araz` SET `counselor_id` = '$coun_id',`coun_assign_ts` = '$date',`ho_remarks_counselor`='$coun_remark' WHERE araz_id LIKE '$araz_id'";
    $result = $db->query($query);
    return $ret;
  }

  function araz_assign_to_saheb($araz_id, $date, $saheb_id, $saheb_remarks) {
    global $db;

    $qry = "UPDATE `tlb_araz` SET `saheb_id` = '$saheb_id', `saheb_assign_ts` = '$date',`ho_remarks_saheb`='$saheb_remarks' WHERE id = '$araz_id'";
    $ret = $db->query($qry);

    $query = "UPDATE `txn_view_araz` SET `saheb_id` = '$saheb_id', `saheb_assign_ts` = '$date',`ho_remarks_saheb`='$saheb_remarks' WHERE araz_id LIKE '$araz_id'";
    $result = $db->query($query);
    return $ret;
  }

  function araz_assign_to_print($araz_id, $saheb_remarks) {
    global $db;

    $qry = "UPDATE `tlb_araz` SET `ho_remarks_saheb`='$saheb_remarks' WHERE id = '$araz_id'";
    $ret = $db->query($qry);

    $query = "UPDATE `txn_view_araz` SET `ho_remarks_saheb`='$saheb_remarks' WHERE araz_id LIKE '$araz_id'";
    $result = $db->query($query);
    return $ret;
  }

  function araz_assign_to_attend_later($araz_id, $remarks) {
    global $db;
    $date = date('Y-m-d');

    $qry = "UPDATE `tlb_araz` SET `attend_later` = '1', `ho_remarks_attend_later` = '$remarks', `attend_later_ts` = '$date' WHERE id = '$araz_id'";
    $ret = $db->query($qry);

    $query = "UPDATE `txn_view_araz` SET `attend_later` = '1', `ho_remarks_attend_later` = '$remarks', `attend_later_ts` = '$date' WHERE araz_id LIKE '$araz_id'";
    $result = $db->query($query);
    return $ret;
  }

  function insert_araz_ho_remarks($araz_id, $remarks) {
    global $db;
    $qry = "UPDATE `tlb_araz` SET `ho_remarks` = '$remarks' WHERE id = '$araz_id'";
    $ret = $db->query($qry);

    $query = "UPDATE `txn_view_araz` SET `ho_remarks` = '$remarks' WHERE araz_id LIKE '$araz_id'";
    $result = $db->query($query);
    return $ret;
  }

  function saheb_recommend_to_counselor_araz($araz_id, $saheb_id, $counselor_id, $date) {
    global $db;
    $qry = "INSERT INTO `tlb_saheb_recommend_counselor` (`araz_id`,`saheb_id`,`counselor_id`,`recommend_ts`) VALUES ('$araz_id', '$saheb_id','$counselor_id','$date')";
    $ret = $db->query($qry);
    return $ret;
  }

  function accept_saheb_recommendation($araz_id, $counselor_id, $remarks, $date) {
    global $db;
    $q = "SELECT * from `tlb_saheb_recommend_counselor` WHERE `araz_id` = '$araz_id'";
    $res = $db->query_fetch_full_result($q);

    if ($res) {
      $saheb_id = $res[0]['saheb_id'];

      $qry = "UPDATE `tlb_araz` SET `counselor_id` = '$counselor_id', `coun_assign_ts` = '$date', `ho_remarks_counselor` = '$remarks' WHERE id = '$araz_id' AND `saheb_id` = '$saheb_id'";
      $ret = $db->query($qry);

      $query = "UPDATE `txn_view_araz` SET `counselor_id` = '$counselor_id', `coun_assign_ts` = '$date', `ho_remarks_counselor` = '$remarks' WHERE araz_id LIKE '$araz_id' AND `saheb_id` = '$saheb_id'";
      $result = $db->query($query);

      $qry = "UPDATE `tlb_saheb_recommend_counselor` SET `counselor_id` = '$counselor_id',`ho_assign_ts` = '$date' WHERE `araz_id` = '$araz_id' AND `saheb_id` = '$saheb_id'";
      $ret = $db->query($qry);
      return $ret;
    }
  }

  function insert_araz_chat_message($araz_id, $msg, $user_id, $user_type) {
    global $db;
    $timestamp = date("Y-m-d h:i:sa");

    $query = "INSERT INTO `tlb_araz_chat` (`araz_id`,`message`,`user_id`,`user_type`,`timestamp`) VALUES ('$araz_id', '$msg','$user_id','$user_type','$timestamp')";
    $result = $db->query($query);

    if ($result) {
      $select = "SELECT * from `tlb_user_admin` WHERE `user_id` = '$user_id'";
      $sender_result = $db->query_fetch_full_result($select);

      $ids = FALSE;
      $query = "SELECT `saheb_id`, `counselor_id` FROM `txn_view_araz` WHERE `araz_id` LIKE '$araz_id'";
      $coun_and_saheb_result = $db->query_fetch_full_result($query);
      if ($coun_and_saheb_result) {
        if ($coun_and_saheb_result[0]['saheb_id'] != 0) {
          $ids[] = $coun_and_saheb_result[0]['saheb_id'];
        }
        if ($coun_and_saheb_result[0]['counselor_id'] != 0) {
          $ids[] = $coun_and_saheb_result[0]['counselor_id'];
        }
        $ids = implode(',', $ids);
      }

      $query = "SELECT `email` from `tlb_user_admin` WHERE `user_type` LIKE '" . ROLE_HEAD_OFFICE . "' OR `user_id` IN ($ids)";
      $receiver_result = $db->query_fetch_full_result($query);

      if ($receiver_result) {
        foreach ($receiver_result as $data) {
          $to = $data['email'];
          $subject = "Your araz Chat";

          $message = "Araz ID: " . $araz_id . '<br>';
          $message .= "Sender: " . $sender_result[0]['full_name'] . '<br>';
          $message .= "Message: " . $msg;

          insert_cron_emails('eduaraiz@talabulilm.com', $to, $subject, $message);
        }
      }
      return TRUE;
    } else {
      return FALSE;
    }
  }

  function insert_araz_review($araz_id, $recommend_test, $recommend_coun, $recommend_qardan, $recommend_sponsorship) {
    global $db;
    $date = date('Y-m-d');

    $qry = "UPDATE `tlb_araz` SET `reviewer_processed` = '1', `reviewer_processed_ts` = '$date', `recommend_psychometric_test` = '$recommend_test', `recommend_counseling` = '$recommend_coun', `recommend_qardan` = '$recommend_qardan', `recommend_sponsorship` = '$recommend_sponsorship'  WHERE id = '$araz_id'";
    $ret = $db->query($qry);

    $query = "UPDATE `txn_view_araz` SET `reviewer_processed` = '1', `reviewer_processed_ts` = '$date', `recommend_psychometric_test` = '$recommend_test', `recommend_counseling` = '$recommend_coun', `recommend_qardan` = '$recommend_qardan', `recommend_sponsorship` = '$recommend_sponsorship'  WHERE araz_id = '$araz_id'";
    $result = $db->query($query);
    return $ret;
  }

  function insert_araz_bchet_status($araz_id, $set_status) {
    global $db;
    $qry = "UPDATE `tlb_araz` SET `bchet_status` = '$set_status' WHERE id = '$araz_id'";
    $ret = $db->query($qry);

    $query = "UPDATE `txn_view_araz` SET `bchet_status` = '$set_status'  WHERE araz_id = '$araz_id'";
    $result = $db->query($query);
    return $ret;
  }

  function insert_mark_completed($araz_id, $counseling_done, $qardan_hasana, $sponsorship, $test_file) {
    global $db;
    $qry = "UPDATE `tlb_araz` SET `test_file_upload` = '$test_file', `counselling_done` = '$counseling_done', `qardan_hasana_done` = '$qardan_hasana', `sponsorship_done` = '$sponsorship' WHERE id = '$araz_id'";
    $ret = $db->query($qry);

    $query = "UPDATE `txn_view_araz` SET `test_file_upload` = '$test_file', `counselling_done` = '$counseling_done', `qardan_hasana_done` = '$qardan_hasana', `sponsorship_done` = '$sponsorship'  WHERE araz_id = '$araz_id'";
    $result = $db->query($query);
    return $ret;
  }

  function get_counselor_details_of_user_admin($user_id) {
    global $db;
    $query = "SELECT * FROM `tlb_user_admin` WHERE `user_id` = '$user_id'";
    $result = $db->query_fetch_full_result($query);
    return $result[0];
  }

  function get_question_answers_of_araz($araz_id) {
    global $db;
    $query = "SELECT * FROM `tlb_araz_questionaire` taq JOIN `tlb_araz_questionaire_answers` taqa ON taq.`id` = taqa.`ques_id` WHERE taqa.`araz_id` = '$araz_id'";
    $result = $db->query_fetch_full_result($query);
    return $result;
  }

  function get_chat_data_of_araz($araz_id) {
    global $db;
    $query = "SELECT * FROM `tlb_araz_chat` tac JOIN `tlb_user_admin` tua ON tac.`user_id` = tua.`user_id` WHERE tac.`araz_id` = '$araz_id' ORDER BY tac.`timestamp`";
    $result = $db->query_fetch_full_result($query);
    return $result;
  }

  function get_previous_study_data($courses, $araz_type) {
    if ($araz_type == 'istirshad') {
      if ($courses) {
        foreach ($courses as $course) {
          $all_dates[] = $course['course_started'];
        }

        usort($all_dates, function($a, $b) {
          return strtotime($a) < strtotime($b) ? -1 : 1;
        });

        $lowest_date = $all_dates[0];

        foreach ($courses as $elementKey => $element) {
          foreach ($element as $key => $value) {
            if ($key == 'course_started' && $value == $lowest_date) {
              $current_study_array = $courses[$elementKey];
            }
          }
        }
        $ret['course_name'] = $current_study_array['course_name'];
        $ret['institute_name'] = $current_study_array['institute_name'];
        $ret['institute_country'] = $current_study_array['institute_country'];
        $ret['institute_city'] = $current_study_array['institute_city'];
      }
    } else {
      $ret['course_name'] = $courses[0]['course_name'];
      $ret['institute_name'] = $courses[0]['institute_name'];
      $ret['institute_country'] = $courses[0]['institute_country'];
      $ret['institute_city'] = $courses[0]['institute_city'];
    }
    return $ret;
  }

  function get_araiz_data_organised($ary_data) {
    $last_araz_id = FALSE; // Reset the araz id
    foreach ($ary_data as $araz) {
      // Create bins for User Data, Araz Data, Previous Studies, Current Studeis, Remarks
      $araz_id = $araz['araz_id'];

      if ($last_araz_id == $araz_id) {
        $ret[$araz_id]['course_details'][] = $this->parse_araz_course_details($araz);
      } else {
        $last_araz_id = $araz_id;
        $ret[$araz_id]['user_data'] = $this->parse_araz_user_details($araz);
        $ret[$araz_id]['araz_data'] = $this->parse_araz_details($araz);
        $ret[$araz_id]['course_details'][] = $this->parse_araz_course_details($araz);
        $ret[$araz_id]['previous_study_details'] = $this->parse_araz_previous_details($araz);
        $ret[$araz_id]['counselor_details'] = $this->parse_araz_counselor_details($araz);
      }
    }

    return $ret;
  }

  private function parse_araz_user_details($araz) {

    $ary_fields = array('login_its', 'user_full_name', 'hifz_sanad', 'nisaab',
        'already_araz_done', 'already_araz_from_name', 'bazid_jawab', 'jawab_given',
        'jawab_city', 'jawab_ts', 'code', 'full_name_ar', 'dob', 'gender',
        'nationality', 'country', 'state', 'city', 'email', 'tanzeem_id',
        'jamaat', 'jamiat', 'quran_sanad', 'araz_type', 'email', 'mobile', 'whatsapp');

    return $this->read_fields_from_array($ary_fields, $araz);
  }

  private function parse_araz_details($araz) {

    $ary_fields = array('araz_id', 'marhala', 'madrasah_darajah', 'madrasah_country', 'madrasah_state', 'madrasah_city', 'madrasah_name', 'madrasah_name_ar', 'school_city', 'school_state', 'school_standard', 'school_name', 'araz_ts', 'school_filter', 'school_pincode', 'ho_remarks_araz', 'ho_remarks_counselor', 'ho_remarks_saheb', 'saheb_id', 'saheb_assign_ts', 'printed', 'email_sent_student', 'taken_psychometric_aptitude_test', 'text_psychometric_aptitude', 'prefer_more_clarity', 'want_further_guidence', 'seek_funding_edu', 'want_assistance', 'received_sponsorship', 'follow_shariaa', 'disclaimer', 'reviewer_processed', 'reviewer_processed_ts', 'recommend_psychometric_test', 'recommend_counseling', 'recommend_qardan', 'recommend_sponsorship', 'bchet_status', 'test_file_upload', 'counselling_done', 'qardan_hasana_done', 'sponsorship_done');

    return $this->read_fields_from_array($ary_fields, $araz);
  }

  private function parse_araz_course_details($araz) {

    $ary_fields = array('araz_course_id', 'stream', 'degree_name', 'course_name',
        'institute_name', 'institute_country', 'institute_city', 'accomodation',
        'course_duration', 'course_started', 'course_jawab_given');

    return $this->read_fields_from_array($ary_fields, $araz);
  }

  private function parse_araz_previous_details($araz) {

    $ary_fields = array('current_course', 'current_inst_name',
        'current_inst_city', 'current_inst_country', 'inst_start_month',
        'inst_start_year', 'inst_end_month', 'inst_end_year');

    return $this->read_fields_from_array($ary_fields, $araz);
  }

  private function parse_araz_counselor_details($araz) {

    $ary_fields = array('counselor_id', 'coun_assign_ts',
        'counselor_sugest_course', 'counselor_sugest_country',
        'counselor_sugest_city', 'counselor_sugest_remarks',
        'counselor_sugest_timestamp', 'counselor_full_name', 'counselor_gender',
        'counselor_email', 'counselor_mobile');

    return $this->read_fields_from_array($ary_fields, $araz);
  }

  private function read_fields_from_array($fields, $array) {
    foreach ($fields as $fld) {
      $ret[$fld] = $array[$fld];
    }

    return $ret;
  }

  function get_degrees_by_marhala($marhala) {
    global $db;
    $query = "SELECT * FROM `tlb_courses` WHERE `marhala_group` = '$marhala' AND `degree` != '' GROUP BY `degree`";
    $result = $db->query_fetch_full_result($query);
    return $result;
  }

  function get_courses_by_degree($degree, $marhala) {
    global $db;
    $query = "SELECT * FROM `tlb_courses` WHERE `degree` = '$degree' AND `marhala_group` = '$marhala' AND `course_name` != ''";
    $result = $db->query_fetch_full_result($query);
    return $result;
  }

  function get_finalize_courses() {
    global $db;
    $query = "SELECT * FROM `tlb_finalize_courses` GROUP BY `course` ORDER BY `course` ASC";
    $result = $db->query_fetch_full_result($query);
    return $result;
  }

  function edit_courses_in_araz($degree_name, $course_duration, $course, $institute_name, $country, $city, $accomodation, $course_start_month, $course_start_year, $araz_id) {
    global $db;
    $course_started = $course_start_month . '-' . $course_start_year;

    $sql = "INSERT INTO `tlb_araz_courses` (`araz_id`, `degree_name`, `course_name`, `institute_name`, `institute_country`, `institute_city`, `accomodation`, `course_duration`, `course_started`) VALUES ( '$araz_id', '$degree_name', '$course', '$institute_name', '$country', '$city', '$accomodation', '$course_duration', '$course_started')";

    $result = $db->query($sql);

    if ($result) {
      // update the txn table accordingly for given araz id
      $qry_del_txn = "DELETE FROM `txn_view_araz` WHERE araz_id LIKE '$araz_id'";
      $rslt_del = $db->query($qry_del_txn);

      $qry_insrt_txn = "INSERT INTO txn_view_araz SELECT * FROM view_araz WHERE araz_id LIKE '$araz_id'";
      $rslt_updt = $db->query($qry_insrt_txn);
    }

    return $result;
  }

  function get_standards_by_marhala($marhala_id) {
    switch ($marhala_id) {
      case 1:
        return array('Pre-school', 'Nursery', 'Kindergarten', 'Junior Kindergarten', 'Senior Kindergarten');
      case 2:
        return array('1st', '2nd', '3rd', '4th');
      case 3:
        return array('5th', '6th', '7th');
      case 4:
        return array('8th', '9th', '10th');
      case 5:
        return array('11th', '12th');
      default:
        return array();
    }
  }

}
