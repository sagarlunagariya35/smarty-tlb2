<?php
require_once '../classes/class.database.php';

class Mtx_Crs_Course {

  function insert_course($topic_id, $title, $slug, $description, $img_icon, $sort, $start_dt, $end_dt, $registration_end_dt, $is_modarated, $teacher_id, $capacity, $req_enroll, $is_paid, $fee, $promote, $promote_end_dt, $course_lng, $assign_to_grp, $user_id, $is_active) {
    global $db;
    
    $timestamp = date('Y-m-d');

    $query = "INSERT INTO `crs_course` (`topic_id`, `title`, `slug`, `description`, `img_icon`, `sort_id`, `start_date`, `end_date`, `registration_end_date`, `is_modarated`, `teacher_id`, `capacity`, `req_enroll`, `is_paid`, `fee`, `promote`, `promote_end_date`, `course_lng`, `assign_to_group`, `created_ts`, `created_by`, `is_active`) VALUES ('$topic_id', '$title', '$slug', '$description', '$img_icon', '$sort', '$start_dt', '$end_dt', '$registration_end_dt', '$is_modarated', '$teacher_id', '$capacity', '$req_enroll', '$is_paid', '$fee', '$promote', '$promote_end_dt', '$course_lng', '$assign_to_grp', '$timestamp', '$user_id', '$is_active')";

    $result = $db->query($query);
    return $result;
  }
  
  function update_course($topic_id, $title, $slug, $description, $img_icon, $sort, $start_dt, $end_dt, $registration_end_dt, $is_modarated, $teacher_id, $capacity, $req_enroll, $is_paid, $fee, $promote, $promote_end_dt, $course_lng, $assign_to_grp, $is_active, $id) {
    global $db;
    $query = "UPDATE `crs_course` SET `topic_id` = '$topic_id', `title` = '$title', `slug` = '$slug', `description` = '$description', `img_icon` = '$img_icon', `sort_id` = '$sort', `start_date` = '$start_dt', `end_date` = '$end_dt', `registration_end_date` = '$registration_end_dt', `is_modarated` = '$is_modarated', `teacher_id` = '$teacher_id', `capacity` = '$capacity', `req_enroll` = '$req_enroll', `is_paid` = '$is_paid', `fee` = '$fee', `promote` = '$promote', `promote_end_date` = '$promote_end_dt', `course_lng` = '$course_lng', `assign_to_group` = '$assign_to_grp', `is_active` = '$is_active' WHERE `id` = '$id'";
    $result = $db->query($query);
    return $result;
  }
  
  function get_all_courses($status = FALSE, $sort = 'ASC') {
    global $db;
    $query = "SELECT * FROM `crs_course` WHERE ";
    
    $query .= ($status) ? "`is_active` LIKE '$status' " : "(`is_active` LIKE '0' OR '1') ";
    
    $query .= "ORDER BY `sort_id` $sort";
    $result = $db->query_fetch_full_result($query);
    return $result;
  }

  function get_all_courses_of_topic($topic) {
    global $db;
    $query = "SELECT * FROM `crs_course` WHERE `topic_id` LIKE '$topic' ORDER BY `id` DESC";
    $result = $db->query_fetch_full_result($query);
    return $result;
  }
  
  function get_title_of_courses($course, $sort = 'ASC') {
    global $db;
    $query = "SELECT * FROM `crs_course` WHERE `title` LIKE '%$course%' AND `is_active` LIKE '1' ORDER BY `sort_id` $sort";
    $result = $db->query_fetch_full_result($query);
    return $result;
  }
  
  function get_course($id) {
    global $db;
    $query = "SELECT * FROM `crs_course` WHERE `id` LIKE '$id'";
    $result = $db->query_fetch_full_result($query);
    return $result[0];
  }
  
  function get_last_insert_course()
  {
    global $db;
    $query = "SELECT `id` FROM `crs_course` ORDER BY `id` DESC";
    $result = $db->query_fetch_full_result($query);
    return $result[0]['id'];
  }
  
  function change_course_status($status, $id)
  {
    global $db;
    $query = "UPDATE `crs_course` SET `is_active` = '$status' WHERE `id` = '$id'";
    $result = $db->query($query);
    return $result;
  }
  
  function delete_course($id) {
    global $db;
    $query = "DELETE FROM `crs_course` WHERE id = '$id'";
    $result = $db->query($query);
    return $result;
  }
  
}
