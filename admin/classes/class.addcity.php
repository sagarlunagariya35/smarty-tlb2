<?php
require_once '../classes/class.database.php';

class City {
  
function insert_city($state,$city) {
  
  global $db;

    $query = "INSERT INTO `city`(`name`,`state_id`) VALUES ('$city','$state')";
    
    $result = $db->query($query);
    return $result;
  
}

function update_city($state,$city ,$id)
{
  global $db;
  $update_query = "UPDATE `city` SET `name`='$city',`state_id`= '$state'  WHERE id = '$id' ";
  
  $result = $db->query($update_query);
    return $result;
}

function count_city_record1() {
  global $db;
  $get_count = "SELECT count(*) AS count FROM `city`";
  $row = $db->query_fetch_full_result($get_count);
  return $row[0]['count'];
}
        
function city_result($page, $num_count, $id = false) {
    global $db;
    
       $limit = ($page - 1) * $num_count;
  if (!$num_count)
    $num_count = 1;
  $get_result = "SELECT *,(SELECT name FROM states s WHERE s.id LIKE c.state_id) state FROM `city` c ";
    if ($id) {
      $get_result .= " where id = $id ";
    }
   $get_result .= " limit $limit,$num_count"; 
   
    $row = $db->query_fetch_full_result($get_result);
    return $row;
  }
}