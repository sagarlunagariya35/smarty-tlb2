<?php

require_once '../classes/class.database.php';

class questionary {

  function insert_Question($groupid, $question, $que_prev, $default_ans) {

    global $db;

    $query = "INSERT INTO `tlb_araz_questionaire`(`grpID`, `question`, `question_preview`, `preferred`) VALUES ('$groupid','$question','$que_prev','$default_ans')";

    $result = $db->query($query);
    return $result;
  }

  function update_question($groupid, $question, $que_prev, $default_ans, $id) {
    global $db;
    $update_query = "UPDATE `tlb_araz_questionaire` SET `grpID`='$groupid',`question`='$question',`question_preview`='$que_prev',`preferred`= $default_ans WHERE id = '$id' ";

    $result = $db->query($update_query);
    return $result;
  }

  function count_question_record1() {
    global $db;
    $get_count = "SELECT count(*) AS count FROM `tlb_araz_questionaire`";
    $row = $db->query_fetch_full_result($get_count);
    return $row[0]['count'];
  }

  function que_result($page, $num_count, $id = false) {
    global $db;
    
    $limit = ($page - 1) * $num_count;
    if (!$num_count)
      $num_count = 1;

    $get_result = "SELECT * FROM `tlb_araz_questionaire` ";
    if ($id) {
      $get_result .= " where id = $id ";
    }
    //$get_result .= " limit $limit,$num_count";

    $row = $db->query_fetch_full_result($get_result);
    return $row;
  }
  
  function que_result_of_marhala($grpID) {
    global $db;
    $query = "SELECT * FROM `tlb_araz_questionaire` WHERE `grpID` = '$grpID' ORDER BY `id` DESC ";
    $result = $db->query_fetch_full_result($query);
    return $result;
  }

}
