<?php
require_once '../classes/class.database.php';

class Mtx_Crs_Istifadah_Master {

  function insert_istifadah_master($title, $slug, $description, $img_icon, $sort, $user_id, $is_active) {
    global $db;
    
    $timestamp = date('Y-m-d');

    $query = "INSERT INTO `crs_istifadah_master` (`title`, `slug`, `description`, `img_icon`, `sort_id`, `created_ts`, `created_by`, `is_active`) VALUES ('$title', '$slug', '$description', '$img_icon', '$sort', '$timestamp', '$user_id', '$is_active')";
    
    $result = $db->query($query);
    return $result;
  }
  
  function update_istifadah_master($title, $slug, $description, $img_icon, $sort, $is_active, $id) {
    global $db;
    $query = "UPDATE `crs_istifadah_master` SET `title` = '$title', `slug` = '$slug', `description` = '$description', `img_icon` = '$img_icon', `sort_id` = '$sort', `is_active` = '$is_active'  WHERE `id` = '$id'";
    $result = $db->query($query);
    return $result;
  }
  
  function get_all_istifadah_master($status = FALSE, $sort = 'ASC') {
    global $db;
    $query = "SELECT * FROM `crs_istifadah_master` WHERE ";
    
    $query .= ($status) ? "`is_active` LIKE '$status' " : "(`is_active` LIKE '0' OR '1') ";
    
    $query .= "ORDER BY `sort_id` $sort";
    $result = $db->query_fetch_full_result($query);
    return $result;
  }
  
  function get_title_of_istifadah_master($text, $sort = 'ASC') {
    global $db;
    $query = "SELECT * FROM `crs_istifadah_master` WHERE `title` LIKE '%$text%' AND `is_active` LIKE '1' ORDER BY `sort_id` $sort";
    $result = $db->query_fetch_full_result($query);
    return $result;
  }
  
  function get_istifadah_master($id) {
    global $db;
    $query = "SELECT * FROM `crs_istifadah_master` WHERE `id` LIKE '$id'";
    $result = $db->query_fetch_full_result($query);
    return $result[0];
  }
  
  function get_last_insert_istifadah_master() {
    global $db;
    $query = "SELECT `id` FROM `crs_istifadah_master` ORDER BY `id` DESC";
    $result = $db->query_fetch_full_result($query);
    return $result[0]['id'];
  }
  
  function change_istifadah_master_status($status, $id)
  {
    global $db;
    $query = "UPDATE `crs_istifadah_master` SET `is_active` = '$status' WHERE `id` = '$id'";
    $result = $db->query($query);
    return $result;
  }
  
  function delete_istifadah_master($id) {
    global $db;
    $query = "DELETE FROM `crs_istifadah_master` WHERE id = '$id'";
    $result = $db->query($query);
    return $result;
  }

  
}
