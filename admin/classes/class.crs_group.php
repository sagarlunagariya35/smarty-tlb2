<?php
require_once '../classes/class.database.php';

class Mtx_Crs_Group {

  function insert_group($title, $group_type, $group_data, $user_id) {
    global $db;
    
    $timestamp = date('Y-m-d');

    $query = "INSERT INTO `crs_group` (`title`, `type`, `created_ts`, `created_by`) VALUES ('$title', '$group_type', '$timestamp', '$user_id')";
    $result = $db->query($query);
    $group_id = $db->get_last_insert_id();
    
    $group_array = array(CRS_JAMAAT => 'jamaat', CRS_JAMIAT => 'jamiat', CRS_SCHOOL => 'school', CRS_ITS_ID => 'its_id', CRS_MARHALA => 'marhala');
    $group_table_array = array('jamaat' => 'crs_group_jamaat', 'jamiat' => 'crs_group_jamiat', 'school' => 'crs_group_school', 'its_id' => 'crs_group_its', 'marhala' => 'crs_group_marhala');
    
    $group_name = $group_array[$group_type];
    $group_table = $group_table_array[$group_name];

    if ($group_data[0]) {
      foreach ($group_data[0] as $data) {

        $query = "INSERT INTO `$group_table` (`group_id`, `$group_name`, `created_ts`) VALUES ('$group_id', '$data', '$timestamp')";
        $res = $db->query($query);
      }
    }
    
    return $result;
  }
  
  function update_group($title, $group_type, $group_data, $group_id) {
    global $db;
    $timestamp = date('Y-m-d');
    
    $query = "UPDATE `crs_group` SET `title` = '$title', `type` = '$group_type', `created_ts` = '$timestamp' WHERE `id` = '$group_id'";
    $result = $db->query($query);
    
    $group_array = array(CRS_JAMAAT => 'jamaat', CRS_JAMIAT => 'jamiat', CRS_SCHOOL => 'school', CRS_ITS_ID => 'its_id', CRS_MARHALA => 'marhala');
    $group_table_array = array('jamaat' => 'crs_group_jamaat', 'jamiat' => 'crs_group_jamiat', 'school' => 'crs_group_school', 'its_id' => 'crs_group_its', 'marhala' => 'crs_group_marhala');
    
    $group_name = $group_array[$group_type];
    $group_table = $group_table_array[$group_name];

    if ($group_data[0]) {
      $delete = "DELETE FROM `$group_table` WHERE `group_id` LIKE '$group_id'";
      $db->query($delete);
        
      foreach ($group_data[0] as $data) {
        $query = "INSERT INTO `$group_table` (`group_id`, `$group_name`, `created_ts`) VALUES ('$group_id', '$data', '$timestamp')";
        $res = $db->query($query);
      }
    }
    
    return $result;
  }
  
  function get_all_groups() {
    global $db;
    $query = "SELECT * FROM `crs_group`";
    $result = $db->query_fetch_full_result($query);
    return $result;
  }
  
  function get_group($id) {
    global $db;
    $query = "SELECT * FROM `crs_group` WHERE `id` LIKE '$id'";
    $result = $db->query_fetch_full_result($query);
    return $result[0];
  }
  
  function get_group_data($table, $id) {
    global $db;
    $query = "SELECT * FROM `$table` WHERE `group_id` LIKE '$id'";
    $result = $db->query_fetch_full_result($query);
    
    if ($result) {
      $group_table_array = array('crs_group_jamaat' => 'jamaat', 'crs_group_jamiat' => 'jamiat', 'crs_group_school' => 'school', 'crs_group_its' => 'its_id', 'crs_group_marhala' => 'marhala');
      
      $group_table_field = $group_table_array[$table];
      
      foreach ($result as $data) {
        $ret[] = $data[$group_table_field];
      }
    }
    return $ret;
  }
  
  function delete_group($id) {
    global $db;
    $query = "DELETE FROM `crs_group` WHERE id = '$id'";
    $result = $db->query($query);
    return $result;
  }
  
}
