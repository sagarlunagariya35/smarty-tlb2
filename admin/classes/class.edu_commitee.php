<?php

require_once '../classes/class.database.php';

class Edu_committee {

  protected $tanzeem_id;
  private $jamat_name = NULL;
  private $hub_name = NULL;
  private $list_cities_under_hub = NULL;
  private $is_araz_download = NULL;

  function __construct($tanzeem_id = '') {
    $this->tanzeem_id = $tanzeem_id;
  }

  function save_members_araz($data) {
    global $db;
    $sql = "INSERT INTO `tlb_araz_edu_committee` (`its`, `name`, `designation`, `is_co_ordinator`, `is_mamur_co_ordinator`, `tanzeem_id`, `email`, `mobile`, `qualification`, `occupation`, `jamaat`, `as_timestamp`) VALUES ";

    foreach ($data as $mem) {
      $its = $mem['its'];
      $name = $mem['name'];
      $designation = $mem['designation'];
      $is_co_ord = $mem['is_co_ordinator'];
      $is_mamur_co_ord = $mem['is_mamur_co_ordinator'];
      $tanzeem_id = $this->tanzeem_id;  //$_SESSION['tanzeem_id'];
      $email = $mem['email'];
      $mobile = $mem['mobile'];
      $qualification = $mem['qualification'];
      $occupation = $mem['occupation'];
      $jamat = $mem['jamaat'];
      $timestamp = date('Y-m-d H:i:s');
      $sql .= "('$its','$name','$designation','$is_co_ord', '$is_mamur_co_ord', '$tanzeem_id', '$email', '$mobile', '$qualification', '$occupation', '$jamat', '$timestamp'),";
    }
    $sql = rtrim($sql, ",");
    $ret = $db->query($sql);
    if ($ret) {
      return TRUE;
    } else {
      log_text_admin($sql);
      return FALSE;
    }
  }

  function check_member_already_registered($its) {
    global $db;
    $query = "SELECT * FROM tlb_araz_edu_committee WHERE its LIKE '$its'";
    
    $data = $db->query_fetch_full_result($query);

    if ($data) {
      return TRUE;
    } else {
      return FALSE;
    }
  }

  function delete_member_by_its($its) {
    global $db;
    $query = "DELETE FROM tlb_araz_edu_committee WHERE its LIKE '$its' AND tanzeem_id LIKE '$this->tanzeem_id' ";

    $data = $db->query($query);

    if ($data) {
      return TRUE;
    } else {
      return FALSE;
    }
  }

  function delete_all_rejected_members() {
    global $db;
    $query = "DELETE FROM tlb_araz_edu_committee WHERE ho_rejected LIKE '1' AND tanzeem_id LIKE '$this->tanzeem_id' ";

    $data = $db->query($query);

    if ($data) {
      return TRUE;
    } else {
      return FALSE;
    }
  }
  
  function member_reset($its) {
    global $db;
    $query = "UPDATE `tlb_araz_edu_committee` SET `ho_approved` = 0 WHERE its LIKE '$its' AND tanzeem_id LIKE '$this->tanzeem_id' ";

    $data = $db->query($query);
    
    $query = "UPDATE `tlb_araz_edu_committee` SET `ho_rejected` = 0 WHERE its LIKE '$its' AND tanzeem_id LIKE '$this->tanzeem_id' ";

    $data = $db->query($query);

    if ($data) {
      return TRUE;
    } else {
      return FALSE;
    }
  }

  function members_get_list() {
    global $db;
    $query = "SELECT * FROM `tlb_araz_edu_committee` WHERE tanzeem_id LIKE '$this->tanzeem_id' ORDER BY `designation`, `ho_rejected`";
    $result = $db->query_fetch_full_result($query);
    return $result;
  }
  
  function members_get_message($is_downloaded, $is_verified, $is_rejected, $misal_uploaded) {

   if($misal_uploaded){
     return '<p class="text-info">Misaal Shareef had been uploaded.<span class="pull-right"><a class="btn btn-info btn-block" target="_blank" href="misaal/'.$this->get_jamat_name().'.pdf">Download Misaal</a></span></p>';
   }
   if($is_downloaded){
     return '<p class="text-info">Your araz has been submitted for Manzuri.</p>';
   }
   if($is_rejected){
     return '<p class="text-info">You may want to do araz for a new member.</p>';
   }
   if($is_verified){
     return '<p class="text-info">Your araz is being verified by Head Office. You may be contacted regarding your members.During this time you can add or delete members.</p>';
   }
   
    return '<p class="text-info">Once you have added all the members, verify the list below and confirm them to send your araz for the Education Committee Members.</p>
        <p class="text-info"><strong>Note: </strong>During araz for Education Committee make sure you have included members from ilaaqa mawaze.</p>';
  }
  
  function members_get_status($misal_uploaded, $is_downloaded, $is_rejected, $is_verified) {

   if($misal_uploaded){
     return 'Misaal Uploaded';
   }
   if($is_downloaded){
     return 'Submitted';
   }
   if($is_rejected){
     return 'Rejected';
   }
   if($is_verified){
     return 'Approved';
   }
   if($is_rejected === 0 && $is_verified === 0){
     return 'HO Verification';
   }
   
    return '';
  }

  function members_get_list_approved() {
    global $db;
    $query = "SELECT `its`, `name`, `designation`, `jamaat` FROM `tlb_araz_edu_committee` WHERE tanzeem_id LIKE '$this->tanzeem_id' AND `ho_approved` = 1 ORDER BY designation";
    $result = $db->query_fetch_full_result($query);
    return $result;
  }
  
  function members_get_list_rejected() {
    global $db;
    $query = "SELECT `its`, `name`, `designation`, `jamaat` FROM `tlb_araz_edu_committee` WHERE tanzeem_id LIKE '$this->tanzeem_id' AND `ho_rejected` = 1 ORDER BY designation";
    $result = $db->query_fetch_full_result($query);
    return $result;
  }

  function committe_misal_uploaded($pdf_path) {
    global $db;
    $query = "UPDATE `tlb_araz_edu_committee` SET misaal = '$pdf_path' WHERE tanzeem_id LIKE '$this->tanzeem_id' AND `ho_approved` = 1";
    $result = $db->query_fetch_full_result($query);
    return $result;
  }

  function save_approved($approved_id) {
    global $db;
    $ids = implode(", ", $approved_id);
    $query = "UPDATE `tlb_araz_edu_committee` SET `ho_approved` = 1 WHERE id in ($ids)";
    $ret = $db->query($query);
    if ($ret) {
      return TRUE;
    } else {
      return FALSE;
    }
  }

  function save_rejected($rejected_id) {
    global $db;
    $ids = implode(", ", $rejected_id);
    $query = "UPDATE `tlb_araz_edu_committee` SET `ho_rejected` = 1 WHERE id in ($ids)";
    $ret = $db->query($query);
    if ($ret) {
      return TRUE;
    } else {
      return FALSE;
    }
  }

  function save_downloaded() {
    global $db;
    $query = "UPDATE `tlb_araz_edu_committee` SET `downloaded` = 1 WHERE tanzeem_id LIKE '$this->tanzeem_id' AND `ho_approved` = 1";
    $ret = $db->query($query);
    if ($ret) {
      return TRUE;
    } else {
      return FALSE;
    }
  }

  function get_jamat_name() {
    global $db;

    if ($this->jamat_name === NULL) {
      $query = "SELECT jamat_name FROM tlb_link_tanzeem_jamaat WHERE tanzeem_id LIKE '$this->tanzeem_id'";
      $result = $db->query_fetch_full_result($query);
      if ($result) {
        $this->jamat_name = $result[0]['jamat_name'];
      }
    }
    return $this->jamat_name;
  }

  function get_hub_name() {
    global $db;

    if ($this->hub_name === NULL) {
      $query = "SELECT hub_name FROM tlb_link_tanzeem_jamaat WHERE tanzeem_id LIKE '$this->tanzeem_id'";
      $result = $db->query_fetch_full_result($query);
      if ($result) {
        $this->hub_name = $result[0]['hub_name'];
      }
    }
    return $this->hub_name;
  }

  function get_list_of_cities_under_hub($hub_name) {
    global $db;

    if ($this->list_cities_under_hub === NULL) {
      $query = "SELECT jamat_name FROM tlb_link_tanzeem_jamaat WHERE hub_name LIKE '$hub_name'";
      $result = $db->query_fetch_full_result($query);
      if ($result) {
        $this->list_cities_under_hub = $result;
      }
    }
    return $this->list_cities_under_hub;
  }
  
  function get_list_of_mamureen_under_hub($hub_name) {
    global $db;

    $query = "SELECT mamureen FROM tlb_link_mamureen_jamaat WHERE jamaat LIKE '$hub_name'";
    $result = $db->query_fetch_full_result($query);
    if ($result) {
      return $result;
    }
    return FALSE;
  }

  function is_araz_download($tanzeem_id) {
    global $db;

    if ($this->is_araz_download === NULL) {
      $query = "SELECT * FROM tlb_link_tanzeem_jamaat WHERE tanzeem_id LIKE '$tanzeem_id'";
      $result = $db->query_fetch_full_result($query);
      if ($result) {
        $this->is_araz_download = TRUE;
      } else {
        $this->is_araz_download = FALSE;
      }
    }
    return $this->is_araz_download;
  }

  function designation_by_id_to_string($designation_id) {
    global $db;
    
    switch ($designation_id){
      case 1:
      case 2:
      case 3:
        return 'Secretory';
      case 4:
        return 'Joint Secretory';
      case 5:
        return 'Treasurer';
      case 6:
        return 'Counselor';
      case 7:
        return 'Member';
    }
    return FALSE;
  }

}
