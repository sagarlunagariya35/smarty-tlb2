<?php

require_once '../classes/class.database.php';

class Mtx_Qasaid {

  private $id;
  private $title;
  private $slug;
  private $qasida_text;
  private $is_active;
  private $created_by_user_id;
  private $created_by_user_name;
  private $created_ts;
  
  private $table_name = 'tlb_qasaid';
  // private $word_table = 'tlb_ohbat_sentence_words';
  

  /*
   * =============== GETTERS ==========================
   */
  public function get_id() {
    return $this->id;
  }

  public function get_title() {
    return $this->title;
  }

  public function get_slug() {
    return $this->slug;
  }

  public function get_qasida_text() {
    return $this->qasida_text;
  }

  public function get_is_active() {
    return $this->is_active;
  }

  public function get_created_by_user_id() {
    return $this->created_by_user_id;
  }

  public function get_created_by_user_name() {
    return $this->created_by_user_name;
  }

  public function get_created_ts() {
    return $this->created_ts;
  }

  public function get_table_name() {
    return $this->table_name;
  }

    /*
   * =============== . / GETTERS ==========================
   */

  function insert_qasida($title, $slug, $alam, $text, $is_active) {
    global $db;
    
    // clean slug
    $slug_clean = $this->clean_slug($slug);
    $user_id = $_SESSION[USER_ID];
    $ts = date('Y-m-d');

    $query = "INSERT INTO `$this->table_name` (`title`, `slug`, `alam`, `text`, `is_active`, `created_user`, `created_ts`) VALUES ('$title', '$slug_clean', '$alam', '$text', '$is_active', '$user_id', '$ts')";

    $result = $db->query($query);
    return ($result) ? $db->get_last_insert_id() : FALSE;
  }

  function edit_qasida($id, $title, $slug, $alam, $text, $is_active) {
    global $db;
    $query = "UPDATE `$this->table_name` SET `title`='$title', `slug`='$slug', `alam`='$alam', `text`='$text', `is_active`='$is_active' WHERE `id`='$id'";

    $result = $db->query($query);
    return $result;
  }

  function get_max_qasida_id() {
    global $db;
    $get_count = "SELECT MAX(id) AS max FROM `$this->table_name`";
    $row = $db->query_fetch_full_result($get_count);
    return $row[0]['max'];
  }

  function get_all_qasaid() {
    global $db;
    $query = "SELECT * FROM `$this->table_name` ORDER BY id DESC";
    $result = $db->query_fetch_full_result($query);
    return $result;
  }
  
  function count_total_qasaid() {
    global $db;
    $query = "SELECT COUNT(`id`) as count FROM `$this->table_name`";
    $result = $db->query_fetch_full_result($query);
    return $result[0]['count'];
  }

  function get_qasida_by_id($id) {
    global $db;
    $query = "SELECT * FROM `$this->table_name` WHERE `id` LIKE '$id'";
    $result = $db->query_fetch_full_result($query);
    return $result[0];
  }

  function delete_qasida($id) {
    global $db;
    $query = "DELETE FROM `$this->table_name` WHERE id='$id'";
    $result = $db->query($query);

    return $result;
  }
  
  function get_log_qasaid() {
    global $db;
    $query = "SELECT *, SUM(tlq.`slider_value`) sum FROM `tlb_log_qasaid` tlq JOIN `tlb_user` tu ON tlq.`its_id` = tu.`its_id` GROUP BY tlq.`its_id`";
    $result = $db->query_fetch_full_result($query);
    return $result;
  }

  public function clean_slug($slug){
    // make all small case
    $slug = strtolower($slug);
    // remove non alphanumeric
    $slug = preg_replace("/[^a-zA-Z0-9 ]/", "", $slug);
    // replace space with hyphen
    $slug = str_replace(' ', '-', $slug);
    
    return $slug;
  }
  
}
