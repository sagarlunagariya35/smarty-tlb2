<?php
require_once '../classes/class.database.php';

class Mtx_Crs_Announcements {

  function insert_announcement($topic_id, $course_id, $chapter_id, $description, $user_id, $is_active) {
    global $db;
    $timestamp = date('Y-m-d');

    $query = "INSERT INTO `crs_announcements` (`topic_id`, `course_id`, `chapter_id`, `description`, `created_ts`, `created_by`, `is_active`) VALUES ('$topic_id', '$course_id', '$chapter_id', '$description', '$timestamp', '$user_id', '$is_active')";

    $result = $db->query($query);
    return $result;
  }
  
  function update_announcement($topic_id, $course_id, $chapter_id, $description, $user_id, $is_active, $id) {
    global $db;
    $query = "UPDATE `crs_announcements` SET `topic_id` = '$topic_id', `course_id` = '$course_id', `chapter_id` = '$course_id', `description` = '$description', `is_active` = '$is_active' WHERE `id` = '$id'";
    $result = $db->query($query);
    return $result;
  }
  
  function get_all_announcements($status = FALSE) {
    global $db;
    $query = "SELECT * FROM `crs_announcements` WHERE ";
    $query .= ($status) ? "`is_active` LIKE '$status' " : "(`is_active` LIKE '0' OR '1') ";
    
    $result = $db->query_fetch_full_result($query);
    return $result;
  }
  
  function get_announcement($id) {
    global $db;
    $query = "SELECT * FROM `crs_announcements` WHERE `id` LIKE '$id'";
    $result = $db->query_fetch_full_result($query);
    return $result[0];
  }
  
  function get_table_title_by_id($table, $id) {
    global $db;
    $query = "SELECT `title` FROM `$table` WHERE `id` LIKE '$id'";
    $result = $db->query_fetch_full_result($query);
    return $result[0]['title'];
  }
  
  function change_announcement_status($status, $id) {
    global $db;
    $query = "UPDATE `crs_announcements` SET `is_active` = '$status' WHERE `id` = '$id'";
    $result = $db->query($query);
    return $result;
  }
  
  function delete_announcement($id) {
    global $db;
    $query = "DELETE FROM `crs_announcements` WHERE id = '$id'";
    $result = $db->query($query);
    return $result;
  }

  
}
