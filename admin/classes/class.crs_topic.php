<?php
require_once '../classes/class.database.php';

class Mtx_Crs_Topic {

  function insert_topic($title, $slug, $description, $img_icon, $sort, $user_id, $is_active, $parent_topic = FALSE) {
    global $db;
    
    $timestamp = date('Y-m-d');

    $query = "INSERT INTO `crs_topic` (`title`, `slug`, `description`, `img_icon`, `sort_id`, `created_ts`, `created_by`, `is_active`, `parent_id`) VALUES ('$title', '$slug', '$description', '$img_icon', '$sort', '$timestamp', '$user_id', '$is_active', '$parent_topic')";

    $result = $db->query($query);
    return $result;
  }
  
  function update_topic($title, $slug, $description, $img_icon, $sort, $is_active, $id, $parent_topic = FALSE) {
    global $db;
    $query = "UPDATE `crs_topic` SET `title` = '$title', `slug` = '$slug', `description` = '$description', `img_icon` = '$img_icon', `sort_id` = '$sort', `is_active` = '$is_active', `parent_id` = '$parent_topic' WHERE `id` = '$id'";
    $result = $db->query($query);
    return $result;
  }
  
  function get_all_topics($parent_topic = FALSE, $status = FALSE, $sort = 'ASC') {
    global $db;
    $query = "SELECT * FROM `crs_topic` WHERE ";
    
    $query .= ($status) ? "`is_active` LIKE '$status' " : "(`is_active` LIKE '0' OR '1') ";
    ($parent_topic) ? $query .= "AND `parent_id` LIKE '$parent_topic' " : '';
    
    $query .= "ORDER BY `parent_id`,`sort_id` $sort";
    $result = $db->query_fetch_full_result($query);
    return $result;
  }
  
  function get_title_of_topics($topic, $sort = 'ASC') {
    global $db;
    $query = "SELECT * FROM `crs_topic` WHERE `title` LIKE '%$topic%' AND `is_active` LIKE '1' ORDER BY `sort_id` $sort";
    $result = $db->query_fetch_full_result($query);
    return $result;
  }
  
  function get_topic($id) {
    global $db;
    $query = "SELECT * FROM `crs_topic` WHERE `id` LIKE '$id'";
    $result = $db->query_fetch_full_result($query);
    return $result[0];
  }
  
  function get_last_insert_topic()
  {
    global $db;
    $query = "SELECT `id` FROM `crs_topic` ORDER BY `id` DESC";
    $result = $db->query_fetch_full_result($query);
    return $result[0]['id'];
  }
  
  function change_topic_status($status, $id)
  {
    global $db;
    $query = "UPDATE `crs_topic` SET `is_active` = '$status' WHERE `id` = '$id'";
    $result = $db->query($query);
    return $result;
  }
  
  function delete_topic($id) {
    global $db;
    $query = "DELETE FROM `crs_topic` WHERE id = '$id'";
    $result = $db->query($query);
    return $result;
  }

  
}
