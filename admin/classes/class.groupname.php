<?php

require_once '../classes/class.database.php';

class Group {

  function insert_grp($subject) {
    global $db;

    $subject = $db->clean_data($subject);
    $query = "INSERT INTO `groups`(`grpName`) VALUES ('$subject')";

    $result = $db->query($query);
    return $result;
  }

  function update_grpname($subject, $id) {
    global $db;
    
    $subject = $db->clean_data($subject);
    $id = (int)trim($id);
    $update_query = "UPDATE `groups` SET `grpName`='$subject'  WHERE id = '$id' ";

    $result = $db->query($update_query);
    return $result;
  }

  function count_grpname_record1() {
    global $db;
    $get_count = "SELECT count(*) AS count FROM `groups`";
    $row = $db->query_fetch_full_result($get_count);
    return $row[0]['count'];
  }

  function grpname_result($id = false) {
    global $db;

    //$limit = ($page - 1) * $num_count;
    $query = "SELECT * FROM `groups` ";
    if ($id) {
      $id = (int) trim($id);
      $query .= " where id = $id ";
    }
    $query .= ' ORDER BY id ';
    //$get_result .= " limit $limit,$num_count";

    $row = $db->query_fetch_full_result($query);
    return $row;
  }

}
