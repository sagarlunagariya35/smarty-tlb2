<?php

/**
 * filter by :
 * 
 * 1) approved by amilsaab and jawab baqi (approved = 1, jawab = null)
 * 2) approaval baqi and three days late and jawab baaqi (approved = null, jawab = null araz_ts < (Now - 3 days
 * 
 */
function get_araiz_for_admin() {
  
}

/**
 * approval remaing, jawab remaining and of amil moze
 * (aprroved = null, jawab = null, tanzeem_id = $tanzeem_id
 */
function get_araiz_for_amil($tanzeem_id) {
  
}

/*
 * TODO: Shift this function to correct class
 * Get all records from tlb_araz_edu_committee with distinct tanzeem id
 * and their araz status
 */

function get_all_tanzeem_from_araz_edu_comm() {
  global $db;

  $query = "SELECT DISTINCT(taec.tanzeem_id), taec.downloaded, taec.misaal, (SELECT jamat_name FROM tlb_link_tanzeem_jamaat tltj WHERE tltj.tanzeem_id LIKE taec.tanzeem_id) jamat_name, (SELECT hub_name FROM tlb_link_tanzeem_jamaat tltj WHERE tltj.tanzeem_id LIKE taec.tanzeem_id) hub_name FROM `tlb_araz_edu_committee` taec WHERE `ho_approved` IS NOT NULL ORDER BY taec.as_timestamp DESC";

  $result = $db->query_fetch_full_result($query);

  return $result;
}

/*
 * TODO: Shift this function to correct class
 */

function get_list_tanzeem_edu_comm_members($tanzeem_id) {
  global $db;

  $query = "SELECT * FROM `tlb_araz_edu_committee` WHERE tanzeem_id LIKE '$tanzeem_id'";

  $result = $db->query_fetch_full_result($query);

  return $result;
}

function log_text_admin($txt) {
  if (!ENABLE_WRITE_LOG)
    return TRUE;
  $fh = fopen('my_log_admin.txt', 'a');

  fwrite($fh, $txt . "\n\n");

  fclose($fh);

  unset($fh);
}

/**
 * 
 * This function will write the data to the csv file and force its download
 * @param type $ary_data    : Array of Data to be put into the CSV File
 * @param type $filename    : Name of the file.
 * @param type $csv_header  : Header Row for the CSV file. In array format.
 */
function csv_download($ary_data, $filename, $csv_headers = FALSE) {
  // output headers so that the file is downloaded rather than displayed
  header('Content-Type: text/csv; charset=utf-8');
  header('Content-Disposition: attachment; filename=' . $filename);
  header("Pragma: no-cache");
  header("Expires: 0");
// create a file pointer connected to the output stream
  $output = fopen('php://output', 'w');

// output the column headings
  if($csv_headers){
    foreach($csv_headers as $row){
      fputcsv($output, $row);
    }
  }

// fetch the data
// loop over the rows, outputting them
  foreach ($ary_data as $row) {
    fputcsv($output, $row);
  }
  fclose($output);
  exit();
}

/**
 * // TODO : Move to the edu_comm class
 * @param type $ary_approved  : Array of the Approved 
 * @param type $ary_rejected
 */
function update_araz_edu_comm($ary_approved, $ary_rejected) {
  
}

function add_tanzeem_jamaat_link($tanzeem_id, $jamat){
  global $db;
  
  $query = "SELECT COUNT(*) cnt FROM tlb_link_tanzeem_jamaat WHERE tanzeem_id LIKE '$tanzeem_id' AND jamat_name LIKE '$jamat'";
  $ret = $db->query_fetch_full_result($query);
  
  if($ret[0]['cnt'] == 0){
    $query = "INSERT INTO tlb_link_tanzeem_jamaat (tanzeem_id, jamat_name) VALUES ('$tanzeem_id', '$jamat')";
    $db->query($query);
  }
}


function get_list_counselor_data() {
  global $db;
  $query = "SELECT * FROM `tlb_temp_counselor_data`";
  $result = $db->query_fetch_full_result($query);
  return $result;
}

function get_all_queries() {
  global $db;
  $query = "SELECT * FROM `tlb_queries` WHERE `ho_timestamp` = 0 ORDER BY `id` DESC";
  $result = $db->query_fetch_full_result($query);
  return $result;
}

function get_all_ho_sent_reply_queries() {
  global $db;
  $query = "SELECT * FROM `tlb_queries` WHERE `ho_timestamp` > 0 ORDER BY `id` DESC";
  $result = $db->query_fetch_full_result($query);
  return $result;
}

function count_total_queries() {
  global $db;
  $query = "SELECT COUNT(*) as count FROM `tlb_queries` WHERE `ho_timestamp` = 0";
  $result = $db->query_fetch_full_result($query);
  return $result[0]['count'];
}

function sent_queries_ho_reply($query_id, $user_id, $ho_reply, $its_id, $date) {
  global $db;
  $query = "UPDATE `tlb_queries` SET `user_id`='$user_id', `ho_remarks`='$ho_reply', `ho_timestamp`='$date' WHERE `id`='$query_id'";
  
  $result = $db->query($query);
  
  if ($result) {
    $qry = "SELECT * from `tlb_user` WHERE `its_id` = '$its_id'";
    $rslt = $db->query_fetch_full_result($qry);

    $to = $rslt[0]['email'];
    
    if($to != ''){
      $subject = "Your Query jawab intimation";
      $message = "<p>Ba’ad al-Salaam al-Jameel,</p>
<p>$ho_reply</p>
<p></p>
      <p>Wa al-Salaam<br>
      Mahad al-Hasanaat al-Burhaniyah<br>
      Al-Shu`oon al-`Aammah <br>
      Al Jamea tus Saifiyah<br>
      Taj Building, B - 2, Fort Dr. D.N Road (Wallace St.), Mumbai 400 001<br>
      Contact no: +9122 4921 6552</p>";

      insert_cron_emails('eduaraiz@talabulilm.com', $to, $subject, $message);

      return TRUE;
    }
  } else {
    return FALSE;
  }
}

function sent_email_of_report($table,$field,$its_key,$remarks,$subject) {
  global $db;
  $query = "UPDATE `$table` SET `email_sent` = '1' WHERE `$field` = '$its_key'";
  $result = $db->query($query);
  
  if ($result) {
    $qry = "SELECT * from `tlb_user` WHERE `its_id` = '$its_key'";
    $rslt = $db->query_fetch_full_result($qry);

    $to = $rslt[0]['email'];
    
    if($to != ''){
      insert_cron_emails('info@talabulilm.com', $to, $subject, $remarks);
      return TRUE;
    }
  } else {
    return FALSE;
  }
}

function get_degrees_by_marhala($marhala) {
  global $db;
  $query = "SELECT * FROM `tlb_courses` WHERE `marhala_group` = '$marhala' AND `degree` != '' GROUP BY `degree` ORDER BY `degree` ASC";
  $result = $db->query_fetch_full_result($query);
  return $result;
}

function check_iso_already_exist($iso) {
  global $db;
  $query = "SELECT * FROM `tlb_country` WHERE `ISO2` LIKE '$iso' LIMIT 1";
  $result = $db->query_fetch_full_result($query);
  if ($result) {
    return 1;
  } else {
    return 0;
  }
}

function insert_course_by_marhala_and_degree($marhala, $degree, $course) {
  global $db;
  $query = "INSERT INTO `tlb_courses`(`marhala_group`,`degree`,`course_name`) VALUES ('$marhala','$degree','$course')";

  $result = $db->query($query);
  return $result;
}

function insert_institute_by_marhala($marhala, $institute) {
  global $db;
  $query = "INSERT INTO `tlb_lp_institute`(`marhala`,`institute`,`approved`) VALUES ('$marhala','$institute','1')";
  $result = $db->query($query);
  return $result;
}

function insert_country_by_queries($country, $iso) {
  global $db;
  $res = check_iso_already_exist($iso);
  if($res == 0){
    $query = "INSERT INTO `tlb_country`(`name`,`ISO2`) VALUES ('$country','$iso')";
    $result = $db->query($query);
    return $result;
  }
}

function insert_city_by_country($country, $city) {
  global $db;
  $qry = "SELECT `ISO2` FROM `tlb_country` WHERE `name` = '$country' LIMIT 0,1";
  $rslt = $db->query_fetch_full_result($qry);
  $country_code = $rslt[0]['ISO2'];
  
  $query = "INSERT INTO `tlb_city`(`city`,`country_code`) VALUES ('$country','$country_code')";
  $result = $db->query($query);
  return $result;
}

function get_all_tasavvuraat_araz() {
  global $db;
  $query = "SELECT * FROM `tlb_tasavvuraat_araz` ta JOIN `tlb_user` tu ON ta.`its_id` = tu.`its_id`";
  $result = $db->query_fetch_full_result($query);
  return $result;
}

function insert_general_setting($link,$category) {
  global $db;
  $query = "INSERT INTO `general_setting`(`redirect_link`,`category`) VALUES ('$link','$category')";
  $result = $db->query($query);
  return $result;
}

function insert_general_setting_for_carousal($image_file, $link, $page) {
  global $db;
  $query = "INSERT INTO `general_setting_for_carousal`(`image`,`link`,`page`) VALUES ('$image_file','$link','$page')";
  $result = $db->query($query);
  return $result;
}

function update_general_setting($link,$category,$id) {
  global $db;
  $query = "UPDATE `general_setting` SET `redirect_link` = '$link', `category` = '$category' WHERE `id`='$id'";
  $result = $db->query($query);
  return $result;
}

function update_general_setting_for_carousal($image_file, $link, $page, $id) {
  global $db;
  $query = "UPDATE `general_setting_for_carousal` SET `image` = '$image_file', `link` = '$link', `page` = '$page' WHERE `id`='$id'";
  $result = $db->query($query);
  return $result;
}

function get_general_settings($category, $id = FALSE) {
  global $db;
  $query = "SELECT * FROM `general_setting` WHERE `category` = '$category'";
  
  ($id) ? $query .= " AND `id` = '$id'" : '';
  
  $result = $db->query_fetch_full_result($query);
  return $result;
}

function get_general_settings_for_carousal($page, $id = FALSE) {
  global $db;
  $query = "SELECT * FROM `general_setting_for_carousal` WHERE `page` = '$page' ";
  
  ($id && $page) ? $query .= " AND `id` = '$id'" : '';
  
  $result = $db->query_fetch_full_result($query);
  return $result;
}

function delete_general_setting($id) {
  global $db;
  $query = "DELETE FROM `general_setting` WHERE `id` LIKE '$id'";
  $result = $db->query($query);
  return $result;
}

function delete_general_setting_for_carousal($id, $page) {
  global $db;
  $query = "DELETE FROM `general_setting_for_carousal` WHERE `id` LIKE '$id' AND `page` LIKE '$page'";
  $result = $db->query($query);
  return $result;
}

function insert_cron_emails($from, $to, $subject, $message) {
  global $db;
  $query = "INSERT INTO `tlb_cron_emails`(`from_user`, `to_user`, `subject`, `message`) VALUES ('$from','$to','$subject','$message')";
  $result = $db->query($query);
  return $result;
}

function get_iso_language_codes($id = FALSE) {
  global $db;
  $query = "SELECT * FROM `iso_language_codes`";
  
  ($id) ? $query .= " WHERE `id` = '$id'" : '';
  
  $result = $db->query_fetch_full_result($query);
  return $result;
}

function get_all_menu() {
  global $db;
  $query = "SELECT * FROM `tlb_menu` WHERE `level` = '0' AND `active` = '1' ORDER BY `index` ASC";
  $result = $db->query_fetch_full_result($query);
  return $result;
}

function get_all_submenu() {
  global $db;
  $query = "SELECT * FROM `tlb_menu` WHERE `level` != '0' AND `active` = '1' ORDER BY `index` ASC";
  $result = $db->query_fetch_full_result($query);
  return $result;
}

function get_submenu_by_menu($parent_menu) {
  global $db;
  $query = "SELECT * FROM `tlb_menu` WHERE `level` = '$parent_menu' AND `active` = '1' ORDER BY `index` ASC";
  $result = $db->query_fetch_full_result($query);
  return $result;
}

function get_menu_by_id($id) {
  global $db;
  $query = "SELECT * FROM `tlb_menu` WHERE id = '$id'";
  $result = $db->query_fetch_full_result($query);
  return $result;
}

function array_sort($array, $on, $order=SORT_ASC)
{
    $new_array = array();
    $sortable_array = array();

    if($array != '') {
        foreach ($array as $k => $v) {
            if (is_array($v)) {
                foreach ($v as $k2 => $v2) {
                    if ($k2 == $on) {
                        $sortable_array[$k] = $v2;
                    }
                }
            } else {
                $sortable_array[$k] = $v;
            }
        }

        switch ($order) {
            case SORT_ASC:
                asort($sortable_array);
            break;
            case SORT_DESC:
                arsort($sortable_array);
            break;
        }

        foreach ($sortable_array as $k => $v) {
            $new_array[$k] = $array[$k];
        }
    }

    return $new_array;
}
