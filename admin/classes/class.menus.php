<?php
require_once '../classes/class.database.php';

class Mtx_Menu {

  function insert_menu($title, $slug, $sort, $is_active, $parent_menu = FALSE) {
    global $db;
    
    $query = "INSERT INTO `tlb_menu` (`name`, `slug`, `index`, `active`, `level`) VALUES ('$title', '$slug', '$sort', '$is_active', '$parent_menu')";

    $result = $db->query($query);
    return $result;
  }
  
  function update_menu($title, $slug, $sort, $is_active, $id, $parent_topic = FALSE) {
    global $db;
    $query = "UPDATE `tlb_menu` SET `name` = '$title', `slug` = '$slug', `index` = '$sort', `active` = '$is_active', `level` = '$parent_topic' WHERE `id` = '$id'";
    $result = $db->query($query);
    return $result;
  }
  
  function get_all_menus($parent_menu = FALSE, $status = FALSE, $sort = 'ASC') {
    global $db;
    $query = "SELECT * FROM `tlb_menu` WHERE ";
    
    $query .= ($status) ? "`active` LIKE '$status' " : "(`active` LIKE '0' OR '1') ";
    ($parent_menu) ? $query .= "AND `level` LIKE '$parent_menu' " : '';
    
    $query .= "ORDER BY `index` $sort";
    $result = $db->query_fetch_full_result($query);
    return $result;
  }
  
  function get_title_of_menus($menu, $sort = 'ASC') {
    global $db;
    $query = "SELECT * FROM `tlb_menu` WHERE `name` LIKE '%$menu%' AND `active` LIKE '1' ORDER BY `index` $sort";
    $result = $db->query_fetch_full_result($query);
    return $result;
  }
  
  function get_menu($id) {
    global $db;
    $query = "SELECT * FROM `tlb_menu` WHERE `id` LIKE '$id'";
    $result = $db->query_fetch_full_result($query);
    return $result[0];
  }
  
  function change_menu_status($status, $id)
  {
    global $db;
    $query = "UPDATE `tlb_menu` SET `active` = '$status' WHERE `id` = '$id'";
    $result = $db->query($query);
    return $result;
  }
  
  function delete_menu($id) {
    global $db;
    $query = "DELETE FROM `tlb_menu` WHERE id = '$id'";
    $result = $db->query($query);
    return $result;
  }

  
}
