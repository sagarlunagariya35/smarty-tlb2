<?php
require_once '../classes/class.database.php';

class Event
{
    function insert_event($event_title,$description,$slug,$page_title,$text,$youtube_video,$user_id,$start_date,$end_date,$capacity,$venue,$contact_person,$email,$mobile,$template,$master_menu,$sub_menu,$active)
    {
        global $db;
        $query = "INSERT INTO `events`(`event_title`,`desc`,`slug`,`page_title`,`text`,`youtube_video`,`user_id`,`start_date`,`end_date`,`capacity`,`venue`,`contact_person`,`email`,`mobile`,`templates`,`menu`,`submenu`,`active`) VALUES ('$event_title','$description','$slug','$page_title','$text','$youtube_video','$user_id','$start_date','$end_date','$capacity','$venue','$contact_person','$email','$mobile','$template','$master_menu','$sub_menu','$active')";
        
        $result = $db->query($query);
        return $result;
        
    }
    
    function edit_event($event_title,$description,$slug,$page_title,$text,$youtube_video,$user_id,$start_date,$end_date,$capacity,$venue,$contact_person,$email,$mobile,$template,$master_menu,$sub_menu,$active,$event_id)
    {
        global $db;
        $query = "UPDATE `events` SET `event_title`='$event_title', `slug`='$slug', `page_title`='$page_title', `text`='$text', `youtube_video`='$youtube_video', `user_id`='$user_id', `start_date`='$start_date', `end_date`='$end_date', `capacity`='$capacity', `venue`='$venue', `contact_person`='$contact_person', `email`='$email', `mobile`='$mobile', `templates`='$template', `menu`='$master_menu', `submenu`='$sub_menu', `active`='$active' WHERE `id`='$event_id'";
        
        $result = $db->query($query);
        return $result;
        
    }
    
    function get_max_eventID()
    {
        global $db;
        $get_count = "SELECT count(*) AS count FROM `events`";
        $row = $db->query_fetch_full_result($get_count);
        return $row[0]['count'];
    }
    
    function insert_media($table,$event_id,$filename,$index)
    {
        global $db;
        $query = "INSERT INTO `".$table."`(`event_id`,`".$table."_url`,`index`)VALUES($event_id,'$filename',$index)";
        
        $result = $db->query($query);
        if($result)
          $id = $db->get_last_insert_id();
        return $id;
    }
    
    function update_media($event_id,$filename,$id)
    {
        global $db;
        $query = "UPDATE `event_video` SET `event_video_thumb` = '$filename' WHERE `event_id` = '$event_id' && `id` = '$id'";
        
        $result = $db->query($query);
        return $result;
    }
    
    function get_all_events() 
    {
        global $db;
        $query = "SELECT * FROM `events`";
        $result = $db->query_fetch_full_result($query);
        return $result;
    }
    
    function get_value_by_id($table,$id)
    {
        global $db;
        $query = "SELECT * FROM ".$table." WHERE id = '".$id."'";
        $result = $db->query_fetch_full_result($query);
        return $result;
    }
    
    function get_media_by_id($table,$id)
    {
        global $db;
        $query = "SELECT * FROM ".$table." where event_id='".$id."' ";
        $result = $db->query_fetch_full_result($query);
        return $result;
    }
    
    function delete_media_by_id($table,$id)
    {
        global $db;
        $query = "DELETE FROM ".$table." WHERE id='".$id."' ";
        $result = $db->query($query);
        return $result;
    }
    
    function event_active_inactive($event_id,$value)
    {
        global $db;
        $query = "UPDATE `events` SET `active` = '".$value."' where id = '".$event_id."'";
        $result = $db->query($query);
        return $result;
    }
    
    function delete_event($id)
    {
        global $db;
        $query = "DELETE FROM `events` WHERE id = '$id'";
        $result = $db->query($query);
        
        $table_array = array('event_video','event_audio','event_image');
        
        foreach ($table_array as $ta){
          $query1 = "DELETE FROM `$ta` WHERE event_id = '$id'";
          $result1 = $db->query($query1);
        }
        
        return $result;
    }
    
    function get_event_registers_by_event_id($event_id){
      global $db;
      $query = "SELECT * FROM `event_register` er JOIN `tlb_user` tu ON er.`its` = tu.`its_id` WHERE er.`event_id` = '$event_id'";
      $result = $db->query_fetch_full_result($query);
      return $result;
    }
}
