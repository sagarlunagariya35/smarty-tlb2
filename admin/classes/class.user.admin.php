<?php

require_once '../classes/class.database.php';

/**
 * 
 * this will be used in raza araz page. will provide data 
 * and will update data from that page
 * 
 * Description of class
 *
 * @author mustafa
 */
class mtx_user_admin {

  private $arb_name;
  private $eng_name;
  private $user_id;
  private $its_id;
  private $username;
  private $password;
  //private $first_prefix;
  //private $first_name;
//  private $middle_prefix;
//  private $middle_name;
//  private $last_name;
  private $gender;
  private $dob;
  private $nationality;
  private $address;
  private $pincode;
  private $country;
  private $state;
  private $city;
  private $mobile;
  private $telephone;
  private $email;
  private $tanzeem_id;
  private $occupation;
  private $qualification;
  private $jamaat;
  private $jamiat;
  private $last_update_ts;
  private $user_type;
  private $prefer_email;
  private $prefer_whatsapp;
  private $prefer_call;
  private $mumin_photo = '';
  private $subscribe;
  private $age;

  /**
   * Loads values from database
   * @param type $user_its
   * @return class or false is not found
   */
  public function loaduser($user_its) {
    global $db;
    $query = "SELECT * FROM `tlb_user_admin` WHERE `its_id` LIKE '$user_its'";
    $result = $db->query_fetch_full_result($query);
    $result = $result[0];
    if ($result) {
      $this->set_user_id($result['user_id']);
      $this->set_its_id($result['its_id']);
      $this->set_first_prefix($result['first_prefix']);
      $this->set_first_name($result['first_name']);
      $this->set_middle_prefix($result['middle_prefix']);
      $this->set_middle_name($result['middle_name']);
      $this->set_last_name($result['last_name']);
      $this->set_gender($result['gender']);
      $this->set_dob($result['dob']);
      $this->set_nationality($result['nationality']);
      $this->set_address($result['address']);
      $this->set_country($result['country']);
      $this->set_state($result['state']);
      $this->set_city($result['city']);
      $this->set_mobile($result['mobile']);
      $this->set_telephone($result['telephone']);
      $this->set_email($result['email']);
      $this->set_user_type($result['user_type']);
      $this->set_prefer_call($result['prefer_call']);
      $this->set_prefer_email($result['prefer_email']);
      $this->set_prefer_whatsapp($result['prefer_whatsapp']);
      $this->set_tanzeem_id($result['tanzeem_id']);
      $this->set_qualification($result['qualification']);
      $this->set_occupation($result['occupation']);
      $this->set_jamaat($result['jamaat']);
      $this->set_jamiat($result['jamiat']);
      $this->set_last_update_ts($result['last_update_ts']);

      return $this;
    } else {
      return FALSE;
    }
  }

  /*
   * 
   */

  public function get_mumeen_detail_from_its($its) {

    $key = 'TASub052';

    $soapUrl = "http://www.its52.com/eJas/EjamaatServices.asmx";

    $xml_post_string = '<?xml version="1.0" encoding="utf-8"?>
  <soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
  <soap:Body>
  <EduDept_MuminDetail xmlns="http://localhost/eJAS/EjamaatServices">
  <EjamaatId>' . $its . '</EjamaatId>
  <strKey>' . $key . '</strKey>
  </EduDept_MuminDetail>
  </soap:Body>
  </soap:Envelope>';

    $headers = array(
        "Content-type: text/xml;charset=\"utf-8\"",
        "Accept: text/xml",
        "Cache-Control: no-cache",
        "Pragma: no-cache",
        "SOAPAction: http://localhost/eJAS/EjamaatServices/EduDept_MuminDetail",
        "Content-length: " . strlen($xml_post_string)
    );

    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $soapUrl);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_POST, true);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $xml_post_string); // the SOAP request
    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

    $response = curl_exec($ch);
    curl_close($ch);

    log_text_admin(__FILE__ . '-response text-' . $response);

    if ($response == FALSE) {
      return FALSE;
    }
    $xml = simplexml_load_string($response);

    $mumin_data = $xml->xpath('//Table');
    if (count($mumin_data) == 0) {
      return FALSE;
    }
    $mumin_data = $mumin_data[0];

    $this->set_its_id($mumin_data->Mumin_id);
    $this->set_first_prefix($mumin_data->prefix);
    $this->set_first_name($mumin_data->First_name);
    $this->set_middle_prefix($mumin_data->Middle_Prefix);
    $this->set_middle_name($mumin_data->Middle_Name);
    $this->set_last_name($mumin_data->Surname);
    $this->set_gender($mumin_data->gender);
    $this->set_address($mumin_data->Address);
// TODO mmm: correct the date format to (Y-m-d)
    $this->set_dob($mumin_data->DOB);
    $this->set_mobile($mumin_data->MOBILE_NO);
    $this->set_email($mumin_data->email);
// TODO mmm: create following four vars and their respective getter setter
    $this->set_qualification($mumin_data->Qualification);
    $this->set_occupation($mumin_data->Occupation);
    $this->set_jamaat($mumin_data->Jamaat);
    $this->set_jamiat($mumin_data->Jamiaat);
    $this->set_tanzeem_id($mumin_data->Tanzeem_ID);
    $this->set_nationality($mumin_data->Nationality);
    $this->set_city($mumin_data->city);
    $this->set_country($mumin_data->country);
    $this->set_arb_name($mumin_data->arabic_fullname);
    $this->set_eng_name($mumin_data->FullName);

    return TRUE;
//    } else {
//      return FALSE;
//    }
  }

  public function get_mumeen_detail_from_its_local($its) {

    /* $key = ITS_KEY;

      $soapUrl = "http://www.its52.com/eJas/EjamaatServices.asmx";

      $xml_post_string = '<?xml version="1.0" encoding="utf-8"?>
      <soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
      <soap:Body>
      <EduDept_MuminDetail xmlns="http://localhost/eJAS/EjamaatServices">
      <EjamaatId>' . $its . '</EjamaatId>
      <strKey>' . $key . '</strKey>
      </EduDept_MuminDetail>
      </soap:Body>
      </soap:Envelope>';

      $headers = array(
      "Content-type: text/xml;charset=\"utf-8\"",
      "Accept: text/xml",
      "Cache-Control: no-cache",
      "Pragma: no-cache",
      "SOAPAction: http://localhost/eJAS/EjamaatServices/EduDept_MuminDetail",
      "Content-length: " . strlen($xml_post_string)
      );

      $ch = curl_init();
      curl_setopt($ch, CURLOPT_URL, $soapUrl);
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
      curl_setopt($ch, CURLOPT_POST, true);
      curl_setopt($ch, CURLOPT_POSTFIELDS, $xml_post_string); // the SOAP request
      curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

      $response = curl_exec($ch);
      curl_close($ch);
     */
    $response = '<?xml version="1.0" encoding="utf-8"?><soap:Envelope xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema"><soap:Body><EduDept_MuminDetailResponse xmlns="http://localhost/eJAS/EjamaatServices"><EduDept_MuminDetailResult><xs:schema id="NewDataSet" xmlns="" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:msdata="urn:schemas-microsoft-com:xml-msdata"><xs:element name="NewDataSet" msdata:IsDataSet="true" msdata:UseCurrentLocale="true"><xs:complexType><xs:choice minOccurs="0" maxOccurs="unbounded"><xs:element name="Table"><xs:complexType><xs:sequence><xs:element name="Mumin_id" type="xs:string" minOccurs="0"/><xs:element name="FullName" type="xs:string" minOccurs="0"/><xs:element name="prefix" type="xs:string" minOccurs="0"/><xs:element name="First_name" type="xs:string" minOccurs="0"/><xs:element name="Middle_Prefix" type="xs:string" minOccurs="0"/><xs:element name="Middle_Name" type="xs:string" minOccurs="0"/><xs:element name="Surname" type="xs:string" minOccurs="0"/><xs:element name="gender" type="xs:string" minOccurs="0"/><xs:element name="age" type="xs:int" minOccurs="0"/><xs:element name="Address" type="xs:string" minOccurs="0"/><xs:element name="DOB" type="xs:string" minOccurs="0"/><xs:element name="Resi_no" type="xs:string" minOccurs="0"/><xs:element name="MOBILE_NO" type="xs:string" minOccurs="0"/><xs:element name="Office_no" type="xs:string" minOccurs="0"/><xs:element name="email" type="xs:string" minOccurs="0"/><xs:element name="Qualification" type="xs:string" minOccurs="0"/><xs:element name="Occupation" type="xs:string" minOccurs="0"/><xs:element name="Jamaat" type="xs:string" minOccurs="0"/><xs:element name="Jamiaat" type="xs:string" minOccurs="0"/><xs:element name="Tanzeem_ID" type="xs:decimal" minOccurs="0"/><xs:element name="Nationality" type="xs:string" minOccurs="0"/><xs:element name="city" type="xs:string" minOccurs="0"/><xs:element name="country" type="xs:string" minOccurs="0"/><xs:element name="Alvazarat_Name" type="xs:string" minOccurs="0"/><xs:element name="Alvazarat_id" type="xs:decimal" minOccurs="0"/><xs:element name="Languages" type="xs:string" minOccurs="0"/><xs:element name="website" type="xs:string" minOccurs="0"/><xs:element name="Jamea_Attended" type="xs:string" minOccurs="0"/><xs:element name="FariqDaraja" type="xs:decimal" minOccurs="0"/><xs:element name="Hobbies" type="xs:string" minOccurs="0"/><xs:element name="arabic_fullname" type="xs:string" minOccurs="0"/></xs:sequence></xs:complexType></xs:element></xs:choice></xs:complexType></xs:element></xs:schema><diffgr:diffgram xmlns:msdata="urn:schemas-microsoft-com:xml-msdata" xmlns:diffgr="urn:schemas-microsoft-com:xml-diffgram-v1"><NewDataSet xmlns=""><Table diffgr:id="Table1" msdata:rowOrder="0"><Mumin_id>50476734</Mumin_id><FullName>Zainab bai Mulla Mustafa bhai Rampurawala</FullName><First_name>Zainab</First_name><Middle_Prefix>Mulla </Middle_Prefix><Middle_Name>Mustafa </Middle_Name><Surname>Rampurawala</Surname><gender>F</gender><age>35</age><Address>mufaddal manzil burhani centenary park bhestan</Address><DOB>01 Jul 1979 </DOB><MOBILE_NO>+919879749952</MOBILE_NO><email>janah.mustafa@gmail.com</email><Qualification>Under Graduate</Qualification><Occupation>House Person</Occupation><Jamaat>Surat_Bhestan_Jamaat</Jamaat><Jamiaat>Surat</Jamiaat><Tanzeem_ID>1814</Tanzeem_ID><Nationality>Indian</Nationality><city>Surat</city><Alvazarat_Name>Burhani Centenary Park (Bhestan)</Alvazarat_Name><Alvazarat_id>1424</Alvazarat_id><Languages>English, Gujrati, Hindi, Marathi</Languages><Jamea_Attended>No</Jamea_Attended><arabic_fullname>زينب بائي ملا مصطفى بهائي رامپوره والا</arabic_fullname></Table></NewDataSet></diffgr:diffgram></EduDept_MuminDetailResult></EduDept_MuminDetailResponse></soap:Body></soap:Envelope>';

    log_text_admin(__FILE__ . '-response text-' . $response);

    if ($response == FALSE) {
      return FALSE;
    }
    $xml = simplexml_load_string($response);

    $mumin_data = $xml->xpath('//Table');
    if (is_array($mumin_data) && count($mumin_data) > 0) {
      $mumin_data = $mumin_data[0];

      //TODO mmm copy to main (non admin user class also
      if ($mumin_data->Remark == "Access Denied") {
        return FALSE;
      }

      $this->set_its_id($mumin_data->Mumin_id);
      $this->set_first_prefix($mumin_data->prefix);
      $this->set_first_name($mumin_data->First_name);
      $this->set_middle_prefix($mumin_data->Middle_Prefix);
      $this->set_middle_name($mumin_data->Middle_Name);
      $this->set_last_name($mumin_data->Surname);
      $this->set_gender($mumin_data->gender);
      $this->set_address($mumin_data->Address);
// TODO mmm: correct the date format to (Y-m-d)
      $this->set_dob($mumin_data->DOB);
      $this->set_mobile($mumin_data->MOBILE_NO);
      $this->set_email($mumin_data->email);
// TODO mmm: create following four vars and their respective getter setter
      $this->set_qualification($mumin_data->Qualification);
      $this->set_occupation($mumin_data->Occupation);
      $this->set_jamaat($mumin_data->Jamaat);
      $this->set_jamiat($mumin_data->Jamiaat);
      $this->set_tanzeem_id($mumin_data->Tanzeem_ID);
      $this->set_nationality($mumin_data->Nationality);
      $this->set_city($mumin_data->city);
      $this->set_country($mumin_data->country);

      return TRUE;
    } else {
      return FALSE;
    }
  }

  private function get_mumeen_photo_from_its($its) {

    $key = ITS_KEY;

    $soapUrl = "http://www.its52.com/eJas/EjamaatServices.asmx";

    $xml_post_string = '<?xml version="1.0" encoding="utf-8"?>
<soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
  <soap:Body>
    <GetMuminNewPhoto xmlns="http://localhost/eJAS/EjamaatServices">
      <EjamaatId>' . $its . '</EjamaatId>
      <strKey>' . $key . '</strKey>
    </GetMuminNewPhoto>
  </soap:Body>
</soap:Envelope>';

    $headers = array(
        "Content-type: text/xml;charset=\"utf-8\"",
        "Accept: text/xml",
        "Cache-Control: no-cache",
        "Pragma: no-cache",
        "SOAPAction: http://localhost/eJAS/EjamaatServices/GetMuminNewPhoto",
        "Content-length: " . strlen($xml_post_string)
    );

    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $soapUrl);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_POST, true);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $xml_post_string); // the SOAP request
    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

    $response = curl_exec($ch);
    curl_close($ch);

    log_text_admin(__FILE__ . '-Get Mumin Photo response text-' . $response);

    if ($response == FALSE) {
      return FALSE;
    }

    $result = preg_match_all('/<GetMuminNewPhotoResult>(.*)<\/GetMuminNewPhotoResult>/imUs', $response, $imgs);

    if (is_array($imgs) && isset($imgs[1][0])) {
      return 'data:image/png;base64,' . $imgs[1][0];
    } else {
      return FALSE;
    }
  }

  /**
   * This function checks if user exists: calls load
   *  if not then it gets data from ITS52 and adds record to database
   * @param type $user_its
   * @return type true/false - will always return true.
   */
  public function check_user_exist($user_its) {
    global $db;

    $query = "SELECT IFNULL(COUNT(*), 0) TOTAL_USER  FROM `tlb_user_admin` WHERE `its_id` LIKE '$user_its'";
    $result = $db->query_fetch_full_result($query);
    $result = $result[0];
    if (!$result['TOTAL_USER'] > 0) {
// Get details from ITS
      if ($this->get_mumeen_detail_from_its($user_its)) {
// Insert new record
        $this->add_to_db();
      } else {
        return FALSE;
      }
    }
    $this->loaduser($user_its);
    return TRUE;
  }

  public function add_to_db() {
    global $db;

//TODO mmm user_type has to be calculated from occupation? 
//only the prefer fields have been left out here.

    $sql = "INSERT INTO `tlb_user_admin` (`its_id`, `first_prefix`, `first_name`, `middle_prefix`, `middle_name`, `last_name`, `gender`, `dob`, `nationality`, `address`, `country`, `state`, `city`, `mobile`, `telephone`, `email` , `tanzeem_id`, `qualification`, `occupation`, `jamaat`, `jamiat`, `last_update_ts`, `password`, ) VALUES ( '$this->its_id', '$this->first_prefix', '$this->first_name', '$this->middle_prefix', '$this->middle_name', '$this->last_name', '$this->gender', '$this->dob', '$this->nationality', '$this->address', '$this->country', '$this->state', '$this->city', '$this->mobile', '$this->telephone', '$this->email', '$this->tanzeem_id', '$this->qualification', '$this->occupation', '$this->jamaat', '$this->jamiat', NOW(), 'pwd')";

    log_text_admin(__FILE__ . '-add_to_db-' . $sql);

    $ret = $db->query($sql);
    if ($ret)
      return TRUE;
  }

  public function update_data($data) {
    global $db;
    $data = $db->clean_data($data);
    $updated = FALSE;

    //$sql = "UPDATE tlb_user SET ";
    foreach ($data as $key => $value) {
      switch ($key) {
        case 'prefix':
          if ($value != $this->first_prefix) {
            //$sql = $sql . "$key = '$value' ,";
            $this->set_first_prefix($value);
            $updated = TRUE;
          }
          break;
        case 'first_name':
          if ($value != $this->first_name) {
            //$sql = $sql . "$key = '$value' ,";
            $this->set_first_name($value);
            $updated = TRUE;
          }
          break;
        case 'father_prefix':
          if ($value != $this->middle_prefix) {
            //$sql = $sql . "$key = '$value' ,";
            $this->set_middle_prefix($value);
            $updated = TRUE;
          }
          break;
        case 'father_name':
          if ($value != $this->middle_name) {
            //$sql = $sql . "$key = '$value' ,";
            $this->set_middle_name($value);
            $updated = TRUE;
          }
          break;
        case 'surname':
          if ($value != $this->last_name) {
            //$sql = $sql . "$key = '$value' ,";
            $this->set_last_name($value);
            $updated = TRUE;
          }
          break;
        case 'email':
          if ($value != $this->email) {
            //$sql = $sql . "$key = '$value' ,";
            $this->set_email($value);
            $updated = TRUE;
          }
          break;
        case 'mobile':
          if ($value != $this->mobile) {
            //$sql = $sql . "$key = '$value' ,";
            $this->set_mobile($value);
            $updated = TRUE;
          }
          break;
        case 'prefer_email':
          if ($value != $this->prefer_email) {
            //$sql = $sql . "$key = '$value' ,";
            $this->set_prefer_email($value);
            $updated = TRUE;
          }
          break;
        case 'prefer_whatsapp':
          if ($value != $this->prefer_whatsapp) {
            $this->set_prefer_whatsapp($value);
            //$sql = $sql . "$key = '$value' ,";
            $updated = TRUE;
          }
          break;
        case 'prefer_call':
          if ($value != $this->prefer_call) {
            $sql = $sql . "$key = '$value' ,";
            //$this->set_prefer_call($value);
            $updated = TRUE;
          }
          break;
        case 'subscribe':
          if ($value != $this->subscribe) {
            $sql = $sql . "$key = '$value' ,";
            //$this->set_prefer_call($value);
            $updated = TRUE;
          }
          break;
      }
    }
//checkboxes will not return off 
    if (!isset($data['prefer_email'])) {
      $this->set_prefer_email(0);
      //$sql .= " prefer_email = '0',";
      $updated = TRUE;
    }
    if (!isset($data['prefer_whatsapp'])) {
      $this->set_prefer_whatsapp(0);
      //$sql .= " prefer_whatsapp = '0',";
      $updated = TRUE;
    }
    if (!isset($data['prefer_call'])) {
      $this->set_prefer_call(0);
      //$sql .= " prefer_call = '0',";
      $updated = TRUE;
    }

//remove last coma
    //$sql = rtrim($sql, ",");
    //$sql = "$sql WHERE its_id = '$data[its_id]'";


    if ($updated) {
      //$ret = $db->query($sql);
      $ret = $this->update_user();
      if ($ret) {
        return array('status' => 1);
      } else {
        $error = $db->get_last_err();
        return array('status' => 0, 'error' => $error);
      }
    } else {
      return array('status' => 1);
    }
  }

  public function update_user() {
    global $db;
    //$data = $db->clean_data($data);

    $sql = "UPDATE tlb_user_admin SET `first_prefix`='$this->first_prefix', `first_name`='$this->first_name', `middle_prefix`='$this->middle_prefix', `middle_name`='$this->middle_name', `last_name`='$this->last_name', `gender`='$this->gender', `dob`='$this->dob', `nationality`='$this->nationality', `address`='$this->address', `pincode`='$this->pincode', `country`='$this->country', `state`='$this->state', `city`='$this->city', `mobile`='$this->mobile', `telephone`='$this->telephone', `email`='$this->email', `user_type`='$this->user_type', `prefer_email`='$this->prefer_email', `prefer_whatsapp`='$this->prefer_whatsapp', `prefer_call`='$this->prefer_call', `tanzeem_id`='$this->tanzeem_id', `last_update_ts`='$this->last_update_ts', `qualification`='$this->qualification', `occupation`='$this->occupation', `jamaat`='$this->jamaat', `jamiat`='$this->jamiat' WHERE its_id = $this->its_id";

    $ret = $db->query($sql);
    if ($ret) {
      return TRUE;
    } else {
      log_text_admin(__FILE__ . " update_user " . $db->get_last_err());
      return FALSE;
    }
  }

  public function get_user_data_by_its($user_its) {
    global $db;
    $query = "SELECT * FROM `tlb_user` WHERE `its_id` LIKE '$user_its'";
    $result = $db->query_fetch_full_result($query);
    return $result;
  }

  public function get_all_counselors($counselor_id = FALSE) {
    global $db;
    $query = "SELECT * FROM `tlb_user_admin` WHERE `user_type` LIKE '".ROLE_COUNSELOR."'";
    ($counselor_id) ? $query .= " AND `user_id` = '$counselor_id'" : '';
    
    $result = $db->query_fetch_full_result($query);
    return $result;
  }

  public function get_all_sahebs($saheb_id = FALSE) {
    global $db;
    $query = "SELECT * FROM `tlb_user_admin` WHERE `user_type` LIKE '".ROLE_SAHEB."'";
    ($saheb_id) ? $query .= " AND `user_id` = '$saheb_id'" : '';
    
    $result = $db->query_fetch_full_result($query);
    return $result;
  }
  
  public function get_all_teachers($teacher_id = FALSE) {
    global $db;
    $query = "SELECT * FROM `tlb_user_admin` WHERE `user_type` LIKE '".ROLE_TEACHER."'";
    ($teacher_id) ? $query .= " AND `user_id` = '$teacher_id'" : '';
    
    $result = $db->query_fetch_full_result($query);
    return $result;
  }

  public function log_in($user_name, $password) {
    global $db;
    $query = "SELECT * FROM `tlb_user_admin` WHERE `username` = '$user_name'";

    $result = $db->query_fetch_full_result($query);
    if ($result) {

      if (md5($password) == $result[0]['password']) {
        $this->set_user_type($result[0]['user_type']);
        $this->set_username($result[0]['username']);
        $this->set_user_id($result[0]['user_id']);
        $this->set_its_id($result[0]['its_id']);
        $this->set_jamiat($result[0]['jamiat']);
        $isAuthorized['success'] = TRUE;
        return $isAuthorized;
      } else {
        $isAuthorized['success'] = FALSE;
        $_SESSION['error'] = 'Incorrect Password.';
        return $isAuthorized;
      }
    } else {
      $isAuthorized['success'] = FALSE;
      $_SESSION['error'] = 'Username not found';
      return $isAuthorized;
    }
  }

  public function get_all_user($user_id = FALSE, $clause = FALSE) {
    global $db;
    $query = "SELECT * FROM `tlb_user_admin`";
    if($clause == 'asharah_admin'){
      $query .= " WHERE `user_type` = ".ROLE_ASHARAH_ADMIN."";
    }
    if ($user_id) {
      $query = "SELECT * FROM `tlb_user_admin` WHERE `user_id` = '$user_id'";
      $result = $db->query_fetch_full_result($query);
      return $result[0];
    }
    
    $result = $db->query_fetch_full_result($query);
    return $result;
  }

  public function check_user_already_exist($userid) {
    global $db;
    $query = "SELECT * FROM `tlb_user_admin` WHERE `username` LIKE '$userid' LIMIT 1";
    $result = $db->query_fetch_full_result($query);
    if ($result) {
      return 1;
    } else {
      return 0;
    }
  }

  public function check_old_password($old_pwd, $id) {
    global $db;
    $query = "SELECT `password` FROM `tlb_user_admin` WHERE `user_id` LIKE '$id'";
    $result = $db->query_fetch_full_result($query);
    if ($result[0]['password'] == md5($old_pwd)) {
      return 1;
    } else {
      return 0;
    }
  }

  public function add_user($its, $name, $email, $mobile, $userid, $password, $user_role, $jamiat) {
    global $db;
    $pwd = md5($password);

    $query = "INSERT INTO tlb_user_admin(`its_id`,`username`,`password`,`user_type`,`full_name`, `email`, `mobile`, `jamiat`) VALUES('$its','$userid','$pwd','$user_role','$name','$email','$mobile','$jamiat')";

    $result = $db->query($query);
    return $result;
  }

  public function update_admin_user($name, $email, $mobile, $confirm_password, $userid, $user_role, $jamiat) {
    global $db;
    $query = "UPDATE `tlb_user_admin` SET `full_name` = '$name', `email` = '$email', `mobile` = '$mobile', `user_type` = '$user_role', `jamiat` = '$jamiat' WHERE `user_id` = '$userid'";
    $result = $db->query($query);
    if ($confirm_password != '') {
      $confirm = md5($confirm_password);
      $query = "UPDATE `tlb_user` SET `password` = '$confirm' WHERE `user_id` LIKE '$userid'";
      $result = $db->query($query);
    }
    return $result;
  }
  
  public function get_its_id_records($its_id) {
    global $db;
    $query = "SELECT * FROM `tlb_user` WHERE `its_id` LIKE '$its_id'";
    $result = $db->query_fetch_full_result($query);
    
    if ($result) {
      $user_its = $result[0]['its_id'];
      $full_name = $result[0]['first_prefix']. ' ' . $result[0]['first_name'] . ' ' . $result[0]['middle_prefix'] . ' ' . $result[0]['middle_name'] . ' ' . $result[0]['last_name'];
      $full_name = trim(str_replace('  ', ' ', $full_name));
      
      $ret = array('user_its' => $user_its, 'user_full_name' => $full_name);
      return $ret;
    }
    return $result;
  }
  
  public function get_full_name_of_user_its($its_id) {
    global $db;
    $query = "SELECT * FROM `tlb_user` WHERE `its_id` LIKE '$its_id'";
    $result = $db->query_fetch_full_result($query);
    
    if ($result) {
      $full_name = $result[0]['first_prefix']. ' ' . $result[0]['first_name'] . ' ' . $result[0]['middle_prefix'] . ' ' . $result[0]['middle_name'] . ' ' . $result[0]['last_name'];
      $full_name = trim(str_replace('  ', ' ', $full_name));
    }
    return $full_name;
  }
  
  public function delete_user($id) {
    global $db;
    $query = "DELETE FROM `tlb_user_admin` WHERE `user_id` LIKE '$id'";
    $result = $db->query($query);
    return $result;
  }

  /*
   * GETTERs and SETTERs 
   * --------------------------
   */

  public function get_age() {
    return $this->age;
  }

  public function get_arb_name() {
    return $this->arb_name;
  }

  public function get_eng_name() {
    return $this->eng_name;
  }

  public function get_subscribe() {
    return $this->subscribe;
  }

  public function get_user_id() {
    return $this->user_id;
  }

  public function get_its_id() {
    return $this->its_id;
  }

  public function get_password() {
    return $this->password;
  }

  public function get_full_name() {
    $name = $this->first_prefix . ' ' . $this->first_name . ' ' . $this->middle_prefix . ' ' . $this->middle_name . ' ' . $this->last_name;
    $name = str_replace('  ', ' ', $name);
    return trim($name);
  }

  public function get_first_prefix() {
    return $this->first_prefix;
  }

  public function get_first_name() {
    return $this->first_name;
  }

  public function get_middle_prefix() {
    return $this->middle_prefix;
  }

  public function get_middle_name() {
    return $this->middle_name;
  }

  public function get_last_name() {
    return $this->last_name;
  }

  public function get_gender() {
    return $this->gender;
  }

  public function get_dob() {
    return $this->dob;
  }

  public function get_nationality() {
    return $this->nationality;
  }

  public function get_address() {
    return $this->address;
  }

  public function get_country() {
    return $this->country;
  }

  public function get_state() {
    return $this->state;
  }

  public function get_city() {
    return $this->city;
  }

  public function get_mobile() {
    return $this->mobile;
  }

  public function get_telephone() {
    return $this->telephone;
  }

  public function get_email() {
    return $this->email;
  }

  public function get_tanzeem_id() {
    return $this->tanzeem_id;
  }

  public function get_last_update_ts() {
    return $this->last_update_ts;
  }

  public function get_user_type() {
    return $this->user_type;
  }

  public function get_prefer_email() {
    return $this->prefer_email;
  }

  public function get_prefer_whatsapp() {
    return $this->prefer_whatsapp;
  }

  public function get_prefer_call() {
    return $this->prefer_call;
  }

  public function get_occupation() {
    return $this->occupation;
  }

  public function get_designation() {
    return $this->designation;
  }

  public function get_qualification() {
    return $this->qualification;
  }

  public function get_jamaat() {
    return $this->jamaat;
  }

  public function get_jamiat() {
    return $this->jamiat;
  }

  public function get_mumin_photo($its) {
    return $this->get_mumeen_photo_from_its($its);
  }

  public function get_pincode() {
    return $this->pincode;
  }

  public function get_username() {
    return $this->username;
  }

  /*
   * -Setters
   */

  public function set_username($name) {
    $this->username = $name;
  }

  public function set_age($age) {
    $this->age = $age;
  }

  public function set_arb_name($arb_name) {
    $this->arb_name = $arb_name;
  }

  public function set_eng_name($eng_name) {
    $this->eng_name = $eng_name;
  }

  public function set_subscribe($subscribe) {
    $this->subscribe = $subscribe;
  }

  public function set_user_id($user_id) {
    $user_id = (int) $user_id;
    if (is_int($user_id)) {
      $this->user_id = $user_id;
    } else {
      return false;
    }
  }

  public function set_its_id($its_id) {
    $itid = (int) $its_id;
    if (is_int($itid)) {
      $this->its_id = $its_id;
    } else {
      return false;
    }
  }

  public function set_password($password) {
    $this->password = $password;
  }

  public function set_first_prefix($first_prefix) {
    $this->first_prefix = $first_prefix;
  }

  public function set_first_name($first_name) {
    $this->first_name = $first_name;
  }

  public function set_middle_prefix($middle_prefix) {
    $this->middle_prefix = $middle_prefix;
  }

  public function set_middle_name($middle_name) {
    $this->middle_name = $middle_name;
  }

  public function set_last_name($last_name) {
    $this->last_name = $last_name;
  }

  public function set_gender($gender) {
    $this->gender = $gender;
  }

  public function set_dob($dob) {
    $my_dob = date('Y-m-d', strtotime($dob));
    $date_array = explode('-', $my_dob);
    if (checkdate($date_array[1], $date_array[2], $date_array[0])) {
      $this->dob = $my_dob;
    } else {
      return false;
    }
  }

  public function set_nationality($nationality) {
    $this->nationality = $nationality;
  }

  public function set_address($address) {
    $this->address = $address;
  }

  public function set_country($country) {
    $this->country = $country;
  }

  public function set_state($state) {
    $this->state = $state;
  }

  public function set_city($city) {
    $this->city = $city;
  }

  public function set_mobile($mobile) {
    $this->mobile = $mobile;
  }

  public function set_telephone($telephone) {
    $this->telephone = $telephone;
  }

  public function set_email($email) {
    $this->email = $email;
  }

  public function set_tanzeem_id($tanzeem_id) {
    $this->tanzeem_id = $tanzeem_id;
  }

  public function set_last_update_ts($last_update_ts) {
    $this->last_update_ts = $last_update_ts;
  }

  /*
   * two values : student , professional
   */

  public function set_user_type($user_type) {
    $this->user_type = $user_type;
  }

  public function set_prefer_email($prefer_email) {
    $this->prefer_email = $prefer_email;
  }

  public function set_prefer_whatsapp($prefer_whatsapp) {
    $this->prefer_whatsapp = $prefer_whatsapp;
  }

  public function set_prefer_call($prefer_call) {
    $this->prefer_call = $prefer_call;
  }

  public function set_occupation($occupation) {
//TODO mmm set user_type here based on occupation value

    $this->occupation = $occupation;
//    if ($occupation == 'student') {
//      $this->set_user_type('student');
//    } else {
//      $this->set_user_type('professional');
//    }
  }

  public function set_qualification($qualification) {
    $this->qualification = $qualification;
  }

  public function set_jamaat($jamaat) {
    $this->jamaat = $jamaat;
  }

  public function set_jamiat($jamiat) {
    $this->jamiat = $jamiat;
  }

  public function set_pincode($pincode) {
    $this->pincode = $pincode;
  }

  /*
   * GETTERs and SETTERs 
   * --------------------------
   */
}

?>
