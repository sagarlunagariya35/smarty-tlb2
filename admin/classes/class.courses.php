<?php
require_once '../classes/class.database.php';

class Mtx_Courses {

  private $id;
  private $title;
  private $slug;
  private $is_active;
  private $created_by_user_id;
  private $created_ts;
  
  private $table_name = 'courses';
  // private $word_table = 'tlb_ohbat_sentence_words';
  

  /*
   * =============== GETTERS ==========================
   */
  public function get_id() {
    return $this->id;
  }

  public function get_title() {
    return $this->title;
  }

  public function get_slug() {
    return $this->slug;
  }

  public function get_is_active() {
    return $this->is_active;
  }

  public function get_created_by_user_id() {
    return $this->created_by_user_id;
  }

  public function get_created_ts() {
    return $this->created_ts;
  }

  public function get_table_name() {
    return $this->table_name;
  }

    /*
   * =============== . / GETTERS ==========================
   */

  function insert_course($title, $slug, $is_active, $miqat_id, $event_time, $description, $user_id) {
    global $db;
    
    // clean slug
    $slug_clean = $this->clean_slug($slug);
    $ts = date('Y-m-d');

    $query = "INSERT INTO `$this->table_name` (`course_title`, `course_slug`, `active`, `miqaat_id`, `event_time`, `description`, `created_user_id`, `created_ts`) VALUES ('$title', '$slug_clean', '$is_active', '$miqat_id', '$event_time', '$description', '$user_id', '$ts')";

    $result = $db->query($query);
    return ($result) ? $db->get_last_insert_id() : FALSE;
  }
  
  function insert_course_question($ques_type, $question, $ques_img, $timer, $option, $correct_answer, $user_id, $course_id) {
    global $db;
    $ts = date('Y-m-d');

    $query = "INSERT INTO `courses_question` (`course_id`,`question_type`,`question`,`ques_img`,`timer`,`options`,`correct_answer`,`created_user_id`,`created_ts`) VALUES ('$course_id','$ques_type','$question','$ques_img','$timer','$option','$correct_answer','$user_id', '$ts')";

    $result = $db->query($query);
    return $result;
  }

  function get_all_courses() {
    global $db;
    $query = "SELECT * FROM `$this->table_name` ORDER BY `course_id` DESC";
    $result = $db->query_fetch_full_result($query);
    return $result;
  }

  function get_course_by_id($id) {
    global $db;
    $query = "SELECT * FROM `$this->table_name` WHERE `course_id` LIKE '$id'";
    $result = $db->query_fetch_full_result($query);
    return $result[0];
  }
  
  function get_courses_question_by_course_id($id) {
    global $db;
    $query = "SELECT * FROM `courses_question` WHERE `course_id` LIKE '$id'";
    $result = $db->query_fetch_full_result($query);
    return $result;
  }
  
  function delete_course_question($id) {
    global $db;
    $query = "DELETE FROM `courses_question` WHERE id = '$id'";
    $result = $db->query($query);
    return $result;
  }
  
  function change_course_status($value, $course_id)
  {
    global $db;
    $query = "UPDATE `$this->table_name` SET `active` = '$value' WHERE `course_id` = '$course_id'";
    $result = $db->query($query);
    return $result;
  }
  
  function get_all_user_taken_courses() {
    global $db;
    $query = "SELECT * FROM `user_take_courses` utc JOIN `tlb_user` tu ON utc.`user_id` = tu.`its_id` GROUP BY utc.`user_id` ORDER BY `id` DESC";
    $result = $db->query_fetch_full_result($query);
    return $result;
  }
  
  function count_total_user_taken_courses() {
    global $db;
    $query = "SELECT COUNT(DISTINCT user_id) as count FROM `user_take_courses`";
    $result = $db->query_fetch_full_result($query);
    return $result[0]['count'];
  }
  
  function get_last_insert_course_question()
  {
    global $db;
    $query = "SELECT `id` FROM `courses_question` ORDER BY `id` DESC";
    $result = $db->query_fetch_full_result($query);
    return $result[0]['id'];
  }

  public function clean_slug($slug){
    // make all small case
    $slug = strtolower($slug);
    // remove non alphanumeric
    $slug = preg_replace("/[^a-zA-Z0-9 ]/", "", $slug);
    // replace space with hyphen
    $slug = str_replace(' ', '-', $slug);
    
    return $slug;
  }
  
}
