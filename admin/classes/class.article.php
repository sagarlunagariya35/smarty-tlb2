<?php
require_once '../classes/class.database.php';

class Article
{
    function insert_article($article_title,$description,$slug,$page_title,$text,$youtube_video,$user_id,$start_date,$end_date,$user_type,$template,$master_menu,$sub_menu,$active,$category)
    {
        global $db;
        
        $query = "INSERT INTO `article`(`article_title`,`desc`,`slug`,`page_title`,`text`,`youtube_video`,`user_id`,`start_date`,`end_date`,`user_type`,`templates`,`menu`,`submenu`,`active`,`category`) VALUES ('$article_title','$description','$slug','$page_title','$text','$youtube_video','$user_id','$start_date','$end_date','$user_type','$template','$master_menu','$sub_menu','$active','$category')";
        
        $result = $db->query($query);
        if($result)
          $id = $db->get_last_insert_id();
        return $id;
        
    }
    
    function edit_article($article_title,$slug,$page_title,$text,$youtube_video,$user_id,$start_date,$end_date,$user_type,$template,$master_menu,$sub_menu,$active,$category,$art_id)
    {
      global $db;
      $query = "UPDATE `article` SET `article_title`='$article_title', `slug`='$slug', `page_title`='$page_title', `text`='$text', `youtube_video`='$youtube_video', `user_id`='$user_id', `start_date`='$start_date', `end_date`='$end_date', `user_type`='$user_type', `templates`='$template', `menu`='$master_menu', `submenu`='$sub_menu', `active`='$active', `category`='$category' WHERE `id`='$art_id'";
      
      $result = $db->query($query);
      return $result;
    }
    
    function get_max_articleID()
    {
        global $db;
        $get_count = "SELECT count(*) AS count FROM `article`";
        $row = $db->query_fetch_full_result($get_count);
        return $row[0]['count'];
    }
    
    function insert_media($table,$arti_id,$filename,$index)
    {
        global $db;
        $query = "INSERT INTO `".$table."`(`arti_id`,`".$table."_url`,`index`)VALUES($arti_id,'$filename',$index)";
        
        $result = $db->query($query);
        if($result)
          $id = $db->get_last_insert_id();
        return $id;
        
    }
    
    function update_media($arti_id,$filename,$id)
    {
        global $db;
        $query = "UPDATE `video` SET `video_thumb` = '$filename' WHERE `arti_id` = '$arti_id' && `id` = '$id'";
        
        $result = $db->query($query);
        return $result;
    }
    
    //For Retrieveing Articles
    function get_all_article() 
    {
        global $db;
        $query = "SELECT * FROM `article`";
        $result = $db->query_fetch_full_result($query);
        return $result;
    }
    
    function get_value_by_id($table,$id)
    {
        global $db;
        $query = "SELECT * FROM ".$table." WHERE id = '".$id."'";
        $result = $db->query_fetch_full_result($query);
        return $result;
    }
    
    function get_media_by_id($table,$id)
    {
        global $db;
        $query = "SELECT * FROM ".$table." where arti_id='".$id."' ";
        $result = $db->query_fetch_full_result($query);
        return $result;
    }
    
    function delete_media_by_id($table,$id)
    {
        global $db;
        $query = "DELETE FROM ".$table." WHERE id='".$id."' ";
        $result = $db->query($query);
        return $result;
    }
    
    function article_active_inactive($arti_id,$value)
    {
        global $db;
        $query = "UPDATE `article` SET `active` = '".$value."' where id = '".$arti_id."'";        
        $result = $db->query($query);
        return $result;
    }
    
    function delete_article($id)
    {
        global $db;
        $query = "DELETE FROM `article` WHERE id='$id'";
        $result = $db->query($query);
        
        $table_array = array('video','audio','image');
        
        foreach ($table_array as $ta){
          $query1 = "DELETE FROM `$ta` WHERE arti_id = '$id'";
          $result1 = $db->query($query1);
        }
        
        return $result;
    }
    
    
    function insert_miqaat_istibsaar($miqaat_title,$slug,$user_id,$year,$logo,$show_pre,$show_in,$show_post)
    {
        global $db;
        
        $query = "INSERT INTO `miqaat_istibsaar`(`miqaat_title`,`slug`,`user_id`,`logo`,`year`,`show_pre_event`,`show_in_event`,`show_post_event`) VALUES ('$miqaat_title','$slug','$user_id','$logo','$year','$show_pre','$show_in','$show_post')";
        
        $result = $db->query($query);
        if($result)
          $id = $db->get_last_insert_id();
        return $id;
        
    }
    
    function edit_miqaat_istibsaar($miqaat_title,$slug,$year,$logo,$show_pre,$show_in,$show_post,$youtube_video,$event_time,$miqaat_istibsaar_id)
    {
      global $db;
      $query = "UPDATE `miqaat_istibsaar` SET `miqaat_title`='$miqaat_title', `slug`='$slug', `year`='$year', `logo`='$logo', `show_pre_event`='$show_pre', `show_in_event`='$show_in', `show_post_event`='$show_post' WHERE `id`='$miqaat_istibsaar_id'";
      
      $result = $db->query($query);
      if($result){
        if($youtube_video){
          $select = "SELECT * FROM `miqaat_youtube` WHERE `miqaat_istibsaar_id` = '$miqaat_istibsaar_id'";
          $miqaat_record = $db->query_fetch_full_result($select);
          if($miqaat_record){
            $miqaat_youtube_id = $miqaat_record[0]['id'];
            $set_youtube = "UPDATE `miqaat_youtube` SET `video_url` = '$youtube_video', `event_time` = '$event_time' WHERE `miqaat_istibsaar_id` = '$miqaat_istibsaar_id' AND `id` = '$miqaat_youtube_id'";
          }else {
            $set_youtube = "INSERT INTO `miqaat_youtube`(`miqaat_istibsaar_id`,`video_url`,`event_time`) VALUES ('$miqaat_istibsaar_id','$youtube_video','$event_time')";
          }
          $res = $db->query($set_youtube);
        }
      }
      return $result;
    }
    
    function insert_miqaat_media($table,$miqaat_id,$filename,$event_time)
    {
        global $db;
        $query = "INSERT INTO `miqaat_$table`(`miqaat_istibsaar_id`,`".$table."_url`,`event_time`)VALUES($miqaat_id,'$filename','$event_time')";
        
        $result = $db->query($query);
        if($result)
          $id = $db->get_last_insert_id();
        return $id;
        
    }
    
    function update_miqaat_media($miqaat_id,$filename,$id)
    {
        global $db;
        $query = "UPDATE `miqaat_video` SET `video_thumb` = '$filename' WHERE `miqaat_istibsaar_id` = '$miqaat_id' && `id` = '$id'";
        
        $result = $db->query($query);
        return $result;
    }
    
    function update_miqaat_media_sr_no($sr_no,$category,$name,$id,$keyword)
    {
      global $db;
      $query = "UPDATE `miqaat_$keyword` SET `sr_no` = '$sr_no', `category` = '$category', `name` = '$name' WHERE `id` = '$id'";
      $result = $db->query($query);
      return $result;
    }
    
    function get_all_miqaat_istibsaar() 
    {
        global $db;
        $query = "SELECT * FROM `miqaat_istibsaar`";
        $result = $db->query_fetch_full_result($query);
        return $result;
    }
    
    function delete_miqaat_istibsaar($id)
    {
        global $db;
        $query = "DELETE FROM `miqaat_istibsaar` WHERE id='$id'";
        $result = $db->query($query);
        
        $table_array = array('miqaat_video','miqaat_audio','miqaat_stationary');
        
        foreach ($table_array as $ta){
          $query1 = "DELETE FROM `$ta` WHERE miqaat_istibsaar_id = '$id'";
          $result1 = $db->query($query1);
        }
        
        return $result;
    }
    
    function delete_miqaat_istibsaar_logo($id)
    {
        global $db;
        $query = "UPDATE `miqaat_istibsaar` SET `logo` = '' WHERE id = '$id'";
        $result = $db->query($query);
        return $result;
    }
    
    function get_miqaat_media_by_id($table,$id)
    {
        global $db;
        $query = "SELECT * FROM `$table` WHERE miqaat_istibsaar_id = '$id' ORDER BY `sr_no`";
        $result = $db->query_fetch_full_result($query);
        return $result;
    }
}
