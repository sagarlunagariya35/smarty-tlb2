<?php
require_once '../classes/class.database.php';

class Mtx_Crs_Chapter {

  function insert_chapter($course, $title, $slug, $description, $img_icon, $start_date, $end_date, $sort, $user_id, $is_active) {
    global $db;
    
    $timestamp = date('Y-m-d');

    $query = "INSERT INTO `crs_chapter` (`course_id`, `title`, `slug`, `description`, `img_icon`, `start_date`, `end_date`, `sort_id`, `created_ts`, `created_by`, `is_active`) VALUES ('$course', '$title', '$slug', '$description', '$img_icon', '$start_date', '$end_date', '$sort', '$timestamp', '$user_id', '$is_active')";

    $result = $db->query($query);
    return $result;
  }
  
  function update_chapter($course, $title, $slug, $description, $img_icon, $start_date, $end_date, $sort, $is_active, $id) {
    global $db;
    $query = "UPDATE `crs_chapter` SET `course_id` = '$course', `title` = '$title', `slug` = '$slug', `description` = '$description', `img_icon` = '$img_icon', `start_date` = '$start_date', `end_date` = '$end_date', `sort_id` = '$sort', `is_active` = '$is_active' WHERE `id` = '$id'";
    $result = $db->query($query);
    return $result;
  }
  
  function insert_elements($chapter_id, $element_type, $element_title, $element_text, $element_desc, $sort, $user_id, $is_active) {
      global $db;
      $timestamp = date('Y-m-d');
      
      $query = "INSERT INTO `crs_element` (`chapter_id`, `element_type`, `element_title`, `element_text`, `description`, `sort_id`, `created_ts`, `created_by`, `is_active`) VALUES('$chapter_id', '$element_type', '$element_title', '$element_text', '$element_desc', '$sort', '$timestamp', '$user_id', '$is_active')";
      
      $result = $db->query($query);
      return $result;
  }
  
  function get_all_chapters($course = FALSE, $status = FALSE, $sort = 'ASC') {
    global $db;
    $query = "SELECT * FROM `crs_chapter` WHERE ";
    
    $query .= ($status) ? "`is_active` LIKE '$status' " : "(`is_active` LIKE '0' OR '1') ";
    ($course) ? $query .= "AND `course_id` LIKE '$course' " : '';
    
    $query .= "ORDER BY `sort_id` $sort";
    $result = $db->query_fetch_full_result($query);
    return $result;
  }
  
  function get_title_of_chapters($chapter) {
    global $db;
    $query = "SELECT * FROM `crs_chapter` WHERE `title` LIKE '%$chapter%' AND `is_active` LIKE '1' ORDER BY `title` ASC";
    $result = $db->query_fetch_full_result($query);
    return $result;
  }
  
  function get_chapter($id) {
    global $db;
    $query = "SELECT * FROM `crs_chapter` WHERE `id` LIKE '$id'";
    $result = $db->query_fetch_full_result($query);
    return $result[0];
  }
  
  function get_last_insert_chapter()
  {
    global $db;
    $query = "SELECT `id` FROM `crs_chapter` ORDER BY `id` DESC";
    $result = $db->query_fetch_full_result($query);
    return $result[0]['id'];
  }
  
  function change_chapter_status($status, $id)
  {
    global $db;
    $query = "UPDATE `crs_chapter` SET `is_active` = '$status' WHERE `id` = '$id'";
    $result = $db->query($query);
    return $result;
  }
  
  function delete_chapter($id) {
    global $db;
    $query = "DELETE FROM `crs_chapter` WHERE id = '$id'";
    $result = $db->query($query);
    return $result;
  }

  
}
