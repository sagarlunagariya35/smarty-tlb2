<?php
require_once '../classes/class.database.php';

class Mtx_Survey {
  
  function get_title_of_surveys(){
    global $db;
    $query = "SELECT * FROM txn_open_surveys GROUP BY `title`";
    $result = $db->query_fetch_full_result($query);
    return $result;
  }
  
  function get_survey_title($survey_id){
    global $db;
    $query = "SELECT `title` FROM txn_open_surveys WHERE `survey_id` = '$survey_id' GROUP BY `survey_id`";
    $result = $db->query_fetch_full_result($query);
    return $result[0]['title'];
  }
  
  function get_survey_data_list($survey_id){
    global $db;
    $query = "SELECT * FROM srv_survey_ques_answers WHERE `survey_id` = '$survey_id' ORDER BY `user_id`,`que_id`";
    $result = $db->query_fetch_full_result($query);
    
    if ($result) {
      foreach ($result as $data) {
        $text = $data['answer'];
        ($data['answer2']) ? $text .= ': '.$data['answer2'] : '';
        
        $ary_data[$data['user_id']][$this->get_survey_1_ans_id($data['que_id'])] = $text;
      }
    }
    return $ary_data;
  }
  
   function get_survey_1_ans_id($db_ans_id){
     $ary_map = array(
         1 => 1,
         42 => 2,
         2 => 3,
         15 => 4,
         16 => 5,
         43 => 6,
         44 => 7,
         33 => 8,
         34 => 9,
         35 => 10,
         36 => 11,
         37 => 12,
         38 => 13,
         39 => 14,
         40 => 15,
         41 => 16,
         17 => 17,
         18 => 18,
         19 => 19,
         20 => 20,
         21 => 21,
         22 => 22,
         23 => 23,
         24 => 24,
         25 => 25,
         26 => 26,
         27 => 27,
         28 => 28,
         29 => 29,
         30 => 30,
         31 => 31,
         32 => 32,
         3 => 33,
         4 => 34,
         5 => 35,
         6 => 36,
         7 => 37,
         8 => 38,
         9 => 39,
         10 => 40,
         11 => 41,
         12 => 42,
         13 => 43,
         14 => 44,
         45 => 45
     );
     
     return $ary_map[$db_ans_id];
   }
}
