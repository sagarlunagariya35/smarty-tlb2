<?php

class Mtx_Ohbat_Sentences {

  private $id;
  private $miqat_id;
  private $miqat_title;
  private $miqat_miqaat_slug;
  private $title;
  private $slug;
  private $bayan;
  private $question;
  private $created_by_user_id;
  private $created_by_user_name;
  private $created_ts;
  private $active;
  
  private $table_name = 'tlb_ohbat_daily_sentences';
  // private $word_table = 'tlb_ohbat_sentence_words';
  

  /*
   * =============== GETTERS ==========================
   */

  public function get_id() {
    return $this->id;
  }

  public function get_miqat_id() {
    return $this->miqat_id;
  }

  public function get_miqat_title() {
    return $this->miqat_title;
  }

  public function get_miqat_miqaat_slug() {
    return $this->miqat_miqaat_slug;
  }

  public function get_title() {
    return $this->title;
  }

  public function get_slug() {
    return $this->slug;
  }

  public function get_bayan() {
    return $this->bayan;
  }

  public function get_question() {
    return $this->question;
  }

  public function get_created_by_user_id() {
    return $this->created_by_user_id;
  }

  public function get_created_by_user_name() {
    return $this->created_by_user_name;
  }

  public function get_created_ts() {
    return $this->created_ts;
  }

  public function get_active() {
    return $this->active;
  }

  /*
   * =============== . / GETTERS ==========================
   */

  function insert_article($miqaat_id, $event_time, $sr, $slug, $title, $title_eng, $bayan_title, $ar_bayan_title_file_name, $bayan, $ar_file_name, $question, $ar_ques_file_name, $active, $words, $ar_word_file_name, $question_eng, $en_ques_file_name, $en_bayan_title, $en_bayan_title_file_name, $bayan_eng, $en_file_name, $words_eng, $en_word_file_name) {
    global $db;
    
    // clean slug
    $slug_clean = $this->clean_slug($slug);
    $user_id = $_SESSION[USER_ID];
    $ts = date('Y-m-d');

    $query = "INSERT INTO `$this->table_name` (`miqaat_id`, `event_time`, `sr`, `slug`, `title`, `title_eng`, `bayan_title`, `ar_bayan_title_image`, `bayan`, `ar_bayan_image`, `question`, `ar_ques_image`,`user_id`, `ts`, `active`, `word_meaning`, `ar_word_image`, `question_eng`, `en_ques_image`, `en_bayan_title`, `en_bayan_title_image`, `bayan_eng`, `en_bayan_image`, `word_meaning_eng`, `en_word_image`) VALUES ('$miqaat_id', '$event_time', '$sr', '$slug_clean', '$title', '$title_eng', '$bayan_title', '$ar_bayan_title_file_name', '$bayan', '$ar_file_name', '$question', '$ar_ques_file_name', '$user_id', '$ts', '$active', '$words', '$ar_word_file_name', '$question_eng', '$en_ques_file_name', '$en_bayan_title', '$en_bayan_title_file_name', '$bayan_eng', '$en_file_name', '$words_eng', '$en_word_file_name')";
    
    $result = $db->query($query);
    return ($result) ? $db->get_last_insert_id() : FALSE;
  }

  function edit_article($id, $miqat_id, $event_time, $sr, $slug, $title, $title_eng, $bayan_title, $ar_bayan_title_file_name, $bayan, $ar_file_name, $question, $ar_ques_file_name, $active, $words, $ar_word_file_name, $question_eng, $en_ques_file_name, $en_bayan_title, $en_bayan_title_file_name, $bayan_eng, $en_file_name, $words_eng, $en_word_file_name) {
    global $db;
    $query = "UPDATE `$this->table_name` SET `miqaat_id`='$miqat_id', `event_time`='$event_time', `sr`='$sr', `slug`='$slug', `title`='$title', `title_eng`='$title_eng', `bayan_title`='$bayan_title', `ar_bayan_title_image`='$ar_bayan_title_file_name', `bayan`='$bayan', `ar_bayan_image`='$ar_file_name', `question`='$question', `ar_ques_image`='$ar_ques_file_name', `active`='$active', `word_meaning`='$words', `ar_word_image`='$ar_word_file_name', `question_eng`='$question_eng', `en_ques_image`='$en_ques_file_name', `en_bayan_title`='$en_bayan_title', `en_bayan_title_image`='$en_bayan_title_file_name', `bayan_eng`='$bayan_eng', `en_bayan_image`='$en_file_name', `word_meaning_eng`='$words_eng', `en_word_image`='$en_word_file_name' WHERE `id`='$id'";

    $result = $db->query($query);
    return $result;
  }

  function get_max_articleID() {
    global $db;
    $get_count = "SELECT MAX(id) AS max FROM `$this->table_name`";
    $row = $db->query_fetch_full_result($get_count);
    return $row[0]['max'];
  }

  //For Retrieveing Articles
  function get_all_articles_by_miqat_id($miqat_id) {
    global $db;
    $miqat_clause = ($miqat_id > 0) ? " AND `miqaat_id` LIKE '$miqat_id'" : '';
    $query = "SELECT *, (SELECT miqaat_title FROM miqaat_istibsaar mi WHERE mi.id LIKE t.miqaat_id) miqaat_name FROM `$this->table_name` t WHERE `active` LIKE '1' $miqat_clause ORDER BY id DESC";
    $result = $db->query_fetch_full_result($query);
    return $result;
  }
  
  function get_article_by_id($id)
  {
    global $db;
    $query = "SELECT * FROM `$this->table_name` WHERE id = '$id'";
    $result = $db->query_fetch_full_result($query);
    return $result[0];
  }
  
  function get_last_insert_article()
  {
    global $db;
    $query = "SELECT `id` FROM `$this->table_name` ORDER BY `id` DESC";
    $result = $db->query_fetch_full_result($query);
    return $result[0]['id'];
  }
  
  function get_miqaat_title_by_miqaat_id($id)
  {
    global $db;
    $query = "SELECT * FROM `miqaat_istibsaar` WHERE id = '$id'";
    $result = $db->query_fetch_full_result($query);
    return $result[0];
  }

  function delete_article($id) {
    global $db;
    $query = "DELETE FROM `$this->table_name` WHERE id='$id'";
    $result = $db->query($query);

    return $result;
  }
  
  function delete_ohbat_image($id, $field)
  {
    global $db;
    $query = "UPDATE `$this->table_name` SET `$field` = '' WHERE id = '$id'";
    $result = $db->query($query);
    return $result;
  }

  public function clean_slug($slug){
    // make all small case
    $slug = strtolower($slug);
    // remove non alphanumeric
    $slug = preg_replace("/[^a-zA-Z0-9 ]/", "", $slug);
    // replace space with hyphen
    $slug = str_replace(' ', '-', $slug);
    
    return $slug;
  }
  
}
