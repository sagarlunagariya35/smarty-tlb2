<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title><?php echo $title; ?></title>

    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/custom.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

    <script src="js/jquery-1.11.1.js"></script>
    <script src="js/jquery.confirm.min.js"></script>
    <style>
      .avoidBreak {
        page-break-inside: avoid !important;
        margin: 4px 0 4px 0;  /* to keep the page break from cutting too close to the text in the div */
      }
      div{
        page-break-inside: avoid;
      }
      .alfatemi-text{
        min-height: 20px;
        margin: 0;
        text-transform: none;
      }
      body{
        font-size: 10px;
      }
    </style>
    <script>
      $(document).ready(function () {
        var ctlTd = $('.dontSplit td');
        if (ctlTd.length > 0)
        {
          //console.log('Found ctlTd');
          ctlTd.wrapInner('<div class="avoidBreak" />');
        }
      });
    </script>
  </head>

  <body>
    <div id="wrapper">
