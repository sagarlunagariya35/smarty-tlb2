<?php

// Database credentials
require '../classes/class.database.php';

$counselor_sugest_course = $counselor_sugest_country = $counselor_sugest_city = $counselor_sugest_remarks = FALSE;
$counselor_sugest_timestamp = $counselor_full_name = $counselor_gender = $counselor_email = $counselor_mobile = FALSE;

// Step 1 get the last araz id inserted from the txn_view_araz
$qry = "INSERT INTO txn_view_araz SELECT * FROM view_araz WHERE araz_id > (SELECT MAX(araz_id) last_araz_id FROM txn_view_araz)";
//$rslt_last_araz_id = $db->query($qry);


$araz_query = "SELECT * FROM tlb_araz WHERE id > (SELECT MAX(araz_id) last_araz_id FROM txn_view_araz)";
$araz_result = $db->query_fetch_full_result($araz_query);

if ($araz_result) {
  foreach ($araz_result as $araz_data) {

    // Araz Data
    $araz_id = $araz_data['id'];
    $marhala = $araz_data['marhala'];
    $user_its = $araz_data['login_its'];
    $counselor_id = $araz_data['counselor_id'];
    $araz_type = 'raza';


    // User Details
    $user_query = "SELECT * FROM `tlb_user` WHERE `its_id` = '$user_its'";
    $user_result = $db->query_fetch_full_result($user_query);
    $user_full_name = trim($user_result[0]['first_prefix'] . ' ' . $user_result[0]['first_name'] . ' ' . $user_result[0]['middle_prefix'] . ' ' . $user_result[0]['middle_name'] . ' ' . $user_result[0]['last_name']);
    $user_full_name = preg_replace('!\s+!', ' ', $user_full_name);

    // User Admin Details
    $counselor_query = "SELECT * FROM `tlb_user_admin` WHERE `user_id` = '$counselor_id'";
    $counselor_result = $db->query_fetch_full_result($counselor_query);
    if ($counselor_result) {
      $counselor_full_name = $counselor_result[0]['full_name'];
      $counselor_gender = $counselor_result[0]['gender'];
      $counselor_email = $counselor_result[0]['email'];
      $counselor_mobile = $counselor_result[0]['mobile'];
    }

    // Counselor Details
    $counselor_suggestion_query = "SELECT * FROM `tlb_counselor_suggestion` WHERE `araz_id` = '$araz_id' AND `counselor_id` = '$counselor_id'";
    $counselor_suggestion_result = $db->query_fetch_full_result($counselor_suggestion_query);
    if ($counselor_suggestion_result) {
      $counselor_sugest_course = $counselor_suggestion_result[0]['course'];
      $counselor_sugest_country = $counselor_suggestion_result[0]['country'];
      $counselor_sugest_city = $counselor_suggestion_result[0]['city'];
      $counselor_sugest_remarks = $counselor_suggestion_result[0]['remarks'];
      $counselor_sugest_timestamp = $counselor_suggestion_result[0]['timestamp'];
    }

    if ($marhala > 4) {
      // Courses Details
      $courses_query = "SELECT * FROM `tlb_araz_courses` WHERE `araz_id` = '$araz_id'";
      $courses_result = $db->query_fetch_full_result($courses_query);

      if ($courses_result) {
        if (count($courses_result) > 1) {
          $araz_type = 'istirshad';
        }

        foreach ($courses_result as $courses_data) {

          $insert_query = "INSERT INTO `txn_view_araz`(`araz_id`, `marhala`, `login_its`, `madrasah_darajah`, `madrasah_country`, `madrasah_state`, `madrasah_city`, `madrasah_name`, `madrasah_name_ar`, `user_full_name`, `school_city`, `school_state`, `school_standard`, `school_name`, `araz_ts`, `nisaab`, `school_filter`, `hifz_sanad`, `already_araz_done`, `already_araz_from_name`, `school_pincode`, `current_course`, `current_inst_name`, `current_inst_city`, `current_inst_country`, `inst_start_month`, `inst_start_year`, `inst_end_month`, `inst_end_year`, `attend_later`, `attend_later_ts`, `bazid_jawab`, `jawab_city`, `jawab_given`, `jawab_ts`, `code`, `counselor_id`, `saheb_id`, `coun_assign_ts`, `saheb_assign_ts`, `ho_remarks_araz`, `ho_remarks_counselor`, `ho_remarks_saheb`, `ho_remarks_attend_later`, `printed`, `email_sent_student`, `email_sent_counselor`, `email_sent_saheb`, `email_sent_ho`, `taken_psychometric_aptitude_test`, `text_psychometric_aptitude`, `prefer_more_clarity`, `want_further_guidence`, `seek_funding_edu`, `want_assistance`, `received_sponsorship`, `follow_shariaa`, `disclaimer`, `reviewer_processed`, `reviewer_processed_ts`, `recommend_psychometric_test`, `recommend_counseling`, `recommend_qardan`, `recommend_sponsorship`, `bchet_status`, `test_file_upload`, `counselling_done`, `qardan_hasana_done`, `sponsorship_done`, `araz_course_id`, `stream`, `degree_name`, `course_name`, `institute_name`, `institute_country`, `institute_city`, `accomodation`, `course_duration`, `course_started`, `course_jawab_given`, `full_name_ar`, `dob`, `gender`, `nationality`, `country`, `state`, `city`, `email`, `mobile`, `whatsapp`, `tanzeem_id`, `jamaat`, `jamiat`, `quran_sanad`, `counselor_sugest_course`, `counselor_sugest_country`, `counselor_sugest_city`, `counselor_sugest_remarks`, `counselor_sugest_timestamp`, `counselor_full_name`, `counselor_gender`, `counselor_email`, `counselor_mobile`, `araz_type`) VALUES('$araz_id', '$marhala', '$user_its', '$araz_data[madrasah_darajah]', '$araz_data[madrasah_country]', '$araz_data[madrasah_state]', '$araz_data[madrasah_city]', '$araz_data[madrasah_name]', '$araz_data[madrasah_name_ar]', '$user_full_name', '$araz_data[school_city]', '$araz_data[school_state]', '$araz_data[school_standard]', '$araz_data[school_name]', '$araz_data[araz_ts]', '$araz_data[nisaab]', '$araz_data[school_filter]', '$araz_data[hifz_sanad]', '$araz_data[already_araz_done]', '$araz_data[already_araz_from_name]', '$araz_data[school_pincode]', '$araz_data[current_course]', '$araz_data[current_inst_name]', '$araz_data[current_inst_city]', '$araz_data[current_inst_country]', '$araz_data[inst_start_month]', '$araz_data[inst_start_year]', '$araz_data[inst_end_month]', '$araz_data[inst_end_year]', '$araz_data[attend_later]', '$araz_data[attend_later_ts]', '$araz_data[bazid_jawab]', '$araz_data[jawab_city]', '$araz_data[jawab_given]', '$araz_data[jawab_ts]', '$araz_data[code]', '$counselor_id', '$araz_data[saheb_id]', '$araz_data[coun_assign_ts]', '$araz_data[saheb_assign_ts]', '$araz_data[ho_remarks_araz]', '$araz_data[ho_remarks_counselor]', '$araz_data[ho_remarks_saheb]', '$araz_data[ho_remarks_attend_later]', '$araz_data[printed]', '$araz_data[email_sent_student]', '$araz_data[email_sent_counselor]', '$araz_data[email_sent_saheb]', '$araz_data[email_sent_ho]', '$araz_data[taken_psychometric_aptitude_test]', '$araz_data[text_psychometric_aptitude]', '$araz_data[prefer_more_clarity]', '$araz_data[want_further_guidence]', '$araz_data[seek_funding_edu]', '$araz_data[want_assistance]', '$araz_data[received_sponsorship]', '$araz_data[follow_shariaa]', '$araz_data[disclaimer]', '$araz_data[reviewer_processed]', '$araz_data[reviewer_processed_ts]', '$araz_data[recommend_psychometric_test]', '$araz_data[recommend_counseling]', '$araz_data[recommend_qardan]', '$araz_data[recommend_sponsorship]', '$araz_data[bchet_status]', '$araz_data[test_file_upload]', '$araz_data[counselling_done]', '$araz_data[qardan_hasana_done]', '$araz_data[sponsorship_done]', '$courses_data[id]', '$courses_data[stream]', '$courses_data[degree_name]', '$courses_data[course_name]', '$courses_data[institute_name]', '$courses_data[institute_country]', '$courses_data[institute_city]', '$courses_data[accomodation]', '$courses_data[course_duration]', '$courses_data[course_started]', '$courses_data[jawab_given]', '" . $user_result[0]['full_name_ar'] . "', '" . $user_result[0]['dob'] . "', '" . $user_result[0]['gender'] . "', '" . $user_result[0]['nationality'] . "', '" . $user_result[0]['country'] . "', '" . $user_result[0]['state'] . "', '" . $user_result[0]['city'] . "', '" . $user_result[0]['email'] . "', '" . $user_result[0]['mobile'] . "', '" . $user_result[0]['whatsapp'] . "', '" . $user_result[0]['tanzeem_id'] . "', '" . $user_result[0]['jamaat'] . "', '" . $user_result[0]['jamiat'] . "', '" . $user_result[0]['quran_sanad'] . "', '$counselor_sugest_course', '$counselor_sugest_country', '$counselor_sugest_city', '$counselor_sugest_remarks', '$counselor_sugest_timestamp', '$counselor_full_name', '$counselor_gender', '$counselor_email', '$counselor_mobile', '$araz_type')";

          $insert_result = $db->query($insert_query);
        }
      }
    } else {

      $insert_query = "INSERT INTO `txn_view_araz`(`araz_id`, `marhala`, `login_its`, `madrasah_darajah`, `madrasah_country`, `madrasah_state`, `madrasah_city`, `madrasah_name`, `madrasah_name_ar`, `user_full_name`, `school_city`, `school_state`, `school_standard`, `school_name`, `araz_ts`, `nisaab`, `school_filter`, `hifz_sanad`, `already_araz_done`, `already_araz_from_name`, `school_pincode`, `current_course`, `current_inst_name`, `current_inst_city`, `current_inst_country`, `inst_start_month`, `inst_start_year`, `inst_end_month`, `inst_end_year`, `attend_later`, `attend_later_ts`, `bazid_jawab`, `jawab_city`, `jawab_given`, `jawab_ts`, `code`, `counselor_id`, `saheb_id`, `coun_assign_ts`, `saheb_assign_ts`, `ho_remarks_araz`, `ho_remarks_counselor`, `ho_remarks_saheb`, `ho_remarks_attend_later`, `printed`, `email_sent_student`, `email_sent_counselor`, `email_sent_saheb`, `email_sent_ho`, `taken_psychometric_aptitude_test`, `text_psychometric_aptitude`, `prefer_more_clarity`, `want_further_guidence`, `seek_funding_edu`, `want_assistance`, `received_sponsorship`, `follow_shariaa`, `disclaimer`, `reviewer_processed`, `reviewer_processed_ts`, `recommend_psychometric_test`, `recommend_counseling`, `recommend_qardan`, `recommend_sponsorship`, `bchet_status`, `test_file_upload`, `counselling_done`, `qardan_hasana_done`, `sponsorship_done`, `full_name_ar`, `dob`, `gender`, `nationality`, `country`, `state`, `city`, `email`, `mobile`, `whatsapp`, `tanzeem_id`, `jamaat`, `jamiat`, `quran_sanad`, `counselor_sugest_course`, `counselor_sugest_country`, `counselor_sugest_city`, `counselor_sugest_remarks`, `counselor_sugest_timestamp`, `counselor_full_name`, `counselor_gender`, `counselor_email`, `counselor_mobile`, `araz_type`) VALUES('$araz_id', '$marhala', '$user_its', '$araz_data[madrasah_darajah]', '$araz_data[madrasah_country]', '$araz_data[madrasah_state]', '$araz_data[madrasah_city]', '$araz_data[madrasah_name]', '$araz_data[madrasah_name_ar]', '$user_full_name', '$araz_data[school_city]', '$araz_data[school_state]', '$araz_data[school_standard]', '$araz_data[school_name]', '$araz_data[araz_ts]', '$araz_data[nisaab]', '$araz_data[school_filter]', '$araz_data[hifz_sanad]', '$araz_data[already_araz_done]', '$araz_data[already_araz_from_name]', '$araz_data[school_pincode]', '$araz_data[current_course]', '$araz_data[current_inst_name]', '$araz_data[current_inst_city]', '$araz_data[current_inst_country]', '$araz_data[inst_start_month]', '$araz_data[inst_start_year]', '$araz_data[inst_end_month]', '$araz_data[inst_end_year]', '$araz_data[attend_later]', '$araz_data[attend_later_ts]', '$araz_data[bazid_jawab]', '$araz_data[jawab_city]', '$araz_data[jawab_given]', '$araz_data[jawab_ts]', '$araz_data[code]', '$counselor_id', '$araz_data[saheb_id]', '$araz_data[coun_assign_ts]', '$araz_data[saheb_assign_ts]', '$araz_data[ho_remarks_araz]', '$araz_data[ho_remarks_counselor]', '$araz_data[ho_remarks_saheb]', '$araz_data[ho_remarks_attend_later]', '$araz_data[printed]', '$araz_data[email_sent_student]', '$araz_data[email_sent_counselor]', '$araz_data[email_sent_saheb]', '$araz_data[email_sent_ho]', '$araz_data[taken_psychometric_aptitude_test]', '$araz_data[text_psychometric_aptitude]', '$araz_data[prefer_more_clarity]', '$araz_data[want_further_guidence]', '$araz_data[seek_funding_edu]', '$araz_data[want_assistance]', '$araz_data[received_sponsorship]', '$araz_data[follow_shariaa]', '$araz_data[disclaimer]', '$araz_data[reviewer_processed]', '$araz_data[reviewer_processed_ts]', '$araz_data[recommend_psychometric_test]', '$araz_data[recommend_counseling]', '$araz_data[recommend_qardan]', '$araz_data[recommend_sponsorship]', '$araz_data[bchet_status]', '$araz_data[test_file_upload]', '$araz_data[counselling_done]', '$araz_data[qardan_hasana_done]', '$araz_data[sponsorship_done]', '" . $user_result[0]['full_name_ar'] . "', '" . $user_result[0]['dob'] . "', '" . $user_result[0]['gender'] . "', '" . $user_result[0]['nationality'] . "', '" . $user_result[0]['country'] . "', '" . $user_result[0]['state'] . "', '" . $user_result[0]['city'] . "', '" . $user_result[0]['email'] . "', '" . $user_result[0]['mobile'] . "', '" . $user_result[0]['whatsapp'] . "', '" . $user_result[0]['tanzeem_id'] . "', '" . $user_result[0]['jamaat'] . "', '" . $user_result[0]['jamiat'] . "', '" . $user_result[0]['quran_sanad'] . "', '$counselor_sugest_course', '$counselor_sugest_country', '$counselor_sugest_city', '$counselor_sugest_remarks', '$counselor_sugest_timestamp', '$counselor_full_name', '$counselor_gender', '$counselor_email', '$counselor_mobile', '$araz_type')";

      $insert_result = $db->query($insert_query);
    }
  }
}

// Step 2 get the new araiz from the trlb_araz
// Step 3 get the courses related to the araiz from the tlb_araz_courses
// Step 4 get the user details
// Step 5 get the counselor suggestion

$qry = "UPDATE `txn_view_araz` SET `course_started` = NULL WHERE `course_started` = '-'";
$rslt_last_araz_id = $db->query($qry);

$qry = "UPDATE `txn_view_araz` SET `course_started` = NULL WHERE `course_started` LIKE '-%' OR `course_started` LIKE '%-'";
$rslt_last_araz_id = $db->query($qry);
?>
