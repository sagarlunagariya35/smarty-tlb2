<?php
require_once '../classes/class.database.php';
include 'classes/class.event.php';
$allowed_roles = array(ROLE_HEAD_OFFICE);
require_once 'session.php';

$evnt = new Event();

$title = 'List of Event Registers';
$description = '';
$keywords = '';
$active_page = 'list_event_register';

$select = $event_id = FALSE;

if(isset($_POST['submit'])){
  $remarks = $_POST['remarks'];
  $its_key = (array) $_POST['its_key'];
  $subject = $_POST['subject'];
  
  if($its_key){
    foreach ($its_key as $its){
      $result = sent_email_of_report('event_register','its',$its,$remarks,$subject);
    }
    
    if($result){
      $_SESSION[SUCCESS_MESSAGE] = 'E-mail Sent Successfully..';
    }else {
      $_SESSION[ERROR_MESSAGE] = 'Error Encounter While Sending E-mail';
    }
  }else {
    $_SESSION[ERROR_MESSAGE] = 'Please Select any User for sent E-mail';
  }
}

if(isset($_POST['search'])) {
  $event_id = $_POST['event_id'];
  $select = $evnt->get_event_registers_by_event_id($event_id);
}

$list_events = $evnt->get_all_events();

include ('header.php');
?>

<div class="content-wrapper">
  <form method="post" action="" name="raza_form" class="form-horizontal" onsubmit="return(validate());">
    <section class="content">
      <div class="col-lg-12">
        <h3 class="page-header"><?php echo $title; ?></h3>
      </div>
      <div class="row">
        <label class="col-md-2 control-label">Events</label>
        <div class="col-md-3">
          <select name="event_id" class="form-control">
            <option value="">Select Event</option>
            <?php
             if($list_events){
               foreach ($list_events as $data){
               ?>
                 <option value="<?php echo $data['id']; ?>" <?php echo ($event_id == $data['id']) ? 'selected' : ''; ?>><?php echo $data['event_title']; ?></option> 
               <?php
               }
             }
            ?>
          </select>
        </div>
      <div class="col-md-2">
        <input type="submit" name="search" value="Search" class="btn btn-block btn-success">
      </div>
    </div>
      
      <?php if(isset($_POST['search'])) { ?>
      <div class="row">
        <div class="col-xs-12">&nbsp;</div>
      </div>
      <div class="row">
        <div class="col-xs-12 col-sm-3" style="padding-top:6px">
          <input type="checkbox" name="print" value="Print" id="check_all" onchange="checkAll(this);" class="css-checkbox1"><label for="check_all" class="css-label1">Check All</label>
          <br><br>
          <input type="text" name="subject" placeholder="Enter Subject" class="form-control">
        </div>
        <div class="col-xs-12 col-sm-6">
          <textarea type="text" class="form-control" rows="4" name="remarks" placeholder="Enter Text"></textarea>
        </div>
        <div class="col-xs-12 col-sm-3">
          <input type="submit" name="submit" value="Send Email" class="btn btn-success btn-block assign-jawab">
          <br>
          <a href="print_event_register.php?event_id=<?php echo $event_id; ?>" target="_blank" class="btn btn-success btn-block pull-right">Print</a>
        </div>
      </div>
      
      <div class="row">
        <div class="clearfix"><br><br></div>
        <div class="panel panel-primary">
          <div class="panel-heading">
            <i class="fa fa-group fa-fw"></i> <?php echo $title; ?>
          </div>
            <!-- /.panel-heading -->
            <div class="panel-body">
              <div class="table-responsive">
                <table class="table table-striped table-hover table-condensed">
                  <thead>
                    <tr>
                      <th>Sr No.</th>
                      <th>ITS</th>
                      <th>Full Name</th>
                      <th>Email</th>
                      <th>Mobile</th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php 
                    if($select){
                      $i = 0; 
                      foreach($select as $data){
                        $i++;
                    ?>
                    <tr>
                      <td><input type="checkbox" id="jawab-<?php echo $i; ?>" name="its_key[]" class="jawab css-checkbox1" value="<?php echo $data['its_id']; ?>"><label for="jawab-<?php echo $i; ?>" class="css-label1"><?php echo $i; ?></label></td>
                      <td><?php echo $data['its']; ?></td>
                      <td><?php echo $data['first_name']. ' '. $data['last_name']; ?></a></td>
                      <td><?php echo $data['email']; ?></td>
                      <td><?php echo $data['mobile']; ?></td>
                    </tr>
                    <?php 
                        } 
                      } else { ?>
                    <tr>
                      <td class="text-center" colspan="5">No users to show.</td>
                    </tr>
                    <?php } ?>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      <?php } ?>
    </section>
  </form>
</div>
<script>
  function checkAll(ele) {
    if (ele.checked) {
      $('.jawab').prop('checked', true);
    } else {
      $('.jawab').prop('checked', false);
    }
  } 
</script>  
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<?php
include('footer.php');
?>