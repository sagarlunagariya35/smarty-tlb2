<?php
require_once 'classes/class.user.admin.php';
require_once '../classes/class.user.php';
include 'classes/class.araiz.php';
$allowed_roles = array(ROLE_PROJECT_COORDINATOR,ROLE_ASHARAH_ADMIN);
require_once 'session.php';

$user = new mtx_user_admin;
$araiz = new Araiz();

$title = 'Araz Mark Complete';
$description = '';
$keywords = '';
$active_page = "project_araz_mark_complete";

$select = $its = FALSE;

if(isset($_POST['search'])){
  $its = $_POST['its_key'];
  $select = $araiz->get_araz_data_by_its_id($its);
  $araiz_parsed_data = $araiz->get_araiz_data_organised($select);
}

if(isset($_POST['submit'])) {
  $araz_id = $_POST['araz_id'];
  $counseling_done = $_POST['counseling_done'];
  $qardan_hasana = $_POST['qardan_hasana'];
  $sponsorship = $_POST['sponsorship'];
  
  $test_file_val = $_FILES['test_file']['name'];
  
  if($test_file_val == ''){
    $test_file = $_POST['last_test_file'];
  }else {
    $test_file = $test_file_val;
  }
  
  $test_file_temp = $_FILES['test_file']['tmp_name'];
  $pathANDname = "../upload/test_file/" . $test_file;
  move_uploaded_file($test_file_temp, $pathANDname);
  
  $insert = $araiz->insert_mark_completed($araz_id, $counseling_done, $qardan_hasana, $sponsorship, $test_file);
  if ($insert) {
    $_SESSION[SUCCESS_MESSAGE] = 'Mark Completed Successfully.';
  } else {
    $_SESSION[ERROR_MESSAGE] = 'Error encountered while Mark Completing';
  }
}

include ('header.php');
?>

<link rel="stylesheet" href="js/tablesorter/css/theme.bootstrap.css">
<script src="js/tablesorter/js/jquery.tablesorter.js"></script>
<script src="js/tablesorter/js/jquery.tablesorter.widgets.js"></script>
<style>
  .alfatemi-text{
    font-size: 18px;
  }
</style>
<div class="row">
  <div class="col-lg-12">
    <h3 class="page-header"><?php echo $title; ?></h3>
  </div>
  <div class="row">
    <div class="col-xs-12 col-sm-12">&nbsp;</div>
  </div>
  <form name="raza_form" class="form" method="post" action="" enctype="multipart/form-data">
    <div class="col-md-12">
      <label class="col-md-2 control-label text-right">ITS Key</label>
      <div class="col-md-3">
        <input type="text" name="its_key" class="form-control" value="<?php echo $its; ?>">
      </div>
      <div class="col-md-2">
        <input type="submit" name="search" value="Search" class="btn btn-block btn-success">
      </div>
    </div>
  
    <?php if(isset($_POST['search'])) { ?>
    <div class="col-lg-12 col-xs-12 col-sm-12 pull-left">
      <div class="col-md-12">&nbsp;</div>
      <div class="clearfix"></div>
      <div class="row">
        <div class="panel panel-primary">
          <div class="panel-heading">
            <i class="fa fa-group fa-fw"></i> Records of <?php echo $its; ?>
          </div>
          <!-- /.panel-heading -->
          <div class="panel-body">
            <?php
            $i = 1;
            foreach ($araiz_parsed_data as $araz_id => $araz) {
              $user_details = $araz['user_data'];
              $araz_general_detail = $araz['araz_data'];
            ?>
            <div class="row">
              <div class="col-xs-12 col-sm-10">
                <div class="col-xs-12 col-sm-4 bottom10px">
                  <strong>ITS: </strong><?php echo $user_details['login_its']; ?>
                </div>
                <div class="col-xs-12 col-sm-6 bottom10px">
                  <strong>Full Name: </strong><?php echo $user_details['user_full_name']; ?>
                </div>
                <div class="col-xs-12 col-sm-4 bottom10px">
                  <strong>Mobile: </strong><?php echo ucfirst($user_details['mobile']); ?>
                </div>
                <div class="col-xs-12 col-sm-4 bottom10px">
                  <strong>email: </strong><?php echo ucfirst($user_details['email']); ?>
                </div>
                <div class="clearfix"></div>
              </div>
            </div>
          </div>
          <div class="panel-footer">
            <div class="row">
              <div class="form-group col-md-3 col-xs-12">
                <label>Psychometric File :</label>
                <input type="file" name="test_file" class="file">
                
                <?php if($araz_general_detail['test_file_upload'] != ''){ ?>
                  <br>
                  <lable><?php echo $araz_general_detail['test_file_upload']; ?></lable>
                  <input type="hidden" name="last_test_file" value="<?php echo $araz_general_detail['test_file_upload']; ?>">
                <?php } ?>
              </div>
              <div class="form-group col-md-2 col-xs-12">
                <div class="checkbox">
                  <label>
                    <input type="checkbox" name="counseling_done" value="1" <?php if($araz_general_detail['counselling_done'] == '1'){ echo 'checked'; } ?>>Counseling Done
                  </label>
                </div>
              </div>
              <div class="form-group col-md-2 col-xs-12">
                <div class="checkbox">
                  <label>
                    <input type="checkbox" name="qardan_hasana" value="1" <?php if($araz_general_detail['qardan_hasana_done'] == '1'){ echo 'checked'; } ?>>Qardan Hasana
                  </label>
                </div>
              </div>
              <div class="form-group col-md-2 col-xs-12">
                <div class="checkbox">
                  <label>
                    <input type="checkbox" name="sponsorship" value="1" <?php if($araz_general_detail['sponsorship_done'] == '1'){ echo 'checked'; } ?>>Sponsorship
                  </label>
                </div>
              </div>
              <div class="col-md-2" style="padding: 10px;">
                <input type="submit" name="submit" value="Submit" class="btn btn-block btn-success">
              </div>
              <input type="hidden" name="araz_id" value="<?php echo $araz_id; ?>">
            </div>
            <div class="col-xs-12 col-md-12">&nbsp;</div>
          </div>
          <?php
            }
          ?>
        </div>
      </div>
    </div>
    <?php } ?>
  </form>
</div>
<?php
include 'footer.php';
?>
