<?php
require_once '../classes/constants.php';

if (isset($_REQUEST['its_id'])) {
  $its = $_REQUEST['its_id'];
  $jamat = $_REQUEST['jamat_name'];
  $tanzeem_id = $_REQUEST['tanzeem_id'];
  $vazarat_salt = 'Vaz6589435183';
  $verification_code = md5($tanzeem_id . $its . $jamat . $vazarat_salt);

  $data = json_encode(array('its_id' => $its, 'jamat_name' => $jamat, 'tanzeem_id' => $tanzeem_id, 'key' => $verification_code));

  header('location: ' . SERVER_PATH . "admin/vazarat_login.php?its_id=$its&jamat_name=$jamat&tanzeem_id=$tanzeem_id&key=$verification_code");
  exit();
}
?>
<html>
  <head>
    <title>Test page for Vazarat login system</title>
    <style>
      #note{
        display: block;
        padding: 10px 20px;
        clear: both;
        font-size: 12px;
        color: #555;
      }
      label{
        display: block;
        float: left;
        clear: left;
        font-weight: 700;
        padding-top: 10px;
        padding-left: 20px;
        min-width: 100px;
        text-align: right;
      }
      input[type="text"]{
        display: block;
        float: left;
        padding: 2px;
        margin-top: 10px;
        margin-left: 10px;
      }
    </style>
  </head>
  <body>
    <form method="POST">
      <label>ITS:</label>
      <input type="text" name="its_id">
      <span id="note">This user will be logged in as the aamil saheb of below specified jamaat</span>
      <label>Jamaat Name:</label>
      <input type="text" name="jamat_name">
      <label>Tanzeem ID:</label>
      <input type="text" name="tanzeem_id">
      <span id="note">The above jamaat will be permanantly linked to this tanzeem id in the database</span>
      <input type="submit" name="submit" value="Submit">
    </form>
  </body>
</html>