<?php
require_once 'classes/class.user.admin.php';
require_once '../classes/class.user.php';
include 'classes/class.araiz.php';
$allowed_roles = array(ROLE_PROJECT_COORDINATOR,ROLE_ASHARAH_ADMIN);
require_once 'session.php';

$user = new mtx_user_admin;
$araiz = new Araiz();

$title = 'Recommended Sponsorship Done';
$description = '';
$keywords = '';
$active_page = "report_sponsorship_done";

$sponsorship = $araiz->get_all_sponsorship_done();
$total_sponsorship = $araiz->count_total_sponsorship_done();

$araiz_sponsorship = $araiz->get_araiz_data_organised($sponsorship);

include ('header.php');
?>

<link rel="stylesheet" href="js/tablesorter/css/theme.bootstrap.css">
<script src="js/tablesorter/js/jquery.tablesorter.js"></script>
<script src="js/tablesorter/js/jquery.tablesorter.widgets.js"></script>
<style>
  .alfatemi-text{
    font-size: 18px;
  }
</style>
<div class="row">
  <div class="row">
    <div class="col-xs-12 col-sm-12">&nbsp;</div>
  </div>
  <div class="row">
    <div class="col-xs-12 col-sm-12">
      <h3 class="text-center">Total Sponsorship Done: <?php echo $total_sponsorship; ?></h3>
    </div>
  </div>
  <form name="raza_form" class="form" method="post" action="">
  <div class="row">
    <div class="col-xs-12 col-sm-2 pull-right">
      <a href="print_sponsorship_done.php" target="_blank" class="btn btn-success btn-block pull-right">Print</a>
    </div>
  </div>
  <div class="col-lg-12 col-xs-12 col-sm-12 pull-left">
    <div class="col-md-12">&nbsp;</div>
    <div class="clearfix"></div>
    <div class="row">
      <div class="panel panel-primary">
        <div class="panel-heading">
          <i class="fa fa-group fa-fw"></i> <?php echo $title; ?>
        </div>
        <!-- /.panel-heading -->
        <div class="panel-body">
          <div class="table-responsive">
            <table class="table table-striped table-hover table-condensed">
              <thead>
                <tr>
                  <th>Sr No.</th>
                  <th>ITS</th>
                  <th>Full Name</th>
                  <th>Email</th>
                  <th>Mobile</th>
                </tr>
              </thead>
              <tbody>
              <?php
              if($araiz_sponsorship){
              $i = 1;
              foreach ($araiz_sponsorship as $araz_id => $araz) {
                $user_details = $araz['user_data'];
                $araz_general_detail = $araz['araz_data'];
              ?>
                <tr>
                  <td><?php echo $i++;?></td>
                  <td><?php echo $user_details['login_its']; ?></td>
                  <td><?php echo $user_details['user_full_name']; ?></a></td>
                  <td><?php echo $user_details['email']; ?></td>
                  <td><?php echo $user_details['mobile']; ?></td>
                </tr>
              <?php
                }
              }else {
                echo '<tr><td class="text-center" colspan="6">No Records..</td></tr>';
              }
              ?>
              </tbody>
              </table>
          </div>
        </div>
      </div>
    </div>
  </div>
  </form>
</div>

<?php
include 'footer.php';
?>
