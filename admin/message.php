<?php
if (isset($_SESSION[SUCCESS_MESSAGE]))
  $success_message = $_SESSION[SUCCESS_MESSAGE];
if (isset($_SESSION[ERROR_MESSAGE]))
  $error_message = $_SESSION[ERROR_MESSAGE];
if (isset($success_message) && $success_message != '') {
  ?>
  <div class="col-md-5 col-md-offset-4 alert alert-success fade in">
    <button type="button" class="close" data-dismiss="alert">&times;</button>
    <?php
    echo $success_message;
    unset($_SESSION[SUCCESS_MESSAGE]);
    ?>
  </div>
  <div class="clearfix"></div>
  <?php
}

if (isset($error_message) && $error_message != '') {
  ?>
  <div class="col-md-5 col-md-offset-4 alert alert-danger">
    <button type="button" class="close" data-dismiss="alert">&times;</button>
    <?php
    echo $error_message;
    unset($_SESSION[ERROR_MESSAGE]);
    ?>
  </div>
  <div class="clearfix"></div>
  <?php
}
?>
