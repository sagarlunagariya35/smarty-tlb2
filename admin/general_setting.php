<?php
require_once 'classes/class.user.admin.php';
$allowed_roles = array(ROLE_HEAD_OFFICE);
require_once 'session.php';

$title = 'General Settings';
$description = '';
$keywords = '';
$active_page = "general_setting";

$id = FALSE;
$select = $result = $category = FALSE;

if (isset($_GET['redirect_id'])) {
  
  $id = $_GET['redirect_id'];
  $category = $_GET['category'];
  $result = get_general_settings($category, $id);
} else if (isset($_GET['redirect_submit_id'])) {
  
  $id = $_GET['redirect_submit_id'];
  $category = $_GET['category'];
  $result = get_general_settings($category, $id);
} else if (isset($_GET['home_id'])) {
  
  $id = $_GET['home_id'];
  $category = $_GET['category'];
  $result = get_general_settings_for_carousal($id, $category);
} else if (isset($_GET['submit_id'])) {
  
  $id = $_GET['submit_id'];
  $category = $_GET['category'];
  $result = get_general_settings_for_carousal($id, $category);
}

if (isset($_POST['delete_id'])) {
  $delete_id = $_POST['delete_id'];
  $result = delete_general_setting(LOGIN_REDIRECT,$delete_id);

  if ($result) {
    $_SESSION[SUCCESS_MESSAGE] = "Login Redirect Link Successfully Delete";
    header('Location: general_setting.php');
    exit();
  } else {
    $_SESSION[ERROR_MESSAGE] = "Error while Deleting";
  }
}

if (isset($_POST['delete_submit_redirect_id'])) {
  $delete_id = $_POST['delete_id'];
  $result = delete_general_setting(SUBMIT_REDIRECT,$delete_id);

  if ($result) {
    $_SESSION[SUCCESS_MESSAGE] = "Submit Redirect Link Successfully Delete";
    header('Location: general_setting.php');
    exit();
  } else {
    $_SESSION[ERROR_MESSAGE] = "Error while Deleting";
  }
}

if (isset($_POST['delete_home_id'])) {
  $delete_id = $_POST['delete_home_id'];
  $page = $_POST['page'];
  $result = delete_general_setting_for_carousal($delete_id, $page);

  if ($result) {
    $_SESSION[SUCCESS_MESSAGE] = "Home page Carousal Successfully Delete";
    header('Location: general_setting.php');
    exit();
  } else {
    $_SESSION[ERROR_MESSAGE] = "Error while Deleting";
  }
}

if (isset($_POST['delete_submit_id'])) {
  $delete_id = $_POST['delete_submit_id'];
  $page = $_POST['page'];
  $result = delete_general_setting_for_carousal($delete_id, $page);

  if ($result) {
    $_SESSION[SUCCESS_MESSAGE] = "Submit page Carousal Successfully Delete";
    header('Location: general_setting.php');
    exit();
  } else {
    $_SESSION[ERROR_MESSAGE] = "Error while Deleting";
  }
}

if (isset($_POST['Update'])) {
  $id = $_GET['redirect_id'];
  $link = $_POST['link'];

  $update = update_general_setting($link, LOGIN_REDIRECT, $id);

  if ($update) {
    $_SESSION[SUCCESS_MESSAGE] = 'Login Redirect Link Successfully Update';
    header('Location: general_setting.php');
    exit();
  } else {
    $_SESSION[ERROR_MESSAGE] = 'Error In Update Data';
    header('Location: general_setting.php');
    exit();
  }
}

if (isset($_POST['Update_submit_link'])) {
  $id = $_GET['redirect_submit_id'];
  $link = $_POST['link'];

  $update = update_general_setting($link, SUBMIT_REDIRECT, $id);

  if ($update) {
    $_SESSION[SUCCESS_MESSAGE] = 'Submit Redirect Link Successfully Update';
    header('Location: general_setting.php');
    exit();
  } else {
    $_SESSION[ERROR_MESSAGE] = 'Error In Update Data';
    header('Location: general_setting.php');
    exit();
  }
}

if (isset($_POST['Update_home_carousal'])) {
  $id = $_GET['home_id'];
  $image_file = $_FILES['home_image']['name'];
  $image_temp = $_FILES['home_image']['tmp_name'];
  $link = $_POST['home_link'];
  
  $pathANDname = "../images/banners/" . $image_file;
  move_uploaded_file($image_temp, $pathANDname);

  $update = update_general_setting_for_carousal($image_file, $link, HOME_CAROUSAL, $id);

  if ($update) {
    $_SESSION[SUCCESS_MESSAGE] = 'Home page Carousal Successfully Update';
    header('Location: general_setting.php');
    exit();
  } else {
    $_SESSION[ERROR_MESSAGE] = 'Error In Update Data';
    header('Location: general_setting.php');
    exit();
  }
}

if (isset($_POST['Update_submit_carousal'])) {
  $id = $_GET['submit_id'];
  $image_file = $_FILES['submit_image']['name'];
  $image_temp = $_FILES['submit_image']['tmp_name'];
  $link = $_POST['submit_link'];
  
  $pathANDname = "../images/banners/" . $image_file;
  move_uploaded_file($image_temp, $pathANDname);

  $update = update_general_setting_for_carousal($image_file, $link, SUBMIT_CAROUSAL, $id);

  if ($update) {
    $_SESSION[SUCCESS_MESSAGE] = 'Submit page Carousal Successfully Update';
    header('Location: general_setting.php');
    exit();
  } else {
    $_SESSION[ERROR_MESSAGE] = 'Error In Update Data';
    header('Location: general_setting.php');
    exit();
  }
}

if (isset($_POST['Save'])) {
  $link = $_POST['link'];
  $insert = insert_general_setting($link,LOGIN_REDIRECT);

  ($insert) ? $_SESSION[SUCCESS_MESSAGE] = "Login Redirect Link Successfully Insert" : $_SESSION[ERROR_MESSAGE] = "Error In Inserting Data";
}

if (isset($_POST['Save_submit_link'])) {
  $link = $_POST['link'];
  $insert = insert_general_setting($link,SUBMIT_REDIRECT);

  ($insert) ? $_SESSION[SUCCESS_MESSAGE] = "Submit Redirect Link Successfully Insert" : $_SESSION[ERROR_MESSAGE] = "Error In Inserting Data";
}

if (isset($_POST['Save_home_carousal'])) {
  $image_file = $_FILES['home_image']['name'];
  $image_temp = $_FILES['home_image']['tmp_name'];
  $link = $_POST['home_link'];
  
  $pathANDname = "../images/banners/" . $image_file;
  move_uploaded_file($image_temp, $pathANDname);
  
  $insert = insert_general_setting_for_carousal($image_file, $link, HOME_CAROUSAL);

  ($insert) ? $_SESSION[SUCCESS_MESSAGE] = "Home page Carousal Successfully Insert" : $_SESSION[ERROR_MESSAGE] = "Error In Inserting Data";
}

if (isset($_POST['Save_submit_carousal'])) {
  $image_file = $_FILES['submit_image']['name'];
  $image_temp = $_FILES['submit_image']['tmp_name'];
  $link = $_POST['submit_link'];
  
  $pathANDname = "../images/banners/" . $image_file;
  move_uploaded_file($image_temp, $pathANDname);
  
  $insert = insert_general_setting_for_carousal($image_file, $link, SUBMIT_CAROUSAL);

  ($insert) ? $_SESSION[SUCCESS_MESSAGE] = "Submit page Carousal Successfully Insert" : $_SESSION[ERROR_MESSAGE] = "Error In Inserting Data";
}

$redirect_link = get_general_settings(LOGIN_REDIRECT);
$submit_redirect_link = get_general_settings(SUBMIT_REDIRECT);
$home_page_carousal = get_general_settings_for_carousal(HOME_CAROUSAL);
$submit_page_carousal = get_general_settings_for_carousal(SUBMIT_CAROUSAL);

include ('header.php');
?>

<style>
  tfoot {
    display: table-header-group;
    width: 100%;
    padding: 3px;
    box-sizing: border-box;
  }
  /*.dataTables_info, .dataTables_paginate{
    display: inline-block;
  }*/
</style>

<div id="wrapper">

  <!-- Navigation -->

  <div class="row">
    <div class="col-md-12">
      <div class="col-lg-12">
        <h3 class="page-header"><?php echo $title; ?></h3>
      </div>
    </div>


    <ul class="nav nav-tabs nav-justified" role="tablist">
      <li role="presentation" class="<?php if($id AND $category == LOGIN_REDIRECT){ echo 'active'; } ?>"><a href="<?php if($id AND ($category != LOGIN_REDIRECT OR $category == FALSE)){ echo ''; }else { echo '#redirect_link'; } ?>" aria-controls="pre" role="tab" data-toggle="tab">Set Login Redirect Link</a></li>
      
      <li role="presentation" class="<?php if($id AND $category == SUBMIT_REDIRECT){ echo 'active'; } ?>"><a href="<?php if($id AND ($category != SUBMIT_REDIRECT OR $category == FALSE)){ echo ''; }else { echo '#redirect_submit_link'; } ?>" aria-controls="pre" role="tab" data-toggle="tab">Set Submit Redirect Link</a></li>
      
      <li role="presentation" class="<?php if($id AND $category == HOME_CAROUSAL){ echo 'active'; } ?>"><a href="<?php if($id AND ($category != HOME_CAROUSAL OR $category == FALSE)){ echo ''; }else { echo '#home_page_carousal'; } ?>" aria-controls="in" role="tab" data-toggle="tab">Set Home page Carousal</a></li>
      
      <li role="presentation" class="<?php if($id AND $category == SUBMIT_CAROUSAL){ echo 'active'; } ?>"><a href="<?php if($id AND ($category != SUBMIT_CAROUSAL OR $category == FALSE)){ echo ''; }else { echo '#submit_page_carousal'; } ?>" aria-controls="post" role="tab" data-toggle="tab">Set Submit page Carousal</a></li>
    </ul>
    <div class="clearfix"><br></div>

    <div class="tab-content">
      <div role="tabpanel" class="tab-pane <?php if($id AND $category == LOGIN_REDIRECT){ echo 'active'; } ?>" id="redirect_link">
        <form method="post" action="" role="form" class="form-horizontal">
          <div class="form-group col-md-8">
            <label class="control-label col-md-3">Redirect Link:</label>
            <div class="col-md-9">
              <input type="text" class="form-control" name="link"  placeholder="Add Redirect Link" value="<?php echo $result[0]['redirect_link']; ?>" required>
            </div>
          </div>

          <div class="form-group">
            <?php
            $btn_name = $id ? 'Update' : 'Save';
            ?>
            <button name="<?php echo $btn_name; ?>" class="btn btn-success">Submit</button>
          </div>
        </form>

        <div class="row">
          <div class="col-lg-12">
            <div class="panel panel-default">
              <div class="panel-heading">
                Login Redirect Link
              </div>
              <!-- /.panel-heading -->
              <div class="panel-body">
                <div class="table-responsive">
                  <table class="table table-striped table-condensed table-hover" id="example">
                    <thead>
                      <tr>
                        <th>Sr.</th>
                        <th>Link</th>
                        <th>Action</th>
                      </tr>
                    </thead>
                    <tbody>
                      <?php
                      if ($redirect_link) {
                        $i = 1;
                        foreach ($redirect_link as $data) {
                          ?>

                          <tr>
                            <td><?php echo $i++; ?></td>
                            <td><?php echo $data['redirect_link']; ?></td>
                            <td>
                              <a href="general_setting.php?redirect_id=<?php echo $data['id']; ?>&category=<?php echo $data['category']; ?>"><i class="fa fa-edit"></i></a>
                              <button class="btn btn-link confirm" id="<?php echo $data['id']; ?>"><i class="fa fa-remove text-danger"></i></button>
                            </td>
                          </tr>
                          <?php
                        }
                      } else {
                        echo '<tr><td colspan="3" class="text-center">No Records..</td></tr>';
                      }
                      ?>
                    </tbody>
                  </table>
                </div>
                <!-- /.table-responsive -->
              </div>
              <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
          </div>
          <!-- /.col-lg-12 -->
        </div>
      </div>
      <div role="tabpanel" class="tab-pane <?php if($id AND $category == SUBMIT_REDIRECT){ echo 'active'; } ?>" id="redirect_submit_link">
        <form method="post" action="" role="form" class="form-horizontal">
          <div class="form-group col-md-8">
            <label class="control-label col-md-3">Redirect Link:</label>
            <div class="col-md-9">
              <input type="text" class="form-control" name="link"  placeholder="Add Redirect Link" value="<?php echo $result[0]['redirect_link']; ?>" required>
            </div>
          </div>

          <div class="form-group">
            <?php
            $btn_name = $id ? 'Update_submit_link' : 'Save_submit_link';
            ?>
            <button name="<?php echo $btn_name; ?>" class="btn btn-success">Submit</button>
          </div>
        </form>

        <div class="row">
          <div class="col-lg-12">
            <div class="panel panel-default">
              <div class="panel-heading">
                Submit Redirect Link
              </div>
              <!-- /.panel-heading -->
              <div class="panel-body">
                <div class="table-responsive">
                  <table class="table table-striped table-condensed table-hover" id="example">
                    <thead>
                      <tr>
                        <th>Sr.</th>
                        <th>Link</th>
                        <th>Action</th>
                      </tr>
                    </thead>
                    <tbody>
                      <?php
                      if ($submit_redirect_link) {
                        $i = 1;
                        foreach ($submit_redirect_link as $data) {
                          ?>

                          <tr>
                            <td><?php echo $i++; ?></td>
                            <td><?php echo $data['redirect_link']; ?></td>
                            <td>
                              <a href="general_setting.php?redirect_submit_id=<?php echo $data['id']; ?>&category=<?php echo $data['category']; ?>"><i class="fa fa-edit"></i></a>
                              <button class="btn btn-link confirm_submit_link" id="<?php echo $data['id']; ?>"><i class="fa fa-remove text-danger"></i></button>
                            </td>
                          </tr>
                          <?php
                        }
                      } else {
                        echo '<tr><td colspan="3" class="text-center">No Records..</td></tr>';
                      }
                      ?>
                    </tbody>
                  </table>
                </div>
                <!-- /.table-responsive -->
              </div>
              <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
          </div>
          <!-- /.col-lg-12 -->
        </div>
      </div>
      <div role="tabpanel" class="tab-pane <?php if($id AND $category == HOME_CAROUSAL){ echo 'active'; } ?>" id="home_page_carousal">
        <form method="post" action="" role="form" class="form-horizontal" enctype="multipart/form-data">
          <div class="form-group col-md-8">
            <label class="control-label col-md-3"></label>
            <div class="col-md-9">
              Note: Please upload Image with size 1280 * 661
            </div>
          </div>
          <div class="form-group col-md-8">
            <label class="control-label col-md-3">Image:</label>
            <div class="col-md-9">
              <input type="file" name="home_image" class="file">
            </div>
          </div>
          <div class="form-group col-md-8">
            <label class="control-label col-md-3">Link:</label>
            <div class="col-md-9">
              <input type="text" class="form-control" name="home_link"  placeholder="Add Link" value="<?php echo $result[0]['link']; ?>" required>
            </div>
          </div>
          <div class="clearfix"></div>
          <div class="form-group col-md-8">
            <label class="control-label col-md-3"></label>
            <div class="col-md-9">
              <?php
              $btn_name = $id ? 'Update_home_carousal' : 'Save_home_carousal';
              ?>
              <button name="<?php echo $btn_name; ?>" class="btn btn-success col-md-2">Submit</button>
            </div>
          </div>
        </form>

        <div class="row">
          <div class="col-lg-12">
            <div class="panel panel-default">
              <div class="panel-heading">
                Home page Carousal List
              </div>
              <!-- /.panel-heading -->
              <div class="panel-body">
                <div class="table-responsive">
                  <table class="table table-striped table-condensed table-hover" id="example">
                    <thead>
                      <tr>
                        <th>Sr.</th>
                        <th>Image</th>
                        <th>Link</th>
                        <th>Action</th>
                      </tr>
                    </thead>
                    <tbody>
                      <?php
                      if ($home_page_carousal) {
                        $i = 1;
                        foreach ($home_page_carousal as $data) {
                          ?>

                          <tr>
                            <td><?php echo $i++; ?></td>
                            <td><img src="<?php echo SERVER_PATH.'images/banners/'.$data['image']; ?>" class="img-thumbnail" /></td>
                            <td><?php echo $data['link']; ?></td>
                            <td>
                              <a href="general_setting.php?home_id=<?php echo $data['id']; ?>&category=<?php echo $data['page']; ?>"><i class="fa fa-edit"></i></a>
                              <button class="btn btn-link confirm_home" id="<?php echo $data['id']; ?>"><i class="fa fa-remove text-danger"></i></button>
                            </td>
                          </tr>
                          <?php
                        }
                      } else {
                        echo '<tr><td colspan="4" class="text-center">No Records..</td></tr>';
                      }
                      ?>
                    </tbody>
                  </table>
                </div>
                <!-- /.table-responsive -->
              </div>
              <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
          </div>
          <!-- /.col-lg-12 -->
        </div>
      </div>
      <div role="tabpanel" class="tab-pane <?php if($id AND $category == SUBMIT_CAROUSAL){ echo 'active'; } ?>" id="submit_page_carousal">
        <form method="post" action="" role="form" class="form-horizontal" enctype="multipart/form-data">
          <div class="form-group col-md-8">
            <label class="control-label col-md-3"></label>
            <div class="col-md-9">
              Note: Please upload Image with size 1180 * 561
            </div>
          </div>
          <div class="form-group col-md-8">
            <label class="control-label col-md-3">Image:</label>
            <div class="col-md-9">
              <input type="file" name="submit_image" class="file">
            </div>
          </div>
          <div class="form-group col-md-8">
            <label class="control-label col-md-3">Link:</label>
            <div class="col-md-9">
              <input type="text" class="form-control" name="submit_link"  placeholder="Add Link" value="<?php echo $result[0]['link']; ?>" required>
            </div>
          </div>
          <div class="clearfix"></div>
          <div class="form-group col-md-8">
            <label class="control-label col-md-3"></label>
            <div class="col-md-9">
              <?php
              $btn_name = $id ? 'Update_submit_carousal' : 'Save_submit_carousal';
              ?>
              <button name="<?php echo $btn_name; ?>" class="btn btn-success col-md-2">Submit</button>
            </div>
          </div>
        </form>

        <div class="row">
          <div class="col-lg-12">
            <div class="panel panel-default">
              <div class="panel-heading">
                Submit page Carousal List
              </div>
              <!-- /.panel-heading -->
              <div class="panel-body">
                <div class="table-responsive">
                  <table class="table table-striped table-condensed table-hover" id="example">
                    <thead>
                      <tr>
                        <th>Sr.</th>
                        <th>Image</th>
                        <th>Link</th>
                        <th>Action</th>
                      </tr>
                    </thead>
                    <tbody>
                      <?php
                      if ($submit_page_carousal) {
                        $i = 1;
                        foreach ($submit_page_carousal as $data) {
                          ?>

                          <tr>
                            <td><?php echo $i++; ?></td>
                            <td><img src="<?php echo SERVER_PATH.'images/banners/'.$data['image']; ?>" class="img-thumbnail" /></td>
                            <td><?php echo $data['link']; ?></td>
                            <td>
                              <a href="general_setting.php?submit_id=<?php echo $data['id']; ?>&category=<?php echo $data['page']; ?>"><i class="fa fa-edit"></i></a>
                              <button class="btn btn-link confirm_submit" id="<?php echo $data['id']; ?>"><i class="fa fa-remove text-danger"></i></button>
                            </td>
                          </tr>
                          <?php
                        }
                      } else {
                        echo '<tr><td colspan="4" class="text-center">No Records..</td></tr>';
                      }
                      ?>
                    </tbody>
                  </table>
                </div>
                <!-- /.table-responsive -->
              </div>
              <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
          </div>
          <!-- /.col-lg-12 -->
        </div>
      </div>
    </div>
  </div>
  <!-- /.row -->

  <!-- /#page-wrapper -->
  <form method="post" id="frm-del-link">
    <input type="hidden" name="delete_id" id="del-grp-val" value="">
    <input type="hidden" name="cmd" value="del-link">
  </form>
  <form method="post" id="frm-del-submit-link">
    <input type="hidden" name="delete_id" id="del-grp-submit-link-val" value="">
    <input type="hidden" name="cmd" value="del-link">
  </form>
  <form method="post" id="frm-del-home-link">
    <input type="hidden" name="delete_home_id" id="del-grp-home-val" value="">
    <input type="hidden" name="page" value="home">
    <input type="hidden" name="cmd" value="del-link">
  </form>
  <form method="post" id="frm-del-submit-link">
    <input type="hidden" name="delete_submit_id" id="del-grp-submit-val" value="">
    <input type="hidden" name="page" value="submit">
    <input type="hidden" name="cmd" value="del-link">
  </form>

</div
<!-- /#wrapper -->

<script>
  $(".confirm").confirm({
    text: "Are you sure you want to delete?",
    title: "Confirmation required",
    confirm: function(button) {
      $('#del-grp-val').val($(button).attr('id'));
      $('#frm-del-link').submit();
      alert('Are you Sure You want to delete: ' + id);
    },
    cancel: function(button) {
      // nothing to do
    },
    confirmButton: "Yes",
    cancelButton: "No",
    post: true,
    confirmButtonClass: "btn-danger"
  });
  
  $(".confirm_submit_link").confirm({
    text: "Are you sure you want to delete?",
    title: "Confirmation required",
    confirm: function(button) {
      $('#del-grp-submit-link-val').val($(button).attr('id'));
      $('#frm-del-submit-link').submit();
      alert('Are you Sure You want to delete: ' + id);
    },
    cancel: function(button) {
      // nothing to do
    },
    confirmButton: "Yes",
    cancelButton: "No",
    post: true,
    confirmButtonClass: "btn-danger"
  });
  
  $(".confirm_home").confirm({
    text: "Are you sure you want to delete?",
    title: "Confirmation required",
    confirm: function(button) {
      $('#del-grp-home-val').val($(button).attr('id'));
      $('#frm-del-home-link').submit();
      alert('Are you Sure You want to delete: ' + id);
    },
    cancel: function(button) {
      // nothing to do
    },
    confirmButton: "Yes",
    cancelButton: "No",
    post: true,
    confirmButtonClass: "btn-danger"
  });
  
  $(".confirm_submit").confirm({
    text: "Are you sure you want to delete?",
    title: "Confirmation required",
    confirm: function(button) {
      $('#del-grp-submit-val').val($(button).attr('id'));
      $('#frm-del-submit-link').submit();
      alert('Are you Sure You want to delete: ' + id);
    },
    cancel: function(button) {
      // nothing to do
    },
    confirmButton: "Yes",
    cancelButton: "No",
    post: true,
    confirmButtonClass: "btn-danger"
  });
</script>

<?php
include 'footer.php';
?>

