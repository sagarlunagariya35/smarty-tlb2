<?php

require_once 'classes/class.user.admin.php';
require_once 'classes/class.araiz.php';

$user = new mtx_user_admin;
$araiz = new Araiz();

$fh = fopen('log_mtx.txt', 'a');

// Transfer new araiz to the txn table
$qry_txn = "INSERT INTO txn_view_araz SELECT * FROM view_araz WHERE araz_id > (SELECT MAX(araz_id) last_araz_id FROM txn_view_araz)";
$db->query_fetch_full_result($qry_txn);


$counselor_query = "SELECT * FROM `txn_view_araz` WHERE `counselor_id` != 0 AND `email_sent_counselor` = 0 GROUP BY `araz_id`";
$counselor_result = $db->query_fetch_full_result($counselor_query);

if ($counselor_result) {
  foreach ($counselor_result as $data) {
    $ret[$data['counselor_id']][] = $data['araz_id'];
  }

  foreach ($ret as $counselor_id => $araz_data) {
    $total_araiz = count($araz_data);

    $qry = "SELECT * FROM `tlb_user_admin` WHERE `user_id` = '$counselor_id'";
    $rslt = $db->query_fetch_full_result($qry);

    if ($rslt[0]['email']) {
      $subject = "You have new Araiz assignments";
      $message = "<p>Ba’ad al-Salaam al-Jameel,</p>
                  <p>You have been assigned $total_araiz new araiz on talabulilm.com. Please review the araiz and counsel the respective students  within 48 hours.</p>
                  <p>Kindly login to <a href=\"https://www.talabulilm.com/admin/\">https://www.talabulilm.com/admin</a> for more details.</p>";

      shoot_mail($rslt[0]['email'], $subject, $message);
      //shoot_mail('janah.mustafa@gmail.com', $subject, $message);
      
      // mark the araiz as mail sent
      $mark_ids = join(',', $ret[$counselor_id]);
      $mark_query = "UPDATE `tlb_araz` SET `email_sent_counselor` = 1 WHERE `id` IN ($mark_ids)";
      fwrite($fh, $mark_query . "\n");
      $mark_rslt = $db->query($mark_query);
      fwrite($fh, ($mark_rslt) ? 'Success' : 'fail');
      
      $mark_ids = join(',', $ret[$counselor_id]);
      $mark_query = "UPDATE `txn_view_araz` SET `email_sent_counselor` = 1 WHERE `araz_id` IN ($mark_ids)";
      fwrite($fh, $mark_query . "\n");
      $mark_rslt = $db->query($mark_query);
      fwrite($fh, ($mark_rslt) ? 'Success' : 'fail');
    }
  }
}

$saheb_query = "SELECT * FROM `txn_view_araz` WHERE `saheb_id` != 0 AND `email_sent_saheb` = 0 GROUP BY `araz_id`";
$saheb_result = $db->query_fetch_full_result($saheb_query);

if ($saheb_result) {
  foreach ($saheb_result as $data) {
    $ret[$data['saheb_id']][] = $data['araz_id'];
  }

  foreach ($ret as $saheb_id => $araz_data) {
    $total_araiz = count($araz_data);

    $qry = "SELECT * FROM `tlb_user_admin` WHERE `user_id` = '$saheb_id'";
    $rslt = $db->query_fetch_full_result($qry);

    if ($rslt[0]['email']) {
      $subject = "You have new Araiz assignments";
      $message = "<p>Ba’ad al-Tasleemaat.</p>
<p>You have been assigned $total_araiz new araiz on talabulilm.com. Please send  jawab within 48 hours.</p>
<p>Kindly login to <a href=\"https://www.talabulilm.com/admin/\">https://www.talabulilm.com/admin</a> for more details.</p>";

      shoot_mail($rslt[0]['email'], $subject, $message);
      //shoot_mail('janah.mustafa@gmail.com', $subject, $message);
      
      // mark the araiz as mail sent
      $mark_ids = join(',', $ret[$saheb_id]);
      $mark_query = "UPDATE `tlb_araz` SET `email_sent_saheb` = 1 WHERE `id` IN ($mark_ids)";
      fwrite($fh, $mark_query . "\n");
      $mark_rslt = $db->query($mark_query);
      fwrite($fh, ($mark_rslt) ? 'Success' : 'fail');
      
      $mark_ids = join(',', $ret[$saheb_id]);
      $mark_query = "UPDATE `txn_view_araz` SET `email_sent_saheb` = 1 WHERE `araz_id` IN ($mark_ids)";
      fwrite($fh, $mark_query . "\n");
      $mark_rslt = $db->query($mark_query);
      fwrite($fh, ($mark_rslt) ? 'Success' : 'fail');
    }
  }
}


$ho_query = "SELECT * FROM `txn_view_araz` WHERE `jawab_given` != 0 AND `email_sent_ho` = 0 GROUP BY `araz_id`";
$ho_result = $db->query_fetch_full_result($ho_query);

if ($ho_result) {
  foreach ($ho_result as $data) {
    $ret[ROLE_HEAD_OFFICE][] = $data['araz_id'];
  }

  foreach ($ret as $ho => $araz_data) {
    $total_araiz = count($araz_data);

    $qry = "SELECT * FROM `tlb_user_admin` WHERE `user_type` = '$ho'";
    $rslt = $db->query_fetch_full_result($qry);

    if ($rslt[0]['email']) {
      foreach ($rslt as $ho_user) {
        $subject = "You have new Araiz assignments";
        $message = "<p>Ba’ad al-Salaam al-Jameel,</p>
 <p>You have been assigned $total_araiz new araiz on talabulilm.com. Please review them and assign to the respective person within 48 hours.</p>
<p>Kindly login to <a href=\"https://www.talabulilm.com/admin/\">https://www.talabulilm.com/admin</a> for more details.</p>";

      //shoot_mail('janah.mustafa@gmail.com', $subject, $message);
      shoot_mail($ho_user['email'], $subject, $message);
      
      }
      //set email_sent_ho = 1 in the jamiat coordinator
      // mark the araiz as mail sent
      /*
      $mark_ids = join(',', $ret[$ho]);
      $mark_query = "UPDATE `tlb_araz` SET `email_sent_ho` = 1 WHERE `id` IN ($mark_ids)";
      fwrite($fh, $mark_query . "\n");
      $mark_rslt = $db->query($mark_query);
      fwrite($fh, ($mark_rslt) ? 'Success' : 'fail');
      
      $mark_ids = join(',', $ret[$ho]);
      $mark_query = "UPDATE `txn_view_araz` SET `email_sent_ho` = 1 WHERE `araz_id` IN ($mark_ids)";
      fwrite($fh, $mark_query . "\n");
      $mark_rslt = $db->query($mark_query);
      fwrite($fh, ($mark_rslt) ? 'Success' : 'fail');
       */
    }
  }
}


$cord_query = "SELECT * FROM `tlb_user_admin` WHERE `user_type` = '".ROLE_JAMIAT_COORDINATOR."'";
$cord_result = $db->query_fetch_full_result($cord_query);

if ($cord_result) {
  foreach ($cord_result as $data) {
    
    $jamiyet = unserialize($data['jamiat']);
    if (count($jamiyet) == 1 && $jamiyet[0] != '') {
      $jamiyet_data = implode("','", $jamiyet[0]);
      
      $query = "SELECT * FROM `txn_view_araz` WHERE `jawab_given` != 0 AND `email_sent_ho` = 0 AND `jamiat` IN ('$jamiyet_data') GROUP BY `araz_id`";
      $result = $db->query_fetch_full_result($query);

      if ($result) {
        foreach ($result as $r) {
          $ret[ROLE_JAMIAT_COORDINATOR][] = $r['araz_id'];
        }

        foreach ($ret as $cord => $araz_data) {
          $total_araiz = count($araz_data);

          if ($data['email']) {
            
            $subject = "You have new Araiz assignments";
            $message = "<p>Ba’ad al-Salaam al-Jameel,</p>
       <p>You have been assigned $total_araiz new araiz on talabulilm.com. Please review them and assign to the respective person within 48 hours.</p>
      <p>Kindly login to <a href=\"https://www.talabulilm.com/admin/\">https://www.talabulilm.com/admin</a> for more details.</p>";

            //shoot_mail('janah.mustafa@gmail.com', $subject, $message);
            shoot_mail($data['email'], $subject, $message);
            
            // mark the araiz as mail sent
            $mark_ids = join(',', $ret[$cord]);
            $mark_query = "UPDATE `tlb_araz` SET `email_sent_ho` = 1 WHERE `id` IN ($mark_ids)";
            fwrite($fh, $mark_query . "\n");
            $mark_rslt = $db->query($mark_query);
            fwrite($fh, ($mark_rslt) ? 'Success' : 'fail');

            $mark_ids = join(',', $ret[$cord]);
            $mark_query = "UPDATE `txn_view_araz` SET `email_sent_ho` = 1 WHERE `araz_id` IN ($mark_ids)";
            fwrite($fh, $mark_query . "\n");
            $mark_rslt = $db->query($mark_query);
            fwrite($fh, ($mark_rslt) ? 'Success' : 'fail');
          }
        }
      }
    }
  }
}


function shoot_mail($to, $subject, $message, $sender = FALSE) {
  $sender = ($sender) ? $sender : 'eduaraiz@talabulilm.com';
  $headers = 'From: ' . $sender . "\r\n" .
          'Bcc: janah.mustafa@gmail.com' . "\r\n" .
          "MIME-Version: 1.0\r\n" .
          "Content-Type: text/html; charset=UTF-8\r\n" .
          'X-Mailer: PHP/' . phpversion();

  mail($to, $subject, $message, $headers);
}

?>
