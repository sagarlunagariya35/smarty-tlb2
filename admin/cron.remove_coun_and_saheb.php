<?php

require_once 'classes/class.user.admin.php';
require_once 'classes/class.araiz.php';

$user = new mtx_user_admin;
$araiz = new Araiz();

$ret = FALSE;
$curr_date = date('Y-m-d');

$fh = fopen('log_mtx_for_remove.txt', 'a');

$counselor_query = "SELECT * FROM `txn_view_araz` WHERE `counselor_id` != 0 AND `counselor_sugest_timestamp` IS NULL GROUP BY `araz_id`";
$counselor_result = $db->query_fetch_full_result($counselor_query);

if ($counselor_result) {
  foreach ($counselor_result as $data) {
    
    $coun_from = new DateTime($data['coun_assign_ts']);
    $coun_to = new DateTime('today');
    $counselor_days_past = $coun_from->diff($coun_to)->days;
    
    if ($counselor_days_past > 2) {
      $ret[$data['counselor_id']][$data['coun_assign_ts']] = $data['araz_id'];
    }    
  }
  
  if ($ret) {
    foreach ($ret as $counselor_id => $araz_data) {
      foreach ($araz_data as $coun_assign_date => $araz_id) {
      
        $mark_ids = join(',', $ret[$counselor_id]);
        $mark_query = "UPDATE `tlb_araz` SET `counselor_id` = 0, `coun_assign_ts` = '', `ho_remarks_counselor` = '' WHERE `id` IN ($mark_ids)";
        fwrite($fh, $mark_query . "\n");
        $mark_rslt = $db->query($mark_query);
        fwrite($fh, ($mark_rslt) ? 'Success' : 'fail');

        $mark_ids = join(',', $ret[$counselor_id]);
        $mark_query = "UPDATE `txn_view_araz` SET `counselor_id` = 0, `coun_assign_ts` = '', `ho_remarks_counselor` = '' WHERE `araz_id` IN ($mark_ids)";
        fwrite($fh, $mark_query . "\n");
        $mark_rslt = $db->query($mark_query);
        fwrite($fh, ($mark_rslt) ? 'Success' : 'fail');
        
        $mark_query = "INSERT INTO `tlb_log_counselor_remove` (`araz_id`, `counselor_id`, `coun_assign_ts`, `coun_remove_ts`) VALUES ( '$araz_id', '$counselor_id', '$coun_assign_date', '$curr_date')";
        fwrite($fh, $mark_query . "\n");
        $mark_rslt = $db->query($mark_query);
        fwrite($fh, ($mark_rslt) ? 'Success' : 'fail');
      }
    }
  }
}


$saheb_query = "SELECT * FROM `txn_view_araz` WHERE `saheb_id` != 0 AND `jawab_given` = '0' GROUP BY `araz_id`";
$saheb_result = $db->query_fetch_full_result($saheb_query);

if ($saheb_result) {
  foreach ($saheb_result as $data) {
    
    $saheb_from = new DateTime($data['saheb_assign_ts']);
    $saheb_to = new DateTime('today');
    $saheb_days_past = $saheb_from->diff($saheb_to)->days;
    
    if ($saheb_days_past > 2) {
      $ret[$data['saheb_id']][$data['saheb_assign_ts']] = $data['araz_id'];
    }    
  }
  
  if ($ret) {
    foreach ($ret as $saheb_id => $araz_data) {
      foreach ($araz_data as $saheb_assign_date => $araz_id) {
      
        $mark_ids = join(',', $ret[$saheb_id]);
        $mark_query = "UPDATE `tlb_araz` SET `saheb_id` = 0, `saheb_assign_ts` = '', `ho_remarks_saheb` = '' WHERE `id` IN ($mark_ids)";
        fwrite($fh, $mark_query . "\n");
        $mark_rslt = $db->query($mark_query);
        fwrite($fh, ($mark_rslt) ? 'Success' : 'fail');

        $mark_ids = join(',', $ret[$saheb_id]);
        $mark_query = "UPDATE `txn_view_araz` SET `saheb_id` = 0, `saheb_assign_ts` = '', `ho_remarks_saheb` = '' WHERE `araz_id` IN ($mark_ids)";
        fwrite($fh, $mark_query . "\n");
        $mark_rslt = $db->query($mark_query);
        fwrite($fh, ($mark_rslt) ? 'Success' : 'fail');
        
        $mark_query = "INSERT INTO `tlb_log_saheb_remove` (`araz_id`, `saheb_id`, `saheb_assign_ts`, `saheb_remove_ts`) VALUES ( '$araz_id', '$saheb_id', '$saheb_assign_date', '$curr_date')";
        fwrite($fh, $mark_query . "\n");
        $mark_rslt = $db->query($mark_query);
        fwrite($fh, ($mark_rslt) ? 'Success' : 'fail');
      }
    }
  }
}

?>
