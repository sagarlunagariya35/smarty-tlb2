<?php
require_once 'classes/class.user.admin.php';
require_once '../classes/class.user.php';
include 'classes/class.araiz.php';
$allowed_roles = array(ROLE_ADMIN_MARHALA_1,ROLE_ADMIN_MARHALA_2,ROLE_ADMIN_MARHALA_3,ROLE_ADMIN_MARHALA_4,ROLE_ADMIN_MARHALA_5,ROLE_ADMIN_MARHALA_6,ROLE_ADMIN_MARHALA_7,ROLE_ADMIN_MARHALA_8);
require_once 'session.php';

$user = new mtx_user_admin;
$araiz = new Araiz();

$title = 'Dashboard Raza Araiz';
$description = '';
$keywords = '';
$active_page = "manage_marhala_araz";

$select = FALSE;
$from_date = $to_date = $pagination = FALSE;
$araz_saheb_id = $saheb_id = $araz_coun_id = $coun_id = FALSE;
$counselor_days_past = $saheb_days_past = FALSE;

if(isset($_POST['print_list']))
{
  $_SESSION['track_id'] = (array) $_POST['jawab'];
  header('location: print_raza_araiz_list_date_wise.php');
}

$date = date('Y-m-d');

if($_SESSION[USER_ROLE] == ROLE_ADMIN_MARHALA_1){
  $marhala = '1';
}else if($_SESSION[USER_ROLE] == ROLE_ADMIN_MARHALA_2){
  $marhala = '2';
}else if($_SESSION[USER_ROLE] == ROLE_ADMIN_MARHALA_3){
  $marhala = '3';
}else if($_SESSION[USER_ROLE] == ROLE_ADMIN_MARHALA_4){
  $marhala = '4';
}else if($_SESSION[USER_ROLE] == ROLE_ADMIN_MARHALA_5){
  $marhala = '5';
}else if($_SESSION[USER_ROLE] == ROLE_ADMIN_MARHALA_6){
  $marhala = '6';
}else if($_SESSION[USER_ROLE] == ROLE_ADMIN_MARHALA_7){
  $marhala = '7';
}else if($_SESSION[USER_ROLE] == ROLE_ADMIN_MARHALA_8){
  $marhala = '8';
}

$select = $araiz->get_pending_marhala_araiz($marhala);
$total_araz = $araiz->get_pending_marhala_araz_count($marhala);

$counselors = $user->get_all_counselors();
$sahebs = $user->get_all_sahebs();

$araiz_parsed_data = $araiz->get_araiz_data_organised($select);

include ('header.php');
$counselor_and_saheb_content = COUNSELOR_AND_SAHEB_CONTENT;
$jawab_sent_content = JAWAB_SENT_CONTENT;
?>

<link rel="stylesheet" href="js/tablesorter/css/theme.bootstrap.css">
<script src="js/tablesorter/js/jquery.tablesorter.js"></script>
<script src="js/tablesorter/js/jquery.tablesorter.widgets.js"></script>
<style>
  .alfatemi-text{
    font-size: 18px;
  }
</style>
<div class="row">
  <div class="row">
    <div class="col-xs-12 col-sm-12">&nbsp;</div>
  </div>
  <div class="row">
    <div class="col-xs-12 col-sm-12">
      <h2 class="text-center">Total Pending Raza Araiz Count: <?php echo $total_araz; ?></h2>
    </div>
  </div>
  <div class="row">
    <div class="col-xs-12 col-sm-12">&nbsp;</div>
  </div>
  <form name="raza_form" class="form" method="post" action="">
    <div class="row">
      <div class="form-group col-xs-12 col-sm-1">
        <label for="from_date">From</label>
      </div>
      <div class="form-group col-xs-12 col-sm-3">
        <input type="date" name="from_date" class="form-control" value="<?php echo $from_date; ?>">
      </div>
      <div class="form-group col-xs-12 col-sm-1">
        <label for="to_date">To</label>
      </div>
      <div class="form-group col-xs-12 col-sm-3">
        <input type="date" name="to_date" class="form-control" value="<?php echo $to_date; ?>">
      </div>
      <div class="col-xs-12 col-sm-4">
        <input type="submit" name="search" value="Search" class="btn btn-block btn-success">
      </div>
    </div>
    <div class="row">
      <div class="col-xs-12">&nbsp;</div>
    </div>
    <div class="row">
      <div class="col-xs-12 col-sm-3">
        <select name="counselor_id" class="form-control" id="counselor_id">
          <option value="">Select Counselor</option>
          <?php
          if ($counselors) {
            foreach ($counselors as $coun) {
              ?>
              <option value="<?php echo $coun['user_id'] ?>"><?php echo $coun['first_name'] . ' ' . $coun['last_name']; ?></option>
              <?php
            }
          }
          ?>
        </select>
      </div>
      <div class="col-xs-12 col-sm-3">
        <input type="submit" name="sent_counselor" id="sent_counselor" value="Assign Counselor" class="btn btn-success btn-block assign-counselor">
      </div>
      <div class="col-xs-12 col-sm-3">
        <select name="saheb_id" class="form-control" id="saheb_id">
          <option value="">Select Saheb</option>
          <?php
          if ($sahebs) {
            foreach ($sahebs as $s) {
              ?>
              <option value="<?php echo $s['user_id'] ?>"><?php echo $s['first_name'] . ' ' . $s['last_name']; ?></option>
              <?php
            }
          }
          ?>
        </select>
      </div>
      <div class="col-xs-12 col-sm-3">
        <input type="submit" name="sent_saheb" id="sent_saheb" value="Assign Saheb" class="btn btn-success btn-block assign-saheb">
      </div>
    </div>
    <div class="row">
      <div class="col-xs-12">&nbsp;</div>
    </div>
    <div class="row">
      <div class="col-xs-12 col-sm-3" style="padding-top:6px">
        <input type="checkbox" name="print" value="Print" id="check_all" onchange="checkAll(this);" class="css-checkbox1"><label for="check_all" class="css-label1">Check All</label>
      </div>
      <div class="col-xs-12 col-sm-3">
        <input type="submit" name="print_list" value="Print" class="btn btn-success btn-block">
      </div>
      <div class="col-xs-12 col-sm-3">
        <input type="text" class="form-control" name="jawab_city" id="jawab_city" placeholder="Jawaab City">
      </div>
      <div class="col-xs-12 col-sm-3">
        <input type="submit" name="sent_jawab" id="sent_jawab" value="Send Jawab" class="btn btn-success btn-block assign-jawab">
      </div>
    </div>
  
  <div class="col-lg-12 col-xs-12 col-sm-12 pull-left">
    <div class="col-md-12">&nbsp;</div>
    <div class="clearfix"></div>
    <div class="row">
      <ul class="nav nav-tabs nav-justified" role="tablist">
        <li role="presentation" class="active"><a href="#new_araiz" aria-controls="pre" role="tab" data-toggle="tab">New Araiz</a></li>
        <li role="presentation"><a href="#araiz_from_counselor" aria-controls="in" role="tab" data-toggle="tab">Araiz from Counselor</a></li>
      </ul>
      <div class="row">
        <div class="col-xs-12 col-sm-12">&nbsp;</div>
      </div>
      <div class="tab-content">
        <div role="tabpanel" class="tab-pane active" id="new_araiz">
          <?php
          foreach ($araiz_parsed_data as $araz_id => $araz) {
            // Create bins for User Data, Araz Data, Previous Studies, Current Studeis, Remarks
            $user_details = $araz['user_data'];
            $araz_general_detail = $araz['araz_data'];
            $previous_study_details = $araz['previous_study_details'];
            $course_details = $araz['course_details'];
            $counselor_details = $araz['counselor_details'];

            if ($counselor_details['counselor_id'] == 0 && $araz_general_detail['saheb_id'] == 0 && $araz_general_detail['printed'] == 0) {
              include 'inc/inc.show_parsed_araiz_details.php';
            }
          }
          ?>
        </div>

        <div role="tabpanel" class="tab-pane" id="araiz_from_counselor">
          <?php
          foreach ($araiz_parsed_data as $araz_id => $araz) {
            // Create bins for User Data, Araz Data, Previous Studies, Current Studeis, Remarks
            $user_details = $araz['user_data'];
            $araz_general_detail = $araz['araz_data'];
            $previous_study_details = $araz['previous_study_details'];
            $course_details = $araz['course_details'];
            $counselor_details = $araz['counselor_details'];

            if ($counselor_details['coun_assign_ts'] > 0) {
              include 'inc/inc.show_parsed_araiz_details.php';
            }
          }
          ?>
        </div>
      </div>
    </div>
  </div>
</form>
</div>
<script>
  $(document).ready(function(){
    $(".example").popover();
  });
  
  function checkAll(ele) {
    if (ele.checked) {
      $('.jawab').prop('checked', true);
    } else {
      $('.jawab').prop('checked', false);
    }
  }

  $('.assign-counselor').on("click", function(){
    $('input:checkbox:checked.jawab').each(function(){
      var araz_id =  $(this).val();
      var coun_remark = $('#ho-remarks-' + araz_id).val();
      var coun_id = $('#counselor_id').val();
    
      $.ajax({
        type: "POST",
        url: "ajax.php",
        data: 'cmd=make_entry_of_counselor&araz_id=' + araz_id +'&counselor_id=' + coun_id +'&coun_remarks=' + coun_remark,
        cache: false,
        success: function () {
        }
      });
    });
  });
  
  $('.assign-saheb').on("click", function(){
    $('input:checkbox:checked.jawab').each(function(){
      var araz_id =  $(this).val();
      var saheb_remark = $('#ho-remarks-' + araz_id).val();
      var saheb_id = $('#saheb_id').val();
    
      $.ajax({
        type: "POST",
        url: "ajax.php",
        data: 'cmd=make_entry_of_saheb&araz_id=' + araz_id +'&saheb_id=' + saheb_id +'&saheb_remarks=' + saheb_remark,
        cache: false,
        success: function () {
        }
      });
    });
  });
  
  $('.assign-jawab').on("click", function(){
    $('input:checkbox:checked.jawab').each(function(){
      var araz_id =  $(this).val();
      var ho_remark = $('#ho-remarks-' + araz_id).val();
      var jawab_city = $('#jawab_city').val();
      
      if (jawab_city == '') {
        alert('Please Enter Jawab City\n');
        return false;
      }else {
        
        $.ajax({
          type: "POST",
          url: "ajax.php",
          data: 'cmd=make_entry_of_jawab&araz_id=' + araz_id +'&city=' + jawab_city +'&ho_remarks=' + ho_remark,
          cache: false,
          success: function () {
          }
        });
        
      }
    });
  });
  
  $('.assign-counselor-araz').on("click", function(){
    var araz_id = $(this).closest(".for-assign-araz").find(".assign_araz_id").val();
    var coun_remark = $('#ho-remarks-' + araz_id).val();
    var coun_id = $('#counselor_id' + araz_id).val();

    $.ajax({
      type: "POST",
      url: "ajax.php",
      data: 'cmd=make_entry_of_counselor&araz_id=' + araz_id +'&counselor_id=' + coun_id +'&coun_remarks=' + coun_remark,
      cache: false,
      success: function () {
      }
    });
  });
  
  $('.assign-saheb-araz').on("click", function(){
    var araz_id = $(this).closest(".for-assign-araz").find(".assign_araz_id").val();
    var saheb_remark = $('#ho-remarks-' + araz_id).val();
    var saheb_id = $('#saheb_id' + araz_id).val();

    $.ajax({
      type: "POST",
      url: "ajax.php",
      data: 'cmd=make_entry_of_saheb&araz_id=' + araz_id +'&saheb_id=' + saheb_id +'&saheb_remarks=' + saheb_remark,
      cache: false,
      success: function () {
      }
    });
  });
  
  $('.assign-jawab-araz').on("click", function(){
    var araz_id = $(this).closest(".for-assign-araz").find(".assign_araz_id").val();
    var ho_remark = $('#ho-remarks-' + araz_id).val();
    var jawab_city = $('#jawab_city' + araz_id).val();

    if (jawab_city == '') {
      alert('Please Enter Jawab City\n');
      return false;
    }else {

      $.ajax({
        type: "POST",
        url: "ajax.php",
        data: 'cmd=make_entry_of_jawab&araz_id=' + araz_id +'&city=' + jawab_city +'&ho_remarks=' + ho_remark,
        cache: false,
        success: function () {
        }
      });
    }
  });
  
  $('.araz_chat').on("click", function(){
    var araz_id = $(this).closest(".clschat").find(".chat_araz_id").val();
    var message = $('#chat-msg-' + araz_id).val();

    $.ajax({
      type: "POST",
      url: "ajax.php",
      data: 'cmd=make_entry_of_chat&araz_id=' + araz_id + '&message=' + message,
      cache: false,
      success: function () {
      }
    });
  });
</script>
<style type="text/css">
  .popover{
    color:#000;
  }
</style>
<?php
include 'footer.php';
?>
