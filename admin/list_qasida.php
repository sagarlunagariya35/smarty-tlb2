<?php
require_once '../classes/class.database.php';
require_once '../classes/constants.php';
require_once 'classes/class.user.admin.php';
require_once 'classes/class.qasida.php';

$allowed_roles = array(ROLE_HEAD_OFFICE);
require_once 'session.php';

// Get list of articles
$qsd = new Mtx_Qasaid();

$qasida_id = (isset($_GET['qasida_id'])) ? $_GET['qasida_id'] : 0;
$list_qasaid = $qsd->get_all_qasaid();

$title = 'List of Qasaid';
$description = '';
$keywords = '';
$active_page = "list_qasida";

if (isset($_POST['delete_id'])) {
  $delete_id = $_POST['delete_id'];
  $result = $qsd->delete_qasida($delete_id);

  if ($result) {
    $_SESSION[SUCCESS_MESSAGE] = "Qasida Successfully Delete";
    header('Location: list_qasida.php');
    exit();
  } else {
    $_SESSION[ERROR_MESSAGE] = "Error while Deleting";
  }
}

include_once("header.php");

?>
  <div class="row">
    <div class="col-lg-12 col-xs-12 col-sm-12  pull-left">
      <div class="col-md-12">&nbsp;</div>
      <div class="clearfix"></div>
      <div class="panel panel-green">
        <div class="panel-heading">
          <i class="fa fa-group fa-fw"></i> Manage Qasaid
          <span class="pull-right"><a href="qasida_add.php" style="color: white;">Add New Qasaud</a></span>
        </div>
        <!-- /.panel-heading -->
        <div class="panel-body">
          <div class="table-responsive">
            <table class="table table-striped table-hover table-condensed">
              <thead>
                <tr>
                  <th>Sr</th>
                  <th>Qasida</th>
                  <th>Action</th>
                </tr>
              </thead>
              <tbody>
                <?php
                if($list_qasaid){
                  $sr = 0;
                  foreach ($list_qasaid as $q) {
                    $sr++;
                ?>
                    <tr>
                      <td><?php echo $sr; ?></td>
                      <td><?php echo $q['title']; ?></td>
                      <td>
                        <a href="edit_qasida.php?id=<?php echo $q['id']; ?>"><i class="fa fa-edit fa-fw"></i></a>
                      <a href="#" class="confirm" id="<?php echo $q['id']; ?>"><i class="fa fa-remove fa-fw text-danger"></i></a>
                      </td>
                    </tr>
                    <?php
                  }
                }else {
                  echo '<tr><td class="text-center" colspan="4">No Records..</td></tr>';
                }
                ?>
              </tbody>
            </table>
          </div>
          <!-- /.table-responsive -->
        </div>
        <!-- /.panel-body -->
      </div>
    </div>
    <form method="post" id="frm-del-grp">
      <input type="hidden" name="delete_id" id="del-qasida-val" value="">
    </form>
  </div><!-- ./ row -->
  <script>
    $(".confirm").confirm({
      text: "Are you sure you want to delete?",
      title: "Confirmation required",
      confirm: function(button) {
        $('#del-qasida-val').val($(button).attr('id'));
        $('#frm-del-grp').submit();
        alert('Are you Sure You want to delete: ' + id);
      },
      cancel: function(button) {
        // nothing to do
      },
      confirmButton: "Yes",
      cancelButton: "No",
      post: true,
      confirmButtonClass: "btn-danger"
    });
  </script>
  
<?php include "./footer.php"; ?>
