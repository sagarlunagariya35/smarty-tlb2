<?php
require_once 'classes/class.user.admin.php';
require_once '../classes/class.user.php';
include 'classes/class.araiz.php';
$allowed_roles = array(ROLE_COUNSELOR,ROLE_SAHEB);
require_once 'session.php';

$user = new mtx_user_admin;
$araiz = new Araiz();

$title = 'Dashboard Araiz';
$description = '';
$keywords = '';
$active_page = "manage_coun_araz";

$select = FALSE;
$from_date = $to_date = $pagination = FALSE;
$araz_saheb_id = $saheb_id = $araz_coun_id = $coun_id = FALSE;
$counselor_days_past = $saheb_days_past = $jamiyet_data = FALSE;
$date = date('Y-m-d');

if(isset($_POST['print_list']))
{
  $_SESSION['track_id'] = (array) $_POST['jawab'];
  header('location: print_raza_araiz_list_date_wise.php');
}
include ('header.php');

$select = $araiz->get_all_assign_araz_to_counselor($_SESSION[USER_ID], $page, 10);
$total_araz = $araiz->count_total_assign_araz_to_counselor($_SESSION[USER_ID]);

$araiz_parsed_data = $araiz->get_araiz_data_organised($select);
$courses = $araiz->get_all_courses();
$countries = $araiz->get_all_countries_list();
?>

<link rel="stylesheet" href="js/tablesorter/css/theme.bootstrap.css">
<script src="js/tablesorter/js/jquery.tablesorter.js"></script>
<script src="js/tablesorter/js/jquery.tablesorter.widgets.js"></script>
<style>
  .alfatemi-text{
    font-size: 18px;
  }
</style>
<div class="row">

  <div class="row">
    <div class="col-xs-12 col-sm-12">&nbsp;</div>
  </div>
  <div class="row">
    <div class="col-xs-12 col-sm-12">
      <h2 class="text-center">Total Pending Araiz Count: <?php echo $total_araz; ?></h2>
    </div>
  </div>
  <div class="row">
    <div class="col-xs-12 col-sm-12">&nbsp;</div>
  </div>
  <form name="raza_form" class="form" method="post" action="">
    <div class="row">
      <div class="col-xs-12 col-sm-3" style="padding-top:6px">
        <input type="checkbox" name="print" value="Print" id="check_all" onchange="checkAll(this);" class="css-checkbox1"><label for="check_all" class="css-label1">Check All</label>
      </div>
      <div class="col-xs-12 col-sm-3 pull-right">
        <input type="submit" name="sent_jawab" id="sent_jawab" value="Send Comment" class="btn btn-success btn-block sent-suggestion">
      </div>
    </div>
  
  <div class="col-lg-12 col-xs-12 col-sm-12 pull-left">
    <div class="col-md-12">&nbsp;</div>
    <div class="clearfix"></div>
    <div class="row">
      <?php
      require_once 'pagination.php';
      $pagination = pagination(10, $page, 'saheb_araz_list.php?page=', $total_araz);
        
      foreach ($araiz_parsed_data as $araz_id => $araz) {
        // Create bins for User Data, Araz Data, Previous Studies, Current Studeis, Remarks
        $user_details = $araz['user_data'];
        $araz_general_detail = $araz['araz_data'];
        $previous_study_details = $araz['previous_study_details'];
        $course_details = $araz['course_details'];
        $counselor_details = $araz['counselor_details'];
        
        include 'inc/inc.show_parsed_araiz_details.php';
        }
      echo $pagination;
      ?>
    </div>
  </div>
</form>
</div>
<style>
  .popover{
    color:#000;
  }
</style>
<script>
  $('.sent-suggestion').on("click", function(){
    $('input:checkbox:checked.jawab').each(function(){
      var araz_id =  $(this).val();
      var course = $('#course-' + araz_id).val();
      var country = $('#country-' + araz_id).val();
      var city = $('#city-' + araz_id).val();
      var remarks = $('#remarks-' + araz_id).val();
      var coun_id = <?php echo $_SESSION[USER_ID]; ?>;
    
      $.ajax({
        type: "POST",
        url: "ajax.php",
        data: 'cmd=make_entry_of_counselor_suggestion&araz_id=' + araz_id +'&counselor_id=' + coun_id +'&course=' + course + '&country=' + country + '&city=' + city + '&remarks=' + remarks,
        cache: false,
        success: function () {
        }
      });
    });
  });
  
  $('.sent-araz-suggestion').on("click", function(){
    var araz_id = $(this).closest(".clslocation").find(".assign_coun_sugg_araz_id").val();
    var course = $('#course-' + araz_id).val();
    var country = $('#country-' + araz_id).val();
    var city = $('#city-' + araz_id).val();
    var remarks = $('#remarks-' + araz_id).val();
    var coun_id = <?php echo $_SESSION[USER_ID]; ?>;

    $.ajax({
      type: "POST",
      url: "ajax.php",
      data: 'cmd=make_entry_of_counselor_suggestion&araz_id=' + araz_id +'&counselor_id=' + coun_id +'&course=' + course + '&country=' + country + '&city=' + city + '&remarks=' + remarks,
      cache: false,
      success: function () {
      }
    });
  });
</script>

<?php
include 'footer.php';
?>

