<?php
require_once 'classes/class.user.admin.php';
include 'classes/class.addcountry.php';
$allowed_roles = array(ROLE_HEAD_OFFICE);
require_once 'session.php';

$title = 'Country';
$description = '';
$keywords = '';
$active_page = "manage_countries";


$country = new Country();
$id = FALSE;
$select = FALSE;

if (isset($_GET['delete_id'])) {
  global $db;

  $delete_id = $_GET['delete_id'];
  $delete_user = "delete from country where id = '$delete_id'";
  $result = $db->query($delete_user);

  if ($result) {
    $_SESSION[SUCCESS_MESSAGE] = "User Successfully Delete";
  } else {
    $_SESSION[ERROR_MESSAGE] = "Error In Delete";
  }
}

if (isset($_GET['id'])) {
  $select = $country->country_result(false, false,$_GET['id']);
//print_r($select);
  $id = $_GET['id'];
}

if (isset($_POST['Update'])) {
  $id = $_GET['id'];
  $cntry = $_POST['cntname'];

  $question_update = $country->update_cnt($cntry, $id);

  if ($question_update)
    $_SESSION[SUCCESS_MESSAGE] = "country Successfully Update";
  if (!$question_update)
    $_SESSION[ERROR_MESSAGE] = "Error In Update Data";
}

if (isset($_POST['Save'])) {

  $cntry = $_POST['cntname'];

  $question_insert = $country->insert_cnt($cntry);
  if ($question_insert)
    $_SESSION[SUCCESS_MESSAGE] = "country Successfully Insert";
  if (!$question_insert)
    $_SESSION[ERROR_MESSAGE] = "Error In Inserting Data";
}

$heading = $id ? 'Update Country' : $heading = 'Add New Country';
$btn_name = $id ? 'Update' : 'Add New';

include ('header.php');
?>

  <div class="row">

    <div class="col-lg-4 col-xs-12 col-sm-12 pull-right">
      <div class="col-lg-12">
        <h3 class="page-header"><?php echo $heading; ?></h3>
      </div>
      <!-- /.col-lg-12 -->

      <form method="post"  action="addcountry.php" role="form" class="form-horizontal" >
        <div class="col-md-12">&nbsp;</div>

        <div class="form-group">
          <label class="control-label">Country:</label>
          <input type="text" class="form-control" name="cntname"  placeholder="Country Name" value="<?php echo $select[0]['nicename']; ?>" required>
        </div>

        <div class="form-group">
            <button name="<?php echo $btn_name; ?>" class="btn btn-success btn-block"><?php echo $btn_name ?></button>
        </div>
        <?php if ($id) echo '<input type="hidden" name="id" value="' . $id . '">'; ?>
      </form>
    </div>

    <div class="col-lg-8 col-xs-12 col-sm-12 pull-left">
      <div class="col-md-12">&nbsp;</div>
      <div class="clearfix"></div>
      <div class="panel panel-primary">
        <div class="panel-heading">
          <i class="fa fa-group fa-fw"></i> Countries
        </div>
        <!-- /.panel-heading -->
        <div class="panel-body">
          <div class="table-responsive">
            <table class="table table-striped table-hover table-condensed">
              <thead>
              <th>No</th>
              <th>Country</th>
              <th>Action</th>
              </thead>
              <tbody>
                <?php
                $select = $country->country_result($page, 10);
                $count_question = $country->count_cnt_record1();
                require_once 'pagination.php';
                $pagination = pagination(10, $page, 'addcountry.php?page=', $count_question);

                $i = (($page - 1) * 10) + 1;
                foreach ($select as $data) {
                  ?>

                  <tr>
                    <td><?php echo $i++; ?></td>
                    <td><?php echo $data['nicename']; ?></td>

                    <td>
                      <a href="addcountry.php?id=<?php echo $data['id']; ?>"><i class="fa fa-edit fa-fw"></i></a>
                      <a href="#" class="confirm"><i class="fa fa-remove fa-fw text-danger"></i></a>
                    </td>
                  </tr>
                  <?php
                }
                ?>
              </tbody>
            </table>
          </div>
          <!-- /.table-responsive -->
        </div>
        <div class="panel-footer">
          <?php echo $pagination; ?>
          <div class="clearfix"></div>
        </div>
        <!-- /.panel-body -->
      </div>
    </div>

    <form method="post" id="frm-del-grp">
      <input type="hidden" name="id" id="del-country-val" value="">
      <input type="hidden" name="cmd" value="del-country">
    </form>
  </div>
  <script>
    $(".confirm").confirm({
      text: "Are you sure you want to delete?",
      title: "Confirmation required",
      confirm: function(button) {
        $('#del-grp-val').val($(button).attr('id'));
        $('#frm-del-grp').submit();
        alert('Are you Sure You want to delete: ' + id);
      },
      cancel: function(button) {
        // nothing to do
      },
      confirmButton: "Yes",
      cancelButton: "No",
      post: true,
      confirmButtonClass: "btn-danger"
    });
  </script>
<!-- /#page-wrapper -->

<!-- /#wrapper -->
<?php
include 'footer.php';
?>
