<?php

session_start();
session_destroy();
session_start();

require_once 'classes/class.user.admin.php';
require_once 'classes/admin_gen_functions.php';

// Verify if required variables posted
$its = $_REQUEST['its_id'];
$jamat = $_REQUEST['jamat_name'];
$tanzeem_id = $_REQUEST['tanzeem_id'];
$verification_code = $_REQUEST['key'];
$vazarat_salt = 'Vaz6589435183';

$error = array();

if ($its == '') {
  $error[] = 'ITS ID is required';
}

if ($jamat == '') {
  $error[] = 'Jamaat not specified';
}

if ($tanzeem_id == '') {
  $error[] = 'Tanzeem not specified';
}

if (count($error) > 0) {
  return_error($error);
  exit();
}

// verify the key
$key = md5($tanzeem_id . $its . $jamat . $vazarat_salt);

if ($key !== $verification_code) {
  return_error(array('Verification failed.'));
  exit();
}
$user = new mtx_user_admin;
$isAuthorized['success'] = FALSE;

function return_error($error) {
  $_SESSION['error_msg'] = implode("<br>\n", $error);
  header('location: login_error.php');
  exit();
}

// Verify the tanzeem and jamat name with database
$query = "SELECT * FROM tlb_link_tanzeem_jamaat WHERE tanzeem_id = '$tanzeem_id' AND jamat_name = '$jamat'";
$result = $db->query_fetch_full_result($query);

if ($result && count($result) > 0) {
  $user_exists = $user->check_user_exist($its);
  if ($user_exists) {
    $isAuthorized['success'] = TRUE;
    $_SESSION[USER_LOGGED_IN] = TRUE;
    $_SESSION[IS_ADMIN] = TRUE;
    $_SESSION[USER_ROLE] = ROLE_AAMIL_SAHEB;
    $_SESSION[USER_ITS] = $its;
    $_SESSION[USER_ID] = $user->get_user_id();
    $_SESSION[USER_TYPE] = $user->get_user_type();
    $_SESSION[TANZEEM_ID] = $tanzeem_id;
    $_SESSION[JAMAT_NAME] = $result[0]['jamat_name'];
    $_SESSION[EDU_HUB] = $result[0]['hub_name'];
    if($result[0]['jamat_name'] == $result[0]['hub_name']){
      $_SESSION[IS_EDU_HUB] = TRUE;
    } else {
      $_SESSION[IS_EDU_HUB] = FALSE;
    }
  } else {
    return_error(array('Invalid login credentials'));
    exit();
  }
} else {
  // LOG THE error for reference
  $date = date('Y-m-d H:i:s');
  $query = "INSERT INTO tlb_err_vaz_login (its, tanzeem_id, jamat_name, timestamp) VALUES ('$its','$tanzeem_id','$jamat', '$date')";
  $db->query($query);
  return_error(array('Invalid login credentials'));
  exit();
}


header('Location:' . SERVER_PATH . 'admin/as_create_edu_committee.php');
exit();
?>
