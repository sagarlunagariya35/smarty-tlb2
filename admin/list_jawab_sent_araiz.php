<?php
require_once 'classes/class.user.admin.php';
require_once '../classes/class.user.php';
include 'classes/class.araiz.php';
$allowed_roles = array(ROLE_HEAD_OFFICE, ROLE_AAMIL_SAHEB, ROLE_JAMIAT_COORDINATOR);
require_once 'session.php';

$user = new mtx_user_admin;
$araiz = new Araiz();

$title = 'Jawab Sent Araiz List';
$description = '';
$keywords = '';
$active_page = "manage_jawab_sent";

$select = FALSE;
$from_date = $to_date = $pagination = FALSE;
$araz_saheb_id = $saheb_id = $araz_coun_id = $coun_id = FALSE;
$counselor_days_past = $saheb_days_past = $jamiyet_data = FALSE;
$date = date('Y-m-d');

$jamiyet = unserialize($_SESSION['jamiat']);
if (count($jamiyet) == 1 && $jamiyet[0] != '') {
  $jamiyet_data = implode("','", $jamiyet[0]);
}

// Fix for ROLE_JAMIAT_COORDINATOR is no jamiat assign
if ($_SESSION[USER_ROLE] == ROLE_JAMIAT_COORDINATOR && $jamiyet_data === FALSE) {
  $jamiyet_data = ' ';
}

if(isset($_POST['send_email_raza'])){
  $araz_ids = (array) $_POST['jawab'];
  $subject = $_POST['subject_raza'];
  $txt_message = $_POST['txt_message_raza'];
  
  if($araz_ids){
    foreach ($araz_ids as $a_id){
      $result = $araiz->araz_send_email_to_student_from_jawab_sent($a_id,$subject,$txt_message);
    }
    
    if($result){
      $_SESSION[SUCCESS_MESSAGE] = 'E-mail Sent Successfully..';
    }else {
      $_SESSION[ERROR_MESSAGE] = 'Error Encounter While Sending E-mail';
    }
  }else {
    $_SESSION[ERROR_MESSAGE] = 'Please Select any Araz for sent E-mail';
  }
}

if(isset($_POST['send_email_istirshad'])){
  $araz_ids = (array) $_POST['jawab'];
  $subject = $_POST['subject_istirshad'];
  $txt_message = $_POST['txt_message_istirshad'];
  
  if($araz_ids){
    foreach ($araz_ids as $a_id){
      $result = $araiz->araz_send_email_to_student_from_jawab_sent($a_id,$subject,$txt_message);
    }
    
    if($result){
      $_SESSION[SUCCESS_MESSAGE] = 'E-mail Sent Successfully..';
    }else {
      $_SESSION[ERROR_MESSAGE] = 'Error Encounter While Sending E-mail';
    }
  }else {
    $_SESSION[ERROR_MESSAGE] = 'Please Select any Araz for sent E-mail';
  }
}
include ('header.php');

$select = $araiz->get_all_jawab_sent_araiz(FALSE, $page, 10, $jamiyet_data);
$total_araz = $araiz->get_jawab_sent_araiz_count(FALSE, $jamiyet_data);

$araiz_parsed_data = $araiz->get_araiz_data_organised($select);
?>
<link rel="stylesheet" href="js/tablesorter/css/theme.bootstrap.css">
<script src="js/tablesorter/js/jquery.tablesorter.js"></script>
<script src="js/tablesorter/js/jquery.tablesorter.widgets.js"></script>
<style>
  .alfatemi-text{
    font-size: 18px;
  }
</style>
<div class="row">
  <div class="row">
    <div class="col-xs-12 col-sm-12">&nbsp;</div>
  </div>
  <div class="row">
    <div class="col-xs-12 col-sm-12">
      <h2 class="text-center">Total Jawab Sent Araiz Count: <?php echo $total_araz; ?></h2>
    </div>
  </div>
  <div class="row">
    <div class="col-xs-12 col-sm-12">&nbsp;</div>
  </div>
  <form name="raza_form" class="form" method="post" action="">
    <div class="col-lg-12 col-xs-12 col-sm-12 pull-left">
      <div class="col-md-12">&nbsp;</div>
      <div class="clearfix"></div>
      <div class="row">
        <ul class="nav nav-tabs nav-justified" role="tablist">
          <li role="presentation" class="active"><a href="#raza_araiz" aria-controls="pre" role="tab" data-toggle="tab">Raza Araiz</a></li>
          <li role="presentation"><a href="#istirshad_araiz" aria-controls="in" role="tab" data-toggle="tab">Isttirshad Araiz</a></li>
        </ul>
        <div class="row">
          <div class="col-xs-12 col-sm-12">&nbsp;</div>
        </div>
        <?php
          require_once 'pagination.php';
          $pagination = pagination(10, $page, 'list_jawab_sent_araiz.php?page=', $total_araz);
        ?>
        <div class="tab-content">
          <div role="tabpanel" class="tab-pane active" id="raza_araiz">
            <div class="col-xs-12 col-sm-2" style="padding-top:6px">
              <input type="checkbox" name="print" value="Print" id="check_all_raza" onchange="checkAllRaza(this);" class="css-checkbox1"><label for="check_all_raza" class="css-label1">Check All</label>
            </div>
            <div class="col-xs-12 col-sm-8">
              <input type="text" name="subject_raza" class="form-control" placeholder="Enter Subject"><br>
              <textarea name="txt_message_raza" placeholder="Enter Message" class="form-control"></textarea>
            </div>
            <div class="col-xs-12 col-sm-2">
              <input type="submit" name="send_email_raza" value="Send Email" class="btn btn-success btn-block">
            </div>
            <div class="row">
              <div class="col-xs-12 col-sm-12">&nbsp;</div>
            </div>
            <?php
            foreach ($araiz_parsed_data as $araz_id => $araz) {
              // Create bins for User Data, Araz Data, Previous Studies, Current Studeis, Remarks
              $user_details = $araz['user_data'];
              $araz_general_detail = $araz['araz_data'];
              $previous_study_details = $araz['previous_study_details'];
              $course_details = $araz['course_details'];
              $counselor_details = $araz['counselor_details'];

              if($user_details['araz_type'] == 'raza'){
                include 'inc/inc.show_parsed_araiz_details.php';
              }
            }
            ?>
          </div>

          <div role="tabpanel" class="tab-pane" id="istirshad_araiz">
            <div class="col-xs-12 col-sm-2" style="padding-top:6px">
              <input type="checkbox" name="print" value="Print" id="check_all_istirshad" onchange="checkAllIstirshad(this);" class="css-checkbox1"><label for="check_all_istirshad" class="css-label1">Check All</label>
            </div>
            <div class="col-xs-12 col-sm-8">
              <input type="text" name="subject_istirshad" class="form-control" placeholder="Enter Subject"><br>
              <textarea name="txt_message_istirshad" placeholder="Enter Message" class="form-control"></textarea>
            </div>
            <div class="col-xs-12 col-sm-2">
              <input type="submit" name="send_email_istirshad" value="Send Email" class="btn btn-success btn-block">
            </div>
            <div class="row">
              <div class="col-xs-12 col-sm-12">&nbsp;</div>
            </div>
            <?php
            foreach ($araiz_parsed_data as $araz_id => $araz) {
              // Create bins for User Data, Araz Data, Previous Studies, Current Studeis, Remarks
              $user_details = $araz['user_data'];
              $araz_general_detail = $araz['araz_data'];
              $previous_study_details = $araz['previous_study_details'];
              $course_details = $araz['course_details'];
              $counselor_details = $araz['counselor_details'];

              if($user_details['araz_type'] == 'istirshad'){
                include 'inc/inc.show_parsed_araiz_details.php';
              }
            }
            ?>
          </div>
        </div>
        <?php echo $pagination; ?>
      </div>
    </div>
  </form>
</div>

<style type="text/css">
  .popover{
    color:#000;
  }
</style>
<?php
include 'footer.php';
?>

