<?php
require_once 'classes/class.user.admin.php';
include 'classes/class.araiz.php';
$allowed_roles = array(ROLE_HEAD_OFFICE);
require_once 'session.php';

$title = 'Manage Institutes';
$description = '';
$keywords = '';
$active_page = "manage_lp_institutes";

$araiz = new Araiz();
$id = FALSE;
$select = $city_name = FALSE;

if(isset($_GET['city'])){
  $city_name = $_GET['city'];
  $select = $araiz->get_lp_institute_data_of_city($city_name);
}

if(isset($_GET['delete_id'])) {
  $delete_id = $_GET['delete_id'];
  
  $result = $araiz->delete_lp_institutes($delete_id);

  if ($result) {
    $_SESSION[SUCCESS_MESSAGE] = "Institute Successfully Delete";
    header('location: lp_institutes.php?city='.$city_name);
    exit();
  } else {
    $_SESSION[ERROR_MESSAGE] = "Error In Delete";
  }
}

if(isset($_GET['status']))
{
    $id = $_GET['id'];
    $status = $_GET['status'];
    if($status == '1') 
    {
        $From = 'Active';
        $To = 'Inactive';
        $Value = '0';
    }
    else
    {			
        $From = 'Inactive';
        $To = 'Active';
        $Value = '1';
    }
    
    $result = $araiz->change_lp_institutes_status($Value, $id);
    
    if($result > 0)
    {
        $_SESSION[SUCCESS_MESSAGE] = "Status is Changed from '$From' To '$To'";
        header('location: lp_institutes.php?city='.$city_name);
        exit();
    }
    else
    {
       $_SESSION[ERROR_MESSAGE] = "Status does not Change";
    }
}

include ('header.php');
?>

<style>
  tfoot {
    display: table-header-group;
    width: 100%;
    padding: 3px;
    box-sizing: border-box;
  }
  /*.dataTables_info, .dataTables_paginate{
    display: inline-block;
  }*/
</style>

<div id="wrapper">
  
  <div class="row">&nbsp;</div>
  <div class="row">
    <form method="get" action="">
      <div class="col-md-12">
        <div class="col-md-4 pull-right">
          <?php $cities = $araiz->get_lp_institute_city(); ?>
          <select name="city" class="form-control" onchange="this.form.submit();">
            <option value="">Select City</option>
              <?php if($cities) { foreach ($cities as $city) { ?>
              <option <?php if($city_name == $city['city']) { echo 'selected'; } ?> value="<?php echo $city['city']; ?>"><?php echo $city['city']; ?></option>
              <?php } } ?>
            </select>
        </div>
        <div class="col-md-1 pull-right">
          <h5>City :</h5>
        </div>
      </div>
      
      <div>&nbsp;</div>
      
      <div class="col-lg-12">
        <div class="panel panel-default">
          <div class="panel-heading">
            Institutes List
          </div>
          <!-- /.panel-heading -->
          <div class="panel-body">
            <div class="table-responsive">
              <table class="table table-striped table-condensed table-hover" id="example">
                <thead>
                  <tr>
                    <th>Id</th>
                    <th>Group Id</th>
                    <th>Institute Name</th>
                    <th>City</th>
                    <th>Action</th>
                  </tr>
                </thead>
                <tbody>
                  <?php
                  if($select){
                    $i = 1;
                    foreach ($select as $data) {
                    ?>

                    <tr>
                      <td><?php echo $data['id']; ?></td>
                      <td><?php echo $data['marhala']; ?></td>
                      <td><?php echo $data['institute']; ?></td>
                      <td><?php echo $data['city']; ?></td>
                      <td>
                        <?php if($data['approved'] == '0') { ?>
                        <a href="lp_institutes.php?city=<?php echo $city_name; ?>&id=<?php echo $data['id']; ?>&status=<?php echo $data['approved']; ?>" class="confirm"><i class="fa fa-ban fa-fw text-danger"></i></a>
                        <?php } else { ?>
                        <a href="lp_institutes.php?city=<?php echo $city_name; ?>&id=<?php echo $data['id']; ?>&status=<?php echo $data['approved']; ?>"><i class="fa fa-check-circle fa-fw"></i></a>
                        <?php } ?>

                        <a href="lp_institutes.php?city=<?php echo $city_name; ?>&delete_id=<?php echo $data['id']; ?>" class="confirm"><i class="fa fa-remove fa-fw text-danger"></i></a>
                      </td>

                    </tr>
                    <?php
                    }
                  }else {
                    echo '<tr><td class="text-center" colspan="5">No Records..</td></tr>';
                  }
                  ?>
                </tbody>
              </table>
            </div>
            <!-- /.table-responsive -->
          </div>
          <!-- /.panel-body -->
        </div>
        <!-- /.panel -->
        </div>
      </form>
    </div>
    <!-- /.row -->
  <!-- /#page-wrapper -->
</div
<!-- /#wrapper -->

<?php
include 'footer.php';
?>

