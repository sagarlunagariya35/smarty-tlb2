<?php

require_once '../classes/constants.php';
$allowed_roles = array(ROLE_AAMIL_SAHEB, ROLE_COMMITTEE_MEMBER, ROLE_EDU_SEC, ROLE_HEAD_OFFICE);
include 'session.php';
header('Location: home.php');