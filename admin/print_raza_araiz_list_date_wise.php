<?php
require_once 'classes/class.user.admin.php';
include 'classes/class.araiz.php';
$allowed_roles = array(ROLE_HEAD_OFFICE, ROLE_AAMIL_SAHEB, ROLE_SAHEB, ROLE_COUNSELOR, ROLE_JAMIAT_COORDINATOR);
require_once 'session.php';

$user = new mtx_user_admin;
$araiz = new Araiz();

$id = $select = $from_date = $to_date = FALSE;

if ($_SESSION['track_id'] != '') {

  $track_id = $_SESSION['track_id'];
  $result = $araiz->print_all_selected_araz($track_id);
  $select = $araiz->get_araz_by_track_id($track_id);
} else {

  $from_date = $_GET['from_date'];
  $to_date = $_GET['to_date'];
  $result = $araiz->print_all_selected_araz_by_date_wise($from_date, $to_date);
  $select = $araiz->get_raza_araz_datewise($from_date, $to_date);
}

$title = 'Araiz List';
$description = '';
$keywords = '';
$active_page = "manage_araiz";

$user_full_name = $user->get_all_user($_SESSION[USER_ID]);

include ('print_header.php');
?>
<body style="padding: 10px;">
  <style>
    .border-only-top {
      border-top: 1px solid #000 !important;
    }
    .border {
      border: 1px solid #000;
      border-top: 0px;
    }
    .border-sides{
      border-left: 1px solid #000;
      border-right: 1px solid #000;
    }
  </style>
  <!--div>
    <p style="display: block; text-align: right"><?php echo date('d-m-Y H:i:s'); ?></p>
  </div-->
  <div class="row text-center">
    <img src="images/Logo-tlb-araz.jpg" height="100" >
  </div>

  <div class="row">
    <div class="col-lg-12">
      <h3 class="page-header font18">List of Araiz</h3>
    </div>
    <!-- /.col-lg-12 -->
  </div>

  <div class="col-xs-12">
    <div class="col-xs-2 text-left alfatemi-text pull-left" style="font-size: 36px; font-weight: 600; padding: 0;">رزا</div>
    <div class="col-xs-6 text-right  alfatemi-text pull-right" style="font-size: 20px; font-weight: 500; padding: 0;">غبّ السّجدات  العبودية</div>
  </div>
  <div class="col-xs-12 border border-only-top">
    <div class="col-xs-2 text-left alfatemi-text pull-left" style="padding: 0;">ملاحظة</div>
    <div class="col-xs-6  alfatemi-text">&nbsp;</div>
    <div class="col-xs-2 text-right  alfatemi-text pull-right" style="font-size: 16px; font-weight: 500; padding: 0;" dir="ltr">Track No.</div>
  </div>
  <?php
  $i = 0;
  foreach ($select as $data) {
    $i++;
    $degree = $course = '';
    $ary_course = array();

    $student_name = $data['full_name_ar'];
    $ITS_city = $data['city'];
    $track_no = $data['araz_id'];
    $age = $araiz->ageCalculator($data['dob']);
    $degree = $data['degree_name'];

    if ($data['institute_name'] != '') {
      $institute_name = $data['institute_name'];
    } else {
      $institute_name = $data['school_name'];
    }

    if ($data['institute_city'] != '') {
      $place = $data['institute_city'];
    } else {
      $place = $data['school_city'];
    }

    if ($data['course_name'] != '') {
      $course = $data['course_name'];
    } else {
      $course = $data['school_standard'];
    }

    if ($data['course_duration'] != '') {
      $duration = $data['course_duration'];
    } else {
      $duration = '-';
    }

    if ($data['accomodation'] != '') {
      $accomodation = $data['accomodation'];
    } else {
      $accomodation = '-';
    }

    ($degree) ? $ary_course[] = $degree : '';
    ($course) ? $ary_course[] = $course : '';
    ?>
    <div class="col-xs-12 border">
      <div class="col-xs-1"></div>
      <div class="col-xs-10 border-sides">
        <div class="row">
          <div class="col-xs-12">
            <div class="col-xs-4 border">
              <span class="alfatemi-text font18" dir="rtl"><strong>موضع: </strong> <?php echo $ITS_city; ?></span>
            </div>
            <div class="col-xs-8" dir="rtl">
              <span class="alfatemi-text font18"><strong>الاسم: </strong> <?php echo $student_name; ?></span>
            </div>
          </div>
        </div>
        <?php
        if ($data['marhala'] > 5 && $data['current_course'] != '') {
          ?>
          <div class="col-md-12 text-center">
            <span class="alfatemi-text font18" dir="rtl">
              في الحال حسب الذيل تعليم حاصل كروطط ححهوطط
            </span>
          </div>
          <table class="table table-bordered table-condensed" dir="ltr">
            <thead>
              <tr>
                <th>Institute name</th>
                <th>Course</th>
                <th>Country</th>
                <th>City</th>
              </tr>
            </thead>
            <tbody>
              <?php
              $current_inst_name = ($data['current_inst_name']) ? $data['current_inst_name'] : $institute_name;
              $current_course = ($data['current_course']) ? $data['current_course'] : join(' / ', $ary_course);
              $current_inst_country = ($data['current_inst_country']) ? $data['current_inst_country'] : $data['institute_country'];
              $current_inst_city = ($data['current_inst_city']) ? $data['current_inst_city'] : $place;
              ?>
              <tr>
                <td><?php echo $current_inst_name; ?></td>
                <td><?php echo $current_course; ?></td>
                <td><?php echo $current_inst_country; ?></td>
                <td><?php echo $current_inst_city; ?></td>
              </tr>
            </tbody>
          </table>
          <?php
        }
        ?>
        <div class="row">
          <div class="col-md-12 text-center">
            <span class="alfatemi-text font18">
              <?php
              // find out if the user has already started the course
              if ($data['course_started']) {
                $course_started_dt = strtotime($data['course_started']);
              } else {
                $course_started_dt = time() + 84000;
              }
              //$course_started_dt = strtotime($data['course_started']);
              $course_already_started = ($course_started_dt < time()) ? TRUE : FALSE;
              
              if ($course_already_started) {
                echo 'ادبًا عرض كه<br>في الحال حسب الذيل تعليم حاصل كروطط ححهوطط';
              } else {
                echo 'مزيد تعليم واسطسس عرض؛';
              }
              ?>
            </span>
          </div>
        </div>
        <table class="table table-bordered table-condensed" dir="ltr">
          <thead>
            <tr>
              <th>Inst. name</th>
              <th>Place</th>
              <th><?php echo ($data['marhala'] < 5) ? 'Standard / Grade' : 'Degree / Course'; ?></th>
              <?php if ($data['marhala'] > 4) { ?>
                <th><?php echo ($data['marhala'] == 5) ? 'Standard' : 'Duration'; ?></th>
                <th>Accomdn.</th>
              <?php } ?>
              <th>Age</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td><?php echo ($institute_name) ? $institute_name : 'Not yet Decided'; ?></td>
              <td><?php echo ($place) ? $place : 'Not yet Decided'; ?></td>
              <td><?php
                //echo (count($ary_course)) ? join(' / ', $ary_course) : 'Not yet Decided';
                echo ($course) ? $course : 'Not yet Decided';
                ?></td>
              <?php if ($data['marhala'] > 4) { ?>
                <td><?php echo $duration; ?></td>
                <td><?php echo ($accomodation) ? $accomodation : 'Not yet Decided'; ?></td>
              <?php } ?>
              <td><?php echo $age ?></td>
            </tr>
          </tbody>
        </table>
        <div class="col-md-12 text-center" dir="rtl">
          <span class="alfatemi-text font18">رزا انسس دعاء مبارك فضل فرما وا ادبـًا عرض ؛،</span>
        </div>
        <?php
        $remarks = FALSE;
        if ($course_already_started && $data['marhala'] > 4) {
          $remarks = ($data['gender'] == 'M') ? 'المذكور تعليم حاصل كروانو شروع كري ححكا؛' : 'المذكور ةتعليم حاصل كروانو شروع كري ححكا؛';
        }
        if ($data['ho_remarks_saheb'] != '') {
          $remarks .= $data['ho_remarks_saheb'];
        }
        if ($remarks) {
          ?>
          <div class="col-md-12 text-right alfatemi-text font18" dir="rtl"><strong>ملاحظة:</strong> <?php echo $remarks; ?></div>
          <?php
        }
        ?>
      </div>
      <div class="col-xs-1">
        <?php echo $track_no; ?>
        <div class="clearfix"></div>
        <span>Marhala: <?php echo $data['marhala']; ?></span>
      </div>
    </div>

    <?php
  }
  ?>

  <div class="col-xs-12">
    &nbsp;<br>
    <?php echo date('d, F Y'); ?>
    <h4 class="pull-right"><?php echo $user_full_name['full_name']; ?></h4>
    <div class="clearfix"></div>
    <a href="javascript:history.back()" class="btn btn-primary pull-right hidden-print">Go Back</a>
  </div>
</body>
</html>


