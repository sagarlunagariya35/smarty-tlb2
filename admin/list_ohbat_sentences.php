<?php
require_once '../classes/constants.php';
require_once '../classes/class.database.php';
require_once 'classes/class.user.admin.php';
require_once 'classes/class.ohbat_sentences.php';

$allowed_roles = array(ROLE_HEAD_OFFICE);
require_once 'session.php';

// Get list of articles
$art = new Mtx_Ohbat_Sentences();

$miqat_id = (isset($_GET['miqat_id'])) ? $_GET['miqat_id'] : 0;
$list_sentences = $art->get_all_articles_by_miqat_id($miqat_id);

$title = 'List of Sentences';
$description = '';
$keywords = '';
$active_page = "list_ohbat_sentences";

if (isset($_POST['delete_id'])) {
  $delete_id = $_POST['delete_id'];
  $result = $art->delete_article($delete_id);

  if ($result) {
    $_SESSION[SUCCESS_MESSAGE] = "Sentence Successfully Delete";
    $miqat = ($_GET['miqat_id']) ? "?miqat_id=" . $_GET['miqat_id'] : '';
    header('Location: list_ohbat_sentences.php'.$miqat);
    exit();
  } else {
    $_SESSION[ERROR_MESSAGE] = "Error while Delete";
  }
}

include_once("header.php");

?>
  <div class="row">
    <div class="col-lg-12 col-xs-12 col-sm-12  pull-left">
      <div class="col-md-12">&nbsp;</div>
      <div class="clearfix"></div>
      <div class="panel panel-green">
        <div class="panel-heading">
          <i class="fa fa-group fa-fw"></i> Manage Ohbat Sentences
          <span class="pull-right"><a href="ohbat_sentences_add.php" style="color: white;">Add New Ohbat Sentence</a></span>
        </div>
        <!-- /.panel-heading -->
        <div class="panel-body">
          <div class="table-responsive">
            <table class="table table-striped table-hover table-condensed">
              <thead>
                <tr>
                  <th>Sr</th>
                  <th>Sentence</th>
                  <th>Miqaat</th>
                  <th>Event Time</th>
                  <th>Action</th>
                </tr>
              </thead>
              <tbody>
                <?php
                if($list_sentences){
                  foreach ($list_sentences as $snt) {
                ?>
                    <tr>
                      <td><?php echo $snt['sr']; ?></td>
                      <td><?php echo $snt['title']; ?></td>
                      <td><?php echo $snt['miqaat_name']; ?></td>
                      <td><?php echo $snt['event_time']; ?></td>
                      <td>
                        <a href="edit_ohbat_sentence.php?id=<?php echo $snt['id']; ?>"><i class="fa fa-edit fa-fw"></i></a>
                      <a href="#" class="confirm" id="<?php echo $snt['id']; ?>"><i class="fa fa-remove fa-fw text-danger"></i></a>
                      </td>
                    </tr>
                    <?php
                  }
                }else {
                  echo '<tr><td class="text-center" colspan="4">No Records..</td></tr>';
                }
                ?>
              </tbody>
            </table>
          </div>
          <!-- /.table-responsive -->
        </div>
        <!-- /.panel-body -->
      </div>
    </div>
    <form method="post" id="frm-del-grp">
      <input type="hidden" name="delete_id" id="del-miqaat-val" value="">
    </form>
  </div><!-- ./ row -->
  <script>
    $(".confirm").confirm({
      text: "Are you sure you want to delete?",
      title: "Confirmation required",
      confirm: function(button) {
        $('#del-miqaat-val').val($(button).attr('id'));
        $('#frm-del-grp').submit();
        alert('Are you Sure You want to delete: ' + id);
      },
      cancel: function(button) {
        // nothing to do
      },
      confirmButton: "Yes",
      cancelButton: "No",
      post: true,
      confirmButtonClass: "btn-danger"
    });
  </script>
  
<?php include "./footer.php"; ?>
