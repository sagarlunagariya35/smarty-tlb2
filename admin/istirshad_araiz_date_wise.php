<?php
require_once 'classes/class.user.admin.php';
require_once '../classes/class.user.php';
include 'classes/class.araiz.php';
$allowed_roles = array(ROLE_HEAD_OFFICE, ROLE_AAMIL_SAHEB, ROLE_JAMIAT_COORDINATOR);
require_once 'session.php';

$user = new mtx_user_admin;

$araiz = new Araiz();

$title = 'Dashboard Istirshad Araiz';
$description = '';
$keywords = '';
$active_page = "search_istirshad_araiz";

$select = FALSE;
$from_date = $to_date = $pagination = FALSE;
$araz_saheb_id = $saheb_id = $araz_coun_id = $coun_id = $marhala = FALSE;
$counselor_days_past = $saheb_days_past = $jamiyet_data = FALSE;
$date = date('Y-m-d');

$jamiyet = unserialize(@$_SESSION['jamiat']);
if (count($jamiyet) == 1 && $jamiyet[0] != '') {
  $jamiyet_data = implode("','", $jamiyet[0]);
}

// Fix for ROLE_JAMIAT_COORDINATOR is no jamiat assign
if ($_SESSION[USER_ROLE] == ROLE_JAMIAT_COORDINATOR && $jamiyet_data === FALSE) {
  $jamiyet_data = ' ';
}

if(isset($_POST['print_list']))
{
  $_SESSION['track_id'] = (array) $_POST['jawab'];
  header('location: print_istirshad_araiz_date_wise.php');
}

if (isset($_POST['delete_id'])) {
  $delete_id = $_POST['delete_id'];
  $result = $araiz->delete_araz($delete_id);

  if ($result) {
    $_SESSION[SUCCESS_MESSAGE] = "Araz Successfully Delete";
  } else {
    $_SESSION[ERROR_MESSAGE] = "Error while Delete";
  }
}
$from_date = @$_REQUEST['from_date'];
$to_date = @$_REQUEST['to_date'];
$marhala = @$_REQUEST['marhala'];

include ('header.php');

if($from_date OR $to_date OR $marhala){
  $select = $araiz->get_pending_ho_istirshad_araiz($from_date, $to_date, $marhala, FALSE, $page, 10, $jamiyet_data);
  $total_araz = $araiz->get_pending_ho_istirshad_araz_count($from_date, $to_date, $marhala, FALSE, $jamiyet_data);
} else {
  $select = $araiz->get_pending_ho_istirshad_araiz(FALSE, FALSE, FALSE, FALSE, $page, 10, $jamiyet_data);
  $total_araz = $araiz->get_pending_ho_istirshad_araz_count(FALSE, FALSE, FALSE, FALSE, $jamiyet_data);
}

$counselors = $user->get_all_counselors();
$sahebs = $user->get_all_sahebs();

$araiz_parsed_data = $araiz->get_araiz_data_organised($select);

$counselor_and_saheb_content = COUNSELOR_AND_SAHEB_CONTENT;
$jawab_sent_content = JAWAB_SENT_CONTENT;
?>

<link rel="stylesheet" href="js/tablesorter/css/theme.bootstrap.css">
<script src="js/tablesorter/js/jquery.tablesorter.js"></script>
<script src="js/tablesorter/js/jquery.tablesorter.widgets.js"></script>
<style>
  .alfatemi-text{
    font-size: 18px;
  }
</style>
<div class="row">

  <div class="row">
    <div class="col-xs-12 col-sm-12">&nbsp;</div>
  </div>
  <div class="row">
    <div class="col-xs-12 col-sm-12">
      <h2 class="text-center">Total Pending Istirshad Araiz Count: <?php echo $total_araz; ?></h2>
    </div>
  </div>
  <div class="row">
    <div class="col-xs-12 col-sm-12">&nbsp;</div>
  </div>
  <form name="raza_form" class="form" method="post" action="">
    <div class="row">
      <div class="form-group col-xs-12 col-sm-1">
        <label for="from_date">From</label>
      </div>
      <div class="form-group col-xs-12 col-sm-3">
        <input type="date" name="from_date" class="form-control" value="<?php echo $from_date; ?>">
      </div>
      <div class="form-group col-xs-12 col-sm-1">
        <label for="to_date">To</label>
      </div>
      <div class="form-group col-xs-12 col-sm-3">
        <input type="date" name="to_date" class="form-control" value="<?php echo $to_date; ?>">
      </div>
      <div class="form-group col-sm-2 col-xs-12">
        <select name="marhala" class="form-control">
          <option value="">Select Marhala</option>
          <option value="1" <?php if($marhala == '1'){ echo 'selected'; } ?>>Pre-Primary</option>
          <option value="2" <?php if($marhala == '2'){ echo 'selected'; } ?>>Primary</option>
          <option value="3" <?php if($marhala == '3'){ echo 'selected'; } ?>>Std 5 to 7</option>
          <option value="4" <?php if($marhala == '4'){ echo 'selected'; } ?>>Std 8 to 10</option>
          <option value="5" <?php if($marhala == '5'){ echo 'selected'; } ?>>Std 11 to 12</option>
          <option value="6" <?php if($marhala == '6'){ echo 'selected'; } ?>>Graduation</option>
          <option value="7" <?php if($marhala == '7'){ echo 'selected'; } ?>>Post Graduation</option>
          <option value="8" <?php if($marhala == '8'){ echo 'selected'; } ?>>Diploma</option>
        </select>
      </div>
      <div class="col-xs-12 col-sm-2">
        <input type="submit" name="search" value="Search" class="btn btn-block btn-success">
      </div>
    </div>
    <div class="row">
      <div class="col-xs-12">&nbsp;</div>
    </div>
    <div class="row">
      <div class="col-xs-12 col-sm-3">
        <select name="counselor_id" class="form-control" id="counselor_id">
          <option value="">Select Counselor</option>
          <?php
          if ($counselors) {
            foreach ($counselors as $coun) {
              $pending_araz_count = $araiz->count_total_assign_araz_to_counselor($coun['user_id']);
              ?>
              <option value="<?php echo $coun['user_id'] ?>"><?php echo $coun['full_name'].' - '.$pending_araz_count; ?></option>
              <?php
            }
          }
          ?>
        </select>
      </div>
      <div class="col-xs-12 col-sm-3">
        <input type="submit" name="sent_counselor" id="sent_counselor" value="Assign Counselor" class="btn btn-success btn-block assign-counselor">
      </div>
      <div class="col-xs-12 col-sm-3">
        <select name="saheb_id" class="form-control" id="saheb_id">
          <option value="">Select Saheb</option>
          <?php
          if ($sahebs) {
            foreach ($sahebs as $s) {
              $pending_araz_count = $araiz->count_total_assign_araz_to_saheb(FALSE, FALSE, FALSE, $s['user_id']);
              ?>
              <option value="<?php echo $s['user_id'] ?>"><?php echo $s['full_name'].' - '.$pending_araz_count; ?></option>
              <?php
            }
          }
          ?>
        </select>
      </div>
      <div class="col-xs-12 col-sm-3">
        <input type="submit" name="sent_saheb" id="sent_saheb" value="Assign Saheb" class="btn btn-success btn-block assign-saheb">
      </div>
    </div>
    <div class="row">
      <div class="col-xs-12">&nbsp;</div>
    </div>
    <div class="row">
      <div class="col-xs-12 col-sm-3" style="padding-top:6px">
        <input type="checkbox" name="print" value="Print" id="check_all" onchange="checkAll(this);" class="css-checkbox1"><label for="check_all" class="css-label1">Check All</label>
      </div>
      <div class="col-xs-12 col-sm-3">
        <input type="submit" name="print_list" value="Print" class="btn btn-success btn-block assign-print">
      </div>
      <div class="col-xs-12 col-sm-3">
        <select name="jawab_city" class="form-control jawab_city" id="jawab_city">
          <option value="">Select City</option>
          <?php
          if ($jamaats) {
            foreach ($jamaats as $data) {
              ?>
              <option value="<?php echo $data['jamaat']; ?>"><?php echo $data['jamaat']; ?></option>
              <?php
            }
          }
          ?>
        </select>
      </div>
      <div class="col-xs-12 col-sm-3">
        <input type="submit" name="sent_jawab" id="sent_jawab" value="Send Jawab" class="btn btn-success btn-block assign-jawab-istirshad">
      </div>
    </div>
    
    <?php
    $counselor_araz = array();
      foreach ($araiz_parsed_data as $araz_id => $araz) {
        $counselor_details = $araz['counselor_details'];

        if ($counselor_details['counselor_id'] != 0 AND $counselor_details['counselor_sugest_timestamp'] > 0) {
          $counselor_araz[] = $araz_id;
        }
      }
      $count_counselor_araz = count($counselor_araz);
    ?>
    
    <div class="col-lg-12 col-xs-12 col-sm-12 pull-left">
      <div class="col-md-12">&nbsp;</div>
      <div class="clearfix"></div>
      <div class="row">
        <ul class="nav nav-tabs nav-justified" role="tablist">
          <li role="presentation" class="active"><a href="#new_araiz" aria-controls="pre" role="tab" data-toggle="tab">New Araiz</a></li>
          <li role="presentation"><a href="#araiz_from_counselor" aria-controls="in" role="tab" data-toggle="tab">Araiz from Counselor <?php if($count_counselor_araz > 0){ ?><span class="badge pull-right"><?php echo $count_counselor_araz; ?></span><?php } ?></a></li>
        </ul>
        <div class="row">
          <div class="col-xs-12 col-sm-12">&nbsp;</div>
        </div>
        <?php
          require_once 'pagination.php';
          $pagination = pagination(10, $page, 'istirshad_araiz_date_wise.php?from_date='.$from_date.'&to_date='.$to_date.'&marhala='.$marhala.'&page=', $total_araz);
        ?>
        <div class="tab-content">
          <div role="tabpanel" class="tab-pane active" id="new_araiz">
            <?php
            foreach ($araiz_parsed_data as $araz_id => $araz) {
              // Create bins for User Data, Araz Data, Previous Studies, Current Studeis, Remarks
              $user_details = $araz['user_data'];
              $araz_general_detail = $araz['araz_data'];
              $previous_study_details = $araz['previous_study_details'];
              $course_details = $araz['course_details'];
              $counselor_details = $araz['counselor_details'];

              if ($counselor_details['counselor_id'] == 0 && $araz_general_detail['saheb_id'] == 0 && $araz_general_detail['printed'] == 0) {
                include 'inc/inc.show_parsed_araiz_details.php';
              }
            }
            ?>
          </div>

          <div role="tabpanel" class="tab-pane" id="araiz_from_counselor">
            <?php
            foreach ($araiz_parsed_data as $araz_id => $araz) {
              // Create bins for User Data, Araz Data, Previous Studies, Current Studeis, Remarks
              $user_details = $araz['user_data'];
              $araz_general_detail = $araz['araz_data'];
              $previous_study_details = $araz['previous_study_details'];
              $course_details = $araz['course_details'];
              $counselor_details = $araz['counselor_details'];

              if ($counselor_details['counselor_id'] != 0 && $counselor_details['counselor_sugest_timestamp'] > 0) {
                include 'inc/inc.show_parsed_araiz_details.php';
              }
            }
            ?>
          </div>
        </div>
        <?php echo $pagination; ?>
      </div>
    </div>
  </form>
  <form method="post" id="frm-del-grp">
    <input type="hidden" name="delete_id" id="del-araz-val" value="">
  </form>
</div>
<style>
  .popover{
    color:#000;
  }
</style>

<?php
include 'footer.php';
?>
