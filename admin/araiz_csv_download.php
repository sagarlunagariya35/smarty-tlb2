<?php
require_once '../classes/class.database.php';
$allowed_roles = array(ROLE_HEAD_OFFICE);
require_once 'session.php';

    
$query = "SELECT `login_its` `ITS ID`, `user_full_name` `Full Name`, `gender` `Gender`, `jamaat` `Jamaat`, `jamiat` `Jamiyet`, `email` `email`, `mobile` `mobile`, `whatsapp` `whatsapp`, `current_course` `Current / Previous Course`, `current_inst_name` `Current / Previous Institute`, `current_inst_city` `City`, `accomodation` `Accomodation`, `institute_name` `Institute Name`, `institute_country` `Institute Country`, `institute_city` `Institute City`, `school_name` `School Name`, `school_city` `School City`, `school_standard` `School Standard`, `course_name` `Course Name`, `marhala` `Marhala`, `course_duration` `Course Duration`, `course_started` `Course Start Date`, `araz_type` `Araz Type` FROM txn_view_araz WHERE `jawab_given` = 1";
$result = $db->query_fetch_full_result($query);

if($result) {
  foreach ($result as $araiz) {
    foreach ($araiz as $key => $val) {
      $araiz_keys[] = $key;
    }
  }
  $araiz_keys = array_unique($araiz_keys);
 
  csv_download_araiz($result, 'araiz_data.csv', $araiz_keys);
}

function csv_download_araiz($ary_data, $filename, $araiz_keys) {
  // output headers so that the file is downloaded rather than displayed
  header('Content-Type: text/csv; charset=utf-8');
  header('Content-Disposition: attachment; filename=' . $filename);
  header("Pragma: no-cache");
  header("Expires: 0");
  // create a file pointer connected to the output stream
  $output = fopen('php://output', 'w');

  // output the column headings
  fputcsv($output, $araiz_keys);
  
  // fetch the data
  // loop over the rows, outputting them
  
  if($ary_data) {
    foreach ($ary_data as $data) {
      $araiz_data = array();
      foreach ($data as $value) {
        $araiz_data[] = $value;
      }
      fputcsv($output, $araiz_data);
    }
  }

  fclose($output);
  exit();
}

?>

