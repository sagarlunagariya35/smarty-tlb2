<?php
require_once 'classes/class.user.admin.php';
include 'classes/class.araiz.php';
$allowed_roles = array(ROLE_COUNSELOR);
require_once 'session.php';

$user = new mtx_user_admin();
$araiz = new Araiz();

$title = 'Student Araz Data';
$description = '';
$keywords = '';
$active_page = "counselor_suggestion";

include ('header.php');

$select = $pagination = FALSE;
$track_no = $its = FALSE;

if(isset($_GET['araz_id'])) {
  $araz_id = (int) $_GET['araz_id'];
  $select = $araiz->get_araz_data_by_araz_id($araz_id);
}

if(isset($_POST['send_suggestion']))
{
  $counselor_id = $_SESSION[USER_ID];
  $course = $_POST['course'];
  $country = $_POST['country'];
  $city = $_POST['city'];
  $remarks = $_POST['remarks'];
  
  $res = $araiz->insert_counselor_suggestions($araz_id,$counselor_id,$course,$country,$city,$remarks);
  if ($res){ 
    $_SESSION[SUCCESS_MESSAGE] = "Suggestion Sent successfully";
  }else { 
    $_SESSION[ERROR_MESSAGE] = "Error In Sending Suggestion..";
  }
}

$courses = $araiz->get_all_courses();
$countries = $araiz->get_all_countries_list();
?>

  <div class="row">
    <form method="post" action="">
    <div class="col-lg-12 col-xs-12 col-sm-12 pull-left">
      <div class="col-md-12">&nbsp;</div>
      <div class="clearfix"></div>
      <div class="panel panel-primary">
        <div class="panel-heading">
          <i class="fa fa-group fa-fw"></i> Details of <?php echo $select[0]['first_name'].' '.$select[0]['last_name']; ?>
        </div>
        <!-- /.panel-heading -->
        <div class="panel-body">
          <div class="table-responsive">
            <table class="table table-striped table-hover table-condensed">
              <thead>
                <th>Track No</th>
                <th>ITS</th>
                <th>Name</th>
                <th>Jamaat</th>
                <th>Araz Type</th>
                <th>City</th>
              </thead>
              <tbody>
              <tr>
                <td><?php echo $select[0]['araz_id']; ?></td>
                <td><?php echo $select[0]['login_its']; ?></td>
                <td><?php echo $select[0]['user_full_name']; ?></td>
                <td><?php echo $select[0]['jamaat']; ?></td>
                <td><?php echo ucfirst($select[0]['araz_type']); ?></td>
                <td><?php echo $select[0]['city']; ?></td>
              </tr>
              </tbody>
            </table>
            <br>
            
            <?php if($select[0]['school_name'] == ''){ ?>
            
            <table class="table table-striped table-hover table-condensed">
              <thead>
                <th>Degree / Course</th>
                <th>Institute Name</th>
                <th>Course Duration</th>
                <th>Institute City</th>
                <th>Accomodation</th>
              </thead>
              <tbody>
                <?php foreach ($select as $ac) { ?>
                
                <tr>
                  <td><?php echo $ac['course_name']; ?></td>
                  <td><?php echo $ac['institute_name']; ?></td>
                  <td><?php echo $ac['course_duration']; ?></td>
                  <td><?php echo $ac['institute_city']; ?></td>
                  <td><?php echo $ac['accomodation']; ?></td>
                </tr>

                <?php } ?>                   
              </tbody>
            </table>
            
            <?php 
              }else {
            ?>
            
            <table class="table table-striped table-hover table-condensed">
              <thead>
                <th>School Name</th>
                <th>Standard</th>
                <th>School City</th>
              </thead>
              <tbody>
                <tr>
                  <td><?php echo $select[0]['school_name']; ?></td>
                  <td><?php echo $select[0]['school_standard']; ?></td>
                  <td><?php echo $select[0]['school_city']; ?></td>
                </tr>
              </tbody>
            </table>
            
            <?php } ?>
            
            <div class="col-md-12">
              <div class="row">
                <h4>Give Your Suggestion :</h4>
                <div class="col-md-6">
                  <select name="course" class="form-control">
                    <option value="">Select Course</option>
                    <?php foreach ($courses as $crs) { ?>
                      <option value="<?php echo $crs['course_name']; ?>"><?php echo $crs['course_name']; ?></option>
                    <?php } ?>
                  </select>
                </div>
                <div class="col-md-3">
                  <select name="country" class="form-control" onChange="showcity(this.value)">
                    <option value="">Select Country</option>
                      <?php foreach ($countries as $cntry) { ?>
                        <option value="<?php echo $cntry['name']; ?>"><?php echo $cntry['name']; ?></option>
                      <?php } ?>
                  </select>
                </div>
                <div class="col-md-3" id="your_city">
                  <select name="city" class="form-control">
                    <option value="">Select City</option>
                  </select>
                </div>
              </div>
              <div class="row">&nbsp;</div>
              <div class="row">
                <div class="col-md-12">
                  <textarea name="remarks" class="form-control" placeholder="Enter Remarks"></textarea>
                </div>
              </div>
              <div class="row">&nbsp;</div>
              <div class="row">
                <div class="col-md-2 pull-right">
                  <input type="submit" name="send_suggestion" value="Send" class="btn btn-success btn-block">
                </div>
              </div>
            </div>
          </div>
          <!-- /.table-responsive -->
        </div>
        <!-- /.panel-body -->
      </div>
    </div>
    </form>
  </div>
<!-- /#page-wrapper -->
<script>
  function showcity(str)
  {
    if (str == "")
    {
      document.getElementById("your_city").innerHTML = "";
      document.getElementById("mname").innerHTML = "";
      return;
    }

    if (window.XMLHttpRequest)
    {// code for IE7+, Firefox, Chrome, Opera, Safari
      xmlhttp = new XMLHttpRequest();
    }
    else
    {// code for IE6, IE5
      xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
    }



    xmlhttp.onreadystatechange = function()
    {
      if (xmlhttp.readyState == 4 && xmlhttp.status == 200)
      {
        document.getElementById("your_city").innerHTML = xmlhttp.responseText;
      }
    }
    xmlhttp.open("GET", "get_data_by_ajax.php?country=" + str, true);
    xmlhttp.send();
  }
</script>
<!-- /#wrapper -->
<?php
include 'footer.php';
?>         