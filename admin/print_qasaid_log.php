<?php
require_once '../classes/class.database.php';
include 'classes/class.qasida.php';
$allowed_roles = array(ROLE_HEAD_OFFICE);
require_once 'session.php';

$qasaid = new Mtx_Qasaid();

$title = 'List of Qasaid Log';
$description = '';
$keywords = '';
$active_page = 'list_qasaid_log';

$select = FALSE;
$select = $qasaid->get_log_qasaid();
$total_qasaid = $qasaid->count_total_qasaid();
$total_qasaid = $total_qasaid * 3;

include ('print_header.php');
?>
<body style="padding: 10px;">
  <div>
    <p style="display: block; text-align: right"><?php echo date('d-m-Y H:i:s'); ?></p>
  </div>
  
  <div class="row">
    <div class="col-lg-12">
      <h3 class="page-header"><?php echo $title; ?></h3>
    </div>
    <!-- /.col-lg-12 -->
  </div>
  
  <div class="row">
    <div class="col-md-12">
      <table class="table table-responsive table-condensed">
        <thead>
          <tr>
            <th>Sr No.</th>
            <th>ITS</th>
            <th>Full Name</th>
            <th>Email</th>
            <th>Mobile</th>
            <th>Average Slider Value</th>
          </tr>
        </thead>
        <tbody>
          <?php 
          if($select){
            $i = 1; 
            foreach($select as $data){
              $slider_value = $data['sum'] / $total_qasaid;
          ?>
          <tr>
            <td><?php echo $i++;?></td>
            <td><?php echo $data['its_id']; ?></td>
            <td><?php echo $data['first_name']. ' '. $data['last_name']; ?></a></td>
            <td><?php echo $data['email']; ?></td>
            <td><?php echo $data['mobile']; ?></td>
            <td><?php echo number_format($slider_value, 2).'%'; ?></td>
          </tr>
          <?php 
              } 
            } else { ?>
          <tr>
            <td class="text-center" colspan="5">No Records..</td>
          </tr>
          <?php } ?>
        </tbody>
      </table>
    </div>
  </div>
</body>
</html>
