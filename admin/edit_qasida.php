<?php
require_once '../classes/class.database.php';
require_once '../classes/constants.php';
require_once 'classes/admin_gen_functions.php';
require_once 'classes/class.user.admin.php';
include 'classes/class.qasida.php';
$allowed_roles = array(ROLE_HEAD_OFFICE);
require_once 'session.php';

$title = 'Edit Qasida';
$description = '';
$keywords = '';
$active_page = "list_qasida";
$qsd = new Mtx_Qasaid();

if (isset($_POST['btn_submit'])) {
  $data = $db->clean_data($_POST);
  // get variables
  $title = $data['title'];
  $slug = $data['slug'];
  $alam = $data['alam'];
  $data_text = addslashes($_POST['data_text']);
  $active = $data['active'];
  $qsd_id = $data['qsd_id'];

  $qasida_insert = $qsd->edit_qasida($qsd_id, $title, $slug, $alam, $data_text, $active);

  if ($qasida_insert) {
    $_SESSION[SUCCESS_MESSAGE] = 'Qasida Updated Successfully.';
  } else {
    $_SESSION[ERROR_MESSAGE] = 'Error encountered while Inserting Qasida';
  }
}

if (isset($_GET['id'])) {
  $load_qsd = $qsd->get_qasida_by_id($_GET['id']);
}

include_once("header.php");
?>
<script type="text/javascript" src="ckeditor/ckeditor.js"></script>

<div class="row">
  <div class="col-lg-12">
    <h1 class="page-header">Update Qasida</h1>
  </div>
  <!-- /.col-lg-12 -->
</div>

<div class="row">
  <div class="col-md-12">
    <form method="post" enctype="multipart/form-data">

      <div class="form-group col-md-5 col-xs-12">
        <label for="title">Qasida Title :</label>
        <input type="text" name="title" id="title" value="<?php echo $load_qsd['title']; ?>" required="required" class="form-control" >
      </div>

      <div class="form-group col-md-5 col-xs-12">
        <label for="slug">Qasida slug :</label>
        <input type="text" name="slug" id="slug" value="<?php echo $load_qsd['slug']; ?>" required="required" class="form-control" >
      </div>

      <div class="form-group col-md-3 col-xs-12">
        <label for="active">Is Active? :</label>
        <select name="active" id="active" class="form-control" required >
          <option value="1" selected >Yes</option>
          <option value="0">No</option>
        </select>
      </div>
      
      <div class="form-group col-md-5 col-xs-12">
        <label for="slug">Qasida Alam :</label>
        <input type="text" name="alam" id="alam" value="<?php echo $load_qsd['alam']; ?>" required="required" class="form-control" >
      </div>

      <div class="form-group col-md-12" id="bayan">
        <label>Data : </label>
        <textarea name="data_text" class="form-control"><?php echo $load_qsd['text']; ?></textarea>
      </div><!----CONTENT_BOX-->


      <div class="col-md-4 col-xs-12">
      <input type="hidden" name="qsd_id" value="<?php echo $_GET['id']; ?>">
      <input type="submit" name="btn_submit" id="btn_submit" class="btn btn-primary btn-block" value="Submit">
      </div>
    </form>

  </div>
</div>

<!-- /#page-wrapper -->
<?php include "./footer.php"; ?>
