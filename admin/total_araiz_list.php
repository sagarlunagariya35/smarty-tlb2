<?php
require_once 'classes/class.user.admin.php';
include 'classes/class.araiz.php';
$allowed_roles = array(ROLE_AAMIL_SAHEB);
require_once 'session.php';

$user = new mtx_user_admin;
$araiz = new Araiz();

$title = 'Total Araiz List';
$description = '';
$keywords = '';
$active_page = "manage_araiz";

include ('header.php');

$select = $pagination = FALSE;

$jamat = $_SESSION[JAMAT_NAME];
$select = $araiz->get_all_araz($jamat);
$count = $araiz->get_all_araz_count($jamat);
?>

<!--link rel="stylesheet" href="js/tablesorter/css/theme.bootstrap.css">
<script src="js/tablesorter/js/jquery.tablesorter.js"></script>
<script src="js/tablesorter/js/jquery.tablesorter.widgets.js"></script-->

  <div class="row">
   <form method="post" action="" name="raza_form" class="form-horizontal" onsubmit="return(validate());">
    <div class="col-lg-12 col-xs-12 col-sm-12 pull-left">
      <div class="col-md-12">&nbsp;</div>
      <div class="clearfix"></div>
      <div class="panel panel-primary">
        <div class="panel-heading">
          <i class="fa fa-group fa-fw"></i> <?php echo $title; ?>
          <span class="pull-right">Total Araiz : <?php echo $count; ?></span>
        </div>
        <!-- /.panel-heading -->
        <div class="panel-body">
          <div class="table-responsive">
            <table class="table table-striped table-hover table-condensed">
              <thead>
                <th>No.</th>
                <th class="text-center" style="width: 50px;">Track No</th>
                <th>ITS</th>
                <th>Name</th>
                <th>Araz Type</th>
                <th>Status</th>
              </thead>
              <tbody>
                <?php
                $current_araz = null;
                if($select){
                  $i = 1;
                  
                  foreach ($select as $data) {
                    if($data['araz_id'] != $current_araz) {
                    $current_araz = $data['araz_id'];
                  ?>
                  <tr><td colspan="9"></td></tr>
                  <tr>
                    <td class="text-center"><?php echo $i++; ?></td>
                    <td class="text-center" style="color: blue;"><?php echo $data['araz_id']; ?></td>
                    <td><?php echo $data['login_its']; ?></td>
                    <td><?php echo $data['user_full_name']; ?></td>
                    <td><?php echo ucfirst($data['araz_type']); ?></td>
                    <td style="color: green;"><?php if($data['counselor_id'] != 0){ echo 'Pending at Counselor'; } else if($data['saheb_id'] != 0){ echo 'Pending at Saheb'; } ?></td>
                  </tr>
                  
                  <?php 
                    if($data['school_name'] == ''){
                      foreach ($select as $ac) {
                        if($ac['araz_id'] == $current_araz) {
                    ?>
                    <tr>
                      <td></td>
                      <td colspan='3'><strong>Degree / Course Name : </strong><?php echo $ac['course_name']; ?></td>
                      <td colspan='2'><strong>Institute Name : </strong><?php echo $ac['institute_name']; ?></td>
                    </tr>
                    <?php 
                        }
                      }
                    }else {
                  ?>
                  <tr>
                    <td></td>
                    <td colspan='3'><strong>School Name : </strong><?php echo $data['school_name']; ?></td>
                    <td colspan='2'><strong>School Standard : </strong><?php echo $data['school_standard']; ?></td>
                  </tr>
                  <?php
                        }
                      }
                    }
                  }
                  else
                  {
                    echo '<tr><td colspan="7" class="text-center">No Records</td></tr>';
                  }
                  ?>
                </tbody>
              </table>
          </div>
          <!-- /.table-responsive -->
        </div>
        <div class="panel-footer">
          <?php echo $pagination; ?>
          <div class="clearfix"></div>
        </div>
        <!-- /.panel-body -->
      </div>
    </div>
   </form>
  </div>
<?php
include 'footer.php';
?>
