<?php
include 'classes/class.crs_istifadah_master.php';
$allowed_roles = array(ROLE_HEAD_OFFICE);
require_once 'session.php';

$crs_istifadah = new Mtx_Crs_Istifadah_Master();

$title = 'Istifadah Master List';
$description = '';
$keywords = '';
$active_page = "list_istifadah_master";

$result = $id = FALSE;

if (isset($_REQUEST['id'])) {
  $id = $_REQUEST['id'];
  $result = $crs_istifadah->get_istifadah_master($id);
}

if(isset($_GET['cmd']) && $_GET['cmd'] == 'change_status') {
  
  $change_status = $crs_istifadah->change_istifadah_master_status($_GET['status'], $_GET['id']);
  
  if ($change_status) {
    $_SESSION[SUCCESS_MESSAGE] = 'Istifadah Master Status changed successfully.';
    header('Location: crs_istifadah_master.php');
    exit();
  } else {
    $_SESSION[ERROR_MESSAGE] = 'Error encountered while Changing Istifadah Master Status';
  }
}

if (isset($_POST['btn_update'])) {
  $data = $db->clean_data($_POST);
  // get variables
  $title = $data['title'];
  $slug = $db->clean_slug($_POST['slug']);
  $active = $data['active'];
  $sort = $data['sort'];
  $description = $data['description'];
  $user_id = $_SESSION[USER_ID];
  
  $last_insert_id = $crs_istifadah->get_last_insert_istifadah_master();
  $last_insert_id = $last_insert_id + 1;
  
  if($_FILES['img_icon']['name'] != ''){
    $icon_image = $_FILES['img_icon']['name'];
    $icon_image_temp = $_FILES['img_icon']['tmp_name'];
    
    $filearray = explode('.', $icon_image);
    $ext = $filearray[count($filearray)-1];
              
    $icon_image_name = $last_insert_id.'.'.$ext;
    $imagepathANDname = "../upload/courses/istifadah_master/" . $icon_image_name;
    move_uploaded_file($icon_image_temp, $imagepathANDname);
  }else {
    $icon_image_name = $data['last_img_icon'];
  }

  $update = $crs_istifadah->update_istifadah_master($title, $slug, $description, $icon_image_name, $sort, $active, $id);

  if ($update) {
    $_SESSION[SUCCESS_MESSAGE] = 'Istifadah Master Updated Successfully.';
    header('Location: crs_istifadah_master.php?id='.$id);
    exit();
  } else {
    $_SESSION[ERROR_MESSAGE] = 'Error encountered while Updating Istifadah Master';
  }
}

if (isset($_POST['btn_submit'])) {
  $data = $db->clean_data($_POST);
  // get variables
  
  $title = $data['title'];
  $slug = $db->clean_slug($_POST['slug']);
  $active = $data['active'];
  $sort = $data['sort'];
  $description = $data['description'];
  $user_id = $_SESSION[USER_ID];
  
  $last_insert_id = $crs_istifadah->get_last_insert_istifadah_master();
  $last_insert_id = $last_insert_id + 1;
  
  if($_FILES['img_icon']['name'] != ''){
    $icon_image = $_FILES['img_icon']['name'];
    $icon_image_temp = $_FILES['img_icon']['tmp_name'];
    
    $filearray = explode('.', $icon_image);
    $ext = $filearray[count($filearray)-1];
              
    $icon_image_name = $last_insert_id.'.'.$ext;
    $imagepathANDname = "../upload/courses/istifadah_master/" . $icon_image_name;
    move_uploaded_file($icon_image_temp, $imagepathANDname);
  }

  $insert = $crs_istifadah->insert_istifadah_master($title, $slug, $description, $icon_image_name, $sort, $user_id, $active);

  if ($insert) {
    $_SESSION[SUCCESS_MESSAGE] = 'Istifadah Master Inserted Successfully.';
  } else {
    $_SESSION[ERROR_MESSAGE] = 'Error encountered while Inserting Istifadah Master';
  }
}

$btn = ($id != FALSE) ? 'btn_update' : 'btn_submit';
$list_istifadah_master = $crs_istifadah->get_all_istifadah_master();

include_once("header.php");
?>
<link rel="stylesheet" href="js/select2/select2.min.css">
<script src="js/select2/select2.full.min.js"></script>
<script type="text/javascript" src="ckeditor/ckeditor.js"></script>

<div class="row">
  <div class="col-lg-12">
    <h3 class="page-header">Add New Istifadah Master</h3>
  </div>
  <!-- /.col-lg-12 -->
</div>

<div class="row">
  <div class="col-md-12">
    <form method="post" enctype="multipart/form-data">

      <div class="form-group col-md-4 col-xs-12">
        <label for="title">Title :</label>
        <input type="text" name="title" id="title" required class="form-control" value="<?php echo $result['title']; ?>">
      </div>
      
      <div class="form-group col-md-4 col-xs-12">
        <label for="slug">Slug :</label>
        <input type="text" name="slug" id="slug" required class="form-control" value="<?php echo $result['slug']; ?>">
      </div>

      <div class="form-group col-md-4 col-xs-12">
        <label for="slug">Image icon :</label>
        <input type="file" name="img_icon" class="file">
        <?php
          if ($result['img_icon'] != '') {
        ?>
          <div class="clearfix">&nbsp;</div>
          <p><img src="../upload/courses/istifadah_master/<?php echo $result['img_icon']; ?>" width="100" height="100" /></p>
          <input type="hidden" name="last_img_icon" value="<?php echo $result['img_icon']; ?>">
        <?php
          }
        ?>
          <div class="clearfix">&nbsp;</div>
      </div>

      <div class="form-group col-md-4 col-xs-12">
        <label for="active">Is Active? :</label>
        <select name="active" id="active" class="form-control">
          <option value="">Select</option>
          <option value="1" <?php echo ($result['is_active'] == '1') ? 'selected' : ''; ?>>Yes</option>
          <option value="0" <?php echo ($result['is_active'] == '0') ? 'selected' : ''; ?>>No</option>
        </select>
      </div>
      
      <div class="form-group col-md-4 col-xs-12">
        <label for="sort">Sort :</label>
        <input type="text" name="sort" id="sort" class="form-control" value="<?php echo $result['sort_id']; ?>">
      </div>
      
      <div class="form-group col-md-12 col-xs-12">
        <label for="miqat">Description :</label>
        <textarea name="description" class="form-control"><?php echo $result['description']; ?></textarea>
      </div>
      
      <div class="clearfix"></div>
      <div class="form-group col-md-2 col-xs-12">
        <input type="submit" name="<?php echo $btn; ?>" id="<?php echo $btn; ?>" value="Submit" class="btn btn-primary btn-block form-control">
      </div>
    </form>

  </div>
</div>

<br>
<div class="panel panel-primary">
  <div class="panel-heading">
    <i class="fa fa-group fa-fw"></i> <?php echo $title; ?>
  </div>
  <!-- /.panel-heading -->
  <div class="panel-body">
    <div class="table-responsive">
      <table class="table table-striped table-hover table-condensed">
        <thead>
          <tr>
            <th>Sr</th>
            <th>Title</th>
            <th>Description</th>
            <th>Date</th>
            <th>Status</th>
            <th>Action</th>
          </tr>
        </thead>
        <tbody>
          <?php
          if($list_istifadah_master){
            $sr = 0;
            foreach ($list_istifadah_master as $data) {
              $sr++;
          ?>
              <tr class="<?php if($data['is_active'] == '1') { echo 'success'; } else { echo 'danger'; } ?>">
                <td><?php echo $sr; ?></td>
                <td><?php echo $data['title']; ?></td>
                <td><?php echo $data['description']; ?></td>
                <td><?php echo date('d, F Y',  strtotime($data['created_ts'])); ?></td>
                <td><input type="checkbox" name="is_active[]" id="action<?php echo $data['id']; ?>" onclick="check(<?php echo $data['id']; ?>)" <?php echo ($data['is_active'] == 1) ? 'checked' : ''; ?>></td>
                <td><a href="crs_istifadah_master.php?id=<?php echo $data['id']; ?>"><i class="fa fa-pencil-square-o"></i></a> | <a href="#" id="<?php echo $data['id']; ?>"><i class="fa fa-remove"></i></a></td>
              </tr>
              <?php
            }
          }else {
            echo '<tr><td class="text-center" colspan="5">No Records..</td></tr>';
          }
          ?>
        </tbody>
      </table>
    </div>
    <!-- /.table-responsive -->
  </div>
  <!-- /.panel-body -->
</div>

<script type="text/javascript">
  function check(val){
    var doc = document.getElementById('action'+val).checked;

    if(doc == true) doc = 1;
    else doc = 0;
    window.location.href='crs_istifadah_master.php?id='+val+'&status='+doc+'&cmd=change_status';
  }
</script>
<script type="text/javascript" src="js/course_details.js"></script>

<!-- /#page-wrapper -->
<?php include "./footer.php"; ?>
