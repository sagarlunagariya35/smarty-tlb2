<?php
require_once 'classes/class.user.admin.php';
require_once '../classes/class.user.php';
include 'classes/class.araiz.php';
$allowed_roles = array(ROLE_REVIEWER,ROLE_ASHARAH_ADMIN);
require_once 'session.php';

$user = new mtx_user_admin;
$araiz = new Araiz();

$title = 'Pending Applications';
$description = '';
$keywords = '';
$active_page = "manage_reviewer_araz";

$select = $pagination = FALSE;
$from_date = $to_date = $pagination = FALSE;
$araz_saheb_id = $saheb_id = $araz_coun_id = $coun_id = FALSE;
$counselor_days_past = $saheb_days_past = FALSE;

if (isset($_POST['print_list'])) {
  $_SESSION['track_id'] = (array) $_POST['jawab'];
  header('location: print_raza_araiz_list_date_wise.php');
}

include ('header.php');

$select = $araiz->get_all_assign_araz_to_reviewer($page,5);
$total_araz = $araiz->count_total_assign_araz_to_reviewer();

$araiz_parsed_data = $araiz->get_araiz_data_organised($select);
?>

<link rel="stylesheet" href="js/tablesorter/css/theme.bootstrap.css">
<script src="js/tablesorter/js/jquery.tablesorter.js"></script>
<script src="js/tablesorter/js/jquery.tablesorter.widgets.js"></script>
<style>
  .alfatemi-text{
    font-size: 18px;
  }
</style>
<div class="row">

  <div class="row">
    <div class="col-xs-12 col-sm-12">&nbsp;</div>
  </div>
  <div class="row">
    <div class="col-xs-12 col-sm-12">
      <h2 class="text-center">Total Pending Applications: <?php echo $total_araz; ?><span class="pull-right"><?php echo 'Page: '.$page; ?></span></h2>
      
    </div>
  </div>
  <div class="row">
    <div class="col-xs-12 col-sm-12">&nbsp;</div>
  </div>
  <div class="col-lg-12 col-xs-12 col-sm-12 pull-left">
    <div class="col-md-12">&nbsp;</div>
    <div class="clearfix"></div>
    <div class="row">
      <?php
      require_once 'pagination.php';
      $pagination = pagination(5, $page, 'reviewer_araz_list.php?page=', $total_araz);
                  
      foreach ($araiz_parsed_data as $araz_id => $araz) {
        // Create bins for User Data, Araz Data, Previous Studies, Current Studeis, Remarks
        $user_details = $araz['user_data'];
        $araz_general_detail = $araz['araz_data'];
        $previous_study_details = $araz['previous_study_details'];
        $course_details = $araz['course_details'];
        $counselor_details = $araz['counselor_details'];

        include 'inc/inc.show_parsed_araiz_details_bchet.php';
      }
      ?>
    </div>
    <?php echo $pagination; ?>
    <div class="clearfix"></div>
  </div>
</div>
<style>
  .popover{
    color:#000;
  }
</style>
<script>
  $('.submitaraz').on("click", function () {
    if ($('.recommend_test').is(":checked"))
    { var recommend_test = $('.recommend_test').val(); }
    if ($('.recommend_coun').is(":checked"))
    { var recommend_coun = $('.recommend_coun').val(); }
    if ($('.recommend_qardan').is(":checked"))
    { var recommend_qardan = $('.recommend_qardan').val(); }
    if ($('.recommend_sponsorship').is(":checked"))
    { var recommend_sponsorship = $('.recommend_sponsorship').val(); }
    
    var araz_id = $(this).closest(".clssubmitbchet").find(".araz_id").val();
    var main_araz_div = $(this).closest("#araz_div");
    
    myApp.showPleaseWait();
    $.ajax({
      type: "POST",
      url: "ajax.php",
      data: 'cmd=make_entry_of_review&araz_id=' + araz_id + '&recommend_test=' + recommend_test + '&recommend_coun=' + recommend_coun + '&recommend_qardan=' + recommend_qardan + '&recommend_sponsorship=' + recommend_sponsorship,
      cache: false,
      success: function () {
        myApp.hidePleaseWait();
        main_araz_div.hide();
      }
    });
  });
</script>

<?php
include 'footer.php';
?>

