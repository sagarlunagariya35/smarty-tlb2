<?php
include 'classes/class.menus.php';
$allowed_roles = array(ROLE_HEAD_OFFICE);
require_once 'session.php';

$crs_menu = new Mtx_Menu();

$title = 'Menu List';
$description = '';
$keywords = '';
$active_page = "manage_menus";

$result = $id = FALSE;

if (isset($_REQUEST['id'])) {
  $id = $_REQUEST['id'];
  $result = $crs_menu->get_menu($id);
}

if (isset($_POST['delete_id'])) {
  $delete_id = $_POST['delete_id'];
  $result = $crs_menu->delete_menu($delete_id);

  if ($result) {
    $_SESSION[SUCCESS_MESSAGE] = "Menu Successfully Delete";
    header('Location: list_menus.php');
    exit();
  } else {
    $_SESSION[ERROR_MESSAGE] = "Error In Delete";
  }
}

if(isset($_GET['cmd']) && $_GET['cmd'] == 'change_status') {
  
  $change_status = $crs_menu->change_menu_status($_GET['status'], $_GET['id']);
  
  if ($change_status) {
    $_SESSION[SUCCESS_MESSAGE] = 'Menu Status changed successfully.';
    header('Location: list_menus.php');
    exit();
  } else {
    $_SESSION[ERROR_MESSAGE] = 'Error encountered while Changing Menu Status';
  }
}

if (isset($_POST['btn_update'])) {
  $data = $db->clean_data($_POST);
  // get variables
  $menu_id = $data['menu_id'];
  $title = $data['title'];
  $slug = $db->clean_slug($_POST['slug']);
  $active = $data['active'];
  $sort = $data['sort'];

  $update = $crs_menu->update_menu($title, $slug, $sort, $active, $id, $menu_id);

  if ($update) {
    $_SESSION[SUCCESS_MESSAGE] = 'Menu Updated Successfully.';
    header('Location: list_menus.php?id='.$id);
    exit();
  } else {
    $_SESSION[ERROR_MESSAGE] = 'Error encountered while Updating Menu';
  }
}

if (isset($_POST['btn_submit'])) {
  $data = $db->clean_data($_POST);
  // get variables
  
  $menu_id = $data['menu_id'];
  $title = $data['title'];
  $slug = $db->clean_slug($_POST['slug']);
  $active = $data['active'];
  $sort = $data['sort'];
  
  $insert = $crs_menu->insert_menu($title, $slug, $sort, $active, $menu_id);

  if ($insert) {
    $_SESSION[SUCCESS_MESSAGE] = 'Menu Inserted Successfully.';
  } else {
    $_SESSION[ERROR_MESSAGE] = 'Error encountered while Inserting Menu';
  }
}

$btn = ($id != FALSE) ? 'btn_update' : 'btn_submit';
$list_menus = $crs_menu->get_all_menus();

include_once("header.php");
?>
<link rel="stylesheet" href="js/select2/select2.min.css">
<script src="js/select2/select2.full.min.js"></script>
<script type="text/javascript" src="ckeditor/ckeditor.js"></script>

<div class="row">
  <div class="col-lg-12">
    <h3 class="page-header">Add New Menu</h3>
  </div>
  <!-- /.col-lg-12 -->
</div>

<div class="row">
  <div class="col-md-12">
    <form method="post" enctype="multipart/form-data">
      
      <div class="form-group col-md-4 col-xs-12">
        <label for="title">Menu :</label>
        <select name="menu_id" id="menu_id" class="form-control menu_id">
          <option value="">Select</option>
          <?php
          if ($list_menus) {
            foreach ($list_menus as $lt) {
              $selected = ($lt['id'] == $result['level']) ? 'selected' : '';
          ?>
            <option value="<?php echo $lt['id']; ?>" <?php echo $selected; ?>><?php echo $lt['name']; ?></option>
          <?php
            }
          }
          ?>
        </select>
      </div>

      <div class="form-group col-md-4 col-xs-12">
        <label for="title">Title :</label>
        <input type="text" name="title" id="title" required class="form-control" value="<?php echo $result['name']; ?>">
      </div>
      
      <div class="form-group col-md-4 col-xs-12">
        <label for="slug">Slug :</label>
        <input type="text" name="slug" id="slug" required class="form-control" value="<?php echo $result['slug']; ?>">
      </div>

      <div class="form-group col-md-4 col-xs-12">
        <label for="active">Is Active? :</label>
        <select name="active" id="active" class="form-control" required >
          <option value="">Select</option>
          <option value="1" <?php echo ($result['active'] == '1') ? 'selected' : ''; ?>>Yes</option>
          <option value="0" <?php echo ($result['active'] == '0') ? 'selected' : ''; ?>>No</option>
        </select>
      </div>
      
      <div class="form-group col-md-4 col-xs-12">
        <label for="sort">Sort :</label>
        <input type="text" name="sort" id="sort" required class="form-control" value="<?php echo $result['index']; ?>">
      </div>
      
      <div class="clearfix"></div>
      <div class="form-group col-md-2 col-xs-12">
        <input type="submit" name="<?php echo $btn; ?>" id="<?php echo $btn; ?>" value="Submit" class="btn btn-primary btn-block form-control">
      </div>
    </form>

  </div>
</div>

<br>
<div class="panel panel-primary">
  <div class="panel-heading">
    <i class="fa fa-group fa-fw"></i> <?php echo $title; ?>
  </div>
  <!-- /.panel-heading -->
  <div class="panel-body">
    <div class="table-responsive">
      <table class="table table-striped table-hover table-condensed">
        <thead>
          <tr>
            <th>Sr</th>
            <th>Title</th>
            <th>Slug</th>
            <th>Parent Menu</th>
            <th>Status</th>
            <th>Action</th>
          </tr>
        </thead>
        <tbody>
          <?php
          if($list_menus){
            $sr = 0;
            foreach ($list_menus as $data) {
              $sr++;
              $menu_name = $crs_menu->get_menu($data['level']);
          ?>
              <tr class="<?php if($data['active'] == '1') { echo 'success'; } else { echo 'danger'; } ?>">
                <td><?php echo $sr; ?></td>
                <td><?php echo $data['name']; ?></td>
                <td><?php echo $data['slug']; ?></td>
                <td><?php echo $menu_name['name']; ?></td>
                <td><input type="checkbox" name="is_active[]" id="action<?php echo $data['id']; ?>" onclick="check(<?php echo $data['id']; ?>)" <?php echo ($data['active'] == 1) ? 'checked' : ''; ?>></td>
                <td><a href="list_menus.php?id=<?php echo $data['id']; ?>"><i class="fa fa-pencil-square-o"></i></a> | <a href="#" class="confirm" id="<?php echo $data['id']; ?>"><i class="fa fa-remove"></i></a></td>
              </tr>
              <?php
            }
          }else {
            echo '<tr><td class="text-center" colspan="6">No Records..</td></tr>';
          }
          ?>
        </tbody>
      </table>
    </div>
    <!-- /.table-responsive -->
  </div>
  <!-- /.panel-body -->
  <form method="post" id="frm-del-grp">
    <input type="hidden" name="delete_id" id="del-menu-val" value="">
  </form>
</div>

<script type="text/javascript">
  function check(val){
    var doc = document.getElementById('action'+val).checked;

    if(doc == true) doc = 1;
    else doc = 0;
    window.location.href='list_menus.php?id='+val+'&status='+doc+'&cmd=change_status';
  }
  
  $(".confirm").confirm({
      text: "Are you sure you want to delete?",
      title: "Confirmation required",
      confirm: function(button) {
        $('#del-menu-val').val($(button).attr('id'));
        $('#frm-del-grp').submit();
        alert('Are you Sure You want to delete: ' + id);
      },
      cancel: function(button) {
        // nothing to do
      },
      confirmButton: "Yes",
      cancelButton: "No",
      post: true,
      confirmButtonClass: "btn-danger"
    });
</script>
<script type="text/javascript" src="js/course_details.js?v=1"></script>

<!-- /#page-wrapper -->
<?php include "./footer.php"; ?>
