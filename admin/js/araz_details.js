  $(document).ready(function(){
    $(".example").popover();
  });

  function checkAll(ele) {
    if (ele.checked) {
      $('.jawab').prop('checked', true);
    } else {
      $('.jawab').prop('checked', false);
    }
  }
  
  function checkAllRaza(ele) {
    if (ele.checked) {
      $('.istirshad').prop('checked', false);
      $('#check_all_istirshad').prop('checked', false);
      $('.raza').prop('checked', true);
    } else {
      $('.raza').prop('checked', false);
    }
  }
  
  function checkAllIstirshad(ele) {
    if (ele.checked) {
      $('.raza').prop('checked', false);
      $('#check_all_raza').prop('checked', false);
      $('.istirshad').prop('checked', true);
    } else {
      $('.istirshad').prop('checked', false);
    }
  }
  
  $('.jawab_city').selectize();
  
  $(".confirm").confirm({
    text: "Are you sure you want to delete?",
    title: "Confirmation required",
    confirm: function(button) {
      $('#del-araz-val').val($(button).attr('id'));
      $('#frm-del-grp').submit();
      alert('Are you Sure You want to delete: ' + id);
    },
    cancel: function(button) {
      // nothing to do
    },
    confirmButton: "Yes",
    cancelButton: "No",
    post: true,
    confirmButtonClass: "btn-danger"
  });

  $('.assign-counselor').on("click", function(){
    $('input:checkbox:checked.jawab').each(function(){
      var araz_id =  $(this).val();
      var coun_remark = $('#ho-remarks-' + araz_id).val();
      var coun_id = $('#counselor_id').val();

      myApp.showPleaseWait();
      $.ajax({
        type: "POST",
        url: "ajax.php",
        data: 'cmd=make_entry_of_counselor&araz_id=' + araz_id +'&counselor_id=' + coun_id +'&coun_remarks=' + coun_remark,
        cache: false,
        success: function () {
          myApp.hidePleaseWait();
        }
      });
    });
  });

  $('.assign-saheb').on("click", function(){
    $('input:checkbox:checked.jawab').each(function(){
      var araz_id =  $(this).val();
      var saheb_remark = $('#ho-remarks-' + araz_id).val();
      var saheb_id = $('#saheb_id').val();

      myApp.showPleaseWait();
      $.ajax({
        type: "POST",
        url: "ajax.php",
        data: 'cmd=make_entry_of_saheb&araz_id=' + araz_id +'&saheb_id=' + saheb_id +'&saheb_remarks=' + saheb_remark,
        cache: false,
        success: function () {
          myApp.hidePleaseWait();
        }
      });
    });
  });

  $('.assign-jawab').on("click", function(){
    $('input:checkbox:checked.jawab').each(function(){
      var araz_id =  $(this).val();
      var ho_remark = $('#ho-remarks-' + araz_id).val();
      var jawab_city = $('#jawab_city').val();
      
      var course_id = '';
      var radios = raza_form.elements['istirshad-course-' + araz_id];
      if (radios) {
        for (var i=0, len=radios.length; i<len; i++) {
          if ( radios[i].checked ) {
            course_id = radios[i].value;
            break;
          }
        }
      }

      if (jawab_city == '') {
        alert('Please Enter Jawab City\n');
        return false;
      }else {

        myApp.showPleaseWait();
        $.ajax({
          type: "POST",
          url: "ajax.php",
          data: 'cmd=make_entry_of_jawab&araz_id=' + araz_id +'&city=' + jawab_city +'&ho_remarks=' + ho_remark  +'&course_id=' + course_id,
          cache: false,
          success: function () {
            myApp.hidePleaseWait();
          }
        });
      }
    });
  });
  
  $('.assign-print').on("click", function(){
    $('input:checkbox:checked.jawab').each(function(){
      var araz_id =  $(this).val();
      var saheb_remark = $('#ho-remarks-' + araz_id).val();

      myApp.showPleaseWait();
      $.ajax({
        type: "POST",
        url: "ajax.php",
        data: 'cmd=make_entry_of_print&araz_id=' + araz_id +'&saheb_remarks=' + saheb_remark,
        cache: false,
        success: function () {
          myApp.hidePleaseWait();
        }
      });
    });
  });
  
  $('.assign-jawab-istirshad').on("click", function(){
    $('input:checkbox:checked.jawab').each(function(){
      var araz_id =  $(this).val();
      var ho_remark = $('#ho-remarks-' + araz_id).val();
      var jawab_city = $('#jawab_city').val();
      
      var course_id = '';
      var radios = raza_form.elements['istirshad-course-' + araz_id];
      for (var i=0, len=radios.length; i<len; i++) {
        if ( radios[i].checked ) {
          course_id = radios[i].value;
          break;
        }
      }
      
      if (jawab_city == '') {
        alert('Please Enter Jawab City\n');
        return false;
      }else {
        
        myApp.showPleaseWait();
        $.ajax({
          type: "POST",
          url: "ajax.php",
          data: 'cmd=make_entry_of_jawab&araz_id=' + araz_id +'&city=' + jawab_city +'&ho_remarks=' + ho_remark +'&course_id=' + course_id,
          cache: false,
          success: function () {
            myApp.hidePleaseWait();
          }
        });
        
      }
    });
  });

  $('.assign-counselor-araz').on("click", function(){
    var araz_id = $(this).closest(".for-assign-araz").find(".assign_araz_id").val();
    var coun_remark = $('#ho-remarks-' + araz_id).val();
    var coun_id = $('#counselor_id' + araz_id).val();

    myApp.showPleaseWait();
    $.ajax({
      type: "POST",
      url: "ajax.php",
      data: 'cmd=make_entry_of_counselor&araz_id=' + araz_id +'&counselor_id=' + coun_id +'&coun_remarks=' + coun_remark,
      cache: false,
      success: function () {
        myApp.hidePleaseWait();
      }
    });
  });

  $('.assign-saheb-araz').on("click", function(){
    var araz_id = $(this).closest(".for-assign-araz").find(".assign_araz_id").val();
    var saheb_remark = $('#ho-remarks-' + araz_id).val();
    var saheb_id = $('#saheb_id' + araz_id).val();

    myApp.showPleaseWait();
    $.ajax({
      type: "POST",
      url: "ajax.php",
      data: 'cmd=make_entry_of_saheb&araz_id=' + araz_id +'&saheb_id=' + saheb_id +'&saheb_remarks=' + saheb_remark,
      cache: false,
      success: function () {
        myApp.hidePleaseWait();
      }
    });
  });

  $('.assign-jawab-araz').on("click", function(e){
    e.preventDefault();
    var araz_id = $(this).closest(".for-assign-araz").find(".assign_araz_id").val();
    var ho_remark = $('#ho-remarks-' + araz_id).val();
    var jawab_city = $('#jawab_city' + araz_id).val();
    
    var course_id = '';
    var radios = raza_form.elements['istirshad-course-' + araz_id];
    if (radios) {
      for (var i=0, len=radios.length; i<len; i++) {
        if ( radios[i].checked ) {
          course_id = radios[i].value;
          break;
        }
      }
    }

    if (jawab_city == '') {
      alert('Please Enter Jawab City\n');
      return false;
    }else {

      myApp.showPleaseWait();
      $.ajax({
        type: "POST",
        url: "ajax.php",
        data: 'cmd=make_entry_of_jawab&araz_id=' + araz_id +'&city=' + jawab_city +'&ho_remarks=' + ho_remark +'&course_id=' + course_id,
        cache: false,
        success: function () {
          myApp.hidePleaseWait();
          $('.main-araz-' + araz_id).hide(600);
        }
      });
    }
  });
  
  $('.assign-bazid-jawab-araz').on("click", function(e){
    e.preventDefault();
    var araz_id = $(this).closest(".for-assign-araz").find(".assign_araz_id").val();
    var ho_remark = $('#ho-remarks-' + araz_id).val();
    var jawab_city = $('#jawab_city' + araz_id).val();
    var bazid_jawab = '1';
    
    var course_id = '';
    var radios = raza_form.elements['istirshad-course-' + araz_id];
    if (radios) {
      for (var i=0, len=radios.length; i<len; i++) {
        if ( radios[i].checked ) {
          course_id = radios[i].value;
          break;
        }
      }
    }

    if (jawab_city == '') {
      alert('Please Enter Jawab City\n');
      return false;
    }else {

      myApp.showPleaseWait();
      $.ajax({
        type: "POST",
        url: "ajax.php",
        data: 'cmd=make_entry_of_bazid_jawab&araz_id=' + araz_id + '&bazid_jawab=' + bazid_jawab + '&city=' + jawab_city + '&ho_remarks=' + ho_remark +'&course_id=' + course_id,
        cache: false,
        success: function () {
          myApp.hidePleaseWait();
          $('.main-araz-' + araz_id).hide(600);
        }
      });
    }
  });
  
  $('.assign-attend-later-araz').on("click", function(e){
    e.preventDefault();
    var araz_id = $(this).closest(".for-assign-araz").find(".assign_araz_id").val();
    var ho_remark = $('#ho-remarks-' + araz_id).val();

    myApp.showPleaseWait();
    $.ajax({
      type: "POST",
      url: "ajax.php",
      data: 'cmd=make_entry_of_attend_later&araz_id=' + araz_id + '&ho_remarks=' + ho_remark,
      cache: false,
      success: function () {
        myApp.hidePleaseWait();
        $('.main-araz-' + araz_id).hide(600);
      }
    });
  });

  $('.araz_chat').on("click", function(){
    var araz_id = $(this).closest(".clschat").find(".chat_araz_id").val();
    var message = $('#chat-msg-' + araz_id).val();

    myApp.showPleaseWait();
    $.ajax({
      type: "POST",
      url: "ajax.php",
      data: 'cmd=make_entry_of_chat&araz_id=' + araz_id + '&message=' + message,
      cache: false,
      success: function () {
        myApp.hidePleaseWait();
      }
    });
  });
  
  $('#whitelist-course').on("click", function(e){
    e.preventDefault();
    var course = $(this).closest(".whitelist-data").find(".whitelist-course").val();

    myApp.showPleaseWait();
    $.ajax({
      type: "POST",
      url: "ajax.php",
      data: 'cmd=make_entry_of_whitelist_course&course=' + course,
      cache: false,
      success: function () {
        myApp.hidePleaseWait();
      }
    });
  });
  
  $('#whitelist-city').on("click", function(e){
    e.preventDefault();
    var city = $(this).closest(".whitelist-data").find(".whitelist-city").val();

    myApp.showPleaseWait();
    $.ajax({
      type: "POST",
      url: "ajax.php",
      data: 'cmd=make_entry_of_whitelist_city&city=' + city,
      cache: false,
      success: function () {
        myApp.hidePleaseWait();
      }
    });
  });
  
  $('#whitelist-institute').on("click", function(e){
    e.preventDefault();
    var institute = $(this).closest(".whitelist-data").find(".whitelist-institute").val();

    myApp.showPleaseWait();
    $.ajax({
      type: "POST",
      url: "ajax.php",
      data: 'cmd=make_entry_of_whitelist_institute&institute=' + institute,
      cache: false,
      success: function () {
        myApp.hidePleaseWait();
      }
    });
  });
  
  $('.clscountry').on("change", function () {
    var strcountry = $(this).val();
    var city = $(this).closest(".clslocation").find(".clscity");
    city.empty();
    $.ajax({
       url: 'get_data_by_ajax.php',
       method: 'POST',
       data: 'cmd=get_cities&country=' + strcountry,
       success: function (data) {
         city.append('<option value="">Select City</option>');
         var objcities = jQuery.parseJSON(data);
         $.each(objcities, function() {
           city.append('<option value="' + this.city + '">' + this.city + '</option>');
         });
         ;
       }
    });    
  });
  
  $('.recommend-counselor').on("click", function(){
    $('input:checkbox:checked.jawab').each(function(){
      var araz_id =  $(this).val();
      //var coun_id = $('#counselor_id').val();
      var coun_id = 0;
    
      myApp.showPleaseWait();
      $.ajax({
        type: "POST",
        url: "ajax.php",
        data: 'cmd=make_entry_of_recommend_counselor&araz_id=' + araz_id +'&counselor_id=' + coun_id,
        cache: false,
        success: function () {
          myApp.hidePleaseWait();
        }
      });
    });
  });
  
  $('.accept-recommend').on("click", function(){
    $('input:checkbox:checked.jawab').each(function(){
      var araz_id =  $(this).val();
      var coun_id = $('#counselor_id').val();
      var remarks = $('#ho-remarks-' + araz_id).val();
    
      myApp.showPleaseWait();
      $.ajax({
        type: "POST",
        url: "ajax.php",
        data: 'cmd=make_entry_of_accept_recommendation&araz_id=' + araz_id + '&counselor_id=' + coun_id +'remarks=' + remarks,
        cache: false,
        success: function () {
          myApp.hidePleaseWait();
        }
      });
    });
  });
  
  $('.set_status').on("change", function () {
    var set_status = $(this).val(); 
    var araz_id = $(this).closest(".clsstatusbchet").find(".araz_id").val();
    
    myApp.showPleaseWait();
    $.ajax({
      type: "POST",
      url: "ajax.php",
      data: 'cmd=make_entry_of_bchet_status&araz_id=' + araz_id + '&set_status=' + set_status,
      cache: false,
      success: function () {
        myApp.hidePleaseWait();
      }
    });
  });

  var myApp;
  myApp = myApp || (function() {
    var pleaseWaitDiv = $('<div class="modal fade" id="pleaseWaitDialog" data-backdrop="static" data-keyboard="false"><div class="modal-dialog"><div class="modal-content"><div class="modal-header"><h4 class="modal-title">Processing . . .</h4></div><div class="modal-body"><img src="../images/loader.gif"></div></div></div></div>');
    return {
      showPleaseWait: function() {
        pleaseWaitDiv.modal();
      },
      hidePleaseWait: function() {
        pleaseWaitDiv.modal('hide');
      },
    };
  })();