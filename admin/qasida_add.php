<?php
include 'classes/class.qasida.php';
$allowed_roles = array(ROLE_HEAD_OFFICE);
require_once 'session.php';

$title = 'Upload Qasida';
$description = '';
$keywords = '';
$active_page = "list_qasida";
$qsd = new Mtx_Qasaid();

if (isset($_POST['btn_submit'])) {
  $data = $db->clean_data($_POST);
  // get variables
  $title = $data['title'];
  $slug = $data['slug'];
  $alam = $data['alam'];
  $data_text = addslashes($_POST['data_text']);
  $active = $data['active'];

  $qasida_insert = $qsd->insert_qasida($title, $slug, $alam, $data_text, $active);

  if ($qasida_insert) {
    $_SESSION[SUCCESS_MESSAGE] = 'Qasida Inserted Successfully.';
  } else {
    $_SESSION[ERROR_MESSAGE] = 'Error encountered while Inserting Qasida';
  }
}

include_once("header.php");
?>
<script type="text/javascript" src="ckeditor/ckeditor.js"></script>

<div class="row">
  <div class="col-lg-12">
    <h1 class="page-header"><?php echo $_SESSION[USER_ID]; ?>Add New Qasida <?php //echo $_SESSION['user_its'];       ?></h1>
  </div>
  <!-- /.col-lg-12 -->
</div>

<div class="row">
  <div class="col-md-12">
    <form method="post" enctype="multipart/form-data">

      <div class="form-group col-md-5 col-xs-12">
        <label for="title">Qasida Title :</label>
        <input type="text" name="title" id="title" required="required" class="form-control" >
      </div>

      <div class="form-group col-md-5 col-xs-12">
        <label for="slug">Qasida slug :</label>
        <input type="text" name="slug" id="slug" required="required" class="form-control" >
      </div>

      <div class="form-group col-md-3 col-xs-12">
        <label for="active">Is Active? :</label>
        <select name="active" id="active" class="form-control" required >
          <option value="1" selected >Yes</option>
          <option value="0">No</option>
        </select>
      </div>
      
      <div class="form-group col-md-5 col-xs-12">
        <label for="slug">Qasida Alam :</label>
        <input type="text" name="alam" id="alam" required="required" class="form-control">
      </div>

      <div class="form-group col-md-12" id="bayan">
        <label>Data : </label>
        <textarea name="data_text" class="form-control"></textarea>
      </div><!----CONTENT_BOX-->

      <div class="clearfix"></div>
      <p>&nbsp;</p>
      <div class="form-group col-md-6 col-xs-12">
        <input type="submit" name="btn_submit" id="btn_submit" value="Submit" class="btn btn-primary btn-block form-control">
      </div>
    </form>

  </div>
</div>

<!-- /#page-wrapper -->
<?php include "./footer.php"; ?>
