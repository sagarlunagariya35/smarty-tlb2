<?php
require_once '../classes/constants.php';
require_once '../classes/class.database.php';
require_once 'classes/admin_gen_functions.php';
require_once 'classes/class.user.admin.php';
require_once 'classes/class.edu_commitee.php';

$allowed_roles = array(ROLE_AAMIL_SAHEB);

require_once 'session.php';
$ret = FALSE;
$mem_designation = FALSE;

// Check if a ITS id is submitted
if (isset($_POST['cmd']) && $_POST['cmd'] == 'verify') {
  $its = $_POST['its'];

  $user = new mtx_user_admin();

  $ret = $user->get_mumeen_detail_from_its($its);
  if ($ret) {
    $mem_its = $user->get_its_id();
    $mem_name = $user->get_arb_name();
    $mem_img = $user->get_mumin_photo($its);
    $mem_mobile = $user->get_mobile();
    $mem_qualification = $user->get_qualification();
    $mem_email = $user->get_email();
    $mem_occupation = $user->get_occupation();
    $mem_jamat = $user->get_jamaat();
  } else {
    $_SESSION[ERROR_MESSAGE] = $its . ' is not a valid ITS ID';
  }
}

if (isset($_POST['cmd']) && $_POST['cmd'] == 'add') {
  if ($_POST['valid']) {
    // if the ITS id is not already in database then add / update or else throw error
    $cls_edu_comm = new Edu_committee($_SESSION[TANZEEM_ID]);

    $its_exists_in_db = $cls_edu_comm->check_member_already_registered($_POST['mem_its']);

    if ($its_exists_in_db) {
      $cls_edu_comm->delete_member_by_its($_POST['mem_its']);
    }

    (isset($_POST['is_co_ordinator'])) ? $is_co = 1 : $is_co = 0;
    (isset($_POST['is_mamur_co_ordinator'])) ? $is_mamur_co = 1 : $is_mamur_co = 0;
    $_SESSION['tmp_comm_mem'][$_POST['mem_its']] = array('its' => $_POST['mem_its'], 'name' => $_POST['mem_name'], 'designation' => $_POST['designation'], 'is_co_ordinator' => $is_co, 'is_mamur_co_ordinator' => $is_mamur_co, 'email' => $_POST['email'], 'mobile' => $_POST['mobile'], 'qualification' => $_POST['qualification'], 'occupation' => $_POST['occupation'], 'jamaat' => $_POST['mem_jamat']);
  }
}

if (isset($_POST['cmd']) && $_POST['cmd'] == 'Delete') {
  if (isset($_POST['del_its'])) {
    $cls_edu_comm = new Edu_committee($_SESSION[TANZEEM_ID]);

    $cls_edu_comm->delete_member_by_its($_POST['mem_its']);

    unset($_SESSION['tmp_comm_mem'][$_POST['del_its']]);
  }
}

if (isset($_POST['cmd']) && $_POST['cmd'] == 'Update') {
  if (isset($_POST['updt_its'])) {
    $its = $_POST['updt_its'];


    $user = new mtx_user_admin();

    $ret = $user->get_mumeen_detail_from_its($its);
    if ($ret) {
      $mem_its = $user->get_its_id();
      $mem_name = $user->get_arb_name();
      $mem_img = $user->get_mumin_photo($its);
      $mem_designation = $user->get_designation();
      $mem_mobile = $user->get_mobile();
      $mem_qualification = $user->get_qualification();
      $mem_email = $user->get_email();
      $mem_occupation = $user->get_occupation();
      $mem_jamat = $user->get_jamaat();
    }
  }
}

// Check if Confirmed data. And if yes insert into the database
if (isset($_POST['cmd']) && $_POST['cmd'] == 'confirm') {

//  if (!isset($_SESSION[TANZEEM_ID])) {
//    $_SESSION[TANZEEM_ID] = 'Test_Committee';
//  }

  $edu_comm = new Edu_committee($_SESSION[TANZEEM_ID]);

  $edu_mem_from_db = $edu_comm->members_get_list();
  $edu_members = array_merge($edu_mem_from_db, $_SESSION['tmp_comm_mem']);

  $is_set_hub_cordinator = FALSE;
  $is_set_secratory = FALSE;

  foreach ($edu_members as $mem) {
    //check if coordinator is set
    if ($mem['is_co_ordinator'] == 1)
      $is_set_hub_cordinator = TRUE;
    if ($mem['designation'] == 3)
      $is_set_secratory = TRUE;
  }

  $ary_error = array();
  if (!$is_set_hub_cordinator)
    $ary_error[] = ' * You need to specify a Hub Coordinator';


  if (!$is_set_secratory)
    $ary_error[] = ' * You need to specify a Secratory';

  if (count($ary_error) > 0) {
    $error_message = implode("<br>", $ary_error);
    $error_message = 'Following Erors Encountered<br>' . $error_message;
    $_SESSION[ERROR_MESSAGE] = $error_message;
  } else {
    $edu_comm->save_members_araz($_SESSION['tmp_comm_mem']);

    $_SESSION[SUCCESS_MESSAGE] = "Your Araz has been submitted successfully";
    $its = $_GET['its'];
    $edu_comm->member_reset($its);

    unset($_SESSION['tmp_comm_mem']);
  }
}

//check in database and fetchdata. 
$is_downloaded = FALSE;
$is_verified = FALSE;
$is_rejected = FALSE;
$tr_class = '';
$not_approved = FALSE;
$misal_uploaded = FALSE;

$edu_comm = new Edu_committee($_SESSION[TANZEEM_ID]);
$edu_mem_from_db = $edu_comm->members_get_list($_SESSION[TANZEEM_ID]);

if (!$edu_mem_from_db) {
  $edu_mem_from_db = array();
}
foreach ($edu_mem_from_db as $mem) {
  if ($mem['downloaded']) {
    $is_downloaded = TRUE;
    $tr_class = 'bg-info';
  }
  if ($mem['ho_approved']) {
    $is_verified = TRUE;
    $tr_class = 'bg-success';
  }
  if ($mem['ho_rejected']) {
    $is_rejected = TRUE;
    $tr_class = 'bg-danger';
  }
  if ($mem['misaal']) {
    $misal_uploaded = TRUE;
    $tr_class = 'bg-primary';
  }
  if (isset($mem['ho_approved']) && $mem['ho_approved'] == NULL) {
    $not_approved++;
  }
}
if (!isset($_SESSION['tmp_comm_mem'])) {
  $_SESSION['tmp_comm_mem'] = array();
}
// if the database and session both has data then merge them both
$edu_members = array_merge($edu_mem_from_db, $_SESSION['tmp_comm_mem']);

//if downloaded 
// // only display table and message = "Downloaded, Your data is under process"
// if not downloaded 
//  can edit
// 
//if rejected 
// have to edit 

$title = 'Aamil Saheb back end Module';
$description = '';
$keywords = '';
$active_page = "create_edu_comm";
include_once("header.php");
?>

<div class="row">
  <div class="col-lg-12">
    <h4 class="page-header">Araz for Mahad al Hasanaat al Burhaniyyah (Education) committee members (<?php echo $_SESSION[JAMAT_NAME]; ?>)</h4>
  </div>
  <!-- /.col-lg-12 -->
</div>
<!-- /.row -->

<?php if ($edu_comm->get_jamat_name() == $edu_comm->get_hub_name()) { ?>

  <div class="row">
    <div class="col-lg-3 col-md-6">
      <div class="panel panel-primary">
        <div class="panel-heading">
          <div class="row">
            <div class="col-xs-3">
              <i class="fa fa-sitemap fa-5x"></i>
            </div>
            <div class="col-xs-9 text-right">
            </div>
          </div>
        </div>
        <a href="#">
          <div class="panel-footer">
            <span class="pull-left">You are a HUB</span>
            <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
            <div class="clearfix"></div>
          </div>
        </a>
      </div>
    </div>
    <div class="col-lg-3 col-md-6">
      <div class="panel panel-green">
        <div class="panel-heading">
          <div class="row">
            <div class="col-xs-3">
              <i class="fa fa-building fa-5x"></i>
            </div>
            <div class="col-xs-9 text-right">
            </div>
          </div>
        </div>
        <a href="#myModal" data-toggle="modal" data-target="#myModal">
          <div class="panel-footer">
            <span class="pull-left">Cities under you</span>
            <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
            <div class="clearfix"></div>
          </div>
        </a>
      </div>
    </div>
    <div class="col-lg-3 col-md-6">
      <div class="panel panel-yellow">
        <div class="panel-heading">
          <div class="row">
            <div class="col-xs-3">
              <i class="fa fa-group fa-5x"></i>
            </div>
            <div class="col-xs-9 text-right">
            </div>
          </div>
        </div>
        <a href="#myModal1" data-toggle="modal" data-target="#myModal1">
          <div class="panel-footer">
            <span class="pull-left">Mamureen under you</span>
            <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
            <div class="clearfix"></div>
          </div>
        </a>
      </div>
    </div>
    <div class="col-lg-3 col-md-6">
      <div class="panel panel-red">
        <div class="panel-heading">
          <div class="row text-center">
            <a href="#myModal3" data-toggle="modal" data-target="#myModal3"><img src="maps/<?php echo $edu_comm->get_hub_name() ?>.jpg" class="img-thumbnail"></a>
          </div>
        </div>
        <a href="#myModal3" data-toggle="modal" data-target="#myModal3">
          <div class="panel-footer">
            <span class="pull-left">Click for map of your Hub</span>
            <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
            <div class="clearfix"></div>
          </div>
        </a>
      </div>
    </div>
  </div>
  <?php } else { ?>
    <p><strong>Your Hub is <?php echo $edu_comm->get_hub_name() ?></strong></p>
  <?php } ?>



  <?php if (!$is_downloaded) { ?>
    <!-- Form Create Committee -->
    <div class="row">
      <div class="col-md-12">
        <form class="form-inline" method="post">
          <label>ITS ID:</label>
          <input type="text" class="form-control" name="its" onKeyUp="numericFilter(this);" required maxlength="8" />
          <input type="submit" name="submit" value="Add Member" class="bnt btn-info" />
          <input type="hidden" name="cmd" value="verify">
        </form>
      </div><!-- ./ md-12 -->
    </div><!-- ./ row -->
    <!-- ./Form Create Committee -->
  <?php } ?>

  <div class="row">
    <div class="col-md-12">
      &nbsp;
    </div>
  </div>

  <?php
  if (!$is_downloaded) {
    if ($ret) {
      ?>
      <!-- Show the ITS detail -->
      <?php if (!$is_downloaded) { ?>
        <form action="" method="post">
          <div class="row">
            <div class="col-md-10">
              <div class="col-md-2">
                <label class="control-label">ITS</label>
                <p class="form-control-static"><?php echo($ret) ? $mem_its : ''; ?></p>
              </div>
              <div class="col-md-4">
                <label class="control-label">Name</label>
                <p class="form-control-static lsd"><?php echo ($ret) ? $mem_name : ''; ?></p>
              </div>
              <?php echo ($ret) ? $mem_designation : ''; ?>
              <div class="col-md-3">
                <label class="control-label">Designation</label>
                <select name="designation" class="form-control" required="required">
                  <option value="">-- Select Designation --</option>
                  <option value="3">Secretory</option>
                  <option value="4">Joint Secretory</option>
                  <option value="5">Treasurer</option>
                  <option value="6">Counselor</option>
                  <option value="7">Member</option>
                </select>
              </div>
              <div class="col-md-3">
                <label class="control-label">Select Hub Coordinator</label>
                <div class="checkbox">
                  <label>
                    <input type="checkbox" name="is_co_ordinator"> Hub Coordinator
                  </label>
                </div>

                <div class="checkbox">
                  <label>
                    <input type="checkbox" name="is_mamur_co_ordinator"> Mamur Coordinator
                  </label>
                </div>
              </div>
              <div class="clearfix"></div>
              <div class="col-md-3">
                <label class="control-label">email</label>
                <input type="text" name="email" value="<?php echo ($ret) ? $mem_email : ''; ?>">
              </div>
              <div class="col-md-3">
                <label class="control-label">Mobile</label>
                <input type="text" name="mobile" value="<?php echo ($ret) ? $mem_mobile : ''; ?>">
              </div>
              <div class="col-md-3">
                <label class="control-label">Qualification</label>
                <input type="text" name="qualification" value="<?php echo ($ret) ? $mem_qualification : ''; ?>">
              </div>
              <div class="col-md-3">
                <label class="control-label">Occupation</label>
                <input type="text" name="occupation" value="<?php echo ($ret) ? $mem_occupation : ''; ?>">
              </div>
            </div>
            <div class="col-md-2">
              <img src="<?php echo ($ret) ? $mem_img : ''; ?>" class="img-responsive">
            </div>
            <div class="col-md-3">
              <input type="submit" value="Confirm this Member" name="submit" class="btn btn-primary btn-lg btn-block">
              <input type="hidden" name="cmd" value="add">
              <input type="hidden" name="valid" value="<?php echo $ret ?>">
              <input type="hidden" name="mem_name" value="<?php echo ($ret) ? $mem_name : ''; ?>">
              <input type="hidden" name="mem_jamat" value="<?php echo ($ret) ? $mem_jamat : ''; ?>">
              <input type="hidden" name="mem_its" value="<?php echo ($ret) ? $mem_its : ''; ?>">
            </div>
          </div>
        </form>
        <!-- ./ Show the ITS detail -->

        <?php
      }
    }
  } //end if of isdownloaded
// Show the committed committee members list
  if (isset($edu_members)) {

    //$misal_uploaded = file_exists("misaal/" . $edu_comm->get_jamat_name() . ".pdf");
    ?>
    <div class="row">
      <div class="col-md-12">
        &nbsp;
      </div>
    </div>

    <div class="row">
      <div class="col-md-12 alert alert-info" role="alert">
        <?php echo $edu_comm->members_get_message($is_downloaded, $is_verified, $is_rejected, $misal_uploaded); ?>
      </div>

    </div>

    <div class="row">
      <div class="col-lg-12">
        <p>Text for submit preview page</p>
        <p>Text for submit preview page</p>
        <p>Text for submit preview page</p>
        <p>Text for submit preview page</p>
        <p>Text for submit preview page</p>
        <p>Text for submit preview page</p>
        <p>Text for submit preview page</p>
      </div>
    </div>

    <div class="row">
      <div class="col-md-12">
        <table class="table table-responsive table-condensed">
          <thead>
            <tr>
              <th>Sr</th>
              <th>Name</th>
              <th>Designation</th>
              <th>City</th>
              <th>Mobile</th>
              <th>Status</th>
              <th>Action</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td>1</td>
              <td class="lsd">العامل الحاضر</td>
              <td>President</td>
              <td><?php echo $edu_comm->get_jamat_name() ?></td>
              <td colspan="3"></td>
            </tr>
            <tr>
              <td>2</td>
              <td class="lsd" colspan="2">لجنة العمال والمعلمين والمراقبين</td>
              <td><?php echo $edu_comm->get_jamat_name() ?></td>
              <td colspan="3"></td>
            </tr>
            <?php
            $i = 2;

            foreach ($edu_members as $mem) {
              $i++;

              $user = new mtx_user_admin();
              $user->get_mumeen_detail_from_its($mem['its']);
              //$mem_img = $cls_user->get_mumin_photo($mem['its']);

              if ($mem['is_co_ordinator'] == 1) {
                $co_ord = ', Hub Coordinator';
              } else {
                $co_ord = '';
              }

              if ($mem['is_mamur_co_ordinator'] == 1) {
                $mamur_co_ord = ', Mamur Coordinator';
              } else {
                $mamur_co_ord = '';
              }
              ?>
              <tr class="<?php echo $tr_class; ?>">
                <td rowspan="2"><?php echo $i; ?></td>
                <td class="lsd"><?php echo $mem['name']; ?></td>
                <td><?php
                  echo $edu_comm->designation_by_id_to_string($mem['designation']);
                  echo $co_ord;
                  echo $mamur_co_ord;
                  ?></td>
                <td><?php echo $mem['jamaat']; ?></td>
                <td><?php echo $mem['mobile']; ?></td>
                <td class='text-center' colspan='1' rowspan='2'><?php echo $edu_comm->members_get_status($mem['misaal'], $mem['downloaded'], $mem['ho_rejected'], $mem['ho_approved']); ?></td>
                <td class='text-center' colspan='1' rowspan='2'>

                  <?php if (!$is_downloaded && !$is_rejected && !$is_verified) { ?>
                    <form action="#" method="post">
                      <input type="submit" name="cmd" value="Delete" class="btn btn-sm btn-link">
                      <input type="hidden" name="del_its"  value="<?php echo $mem['its']; ?>"> |
                      <input type="submit" name="cmd" value="Update" class="btn btn-sm btn-link">
                      <input type="hidden" name="updt_its"  value="<?php echo $mem['its']; ?>">
                    </form>
                  <?php } ?>
                </td>
              </tr>
              <tr class="<?php echo $tr_class; ?>">
                <td><strong>Qualification: </strong><?php echo $mem['qualification']; ?></td>
                <td colspan="2"><strong>Occupation: </strong><?php echo $mem['occupation']; ?></td>
                <td><?php echo $mem['email']; ?></td>
              </tr>
              <?php
            }
            ?>
          </tbody>
        </table>
        <?php if (!$is_downloaded) { ?>
          <form class="form-inline" method="post" action="#">
            <input type="submit" name="submcmit" value="Confirm" class="btn btn-success btn-lg pull-right">
            <input type="hidden" name="cmd" value="confirm">
          </form>
        <?php } ?> 
      </div>
    </div>
    <?php
  }
  ?>

  <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title" id="myModalLabel">Cities under you</h4>
        </div>
        <div class="modal-body" style="overflow-y: scroll; max-height:70%;">
          <?php
          $i = 0;
          $list_cities = $edu_comm->get_list_of_cities_under_hub($edu_comm->get_hub_name());
          foreach ($list_cities as $city) {
            $i++;
            ?>
            <p><?php echo $i . " - " . $city['jamat_name']; ?></p>
            <?php
          }
          ?>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
  </div>

  <div class="modal fade" id="myModal1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title" id="myModalLabel">Mamureen under you</h4>
        </div>
        <div class="modal-body" style="overflow-y: scroll; max-height:70%;">
          <?php
          $i = 0;
          $list_mamureen = $edu_comm->get_list_of_mamureen_under_hub($edu_comm->get_hub_name());
          foreach ($list_mamureen as $mamureen) {
            $i++;
            ?>
            <p><?php echo $i . " - " . $mamureen['mamureen']; ?></p>
            <?php
          }
          ?>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
  </div>

  <div class="modal fade" id="myModal3" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title" id="myModalLabel">Click for map of your Hub</h4>
        </div>
        <div class="modal-body" style="overflow-y: scroll; max-height:70%;">
          <img src="maps/<?php echo $edu_comm->get_hub_name() ?>.jpg" class="img-responsive">
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
  </div>



  <script>
    $('#myModal').on('shown.bs.modal', function() {
      $('#myInput').focus()
    })

    $('#myModal1').on('shown.bs.modal', function() {
      $('#myInput').focus()
    })

    $('#myModal3').on('shown.bs.modal', function() {
      $('#myInput').focus()
    })

    $("a[rel^='prettyPhoto']").prettyPhoto();

    $('.tooltip').tooltip({
      selector: "[data-toggle=tooltip]",
      container: "body"
    })

    function numericFilter(txb) {
      txb.value = txb.value.replace(/[^\0-9]/ig, "");
    }
  </script>
  <!-- /#page-wrapper -->
  <?php include "./footer.php"; ?>
