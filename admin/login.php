<?php
session_start();
session_destroy();
session_start();

require_once 'classes/class.user.admin.php';
require_once 'classes/admin_gen_functions.php';

if (isset($_POST['btnLogin'])) {
  $data = $db->clean_data($_POST);
  $user_its = $data['admin_user'];
  $pass = $data['password'];

  $user = new mtx_user_admin;

  $user_exists = $user->log_in($user_its, $pass);
  if ($user_exists['success']) {
    
    // if user type is not set then redirect user to select his primary role
    $_SESSION[IS_ADMIN] = TRUE;
    $_SESSION[USER_LOGGED_IN] = TRUE;
    $isAuthorized['success'] = TRUE;
    $_SESSION[USER_ROLE] = $user->get_user_type();
    $_SESSION[USER_ITS] = $user->get_its_id();
    $_SESSION[USER_ID] = $user->get_user_id();
    $_SESSION[USER_TYPE] = $user->get_user_type();
    $_SESSION['jamiat'] = $user->get_jamiat();
    
    
    if($_SESSION[USER_ROLE] == ROLE_SAHEB){
      header('Location:' . SERVER_PATH . 'admin/saheb_araz_list.php');
      exit();
    }elseif($_SESSION[USER_ROLE] == ROLE_COUNSELOR) {
      header('Location:' . SERVER_PATH . 'admin/counselor_araz_list.php');
      exit();
    }elseif($_SESSION[USER_ROLE] == ROLE_HEAD_OFFICE){
      header('Location:' . SERVER_PATH . 'admin/home.php');
      exit();
    }elseif($_SESSION[USER_ROLE] == ROLE_REVIEWER){
      header('Location:' . SERVER_PATH . 'admin/reviewer_araz_list.php');
      exit();
    }elseif($_SESSION[USER_ROLE] == ROLE_PROJECT_COORDINATOR){
      header('Location:' . SERVER_PATH . 'admin/coordinator_araz_list.php');
      exit();
    }elseif($_SESSION[USER_ROLE] == ROLE_ADMIN_MARHALA_1 || $_SESSION[USER_ROLE] == ROLE_ADMIN_MARHALA_2 || $_SESSION[USER_ROLE] == ROLE_ADMIN_MARHALA_3 || $_SESSION[USER_ROLE] == ROLE_ADMIN_MARHALA_4 || $_SESSION[USER_ROLE] == ROLE_ADMIN_MARHALA_5 || $_SESSION[USER_ROLE] == ROLE_ADMIN_MARHALA_6 || $_SESSION[USER_ROLE] == ROLE_ADMIN_MARHALA_7 || $_SESSION[USER_ROLE] == ROLE_ADMIN_MARHALA_8){
      header('Location:' . SERVER_PATH . 'admin/marhala_araz_list.php');
      exit();
    }elseif($_SESSION[USER_ROLE] == ROLE_ASHARAH_ADMIN){
      header('Location:' . SERVER_PATH . 'admin/home.php');
      exit();
    }elseif($_SESSION[USER_ROLE] == ROLE_DATA_ENTRY){
      header('Location:' . SERVER_PATH . 'admin/queries.php');
      exit();
    }elseif($_SESSION[USER_ROLE] == ROLE_JAMIAT_COORDINATOR){
      header('Location:' . SERVER_PATH . 'admin/raza_araiz_already_started.php');
      exit();
    }
  } 

  //cheating for testing purposes
  $isAuthorized['success'] = FALSE;
  if ($user_its == 'test_ho' && $pass == 'admin') {
    $_SESSION[IS_ADMIN] = TRUE;
    $_SESSION[USER_LOGGED_IN] = TRUE;
    $isAuthorized['success'] = TRUE;
    $_SESSION[USER_ROLE] = ROLE_HEAD_OFFICE;
    $_SESSION[USER_ITS] = $user_its;
    $_SESSION[USER_ID] = $user->get_user_id();
    $_SESSION[USER_TYPE] = $user->get_user_type();
    header('Location:' . SERVER_PATH . 'admin/home.php');
    exit();
  }

  if ($user_its == 'test_as' && $pass == 'admin') {
    $_SESSION[IS_ADMIN] = TRUE;
    $_SESSION[USER_LOGGED_IN] = TRUE;
    $isAuthorized['success'] = TRUE;
    $_SESSION[USER_ROLE] = ROLE_AAMIL_SAHEB;
    $_SESSION[USER_ITS] = $user_its;
    $_SESSION[TANZEEM_ID] = $user->get_tanzeem_id();
  }

  if ($isAuthorized['success']) {
    $_SESSION[IS_ADMIN] = TRUE;
    $_SESSION[USER_LOGGED_IN] = TRUE;
    $_SESSION[USER_ID] = $user->get_user_id();
    $_SESSION[USER_ITS] = $user_its;

    header('Location:' . SERVER_PATH . 'admin/index.php');
    exit();
  } else {
    $error = $_SESSION['error'] = 'Error: Authorization Failed.';
  }
}
?>
<!DOCTYPE html>
<html lang="en">

  <head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Talabul-ilm</title>
    <style>

    </style>
    <!-- Bootstrap Core CSS -->
    <link href="<?php echo SERVER_PATH; ?>admin/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="<?php echo SERVER_PATH; ?>admin/css/sb-admin-2.css" rel="stylesheet">

    <link href="<?php echo SERVER_PATH; ?>admin/css/custom.css" rel="stylesheet">
    <!-- Custom Fonts -->
    <link href="<?php echo SERVER_PATH; ?>admin/font-awesome-4.1.0/css/font-awesome.min.css" rel="stylesheet" type="text/css">

    <style>
      body {
        background: url('<?php echo SERVER_PATH; ?>assets/img/login.jpg');
        background-repeat: no-repeat;
        background-size: cover;
      }
    </style>
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    <style>
      .error_msg {
        padding: 5px;
      }
    </style>
  </head>

  <body>
    <div class="container">

      <div class="col-md-4 col-sm-6 col-xs-10 col-md-offset-4 col-sm-offset-3 col-xs-offset-1 login_form">

        <div class="text-center">
          <image class="login_main" src="<?php echo SERVER_PATH; ?>assets/img/pen_logo.png" />
          <div class="text-center col-md-12">
            <h1 style="color: white;text-shadow: 0px 2px 2px rgba(50, 50, 50, 0.7);" class="font-white">Talab ul ilm</h1>
          </div>
        </div>

        <?php
        if (isset($_SESSION['error']) && $_SESSION['error'] != '') {
          ?>
          <div class="col-md-12 alert alert-danger">
            <button type="button" class="close" data-dismiss="alert">&times;</button>
            <?php
            echo $_SESSION['error'];
            unset($_SESSION['error']);
            ?>
          </div>
          <?php
        }
        ?>

        <form method="post" action="<?php echo SERVER_PATH; ?>admin/login.php">

          <div class="form-group">
            <label style="color: white;text-shadow: 0px 2px 2px rgba(50, 50, 50, 0.7);" class="font_cl">Admin user: </label>
            <input class="form-control input_type font-white" type="text" name="admin_user" id="admin" required maxlength="32">
          </div>

          <div class="form-group">
            <label style="color: white;text-shadow: 0px 2px 2px rgba(50, 50, 50, 0.7);" class="font_cl">Admin Password: </label>
            <input class="form-control input_type font-white" type="password" name="password" required="">
          </div>

          <div class="form-group pull-right">
            <button class="btn btn-success" name="btnLogin"> Login</button>
          </div>

        </form>
      </div>

    </div>

    <!-- jQuery Version 1.11.0 -->
    <script src="<?php echo SERVER_PATH; ?>admin/js/jquery-1.11.0.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="<?php echo SERVER_PATH; ?>admin/js/bootstrap.min.js"></script>

</html>
