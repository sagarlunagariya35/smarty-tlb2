<?php
require_once 'classes/class.user.admin.php';
require_once '../classes/class.user.php';
include 'classes/class.araiz.php';
require_once 'classes/hijri_cal.php';
$allowed_roles = array(ROLE_HEAD_OFFICE, ROLE_AAMIL_SAHEB, ROLE_JAMIAT_COORDINATOR);
require_once 'session.php';

$user = new mtx_user_admin;
$araiz = new Araiz();
$hijari = new HijriCalendar();

$title = 'Araz Preview';
$description = '';
$keywords = '';
$active_page = "araz_preview";

$select = FALSE;
$from_date = $to_date = $pagination = FALSE;
$araz_saheb_id = $saheb_id = $araz_coun_id = $coun_id = FALSE;
$counselor_days_past = $saheb_days_past = $jamiyet_data = FALSE;

$jamiyet = unserialize($_SESSION['jamiat']);
if (count($jamiyet) == 1 && $jamiyet[0] != '') {
  $jamiyet_data = implode("','", $jamiyet[0]);
}

// Fix for ROLE_JAMIAT_COORDINATOR is no jamiat assign
if ($_SESSION[USER_ROLE] == ROLE_JAMIAT_COORDINATOR && $jamiyet_data === FALSE) {
  $jamiyet_data = ' ';
}

if(isset($_POST['sent_email']))
{
  $araz_id = (array) $_POST['jawab'];
  foreach ($araz_id as $a_id){
    $email_sent = $araiz->araz_send_email_to_student($a_id);
    if($email_sent){
      $_SESSION[SUCCESS_MESSAGE] = 'Email Send Successfully..';
    } else {
      $_SESSION[ERROR_MESSAGE] = 'Error Encounter While Sending E-mail..';
    }
  }
}

if(isset($_POST['clear_araz']))
{
  $araz_id = (array) $_POST['jawab'];
  foreach ($araz_id as $a_id){
    $clear_araz = $araiz->clear_araz_data($a_id);
    if($clear_araz){
      $_SESSION[SUCCESS_MESSAGE] = 'Clear Araz Successfully..';
    } else {
      $_SESSION[ERROR_MESSAGE] = 'Error Encounter While Clear Araz..';
    }
  }
}

if(isset($_POST['skip']))
{
  $araz_id = (array)$_POST['jawab'];
  $araz_id = max($araz_id);
  $select = $araiz->get_araz_preview($araz_id, $jamiyet_data);
}else {
  $select = $araiz->get_araz_preview(FALSE, $jamiyet_data);
}

$total_araz = $araiz->get_total_araz_preview($jamiyet_data);
$araiz_parsed_data = $araiz->get_araiz_data_organised($select);

$quote_saheb = 'انسس جيم الداعي الاجل المقدس سيدنا محمد برهان الدين ر ض يھ ارشاد فرمايو  ؛ كھ';
$quote = ' تعليم صحيح اسلامي منطلق سي هوئي، ايمان ني منزل  اهني منزل هوئي،  اْل محمد ني طاعة اهنو مقصد هوئي، بقاء انسس  نجاة اهنو ثمرة هوئي.';

include ('header.php');
?>

<link rel="stylesheet" href="js/tablesorter/css/theme.bootstrap.css">
<script src="js/tablesorter/js/jquery.tablesorter.js"></script>
<script src="js/tablesorter/js/jquery.tablesorter.widgets.js"></script>
<style>
  .alfatemi-text{
    font-size: 18px;
  }
</style>
<div class="row">
  <form name="raza_form" class="form" method="post" action="">
    <div class="row">
      <div class="col-xs-12 col-sm-12">&nbsp;</div>
    </div>
    <div class="row">
      <div class="col-xs-12 col-sm-12">
        <h2 class="text-center">Total Araz Preview Count: <?php echo $total_araz; ?></h2>
      </div>
    </div>
    <div class="row">
      <div class="col-xs-12 col-sm-12">&nbsp;</div>
    </div>
    <div class="row">
      <div class="col-xs-12 col-sm-3">
        <input type="checkbox" name="print" value="Print" id="check_all" onchange="checkAll(this);" class="css-checkbox1"><label for="check_all" class="css-label1">Check All</label>
      </div>
      <div class="col-xs-12 col-sm-2 pull-right">
        <input type="submit" name="skip" value="Skip" class="btn btn-danger btn-block">
      </div>
      <div class="col-xs-12 col-sm-2 pull-right">
        <input type="submit" name="clear_araz" value="Clear Araz" class="btn btn-primary btn-block">
      </div>
      <div class="col-xs-12 col-sm-2 pull-right">
        <input type="submit" name="sent_email" id="sent_email" value="Send Email" class="btn btn-success btn-block">
      </div>
    </div>
    <div class="col-lg-12 col-xs-12 col-sm-12 pull-left">
      <div class="col-md-12">&nbsp;</div>
      <div class="clearfix"></div>
      <div class="row">
        <?php
        foreach ($araiz_parsed_data as $araz_id => $araz) {
          // Create bins for User Data, Araz Data, Previous Studies, Current Studeis, Remarks
          $user_details = $araz['user_data'];
          $araz_general_detail = $araz['araz_data'];
          $previous_study_details = $araz['previous_study_details'];
          $course_details = $araz['course_details'];
          $counselor_details = $araz['counselor_details'];

          $marhala_id = $araz_general_detail['marhala'];
          $araz_std = $araz_general_detail['school_standard'];
          $ary_std = $araiz->get_standards_by_marhala($marhala_id);

          if ($ary_std) {
            $remove_std = TRUE;
            foreach ($ary_std as $key => $std) {
              ($araz_std == $std) ? $remove_std = FALSE : '';
              if($remove_std) {
                unset($ary_std[$key]);
              }
            }
          }
          $std_text = join(', ', $ary_std);

          switch ($marhala_id) {
            case 1:
              $marhala_text = 'پظظلا مرحلة Pre-Primary';
              break;
            case 2:
              $marhala_text = "بيجا مرحلة Primary ($std_text standard/grade)";
              break;
            case 3:
              $marhala_text = "تيجامرحلة ($std_text standard/grade)";
              break;
            case 4:
              $marhala_text = "ححوتهامرحلة ($std_text standard/grade)";
              break;
            case 5:
              $marhala_text = "ثثانححمامرحلة Under graduation ($std_text standard/grade)";
              break;
            case 6:
              $marhala_text = "ححهتا مرحلة Graduation";
              break;
            case 7:
              $marhala_text = " ساتمامرحلة Post-Graduation";
              break;
            case 8:
              $marhala_text = "Diploma";
              break;
          }

          $araz_courses = $araiz->get_all_araz_course_data($araz_id, 'jawab_given');
          if ($araz_courses) {
            $araz_years = $araz_courses[0]['course_started'];
            $araz_city = $araz_courses[0]['institute_city'];
            $inst_name = $araz_courses[0]['institute_name'];
            $course = $araz_courses[0]['course_name'];
            $degree = $araz_courses[0]['degree_name'];
          } else {
            $araz_years = '';
            $araz_city = $araz_general_detail['school_city'];
            $inst_name = $araz_general_detail['school_name'];
            $course = $araz_general_detail['school_standard'];
          }

          $jawab_city = $user_details['jawab_city'];
          $ITS_city = $user_details['jamaat'];
          $student_name_ar = $user_details['full_name_ar'];
          $ITS = $user_details['login_its'];
          $ho_remark = $araz_general_detail['ho_remarks_araz'];
          $bazid_jawab = $user_details['bazid_jawab'];

          $hijari_date = $hijari->GregorianToHijri(strtotime($user_details['jawab_ts']));
          $mon = $hijari->monthName($hijari_date[0]);
          $yr = $hijari_date[2];
          $dt = $hijari_date[1];

          $arb_date = $dt . ', ' . $mon . ', ' . $yr . ' H';
          $eng_date = date('d, F Y', strtotime($user_details['jawab_ts']));

          $count_araz_courses = $araiz->get_all_araz_course_data($araz_id);
          ?>
          <div class="panel panel-default">
            <div class="panel-heading" style="font-size: 19px;">
              <div class="row">
                <div class="col-xs-12 col-sm-4">
                  <input type="checkbox" id="jawab-<?php echo $araz_id; ?>" name="jawab[]" class="jawab css-checkbox1 <?php echo $user_details['araz_type']; ?>" value="<?php echo $araz_id; ?>">
                  <label for="jawab-<?php echo $araz_id; ?>" class="css-label1">Track No: </strong><?php echo $araz_id; ?></label>
                </div>
                <div class="clearfix"></div>
              </div>
              <?php
              if (count($count_araz_courses) > 1) {
                if ($user_details['gender'] == 'M') {
                  $salutation = 'الاخ النجيب';
                  $dua = 'حفظه الله تعالى وسلمه';
                  include 'istirshad_araiz.php';
                } else {
                  $salutation = 'الاخت النجيبة';
                  $dua = 'حفظها الله تعالى وسلمها';
                  include 'istirshad_araiz.php';
                }
              } else {
                if ($user_details['gender'] == 'M') {
                  $salutation = 'الاخ النجيب';
                  $dua = 'حفظه الله تعالى وسلمه';
                  include 'raza_araiz.php';
                } else {
                  $salutation = 'الاخت النجيبة';
                  $dua = 'حفظها الله تعالى وسلمها';
                  include 'raza_araiz.php';
                }
              }
              ?>
              </div>
            </div>
          <hr>
          <?php
        }
        ?>
      </div>
      <input type="hidden" name="araz_id" value="<?php echo $araz_id; ?>">
    </div>
  </form>
</div>
<style>
  .avoidBreak {
    page-break-inside: avoid !important;
    margin: 4px 0 4px 0;  /* to keep the page break from cutting too close to the text in the div */
  }
  div{
    page-break-inside: avoid;
  }
  .alfatemi-text{
    min-height: 20px;
    margin: 0;
    text-transform: none;
  }
  .container{
    width : 1000px;
  }
</style>
<script>
  $(document).ready(function () {
    var ctlTd = $('.dontSplit td');
    if (ctlTd.length > 0)
    {
      //console.log('Found ctlTd');
      ctlTd.wrapInner('<div class="avoidBreak" />');
    }
  });
  
  function checkAll(ele) {
    if (ele.checked) {
      $('.jawab').prop('checked', true);
    } else {
      $('.jawab').prop('checked', false);
    }
  }
</script>
<?php
include 'footer.php';
?>
