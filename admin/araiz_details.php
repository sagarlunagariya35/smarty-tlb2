<?php
require_once 'classes/class.user.admin.php';
include 'classes/class.araiz.php';
$allowed_roles = array(ROLE_HEAD_OFFICE,ROLE_AAMIL_SAHEB);
require_once 'session.php';

$user = new mtx_user_admin;
$araiz = new Araiz();

$id = $select = $res1 = $res2 = FALSE;

if(isset($_GET['id'])) {
  $id = $_GET['id'];
  $select = $araiz->get_single_araz_and_user_data($id);
}

if(isset($_POST['sent_jawab']))
{
  $araz_id = $_POST['araz_id'];
  $course_id = @$_POST['jawab'];
  $jawab_city = $_POST['jawab_city'];
  $date = date('Y-m-d');
  
  if($course_id){
    $res = $araiz->araz_send_jawab($araz_id,$jawab_city,$date,$course_id);
    
    if($res)
    { $_SESSION[SUCCESS_MESSAGE] = "Jawab Sent successfully"; }
    else
    { $_SESSION[ERROR_MESSAGE] = "Error In Jawab Sending.."; }
    
  }else {
    
    $app_counselor = array_filter($_POST['counselor']);
    $app_saheb = array_filter($_POST['saheb']);

    if($app_counselor){
      foreach($app_counselor as $app_c)
      {
        $coun_data = explode('-', $app_c);
        $araz_coun_id = $coun_data[0];
        $coun_id = $coun_data[1];

        $res1 = $araiz->araz_assign_counselor_and_saheb($araz_coun_id,$date,$coun_id);
      }
    }
    
    if($app_saheb){
      foreach($app_saheb as $app_s)
      {
        $saheb_data = explode('-', $app_s);
        $araz_saheb_id = $saheb_data[0];
        $saheb_id = $saheb_data[1];

        $res2 = $araiz->araz_assign_counselor_and_saheb($araz_saheb_id,$date,FALSE,$saheb_id);
      }
    }
    
    if($res1 || $res2)
    { $_SESSION[SUCCESS_MESSAGE] = "Jawab Sent successfully"; }
    else
    { $_SESSION[ERROR_MESSAGE] = "Error In Jawab Sending.."; }
  }
}

$title = 'Araiz List';
$description = '';
$keywords = '';
$active_page = "manage_araiz";

$counselors = $user->get_all_counselors();
$sahebs = $user->get_all_sahebs();

include ('header.php');
?>

  <div class="row">
    <form method="post" action="" name="raza_form" class="form-horizontal" onsubmit="return(validate());">
    <div class="col-lg-12 col-xs-12 col-sm-12 pull-left">
      <div class="col-md-12">&nbsp;</div>
      <div class="clearfix"></div>
      <div class="panel panel-primary">
        <div class="panel-heading">
          <i class="fa fa-group fa-fw"></i> Araz Details of <?php echo $select[0]['first_name'].' '.$select[0]['last_name']; ?>
        </div>
        <!-- /.panel-heading -->
        <div class="panel-body">
          <div class="table-responsive">
            <table class="table table-striped table-hover table-condensed">
              <thead>
                <th>Track No</th>
                <th>ITS</th>
                <th>Name</th>
                <th>Jamaat</th>
                <th>Araz Type</th>
                <th>City</th>
              </thead>
              <tbody>
                <?php
                    $araz_courses = $araiz->get_all_araz_course_data($select[0]['id']);
                    if(count($araz_courses) > 1)
                    { $araz_type = 'Istirshad'; } else { $araz_type = 'Raza'; }
                    $count_jawab_send_araz = $araiz->count_jawab_send_araz_course_data($select[0]['id']);
                  ?>

                <tr>
                  <td><?php echo $select[0]['id']; ?></td>
                  <td><?php echo $select[0]['login_its']; ?></td>
                  <td><?php echo $select[0]['first_name'].' '.$select[0]['last_name']; ?></td>
                  <td><?php echo $select[0]['jamaat']; ?></td>
                  <td><?php echo $araz_type; ?></td>
                  <td><?php echo $select[0]['city']; ?></td>
                </tr>
                
              </tbody>
            </table>
            <br>
            
            <?php if($araz_courses){ ?>
            
            <table class="table table-striped table-hover table-condensed">
              <thead>
                <th>Degree / Course</th>
                <th>Institute Name</th>
                <th>Course Duration</th>
                <th>Institute City</th>
                <th>Accomodation</th>
                <th class="text-center">Jawab</th>
              </thead>
              <tbody>
                <?php foreach ($araz_courses as $ac) { ?>
                
                <tr class="<?php if($ac['jawab_given'] == '1'){ echo 'success'; } ?>">
                  <td><?php echo $ac['course_name']; ?></td>
                  <td><?php echo $ac['institute_name']; ?></td>
                  <td><?php echo $ac['course_duration']; ?></td>
                  <td><?php echo $ac['institute_city']; ?></td>
                  <td><?php echo $ac['accomodation']; ?></td>
                  <td class="text-center"><?php if($count_jawab_send_araz < '1'){ ?><input type="radio" name="jawab" id="jawab" value="<?php echo $ac['id']; ?>" <?php if($ac['jawab_given'] == '1'){ echo 'checked'; } ?>><?php }else if($ac['jawab_given'] == '1') { echo 'Yes'; } ?></td>
                </tr>

                <?php } ?>                   
              </tbody>
            </table>
            
            <?php if($count_jawab_send_araz < '1'){ ?>
            <div class="col-md-12">
              <div class="col-md-2 pull-right">
                <input type="hidden" name="araz_id" value="<?php echo $select[0]['id']; ?>">
                <input type="submit" name="sent_jawab" value="Send Jawab" class="btn btn-success btn-block">
              </div>
              <div class="col-md-3 pull-right">
                  <input type="text" name="jawab_city" placeholder="Enter Jawab City" class="form-control">
              </div>
              <div class="col-md-3 pull-right">
                <select name="counselor[]" class="form-control">
                  <option value="">Select Counselor</option>
                  <?php
                    if($counselors){
                      foreach ($counselors as $coun){
                  ?>
                  <option value="<?php echo $select[0]['id'].'-'.$coun['user_id'] ?>"><?php echo $coun['first_name'].' '.$coun['last_name']; ?></option>
                  <?php
                      }
                    }
                  ?>
                </select>
              </div>
              <div class="col-md-3 pull-right">
                <select name="saheb[]" class="form-control">
                  <option value="">Select Saheb</option>
                  <?php
                    if($sahebs){
                      foreach ($sahebs as $s){
                  ?>
                  <option value="<?php echo $select[0]['id'].'-'.$s['user_id'] ?>"><?php echo $s['first_name'].' '.$s['last_name']; ?></option>
                  <?php
                      }
                    }
                  ?>
                </select>
              </div>
            </div>
            <?php 
                } 
              }else {
            ?>
            
            <table class="table table-striped table-hover table-condensed">
              <thead>
                <th>School Name</th>
                <th>Standard</th>
                <th>School City</th>
                <th class="text-center">Jawab</th>
              </thead>
              <tbody>
                <tr class="<?php if($select[0]['jawab_given'] == '1'){ echo 'success'; } ?>">
                  <td><?php echo $select[0]['school_name']; ?></td>
                  <td><?php echo $select[0]['school_standard']; ?></td>
                  <td><?php echo $select[0]['school_city']; ?></td>
                  <td class="text-center"><?php if($select[0]['jawab_given'] != '1'){ ?><input type="radio" name="jawab" id="jawab" value="<?php echo $select[0]['id']; ?>"><?php }else { echo 'Yes'; } ?></td>
                </tr>
              </tbody>
            </table>
            
            <?php if($select[0]['jawab_given'] != '1'){ ?>
            <div class="col-md-12">
              <div class="col-md-2 pull-right">
                <input type="hidden" name="araz_id" value="<?php echo $select[0]['id']; ?>">
                <input type="submit" name="sent_jawab" value="Send Jawab" class="btn btn-success btn-block">
              </div>
              <div class="col-md-2 pull-right">
                  <input type="text" name="jawab_city" placeholder="Enter Jawab City" class="form-control">
              </div>
              <div class="col-md-3 pull-right">
                  <input type="submit" name="send_count_saheb" id="send_count_saheb" value="Send Counselor / Saheb" class="btn btn-success btn-block">
                </div>
              <div class="col-md-2 pull-right">
                <select name="counselor[]" class="form-control">
                  <option value="">Select Counselor</option>
                  <?php
                    if($counselors){
                      foreach ($counselors as $coun){
                  ?>
                  <option value="<?php echo $select[0]['id'].'-'.$coun['user_id'] ?>"><?php echo $coun['first_name'].' '.$coun['last_name']; ?></option>
                  <?php
                      }
                    }
                  ?>
                </select>
              </div>
              <div class="col-md-2 pull-right">
                <select name="saheb[]" class="form-control">
                  <option value="">Select Saheb</option>
                  <?php
                    if($sahebs){
                      foreach ($sahebs as $s){
                  ?>
                  <option value="<?php echo $select[0]['id'].'-'.$s['user_id'] ?>"><?php echo $s['first_name'].' '.$s['last_name']; ?></option>
                  <?php
                      }
                    }
                  ?>
                </select>
              </div>
            </div>
              <?php } } ?>
          </div>
          <!-- /.table-responsive -->
        </div>
        <!-- /.panel-body -->
      </div>
    </div>
    </form>
  </div>
  <script>
    $(".confirm").confirm({
      text: "Are you sure you want to delete?",
      title: "Confirmation required",
      confirm: function(button) {
        $('#del-grp-val').val($(button).attr('id'));
        $('#frm-del-grp').submit();
        alert('Are you Sure You want to delete: ' + id);
      },
      cancel: function(button) {
        // nothing to do
      },
      confirmButton: "Yes",
      cancelButton: "No",
      post: true,
      confirmButtonClass: "btn-danger"
    });
    
    function validate()
    {
        var jawab = document.raza_form.jawab.checked;
        var error = 'Following Error Encountered :\n\n';
        var validate = true;

        if(jawab != '') {
          if(document.raza_form.jawab_city.value == ''){
            error += 'Please Enter Jawab City\n';
            validate = false;
          }
        }

        if (validate == false)
        {
          alert(error);
          return validate;
        }
    }
  </script>
<!-- /#page-wrapper -->

<!-- /#wrapper -->
<?php
include 'footer.php';
?>
