<?php
require_once 'classes/class.user.admin.php';
include 'classes/class.subject.php';

$allowed_roles = array(ROLE_HEAD_OFFICE);
require_once 'session.php';

$sub = new subjects();
$id = FALSE;
$select = FALSE;

if (isset($_GET['delete_id'])) {
  
}

if (isset($_GET['id'])) {
  $select = $sub->subjects_result(false, false,$_GET['id']);
//print_r($select);
  $id = $_GET['id'];
}

if (isset($_POST['Update'])) {
  $id = $_GET['id'];
  $subject = $_POST['subject'];

  $question_update = $sub->update_subjects($subject, $id);

  if ($question_update)
    $_SESSION[SUCCESS_MESSAGE] = "Question Successfully Update";
  if (!$question_update)
    $_SESSION[ERROR_MESSAGE] = "Error In Update Data";
}

if (isset($_POST['Save'])) {

  $subject = $_POST['subject'];

  $question_insert = $sub->insert_subjects($subject);
  if ($question_insert)
    $_SESSION[SUCCESS_MESSAGE] = "Subject Successfully Insert";
  if (!$question_insert)
    $_SESSION[ERROR_MESSAGE] = "Error In Inserting Data";
}

$heading = $id ? 'Update City' : $heading = 'Add New City';
$btn_name = $id ? 'Update' : 'Add New';

$title = 'Araz Subjects';
$description = '';
$keywords = '';
$active_page = "manage_arz_subjects";

include_once('header.php');
?>
  <div class="row">
    <div class="col-lg-4 col-xs-12 col-sm-12 pull-right">
      <div class="col-lg-12">
        <h3 class="page-header"><?php echo $heading; ?></h3>
      </div>
      <!-- /.col-lg-12 -->

      <form method="post"  action="araz_sub.php" role="form" class="form-horizontal" >
        <div class="col-md-12">&nbsp;</div>

        <div class="form-group">
          <label class="control-label">Subject:</label>
          <input type="text" class="form-control" name="subject"  placeholder="Add Subject" value="<?php echo $select[0]['name']; ?>" required>
        </div>

        <div class="form-group">
          <?php
          $btn_name = $id ? 'Update' : 'Save';
          ?>
          <button name="<?php echo $btn_name; ?>" class="btn btn-success btn-block"><?php echo $btn_name ?></button>
        </div>
      </form>
    </div>

    <div class="col-lg-8 col-xs-12 col-sm-12 pull-left">
      <div class="col-md-12">&nbsp;</div>
      <div class="clearfix"></div>
      <div class="panel panel-success">
        <div class="panel-heading">
          <i class="fa fa-group fa-fw"></i> Cities
        </div>
        <!-- /.panel-heading -->

        <div class="panel-body">
          <div class="table-responsive">
            <table class="table table-striped table-hover table-condensed">
              <thead>
              <th>No</th>
              <th>Subject</th>
              <th>Action</th>
              </thead>
              <tbody>
                <?php
                $select = $sub->subjects_result();
                $count = $sub->count_subjects();
                require_once 'pagination.php';
                $pagination = pagination(10, $page, 'addcity.php?page=', $count);

                $i = 1;
                foreach ($select as $data) {
                  ?>
                  <tr>
                    <td>
                      <?php echo $i++; ?>
                    </td>
                    <td>
                      <?php echo $data['name']; ?>
                    </td>
                    <td>
                      <a href="araz_sub.php?id=<?php echo $data['id']; ?>"><i class="fa fa-edit fa-fw"></i></a>
                      <a href="#" class="confirm"><i class="fa fa-remove fa-fw text-danger"></i></a>
                    </td>
                  </tr>
                  <?php
                }
                ?>
              </tbody>
            </table>
          </div>
          <!-- /.table-responsive -->
        </div>
        <div class="panel-footer">
          <?php echo $pagination; ?>
          <div class="clearfix"></div>
        </div>
        <!-- /.panel-body -->
      </div>
    </div>

    <form method="post" id="frm-del-grp">
      <input type="hidden" name="id" id="del-arz-sub-val" value="">
      <input type="hidden" name="cmd" value="del-city">
    </form>
  </div>
  <script>
    $(".confirm").confirm({
      text: "Are you sure you want to delete?",
      title: "Confirmation required",
      confirm: function(button) {
        $('#del-arz-sub-val').val($(button).attr('id'));
        $('#frm-del-arz-sub').submit();
        alert('Are you Sure You want to delete: ' + id);
      },
      cancel: function(button) {
        // nothing to do
      },
      confirmButton: "Yes",
      cancelButton: "No",
      post: true,
      confirmButtonClass: "btn-danger"
    });
  </script>
<!-- /#page-wrapper -->

<!-- /#wrapper -->
<?php
include 'footer.php';
?>
