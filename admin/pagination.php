<?php

function pagination($per_page = 10, $page = 1, $url = '', $total) {

  $adjacents = "2";

  $page = ($page == 0 ? 1 : $page);
  $start = ($page - 1) * $per_page;

  $prev = $page - 1;
  $next = $page + 1;
  $lastpage = ceil($total / $per_page);
  $lpm1 = $lastpage - 1;

  $pagination = "";
  if ($lastpage > 1) {
    $pagination .= "<div class='col-md-8'>
                            <ul class='pagination'>";
    if ($page > 4) {
      $pagination.= "<li><a href='{$url}1'>&lsaquo;&lsaquo;</a></li>";
    }

    if ($page > 1) {
      $pagination.= "<li><a href='{$url}$prev'>&lsaquo;</a></li>";
    }

    if ($lastpage < 7 + ($adjacents * 2)) {
      for ($counter = 1; $counter <= $lastpage; $counter++) {
        if ($counter == $page)
          $pagination.= "<li class='active'><a href='#'>$counter </a></li>";
        else
          $pagination.= "<li><a href='{$url}$counter'>$counter </a></li>";
      }
    }
    elseif ($lastpage > 5 + ($adjacents * 2)) {

      if ($page < 1 + ($adjacents * 2)) {
        for ($counter = 1; $counter < 4 + ($adjacents * 2); $counter++) {
          if ($counter == $page)
            $pagination.= "<li class='active'><a href='#'>$counter </a></li>";
          else
            $pagination.= "<li><a href='{$url}$counter'>$counter </a></li>";
        }
      }
      elseif ($lastpage - ($adjacents * 2) > $page && $page > ($adjacents * 2)) {
        for ($counter = $page - $adjacents; $counter <= $page + $adjacents; $counter++) {
          if ($counter == $page)
            $pagination.= "<li class='active'><a href='#'>$counter </a></li>";
          else
            $pagination.= "<li><a href='{$url}$counter'>$counter </a></li>";
        }
      }
      else {
        for ($counter = $lastpage - (2 + ($adjacents * 2)); $counter <= $lastpage; $counter++) {
          if ($counter == $page)
            $pagination.= "<li class='active'><a href='#'>$counter </a></li>";
          else
            $pagination.= "<li><a href='{$url}$counter'>$counter </a></li>";
        }
      }
    }

    if ($page < $counter - 1) {
      $pagination.= "<li><a href='{$url}$next'>&rsaquo;</a></li>";
    }
    if ($page + 4 < $lastpage) {
      $pagination.= "<li><a href='{$url}$lastpage'>&rsaquo;&rsaquo;</a></li>";
    }
    $pagination.= "</ul>\n</div>\n";
  }

  $last_Record = $start + $per_page;
  if ($total < $last_Record)
    $last_Record = $total;
  $start = $start + 1;
  // fix for 0 records
  if ($start > $total)
    $start = $total;
  $pagination .=" <div class='col-md-3 record-count pull-right'>" .
          $start . " - " . $last_Record . " of " . $total . "</div>";
  return $pagination;
}

?>
