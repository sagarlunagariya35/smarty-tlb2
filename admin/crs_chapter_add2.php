<?php
include 'classes/class.crs_course.php';
include 'classes/class.crs_chapter.php';
$allowed_roles = array(ROLE_HEAD_OFFICE);
require_once 'session.php';

$crs_chapter = new Mtx_Crs_Chapter();
$crs_course = new Mtx_Crs_Course();

$title = 'Chapters List';
$description = '';
$keywords = '';
$active_page = "list_chapters";

$result = $id = FALSE;

if (isset($_REQUEST['id'])) {
  $id = $_REQUEST['id'];
  $result = $crs_chapter->get_chapter($id);
}

if (isset($_POST['btn_submit'])) {
  $data = $db->clean_data($_POST);
  // get variables
  
  $course_id = $data['course_id'];
  $title = $data['title'];
  $slug = $db->clean_slug($_POST['slug']);
  $active = $data['active'];
  $sort = $data['sort'];
  $description = $data['description'];
  $start_date = $data['start_date'];
  $end_date = $data['end_date'];
  $user_id = $_SESSION[USER_ID];
  
  $last_insert_id = $crs_chapter->get_last_insert_chapter();
  $last_insert_id = $last_insert_id + 1;
  
  if($_FILES['img_icon']['name'] != ''){
    $icon_image = $_FILES['img_icon']['name'];
    $icon_image_temp = $_FILES['img_icon']['tmp_name'];
    
    $filearray = explode('.', $icon_image);
    $ext = $filearray[count($filearray)-1];
              
    $icon_image_name = $last_insert_id.'.'.$ext;
    $imagepathANDname = "../upload/courses/chapter/" . $icon_image_name;
    move_uploaded_file($icon_image_temp, $imagepathANDname);
  }

  $insert = $crs_chapter->insert_chapter($course_id, $title, $slug, $description, $icon_image_name, $start_date, $end_date, $sort, $user_id, $active);

  if ($insert) {
    $chapter_id = $last_insert_id;
    foreach($_FILES as $files => $fileattributes)
    {
      if($_FILES[$files]['size'] != '0' AND $files != 'img_icon')
      {
        if(strlen($files) > 5){
          $keyword = substr($files, 0, 5);
        }else {
          $keyword = substr($files, 0, 4);
        }

        $number = substr($files,5,strlen($files));
        $originalfile = $_FILES[$files]['name'];
        $filearray = explode('.',$originalfile);
        $ext = $filearray[count($filearray)-1];
        $filename = $chapter_id.'-'.$originalfile;
        $filetemp = $_FILES[$files]['tmp_name'];
        $index = 1;

        $pathANDname = "../upload/courses/chapter/" . $keyword . "/" . $filename;
        $moveResult = move_uploaded_file($filetemp, $pathANDname);

        $insert_files = $crs_chapter->insert_media($keyword, $chapter_id, $filename, $user_id, $active);
      }
    }
      
    $_SESSION[SUCCESS_MESSAGE] = 'Chapter Inserted Successfully.';
  } else {
    $_SESSION[ERROR_MESSAGE] = 'Error encountered while Inserting Chapter';
  }
}

$list_chapters = $crs_chapter->get_all_chapters();
$list_courses = $crs_course->get_all_courses();

include_once("header.php");
?>
<link rel="stylesheet" href="js/select2/select2.min.css">
<script src="js/select2/select2.full.min.js"></script>
<script type="text/javascript" src="ckeditor/ckeditor.js"></script>

<style type="text/css">
  .video_box,.audio_box,.image_box,.file_box
  {
    border: 1px solid #ccc;
    padding:10px 5px 50px 5px;
    margin-left: 15px;
  }
  #content_box
  {
    margin-top: 10px;
    margin-left: 15px;
  }
  .video_link,.audio_link,.image_link,.file_link
  {
    padding: 5px 0px;
  }
  input[type="file"]
  {
    width: 95px;
    display: inline-block;
  }
  #remove_box_link1,#remove_audio_box_link1,#remove_image_box_link1,#remove_file_box_link1
  {
    margin-left: 0px !important;
  }
</style>

<div class="row">
  <div class="col-lg-12">
    <h3 class="page-header">Add New Chapter</h3>
  </div>
  <!-- /.col-lg-12 -->
</div>

<div class="row">
  <div class="col-md-12">
    <form method="post" enctype="multipart/form-data">
      
      <div class="form-group col-md-4 col-xs-12">
        <label for="title">Course :</label>
        <select name="course_id" id="course_id" class="form-control course_id">
          <option value="">Select</option>
          <?php
          if ($list_courses) {
            foreach ($list_courses as $lc) {
          ?>
            <option value="<?php echo $lc['id']; ?>"><?php echo $lc['title']; ?></option>
          <?php
            }
          }
          ?>
        </select>
      </div>

      <div class="form-group col-md-4 col-xs-12">
        <label for="title">Title :</label>
        <input type="text" name="title" id="title" required class="form-control">
      </div>
      
      <div class="form-group col-md-4 col-xs-12">
        <label for="slug">Slug :</label>
        <input type="text" name="slug" id="slug" required class="form-control">
      </div>

      <div class="form-group col-md-4 col-xs-12">
        <label for="slug">Image icon :</label>
        <input type="file" name="img_icon" class="file">
      </div>

      <div class="form-group col-md-4 col-xs-12">
        <label for="active">Is Active? :</label>
        <select name="active" id="active" class="form-control" required >
          <option value="">Select</option>
          <option value="1">Yes</option>
          <option value="0">No</option>
        </select>
      </div>
      
      <div class="form-group col-md-4 col-xs-12">
        <label for="sort">Sort :</label>
        <input type="text" name="sort" id="sort" required class="form-control">
      </div>
      
      <div class="form-group col-md-4 col-xs-12">
        <label for="slug">Start Date :</label>
        <input type="date" name="start_date" class="form-control">
      </div>
      
      <div class="form-group col-md-4 col-xs-12">
        <label for="slug">End Date :</label>
        <input type="date" name="end_date" class="form-control">
      </div>
      
      <div class="form-group col-md-12 col-xs-12">
        <label for="miqat">Description :</label>
        <textarea name="description" class="form-control"></textarea>
      </div>
      
      <div class="form-group col-md-12 col-xs-12">
        <label for="miqat">HTML Code :</label>
        <textarea name="html_code" class="form-control"></textarea>
      </div>
      
      <div class="clearfix"></div>
      <div class="video_box" id="video_box" style="margin-top:20px;">
        <div class="video_link" id="video_link">
          <label>Upload Video : </label>
          <input type="text" class="videofilename1" id="videofilename1" readonly="readonly">
          <input type="file" name="video1" id="video1" class="file" onchange="changevideo(this);">          
        </div>
        <div class="add_box" id="add_box"><a href="javascript:void(0);" id="add_box_link"><img src="images/add.png" alt="" style="height: 25px;"></a></div>
        <input type="hidden" name="videovalue" id="videovalue" value="1"> 
      </div><!----VIDEO_BOX-->

      <div class="audio_box" id="audio_box">
        <div class="audio_link" id="audio_link">
          <label>Upload Audio : </label>
          <input type="text" class="audiofilename1" id="audiofilename1" readonly="readonly">
          <input type="file" name="audio1" id="audio1" class="file" onchange="audiochange(this);">
        </div>
        <div class="add_audio_box" id="add_audio_box"><a href="javascript:void(0);" id="add_audio_box_link"><img src="images/add.png" alt="" style="height: 25px;"></a></div>
        <input type="hidden" name="audiovalue" id="audiovalue" value="1"> 
      </div><!----AUDIO_BOX-->

      <div class="image_box" id="image_box">
        <div class="image_link" id="image_link">
          <label>Upload Image : </label>
          <input type="text" class="imagefilename1" id="imagefilename1" readonly="readonly">
          <input type="file" name="image1" id="image1" class="file" onchange="imagechange(this);">
        </div>
        <div class="add_image_box" id="add_image_box"><a href="javascript:void(0);" id="add_image_box_link"><img src="images/add.png" alt="" style="height: 25px;"></a></div>
        <input type="hidden" name="imagevalue" id="imagevalue" value="1"> 
      </div><!----IMAGE_BOX-->
      
      <div class="file_box" id="file_box">
        <div class="file_link" id="file_link">
          <label>Upload File : </label>
          <input type="text" class="filename1" id="filename1" readonly="readonly">
          <input type="file" name="file1" id="file1" class="file" onchange="filechange(this);">
        </div>
        <div class="add_file_box" id="add_file_box"><a href="javascript:void(0);" id="add_file_box_link"><img src="images/add.png" alt="" style="height: 25px;"></a></div>
        <input type="hidden" name="filevalue" id="filevalue" value="1"> 
      </div><!----IMAGE_BOX-->
      
      <div class="clearfix">&nbsp;</div>
      <div class="form-group col-md-2 col-xs-12">
        <input type="submit" name="btn_submit" id="btn_submit" value="Submit" class="btn btn-primary btn-block form-control">
      </div>
    </form>

  </div>
</div>

<script type="text/javascript" src="js/course_details.js?v=1"></script>
<script type="text/javascript">
  /////////////////////////////////////////FOR VIDEO START/////////////////////////////////////////////////////     
  $(document).ready(function() {
    var videocount = $('.video_link').length;
    var audiocount = $('.audio_link').length;
    var imagecount = $('.image_link').length;
    var filecount = $('.file_link').length;
    if (videocount == 1)
    {
      $('.video_link').css('float', 'left');
      $('#add_box').css('float', 'left');
    }
    if (audiocount == 1)
    {
      $('.audio_link').css('float', 'left');
      $('#add_audio_box').css('float', 'left');
    }
    if (imagecount == 1)
    {
      $('.image_link').css('float', 'left');
      $('#add_image_box').css('float', 'left');
    }
    if (filecount == 1)
    {
      $('.file_link').css('float', 'left');
      $('#add_file_box').css('float', 'left');
    }

  });

  $(document).ready(function() {

    $("#add_box").click(function() {
      /*REMOVE ALL THINGS*/

      var videovalue = parseInt($('#videovalue').val()) + 1;
      $('#videovalue').val(videovalue);

      $('#video_box div.video_link').removeClass('lastchild');

      //ADD NEW BOX AND SET CSS
      $('#add_box').before('<div class="video_link" id="video_link"><label> Upload Video : </label><input type="text" class="videofilename' + videovalue + '" id="videofilename' + videovalue + '" readonly="readonly" style="margin-left:4px;"><input type="file" name="video' + videovalue + '" id="video' + videovalue + '" class="file" onchange="changevideo(this);" style="margin-left:4px;"></div>');
      var count = $('.video_link').length;
      if (count > 1)
      {
        $('.video_link').css('float', '');
        $('#video_box div.video_link:last').css('float', 'left');
        $('#video_box div.video_link:last').addClass('lastchild');

        //APPEND ONLY IF NOT THERE
        $(".video_link").each(function(index) {
          if ($(this).find("a").length)
          {
          }
          else
          {
            var rvideovalue = videovalue - 1;
            $(this).append('<a href="javascript:void(0);" id="remove_box_link' + rvideovalue + '" onclick="return removevideo(this);" style="margin-left:4px;"><img src="images/remove.png" alt="" style="height: 15px;"></a>');
          }
        });
        //REMOVE REMOVE BTN FROM LAST CHILD
        if ($('#video_box div.video_link').hasClass('lastchild'))
        {
          $("#video_box div.video_link:last").children("a:first").remove();
        }
      }
    });//END ADD CLICK
  });
  
  function changevideo(elem)
  {
    var id = $(elem).attr("id");
    var file = $('#' + id).val();
    var filearray = file.split('.');
    var arraysize = filearray.length;
    var filetype = filearray[arraysize - 1].toLowerCase();
    if (filetype == '3gp' || filetype == 'wmv' || filetype == 'mp4' || filetype == 'avi' || filetype == 'mpeg')
    {
      var lastChar = id.substr(id.length - 1);
      //alert(lastChar);
      $('#videofilename' + lastChar).val(file);
    }
    else
    {
      //alert(id);
      $('#' + id).val('');
      alert('Invalid File Type, Please Select Video Only');
    }
  }

  function removevideo(elem)
  {
    if ($(".video_link").length == 2)
    {
      //alert($( ".video_link" ).length);
      //SET HVALUE AS 1
    }
    var id = $(elem).attr("id");
    $('#' + id).parent().remove();
  }
/////////////////////////////////////////FOR VIDEO END/////////////////////////////////////////////////////     
  $(document).ready(function() {
    $('#add_audio_box').click(function() {
      var audiovalue = parseInt($('#audiovalue').val()) + 1;
      $('#audiovalue').val(audiovalue);
      $('#audio_box div.audio_link').removeClass('lastchild');
      //APPEND THE CODE
      $('#add_audio_box').before('<div class="audio_link" id="audio_link"><label>Upload Audio : </label><input type="text" style="margin-left:4px;" class="audiofilename' + audiovalue + '" id="audiofilename' + audiovalue + '" readonly="readonly"><input type="file" name="audio' + audiovalue + '" id="audio' + audiovalue + '" class="file" onchange="audiochange(this);" style="margin-left:4px;"></div>');

      var count = $('.audio_link').length;
      if (count > 1)
      {
        $('.audio_link').css('float', '');
        $('#audio_box div.audio_link:last').css('float', 'left');
        $('#audio_box div.audio_link:last').addClass('lastchild');

        //APPEND ONLY IF NOT THERE
        $(".audio_link").each(function(index) {
          if ($(this).find("a").length)
          {
          }
          else
          {
            var raudiovalue = audiovalue - 1;
            $(this).append('<a href="javascript:void(0);" id="remove_audio_box_link' + raudiovalue + '" onclick="return removeaudio(this);" style="margin-left:4px;"><img src="images/remove.png" alt="" style="height: 15px;"></a>');
          }
        });
        //REMOVE REMOVE ADD BTN FROM LAST CHILD
        if ($('#audio_box div.audio_link').hasClass('lastchild'))
        {
          $("#audio_box div.audio_link:last").children("a:first").remove();
        }
      }
    });
  });
  
  function audiochange(elem)
  {
    var id = $(elem).attr("id");
    //alert(id.val);
    var file = $('#' + id).val();

    var filearray = file.split('.');
    var arraysize = filearray.length;
    var filetype = filearray[arraysize - 1].toLowerCase();
    if (filetype == 'mp3' || filetype == '3ga' || filetype == 'wav')
    {
      var lastChar = id.substr(id.length - 1);
      //alert(lastChar);
      $('#audiofilename' + lastChar).val(file);

    }
    else
    {
      //alert(id);
      $('#' + id).val('');
      alert('Invalid File Type, Please Select Audio File Only');
    }
  }
  
  function removeaudio(elem)
  {
    if ($(".audio_link").length == 2)
    {
      //alert($( ".audio_link" ).length);
      //SET HVALUE AS 1
    }
    var id = $(elem).attr("id");
    $('#' + id).parent().remove();
  }
////////////////////////////////////////////////////////////////////////AUDIO END/////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////IMAGE START////////////////////////////////////////////////////////////
  $(document).ready(function() {
    $('#add_image_box').click(function() {
      var imagevalue = parseInt($('#imagevalue').val()) + 1;
      $('#imagevalue').val(imagevalue);
      $('#image_box div.image_link').removeClass('lastchild');
      //APPEND THE CODE
      $('#add_image_box').before('<div class="image_link" id="image_link"><label>Upload Image : </label><input type="text" class="imagefilename' + imagevalue + '" id="imagefilename' + imagevalue + '" readonly="readonly" style="margin-left:4px;"><input type="file" name="image' + imagevalue + '" id="image' + imagevalue + '" class="file" onchange="imagechange(this);" style="margin-left:4px;"></div>');

      var count = $('.image_link').length;
      if (count > 1)
      {
        $('.image_link').css('float', '');
        $('#image_box div.image_link:last').css('float', 'left');
        $('#image_box div.image_link:last').addClass('lastchild');

        //APPEND ONLY IF NOT THERE
        $(".image_link").each(function(index) {
          if ($(this).find("a").length)
          {
          }
          else
          {
            var rimagevalue = imagevalue - 1;
            $(this).append('<a href="javascript:void(0);" id="remove_image_box_link' + rimagevalue + '" onclick="return removeimage(this);" style="margin-left:4px;"><img src="images/remove.png" alt="" style="height: 15px;"></a>');
          }
        });
        //REMOVE REMOVE ADD BTN FROM LAST CHILD
        if ($('#image_box div.image_link').hasClass('lastchild'))
        {
          $("#image_box div.image_link:last").children("a:first").remove();
        }
      }
    });
  });
  
  function imagechange(elem)
  {
    var id = $(elem).attr("id");
    //alert(id.val);
    var file = $('#' + id).val();


    var filearray = file.split('.');
    var arraysize = filearray.length;
    var filetype = filearray[arraysize - 1].toLowerCase();
    if (filetype == 'jpg' || filetype == 'png' || filetype == 'jpeg' || filetype == 'gif' || filetype == 'tif' || filetype == 'raw' || filetype == 'bmp')
    {
      var lastChar = id.substr(id.length - 1);
      //alert(lastChar);
      $('#imagefilename' + lastChar).val(file);
    }
    else
    {
      $('#' + id).val('');
      alert('Invalid File Type, Please Select Image File Only');
    }
  }
  
  function removeimage(elem)
  {
    if ($(".image_link").length == 2)
    {
      //alert($( ".image_link" ).length);
      //SET HVALUE AS 1
    }
    var id = $(elem).attr("id");
    $('#' + id).parent().remove();
  }
////////IMAGE END///////////////
//
///////////////////////////////////////////////////////////////////////FILE START////////////////////////////////////////////////////////////
  $(document).ready(function() {
    $('#add_file_box').click(function() {
      var filevalue = parseInt($('#filevalue').val()) + 1;
      $('#filevalue').val(filevalue);
      $('#file_box div.file_link').removeClass('lastchild');
      //APPEND THE CODE
      $('#add_file_box').before('<div class="file_link" id="file_link"><label>Upload File : </label><input type="text" class="filename' + filevalue + '" id="filename' + filevalue + '" readonly="readonly" style="margin-left:4px;"><input type="file" name="file' + filevalue + '" id="file' + filevalue + '" class="file" onchange="filechange(this);" style="margin-left:4px;"></div>');

      var count = $('.file_link').length;
      if (count > 1)
      {
        $('.file_link').css('float', '');
        $('#file_box div.file_link:last').css('float', 'left');
        $('#file_box div.file_link:last').addClass('lastchild');

        //APPEND ONLY IF NOT THERE
        $(".file_link").each(function(index) {
          if ($(this).find("a").length)
          {
          }
          else
          {
            var rfilevalue = filevalue - 1;
            $(this).append('<a href="javascript:void(0);" id="remove_file_box_link' + rfilevalue + '" onclick="return removefile(this);" style="margin-left:4px;"><img src="images/remove.png" alt="" style="height: 15px;"></a>');
          }
        });
        //REMOVE REMOVE ADD BTN FROM LAST CHILD
        if ($('#file_box div.file_link').hasClass('lastchild'))
        {
          $("#file_box div.file_link:last").children("a:first").remove();
        }
      }
    });
  });
  
  function filechange(elem)
  {
    var id = $(elem).attr("id");
    //alert(id.val);
    var file = $('#' + id).val();


    var filearray = file.split('.');
    var arraysize = filearray.length;
    var filetype = filearray[arraysize - 1].toLowerCase();
    if (filetype == 'pdf' || filetype == 'doc' || filetype == 'xls' || filetype == 'ppt' || filetype == 'docx' || filetype == 'xlsx' || filetype == 'pptx')
    {
      var lastChar = id.substr(id.length - 1);
      //alert(lastChar);
      $('#filename' + lastChar).val(file);
    }
    else
    {
      $('#' + id).val('');
      alert('Invalid File Type, Please Select Files Only');
    }
  }
  
  function removefile(elem)
  {
    if ($(".file_link").length == 2)
    {
      //alert($( ".image_link" ).length);
      //SET HVALUE AS 1
    }
    var id = $(elem).attr("id");
    $('#' + id).parent().remove();
  }
////////FILE END///////////////

//DATE VALIDATION
  function datevalidation()
  {
    var startDate = new Date($('#start_date').val());
    var endDate = new Date($('#end_date').val());

    if (startDate < endDate)
    {
      // Do something
    }
    else
    {
      alert('End Date Must be Greater');
      $('#end_date').val('');
    }
  }
</script>

<!-- /#page-wrapper -->
<?php include "./footer.php"; ?>
