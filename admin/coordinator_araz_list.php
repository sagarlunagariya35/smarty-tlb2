<?php
require_once 'classes/class.user.admin.php';
require_once '../classes/class.user.php';
include 'classes/class.araiz.php';
$allowed_roles = array(ROLE_PROJECT_COORDINATOR,ROLE_ASHARAH_ADMIN);
require_once 'session.php';

$user = new mtx_user_admin;
$araiz = new Araiz();

$title = 'Project Co-ordinator Applications';
$description = '';
$keywords = '';
$active_page = "manage_project_araz";

$select = $araiz->get_all_assign_araz_to_project_coordinator();
$total_araz = $araiz->count_total_assign_araz_to_project_coordinator();

$araiz_parsed_data = $araiz->get_araiz_data_organised($select);

include ('header.php');
?>

<link rel="stylesheet" href="js/tablesorter/css/theme.bootstrap.css">
<script src="js/tablesorter/js/jquery.tablesorter.js"></script>
<script src="js/tablesorter/js/jquery.tablesorter.widgets.js"></script>
<style>
  .alfatemi-text{
    font-size: 18px;
  }
</style>
<div class="row">
  <div class="row">
    <div class="col-xs-12 col-sm-12">&nbsp;</div>
  </div>
  <div class="row">
    <div class="col-xs-12 col-sm-12">
      <h2 class="text-center">Total Project Co-ordinator Applications: <?php echo $total_araz; ?><span class="pull-right"><?php echo 'Page: '.$page; ?></span></h2>
      
    </div>
  </div>
  <div class="row">
    <div class="col-xs-12 col-sm-12">&nbsp;</div>
  </div>
  <form name="raza_form" class="form" method="post" action="">
  
  <div class="col-lg-12 col-xs-12 col-sm-12 pull-left">
    <div class="col-md-12">&nbsp;</div>
    <div class="clearfix"></div>
    <div class="row">
      <?php
        foreach ($araiz_parsed_data as $araz_id => $araz) {
          $user_details = $araz['user_data'];
          $araz_general_detail = $araz['araz_data'];
      ?>
        <div class="panel panel-primary">
          <div class="panel-heading" style="font-size: 19px;">
            <div class="row">
              <div class="col-xs-12 col-sm-4"><strong>Track No: </strong><?php echo $araz_id; ?></div>
              <div class="col-xs-12 col-sm-4"></div>
              <div class="col-xs-12 col-sm-4 text-right"><strong>Marhala: </strong><?php echo $araz_general_detail['marhala']; ?></div>
              <div class="clearfix"></div>
            </div>
          </div>
          <div class="row"></div>
          <!-- /.panel-heading -->
          <div class="panel-body">
            <div class="panel panel-default">
              <div class="panel-heading">
                <strong>Personal Details: </strong><?php echo $user_details['user_full_name']; ?>
              </div>
              <div class="panel-body">
                <div class="row">
                  <div class="col-xs-12 col-sm-10">
                    <div class="col-xs-12 col-sm-4 bottom10px">
                      <strong>ITS: </strong><?php echo $user_details['login_its']; ?>
                    </div>
                    <div class="col-xs-12 col-sm-4 bottom10px">
                      <strong>Mobile: </strong><?php echo ucfirst($user_details['mobile']); ?>
                    </div>
                    <div class="col-xs-12 col-sm-4 bottom10px">
                      <strong>Whatsapp: </strong><?php echo ucfirst($user_details['whatsapp']); ?>
                    </div>
                    <div class="clearfix"></div>
                  </div>
                </div>
              </div>
            </div>
            <div class="panel panel-default">
              <div class="panel-heading">
                <strong>Project Co-ordinator Details: </strong>
              </div>
              <?php
                $review_answer[0] = 'No';
                $review_answer[1] = 'Yes';
              ?>
              <div class="panel-body">
                <div class="row">
                  <div class="col-xs-12 col-sm-12">
                    <div class="col-xs-12 col-sm-4 bottom10px">
                      <strong>Recommend Psychometric Test: </strong> <?php echo $review_answer[$araz_general_detail['recommend_psychometric_test']] ?>
                    </div>
                    <div class="col-xs-12 col-sm-4 bottom10px">
                      <strong>Recommend Counseling: </strong> <?php echo $review_answer[$araz_general_detail['recommend_counseling']] ?>
                    </div>
                    <div class="col-xs-12 col-sm-4 bottom10px">
                      <strong>Recommend Qardan Hasana: </strong> <?php echo $review_answer[$araz_general_detail['recommend_qardan']] ?>
                    </div>
                    <div class="clearfix"></div>
                    <div class="col-xs-12 col-sm-4 bottom10px">
                      <strong>Recommend Sponsorship: </strong> <?php echo $review_answer[$araz_general_detail['recommend_sponsorship']] ?>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <?php
            $stauts[0] = 'Not Set';
            $stauts[1] = 'Call for Action';
            $stauts[2] = 'Under Process';
            $stauts[3] = 'Mark Completion';
          ?>
          <div class="panel-footer">
            <div class="row clsstatusbchet">
              <div class="col-xs-12 col-md-4">
                <strong>Status: </strong> <?php echo $stauts[$araz_general_detail['bchet_status']] ?>
              </div>
              <div class="col-xs-12 col-md-3">
                <select name="set_status" class="form-control set_status">
                  <option value="">Select Status</option>
                  <option value="1">Call for Action</option>
                  <option value="2">Under Process</option>
                  <option value="3">Mark Completion</option>
                </select>
              </div>
              <input type="hidden" name="araz_id" class="araz_id" value="<?php echo $araz_id; ?>" >
            </div>
          </div>
        </div>
      <?php
        }
      ?>
    </div>
  </div>
</form>
</div>

<?php
include 'footer.php';
?>
