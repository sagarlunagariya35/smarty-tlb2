<?php
include 'classes/class.crs_topic.php';
$allowed_roles = array(ROLE_HEAD_OFFICE);
require_once 'session.php';

$crs_topic = new Mtx_Crs_Topic();

$title = 'Topics List';
$description = '';
$keywords = '';
$active_page = "list_topics";

$result = $id = FALSE;

if (isset($_REQUEST['id'])) {
  $id = $_REQUEST['id'];
  $result = $crs_topic->get_topic($id);
}

if(isset($_GET['cmd']) && $_GET['cmd'] == 'change_status') {
  
  $change_status = $crs_topic->change_topic_status($_GET['status'], $_GET['id']);
  
  if ($change_status) {
    $_SESSION[SUCCESS_MESSAGE] = 'Topic Status changed successfully.';
    header('Location: crs_topics.php');
    exit();
  } else {
    $_SESSION[ERROR_MESSAGE] = 'Error encountered while Changing Topic Status';
  }
}

if (isset($_POST['btn_update'])) {
  $data = $db->clean_data($_POST);
  // get variables
  $topic_id = $data['topic_id'];
  $title = $data['title'];
  $slug = $db->clean_slug($_POST['slug']);
  $active = $data['active'];
  $sort = $data['sort'];
  $description = $data['description'];
  $user_id = $_SESSION[USER_ID];
  
  $last_insert_id = $crs_topic->get_last_insert_topic();
  $last_insert_id = $last_insert_id + 1;
  
  if($_FILES['img_icon']['name'] != ''){
    $icon_image = $_FILES['img_icon']['name'];
    $icon_image_temp = $_FILES['img_icon']['tmp_name'];
    
    $filearray = explode('.', $icon_image);
    $ext = $filearray[count($filearray)-1];
              
    $icon_image_name = $last_insert_id.'.'.$ext;
    $imagepathANDname = "../upload/courses/topics/" . $icon_image_name;
    move_uploaded_file($icon_image_temp, $imagepathANDname);
  }else {
    $icon_image_name = $data['last_img_icon'];
  }

  $update = $crs_topic->update_topic($title, $slug, $description, $icon_image_name, $sort, $active, $id, $topic_id);

  if ($update) {
    $_SESSION[SUCCESS_MESSAGE] = 'Topic Updated Successfully.';
    header('Location: crs_topics.php?id='.$id);
    exit();
  } else {
    $_SESSION[ERROR_MESSAGE] = 'Error encountered while Updating Topic';
  }
}

if (isset($_POST['btn_submit'])) {
  $data = $db->clean_data($_POST);
  // get variables
  
  $topic_id = $data['topic_id'];
  $title = $data['title'];
  $slug = $db->clean_slug($_POST['slug']);
  $active = $data['active'];
  $sort = $data['sort'];
  $description = $data['description'];
  $user_id = $_SESSION[USER_ID];
  
  $last_insert_id = $crs_topic->get_last_insert_topic();
  $last_insert_id = $last_insert_id + 1;
  
  if($_FILES['img_icon']['name'] != ''){
    $icon_image = $_FILES['img_icon']['name'];
    $icon_image_temp = $_FILES['img_icon']['tmp_name'];
    
    $filearray = explode('.', $icon_image);
    $ext = $filearray[count($filearray)-1];
              
    $icon_image_name = $last_insert_id.'.'.$ext;
    $imagepathANDname = "../upload/courses/topics/" . $icon_image_name;
    move_uploaded_file($icon_image_temp, $imagepathANDname);
  }

  $insert = $crs_topic->insert_topic($title, $slug, $description, $icon_image_name, $sort, $user_id, $active, $topic_id);

  if ($insert) {
    $_SESSION[SUCCESS_MESSAGE] = 'Topic Inserted Successfully.';
  } else {
    $_SESSION[ERROR_MESSAGE] = 'Error encountered while Inserting Topic';
  }
}

$btn = ($id != FALSE) ? 'btn_update' : 'btn_submit';
$list_topics = $crs_topic->get_all_topics();

include_once("header.php");
?>
<link rel="stylesheet" href="js/select2/select2.min.css">
<script src="js/select2/select2.full.min.js"></script>
<script type="text/javascript" src="ckeditor/ckeditor.js"></script>

<div class="row">
  <div class="col-lg-12">
    <h3 class="page-header">Add New Topic</h3>
  </div>
  <!-- /.col-lg-12 -->
</div>

<div class="row">
  <div class="col-md-12">
    <form method="post" enctype="multipart/form-data">
      
      <div class="form-group col-md-4 col-xs-12">
        <label for="title">Parent :</label>
        <select name="topic_id" id="topic_id" class="form-control topic_id">
          <option value="">Select</option>
          <?php
          if ($list_topics) {
            foreach ($list_topics as $lt) {
          ?>
            <option value="<?php echo $lt['id']; ?>"><?php echo $lt['title']; ?></option>
          <?php
            }
          }
          ?>
        </select>
      </div>

      <div class="form-group col-md-4 col-xs-12">
        <label for="title">Title :</label>
        <input type="text" name="title" id="title" required class="form-control" value="<?php echo $result['title']; ?>">
      </div>
      
      <div class="form-group col-md-4 col-xs-12">
        <label for="slug">Slug :</label>
        <input type="text" name="slug" id="slug" required class="form-control" value="<?php echo $result['slug']; ?>">
      </div>

      <div class="form-group col-md-4 col-xs-12">
        <label for="slug">Image icon :</label>
        <input type="file" name="img_icon" class="file">
        <?php
          if ($result['img_icon'] != '') {
        ?>
          <div class="clearfix">&nbsp;</div>
          <p><img src="../upload/courses/topics/<?php echo $result['img_icon']; ?>" width="100" height="100" /></p>
          <input type="hidden" name="last_img_icon" value="<?php echo $result['img_icon']; ?>">
        <?php
          }
        ?>
      </div>

      <div class="form-group col-md-4 col-xs-12">
        <label for="active">Is Active? :</label>
        <select name="active" id="active" class="form-control" required >
          <option value="">Select</option>
          <option value="1" <?php echo ($result['is_active'] == '1') ? 'selected' : ''; ?>>Yes</option>
          <option value="0" <?php echo ($result['is_active'] == '0') ? 'selected' : ''; ?>>No</option>
        </select>
      </div>
      
      <div class="form-group col-md-4 col-xs-12">
        <label for="sort">Sort :</label>
        <input type="text" name="sort" id="sort" required class="form-control" value="<?php echo $result['sort_id']; ?>">
      </div>
      
      <div class="form-group col-md-12 col-xs-12">
        <label for="miqat">Description :</label>
        <textarea name="description" class="form-control"><?php echo $result['description']; ?></textarea>
      </div>
      
      <div class="clearfix"></div>
      <div class="form-group col-md-2 col-xs-12">
        <input type="submit" name="<?php echo $btn; ?>" id="<?php echo $btn; ?>" value="Submit" class="btn btn-primary btn-block form-control">
      </div>
    </form>

  </div>
</div>

<br>
<div class="panel panel-primary">
  <div class="panel-heading">
    <i class="fa fa-group fa-fw"></i> <?php echo $title; ?>
  </div>
  <!-- /.panel-heading -->
  <div class="panel-body">
    <div class="table-responsive">
      <table class="table table-striped table-hover table-condensed">
        <thead>
          <tr>
            <th>Sr</th>
            <th>Title</th>
            <th>Parent</th>
            <th>Description</th>
            <th>Date</th>
            <th>Status</th>
            <th>Action</th>
          </tr>
        </thead>
        <tbody>
          <?php
          if($list_topics){
            $sr = 0;
            foreach ($list_topics as $data) {
              $sr++;
              $parent_topic_name = $crs_topic->get_topic($data['parent_id']);
          ?>
              <tr class="<?php if($data['is_active'] == '1') { echo 'success'; } else { echo 'danger'; } ?>">
                <td><?php echo $sr; ?></td>
                <td><?php echo $data['title']; ?></td>
                <td><?php echo $parent_topic_name['title']; ?></td>
                <td><?php echo $data['description']; ?></td>
                <td><?php echo date('d, F Y',  strtotime($data['created_ts'])); ?></td>
                <td><input type="checkbox" name="is_active[]" id="action<?php echo $data['id']; ?>" onclick="check(<?php echo $data['id']; ?>)" <?php echo ($data['is_active'] == 1) ? 'checked' : ''; ?>></td>
                <td><a href="crs_topics.php?id=<?php echo $data['id']; ?>"><i class="fa fa-pencil-square-o"></i></a></td>
              </tr>
              <?php
            }
          }else {
            echo '<tr><td class="text-center" colspan="6">No Records..</td></tr>';
          }
          ?>
        </tbody>
      </table>
    </div>
    <!-- /.table-responsive -->
  </div>
  <!-- /.panel-body -->
</div>

<script type="text/javascript">
  function check(val){
    var doc = document.getElementById('action'+val).checked;

    if(doc == true) doc = 1;
    else doc = 0;
    window.location.href='crs_topics.php?id='+val+'&status='+doc+'&cmd=change_status';
  }
</script>
<script type="text/javascript" src="js/course_details.js?v=1"></script>

<!-- /#page-wrapper -->
<?php include "./footer.php"; ?>
