<?php
require_once 'classes/class.user.admin.php';
include 'classes/class.araiz.php';
$allowed_roles = array(ROLE_HEAD_OFFICE, ROLE_AAMIL_SAHEB, ROLE_JAMIAT_COORDINATOR);
require_once 'session.php';

$user = new mtx_user_admin;
$araiz = new Araiz();

$jamiyet_data = FALSE;

$title = 'Search Araiz By Multiple Data';
$description = '';
$keywords = '';
$active_page = "search_araiz_by_multiple_data";

include ('header.php');
?>

  <div class="row">
    <div class="col-lg-12">
        <h3 class="page-header"><?php echo $title; ?></h3>
      </div>
  </div>

  <div class="row">
    <div class="col-md-12">
      <form method="post" action="result_multiple_data_search_araiz.php" name="raza_form" target="_blank">
      
        <div class="form-group col-md-3 col-xs-12">
          <label>Track No.</label>
          <input type="text" name="track_no" class="form-control">
        </div>
        
        <div class="form-group col-md-3 col-xs-12">
          <label>ITS Key</label>
          <input type="text" name="its_id" class="form-control">
        </div>
        
        <div class="form-group col-md-3 col-xs-12">
          <label>Marhala Group</label>
          <select name="marhala" class="form-control">
            <option value="">Select Marhala Group</option>
            <option value="1">Pre-Primary</option>
            <option value="2">Primary</option>
            <option value="3">Std 5 to 7</option>
            <option value="4">Std 8 to 10</option>
            <option value="5">Std 11 to 12</option>
            <option value="6">Graduation</option>
            <option value="7">Post Graduation</option>
            <option value="8">Diploma</option>
          </select>
        </div>
        
        <?php
          $ary_darajah = array('Atfaal', 'Awwala', 'Saniyah', 'Salesa', 'Rabeah', 'Khamesah', 'Sadesah', 'Sabeah', 'Samenah', 'Taseah', 'Asherah', 'Nisaab e Awwal', 'Nisaab e Saani', 'Nisaab e Saalis');
        ?>
        
        <div class="form-group col-md-3 col-xs-12">
          <label>Madrasah Darajah</label>
          <select name="darajah" class="form-control">
              <option value="">Select Darajah</option>
              <?php
              foreach ($ary_darajah as $drj) {
                ?>
                <option value="<?php echo $drj; ?>"><?php echo $drj; ?></option>
                <?php
              }
              ?>
            </select>
        </div>
        
        <div class="form-group col-md-3 col-xs-12">
          <label>Madrasah Name</label>
          <input type="text" name="madrasah_name" class="form-control">
        </div>
        
        <div class="form-group col-md-3 col-xs-12">
          <label>User Full Name</label>
          <input type="text" name="user_full_name" class="form-control">
        </div>
        
        <div class="form-group col-md-3 col-xs-12">
          <label>School City</label>
          <input type="text" name="school_city" class="form-control">
        </div>
        
        <?php
          $ary_standard = array('Pre-school', 'Nursery', 'Kindergarten', 'Junior Kindergarten', 'Senior Kindergarten', '1st', '2nd', '3rd', '4th', '5th', '6th', '7th', '8th', '9th', '10th', '11th', '12th');
        ?>
        
        <div class="form-group col-md-3 col-xs-12">
          <label>School Standard</label>
          <select name="school_standard" class="form-control">
            <option value="">Standard</option>
            <?php
            foreach ($ary_standard as $std) {
              ?>
              <option value="<?php echo $std; ?>"><?php echo $std; ?></option>
              <?php
            }
            ?>
          </select>
        </div>
        
        <div class="form-group col-md-3 col-xs-12">
          <label>School Name</label>
          <input type="text" name="school_name" class="form-control">
        </div>
        
        <?php
          $filter_school_array = array('School run by Dawat e Hadiyah - Boys', 'School run by Dawat e Hadiyah - Girls', 'School run by Dawat e Hadiyah - Co-Ed', 'Private School - Boys', 'Private School - Girls', 'Private School - Co-Ed', 'Public School - Boys', 'Public School - Girls', 'Public School - Co-Ed');
        ?>
        
        <div class="form-group col-md-3 col-xs-12">
          <label>School Filter</label>
          <select name="filter_school" class="form-control">
            <option value="">Is your school</option>
            <?php
              foreach ($filter_school_array as $scl) {
                ?>
                <option value="<?php echo $scl; ?>"><?php echo $scl; ?></option>
                <?php
              }
              ?>
          </select>
        </div>
        
        <div class="form-group col-md-3 col-xs-12">
          <label>From</label>
          <input type="date" name="from_date" class="form-control">
        </div>
        
        <div class="form-group col-md-3 col-xs-12">
          <label>To</label>
          <input type="date" name="to_date" class="form-control">
        </div>
        
        <div class="form-group col-md-3 col-xs-12">
          <label>Stream</label>
          <input type="text" name="stream" class="form-control">
        </div>
        
        <div class="form-group col-md-3 col-xs-12">
          <label>Course Name</label>
          <input type="text" name="course_name" class="form-control">
        </div>
        
        <div class="form-group col-md-3 col-xs-12">
          <label>Institute Name</label>
          <input type="text" name="institute_name" class="form-control">
        </div>
        
        <div class="form-group col-md-3 col-xs-12">
          <label>Institute City</label>
          <input type="text" name="institute_city" class="form-control">
        </div>
        
        <div class="form-group col-md-3 col-xs-12">
          <label>Jamaat</label>
          <input type="text" name="jamaat" class="form-control">
        </div>
        
        <div class="form-group col-md-3 col-xs-12">
          <label>Araz Type</label>
          <select name="araz_type" class="form-control">
            <option value="">Select Araz Type</option>
            <option value="raza">Raza</option>
            <option value="istirshad">Istirshad</option>
          </select>
        </div>
        
        <div class="form-group col-md-3 col-xs-12">
          <label>Araz Status</label>
          <select name="araz_status" class="form-control">
            <option value="">Select Araz Status</option>
            <option value="0">Open</option>
            <option value="1">Close</option>
          </select>
        </div>
        
        <div class="clearfix"></div>
        <div class="col-md-3">
          <label>&nbsp;</label>
          <input type="submit" name="search" value="Search" class="btn btn-block btn-success">
        </div>
        <div class="col-md-3">
          <label>&nbsp;</label>
          <button class="btn btn-block btn-danger" type="reset">Reset</button>
        </div>
      </form>
    </div>
  </div>

<?php
include 'footer.php';
?>         