<?php
require_once '../classes/class.database.php';
include 'classes/class.event.php';
$allowed_roles = array(ROLE_HEAD_OFFICE);
require_once 'session.php';

$title = 'Edit Event';
$description = '';
$keywords = '';
$active_page = "manage_events";

$evnt = new Event();

(isset($_GET['id'])) ? $event_id = $_GET['id'] : '';

if (isset($_REQUEST['video_id'])) {
  $video_id = $_REQUEST['video_id'];
  $video_file = $evnt->get_value_by_id('event_video', $video_id);
  if ($video_file) {
    $video_file_name = SERVER_PATH . 'upload/events/video/' . $video_file[0]['video_url'];
    if (file_exists($video_file_name)) {
      unlink($video_file_name);
    }
    $v_rslt = $evnt->delete_media_by_id('event_video', $video_id);
    if ($v_rslt) {
      $_SESSION[SUCCESS_MESSAGE] = 'Video File deleted Successfully';
    } else {
      $_SESSION[ERROR_MESSAGE] = 'Erors encountered while processing the request';
    }
  }
  header('Location: edit_event.php?id=' . $event_id);
  exit();
}

if (isset($_REQUEST['audio_id'])) {
  $audio_id = $_REQUEST['audio_id'];
  $audio_file = $evnt->get_value_by_id('event_audio', $audio_id);
  if ($audio_file) {
    $audio_file_name = SERVER_PATH . 'upload/events/audio/' . $audio_file[0]['audio_url'];
    if (file_exists($audio_file_name)) {
      unlink($audio_file_name);
    }
    $a_rslt = $evnt->delete_media_by_id('event_audio', $audio_id);
    if ($a_rslt) {
      $_SESSION[SUCCESS_MESSAGE] = 'Audio File deleted Successfully';
    } else {
      $_SESSION[ERROR_MESSAGE] = 'Erors encountered while processing the request';
    }
  }
  header('Location: edit_event.php?id=' . $event_id);
  exit();
}

if (isset($_REQUEST['image_id'])) {
  $image_id = $_REQUEST['image_id'];
  $image_file = $evnt->get_value_by_id('event_image', $image_id);
  if ($image_file) {
    $image_file_name = SERVER_PATH . 'upload/events/image/' . $image_file[0]['image_url'];
    if (file_exists($image_file_name)) {
      unlink($image_file_name);
    }
    $i_rslt = $evnt->delete_media_by_id('event_image', $image_id);
    if ($i_rslt) {
      $_SESSION[SUCCESS_MESSAGE] = 'Image File deleted Successfully';
    } else {
      $_SESSION[ERROR_MESSAGE] = 'Erors encountered while processing the request';
    }
  }
  header('Location: edit_event.php?id=' . $event_id);
  exit();
}

if (isset($_POST['btn_submit'])) {
  $data = $db->clean_data($_POST);
  $media = 0;
  foreach ($_FILES as $files => $fileattributes) {
    if ($_FILES[$files]['size'] != '0' || $media == 1) {
      $media = 1;
    } else {
      $media = 0;
    }
  }
  if ($media == 0 && $data['event_content'] == '') {
    echo ' does not need to insert';
  } else {
    $event_title = $data['event_title'];
    $description = 'nothing';
    $slug = str_replace(' ', '', $data['slug']);
    if ($slug == '') {
      $slug = str_replace(' ', '', $event_title);
    }
    $page_title = $data['page_title'];
    $text = addslashes($_POST['event_content']);
    $youtube_video = $_POST['youtube_video'];
    $user_id = $_SESSION[USER_ID];
    $start_date = $data['start_date'];
    $end_date = $data['end_date'];
    $capacity = $data['capacity'];
    $venue = $data['venue'];
    $contact_person = $data['contact_person'];
    $email = $data['email'];
    $mobile = $data['mobile'];
    $template = $data['template'];
    $master_menu = $data['master_menu'];
    $sub_menu = $data['sub_menu'];
    $active = $data['active'];
    $event_id = $data['event_id'];

    $event_insert = $evnt->edit_event($event_title, $description, $slug, $page_title, $text, $youtube_video, $user_id, $start_date, $end_date, $capacity, $venue, $contact_person, $email, $mobile, $template, $master_menu, $sub_menu, $active, $event_id);

    if ($event_insert) {
      $max_id = $event_id;
      foreach ($_FILES as $files => $fileattributes) {
        if ($_FILES[$files]['size'] != '0') {
          if (strlen($files) > 10) {
            $keyword = substr($files, 0, 11);
          } else {
            $keyword = substr($files, 0, 5);
          }

          $number = substr($files, 5, strlen($files));
          $originalfile = $_FILES[$files]['name'];
          $filearray = explode('.', $originalfile);
          $ext = $filearray[count($filearray) - 1];
          $filename = $max_id . '-' . $originalfile;
          $filetemp = $_FILES[$files]['tmp_name'];
          $index = 1;

          if ($keyword == 'video_thumb') {
            $thumbpathANDname = "../upload/events/video/images/" . $filename;
            $moveResult = move_uploaded_file($filetemp, $thumbpathANDname);

            $update_image = $evnt->update_media($max_id, $filename, $v_thumb_id);
          } else {
            $pathANDname = "../upload/events/" . $keyword . "/" . $filename;
            $moveResult = move_uploaded_file($filetemp, $pathANDname);

            $insert_image = $evnt->insert_media('event_' . $keyword, $max_id, $filename, $index);
          }
          $v_thumb_id = $insert_image;
        }
      }
      $_SESSION[SUCCESS_MESSAGE] = 'Event Successfully Updated.';
    } else {
      $_SESSION[ERROR_MESSAGE] = 'Errors encountered while Updating Data';
    }
  }
}

if (isset($_GET['id'])) {
  $eventsvalue = $evnt->get_value_by_id('events', $event_id);
  $videos = $evnt->get_media_by_id('event_video', $event_id);
  $images = $evnt->get_media_by_id('event_image', $event_id);
  $audios = $evnt->get_media_by_id('event_audio', $event_id);
}

$mastermenus = get_all_menu();
$submenus = get_all_submenu();

include_once("header.php");
?>
<script type="text/javascript" src="ckeditor/ckeditor.js"></script>
<style type="text/css">
  .video_box,.audio_box,.image_box
  {
    border: 1px solid #ccc;
    padding:10px 5px 50px 5px;
  }
  #content_box
  {
    margin-top: 10px;
  }
  .video_link,.audio_link,.image_link
  {
    padding: 5px 0px;
  }
  input[type="file"]
  {
    width: 95px;
    display: inline-block;
  }
  #remove_box_link1,#remove_audio_box_link1,#remove_image_box_link1
  {
    margin-left: 0px !important;
  }
  #btn_submit
  {
    margin-top: 20px;
    padding: 5px;
    width: 80px;
    font-size: 16px;
    font-weight: bold;
    background-color: #5bc0de;
    border-radius: 5px;

  }
</style>

<div class="row">
  <div class="col-lg-12">
    <h1 class="page-header">Edit Event,Videos,Audios, and Images <?php //echo $_SESSION['user_its'];  ?></h1>
  </div>
  <!-- /.col-lg-12 -->
</div>
<div class="row">
  <div class="col-md-12">
    <form method="post" enctype="multipart/form-data">

      <div class="form-group col-md-4 col-xs-12">
        <label for="title">Event Title :</label>
        <input type="text" name="event_title" id="event_title" required class="form-control" value="<?php echo $eventsvalue[0]['event_title']; ?>">
      </div>

      <div class="form-group col-md-4 col-xs-12">
        <label for="title">Slug :</label>
        <input type="text" name="slug" id="slug" required class="form-control" value="<?php echo $eventsvalue[0]['slug']; ?>">
      </div>

      <div class="form-group col-md-4 col-xs-12">
        <label for="title">Page Title :</label>
        <input type="text" name="page_title" id="page_title" required class="form-control" value="<?php echo $eventsvalue[0]['page_title']; ?>">
      </div>

      <div class="form-group col-md-3 col-xs-12">
        <label for="title">Start Date :</label>
        <input type="date" name="start_date" id="start_date" required class="form-control" value="<?php echo date("Y-m-d", strtotime($eventsvalue[0]['start_date'])); ?>">
      </div>

      <div class="form-group col-md-3 col-xs-12">
        <label for="title">End Date :</label>
        <input type="date" name="end_date" id="end_date" required class="form-control" value="<?php echo date("Y-m-d", strtotime($eventsvalue[0]['end_date'])); ?>">
      </div>

      <div class="form-group col-md-3 col-xs-12">
        <label for="title">Capacity :</label>
        <input type="text" name="capacity" id="capacity" required class="form-control" value="<?php echo $eventsvalue[0]['capacity']; ?>">
      </div>
      
      <div class="form-group col-md-3 col-xs-12">
        <label for="title">Venue :</label>
        <input type="text" name="venue" id="venue" required class="form-control" value="<?php echo $eventsvalue[0]['venue']; ?>">
      </div>
      
      <div class="form-group col-md-4 col-xs-12">
        <label for="title">Contact Person :</label>
        <input type="text" name="contact_person" id="contact_person" required class="form-control" value="<?php echo $eventsvalue[0]['contact_person']; ?>">
      </div>
      
      <div class="form-group col-md-4 col-xs-12">
        <label for="title">Email :</label>
        <input type="text" name="email" id="email" required class="form-control" value="<?php echo $eventsvalue[0]['email']; ?>">
      </div>
      
      <div class="form-group col-md-4 col-xs-12">
        <label for="title">Mobile :</label>
        <input type="text" name="mobile" id="mobile" required class="form-control" value="<?php echo $eventsvalue[0]['mobile']; ?>">
      </div>

      <div class="form-group col-md-3 col-xs-12">
        <label for="title">Template :</label>
        <select name="template" id="template" class="form-control">
            <option value="1" <?php
            if ($eventsvalue[0]['templates'] == '1') {
              echo 'selected';
            }
  ?> >Template 1</option>
            <option value="2" <?php
            if ($eventsvalue[0]['templates'] == '2') {
              echo 'selected';
            }
  ?>>Template 2</option>
            <option value="3" <?php
            if ($eventsvalue[0]['templates'] == '3') {
              echo 'selected';
            }
  ?>>Template 3</option>
            <option value="4" <?php
            if ($eventsvalue[0]['templates'] == '4') {
              echo 'selected';
            }
  ?>>Template 4</option>
        </select>
      </div>

      <div class="form-group col-md-3 col-xs-12">
        <label for="title">Master Menu :</label>
        <select name="master_menu" id="master_menu" class="form-control" onChange="show_sub_menu(this.value)">
          <option value="">Select Menu</option>
          <?php foreach($mastermenus as $mastermenu){ ?>
          
              <option value="<?php echo $mastermenu['id']; ?>" <?php if ($mastermenu['id'] == $eventsvalue[0]['menu']) { echo 'selected'; } ?> ><?php echo $mastermenu['name']; ?></option>
              
          <?php } ?>
        </select>
      </div>

      <div class="form-group col-md-3 col-xs-12">
        <label for="title">Sub-Menu : </label>
        <select name="sub_menu" id="sub_menu" class="form-control">
          <option>Select Sub Menu</option>
          <?php foreach ($submenus as $submenu) { ?>

            <option value="<?php echo $submenu['id']; ?>" <?php
            if ($submenu['id'] == $eventsvalue[0]['submenu']) {
              echo 'selected';
            }
            ?> ><?php echo $submenu['name']; ?></option>

          <?php } ?>
        </select>
      </div>

      <div class="form-group col-md-3 col-xs-12">
        <label for="active">Is Active? :</label>
        <select name="active" id="active" class="form-control" required >
          <option value="1" <?php if ($eventsvalue[0]['active'] == '1') echo 'selected'; ?>>Yes</option>
          <option value="0" <?php if ($eventsvalue[0]['active'] == '0') echo 'selected'; ?>>No</option>
        </select>
      </div>

      <div class="content_box" id="content_box">
        <div class="content_link" id="content_link">
          <label>Write Article : </label>
          <textarea name="event_content" class="ckeditor" style="width:100%" ><?php echo $eventsvalue[0]['text']; ?></textarea>
        </div>
      </div>
      
      <div class="content_box" id="content_box">
        <div class="content_link" id="content_link">
          <label>Youtube Video Embed: </label>
          <textarea name="youtube_video" class="form-control" placeholder="Enter Youtube Video Embed Code"><?php echo $eventsvalue[0]['youtube_video']; ?></textarea>
        </div>
      </div>



      <div class="video_box" id="video_box" style="margin-top:20px;">

        <?php
        if ($videos) {
          foreach ($videos as $video) {
            ?>
            <video width="200" height="100" controls>
              <source src="<?php echo SERVER_PATH . 'upload/events/video/' . $video['event_video_url']; ?>">
            </video>
            <a href="edit_event.php?id=<?php echo $event_id; ?>&video_id=<?php echo $video['id']; ?>"><img src="images/remove.png" style="height: 25px;vertical-align: auto;margin-right: 10px;"></a>
  <?php }
} ?>
        <br>
        <div class="video_link" id="video_link">
          <label>Upload Video : </label>
          <input type="text" class="videofilename1" id="videofilename1" readonly="readonly">
          <input type="file" name="video1" id="video1" class="file" onchange="changevideo(this);">
          <label>Upload Thumbnail : </label>
          <input type="text" class="video_thumbnail1" id="video_thumbnail1" readonly="readonly">
          <input type="file" name="video_thumb1" id="video_thumb1" class="file" onchange="changevideo_thumbnail(this);">
        </div>
        <div class="add_box" id="add_box"><a href="javascript:void(0);" id="add_box_link"><img src="images/add.png" alt="" style="height: 25px;"></a></div>
        <input type="hidden" name="videovalue" id="videovalue" value="1">
      </div><!----VIDEO_BOX-->

      <div class="audio_box" id="audio_box">

      <?php if ($audios) {
        foreach ($audios as $audio) {
          ?>
            <audio controls>
              <source src="<?php echo SERVER_PATH . 'upload/events/audio/' . $audio['event_audio_url']; ?>">
            </audio>
            <a href="edit_event.php?id=<?php echo $event_id; ?>&audio_id=<?php echo $audio['id']; ?>"><img src="images/remove.png" alt="" style="height: 25px;vertical-align: auto;margin-right: 10px;"></a>
          <?php
          }
        } 
        ?>
        <br>
        <div class="audio_link" id="audio_link">
          <label>Upload Audio : </label>
          <input type="text" class="audiofilename1" id="audiofilename1" readonly="readonly">
          <input type="file" name="audio1" id="audio1" class="file" onchange="audiochange(this);">
        </div>
        <div class="add_audio_box" id="add_audio_box"><a href="javascript:void(0);" id="add_audio_box_link"><img src="images/add.png" alt="" style="height: 25px;"></a></div>
        <input type="hidden" name="audiovalue" id="audiovalue" value="1"> 
      </div><!----AUDIO_BOX-->

      <div class="image_box" id="image_box">

        <?php if ($images) {
          foreach ($images as $image) {
            ?>
            <img src="<?php echo SERVER_PATH . 'upload/events/image/' . $image['event_image_url']; ?>" height="100" width="100">
            <a href="edit_event.php?id=<?php echo $event_id; ?>&image_id=<?php echo $image['id']; ?>"><img src="images/remove.png" alt="" style="height: 25px;vertical-align: auto;margin-right: 10px;"></a>
  <?php }
} ?>
        <br>
        <div class="image_link" id="image_link">
          <label>Upload Image : </label>
          <input type="text" class="imagefilename1" id="imagefilename1" readonly="readonly">
          <input type="file" name="image1" id="image1" class="file" onchange="imagechange(this);">
        </div>
        <div class="add_image_box" id="add_image_box"><a href="javascript:void(0);" id="add_image_box_link"><img src="images/add.png" alt="" style="height: 25px;"></a></div>
        <input type="hidden" name="imagevalue" id="imagevalue" value="1"> 
      </div><!----IMAGE_BOX-->

      <input type="hidden" name="event_id" value="<?php echo $event_id; ?>">
      <input type="submit" name="btn_submit" id="btn_submit" value="Submit" style="">
    </form>
  </div>
</div>

<script type="text/javascript">
  /////////////////////////////////////////FOR VIDEO START/////////////////////////////////////////////////////     
  $(document).ready(function() {
    var videocount = $('.video_link').length;
    var audiocount = $('.audio_link').length;
    var imagecount = $('.image_link').length;
    if (videocount == 1)
    {
      $('.video_link').css('float', 'left');
      $('#add_box').css('float', 'left');
    }
    if (audiocount == 1)
    {
      $('.audio_link').css('float', 'left');
      $('#add_audio_box').css('float', 'left');
    }
    if (imagecount == 1)
    {
      $('.image_link').css('float', 'left');
      $('#add_image_box').css('float', 'left');
    }

  });

  $(document).ready(function() {

    $("#add_box").click(function() {
      /*REMOVE ALL THINGS*/

      var videovalue = parseInt($('#videovalue').val()) + 1;
      $('#videovalue').val(videovalue);

      $('#video_box div.video_link').removeClass('lastchild');

      //ADD NEW BOX AND SET CSS
      $('#add_box').before('<div class="video_link" id="video_link"><label> Upload Video : </label><input type="text" class="videofilename' + videovalue + '" id="videofilename' + videovalue + '" readonly="readonly" style="margin-left:4px;"><input type="file" name="video' + videovalue + '" id="video' + videovalue + '" class="file" onchange="changevideo(this);" style="margin-left:4px;"><label> Upload Thumbnai : </label><input type="text" class="video_thumbnail' + videovalue + '" id="video_thumbnail' + videovalue + '" readonly="readonly" style="margin-left:4px;"><input type="file" name="video_thumb' + videovalue + '" id="video_thumb' + videovalue + '" class="file" onchange="changevideo_thumbnail(this);" style="margin-left:4px;"></div>');
      var count = $('.video_link').length;
      if (count > 1)
      {
        $('.video_link').css('float', '');
        $('#video_box div.video_link:last').css('float', 'left');
        $('#video_box div.video_link:last').addClass('lastchild');

        //APPEND ONLY IF NOT THERE
        $(".video_link").each(function(index) {
          if ($(this).find("a").length)
          {
          }
          else
          {
            var rvideovalue = videovalue - 1;
            $(this).append('<a href="javascript:void(0);" id="remove_box_link' + rvideovalue + '" onclick="return removevideo(this);" style="margin-left:4px;"><img src="images/remove.png" alt="" style="height: 15px;"></a>');
          }
        });
        //REMOVE REMOVE BTN FROM LAST CHILD
        if ($('#video_box div.video_link').hasClass('lastchild'))
        {
          $("#video_box div.video_link:last").children("a:first").remove();
        }
      }
    });//END ADD CLICK
  });
  
  function changevideo(elem)
  {
    var id = $(elem).attr("id");
    var file = $('#' + id).val();
    var filearray = file.split('.');
    var arraysize = filearray.length;
    var filetype = filearray[arraysize - 1].toLowerCase();
    if (filetype == '3gp' || filetype == 'wmv' || filetype == 'mp4' || filetype == 'avi' || filetype == 'mpeg')
    {
      var lastChar = id.substr(id.length - 1);
      //alert(lastChar);
      $('#videofilename' + lastChar).val(file);
    }
    else
    {
      //alert(id);
      $('#' + id).val('');
      alert('Invalid File Type, Please Select Video Only');
    }

  }
  
  function changevideo_thumbnail(elem)
  {
    var id = $(elem).attr("id");
    //alert(id.val);
    var file = $('#' + id).val();


    var filearray = file.split('.');
    var arraysize = filearray.length;
    var filetype = filearray[arraysize - 1].toLowerCase();
    if (filetype == 'jpg' || filetype == 'png' || filetype == 'jpeg' || filetype == 'gif' || filetype == 'tif' || filetype == 'raw' || filetype == 'bmp')
    {
      var lastChar = id.substr(id.length - 1);
      //alert(lastChar);
      $('#video_thumbnail' + lastChar).val(file);
    }
    else
    {
      $('#' + id).val('');
      alert('Invalid File Type, Please Select Image File Only');
    }
  }
  
  function removevideo(elem)
  {
    if ($(".video_link").length == 2)
    {
      //alert($( ".video_link" ).length);
      //SET HVALUE AS 1
    }
    var id = $(elem).attr("id");
    $('#' + id).parent().remove();
  }
/////////////////////////////////////////FOR VIDEO END/////////////////////////////////////////////////////     
  $(document).ready(function() {
    $('#add_audio_box').click(function() {
      var audiovalue = parseInt($('#audiovalue').val()) + 1;
      $('#audiovalue').val(audiovalue);
      $('#audio_box div.audio_link').removeClass('lastchild');
      //APPEND THE CODE
      $('#add_audio_box').before('<div class="audio_link" id="audio_link"><label>Upload Audio : </label><input type="text" style="margin-left:4px;" class="audiofilename' + audiovalue + '" id="audiofilename' + audiovalue + '" readonly="readonly"><input type="file" name="audio' + audiovalue + '" id="audio' + audiovalue + '" class="file" onchange="audiochange(this);" style="margin-left:4px;"></div>');

      var count = $('.audio_link').length;
      if (count > 1)
      {
        $('.audio_link').css('float', '');
        $('#audio_box div.audio_link:last').css('float', 'left');
        $('#audio_box div.audio_link:last').addClass('lastchild');

        //APPEND ONLY IF NOT THERE
        $(".audio_link").each(function(index) {
          if ($(this).find("a").length)
          {
          }
          else
          {
            var raudiovalue = audiovalue - 1;
            $(this).append('<a href="javascript:void(0);" id="remove_audio_box_link' + raudiovalue + '" onclick="return removeaudio(this);" style="margin-left:4px;"><img src="images/remove.png" alt="" style="height: 15px;"></a>');
          }
        });
        //REMOVE REMOVE ADD BTN FROM LAST CHILD
        if ($('#audio_box div.audio_link').hasClass('lastchild'))
        {
          $("#audio_box div.audio_link:last").children("a:first").remove();
        }
      }
    });
  });
  function audiochange(elem)
  {
    var id = $(elem).attr("id");
    //alert(id.val);
    var file = $('#' + id).val();

    var filearray = file.split('.');
    var arraysize = filearray.length;
    var filetype = filearray[arraysize - 1].toLowerCase();
    if (filetype == 'mp3' || filetype == '3ga' || filetype == 'wav')
    {
      var lastChar = id.substr(id.length - 1);
      //alert(lastChar);
      $('#audiofilename' + lastChar).val(file);

    }
    else
    {
      //alert(id);
      $('#' + id).val('');
      alert('Invalid File Type, Please Select Audio File Only');
    }

  }
  function removeaudio(elem)
  {
    if ($(".audio_link").length == 2)
    {
      //alert($( ".audio_link" ).length);
      //SET HVALUE AS 1
    }
    var id = $(elem).attr("id");
    $('#' + id).parent().remove();
  }
////////////////////////////////////////////////////////////////////////AUDIO END/////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////IMAGE START////////////////////////////////////////////////////////////
  $(document).ready(function() {
    $('#add_image_box').click(function() {
      var imagevalue = parseInt($('#imagevalue').val()) + 1;
      $('#imagevalue').val(imagevalue);
      $('#image_box div.image_link').removeClass('lastchild');
      //APPEND THE CODE
      $('#add_image_box').before('<div class="image_link" id="image_link"><label>Upload Image : </label><input type="text" class="imagefilename' + imagevalue + '" id="imagefilename' + imagevalue + '" readonly="readonly" style="margin-left:4px;"><input type="file" name="image' + imagevalue + '" id="image' + imagevalue + '" class="file" onchange="imagechange(this);" style="margin-left:4px;"></div>');

      var count = $('.image_link').length;
      if (count > 1)
      {
        $('.image_link').css('float', '');
        $('#image_box div.image_link:last').css('float', 'left');
        $('#image_box div.image_link:last').addClass('lastchild');

        //APPEND ONLY IF NOT THERE
        $(".image_link").each(function(index) {
          if ($(this).find("a").length)
          {
          }
          else
          {
            var rimagevalue = imagevalue - 1;
            $(this).append('<a href="javascript:void(0);" id="remove_image_box_link' + rimagevalue + '" onclick="return removeimage(this);" style="margin-left:4px;"><img src="images/remove.png" alt="" style="height: 15px;"></a>');
          }
        });
        //REMOVE REMOVE ADD BTN FROM LAST CHILD
        if ($('#image_box div.image_link').hasClass('lastchild'))
        {
          $("#image_box div.image_link:last").children("a:first").remove();
        }
      }
    });
  });
  function imagechange(elem)
  {
    var id = $(elem).attr("id");
    //alert(id.val);
    var file = $('#' + id).val();


    var filearray = file.split('.');
    var arraysize = filearray.length;
    var filetype = filearray[arraysize - 1].toLowerCase();
    if (filetype == 'jpg' || filetype == 'png' || filetype == 'jpeg' || filetype == 'gif' || filetype == 'tif' || filetype == 'raw' || filetype == 'bmp')
    {
      var lastChar = id.substr(id.length - 1);
      //alert(lastChar);
      $('#imagefilename' + lastChar).val(file);
    }
    else
    {
      $('#' + id).val('');
      alert('Invalid File Type, Please Select Image File Only');
    }
  }
  function removeimage(elem)
  {
    if ($(".image_link").length == 2)
    {
      //alert($( ".image_link" ).length);
      //SET HVALUE AS 1
    }
    var id = $(elem).attr("id");
    $('#' + id).parent().remove();
  }

//DATE VALIDATION
  function datevalidation()
  {

    var startDate = new Date($('#start_date').val());
    var endDate = new Date($('#end_date').val());

    if (startDate < endDate)
    {
      // Do something
    }
    else
    {
      alert('End Date Must be Greater');
      $('#end_date').val('');
    }
  }
  
  function show_sub_menu(str)
  {
    if (str == "")
    {
      document.getElementById("sub_menu").innerHTML = "";
      document.getElementById("mname").innerHTML = "";
      return;
    }

    if (window.XMLHttpRequest)
    {// code for IE7+, Firefox, Chrome, Opera, Safari
      xmlhttp = new XMLHttpRequest();
    }
    else
    {// code for IE6, IE5
      xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
    }



    xmlhttp.onreadystatechange = function()
    {
      if (xmlhttp.readyState == 4 && xmlhttp.status == 200)
      {
        document.getElementById("sub_menu").innerHTML = xmlhttp.responseText;
      }
    }
    xmlhttp.open("GET", "get_data_by_ajax.php?menu=" + str, true);
    xmlhttp.send();
  }
</script>
<!-- /#page-wrapper -->
<?php include "./footer.php"; ?>
