<?php
include 'classes/class.crs_course.php';
include 'classes/class.crs_chapter.php';
$allowed_roles = array(ROLE_HEAD_OFFICE);
require_once 'session.php';

$crs_chapter = new Mtx_Crs_Chapter();
$crs_course = new Mtx_Crs_Course();

$title = 'Chapters List';
$description = '';
$keywords = '';
$active_page = "list_chapters";

$element_array = array(CRS_ELEMENT_AUDIO => 'audio', CRS_ELEMENT_IMAGE => 'image', CRS_ELEMENT_FILE => 'file');
$field_array = array(CRS_ELEMENT_VIDEO => 'element_youtube', CRS_ELEMENT_AUDIO => 'element_audio', CRS_ELEMENT_IMAGE => 'element_image', CRS_ELEMENT_FILE => 'element_file', CRS_ELEMENT_OPEN_HTML => 'element_open_html', CRS_ELEMENT_HTML_TEXT => 'element_html_text');

$result = $id = FALSE;

if (isset($_REQUEST['id'])) {
  $id = $_REQUEST['id'];
  $result = $crs_chapter->get_chapter($id);
}

if (isset($_POST['btn_update'])) {
  $data = $db->clean_data($_POST);
  // get variables
  $course_id = $data['course_id'];
  $title = $data['title'];
  $slug = $db->clean_slug($_POST['slug']);
  $active = $data['active'];
  $sort = $data['sort'];
  $description = $data['description'];
  $start_date = $data['start_date'];
  $end_date = $data['end_date'];
  $user_id = $_SESSION[USER_ID];
  $last_insert_id = $id;
  
  if($_FILES['img_icon']['name'] != ''){
    $icon_image = $_FILES['img_icon']['name'];
    $icon_image_temp = $_FILES['img_icon']['tmp_name'];
    
    $filearray = explode('.', $icon_image);
    $ext = $filearray[count($filearray)-1];
              
    $icon_image_name = $last_insert_id.'.'.$ext;
    $imagepathANDname = "../upload/courses/chapter/" . $icon_image_name;
    move_uploaded_file($icon_image_temp, $imagepathANDname);
  }else {
    $icon_image_name = $data['last_img_icon'];
  }

  $update = $crs_chapter->update_chapter($course_id, $title, $slug, $description, $icon_image_name, $start_date, $end_date, $sort, $active, $id);

  if ($update) {
    $element_title = $data['element_title'];
    $element_type = $data['element_type'];
    $element_sort = $data['element_sort'];
    $element_desc = $data['element_desc'];
    $element_active = $data['element_active'];
    
    if ($element_type != CRS_ELEMENT_VIDEO AND $element_type != CRS_ELEMENT_OPEN_HTML AND $element_type != CRS_ELEMENT_HTML_TEXT) {
      
      $element_text = $_FILES[$field_array[$element_type]]['name'];
      $element_text_temp = $_FILES[$field_array[$element_type]]['tmp_name'];
      
      $filearray = explode('.', $element_text);
      $elm_ext = $filearray[count($filearray)-1];

      $element_text = $last_insert_id.'.'.$elm_ext;
      
      $elementpathANDname = "../upload/courses/chapter/".$element_array[$element_type].'/'.$element_text;
      move_uploaded_file($element_text_temp, $elementpathANDname);
    
    } else if ($element_type == CRS_ELEMENT_HTML_TEXT) {
      $element_text = addslashes($data[$field_array[$element_type]]);
    } else {
      $element_text = htmlentities($data[$field_array[$element_type]]);
    }
    
    $insert_elements = $crs_chapter->insert_elements($id, $element_type, $element_title, $element_text, $element_desc, $element_sort, $user_id, $element_active);
    
    $_SESSION[SUCCESS_MESSAGE] = 'Chapter Updated Successfully.';
    header('Location: crs_chapter_update.php?id='.$id);
    exit();
  } else {
    $_SESSION[ERROR_MESSAGE] = 'Error encountered while Updating Chapter';
  }
}

$list_chapters = $crs_chapter->get_all_chapters();
$list_courses = $crs_course->get_all_courses();

include_once("header.php");
?>
<link rel="stylesheet" href="js/select2/select2.min.css">
<script src="js/select2/select2.full.min.js"></script>
<script type="text/javascript" src="ckeditor/ckeditor.js"></script>

<div class="row">
  <div class="col-lg-12">
    <h3 class="page-header">Update Chapter</h3>
  </div>
  <!-- /.col-lg-12 -->
</div>

<div class="row">
  <div class="col-md-12">
    <form method="post" enctype="multipart/form-data">
      
      <div class="form-group col-md-4 col-xs-12">
        <label for="title">Course :</label>
        <select name="course_id" id="course_id" class="form-control course_id">
          <option value="">Select</option>
          <?php
          if ($list_courses) {
            foreach ($list_courses as $lc) {
              $selected = ($lc['id'] == $result['course_id']) ? 'selected' : '';
          ?>
            <option value="<?php echo $lc['id']; ?>" <?php echo $selected; ?>><?php echo $lc['title']; ?></option>
          <?php
            }
          }
          ?>
        </select>
      </div>

      <div class="form-group col-md-4 col-xs-12">
        <label for="title">Title :</label>
        <input type="text" name="title" id="title" required class="form-control" value="<?php echo $result['title']; ?>">
      </div>
      
      <div class="form-group col-md-4 col-xs-12">
        <label for="slug">Slug :</label>
        <input type="text" name="slug" id="slug" required class="form-control" value="<?php echo $result['slug']; ?>">
      </div>

      <div class="form-group col-md-4 col-xs-12">
        <label for="slug">Image icon :</label>
        <input type="file" name="img_icon" class="file">
        <?php
          if ($result['img_icon'] != '') {
        ?>
          <div class="clearfix">&nbsp;</div>
          <p><img src="../upload/courses/chapter/<?php echo $result['img_icon']; ?>" width="100" height="100" /></p>
          <input type="hidden" name="last_img_icon" value="<?php echo $result['img_icon']; ?>">
        <?php
          }
        ?>
      </div>

      <div class="form-group col-md-4 col-xs-12">
        <label for="active">Is Active? :</label>
        <select name="active" id="active" class="form-control" required >
          <option value="">Select</option>
          <option value="1" <?php echo ($result['is_active'] == '1') ? 'selected' : ''; ?>>Yes</option>
          <option value="0" <?php echo ($result['is_active'] == '0') ? 'selected' : ''; ?>>No</option>
        </select>
      </div>
      
      <div class="form-group col-md-4 col-xs-12">
        <label for="sort">Sort :</label>
        <input type="text" name="sort" id="sort" required class="form-control" value="<?php echo $result['sort_id']; ?>">
      </div>
      
      <div class="form-group col-md-4 col-xs-12">
        <label for="slug">Start Date :</label>
        <input type="date" name="start_date" class="form-control" value="<?php echo date('Y-m-d'); ?>">
      </div>
      
      <div class="form-group col-md-4 col-xs-12">
        <label for="slug">End Date :</label>
        <input type="date" name="end_date" class="form-control" value="<?php echo date('Y-m-d'); ?>">
      </div>
      
      <div class="form-group col-md-12 col-xs-12">
        <label for="miqat">Description :</label>
        <textarea name="description" class="form-control"><?php echo $result['description']; ?></textarea>
      </div>
      
      <div class="form-group col-md-4 col-xs-12">
        <label for="element_type">Element Type :</label>
        <select name="element_type" id="element_type" class="form-control element_type" required >
          <option value="">Select</option>
          <option value="1">Youtube Embed Code</option>
          <option value="2">Audio</option>
          <option value="3">Image</option>
          <option value="4">File</option>
          <option value="5">Open HTML</option>
          <option value="6">HTML Text</option>
        </select>
      </div>
      
      <div class="clearfix"></div>
      <div class="row col-md-12 col-xs-12 crs_elements" style="display: none;">
        <div class="form-group col-md-4 col-xs-12">
          <label for="element_title">Title</label>
          <input type="text" name="element_title" id="element_title" class="form-control">
        </div>
        <div class="form-group col-md-4 col-xs-12">
          <label for="element_active">Is Active? :</label>
          <select name="element_active" id="element_active" class="form-control">
            <option value="">Select</option>
            <option value="1">Yes</option>
            <option value="0">No</option>
          </select>
        </div>
        <div class="form-group col-md-4 col-xs-12">
          <label for="element_active">Sort</label>
          <input type="text" name="element_sort" id="element_sort" class="form-control">
        </div>
        <div class="form-group col-md-6 col-xs-12">
          <label for="element_desc">Description</label>
          <textarea type="text" name="element_desc" id="element_desc" class="form-control"></textarea>
        </div>
        
        <div class="form-group col-md-6 col-xs-12 crs_youtube">
          <label for="element_youtube">Youtube Embed Code</label>
          <textarea type="text" name="element_youtube" id="element_youtube" class="form-control"></textarea>
        </div>
        <div class="form-group col-md-6 col-xs-12 crs_audio">
          <label for="element_audio">Audio</label>
          <input type="file" name="element_audio" id="element_audio" class="file">
        </div>
        <div class="form-group col-md-6 col-xs-12 crs_image">
          <label for="element_image">Image</label>
          <input type="file" name="element_image" id="element_image" class="file">
        </div>
        <div class="form-group col-md-6 col-xs-12 crs_file">
          <label for="element_file">File</label>
          <input type="file" name="element_file" id="element_file" class="file">
        </div>
        <div class="form-group col-md-6 col-xs-12 crs_open_html">
          <label for="element_open_html">Open Html</label>
          <textarea type="text" name="element_open_html" id="element_open_html" class="form-control"></textarea>
        </div>
        
        <div class="clearfix"></div>
        <div class="form-group col-md-12 col-xs-12 crs_html_text">
          <label for="element_html_text">Html Text</label>
          <textarea type="text" name="element_html_text" id="element_html_text" class="form-control ckeditor"></textarea>
        </div>
      </div>
      
      <div class="clearfix"></div>
      <div class="form-group col-md-2 col-xs-12">
        <input type="submit" name="btn_update" id="btn_update" value="Submit" class="btn btn-primary btn-block form-control">
      </div>
      
    </form>
  </div>
</div>

<script type="text/javascript">
  $('#element_type').on('click', function() {
    var element_type = $('#element_type').val();
    
    if(element_type != '') {
      $('.crs_elements').show(600);
      
      if(element_type == '<?php echo CRS_ELEMENT_VIDEO; ?>'){
        $('.crs_youtube').show(600);
        $('.crs_audio').hide(600);
        $('.crs_image').hide(600);
        $('.crs_file').hide(600);
        $('.crs_open_html').hide(600);
        $('.crs_html_text').hide(600);
      } else if(element_type == '<?php echo CRS_ELEMENT_AUDIO; ?>') {
        $('.crs_youtube').hide(600);
        $('.crs_audio').show(600);
        $('.crs_image').hide(600);
        $('.crs_file').hide(600);
        $('.crs_open_html').hide(600);
        $('.crs_html_text').hide(600);
      } else if(element_type == '<?php echo CRS_ELEMENT_IMAGE; ?>') {
        $('.crs_youtube').hide(600);
        $('.crs_audio').hide(600);
        $('.crs_image').show(600);
        $('.crs_file').hide(600);
        $('.crs_open_html').hide(600);
        $('.crs_html_text').hide(600);
      } else if(element_type == '<?php echo CRS_ELEMENT_FILE; ?>') {
        $('.crs_youtube').hide(600);
        $('.crs_audio').hide(600);
        $('.crs_image').hide(600);
        $('.crs_file').show(600);
        $('.crs_open_html').hide(600);
        $('.crs_html_text').hide(600);
      } else if(element_type == '<?php echo CRS_ELEMENT_OPEN_HTML; ?>') {
        $('.crs_youtube').hide(600);
        $('.crs_audio').hide(600);
        $('.crs_image').hide(600);
        $('.crs_file').hide(600);
        $('.crs_open_html').show(600);
        $('.crs_html_text').hide(600);
      } else if(element_type == '<?php echo CRS_ELEMENT_HTML_TEXT; ?>') {
        $('.crs_youtube').hide(600);
        $('.crs_audio').hide(600);
        $('.crs_image').hide(600);
        $('.crs_file').hide(600);
        $('.crs_open_html').hide(600);
        $('.crs_html_text').show(600);
      }
    } else {
       $('.crs_elements').hide(600);
    }
    
  });
</script>
<script type="text/javascript" src="js/course_details.js?v=1"></script>

<!-- /#page-wrapper -->
<?php include "./footer.php"; ?>
