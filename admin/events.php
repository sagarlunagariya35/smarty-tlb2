<?php
require_once '../classes/constants.php';
require_once '../classes/class.database.php';
require_once 'classes/class.user.admin.php';
require_once 'classes/class.event.php';

$allowed_roles = array(ROLE_HEAD_OFFICE);
require_once 'session.php';

// Get list of articles
$evnt = new Event();

if (isset($_POST['delete_id'])) {
  $delete_id = $_POST['delete_id'];
  $result = $evnt->delete_event($delete_id);

  if ($result) {
    $_SESSION[SUCCESS_MESSAGE] = "Event Successfully Delete";
    header('Location: events.php');
    exit();
  } else {
    $_SESSION[ERROR_MESSAGE] = "Error In Delete";
  }
}

$list_events = $evnt->get_all_events();

$title = 'Events';
$description = '';
$keywords = '';
$active_page = "manage_events";

include_once("header.php");

?>
<style type="text/css">
  .skip{
  display:block;
  width:150px;
  border-radius: 10px;
  -webkit-transition: 0.4s;
  -moz-transition: 0.4s;
  -o-transition: 0.4s;
  transition: 0.4s;
  border: none;
  background-color: #009be3;
  height: auto;
  line-height:20px;
  margin:0 auto 20px;
  padding:10px;
  outline: none;
  color:#222;
  text-transform:uppercase;
  text-align:center;
  font-weight:bold;
  font-size:12px;
  float:right;
  text-decoration: none;
  }
</style>

  <div class="row">
    <div class="col-lg-12 col-xs-12 col-sm-12  pull-left">
      <div class="col-md-12">&nbsp;</div>
      <div class="clearfix"></div>
      <div class="panel panel-green">
        <div class="panel-heading">
          <i class="fa fa-group fa-fw"></i> Manage Events
          <span class="pull-right"><a href="add_event.php" style="color: white;">Add New Event</a></span>
        </div>
        <!-- /.panel-heading -->
        <div class="panel-body">
          <div class="table-responsive">
            <table class="table table-striped table-hover table-condensed">
              <thead>
                <tr>
                  <th>No</th>
                  <th>Event Title</th>
                  <th>Event Path</th>
                  <th>Action</th>
                </tr>
              </thead>
              <tbody>
                <?php
                $i = 1;
                if($list_events){
                  foreach ($list_events as $event) {
                    $menu = get_menu_by_id($event['menu']);
                    $submenu = get_menu_by_id($event['submenu']);
                ?>
                    <tr>
                      <td><?php echo $i++; ?></td>
                      <td><?php echo $event['event_title']; ?></td>
                      <td><?php echo $menu[0]['name'].'/'.$submenu[0]['name']; ?></td>
                      <td>
                        <a href="edit_event.php?id=<?php echo $event['id']; ?>"><i class="fa fa-edit fa-fw"></i></a>
                      <a href="#" class="confirm" id="<?php echo $event['id']; ?>"><i class="fa fa-remove fa-fw text-danger"></i></a>
                      </td>
                    </tr>
                    <?php
                  }
                }else {
                  echo '<tr><td class="text-center" colspan="4">No Records..</td></tr>';
                }
                ?>
              </tbody>
            </table>
          </div>
          <!-- /.table-responsive -->
        </div>
        <!-- /.panel-body -->
      </div>
    </div>
    <form method="post" id="frm-del-grp">
      <input type="hidden" name="delete_id" id="del-event-val" value="">
      <input type="hidden" name="cmd" value="del-event">
    </form>
  </div><!-- ./ row -->
  
  <script>
    $(".confirm").confirm({
      text: "Are you sure you want to delete?",
      title: "Confirmation required",
      confirm: function(button) {
        $('#del-event-val').val($(button).attr('id'));
        $('#frm-del-grp').submit();
        alert('Are you Sure You want to delete: ' + id);
      },
      cancel: function(button) {
        // nothing to do
      },
      confirmButton: "Yes",
      cancelButton: "No",
      post: true,
      confirmButtonClass: "btn-danger"
    });
  </script>
  
<?php include "./footer.php"; ?>
