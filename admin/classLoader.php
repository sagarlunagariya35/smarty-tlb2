<?php
require_once '../classes/class.database.php';
function classLoader ($className) {
  $className = strtolower($className);
  return require_once "../classes/class.$className.php";
}

spl_autoload_register('classLoader');
?>