<?php
require_once 'classes/class.user.admin.php';
require_once '../classes/class.user.php';
include 'classes/class.araiz.php';
$allowed_roles = array(ROLE_HEAD_OFFICE, ROLE_AAMIL_SAHEB, ROLE_JAMIAT_COORDINATOR);
require_once 'session.php';

$user = new mtx_user_admin;
$araiz = new Araiz();

$title = 'Pending Araiz at Counselor';
$description = '';
$keywords = '';
$active_page = "pending_araiz_at_saheb";

$select = FALSE;
$from_date = $to_date = $pagination = $marhala = FALSE;
$araz_saheb_id = $saheb_id = $araz_coun_id = $coun_id = FALSE;
$counselor_days_past = $saheb_days_past = $jamiyet_data = FALSE;

$jamiyet = unserialize($_SESSION['jamiat']);
if (count($jamiyet) == 1 && $jamiyet[0] != '') {
  $jamiyet_data = implode("','", $jamiyet[0]);
}

// Fix for ROLE_JAMIAT_COORDINATOR is no jamiat assign
if ($_SESSION[USER_ROLE] == ROLE_JAMIAT_COORDINATOR && $jamiyet_data === FALSE) {
  $jamiyet_data = ' ';
}

if(isset($_POST['print_list_raza']))
{
  $_SESSION['track_id'] = (array) $_POST['jawab'];
  header('location: print_raza_araiz_list_date_wise.php');
}

if(isset($_POST['print_list_istirshad']))
{
  $_SESSION['track_id'] = (array) $_POST['jawab'];
  header('location: print_istirshad_araiz_date_wise.php');
}

if (isset($_POST['delete_id'])) {
  $delete_id = $_POST['delete_id'];
  $result = $araiz->delete_araz($delete_id);

  if ($result) {
    $_SESSION[SUCCESS_MESSAGE] = "Araz Successfully Delete";
  } else {
    $_SESSION[ERROR_MESSAGE] = "Error while Delete";
  }
}

if(isset($_POST['clear_assign_raza']))
{
  $remove_araz_id = (array) $_POST['jawab'];
  $result = $araiz->clear_assign_saheb_data($remove_araz_id);

  if ($result) {
    $_SESSION[SUCCESS_MESSAGE] = "Assign Data Cleared Successfully";
  } else {
    $_SESSION[ERROR_MESSAGE] = "Error while Clearing";
  }
}

if(isset($_POST['clear_assign_istirshad']))
{
  $remove_araz_id = (array) $_POST['jawab'];
  $result = $araiz->clear_assign_saheb_data($remove_araz_id);

  if ($result) {
    $_SESSION[SUCCESS_MESSAGE] = "Assign Data Cleared Successfully";
  } else {
    $_SESSION[ERROR_MESSAGE] = "Error while Clearing";
  }
}

$from_date = $_REQUEST['from_date'];
$to_date = $_REQUEST['to_date'];
$marhala = $_REQUEST['marhala'];

include ('header.php');

if($from_date OR $to_date OR $marhala){
  $select = $araiz->get_pending_araiz_of_saheb($from_date, $to_date, $marhala, FALSE, FALSE, $page, 10, $jamiyet_data);
  $total_araz = $araiz->get_pending_araiz_of_saheb_count($from_date, $to_date, $marhala, FALSE, FALSE, $jamiyet_data);
}else {
  $select = $araiz->get_pending_araiz_of_saheb(FALSE, FALSE, FALSE, FALSE, FALSE, $page, 10, $jamiyet_data);
  $total_araz = $araiz->get_pending_araiz_of_saheb_count(FALSE, FALSE, FALSE, FALSE, FALSE, $jamiyet_data);
}

$araiz_parsed_data = $araiz->get_araiz_data_organised($select);

$jawab_sent_content = JAWAB_SENT_CONTENT;
?>

<link rel="stylesheet" href="js/tablesorter/css/theme.bootstrap.css">
<script src="js/tablesorter/js/jquery.tablesorter.js"></script>
<script src="js/tablesorter/js/jquery.tablesorter.widgets.js"></script>
<style>
  .alfatemi-text{
    font-size: 18px;
  }
</style>
<div class="row">

  <div class="row">
    <div class="col-xs-12 col-sm-12">&nbsp;</div>
  </div>
  <div class="row">
    <div class="col-xs-12 col-sm-12">
      <h2 class="text-center">Total Pending Saheb Araiz Count: <?php echo $total_araz; ?></h2>
    </div>
  </div>
  <div class="row">
    <div class="col-xs-12 col-sm-12">&nbsp;</div>
  </div>
  <form name="raza_form" class="form" method="post" action="">
    <div class="row">
      <div class="form-group col-xs-12 col-sm-1">
        <label for="from_date">From</label>
      </div>
      <div class="form-group col-xs-12 col-sm-3">
        <input type="date" name="from_date" class="form-control" value="<?php echo $from_date; ?>">
      </div>
      <div class="form-group col-xs-12 col-sm-1">
        <label for="to_date">To</label>
      </div>
      <div class="form-group col-xs-12 col-sm-3">
        <input type="date" name="to_date" class="form-control" value="<?php echo $to_date; ?>">
      </div>
      <div class="form-group col-sm-2 col-xs-12">
        <select name="marhala" class="form-control">
          <option value="">Select Marhala</option>
          <option value="1" <?php if($marhala == '1'){ echo 'selected'; } ?>>Pre-Primary</option>
          <option value="2" <?php if($marhala == '2'){ echo 'selected'; } ?>>Primary</option>
          <option value="3" <?php if($marhala == '3'){ echo 'selected'; } ?>>Std 5 to 7</option>
          <option value="4" <?php if($marhala == '4'){ echo 'selected'; } ?>>Std 8 to 10</option>
          <option value="5" <?php if($marhala == '5'){ echo 'selected'; } ?>>Std 11 to 12</option>
          <option value="6" <?php if($marhala == '6'){ echo 'selected'; } ?>>Graduation</option>
          <option value="7" <?php if($marhala == '7'){ echo 'selected'; } ?>>Post Graduation</option>
          <option value="8" <?php if($marhala == '8'){ echo 'selected'; } ?>>Diploma</option>
        </select>
      </div>
      <div class="col-xs-12 col-sm-2">
        <input type="submit" name="search" value="Search" class="btn btn-block btn-success">
      </div>
    </div>
    <div class="row">
      <div class="col-xs-12">&nbsp;</div>
    </div>
    <div class="row">
      <div class="col-xs-12 col-sm-3" style="padding-top:6px">
      </div>
      <div class="col-xs-12 col-sm-3">
      </div>
      <div class="col-xs-12 col-sm-3">
        <select name="jawab_city" class="form-control jawab_city" id="jawab_city">
          <option value="">Select City</option>
          <?php
          if ($jamaats) {
            foreach ($jamaats as $data) {
              ?>
              <option value="<?php echo $data['jamaat']; ?>"><?php echo $data['jamaat']; ?></option>
              <?php
            }
          }
          ?>
        </select>
      </div>
      <div class="col-xs-12 col-sm-3">
        <input type="submit" name="sent_jawab" id="sent_jawab" value="Send Jawab" class="btn btn-success btn-block assign-jawab">
      </div>
    </div>
  
  <div class="col-lg-12 col-xs-12 col-sm-12 pull-left">
    <div class="col-md-12">&nbsp;</div>
    <div class="clearfix"></div>
    <div class="row">
      <ul class="nav nav-tabs nav-justified" role="tablist">
        <li role="presentation" class="active"><a href="#raza_araiz" aria-controls="pre" role="tab" data-toggle="tab">Raza Araiz</a></li>
        <li role="presentation"><a href="#istirshad_araiz" aria-controls="in" role="tab" data-toggle="tab">Isttirshad Araiz</a></li>
      </ul>
      <div class="row">
        <div class="col-xs-12 col-sm-12">&nbsp;</div>
      </div>
      <?php
        require_once 'pagination.php';
        $pagination = pagination(10, $page, 'pending_araiz_saheb.php?from_date='.$from_date.'&to_date='.$to_date.'&marhala='.$marhala.'&page=', $total_araz);
      ?>
      <div class="tab-content">
        <div role="tabpanel" class="tab-pane active" id="raza_araiz">
          <div class="col-xs-12 col-sm-3" style="padding-top:6px">
            <input type="checkbox" name="print" value="Print" id="check_all_raza" onchange="checkAllRaza(this);" class="css-checkbox1"><label for="check_all_raza" class="css-label1">Check All</label>
          </div>
          <div class="col-xs-12 col-sm-3 pull-right">
            <input type="submit" name="clear_assign_raza" value="Remove Assign Data" class="btn btn-success btn-block">
          </div>
          <div class="col-xs-12 col-sm-3 pull-right">
            <input type="submit" name="print_list_raza" value="Print" class="btn btn-success btn-block assign-print">
          </div>
          <div class="row">
            <div class="col-xs-12 col-sm-12">&nbsp;</div>
          </div>
          <?php
          foreach ($araiz_parsed_data as $araz_id => $araz) {
            // Create bins for User Data, Araz Data, Previous Studies, Current Studeis, Remarks
            $user_details = $araz['user_data'];
            $araz_general_detail = $araz['araz_data'];
            $previous_study_details = $araz['previous_study_details'];
            $course_details = $araz['course_details'];
            $counselor_details = $araz['counselor_details'];

            if($user_details['araz_type'] == 'raza'){
              include 'inc/inc.show_parsed_araiz_details.php';
            }
          }
          ?>
        </div>
        
        <div role="tabpanel" class="tab-pane" id="istirshad_araiz">
          <div class="col-xs-12 col-sm-3" style="padding-top:6px">
            <input type="checkbox" name="print" value="Print" id="check_all_istirshad" onchange="checkAllIstirshad(this);" class="css-checkbox1"><label for="check_all_istirshad" class="css-label1">Check All</label>
          </div>
          <div class="col-xs-12 col-sm-3 pull-right">
            <input type="submit" name="clear_assign_istirshad" value="Remove Assign Data" class="btn btn-success btn-block">
          </div>
          <div class="col-xs-12 col-sm-3 pull-right">
            <input type="submit" name="print_list_istirshad" value="Print" class="btn btn-success btn-block assign-print">
          </div>
          <div class="row">
            <div class="col-xs-12 col-sm-12">&nbsp;</div>
          </div>
          <?php
          foreach ($araiz_parsed_data as $araz_id => $araz) {
            // Create bins for User Data, Araz Data, Previous Studies, Current Studeis, Remarks
            $user_details = $araz['user_data'];
            $araz_general_detail = $araz['araz_data'];
            $previous_study_details = $araz['previous_study_details'];
            $course_details = $araz['course_details'];
            $counselor_details = $araz['counselor_details'];

            if($user_details['araz_type'] == 'istirshad'){
              include 'inc/inc.show_parsed_araiz_details.php';
            }
          }
          ?>
        </div>
      </div>
      <?php echo $pagination; ?>
    </div>
  </div>
</form>
<form method="post" id="frm-del-grp">
  <input type="hidden" name="delete_id" id="del-araz-val" value="">
</form>
</div>
<style>
  .popover{
    color:#000;
  }
</style>

<?php
include 'footer.php';
?>
