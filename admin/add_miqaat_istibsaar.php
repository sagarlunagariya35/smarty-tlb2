<?php
require_once '../classes/class.database.php';
include 'classes/class.article.php';
$allowed_roles = array(ROLE_HEAD_OFFICE);
require_once 'session.php';

$title = 'Miqaat Istibsaar';
$description = '';
$keywords = '';
$active_page = "list_miqaat_istibsaar";
$art = new Article();

if (isset($_POST['btn_submit'])) {
  
  $data = $db->clean_data($_POST);
  $miqaat_title = $data['miqaat_title'];
  $slug = str_replace(' ', '', $data['slug']);
  if ($slug == '') {
    $slug = str_replace(' ', '', $miqaat_title);
  }
  $user_id = $_SESSION[USER_ID];
  $year = $data['year'];
  $logo_file = $_FILES['logo']['name'];
  $logo_temp = $_FILES['logo']['tmp_name'];
  $show_pre = $data['show_pre'];
  $show_in = $data['show_in'];
  $show_post = $data['show_post'];

  $logopathANDname = "../upload/miqaat_upload/logo/" . $logo_file;
  move_uploaded_file($logo_temp, $logopathANDname);

  $miqaat_insert = $art->insert_miqaat_istibsaar($miqaat_title, $slug, $user_id, $year, $logo_file, $show_pre, $show_in, $show_post);

  if ($miqaat_insert) {
    $_SESSION[SUCCESS_MESSAGE] = 'Miqaat Istibsaar Successfully Created.';
  } else {
    $_SESSION[ERROR_MESSAGE] = 'Error In Inserting Data';
  }
}

include_once("header.php");
?>
<script type="text/javascript" src="ckeditor/ckeditor.js"></script>

<div class="row">
  <div class="col-lg-12">
    <h1 class="page-header"><?php echo $_SESSION[USER_ID]; ?> Add Miqaat, Stationary, Videos, Audios <?php //echo $_SESSION['user_its'];   ?></h1>
  </div>
  <!-- /.col-lg-12 -->
</div>

<div class="row">
  <div class="col-md-12">
    <form method="post" enctype="multipart/form-data">

      <div class="form-group col-md-6 col-xs-12">
        <label for="title">Miqaat Title :</label>
        <input type="text" name="miqaat_title" id="title" required="required" class="form-control" >
      </div>

      <div class="form-group col-md-6 col-xs-12">
        <label for="slug">Slug :</label>
        <input type="text" name="slug" id="slug" required="required" class="form-control" >
      </div>

      <div class="form-group col-md-3 col-xs-12">
        <label for="year">Select Year :</label>
        <select name="year" id="year" class="form-control" required >
          <option value="">Select Year</option>
          <option value="1435">1435</option>
          <option value="1436">1436</option>
          <option value="1437">1437</option>
        </select>
      </div>

      <div class="form-group col-md-6 col-xs-12">
        <label for="miqaat_logo">Miqaat Logo :</label>
        <input type="file" name="logo" class="file" onchange='$("#miqaat_logo1").val($(this).val());'>
      </div>

      <div class="clearfix"></div>

      <div class="form-group col-md-4 col-xs-12">
        <div class="checkbox">
          <label>
            <input type="checkbox" name="show_pre" id="show_pre" value="1">Show Pre event tab
          </label>
        </div>
      </div>

      <div class="form-group col-md-4 col-xs-12">
        <div class="checkbox">
          <label>
            <input type="checkbox" name="show_in" id="show_in" value="1">Show In event tab
          </label>
        </div>
      </div>

      <div class="form-group col-md-4 col-xs-12">
        <div class="checkbox">
          <label>
            <input type="checkbox" name="show_post" id="show_post" value="1">Show Post event tab
          </label>
        </div>
      </div>

      <div class="clearfix"></div>
      <div class="form-group col-md-4 col-xs-12">
      <input type="submit" name="btn_submit" id="btn_submit" value="Submit" class="btn btn-block btn-primary">
      </div>
    </form>

  </div>
</div>

<script type="text/javascript">

//DATE VALIDATION
  function datevalidation()
  {

    var startDate = new Date($('#start_date').val());
    var endDate = new Date($('#end_date').val());

    if (startDate < endDate)
    {
      // Do something
    }
    else
    {
      alert('End Date Must be Greater');
      $('#end_date').val('');
    }
  }

</script>
<!-- /#page-wrapper -->
<?php include "./footer.php"; ?>
