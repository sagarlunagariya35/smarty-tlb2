<?php
require 'smarty/libs/Smarty.class.php';
require_once 'session.php';
require_once 'requires_login.php';

$body_class = 'page-sub-page';

$year = $_GET['year'];
$event_id = trim($_GET['page'], "/");

if (isset($_POST['register'])) {
  $post_event_id = $_POST['event_id'];
  $its_id = $_SESSION[USER_ITS];
  
  $event_insert = insert_event_register($post_event_id, $its_id);
  
  if ($event_insert) {
    $_SESSION[SUCCESS_MESSAGE] = "Your Event Register Successfully";
  }else {
    $_SESSION[ERROR_MESSAGE] = "Error In Inserting Data";
  }
}

if (isset($_POST['cancel_event'])) {
  $post_event_id = $_POST['event_id'];
  $its_id = $_SESSION[USER_ITS];
  $reason = $_POST['reason'];
  
  $event_insert = cancel_event_register($post_event_id, $its_id, $reason);
  
  if ($event_insert) {
    $_SESSION[SUCCESS_MESSAGE] = "Your Event Registtation Cancel Successfully";
  }else {
    $_SESSION[ERROR_MESSAGE] = "Error In Cancel Data";
  }
}

$menu_id = get_value_by_name('tlb_menu', $year, ' AND `level` = (SELECT id FROM `tlb_menu` WHERE `name` = \'Events\')');
$all_events = get_article_by_submenu('events', $menu_id[0]['id']);
$event = ($event_id > 0) ? get_article_by_id('events', $event_id) : FALSE;
$event_register = get_event_register($event_id,$_SESSION[USER_ITS]);
$total_event_register = count_all_event_register($event_id);
$videos = get_event_media_by_id('event_video', $event['id']);
$audios = get_event_media_by_id('event_audio', $event['id']);
$images = get_event_media_by_id('event_image', $event['id']);

require_once 'inc/inc.header2.php';
require_once 'inc/inc.footer.php';

$smarty = new Smarty;

// Header / Session variables
$smarty->assign("server_path", SERVER_PATH);
$smarty->assign("page_title", "");
$smarty->assign("loggedin_user", "");
$smarty->assign("active_menu", 'index');
$smarty->assign("active_tree", '');
$smarty->assign("year", $year);
$smarty->assign("event_id", $event_id);
$smarty->assign("menu_id", $menu_id);
$smarty->assign("all_events", $all_events);
$smarty->assign("event", $event);
$smarty->assign("videos", $videos);
$smarty->assign("audios", $audios);
$smarty->assign("images", $images);
$smarty->assign("event_register", $event_register);
$smarty->assign("total_event_register", $total_event_register);

$smarty->display('events.tpl');