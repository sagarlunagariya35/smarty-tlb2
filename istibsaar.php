<?php
require_once 'session.php';
require_once 'requires_login.php';

$tag = 'ashara-mubarakah';
// Page body class
$body_class = 'page-sub-page';
$sentences = get_all_sentences();

require_once 'inc/inc.header2.php';
?>

<div class="container white-bg">
  <div class="col-md-12 col-sm-12">
    <div class="page-title">
      <p style="margin-top:5px;"><a href="#">Home</a> / <a href="#">Istibsaar</a> / <a href="#" class="active">Asharah Mubarakah 1436 H</a></p>
      <h1>Asharah Mubarakah 1436 H</h1>
    </div>
  </div>
  <div class="clearfix"></div> <!-- do not delete -->
    <div class="col-md-12 col-sm-12">
      <div class="page">
        <form class="form1 white" method="post" action="">
          
        <div class="clearfix"></div> <!-- do not delete -->  
        <div class="block top-mgn10"></div>
          <div class="blue-box1">
            <h3 class="bordered">
              <span class="lsd">
                <p class="lsd large-fonts" dir="rtl">مولانا المنعام ط ع ــ الداعي الاجل الحي المقدس سيدنا محمد برهان الدين رض نا ذكرى چھے. برهان الدين مولى رض جھ هر علم نا منبع چھے ـ اهني اْپ يھ العشرة المباركة 1436هـ ما عجب شان سي معرفة كراوي، اْ پهلا عرس نا مبارك ميقات پر يھ بيانو ما سي 53 كلمات يهاں ليوا ما اْيا چھے، مؤمنين ــ يھ كلمات نے وارم وار پڑهے، سنے انے حفظ بھي كرے ــ تو ان شاء الله تع يھ علم نا درجات ما اعلى حظ حاصل كروا نو سبب بنسے.</p>
              </span>
            </h3>
            <div class="clearfix"></div> <!-- do not delete -->
          </div>
          <br><br>
          <div class="row">
            <?php $i=1; foreach ($sentences as $sent) { ?>
              <div class="col-md-12 lsd pull-right" dir="rtl">
                <div class="blue-box1 blue-box1-level col-md-3 col-xs-12 pull-right">
                  <a class="" href="<?php echo SERVER_PATH.'istibsaar/'.$tag.'/'.$sent['id'].'/'; ?>"><?php echo $sent['sr']; ?> - <?php echo $sent['topic']; ?> </a>
                </div>
                <div class="clearfix"></div> <!-- do not delete -->

                <div class="lsd" dir="rtl">
                  <?php
                    $str = substr($sent['bayan'], 0, 200);
                  ?>
                  <p class="color1"><?php echo substr($str, 0, strrpos($str, ' ')).'...'; ?></p>
                </div>
                <br>
                <hr>
              </div>
            <?php } ?>
          </div>
        </form>
      </div>
    </div>
</div>
<style>
  .blue-box1-level {
    background-color: #549BC7 !important;
  }
  .blue-box1-level a {
    color: #FFF !important;
    text-decoration: none;
  }
</style>

<?php
require_once 'inc/inc.footer.php';
?>