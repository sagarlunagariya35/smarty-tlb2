<?php
require_once 'session.php';
require_once 'requires_login.php';

$tag = 'ashara-mubarakah';
$id = $_GET['id'];

$sentence = get_single_sentences($id);
$count = count_all_sentences();

if($id == '1'){
  $prev_id = '1';
}else {
  $prev_id = $id - 1;
}

if($id == $count){
  $next_id = $count;
}else {
  $next_id = $id + 1;
}
// Page body class
$body_class = 'page-sub-page';

require_once 'inc/inc.header2.php';
?>

<div class="container white-bg">
  <div class="col-md-12 col-sm-12">
    <div class="page-title">
      <p style="margin-top:5px;"><a href="#">Home</a> / <a href="#">Istibsaar</a> / <a href="#" class="active">Asharah Mubarakah 1436 H</a></p>
      <h1>Asharah Mubarakah 1436 H</h1>
    </div>
  </div>
  <div class="clearfix"></div> <!-- do not delete -->
    <div class="col-md-12 col-sm-12">
      <div class="page">
        <form class="form1 white" method="post" action="">
          
        <div class="clearfix"></div> <!-- do not delete -->  
        <div class="block top-mgn10"></div>
          <div class="blue-box1">
            <h3 class="bordered">
              <span class="lsd">
                <p class="lsd large-fonts text-center" dir="rtl"><?php echo $sentence['sr']; ?> - <?php echo $sentence['topic']; ?></p>                
                <p class="lsd large-fonts text-center" dir="rtl"><?php echo $sentence['bayan']; ?></p><br>
              </span>
            </h3>
            <div class="clearfix"></div>
          </div>
          <div class="clearfix"></div>
          
          <br>
          <div class="row text-center">
            <div class="col-xs-12 text-center">
              <audio controls="controls" style="margin:0 auto;">
                <source src="<?php echo $sentence['audio_link']; ?>" type="audio/mpeg">
              </audio>
            </div>
          </div>
          <br>
          <p class="large-fonts text-center color-black">Note: Downloading of audio file may take a few seconds before playback. Please be patient</p><br>
          
          <div class="row">
            <div class="col-xs-12 text-center">
              <a href="<?php echo SERVER_PATH.'istibsaar/'.$tag.'/'.$prev_id.'/'; ?>" class="skip">Previous</a>
              <a href="<?php echo SERVER_PATH.'istibsaar/'.$tag.'/'; ?>" class="skip">Index</a>
              <a href="<?php echo SERVER_PATH.'istibsaar/'.$tag.'/'.$next_id.'/'; ?>" class="skip">Next</a>
            </div>
          </div>
          <div class="clearfix"></div> <!-- do not delete -->  
               
            <div class="clearfix"></div> <!-- do not delete -->  
          </div>
            <br>
        </form>
      </div>
    </div>
</div>
<style>
  .skip{
    display:inline-block;
    width:auto;
    -webkit-transition: 0.4s;
    -moz-transition: 0.4s;
    -o-transition: 0.4s;
    transition: 0.4s;
    border: none;
    background-color: #033054;
    height: auto;
    margin:20px auto;
    padding:10px;
    outline: none;
    color:#fff;
    max-width:350px;
    text-transform:uppercase;
    text-align:center;
    line-height:20px;
    width:100px;
  }
  .skip:hover{
    color:#FFF;
    text-decoration: none;
  }
</style>

<?php
require_once 'inc/inc.footer.php';
?>