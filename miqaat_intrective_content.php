<?php
require_once 'session.php';
require_once 'requires_login.php';

$body_class = 'page-sub-page';

require_once 'inc/inc.header2.php';
?>

<!-- Contents -->
<!-- ====================================================================================================== -->
<div class="container white-bg">
  <div class="col-md-12 col-sm-12">

    <div class="page-title">
      <p style="margin-top:5px;">
        <a href="#" class="active">Istibsaar 1436 H</a></p>
      <h1>Istibsaar 1436 H<span class="alfatemi-text">معلومات ذاتية</span></h1>
    </div>
  </div>
  <div class="clearfix"></div> <!-- do not delete -->
  <form class="forms1 white" name="raza_form" method="post">
    <div class="col-md-3 col-sm-12 hidden-xs">
      <div class="profile-box-static">
        <h3 class="uppercase text-center">Tareekhi Event</h3>
      </div>
      <div class="profile-box-static-bottom">
        <a href="#">» 1435 H<hr></a>
        <a href="#">» 1436 H<hr></a>
        <a href="#">» 1437 H<hr></a>
      </div>
    </div>

    <div class="col-md-8 col-sm-12">
      <div class="static-show-button lsd large-fonts col-xs-12 col-sm-5" dir="rtl">معلومات ذاتية</div>
      <div class="clearfix"></div>
      
      <div class="col-md-4">
        <div class="miqaat_stat text-center">
          <a href="tasavvuraat_araz.php"><span>Tasavvuraat Araz</span></a>
        </div>
      </div>
      
      <div class="col-md-1"></div>
      
      <div class="col-md-4">
        <div class="miqaat_stat text-center">
          <a href=""><span>Iqtebasaat</span></a>
        </div>
      </div>
      
      <div class="col-md-4">
        <div class="miqaat_stat text-center">
          <a href="quiz_group_list.php"><span>Take Quiz</span></a>
        </div>
      </div>
      
      <div class="col-md-1"></div>
      
      <div class="col-md-4">
        <div class="miqaat_stat text-center">
          <a href="<?php echo SERVER_PATH . 'istibsaar/ashara-mubarakah/'; ?>"><span>53 Sentences</span></a>
        </div>
      </div>
      
    </div>
  </form>
</div>

<style>
  .miqaat_stat:hover{
    background-color: #549BC7;
  }
</style>

<?php
require_once 'inc/inc.footer.php';
?>
